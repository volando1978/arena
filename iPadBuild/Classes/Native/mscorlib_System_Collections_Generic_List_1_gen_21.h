﻿#pragma once
#include <stdint.h>
// cabezaScr/Frame[]
struct FrameU5BU5D_t3823;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<cabezaScr/Frame>
struct  List_1_t772  : public Object_t
{
	// T[] System.Collections.Generic.List`1<cabezaScr/Frame>::_items
	FrameU5BU5D_t3823* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<cabezaScr/Frame>::_version
	int32_t ____version_3;
};
struct List_1_t772_StaticFields{
	// T[] System.Collections.Generic.List`1<cabezaScr/Frame>::EmptyArray
	FrameU5BU5D_t3823* ___EmptyArray_4;
};
