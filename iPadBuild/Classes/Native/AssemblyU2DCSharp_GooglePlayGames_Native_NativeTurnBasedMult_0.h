﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
struct Action_2_t632;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E
struct  U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634  : public Object_t
{
	// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::userCallback
	Action_2_t632 * ___userCallback_0;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_1;
};
