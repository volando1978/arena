﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t3963;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t4455;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1267;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m22938(__this, method) (( void (*) (Stack_1_t3963 *, MethodInfo*))Stack_1__ctor_m21668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m22939(__this, method) (( bool (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m22940(__this, method) (( Object_t * (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m21670_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m22941(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3963 *, Array_t *, int32_t, MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m21671_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22942(__this, method) (( Object_t* (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m22943(__this, method) (( Object_t * (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m22944(__this, method) (( List_1_t1267 * (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_Peek_m21674_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m22945(__this, method) (( List_1_t1267 * (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_Pop_m21675_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m22946(__this, ___t, method) (( void (*) (Stack_1_t3963 *, List_1_t1267 *, MethodInfo*))Stack_1_Push_m21676_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m22947(__this, method) (( int32_t (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_get_Count_m21677_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m22948(__this, method) (( Enumerator_t4456  (*) (Stack_1_t3963 *, MethodInfo*))Stack_1_GetEnumerator_m21678_gshared)(__this, method)
