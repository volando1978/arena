﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.AsyncExec
struct AsyncExec_t145;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t26;

// System.Void UnityEngine.Advertisements.AsyncExec::.ctor()
extern "C" void AsyncExec__ctor_m703 (AsyncExec_t145 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.AsyncExec::.cctor()
extern "C" void AsyncExec__cctor_m704 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour UnityEngine.Advertisements.AsyncExec::getImpl()
extern "C" MonoBehaviour_t26 * AsyncExec_getImpl_m705 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.AsyncExec UnityEngine.Advertisements.AsyncExec::getAsyncImpl()
extern "C" AsyncExec_t145 * AsyncExec_getAsyncImpl_m706 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
