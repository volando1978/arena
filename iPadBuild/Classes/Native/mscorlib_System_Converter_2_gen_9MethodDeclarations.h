﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>
struct Converter_2_t3461;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>::.ctor(System.Object,System.IntPtr)
// System.Converter`2<System.Object,System.Object>
#include "mscorlib_System_Converter_2_gen_2MethodDeclarations.h"
#define Converter_2__ctor_m15898(__this, ___object, ___method, method) (( void (*) (Converter_2_t3461 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15602_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>::Invoke(TInput)
#define Converter_2_Invoke_m15899(__this, ___input, method) (( Object_t * (*) (Converter_2_t3461 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))Converter_2_Invoke_m15604_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m15900(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3461 *, UnityKeyValuePair_2_t3457 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15606_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m15901(__this, ___result, method) (( Object_t * (*) (Converter_2_t3461 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15608_gshared)(__this, ___result, method)
