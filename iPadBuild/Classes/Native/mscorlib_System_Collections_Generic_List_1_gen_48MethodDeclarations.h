﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t1651;
// System.Object
struct Object_t;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1650;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.PersistentCall>
struct IEnumerable_1_t4527;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Events.PersistentCall>
struct IEnumerator_1_t4528;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.Events.PersistentCall>
struct ICollection_1_t4529;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.PersistentCall>
struct ReadOnlyCollection_1_t4154;
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t4152;
// System.Predicate`1<UnityEngine.Events.PersistentCall>
struct Predicate_1_t4155;
// System.Action`1<UnityEngine.Events.PersistentCall>
struct Action_1_t4156;
// System.Comparison`1<UnityEngine.Events.PersistentCall>
struct Comparison_1_t4157;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m7541(__this, method) (( void (*) (List_1_t1651 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m25321(__this, ___collection, method) (( void (*) (List_1_t1651 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor(System.Int32)
#define List_1__ctor_m25322(__this, ___capacity, method) (( void (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.cctor()
#define List_1__cctor_m25323(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25324(__this, method) (( Object_t* (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m25325(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1651 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m25326(__this, method) (( Object_t * (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m25327(__this, ___item, method) (( int32_t (*) (List_1_t1651 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m25328(__this, ___item, method) (( bool (*) (List_1_t1651 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m25329(__this, ___item, method) (( int32_t (*) (List_1_t1651 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m25330(__this, ___index, ___item, method) (( void (*) (List_1_t1651 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m25331(__this, ___item, method) (( void (*) (List_1_t1651 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25332(__this, method) (( bool (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m25333(__this, method) (( bool (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m25334(__this, method) (( Object_t * (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m25335(__this, method) (( bool (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m25336(__this, method) (( bool (*) (List_1_t1651 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m25337(__this, ___index, method) (( Object_t * (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m25338(__this, ___index, ___value, method) (( void (*) (List_1_t1651 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Add(T)
#define List_1_Add_m25339(__this, ___item, method) (( void (*) (List_1_t1651 *, PersistentCall_t1650 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m25340(__this, ___newCount, method) (( void (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m25341(__this, ___collection, method) (( void (*) (List_1_t1651 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m25342(__this, ___enumerable, method) (( void (*) (List_1_t1651 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m25343(__this, ___collection, method) (( void (*) (List_1_t1651 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::AsReadOnly()
#define List_1_AsReadOnly_m25344(__this, method) (( ReadOnlyCollection_1_t4154 * (*) (List_1_t1651 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Clear()
#define List_1_Clear_m25345(__this, method) (( void (*) (List_1_t1651 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Contains(T)
#define List_1_Contains_m25346(__this, ___item, method) (( bool (*) (List_1_t1651 *, PersistentCall_t1650 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m25347(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1651 *, PersistentCallU5BU5D_t4152*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m25348(__this, ___match, method) (( PersistentCall_t1650 * (*) (List_1_t1651 *, Predicate_1_t4155 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m25349(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4155 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m25350(__this, ___match, method) (( int32_t (*) (List_1_t1651 *, Predicate_1_t4155 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m25351(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1651 *, int32_t, int32_t, Predicate_1_t4155 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m25352(__this, ___action, method) (( void (*) (List_1_t1651 *, Action_1_t4156 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m7542(__this, method) (( Enumerator_t1700  (*) (List_1_t1651 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::IndexOf(T)
#define List_1_IndexOf_m25353(__this, ___item, method) (( int32_t (*) (List_1_t1651 *, PersistentCall_t1650 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m25354(__this, ___start, ___delta, method) (( void (*) (List_1_t1651 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m25355(__this, ___index, method) (( void (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Insert(System.Int32,T)
#define List_1_Insert_m25356(__this, ___index, ___item, method) (( void (*) (List_1_t1651 *, int32_t, PersistentCall_t1650 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m25357(__this, ___collection, method) (( void (*) (List_1_t1651 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Remove(T)
#define List_1_Remove_m25358(__this, ___item, method) (( bool (*) (List_1_t1651 *, PersistentCall_t1650 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m25359(__this, ___match, method) (( int32_t (*) (List_1_t1651 *, Predicate_1_t4155 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m25360(__this, ___index, method) (( void (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Reverse()
#define List_1_Reverse_m25361(__this, method) (( void (*) (List_1_t1651 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Sort()
#define List_1_Sort_m25362(__this, method) (( void (*) (List_1_t1651 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m25363(__this, ___comparison, method) (( void (*) (List_1_t1651 *, Comparison_1_t4157 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::ToArray()
#define List_1_ToArray_m25364(__this, method) (( PersistentCallU5BU5D_t4152* (*) (List_1_t1651 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::TrimExcess()
#define List_1_TrimExcess_m25365(__this, method) (( void (*) (List_1_t1651 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Capacity()
#define List_1_get_Capacity_m25366(__this, method) (( int32_t (*) (List_1_t1651 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m25367(__this, ___value, method) (( void (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Count()
#define List_1_get_Count_m25368(__this, method) (( int32_t (*) (List_1_t1651 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Item(System.Int32)
#define List_1_get_Item_m25369(__this, ___index, method) (( PersistentCall_t1650 * (*) (List_1_t1651 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m25370(__this, ___index, ___value, method) (( void (*) (List_1_t1651 *, int32_t, PersistentCall_t1650 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
