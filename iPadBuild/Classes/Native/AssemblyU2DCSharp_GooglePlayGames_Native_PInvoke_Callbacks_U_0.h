﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3725  : public Object_t
{
	// T1 GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>::result1
	Object_t * ___result1_0;
	// T2 GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>::result2
	uint8_t ___result2_1;
	// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<T1,T2> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Object,System.Byte>::<>f__ref$96
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3724 * ___U3CU3Ef__refU2496_2;
};
