﻿#pragma once
#include <stdint.h>
// Soomla.Store.StoreEvents/RunLaterDelegate
struct RunLaterDelegate_t87;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3
struct  U3CRunLaterPrivU3Ec__Iterator3_t89  : public Object_t
{
	// Soomla.Store.StoreEvents/RunLaterDelegate Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::runLaterDelegate
	RunLaterDelegate_t87 * ___runLaterDelegate_0;
	// System.Int32 Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::$PC
	int32_t ___U24PC_1;
	// System.Object Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::$current
	Object_t * ___U24current_2;
	// Soomla.Store.StoreEvents/RunLaterDelegate Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::<$>runLaterDelegate
	RunLaterDelegate_t87 * ___U3CU24U3ErunLaterDelegate_3;
};
