﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.VirtualItem>
struct List_1_t179;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>
struct  Enumerator_t226 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::l
	List_1_t179 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::current
	VirtualItem_t125 * ___current_3;
};
