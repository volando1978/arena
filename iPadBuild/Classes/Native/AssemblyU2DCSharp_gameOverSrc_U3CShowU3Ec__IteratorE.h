﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// gameOverSrc
struct gameOverSrc_t801;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// gameOverSrc/<Show>c__IteratorE
struct  U3CShowU3Ec__IteratorE_t803  : public Object_t
{
	// UnityEngine.Color gameOverSrc/<Show>c__IteratorE::<backColor>__0
	Color_t747  ___U3CbackColorU3E__0_0;
	// System.Boolean gameOverSrc/<Show>c__IteratorE::<isPad>__1
	bool ___U3CisPadU3E__1_1;
	// System.Int32 gameOverSrc/<Show>c__IteratorE::$PC
	int32_t ___U24PC_2;
	// System.Object gameOverSrc/<Show>c__IteratorE::$current
	Object_t * ___U24current_3;
	// gameOverSrc gameOverSrc/<Show>c__IteratorE::<>f__this
	gameOverSrc_t801 * ___U3CU3Ef__this_4;
};
