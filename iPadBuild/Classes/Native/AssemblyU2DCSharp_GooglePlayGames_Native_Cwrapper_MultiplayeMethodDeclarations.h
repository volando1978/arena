﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.MultiplayerInvitation
struct MultiplayerInvitation_t435;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"

// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t MultiplayerInvitation_MultiplayerInvitation_AutomatchingSlotsAvailable_m1790 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_InvitingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t MultiplayerInvitation_MultiplayerInvitation_InvitingParticipant_m1791 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t MultiplayerInvitation_MultiplayerInvitation_Variant_m1792 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t MultiplayerInvitation_MultiplayerInvitation_CreationTime_m1793 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Participants_Length_m1794 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t MultiplayerInvitation_MultiplayerInvitation_Participants_GetElement_m1795 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool MultiplayerInvitation_MultiplayerInvitation_Valid_m1796 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Type(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t MultiplayerInvitation_MultiplayerInvitation_Type_m1797 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerInvitation_MultiplayerInvitation_Id_m1798 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.MultiplayerInvitation::MultiplayerInvitation_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerInvitation_MultiplayerInvitation_Dispose_m1799 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
