﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Tutorial
struct Tutorial_t759;

// System.Void Tutorial::.ctor()
extern "C" void Tutorial__ctor_m3295 (Tutorial_t759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::Start()
extern "C" void Tutorial_Start_m3296 (Tutorial_t759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::OnGUI()
extern "C" void Tutorial_OnGUI_m3297 (Tutorial_t759 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
