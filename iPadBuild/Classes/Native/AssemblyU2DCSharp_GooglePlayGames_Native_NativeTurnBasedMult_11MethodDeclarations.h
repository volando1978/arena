﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59
struct U3CLeaveDuringTurnU3Ec__AnonStorey59_t648;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::.ctor()
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59__ctor_m2570 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::<>m__63(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__63_m2571 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, MultiplayerParticipant_t672 * ___pendingParticipant, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<LeaveDuringTurn>c__AnonStorey59::<>m__6C(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CLeaveDuringTurnU3Ec__AnonStorey59_U3CU3Em__6C_m2572 (U3CLeaveDuringTurnU3Ec__AnonStorey59_t648 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
