﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t1341;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t1153;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct  EventFunction_1_t1164  : public MulticastDelegate_t22
{
};
