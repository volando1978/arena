﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t1297;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.StencilMaterial
struct  StencilMaterial_t1298  : public Object_t
{
};
struct StencilMaterial_t1298_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry> UnityEngine.UI.StencilMaterial::m_List
	List_1_t1297 * ___m_List_0;
};
