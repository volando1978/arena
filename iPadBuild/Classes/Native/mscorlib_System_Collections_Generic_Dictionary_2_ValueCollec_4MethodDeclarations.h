﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Enumerator_t928;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Dictionary_2_t575;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"
#define Enumerator__ctor_m20301(__this, ___host, method) (( void (*) (Enumerator_t928 *, Dictionary_2_t575 *, MethodInfo*))Enumerator__ctor_m16310_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20302(__this, method) (( Object_t * (*) (Enumerator_t928 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Dispose()
#define Enumerator_Dispose_m20303(__this, method) (( void (*) (Enumerator_t928 *, MethodInfo*))Enumerator_Dispose_m16312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::MoveNext()
#define Enumerator_MoveNext_m3830(__this, method) (( bool (*) (Enumerator_t928 *, MethodInfo*))Enumerator_MoveNext_m16313_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current()
#define Enumerator_get_Current_m3829(__this, method) (( MultiplayerParticipant_t672 * (*) (Enumerator_t928 *, MethodInfo*))Enumerator_get_Current_m16314_gshared)(__this, method)
