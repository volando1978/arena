﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ArrayList/FixedSizeArrayListWrapper
struct FixedSizeArrayListWrapper_t2431;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t737;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1789;

// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::.ctor(System.Collections.ArrayList)
extern "C" void FixedSizeArrayListWrapper__ctor_m11282 (FixedSizeArrayListWrapper_t2431 * __this, ArrayList_t737 * ___innerList, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.ArrayList/FixedSizeArrayListWrapper::get_ErrorMessage()
extern "C" String_t* FixedSizeArrayListWrapper_get_ErrorMessage_m11283 (FixedSizeArrayListWrapper_t2431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/FixedSizeArrayListWrapper::get_IsFixedSize()
extern "C" bool FixedSizeArrayListWrapper_get_IsFixedSize_m11284 (FixedSizeArrayListWrapper_t2431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/FixedSizeArrayListWrapper::Add(System.Object)
extern "C" int32_t FixedSizeArrayListWrapper_Add_m11285 (FixedSizeArrayListWrapper_t2431 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::AddRange(System.Collections.ICollection)
extern "C" void FixedSizeArrayListWrapper_AddRange_m11286 (FixedSizeArrayListWrapper_t2431 * __this, Object_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Clear()
extern "C" void FixedSizeArrayListWrapper_Clear_m11287 (FixedSizeArrayListWrapper_t2431 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Insert(System.Int32,System.Object)
extern "C" void FixedSizeArrayListWrapper_Insert_m11288 (FixedSizeArrayListWrapper_t2431 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::InsertRange(System.Int32,System.Collections.ICollection)
extern "C" void FixedSizeArrayListWrapper_InsertRange_m11289 (FixedSizeArrayListWrapper_t2431 * __this, int32_t ___index, Object_t * ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Remove(System.Object)
extern "C" void FixedSizeArrayListWrapper_Remove_m11290 (FixedSizeArrayListWrapper_t2431 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::RemoveAt(System.Int32)
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m11291 (FixedSizeArrayListWrapper_t2431 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
