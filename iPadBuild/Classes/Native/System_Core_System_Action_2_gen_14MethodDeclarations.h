﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
struct Action_2_t632;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Byte,System.Object>
#include "System_Core_System_Action_2_gen_19MethodDeclarations.h"
#define Action_2__ctor_m18853(__this, ___object, ___method, method) (( void (*) (Action_2_t632 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m18854_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::Invoke(T1,T2)
#define Action_2_Invoke_m18855(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t632 *, bool, TurnBasedMatch_t353 *, MethodInfo*))Action_2_Invoke_m18856_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m18857(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t632 *, bool, TurnBasedMatch_t353 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m18858_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m18859(__this, ___result, method) (( void (*) (Action_2_t632 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m18860_gshared)(__this, ___result, method)
