﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t1224;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.FontUpdateTracker
struct  FontUpdateTracker_t1225  : public Object_t
{
};
struct FontUpdateTracker_t1225_StaticFields{
	// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>> UnityEngine.UI.FontUpdateTracker::m_Tracked
	Dictionary_2_t1224 * ___m_Tracked_0;
};
