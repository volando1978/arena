﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal
struct ShowUICallbackInternal_t659;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::.ctor(System.Object,System.IntPtr)
extern "C" void ShowUICallbackInternal__ctor_m2631 (ShowUICallbackInternal_t659 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowUICallbackInternal_Invoke_m2632 (ShowUICallbackInternal_t659 * __this, int32_t ___status, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ShowUICallbackInternal_t659(Il2CppObject* delegate, int32_t ___status, IntPtr_t ___data);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * ShowUICallbackInternal_BeginInvoke_m2633 (ShowUICallbackInternal_t659 * __this, int32_t ___status, IntPtr_t ___data, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal::EndInvoke(System.IAsyncResult)
extern "C" void ShowUICallbackInternal_EndInvoke_m2634 (ShowUICallbackInternal_t659 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
