﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t702;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeSnapshotMetadata>
struct IEnumerable_1_t892;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C" void FetchAllResponse__ctor_m3020 (FetchAllResponse_t702 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::ResponseStatus()
extern "C" int32_t FetchAllResponse_ResponseStatus_m3021 (FetchAllResponse_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::RequestSucceeded()
extern "C" bool FetchAllResponse_RequestSucceeded_m3022 (FetchAllResponse_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeSnapshotMetadata> GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::Data()
extern "C" Object_t* FetchAllResponse_Data_m3023 (FetchAllResponse_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchAllResponse_CallDispose_m3024 (FetchAllResponse_t702 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C" FetchAllResponse_t702 * FetchAllResponse_FromPointer_m3025 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse::<Data>m__9E(System.UIntPtr)
extern "C" NativeSnapshotMetadata_t611 * FetchAllResponse_U3CDataU3Em__9E_m3026 (FetchAllResponse_t702 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
