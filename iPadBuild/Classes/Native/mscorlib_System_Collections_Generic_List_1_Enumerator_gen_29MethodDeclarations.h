﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>
struct Enumerator_t3847;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t809;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t810;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m21272(__this, ___l, method) (( void (*) (Enumerator_t3847 *, List_1_t810 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21273(__this, method) (( Object_t * (*) (Enumerator_t3847 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::Dispose()
#define Enumerator_Dispose_m21274(__this, method) (( void (*) (Enumerator_t3847 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::VerifyState()
#define Enumerator_VerifyState_m21275(__this, method) (( void (*) (Enumerator_t3847 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::MoveNext()
#define Enumerator_MoveNext_m21276(__this, method) (( bool (*) (Enumerator_t3847 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::get_Current()
#define Enumerator_get_Current_m21277(__this, method) (( Transform_t809 * (*) (Enumerator_t3847 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
