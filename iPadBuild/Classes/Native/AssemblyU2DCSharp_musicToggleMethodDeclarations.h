﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// musicToggle
struct musicToggle_t819;

// System.Void musicToggle::.ctor()
extern "C" void musicToggle__ctor_m3584 (musicToggle_t819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void musicToggle::Start()
extern "C" void musicToggle_Start_m3585 (musicToggle_t819 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void musicToggle::<Start>m__A6(System.Boolean)
extern "C" void musicToggle_U3CStartU3Em__A6_m3586 (Object_t * __this /* static, unused */, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
