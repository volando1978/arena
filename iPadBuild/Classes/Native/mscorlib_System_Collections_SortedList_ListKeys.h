﻿#pragma once
#include <stdint.h>
// System.Collections.SortedList
struct SortedList_t2143;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.SortedList/ListKeys
struct  ListKeys_t2446  : public Object_t
{
	// System.Collections.SortedList System.Collections.SortedList/ListKeys::host
	SortedList_t2143 * ___host_0;
};
