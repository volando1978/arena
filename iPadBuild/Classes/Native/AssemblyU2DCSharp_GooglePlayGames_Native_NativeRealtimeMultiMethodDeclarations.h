﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener
struct NoopListener_t562;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.String[]
struct StringU5BU5D_t169;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::.ctor()
extern "C" void NoopListener__ctor_m2320 (NoopListener_t562 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRoomSetupProgress(System.Single)
extern "C" void NoopListener_OnRoomSetupProgress_m2321 (NoopListener_t562 * __this, float ___percent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRoomConnected(System.Boolean)
extern "C" void NoopListener_OnRoomConnected_m2322 (NoopListener_t562 * __this, bool ___success, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnLeftRoom()
extern "C" void NoopListener_OnLeftRoom_m2323 (NoopListener_t562 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnParticipantLeft(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" void NoopListener_OnParticipantLeft_m2324 (NoopListener_t562 * __this, Participant_t340 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnPeersConnected(System.String[])
extern "C" void NoopListener_OnPeersConnected_m2325 (NoopListener_t562 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnPeersDisconnected(System.String[])
extern "C" void NoopListener_OnPeersDisconnected_m2326 (NoopListener_t562 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRealTimeMessageReceived(System.Boolean,System.String,System.Byte[])
extern "C" void NoopListener_OnRealTimeMessageReceived_m2327 (NoopListener_t562 * __this, bool ___isReliable, String_t* ___senderId, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
