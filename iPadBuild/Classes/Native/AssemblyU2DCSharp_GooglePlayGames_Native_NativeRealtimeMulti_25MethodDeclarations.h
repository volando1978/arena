﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey2F__ctor_m2461 (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F::<>m__28(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_U3CU3Em__28_m2462 (U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * __this, PlayerSelectUIResponse_t686 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
