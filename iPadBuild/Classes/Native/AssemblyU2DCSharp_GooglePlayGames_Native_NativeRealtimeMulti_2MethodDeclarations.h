﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C
struct U3CRoomConnectedU3Ec__AnonStorey3C_t568;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C::.ctor()
extern "C" void U3CRoomConnectedU3Ec__AnonStorey3C__ctor_m2354 (U3CRoomConnectedU3Ec__AnonStorey3C_t568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C::<>m__31()
extern "C" void U3CRoomConnectedU3Ec__AnonStorey3C_U3CU3Em__31_m2355 (U3CRoomConnectedU3Ec__AnonStorey3C_t568 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
