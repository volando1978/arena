﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.VirtualGood>
struct List_1_t116;
// System.Object
struct Object_t;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.VirtualGood>
struct IEnumerable_1_t225;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualGood>
struct IEnumerator_1_t4323;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.VirtualGood>
struct ICollection_1_t4324;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualGood>
struct ReadOnlyCollection_1_t3596;
// Soomla.Store.VirtualGood[]
struct VirtualGoodU5BU5D_t173;
// System.Predicate`1<Soomla.Store.VirtualGood>
struct Predicate_1_t3597;
// System.Action`1<Soomla.Store.VirtualGood>
struct Action_1_t3598;
// System.Comparison`1<Soomla.Store.VirtualGood>
struct Comparison_1_t3599;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m1011(__this, method) (( void (*) (List_1_t116 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17814(__this, ___collection, method) (( void (*) (List_1_t116 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::.ctor(System.Int32)
#define List_1__ctor_m17815(__this, ___capacity, method) (( void (*) (List_1_t116 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::.cctor()
#define List_1__cctor_m17816(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17817(__this, method) (( Object_t* (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17818(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t116 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17819(__this, method) (( Object_t * (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17820(__this, ___item, method) (( int32_t (*) (List_1_t116 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17821(__this, ___item, method) (( bool (*) (List_1_t116 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17822(__this, ___item, method) (( int32_t (*) (List_1_t116 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17823(__this, ___index, ___item, method) (( void (*) (List_1_t116 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17824(__this, ___item, method) (( void (*) (List_1_t116 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17825(__this, method) (( bool (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17826(__this, method) (( bool (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17827(__this, method) (( Object_t * (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17828(__this, method) (( bool (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17829(__this, method) (( bool (*) (List_1_t116 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17830(__this, ___index, method) (( Object_t * (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17831(__this, ___index, ___value, method) (( void (*) (List_1_t116 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Add(T)
#define List_1_Add_m17832(__this, ___item, method) (( void (*) (List_1_t116 *, VirtualGood_t79 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17833(__this, ___newCount, method) (( void (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17834(__this, ___collection, method) (( void (*) (List_1_t116 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17835(__this, ___enumerable, method) (( void (*) (List_1_t116 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17836(__this, ___collection, method) (( void (*) (List_1_t116 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::AsReadOnly()
#define List_1_AsReadOnly_m17837(__this, method) (( ReadOnlyCollection_1_t3596 * (*) (List_1_t116 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Clear()
#define List_1_Clear_m17838(__this, method) (( void (*) (List_1_t116 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Contains(T)
#define List_1_Contains_m17839(__this, ___item, method) (( bool (*) (List_1_t116 *, VirtualGood_t79 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17840(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t116 *, VirtualGoodU5BU5D_t173*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Find(System.Predicate`1<T>)
#define List_1_Find_m17841(__this, ___match, method) (( VirtualGood_t79 * (*) (List_1_t116 *, Predicate_1_t3597 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17842(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3597 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m17843(__this, ___match, method) (( int32_t (*) (List_1_t116 *, Predicate_1_t3597 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17844(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t116 *, int32_t, int32_t, Predicate_1_t3597 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m17845(__this, ___action, method) (( void (*) (List_1_t116 *, Action_1_t3598 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::GetEnumerator()
#define List_1_GetEnumerator_m987(__this, method) (( Enumerator_t217  (*) (List_1_t116 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::IndexOf(T)
#define List_1_IndexOf_m17846(__this, ___item, method) (( int32_t (*) (List_1_t116 *, VirtualGood_t79 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17847(__this, ___start, ___delta, method) (( void (*) (List_1_t116 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17848(__this, ___index, method) (( void (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Insert(System.Int32,T)
#define List_1_Insert_m17849(__this, ___index, ___item, method) (( void (*) (List_1_t116 *, int32_t, VirtualGood_t79 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17850(__this, ___collection, method) (( void (*) (List_1_t116 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Remove(T)
#define List_1_Remove_m17851(__this, ___item, method) (( bool (*) (List_1_t116 *, VirtualGood_t79 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17852(__this, ___match, method) (( int32_t (*) (List_1_t116 *, Predicate_1_t3597 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17853(__this, ___index, method) (( void (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Reverse()
#define List_1_Reverse_m17854(__this, method) (( void (*) (List_1_t116 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Sort()
#define List_1_Sort_m17855(__this, method) (( void (*) (List_1_t116 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17856(__this, ___comparison, method) (( void (*) (List_1_t116 *, Comparison_1_t3599 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::ToArray()
#define List_1_ToArray_m17857(__this, method) (( VirtualGoodU5BU5D_t173* (*) (List_1_t116 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::TrimExcess()
#define List_1_TrimExcess_m17858(__this, method) (( void (*) (List_1_t116 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::get_Capacity()
#define List_1_get_Capacity_m17859(__this, method) (( int32_t (*) (List_1_t116 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17860(__this, ___value, method) (( void (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::get_Count()
#define List_1_get_Count_m17861(__this, method) (( int32_t (*) (List_1_t116 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::get_Item(System.Int32)
#define List_1_get_Item_m17862(__this, ___index, method) (( VirtualGood_t79 * (*) (List_1_t116 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::set_Item(System.Int32,T)
#define List_1_set_Item_m17863(__this, ___index, ___value, method) (( void (*) (List_1_t116 *, int32_t, VirtualGood_t79 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
