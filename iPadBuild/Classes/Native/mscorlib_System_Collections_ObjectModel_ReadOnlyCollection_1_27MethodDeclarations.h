﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t3852;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t3851;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1284;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4406;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m21344_gshared (ReadOnlyCollection_1_t3852 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m21344(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3852 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m21344_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21345_gshared (ReadOnlyCollection_1_t3852 * __this, Vector3_t758  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21345(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3852 *, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21345_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21346_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21346(__this, method) (( void (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21346_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21347_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, Vector3_t758  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21347(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21347_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21348_gshared (ReadOnlyCollection_1_t3852 * __this, Vector3_t758  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21348(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21348_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21349_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21349(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21349_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Vector3_t758  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21350_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21350(__this, ___index, method) (( Vector3_t758  (*) (ReadOnlyCollection_1_t3852 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21350_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21351_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, Vector3_t758  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21351(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21351_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21352_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21352(__this, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21352_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21353_gshared (ReadOnlyCollection_1_t3852 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21353(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3852 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21353_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21354_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21354(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21354_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m21355_gshared (ReadOnlyCollection_1_t3852 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m21355(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3852 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m21355_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21356_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m21356(__this, method) (( void (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m21356_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m21357_gshared (ReadOnlyCollection_1_t3852 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m21357(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m21357_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21358_gshared (ReadOnlyCollection_1_t3852 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21358(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3852 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21358_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21359_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m21359(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m21359_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21360_gshared (ReadOnlyCollection_1_t3852 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m21360(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3852 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m21360_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21361_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21361(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21361_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21362_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21362(__this, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21362_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21363_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21363(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21363_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21364_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21364(__this, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21364_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21365_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21365(__this, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21365_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m21366_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m21366(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3852 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m21366_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21367_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m21367(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3852 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m21367_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m21368_gshared (ReadOnlyCollection_1_t3852 * __this, Vector3_t758  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m21368(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3852 *, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_Contains_m21368_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m21369_gshared (ReadOnlyCollection_1_t3852 * __this, Vector3U5BU5D_t1284* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m21369(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3852 *, Vector3U5BU5D_t1284*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m21369_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m21370_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m21370(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m21370_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m21371_gshared (ReadOnlyCollection_1_t3852 * __this, Vector3_t758  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m21371(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3852 *, Vector3_t758 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m21371_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m21372_gshared (ReadOnlyCollection_1_t3852 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m21372(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3852 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m21372_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t758  ReadOnlyCollection_1_get_Item_m21373_gshared (ReadOnlyCollection_1_t3852 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m21373(__this, ___index, method) (( Vector3_t758  (*) (ReadOnlyCollection_1_t3852 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m21373_gshared)(__this, ___index, method)
