﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualItemStorage
struct VirtualItemStorage_t120;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.VirtualItemStorage::.ctor()
extern "C" void VirtualItemStorage__ctor_m576 (VirtualItemStorage_t120 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualItemStorage::.cctor()
extern "C" void VirtualItemStorage__cctor_m577 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItemStorage::_getBalance(Soomla.Store.VirtualItem)
extern "C" int32_t VirtualItemStorage__getBalance_m578 (VirtualItemStorage_t120 * __this, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItemStorage::_setBalance(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualItemStorage__setBalance_m579 (VirtualItemStorage_t120 * __this, VirtualItem_t125 * ___item, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItemStorage::_add(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualItemStorage__add_m580 (VirtualItemStorage_t120 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItemStorage::_remove(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualItemStorage__remove_m581 (VirtualItemStorage_t120 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
