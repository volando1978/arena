﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// tilePrefabScr
struct tilePrefabScr_t835;

// System.Void tilePrefabScr::.ctor()
extern "C" void tilePrefabScr__ctor_m3651 (tilePrefabScr_t835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tilePrefabScr::Awake()
extern "C" void tilePrefabScr_Awake_m3652 (tilePrefabScr_t835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tilePrefabScr::Update()
extern "C" void tilePrefabScr_Update_m3653 (tilePrefabScr_t835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
