﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualCurrencyPack[]
struct VirtualCurrencyPackU5BU5D_t174;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>
struct  List_1_t115  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::_items
	VirtualCurrencyPackU5BU5D_t174* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::_version
	int32_t ____version_3;
};
struct List_1_t115_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::EmptyArray
	VirtualCurrencyPackU5BU5D_t174* ___EmptyArray_4;
};
