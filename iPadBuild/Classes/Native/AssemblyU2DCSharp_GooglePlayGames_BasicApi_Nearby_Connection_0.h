﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_0.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
struct  Status_t357 
{
	// System.Int32 GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status::value__
	int32_t ___value___1;
};
