﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccep.h"
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
struct  QuestAcceptStatus_t370 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.QuestAcceptStatus::value__
	int32_t ___value___1;
};
