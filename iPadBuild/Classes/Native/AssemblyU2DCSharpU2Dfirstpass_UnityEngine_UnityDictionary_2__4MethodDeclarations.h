﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>
struct UnityDictionaryEnumerator_t3454;
// System.Object
struct Object_t;
// UnityEngine.UnityDictionary`2<System.Object,System.Object>
struct UnityDictionary_2_t3406;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::.ctor()
extern "C" void UnityDictionaryEnumerator__ctor_m15775_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator__ctor_m15775(__this, method) (( void (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator__ctor_m15775_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::.ctor(UnityEngine.UnityDictionary`2<K,V>)
extern "C" void UnityDictionaryEnumerator__ctor_m15776_gshared (UnityDictionaryEnumerator_t3454 * __this, UnityDictionary_2_t3406 * ___ud, MethodInfo* method);
#define UnityDictionaryEnumerator__ctor_m15776(__this, ___ud, method) (( void (*) (UnityDictionaryEnumerator_t3454 *, UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionaryEnumerator__ctor_m15776_gshared)(__this, ___ud, method)
// System.Object UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * UnityDictionaryEnumerator_System_Collections_IEnumerator_get_Current_m15777_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_System_Collections_IEnumerator_get_Current_m15777(__this, method) (( Object_t * (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_System_Collections_IEnumerator_get_Current_m15777_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3407  UnityDictionaryEnumerator_get_Current_m15778_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_get_Current_m15778(__this, method) (( KeyValuePair_2_t3407  (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_get_Current_m15778_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::get_Entry()
extern "C" KeyValuePair_2_t3407  UnityDictionaryEnumerator_get_Entry_m15779_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_get_Entry_m15779(__this, method) (( KeyValuePair_2_t3407  (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_get_Entry_m15779_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::Dispose()
extern "C" void UnityDictionaryEnumerator_Dispose_m15780_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_Dispose_m15780(__this, method) (( void (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_Dispose_m15780_gshared)(__this, method)
// K UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * UnityDictionaryEnumerator_get_Key_m15781_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_get_Key_m15781(__this, method) (( Object_t * (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_get_Key_m15781_gshared)(__this, method)
// V UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * UnityDictionaryEnumerator_get_Value_m15782_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_get_Value_m15782(__this, method) (( Object_t * (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_get_Value_m15782_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool UnityDictionaryEnumerator_MoveNext_m15783_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_MoveNext_m15783(__this, method) (( bool (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_MoveNext_m15783_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::ValidateIndex()
extern "C" void UnityDictionaryEnumerator_ValidateIndex_m15784_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_ValidateIndex_m15784(__this, method) (( void (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_ValidateIndex_m15784_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2/UnityDictionaryEnumerator<System.Object,System.Object>::Reset()
extern "C" void UnityDictionaryEnumerator_Reset_m15785_gshared (UnityDictionaryEnumerator_t3454 * __this, MethodInfo* method);
#define UnityDictionaryEnumerator_Reset_m15785(__this, method) (( void (*) (UnityDictionaryEnumerator_t3454 *, MethodInfo*))UnityDictionaryEnumerator_Reset_m15785_gshared)(__this, method)
