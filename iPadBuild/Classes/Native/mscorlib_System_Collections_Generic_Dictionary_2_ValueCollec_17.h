﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct Dictionary_2_t102;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct  ValueCollection_t3574  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::dictionary
	Dictionary_2_t102 * ___dictionary_0;
};
