﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PInvokeUtilities
struct PInvokeUtilities_t682;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
struct OutStringMethod_t681;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities::.cctor()
extern "C" void PInvokeUtilities__cctor_m2855 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.PInvokeUtilities::CheckNonNull(System.Runtime.InteropServices.HandleRef)
extern "C" HandleRef_t657  PInvokeUtilities_CheckNonNull_m2856 (Object_t * __this /* static, unused */, HandleRef_t657  ___reference, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities::IsNull(System.Runtime.InteropServices.HandleRef)
extern "C" bool PInvokeUtilities_IsNull_m2857 (Object_t * __this /* static, unused */, HandleRef_t657  ___reference, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities::IsNull(System.IntPtr)
extern "C" bool PInvokeUtilities_IsNull_m2858 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.PInvoke.PInvokeUtilities::FromMillisSinceUnixEpoch(System.Int64)
extern "C" DateTime_t48  PInvokeUtilities_FromMillisSinceUnixEpoch_m2859 (Object_t * __this /* static, unused */, int64_t ___millisSinceEpoch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.PInvokeUtilities::OutParamsToString(GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod)
extern "C" String_t* PInvokeUtilities_OutParamsToString_m2860 (Object_t * __this /* static, unused */, OutStringMethod_t681 * ___outStringMethod, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.PInvoke.PInvokeUtilities::ToMilliseconds(System.TimeSpan)
extern "C" int64_t PInvokeUtilities_ToMilliseconds_m2861 (Object_t * __this /* static, unused */, TimeSpan_t190  ___span, MethodInfo* method) IL2CPP_METHOD_ATTR;
