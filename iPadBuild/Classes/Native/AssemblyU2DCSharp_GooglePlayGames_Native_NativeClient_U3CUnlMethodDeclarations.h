﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D
struct U3CUnlockAchievementU3Ec__AnonStorey1D_t526;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;

// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::.ctor()
extern "C" void U3CUnlockAchievementU3Ec__AnonStorey1D__ctor_m2241 (U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::<>m__14(GooglePlayGames.BasicApi.Achievement)
extern "C" void U3CUnlockAchievementU3Ec__AnonStorey1D_U3CU3Em__14_m2242 (U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * __this, Achievement_t333 * ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
