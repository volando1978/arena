﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager
struct TurnBasedMultiplayerManager_t501;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
struct PlayerSelectUICallback_t500;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback
struct MultiplayerStatusCallback_t497;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback
struct MatchInboxUICallback_t499;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
struct TurnBasedMatchCallback_t496;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback
struct TurnBasedMatchesCallback_t498;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowPlayerSelectUI(System.Runtime.InteropServices.HandleRef,System.UInt32,System.UInt32,System.Boolean,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowPlayerSelectUI_m2199 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_players, uint32_t ___maximum_players, bool ___allow_automatch, PlayerSelectUICallback_t500 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CancelMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CancelMatch_m2200 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissMatch_m2201 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowMatchInboxUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowMatchInboxUI_m2202 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MatchInboxUICallback_t499 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_SynchronizeData(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_SynchronizeData_m2203 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_Rematch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_Rematch_m2204 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissInvitation_m2205 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatch(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatch_m2206 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___match_id, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DeclineInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DeclineInvitation_m2207 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FinishMatchDuringMyTurn_m2208 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, ByteU5BU5D_t350* ___match_data, UIntPtr_t  ___match_data_size, IntPtr_t ___results, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatches(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatches_m2209 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, TurnBasedMatchesCallback_t498 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CreateTurnBasedMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CreateTurnBasedMatch_m2210 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___config, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_AcceptInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_AcceptInvitation_m2211 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TakeMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TakeMyTurn_m2212 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, ByteU5BU5D_t350* ___match_data, UIntPtr_t  ___match_data_size, IntPtr_t ___results, IntPtr_t ___next_participant, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ConfirmPendingCompletion(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ConfirmPendingCompletion_m2213 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn_m2214 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, IntPtr_t ___next_participant, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn_m2215 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose_m2216 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus_m2217 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch_m2218 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose_m2219 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus_m2220 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length_m2221 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement_m2222 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length_m2223 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement_m2224 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length_m2225 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement_m2226 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length_m2227 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement_m2228 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose_m2229 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus_m2230 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch_m2231 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose_m2232 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus_m2233 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length_m2234 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement_m2235 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers_m2236 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers_m2237 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
