﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct IList_1_t3690;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct  ReadOnlyCollection_1_t3691  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::list
	Object_t* ___list_0;
};
