﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t2488;
struct MonoIOStat_t2488_marshaled;

void MonoIOStat_t2488_marshal(const MonoIOStat_t2488& unmarshaled, MonoIOStat_t2488_marshaled& marshaled);
void MonoIOStat_t2488_marshal_back(const MonoIOStat_t2488_marshaled& marshaled, MonoIOStat_t2488& unmarshaled);
void MonoIOStat_t2488_marshal_cleanup(MonoIOStat_t2488_marshaled& marshaled);
