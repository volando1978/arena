﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>
struct Action_2_t541;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Byte>
#include "System_Core_System_Action_2_gen_20MethodDeclarations.h"
#define Action_2__ctor_m3759(__this, ___object, ___method, method) (( void (*) (Action_2_t541 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m19626_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>::Invoke(T1,T2)
#define Action_2_Invoke_m19627(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t541 *, Invitation_t341 *, bool, MethodInfo*))Action_2_Invoke_m19628_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m19629(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t541 *, Invitation_t341 *, bool, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m19630_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m19631(__this, ___result, method) (( void (*) (Action_2_t541 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m19632_gshared)(__this, ___result, method)
