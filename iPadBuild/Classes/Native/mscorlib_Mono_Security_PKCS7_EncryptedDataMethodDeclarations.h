﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t2416;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t2415;
// System.Byte[]
struct ByteU5BU5D_t350;
// Mono.Security.ASN1
struct ASN1_t2403;

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
extern "C" void EncryptedData__ctor_m11173 (EncryptedData_t2416 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
extern "C" void EncryptedData__ctor_m11174 (EncryptedData_t2416 * __this, ASN1_t2403 * ___asn1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
extern "C" ContentInfo_t2415 * EncryptedData_get_EncryptionAlgorithm_m11175 (EncryptedData_t2416 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
extern "C" ByteU5BU5D_t350* EncryptedData_get_EncryptedContent_m11176 (EncryptedData_t2416 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
