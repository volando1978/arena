﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct U24ArrayTypeU2448_t2301;
struct U24ArrayTypeU2448_t2301_marshaled;

void U24ArrayTypeU2448_t2301_marshal(const U24ArrayTypeU2448_t2301& unmarshaled, U24ArrayTypeU2448_t2301_marshaled& marshaled);
void U24ArrayTypeU2448_t2301_marshal_back(const U24ArrayTypeU2448_t2301_marshaled& marshaled, U24ArrayTypeU2448_t2301& unmarshaled);
void U24ArrayTypeU2448_t2301_marshal_cleanup(U24ArrayTypeU2448_t2301_marshaled& marshaled);
