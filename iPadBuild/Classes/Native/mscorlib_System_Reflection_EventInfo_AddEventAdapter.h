﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t211;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.EventInfo/AddEventAdapter
struct  AddEventAdapter_t2533  : public MulticastDelegate_t22
{
};
