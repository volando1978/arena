﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.LifetimeVG
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_LifetimeVG.h"
// Soomla.Store.UpgradeVG
struct  UpgradeVG_t133  : public LifetimeVG_t130
{
	// System.String Soomla.Store.UpgradeVG::GoodItemId
	String_t* ___GoodItemId_9;
	// System.String Soomla.Store.UpgradeVG::NextItemId
	String_t* ___NextItemId_10;
	// System.String Soomla.Store.UpgradeVG::PrevItemId
	String_t* ___PrevItemId_11;
};
struct UpgradeVG_t133_StaticFields{
	// System.String Soomla.Store.UpgradeVG::TAG
	String_t* ___TAG_8;
};
