﻿#pragma once
#include <stdint.h>
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.AsyncOperation
struct  AsyncOperation_t1486  : public YieldInstruction_t1493
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	IntPtr_t ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.AsyncOperation
struct AsyncOperation_t1486_marshaled
{
	IntPtr_t ___m_Ptr_0;
};
