﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreInventory/LocalUpgrade
struct LocalUpgrade_t101;

// System.Void Soomla.Store.StoreInventory/LocalUpgrade::.ctor()
extern "C" void LocalUpgrade__ctor_m484 (LocalUpgrade_t101 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
