﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IList_1_t3749;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct  ReadOnlyCollection_1_t3750  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::list
	Object_t* ___list_0;
};
