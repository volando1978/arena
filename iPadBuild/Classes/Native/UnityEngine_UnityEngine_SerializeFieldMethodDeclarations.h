﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t246;

// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m1073 (SerializeField_t246 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
