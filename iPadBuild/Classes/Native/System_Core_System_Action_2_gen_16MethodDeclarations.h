﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>
struct Action_2_t652;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Byte>
#include "System_Core_System_Action_2_gen_20MethodDeclarations.h"
#define Action_2__ctor_m3881(__this, ___object, ___method, method) (( void (*) (Action_2_t652 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m19626_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::Invoke(T1,T2)
#define Action_2_Invoke_m20339(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t652 *, TurnBasedMatch_t353 *, bool, MethodInfo*))Action_2_Invoke_m19628_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m20340(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t652 *, TurnBasedMatch_t353 *, bool, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m19630_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m20341(__this, ___result, method) (( void (*) (Action_2_t652 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m19632_gshared)(__this, ___result, method)
