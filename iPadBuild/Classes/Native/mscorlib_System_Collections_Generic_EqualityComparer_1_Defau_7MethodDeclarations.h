﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t3895;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m21935_gshared (DefaultComparer_t3895 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21935(__this, method) (( void (*) (DefaultComparer_t3895 *, MethodInfo*))DefaultComparer__ctor_m21935_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m21936_gshared (DefaultComparer_t3895 * __this, RaycastResult_t1187  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m21936(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3895 *, RaycastResult_t1187 , MethodInfo*))DefaultComparer_GetHashCode_m21936_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m21937_gshared (DefaultComparer_t3895 * __this, RaycastResult_t1187  ___x, RaycastResult_t1187  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m21937(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3895 *, RaycastResult_t1187 , RaycastResult_t1187 , MethodInfo*))DefaultComparer_Equals_m21937_gshared)(__this, ___x, ___y, method)
