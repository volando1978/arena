﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t3556;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_3__ctor_m17301_gshared (Action_3_t3556 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_3__ctor_m17301(__this, ___object, ___method, method) (( void (*) (Action_3_t3556 *, Object_t *, IntPtr_t, MethodInfo*))Action_3__ctor_m17301_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C" void Action_3_Invoke_m17303_gshared (Action_3_t3556 * __this, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, MethodInfo* method);
#define Action_3_Invoke_m17303(__this, ___arg1, ___arg2, ___arg3, method) (( void (*) (Action_3_t3556 *, Object_t *, Object_t *, Object_t *, MethodInfo*))Action_3_Invoke_m17303_gshared)(__this, ___arg1, ___arg2, ___arg3, method)
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_3_BeginInvoke_m17305_gshared (Action_3_t3556 * __this, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_3_BeginInvoke_m17305(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method) (( Object_t * (*) (Action_3_t3556 *, Object_t *, Object_t *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_3_BeginInvoke_m17305_gshared)(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method)
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void Action_3_EndInvoke_m17307_gshared (Action_3_t3556 * __this, Object_t * ___result, MethodInfo* method);
#define Action_3_EndInvoke_m17307(__this, ___result, method) (( void (*) (Action_3_t3556 *, Object_t *, MethodInfo*))Action_3_EndInvoke_m17307_gshared)(__this, ___result, method)
