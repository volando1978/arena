﻿#pragma once
#include <stdint.h>
// Soomla.Store.EventHandler
struct EventHandler_t76;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct List_1_t806;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// globales
struct  globales_t807  : public MonoBehaviour_t26
{
	// System.Int32 globales::frameRate
	int32_t ___frameRate_2;
};
struct globales_t807_StaticFields{
	// System.Boolean globales::goldenVersion
	bool ___goldenVersion_3;
	// System.Int32 globales::kills
	int32_t ___kills_4;
	// System.Int32 globales::maxKills1
	int32_t ___maxKills1_5;
	// System.Int32 globales::lastKills
	int32_t ___lastKills_6;
	// System.Int32 globales::hview
	int32_t ___hview_7;
	// System.Int32 globales::wview
	int32_t ___wview_8;
	// System.Int32 globales::numberOfGames
	int32_t ___numberOfGames_9;
	// System.Int32 globales::milisecsEnemyDestroyed
	int32_t ___milisecsEnemyDestroyed_10;
	// System.Int32 globales::dustLevel
	int32_t ___dustLevel_11;
	// System.Int32 globales::currentMap
	int32_t ___currentMap_12;
	// System.Int32 globales::totalNumberMaps
	int32_t ___totalNumberMaps_13;
	// System.Int32 globales::currentStage
	int32_t ___currentStage_14;
	// System.Int32 globales::level
	int32_t ___level_15;
	// System.Single globales::camWidth
	float ___camWidth_16;
	// System.Single globales::camHeight
	float ___camHeight_17;
	// System.Single globales::minX
	float ___minX_18;
	// System.Single globales::maxX
	float ___maxX_19;
	// System.Single globales::minY
	float ___minY_20;
	// System.Single globales::maxY
	float ___maxY_21;
	// System.Single globales::WIDTH
	float ___WIDTH_22;
	// System.Single globales::HEIGHT
	float ___HEIGHT_23;
	// System.Single globales::SCREENH
	float ___SCREENH_24;
	// System.Single globales::SCREENW
	float ___SCREENW_25;
	// System.Boolean globales::isLandscape
	bool ___isLandscape_26;
	// System.Boolean globales::ISWIDE
	bool ___ISWIDE_27;
	// System.Boolean globales::showAdApple
	bool ___showAdApple_28;
	// System.Boolean globales::showAdmob
	bool ___showAdmob_29;
	// System.Boolean globales::loadedAdmob
	bool ___loadedAdmob_30;
	// System.Boolean globales::shaking
	bool ___shaking_31;
	// System.Boolean globales::showNewRecord
	bool ___showNewRecord_32;
	// System.Boolean globales::showNewLevel
	bool ___showNewLevel_33;
	// System.Boolean globales::tutorial
	bool ___tutorial_34;
	// System.Boolean globales::tutorialEnemiesReady
	bool ___tutorialEnemiesReady_35;
	// System.Boolean globales::failedTutorial
	bool ___failedTutorial_36;
	// System.Boolean globales::sfxSwitch
	bool ___sfxSwitch_37;
	// System.Boolean globales::musicSwitch
	bool ___musicSwitch_38;
	// System.Boolean globales::tutorialIsFinished
	bool ___tutorialIsFinished_39;
	// System.Boolean globales::showUnity
	bool ___showUnity_40;
	// Soomla.Store.EventHandler globales::handler
	EventHandler_t76 * ___handler_41;
	// UnityEngine.Vector2 globales::SCREENVECTOR
	Vector2_t739  ___SCREENVECTOR_42;
	// UnityEngine.Vector2 globales::SCREENSCALE
	Vector2_t739  ___SCREENSCALE_43;
	// System.String globales::OLEADA
	String_t* ___OLEADA_44;
	// System.Collections.Generic.List`1<UnityEngine.Matrix4x4> globales::stack
	List_1_t806 * ___stack_45;
};
