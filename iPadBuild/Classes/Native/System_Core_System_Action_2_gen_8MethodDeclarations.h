﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Action_2_t881;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Object>
#include "System_Core_System_Action_2_gen_17MethodDeclarations.h"
#define Action_2__ctor_m3838(__this, ___object, ___method, method) (( void (*) (Action_2_t881 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m16118_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Invoke(T1,T2)
#define Action_2_Invoke_m19905(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t881 *, NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 *, MethodInfo*))Action_2_Invoke_m16120_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m19906(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t881 *, NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m16122_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m19907(__this, ___result, method) (( void (*) (Action_2_t881 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m16124_gshared)(__this, ___result, method)
