﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>
struct List_1_t114;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>
struct  Enumerator_t216 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::l
	List_1_t114 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::current
	VirtualCurrency_t77 * ___current_3;
};
