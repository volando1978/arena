﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t710;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::.ctor(System.IntPtr)
extern "C" void TurnBasedMatchConfig__ctor_m3103 (TurnBasedMatchConfig_t710 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdAtIndex(System.UIntPtr)
extern "C" String_t* TurnBasedMatchConfig_PlayerIdAtIndex_m3104 (TurnBasedMatchConfig_t710 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::PlayerIdsToInvite()
extern "C" Object_t* TurnBasedMatchConfig_PlayerIdsToInvite_m3105 (TurnBasedMatchConfig_t710 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::Variant()
extern "C" uint32_t TurnBasedMatchConfig_Variant_m3106 (TurnBasedMatchConfig_t710 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::ExclusiveBitMask()
extern "C" int64_t TurnBasedMatchConfig_ExclusiveBitMask_m3107 (TurnBasedMatchConfig_t710 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MinimumAutomatchingPlayers()
extern "C" uint32_t TurnBasedMatchConfig_MinimumAutomatchingPlayers_m3108 (TurnBasedMatchConfig_t710 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::MaximumAutomatchingPlayers()
extern "C" uint32_t TurnBasedMatchConfig_MaximumAutomatchingPlayers_m3109 (TurnBasedMatchConfig_t710 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchConfig_CallDispose_m3110 (TurnBasedMatchConfig_t710 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
