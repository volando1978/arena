﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
struct Enumerator_t3957;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t1263;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t1382;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m22772(__this, ___l, method) (( void (*) (Enumerator_t3957 *, List_1_t1382 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22773(__this, method) (( Object_t * (*) (Enumerator_t3957 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::Dispose()
#define Enumerator_Dispose_m22774(__this, method) (( void (*) (Enumerator_t3957 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::VerifyState()
#define Enumerator_VerifyState_m22775(__this, method) (( void (*) (Enumerator_t3957 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::MoveNext()
#define Enumerator_MoveNext_m22776(__this, method) (( bool (*) (Enumerator_t3957 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::get_Current()
#define Enumerator_get_Current_m22777(__this, method) (( Text_t1263 * (*) (Enumerator_t3957 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
