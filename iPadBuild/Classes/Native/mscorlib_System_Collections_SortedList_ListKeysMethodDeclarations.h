﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.SortedList/ListKeys
struct ListKeys_t2446;
// System.Object
struct Object_t;
// System.Collections.SortedList
struct SortedList_t2143;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void System.Collections.SortedList/ListKeys::.ctor(System.Collections.SortedList)
extern "C" void ListKeys__ctor_m11428 (ListKeys_t2446 * __this, SortedList_t2143 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::get_Count()
extern "C" int32_t ListKeys_get_Count_m11429 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsSynchronized()
extern "C" bool ListKeys_get_IsSynchronized_m11430 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_SyncRoot()
extern "C" Object_t * ListKeys_get_SyncRoot_m11431 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m11432 (ListKeys_t2446 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsFixedSize()
extern "C" bool ListKeys_get_IsFixedSize_m11433 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m11434 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/ListKeys::get_Item(System.Int32)
extern "C" Object_t * ListKeys_get_Item_m11435 (ListKeys_t2446 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::set_Item(System.Int32,System.Object)
extern "C" void ListKeys_set_Item_m11436 (ListKeys_t2446 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::Add(System.Object)
extern "C" int32_t ListKeys_Add_m11437 (ListKeys_t2446 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Clear()
extern "C" void ListKeys_Clear_m11438 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/ListKeys::Contains(System.Object)
extern "C" bool ListKeys_Contains_m11439 (ListKeys_t2446 * __this, Object_t * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/ListKeys::IndexOf(System.Object)
extern "C" int32_t ListKeys_IndexOf_m11440 (ListKeys_t2446 * __this, Object_t * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Insert(System.Int32,System.Object)
extern "C" void ListKeys_Insert_m11441 (ListKeys_t2446 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::Remove(System.Object)
extern "C" void ListKeys_Remove_m11442 (ListKeys_t2446 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/ListKeys::RemoveAt(System.Int32)
extern "C" void ListKeys_RemoveAt_m11443 (ListKeys_t2446 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.SortedList/ListKeys::GetEnumerator()
extern "C" Object_t * ListKeys_GetEnumerator_m11444 (ListKeys_t2446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
