﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t1616;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t1626;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern "C" AchievementDescription_t1626 * GcAchievementDescriptionData_ToAchievementDescription_m7314 (GcAchievementDescriptionData_t1616 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
