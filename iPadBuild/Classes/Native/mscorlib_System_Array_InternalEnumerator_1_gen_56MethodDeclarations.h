﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>
struct InternalEnumerator_1_t4172;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25617_gshared (InternalEnumerator_1_t4172 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25617(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4172 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25617_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25618_gshared (InternalEnumerator_1_t4172 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25618(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4172 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25618_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25619_gshared (InternalEnumerator_1_t4172 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25619(__this, method) (( void (*) (InternalEnumerator_1_t4172 *, MethodInfo*))InternalEnumerator_1_Dispose_m25619_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25620_gshared (InternalEnumerator_1_t4172 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25620(__this, method) (( bool (*) (InternalEnumerator_1_t4172 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25620_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>::get_Current()
extern "C" KeyValuePair_2_t4171  InternalEnumerator_1_get_Current_m25621_gshared (InternalEnumerator_1_t4172 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25621(__this, method) (( KeyValuePair_2_t4171  (*) (InternalEnumerator_1_t4172 *, MethodInfo*))InternalEnumerator_1_get_Current_m25621_gshared)(__this, method)
