﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// Soomla.Reward
struct Reward_t55;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Reward>
struct  Enumerator_t206 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::l
	List_1_t56 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Reward>::current
	Reward_t55 * ___current_3;
};
