﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t3448;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m15757_gshared (Action_1_t3448 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m15757(__this, ___object, ___method, method) (( void (*) (Action_1_t3448 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15757_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C" void Action_1_Invoke_m15758_gshared (Action_1_t3448 * __this, KeyValuePair_2_t3407  ___obj, MethodInfo* method);
#define Action_1_Invoke_m15758(__this, ___obj, method) (( void (*) (Action_1_t3448 *, KeyValuePair_2_t3407 , MethodInfo*))Action_1_Invoke_m15758_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m15759_gshared (Action_1_t3448 * __this, KeyValuePair_2_t3407  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m15759(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3448 *, KeyValuePair_2_t3407 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15759_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m15760_gshared (Action_1_t3448 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m15760(__this, ___result, method) (( void (*) (Action_1_t3448 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15760_gshared)(__this, ___result, method)
