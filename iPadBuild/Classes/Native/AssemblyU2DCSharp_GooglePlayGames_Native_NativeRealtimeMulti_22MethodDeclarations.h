﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey30
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey30::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey30__ctor_m2457 (U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
