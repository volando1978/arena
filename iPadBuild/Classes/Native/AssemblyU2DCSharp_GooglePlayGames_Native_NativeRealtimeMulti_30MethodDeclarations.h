﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38
struct U3CAcceptInvitationU3Ec__AnonStorey38_t607;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36/<AcceptInvitation>c__AnonStorey38::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey38__ctor_m2470 (U3CAcceptInvitationU3Ec__AnonStorey38_t607 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
