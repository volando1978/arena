﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct  KeyValuePair_2_t3677 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::value
	int32_t ___value_1;
};
