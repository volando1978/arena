﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Screen
struct Screen_t1506;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Int32 UnityEngine.Screen::get_width()
extern "C" int32_t Screen_get_width_m4246 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C" int32_t Screen_get_height_m4088 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Screen::get_dpi()
extern "C" float Screen_get_dpi_m6133 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C" int32_t Screen_get_orientation_m4195 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
