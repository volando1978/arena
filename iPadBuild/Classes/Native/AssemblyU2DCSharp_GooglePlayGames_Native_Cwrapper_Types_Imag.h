﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
struct  ImageResolution_t505 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/ImageResolution::value__
	int32_t ___value___1;
};
