﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Reward
struct Reward_t55;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;

// System.Void Soomla.Reward::.ctor(System.String,System.String)
extern "C" void Reward__ctor_m234 (Reward_t55 * __this, String_t* ___id, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Reward::.ctor(JSONObject)
extern "C" void Reward__ctor_m235 (Reward_t55 * __this, JSONObject_t30 * ___jsonReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Reward::.cctor()
extern "C" void Reward__cctor_m236 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Reward::get_Owned()
extern "C" bool Reward_get_Owned_m237 (Reward_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Reward::toJSONObject()
extern "C" JSONObject_t30 * Reward_toJSONObject_m238 (Reward_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Reward Soomla.Reward::fromJSONObject(JSONObject)
extern "C" Reward_t55 * Reward_fromJSONObject_m239 (Object_t * __this /* static, unused */, JSONObject_t30 * ___rewardObj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Reward::Take()
extern "C" bool Reward_Take_m240 (Reward_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Reward::CanGive()
extern "C" bool Reward_CanGive_m241 (Reward_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Reward::Give()
extern "C" bool Reward_Give_m242 (Reward_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Reward::giveInner()
// System.Boolean Soomla.Reward::takeInner()
// Soomla.Reward Soomla.Reward::GetReward(System.String)
extern "C" Reward_t55 * Reward_GetReward_m243 (Object_t * __this /* static, unused */, String_t* ___rewardID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Soomla.Reward> Soomla.Reward::GetRewards()
extern "C" List_1_t56 * Reward_GetRewards_m244 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
