﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21
struct U3CShowAchievementsUIU3Ec__AnonStorey21_t531;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21::.ctor()
extern "C" void U3CShowAchievementsUIU3Ec__AnonStorey21__ctor_m2249 (U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21::<>m__19(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" void U3CShowAchievementsUIU3Ec__AnonStorey21_U3CU3Em__19_m2250 (U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * __this, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
