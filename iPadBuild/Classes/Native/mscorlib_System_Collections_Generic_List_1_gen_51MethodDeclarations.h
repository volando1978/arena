﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct List_1_t3413;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t4260;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4261;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t4262;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t3431;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>[]
struct UnityKeyValuePair_2U5BU5D_t3411;
// System.Predicate`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t3432;
// System.Action`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct Action_1_t3433;
// System.Comparison`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct Comparison_1_t3435;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m15319(__this, method) (( void (*) (List_1_t3413 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15320(__this, ___collection, method) (( void (*) (List_1_t3413 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
#define List_1__ctor_m15322(__this, ___capacity, method) (( void (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.cctor()
#define List_1__cctor_m15324(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15326(__this, method) (( Object_t* (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15328(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3413 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15330(__this, method) (( Object_t * (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15332(__this, ___item, method) (( int32_t (*) (List_1_t3413 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15334(__this, ___item, method) (( bool (*) (List_1_t3413 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15336(__this, ___item, method) (( int32_t (*) (List_1_t3413 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15338(__this, ___index, ___item, method) (( void (*) (List_1_t3413 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15340(__this, ___item, method) (( void (*) (List_1_t3413 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15342(__this, method) (( bool (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15344(__this, method) (( bool (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15346(__this, method) (( Object_t * (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15348(__this, method) (( bool (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15350(__this, method) (( bool (*) (List_1_t3413 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15352(__this, ___index, method) (( Object_t * (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15354(__this, ___index, ___value, method) (( void (*) (List_1_t3413 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Add(T)
#define List_1_Add_m15356(__this, ___item, method) (( void (*) (List_1_t3413 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15358(__this, ___newCount, method) (( void (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15360(__this, ___collection, method) (( void (*) (List_1_t3413 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15362(__this, ___enumerable, method) (( void (*) (List_1_t3413 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15364(__this, ___collection, method) (( void (*) (List_1_t3413 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::AsReadOnly()
#define List_1_AsReadOnly_m15366(__this, method) (( ReadOnlyCollection_1_t3431 * (*) (List_1_t3413 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Clear()
#define List_1_Clear_m15368(__this, method) (( void (*) (List_1_t3413 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Contains(T)
#define List_1_Contains_m15370(__this, ___item, method) (( bool (*) (List_1_t3413 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15372(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3413 *, UnityKeyValuePair_2U5BU5D_t3411*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Find(System.Predicate`1<T>)
#define List_1_Find_m15374(__this, ___match, method) (( UnityKeyValuePair_2_t3412 * (*) (List_1_t3413 *, Predicate_1_t3432 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15376(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3432 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m15378(__this, ___match, method) (( int32_t (*) (List_1_t3413 *, Predicate_1_t3432 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15380(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3413 *, int32_t, int32_t, Predicate_1_t3432 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m15382(__this, ___action, method) (( void (*) (List_1_t3413 *, Action_1_t3433 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
#define List_1_GetEnumerator_m15384(__this, method) (( Enumerator_t3434  (*) (List_1_t3413 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
#define List_1_IndexOf_m15386(__this, ___item, method) (( int32_t (*) (List_1_t3413 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15388(__this, ___start, ___delta, method) (( void (*) (List_1_t3413 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15390(__this, ___index, method) (( void (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
#define List_1_Insert_m15392(__this, ___index, ___item, method) (( void (*) (List_1_t3413 *, int32_t, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15394(__this, ___collection, method) (( void (*) (List_1_t3413 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Remove(T)
#define List_1_Remove_m15396(__this, ___item, method) (( bool (*) (List_1_t3413 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15398(__this, ___match, method) (( int32_t (*) (List_1_t3413 *, Predicate_1_t3432 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15400(__this, ___index, method) (( void (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Reverse()
#define List_1_Reverse_m15402(__this, method) (( void (*) (List_1_t3413 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Sort()
#define List_1_Sort_m15404(__this, method) (( void (*) (List_1_t3413 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15406(__this, ___comparison, method) (( void (*) (List_1_t3413 *, Comparison_1_t3435 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::ToArray()
#define List_1_ToArray_m15408(__this, method) (( UnityKeyValuePair_2U5BU5D_t3411* (*) (List_1_t3413 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::TrimExcess()
#define List_1_TrimExcess_m15410(__this, method) (( void (*) (List_1_t3413 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Capacity()
#define List_1_get_Capacity_m15412(__this, method) (( int32_t (*) (List_1_t3413 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15414(__this, ___value, method) (( void (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Count()
#define List_1_get_Count_m15416(__this, method) (( int32_t (*) (List_1_t3413 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
#define List_1_get_Item_m15418(__this, ___index, method) (( UnityKeyValuePair_2_t3412 * (*) (List_1_t3413 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
#define List_1_set_Item_m15420(__this, ___index, ___value, method) (( void (*) (List_1_t3413 *, int32_t, UnityKeyValuePair_2_t3412 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
