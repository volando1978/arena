﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
struct FetchListResponse_t689;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeQuest>
struct IEnumerable_1_t876;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::.ctor(System.IntPtr)
extern "C" void FetchListResponse__ctor_m2895 (FetchListResponse_t689 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::ResponseStatus()
extern "C" int32_t FetchListResponse_ResponseStatus_m2896 (FetchListResponse_t689 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::RequestSucceeded()
extern "C" bool FetchListResponse_RequestSucceeded_m2897 (FetchListResponse_t689 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.NativeQuest> GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::Data()
extern "C" Object_t* FetchListResponse_Data_m2898 (FetchListResponse_t689 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchListResponse_CallDispose_m2899 (FetchListResponse_t689 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::FromPointer(System.IntPtr)
extern "C" FetchListResponse_t689 * FetchListResponse_FromPointer_m2900 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse::<Data>m__99(System.UIntPtr)
extern "C" NativeQuest_t677 * FetchListResponse_U3CDataU3Em__99_m2901 (FetchListResponse_t689 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
