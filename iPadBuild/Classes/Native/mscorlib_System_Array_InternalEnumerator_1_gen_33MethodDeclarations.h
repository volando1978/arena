﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>
struct InternalEnumerator_1_t3941;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22540_gshared (InternalEnumerator_1_t3941 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m22540(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3941 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m22540_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22541_gshared (InternalEnumerator_1_t3941 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22541(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3941 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22541_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22542_gshared (InternalEnumerator_1_t3941 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22542(__this, method) (( void (*) (InternalEnumerator_1_t3941 *, MethodInfo*))InternalEnumerator_1_Dispose_m22542_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22543_gshared (InternalEnumerator_1_t3941 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22543(__this, method) (( bool (*) (InternalEnumerator_1_t3941 *, MethodInfo*))InternalEnumerator_1_MoveNext_m22543_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C" RaycastHit_t990  InternalEnumerator_1_get_Current_m22544_gshared (InternalEnumerator_1_t3941 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22544(__this, method) (( RaycastHit_t990  (*) (InternalEnumerator_1_t3941 *, MethodInfo*))InternalEnumerator_1_get_Current_m22544_gshared)(__this, method)
