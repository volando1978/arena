﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54
struct U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::.ctor()
extern "C" void U3CFindEqualVersionMatchU3Ec__AnonStorey54__ctor_m2557 (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::<>m__5E(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CFindEqualVersionMatchU3Ec__AnonStorey54_U3CU3Em__5E_m2558 (U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
