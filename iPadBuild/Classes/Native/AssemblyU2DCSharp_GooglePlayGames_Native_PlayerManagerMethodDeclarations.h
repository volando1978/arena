﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PlayerManager
struct PlayerManager_t685;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
struct Action_1_t875;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PlayerManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void PlayerManager__ctor_m2875 (PlayerManager_t685 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PlayerManager::FetchSelf(System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>)
extern "C" void PlayerManager_FetchSelf_m2876 (PlayerManager_t685 * __this, Action_1_t875 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PlayerManager::InternalFetchSelfCallback(System.IntPtr,System.IntPtr)
extern "C" void PlayerManager_InternalFetchSelfCallback_m2877 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
