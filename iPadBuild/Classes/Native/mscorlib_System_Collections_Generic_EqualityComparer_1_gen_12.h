﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t2912;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct  EqualityComparer_1_t2912  : public Object_t
{
};
struct EqualityComparer_1_t2912_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::_default
	EqualityComparer_1_t2912 * ____default_0;
};
