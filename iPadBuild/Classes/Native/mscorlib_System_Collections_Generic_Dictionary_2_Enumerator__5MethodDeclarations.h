﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
struct Enumerator_t3535;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3530;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17124_gshared (Enumerator_t3535 * __this, Dictionary_2_t3530 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m17124(__this, ___dictionary, method) (( void (*) (Enumerator_t3535 *, Dictionary_2_t3530 *, MethodInfo*))Enumerator__ctor_m17124_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17125(__this, method) (( Object_t * (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127(__this, method) (( Object_t * (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128(__this, method) (( Object_t * (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17129_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m17129(__this, method) (( bool (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_MoveNext_m17129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t3531  Enumerator_get_Current_m17130_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_get_Current_m17130(__this, method) (( KeyValuePair_2_t3531  (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_get_Current_m17130_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17131_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m17131(__this, method) (( Object_t * (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_get_CurrentKey_m17131_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m17132_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m17132(__this, method) (( int32_t (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_get_CurrentValue_m17132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m17133_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m17133(__this, method) (( void (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_VerifyState_m17133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17134_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m17134(__this, method) (( void (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_VerifyCurrent_m17134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m17135_gshared (Enumerator_t3535 * __this, MethodInfo* method);
#define Enumerator_Dispose_m17135(__this, method) (( void (*) (Enumerator_t3535 *, MethodInfo*))Enumerator_Dispose_m17135_gshared)(__this, method)
