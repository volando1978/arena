﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct List_1_t918;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>
struct  Enumerator_t3687 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::l
	List_1_t918 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>::current
	Object_t * ___current_3;
};
