﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
struct State_t565;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::.ctor()
extern "C" void State__ctor_m2373 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void State_HandleRoomResponse_m2374 (State_t565 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsActive()
extern "C" bool State_IsActive_m2375 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::LeaveRoom()
extern "C" void State_LeaveRoom_m2376 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::ShowWaitingRoomUI(System.UInt32)
extern "C" void State_ShowWaitingRoomUI_m2377 (State_t565 * __this, uint32_t ___minimumParticipantsBeforeStarting, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnStateEntered()
extern "C" void State_OnStateEntered_m2378 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void State_OnRoomStatusChanged_m2379 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void State_OnConnectedSetChanged_m2380 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void State_OnParticipantStatusChanged_m2381 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void State_OnDataReceived_m2382 (State_t565 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C" void State_SendToSpecificRecipient_m2383 (State_t565 * __this, String_t* ___recipientId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToAll(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C" void State_SendToAll_m2384 (State_t565 * __this, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetConnectedParticipants()
extern "C" List_1_t351 * State_GetConnectedParticipants_m2385 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetSelf()
extern "C" Participant_t340 * State_GetSelf_m2386 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetParticipant(System.String)
extern "C" Participant_t340 * State_GetParticipant_m2387 (State_t565 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsRoomConnected()
extern "C" bool State_IsRoomConnected_m2388 (State_t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
