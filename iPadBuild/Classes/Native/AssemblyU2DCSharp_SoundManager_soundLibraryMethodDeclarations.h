﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SoundManager/soundLibrary
struct soundLibrary_t754;

// System.Void SoundManager/soundLibrary::.ctor()
extern "C" void soundLibrary__ctor_m3253 (soundLibrary_t754 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
