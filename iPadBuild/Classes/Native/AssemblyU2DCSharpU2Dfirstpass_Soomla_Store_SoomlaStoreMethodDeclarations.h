﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.SoomlaStore
struct SoomlaStore_t63;
// Soomla.Store.IStoreAssets
struct IStoreAssets_t176;
// System.String
struct String_t;

// System.Void Soomla.Store.SoomlaStore::.ctor()
extern "C" void SoomlaStore__ctor_m374 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::.cctor()
extern "C" void SoomlaStore__cctor_m375 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.SoomlaStore Soomla.Store.SoomlaStore::get_instance()
extern "C" SoomlaStore_t63 * SoomlaStore_get_instance_m376 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SoomlaStore::Initialize(Soomla.Store.IStoreAssets)
extern "C" bool SoomlaStore_Initialize_m377 (Object_t * __this /* static, unused */, Object_t * ___storeAssets, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::BuyMarketItem(System.String,System.String)
extern "C" void SoomlaStore_BuyMarketItem_m378 (Object_t * __this /* static, unused */, String_t* ___productId, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::RefreshInventory()
extern "C" void SoomlaStore_RefreshInventory_m379 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::RefreshMarketItemsDetails()
extern "C" void SoomlaStore_RefreshMarketItemsDetails_m380 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::RestoreTransactions()
extern "C" void SoomlaStore_RestoreTransactions_m381 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SoomlaStore::TransactionsAlreadyRestored()
extern "C" bool SoomlaStore_TransactionsAlreadyRestored_m382 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::StartIabServiceInBg()
extern "C" void SoomlaStore_StartIabServiceInBg_m383 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::StopIabServiceInBg()
extern "C" void SoomlaStore_StopIabServiceInBg_m384 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_loadBillingService()
extern "C" void SoomlaStore__loadBillingService_m385 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_buyMarketItem(System.String,System.String)
extern "C" void SoomlaStore__buyMarketItem_m386 (SoomlaStore_t63 * __this, String_t* ___productId, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_refreshInventory()
extern "C" void SoomlaStore__refreshInventory_m387 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_restoreTransactions()
extern "C" void SoomlaStore__restoreTransactions_m388 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_refreshMarketItemsDetails()
extern "C" void SoomlaStore__refreshMarketItemsDetails_m389 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SoomlaStore::_transactionsAlreadyRestored()
extern "C" bool SoomlaStore__transactionsAlreadyRestored_m390 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_startIabServiceInBg()
extern "C" void SoomlaStore__startIabServiceInBg_m391 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::_stopIabServiceInBg()
extern "C" void SoomlaStore__stopIabServiceInBg_m392 (SoomlaStore_t63 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SoomlaStore::get_Initialized()
extern "C" bool SoomlaStore_get_Initialized_m393 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStore::set_Initialized(System.Boolean)
extern "C" void SoomlaStore_set_Initialized_m394 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
