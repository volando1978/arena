﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.KeyValueStorageAndroid
struct KeyValueStorageAndroid_t1;

// System.Void Soomla.KeyValueStorageAndroid::.ctor()
extern "C" void KeyValueStorageAndroid__ctor_m0 (KeyValueStorageAndroid_t1 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
