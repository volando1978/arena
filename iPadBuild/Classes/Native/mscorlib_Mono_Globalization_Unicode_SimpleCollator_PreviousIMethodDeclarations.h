﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
struct PreviousInfo_t2372;
struct PreviousInfo_t2372_marshaled;

// System.Void Mono.Globalization.Unicode.SimpleCollator/PreviousInfo::.ctor(System.Boolean)
extern "C" void PreviousInfo__ctor_m10775 (PreviousInfo_t2372 * __this, bool ___dummy, MethodInfo* method) IL2CPP_METHOD_ATTR;
void PreviousInfo_t2372_marshal(const PreviousInfo_t2372& unmarshaled, PreviousInfo_t2372_marshaled& marshaled);
void PreviousInfo_t2372_marshal_back(const PreviousInfo_t2372_marshaled& marshaled, PreviousInfo_t2372& unmarshaled);
void PreviousInfo_t2372_marshal_cleanup(PreviousInfo_t2372_marshaled& marshaled);
