﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PlayerManager
struct  PlayerManager_t685  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PlayerManager::mGameServices
	GameServices_t534 * ___mGameServices_0;
};
