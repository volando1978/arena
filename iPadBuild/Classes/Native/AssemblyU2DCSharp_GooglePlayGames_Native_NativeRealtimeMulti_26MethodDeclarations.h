﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34
struct U3CAcceptFromInboxU3Ec__AnonStorey34_t603;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey34__ctor_m2463 (U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
