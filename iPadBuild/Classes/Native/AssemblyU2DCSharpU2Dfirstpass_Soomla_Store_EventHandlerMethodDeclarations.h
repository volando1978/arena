﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.EventHandler
struct EventHandler_t76;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;

// System.Void Soomla.Store.EventHandler::.ctor()
extern "C" void EventHandler__ctor_m279 (EventHandler_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onMarketPurchase(Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void EventHandler_onMarketPurchase_m280 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, String_t* ___payload, Dictionary_2_t165 * ___extra, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onMarketRefund(Soomla.Store.PurchasableVirtualItem)
extern "C" void EventHandler_onMarketRefund_m281 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onItemPurchased(Soomla.Store.PurchasableVirtualItem,System.String)
extern "C" void EventHandler_onItemPurchased_m282 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onGoodEquipped(Soomla.Store.EquippableVG)
extern "C" void EventHandler_onGoodEquipped_m283 (EventHandler_t76 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onGoodUnequipped(Soomla.Store.EquippableVG)
extern "C" void EventHandler_onGoodUnequipped_m284 (EventHandler_t76 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onGoodUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG)
extern "C" void EventHandler_onGoodUpgrade_m285 (EventHandler_t76 * __this, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___currentUpgrade, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onBillingSupported()
extern "C" void EventHandler_onBillingSupported_m286 (EventHandler_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onBillingNotSupported()
extern "C" void EventHandler_onBillingNotSupported_m287 (EventHandler_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onMarketPurchaseStarted(Soomla.Store.PurchasableVirtualItem)
extern "C" void EventHandler_onMarketPurchaseStarted_m288 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onItemPurchaseStarted(Soomla.Store.PurchasableVirtualItem)
extern "C" void EventHandler_onItemPurchaseStarted_m289 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onMarketPurchaseCancelled(Soomla.Store.PurchasableVirtualItem)
extern "C" void EventHandler_onMarketPurchaseCancelled_m290 (EventHandler_t76 * __this, PurchasableVirtualItem_t124 * ___pvi, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onUnexpectedErrorInStore(System.String)
extern "C" void EventHandler_onUnexpectedErrorInStore_m291 (EventHandler_t76 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onCurrencyBalanceChanged(Soomla.Store.VirtualCurrency,System.Int32,System.Int32)
extern "C" void EventHandler_onCurrencyBalanceChanged_m292 (EventHandler_t76 * __this, VirtualCurrency_t77 * ___virtualCurrency, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onGoodBalanceChanged(Soomla.Store.VirtualGood,System.Int32,System.Int32)
extern "C" void EventHandler_onGoodBalanceChanged_m293 (EventHandler_t76 * __this, VirtualGood_t79 * ___good, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onRestoreTransactionsStarted()
extern "C" void EventHandler_onRestoreTransactionsStarted_m294 (EventHandler_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onRestoreTransactionsFinished(System.Boolean)
extern "C" void EventHandler_onRestoreTransactionsFinished_m295 (EventHandler_t76 * __this, bool ___success, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.EventHandler::onSoomlaStoreInitialized()
extern "C" void EventHandler_onSoomlaStoreInitialized_m296 (EventHandler_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
