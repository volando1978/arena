﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct U3CToIntPtrU3Ec__AnonStorey5C_1_t3775;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::.ctor()
extern "C" void U3CToIntPtrU3Ec__AnonStorey5C_1__ctor_m20386_gshared (U3CToIntPtrU3Ec__AnonStorey5C_1_t3775 * __this, MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey5C_1__ctor_m20386(__this, method) (( void (*) (U3CToIntPtrU3Ec__AnonStorey5C_1_t3775 *, MethodInfo*))U3CToIntPtrU3Ec__AnonStorey5C_1__ctor_m20386_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::<>m__73(System.IntPtr)
extern "C" void U3CToIntPtrU3Ec__AnonStorey5C_1_U3CU3Em__73_m20387_gshared (U3CToIntPtrU3Ec__AnonStorey5C_1_t3775 * __this, IntPtr_t ___result, MethodInfo* method);
#define U3CToIntPtrU3Ec__AnonStorey5C_1_U3CU3Em__73_m20387(__this, ___result, method) (( void (*) (U3CToIntPtrU3Ec__AnonStorey5C_1_t3775 *, IntPtr_t, MethodInfo*))U3CToIntPtrU3Ec__AnonStorey5C_1_U3CU3Em__73_m20387_gshared)(__this, ___result, method)
