﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.Optional.ShowOptionsExtended
struct ShowOptionsExtended_t152;
// System.String
struct String_t;

// System.Void UnityEngine.Advertisements.Optional.ShowOptionsExtended::.ctor()
extern "C" void ShowOptionsExtended__ctor_m736 (ShowOptionsExtended_t152 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.Optional.ShowOptionsExtended::get_gamerSid()
extern "C" String_t* ShowOptionsExtended_get_gamerSid_m737 (ShowOptionsExtended_t152 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Optional.ShowOptionsExtended::set_gamerSid(System.String)
extern "C" void ShowOptionsExtended_set_gamerSid_m738 (ShowOptionsExtended_t152 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
