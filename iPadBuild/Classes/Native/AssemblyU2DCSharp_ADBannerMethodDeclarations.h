﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ADBanner
struct ADBanner_t718;

// System.Void ADBanner::.ctor()
extern "C" void ADBanner__ctor_m3135 (ADBanner_t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADBanner::.cctor()
extern "C" void ADBanner__cctor_m3136 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADBanner::Start()
extern "C" void ADBanner_Start_m3137 (ADBanner_t718 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADBanner::OnBannerLoaded()
extern "C" void ADBanner_OnBannerLoaded_m3138 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
