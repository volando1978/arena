﻿#pragma once
#include <stdint.h>
// System.IO.TextWriter
struct TextWriter_t2162;
// System.IO.TextReader
struct TextReader_t231;
// System.Text.Encoding
struct Encoding_t1666;
// System.Object
#include "mscorlib_System_Object.h"
// System.Console
struct  Console_t2161  : public Object_t
{
};
struct Console_t2161_StaticFields{
	// System.IO.TextWriter System.Console::stdout
	TextWriter_t2162 * ___stdout_0;
	// System.IO.TextWriter System.Console::stderr
	TextWriter_t2162 * ___stderr_1;
	// System.IO.TextReader System.Console::stdin
	TextReader_t231 * ___stdin_2;
	// System.Text.Encoding System.Console::inputEncoding
	Encoding_t1666 * ___inputEncoding_3;
	// System.Text.Encoding System.Console::outputEncoding
	Encoding_t1666 * ___outputEncoding_4;
};
