﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3774  : public Object_t
{
	// T GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>::result
	uint8_t ___result_0;
	// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<T> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Byte>::<>f__ref$94
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3773 * ___U3CU3Ef__refU2494_1;
};
