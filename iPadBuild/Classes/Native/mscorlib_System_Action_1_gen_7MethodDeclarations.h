﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
struct Action_1_t845;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Advertisin.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m19214_gshared (Action_1_t845 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m19214(__this, ___object, ___method, method) (( void (*) (Action_1_t845 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m19214_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::Invoke(T)
extern "C" void Action_1_Invoke_m19215_gshared (Action_1_t845 * __this, AdvertisingResult_t354  ___obj, MethodInfo* method);
#define Action_1_Invoke_m19215(__this, ___obj, method) (( void (*) (Action_1_t845 *, AdvertisingResult_t354 , MethodInfo*))Action_1_Invoke_m19215_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m19216_gshared (Action_1_t845 * __this, AdvertisingResult_t354  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m19216(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t845 *, AdvertisingResult_t354 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m19216_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m19217_gshared (Action_1_t845 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m19217(__this, ___result, method) (( void (*) (Action_1_t845 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m19217_gshared)(__this, ___result, method)
