﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct U24ArrayTypeU2420_t2299;
struct U24ArrayTypeU2420_t2299_marshaled;

void U24ArrayTypeU2420_t2299_marshal(const U24ArrayTypeU2420_t2299& unmarshaled, U24ArrayTypeU2420_t2299_marshaled& marshaled);
void U24ArrayTypeU2420_t2299_marshal_back(const U24ArrayTypeU2420_t2299_marshaled& marshaled, U24ArrayTypeU2420_t2299& unmarshaled);
void U24ArrayTypeU2420_t2299_marshal_cleanup(U24ArrayTypeU2420_t2299_marshaled& marshaled);
