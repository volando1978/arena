﻿#pragma once
#include <stdint.h>
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct Func_2_t938;
// System.Action`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct Action_1_t937;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct  U3CToIntPtrU3Ec__AnonStorey5C_1_t3775  : public Object_t
{
	// System.Func`2<System.IntPtr,T> GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::conversionFunction
	Func_2_t938 * ___conversionFunction_0;
	// System.Action`1<T> GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5C`1<GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::callback
	Action_1_t937 * ___callback_1;
};
