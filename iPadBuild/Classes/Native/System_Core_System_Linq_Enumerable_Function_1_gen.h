﻿#pragma once
#include <stdint.h>
// System.Func`2<System.Object,System.Object>
struct Func_2_t903;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/Function`1<System.Object>
struct  Function_1_t3747  : public Object_t
{
};
struct Function_1_t3747_StaticFields{
	// System.Func`2<T,T> System.Linq.Enumerable/Function`1<System.Object>::Identity
	Func_2_t903 * ___Identity_0;
	// System.Func`2<T,T> System.Linq.Enumerable/Function`1<System.Object>::<>f__am$cache1
	Func_2_t903 * ___U3CU3Ef__amU24cache1_1;
};
