﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>
struct KeyCollection_t3659;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m18968_gshared (KeyCollection_t3659 * __this, Dictionary_2_t3655 * ___dictionary, MethodInfo* method);
#define KeyCollection__ctor_m18968(__this, ___dictionary, method) (( void (*) (KeyCollection_t3659 *, Dictionary_2_t3655 *, MethodInfo*))KeyCollection__ctor_m18968_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969_gshared (KeyCollection_t3659 * __this, Object_t * ___item, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969(__this, ___item, method) (( void (*) (KeyCollection_t3659 *, Object_t *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970(__this, method) (( void (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971_gshared (KeyCollection_t3659 * __this, Object_t * ___item, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971(__this, ___item, method) (( bool (*) (KeyCollection_t3659 *, Object_t *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972_gshared (KeyCollection_t3659 * __this, Object_t * ___item, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972(__this, ___item, method) (( bool (*) (KeyCollection_t3659 *, Object_t *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973(__this, method) (( Object_t* (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18974_gshared (KeyCollection_t3659 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m18974(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3659 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m18974_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975(__this, method) (( Object_t * (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976(__this, method) (( bool (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977(__this, method) (( bool (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978(__this, method) (( Object_t * (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m18979_gshared (KeyCollection_t3659 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define KeyCollection_CopyTo_m18979(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3659 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))KeyCollection_CopyTo_m18979_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::GetEnumerator()
extern "C" Enumerator_t3660  KeyCollection_GetEnumerator_m18980_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_GetEnumerator_m18980(__this, method) (( Enumerator_t3660  (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_GetEnumerator_m18980_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m18981_gshared (KeyCollection_t3659 * __this, MethodInfo* method);
#define KeyCollection_get_Count_m18981(__this, method) (( int32_t (*) (KeyCollection_t3659 *, MethodInfo*))KeyCollection_get_Count_m18981_gshared)(__this, method)
