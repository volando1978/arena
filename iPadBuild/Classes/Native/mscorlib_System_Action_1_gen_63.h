﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Soomla.Store.VirtualCategory>
struct  Action_1_t3630  : public MulticastDelegate_t22
{
};
