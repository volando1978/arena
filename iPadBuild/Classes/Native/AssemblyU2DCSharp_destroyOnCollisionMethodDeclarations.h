﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// destroyOnCollision
struct destroyOnCollision_t783;
// UnityEngine.Collider
struct Collider_t900;

// System.Void destroyOnCollision::.ctor()
extern "C" void destroyOnCollision__ctor_m3368 (destroyOnCollision_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void destroyOnCollision::Start()
extern "C" void destroyOnCollision_Start_m3369 (destroyOnCollision_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void destroyOnCollision::Update()
extern "C" void destroyOnCollision_Update_m3370 (destroyOnCollision_t783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void destroyOnCollision::OnTriggerEnter(UnityEngine.Collider)
extern "C" void destroyOnCollision_OnTriggerEnter_m3371 (destroyOnCollision_t783 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
