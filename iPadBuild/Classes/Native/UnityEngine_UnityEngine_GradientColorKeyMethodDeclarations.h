﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GradientColorKey
struct GradientColorKey_t1511;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m6484 (GradientColorKey_t1511 * __this, Color_t747  ___col, float ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
