﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Status/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Mul.h"
// GooglePlayGames.Native.Cwrapper.Status/MultiplayerStatus
struct  MultiplayerStatus_t486 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Status/MultiplayerStatus::value__
	int32_t ___value___1;
};
