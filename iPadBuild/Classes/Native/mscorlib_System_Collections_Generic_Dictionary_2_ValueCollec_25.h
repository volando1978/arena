﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t344;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct  ValueCollection_t3679  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::dictionary
	Dictionary_2_t344 * ___dictionary_0;
};
