﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
struct AcceptResponse_t691;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::.ctor(System.IntPtr)
extern "C" void AcceptResponse__ctor_m2909 (AcceptResponse_t691 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::ResponseStatus()
extern "C" int32_t AcceptResponse_ResponseStatus_m2910 (AcceptResponse_t691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::AcceptedQuest()
extern "C" NativeQuest_t677 * AcceptResponse_AcceptedQuest_m2911 (AcceptResponse_t691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::RequestSucceeded()
extern "C" bool AcceptResponse_RequestSucceeded_m2912 (AcceptResponse_t691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void AcceptResponse_CallDispose_m2913 (AcceptResponse_t691 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse::FromPointer(System.IntPtr)
extern "C" AcceptResponse_t691 * AcceptResponse_FromPointer_m2914 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
