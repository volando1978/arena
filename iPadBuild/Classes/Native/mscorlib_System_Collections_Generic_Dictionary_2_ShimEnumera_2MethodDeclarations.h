﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t3800;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t3789;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20611_gshared (ShimEnumerator_t3800 * __this, Dictionary_2_t3789 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m20611(__this, ___host, method) (( void (*) (ShimEnumerator_t3800 *, Dictionary_2_t3789 *, MethodInfo*))ShimEnumerator__ctor_m20611_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20612_gshared (ShimEnumerator_t3800 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m20612(__this, method) (( bool (*) (ShimEnumerator_t3800 *, MethodInfo*))ShimEnumerator_MoveNext_m20612_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m20613_gshared (ShimEnumerator_t3800 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m20613(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t3800 *, MethodInfo*))ShimEnumerator_get_Entry_m20613_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20614_gshared (ShimEnumerator_t3800 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m20614(__this, method) (( Object_t * (*) (ShimEnumerator_t3800 *, MethodInfo*))ShimEnumerator_get_Key_m20614_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20615_gshared (ShimEnumerator_t3800 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m20615(__this, method) (( Object_t * (*) (ShimEnumerator_t3800 *, MethodInfo*))ShimEnumerator_get_Value_m20615_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20616_gshared (ShimEnumerator_t3800 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m20616(__this, method) (( Object_t * (*) (ShimEnumerator_t3800 *, MethodInfo*))ShimEnumerator_get_Current_m20616_gshared)(__this, method)
