﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>
struct  Converter_2_t3461  : public MulticastDelegate_t22
{
};
