﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.InteropServices.DispIdAttribute
struct  DispIdAttribute_t2574  : public Attribute_t1546
{
	// System.Int32 System.Runtime.InteropServices.DispIdAttribute::id
	int32_t ___id_0;
};
