﻿#pragma once
#include <stdint.h>
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.String>
struct Converter_2_t3460;
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Object>
struct Converter_2_t3461;
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct Converter_2_t3462;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UnityDictionary`2<System.String,System.Object>
struct  UnityDictionary_2_t3459  : public Object_t
{
};
struct UnityDictionary_2_t3459_StaticFields{
	// System.Converter`2<UnityEngine.UnityKeyValuePair`2<K,V>,K> UnityEngine.UnityDictionary`2<System.String,System.Object>::<>f__am$cache0
	Converter_2_t3460 * ___U3CU3Ef__amU24cache0_0;
	// System.Converter`2<UnityEngine.UnityKeyValuePair`2<K,V>,V> UnityEngine.UnityDictionary`2<System.String,System.Object>::<>f__am$cache1
	Converter_2_t3461 * ___U3CU3Ef__amU24cache1_1;
	// System.Converter`2<UnityEngine.UnityKeyValuePair`2<K,V>,System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.Object>::<>f__am$cache2
	Converter_2_t3462 * ___U3CU3Ef__amU24cache2_2;
	// System.Converter`2<UnityEngine.UnityKeyValuePair`2<K,V>,System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.Object>::<>f__am$cache3
	Converter_2_t3462 * ___U3CU3Ef__amU24cache3_3;
};
