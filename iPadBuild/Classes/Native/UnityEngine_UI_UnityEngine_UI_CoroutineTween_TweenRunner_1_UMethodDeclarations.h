﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3975;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m23050_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m23050(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m23050_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m23051_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m23051(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m23051_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m23052_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m23052(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m23052_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m23053_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m23053(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m23053_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m23054_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m23054(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m23054_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m23055_gshared (U3CStartU3Ec__Iterator0_t3975 * __this, MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m23055(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t3975 *, MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m23055_gshared)(__this, method)
