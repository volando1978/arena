﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct U24ArrayTypeU2432_t2858;
struct U24ArrayTypeU2432_t2858_marshaled;

void U24ArrayTypeU2432_t2858_marshal(const U24ArrayTypeU2432_t2858& unmarshaled, U24ArrayTypeU2432_t2858_marshaled& marshaled);
void U24ArrayTypeU2432_t2858_marshal_back(const U24ArrayTypeU2432_t2858_marshaled& marshaled, U24ArrayTypeU2432_t2858& unmarshaled);
void U24ArrayTypeU2432_t2858_marshal_cleanup(U24ArrayTypeU2432_t2858_marshaled& marshaled);
