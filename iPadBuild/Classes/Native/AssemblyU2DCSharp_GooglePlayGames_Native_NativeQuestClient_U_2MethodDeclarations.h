﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29
struct U3CAcceptU3Ec__AnonStorey29_t557;
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
struct AcceptResponse_t691;

// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29::.ctor()
extern "C" void U3CAcceptU3Ec__AnonStorey29__ctor_m2305 (U3CAcceptU3Ec__AnonStorey29_t557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29::<>m__21(GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse)
extern "C" void U3CAcceptU3Ec__AnonStorey29_U3CU3Em__21_m2306 (U3CAcceptU3Ec__AnonStorey29_t557 * __this, AcceptResponse_t691 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
