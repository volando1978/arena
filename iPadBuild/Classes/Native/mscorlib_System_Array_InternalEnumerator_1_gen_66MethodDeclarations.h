﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Delegate>
struct InternalEnumerator_1_t4201;
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t211;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Delegate>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25814(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4201 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Delegate>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25815(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4201 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Delegate>::Dispose()
#define InternalEnumerator_1_Dispose_m25816(__this, method) (( void (*) (InternalEnumerator_1_t4201 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Delegate>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25817(__this, method) (( bool (*) (InternalEnumerator_1_t4201 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Delegate>::get_Current()
#define InternalEnumerator_1_get_Current_m25818(__this, method) (( Delegate_t211 * (*) (InternalEnumerator_1_t4201 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
