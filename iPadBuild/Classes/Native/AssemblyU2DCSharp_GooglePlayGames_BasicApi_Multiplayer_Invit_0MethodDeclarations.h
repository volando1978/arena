﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.Invitation::.ctor(GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType,System.String,GooglePlayGames.BasicApi.Multiplayer.Participant,System.Int32)
extern "C" void Invitation__ctor_m1371 (Invitation_t341 * __this, int32_t ___invType, String_t* ___invId, Participant_t340 * ___inviter, int32_t ___variant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType GooglePlayGames.BasicApi.Multiplayer.Invitation::get_InvitationType()
extern "C" int32_t Invitation_get_InvitationType_m1372 (Invitation_t341 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Invitation::get_InvitationId()
extern "C" String_t* Invitation_get_InvitationId_m1373 (Invitation_t341 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.Invitation::get_Inviter()
extern "C" Participant_t340 * Invitation_get_Inviter_m1374 (Invitation_t341 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Invitation::get_Variant()
extern "C" int32_t Invitation_get_Variant_m1375 (Invitation_t341 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Invitation::ToString()
extern "C" String_t* Invitation_ToString_m1376 (Invitation_t341 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
