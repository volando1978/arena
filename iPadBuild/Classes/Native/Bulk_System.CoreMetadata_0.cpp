﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1800_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
static MethodInfo* U3CModuleU3E_t1800_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CModuleU3E_t1800_0_0_0;
extern Il2CppType U3CModuleU3E_t1800_1_0_0;
struct U3CModuleU3E_t1800;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1800_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CModuleU3E_t1800_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1800_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U3CModuleU3E_t1800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1800_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1800_1_0_0/* this_arg */
	, &U3CModuleU3E_t1800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1800)/* instance_size */
	, sizeof (U3CModuleU3E_t1800)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t242_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
MethodInfo ExtensionAttribute__ctor_m1071_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m1071/* method */
	, &ExtensionAttribute_t242_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExtensionAttribute_t242_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m1071_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m7644_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m7549_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference ExtensionAttribute_t242_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ExtensionAttribute_t242_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType _Attribute_t1731_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t242_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType ExtensionAttribute_t242_0_0_0;
extern Il2CppType ExtensionAttribute_t242_1_0_0;
extern Il2CppType Attribute_t1546_0_0_0;
struct ExtensionAttribute_t242;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t242_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, ExtensionAttribute_t242_VTable/* vtableMethods */
	, ExtensionAttribute_t242_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ExtensionAttribute_t242_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t242_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ExtensionAttribute_t242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &ExtensionAttribute_t242_0_0_0/* byval_arg */
	, &ExtensionAttribute_t242_1_0_0/* this_arg */
	, &ExtensionAttribute_t242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t242)/* instance_size */
	, sizeof (ExtensionAttribute_t242)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t1801_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTODOAttribute::.ctor()
MethodInfo MonoTODOAttribute__ctor_m7679_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTODOAttribute__ctor_m7679/* method */
	, &MonoTODOAttribute_t1801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MonoTODOAttribute_t1801_MethodInfos[] =
{
	&MonoTODOAttribute__ctor_m7679_MethodInfo,
	NULL
};
static Il2CppMethodReference MonoTODOAttribute_t1801_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool MonoTODOAttribute_t1801_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1801_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType MonoTODOAttribute_t1801_0_0_0;
extern Il2CppType MonoTODOAttribute_t1801_1_0_0;
struct MonoTODOAttribute_t1801;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t1801_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t1801_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, MonoTODOAttribute_t1801_VTable/* vtableMethods */
	, MonoTODOAttribute_t1801_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MonoTODOAttribute_t1801_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, MonoTODOAttribute_t1801_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoTODOAttribute_t1801_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &MonoTODOAttribute_t1801_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1801_1_0_0/* this_arg */
	, &MonoTODOAttribute_t1801_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1801)/* instance_size */
	, sizeof (MonoTODOAttribute_t1801)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Link
extern TypeInfo Link_t1816_il2cpp_TypeInfo;
extern Il2CppGenericContainer Link_t1816_Il2CppGenericContainer;
extern TypeInfo Link_t1816_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Link_t1816_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Link_t1816_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* Link_t1816_Il2CppGenericParametersArray[1] = 
{
	&Link_t1816_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Link_t1816_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Link_t1816_il2cpp_TypeInfo, 1, 0, Link_t1816_Il2CppGenericParametersArray };
static MethodInfo* Link_t1816_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Link_t1816____HashCode_0_FieldInfo = 
{
	"HashCode"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Link_t1816_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo Link_t1816____Next_1_FieldInfo = 
{
	"Next"/* name */
	, &Int32_t189_0_0_6/* type */
	, &Link_t1816_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Link_t1816_FieldInfos[] =
{
	&Link_t1816____HashCode_0_FieldInfo,
	&Link_t1816____Next_1_FieldInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference Link_t1816_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool Link_t1816_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Link_t1816_0_0_0;
extern Il2CppType Link_t1816_1_0_0;
extern Il2CppType ValueType_t329_0_0_0;
extern TypeInfo HashSet_1_t1815_il2cpp_TypeInfo;
extern Il2CppType HashSet_1_t1815_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1816_DefinitionMetadata = 
{
	&HashSet_1_t1815_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Link_t1816_VTable/* vtableMethods */
	, Link_t1816_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Link_t1816_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1816_MethodInfos/* methods */
	, NULL/* properties */
	, Link_t1816_FieldInfos/* fields */
	, NULL/* events */
	, &Link_t1816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1816_0_0_0/* byval_arg */
	, &Link_t1816_1_0_0/* this_arg */
	, &Link_t1816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Link_t1816_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Enumerator
extern TypeInfo Enumerator_t1817_il2cpp_TypeInfo;
extern Il2CppGenericContainer Enumerator_t1817_Il2CppGenericContainer;
extern TypeInfo Enumerator_t1817_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerator_t1817_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerator_t1817_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerator_t1817_Il2CppGenericParametersArray[1] = 
{
	&Enumerator_t1817_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Enumerator_t1817_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerator_t1817_il2cpp_TypeInfo, 1, 0, Enumerator_t1817_Il2CppGenericParametersArray };
extern Il2CppType HashSet_1_t1831_0_0_0;
extern Il2CppType HashSet_1_t1831_0_0_0;
static ParameterInfo Enumerator_t1817_Enumerator__ctor_m7720_ParameterInfos[] = 
{
	{"hashset", 0, 134217755, 0, &HashSet_1_t1831_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
MethodInfo Enumerator__ctor_m7720_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerator_t1817_Enumerator__ctor_m7720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7721_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
MethodInfo Enumerator_MoveNext_m7722_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Enumerator_t1817_gp_0_0_0_0;
// T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
MethodInfo Enumerator_get_Current_m7723_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1817_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
MethodInfo Enumerator_Dispose_m7724_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::CheckState()
MethodInfo Enumerator_CheckState_m7725_MethodInfo = 
{
	"CheckState"/* name */
	, NULL/* method */
	, &Enumerator_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Enumerator_t1817_MethodInfos[] =
{
	&Enumerator__ctor_m7720_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7721_MethodInfo,
	&Enumerator_MoveNext_m7722_MethodInfo,
	&Enumerator_get_Current_m7723_MethodInfo,
	&Enumerator_Dispose_m7724_MethodInfo,
	&Enumerator_CheckState_m7725_MethodInfo,
	NULL
};
extern Il2CppType HashSet_1_t1831_0_0_1;
FieldInfo Enumerator_t1817____hashset_0_FieldInfo = 
{
	"hashset"/* name */
	, &HashSet_1_t1831_0_0_1/* type */
	, &Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Enumerator_t1817____next_1_FieldInfo = 
{
	"next"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Enumerator_t1817____stamp_2_FieldInfo = 
{
	"stamp"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Enumerator_t1817_gp_0_0_0_1;
FieldInfo Enumerator_t1817____current_3_FieldInfo = 
{
	"current"/* name */
	, &Enumerator_t1817_gp_0_0_0_1/* type */
	, &Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t1817_FieldInfos[] =
{
	&Enumerator_t1817____hashset_0_FieldInfo,
	&Enumerator_t1817____next_1_FieldInfo,
	&Enumerator_t1817____stamp_2_FieldInfo,
	&Enumerator_t1817____current_3_FieldInfo,
	NULL
};
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7721_MethodInfo;
static PropertyInfo Enumerator_t1817____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m7721_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Enumerator_get_Current_m7723_MethodInfo;
static PropertyInfo Enumerator_t1817____Current_PropertyInfo = 
{
	&Enumerator_t1817_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m7723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t1817_PropertyInfos[] =
{
	&Enumerator_t1817____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t1817____Current_PropertyInfo,
	NULL
};
extern MethodInfo Enumerator_MoveNext_m7722_MethodInfo;
extern MethodInfo Enumerator_Dispose_m7724_MethodInfo;
static Il2CppMethodReference Enumerator_t1817_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7721_MethodInfo,
	&Enumerator_MoveNext_m7722_MethodInfo,
	&Enumerator_Dispose_m7724_MethodInfo,
	&Enumerator_get_Current_m7723_MethodInfo,
};
static bool Enumerator_t1817_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern Il2CppType IDisposable_t191_0_0_0;
extern Il2CppType IEnumerator_1_t1832_0_0_0;
static const Il2CppType* Enumerator_t1817_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDisposable_t191_0_0_0,
	&IEnumerator_1_t1832_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1817_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDisposable_t191_0_0_0, 6},
	{ &IEnumerator_1_t1832_0_0_0, 7},
};
extern Il2CppGenericMethod Enumerator_CheckState_m7799_GenericMethod;
extern Il2CppType Enumerator_t1817_gp_0_0_0_0;
extern Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7800_GenericMethod;
static Il2CppRGCTXDefinition Enumerator_t1817_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator_CheckState_m7799_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &Enumerator_t1817_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7800_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Enumerator_t1817_0_0_0;
extern Il2CppType Enumerator_t1817_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t1817_DefinitionMetadata = 
{
	&HashSet_1_t1815_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1817_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1817_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, Enumerator_t1817_VTable/* vtableMethods */
	, Enumerator_t1817_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, Enumerator_t1817_RGCTXData/* rgctxDefinition */

};
TypeInfo Enumerator_t1817_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1817_MethodInfos/* methods */
	, Enumerator_t1817_PropertyInfos/* properties */
	, Enumerator_t1817_FieldInfos/* fields */
	, NULL/* events */
	, &Enumerator_t1817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1817_0_0_0/* byval_arg */
	, &Enumerator_t1817_1_0_0/* this_arg */
	, &Enumerator_t1817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Enumerator_t1817_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/PrimeHelper
extern TypeInfo PrimeHelper_t1818_il2cpp_TypeInfo;
extern Il2CppGenericContainer PrimeHelper_t1818_Il2CppGenericContainer;
extern TypeInfo PrimeHelper_t1818_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull PrimeHelper_t1818_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &PrimeHelper_t1818_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* PrimeHelper_t1818_Il2CppGenericParametersArray[1] = 
{
	&PrimeHelper_t1818_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer PrimeHelper_t1818_Il2CppGenericContainer = { { NULL, NULL }, NULL, &PrimeHelper_t1818_il2cpp_TypeInfo, 1, 0, PrimeHelper_t1818_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/PrimeHelper::.cctor()
MethodInfo PrimeHelper__cctor_m7726_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PrimeHelper_t1818_PrimeHelper_TestPrime_m7727_ParameterInfos[] = 
{
	{"x", 0, 134217756, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper::TestPrime(System.Int32)
MethodInfo PrimeHelper_TestPrime_m7727_MethodInfo = 
{
	"TestPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1818_PrimeHelper_TestPrime_m7727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PrimeHelper_t1818_PrimeHelper_CalcPrime_m7728_ParameterInfos[] = 
{
	{"x", 0, 134217757, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::CalcPrime(System.Int32)
MethodInfo PrimeHelper_CalcPrime_m7728_MethodInfo = 
{
	"CalcPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1818_PrimeHelper_CalcPrime_m7728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PrimeHelper_t1818_PrimeHelper_ToPrime_m7729_ParameterInfos[] = 
{
	{"x", 0, 134217758, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::ToPrime(System.Int32)
MethodInfo PrimeHelper_ToPrime_m7729_MethodInfo = 
{
	"ToPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1818_PrimeHelper_ToPrime_m7729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PrimeHelper_t1818_MethodInfos[] =
{
	&PrimeHelper__cctor_m7726_MethodInfo,
	&PrimeHelper_TestPrime_m7727_MethodInfo,
	&PrimeHelper_CalcPrime_m7728_MethodInfo,
	&PrimeHelper_ToPrime_m7729_MethodInfo,
	NULL
};
extern Il2CppType Int32U5BU5D_t107_0_0_49;
FieldInfo PrimeHelper_t1818____primes_table_0_FieldInfo = 
{
	"primes_table"/* name */
	, &Int32U5BU5D_t107_0_0_49/* type */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PrimeHelper_t1818_FieldInfos[] =
{
	&PrimeHelper_t1818____primes_table_0_FieldInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
static Il2CppMethodReference PrimeHelper_t1818_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool PrimeHelper_t1818_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType PrimeHelper_t1834_0_0_0;
extern Il2CppGenericMethod PrimeHelper_TestPrime_m7801_GenericMethod;
extern Il2CppGenericMethod PrimeHelper_CalcPrime_m7802_GenericMethod;
static Il2CppRGCTXDefinition PrimeHelper_t1818_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &PrimeHelper_t1834_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_TestPrime_m7801_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_CalcPrime_m7802_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType PrimeHelper_t1818_0_0_0;
extern Il2CppType PrimeHelper_t1818_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct PrimeHelper_t1818;
const Il2CppTypeDefinitionMetadata PrimeHelper_t1818_DefinitionMetadata = 
{
	&HashSet_1_t1815_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeHelper_t1818_VTable/* vtableMethods */
	, PrimeHelper_t1818_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, PrimeHelper_t1818_RGCTXData/* rgctxDefinition */

};
TypeInfo PrimeHelper_t1818_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeHelper"/* name */
	, ""/* namespaze */
	, PrimeHelper_t1818_MethodInfos/* methods */
	, NULL/* properties */
	, PrimeHelper_t1818_FieldInfos/* fields */
	, NULL/* events */
	, &PrimeHelper_t1818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeHelper_t1818_0_0_0/* byval_arg */
	, &PrimeHelper_t1818_1_0_0/* this_arg */
	, &PrimeHelper_t1818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &PrimeHelper_t1818_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048963/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1
extern Il2CppGenericContainer HashSet_1_t1815_Il2CppGenericContainer;
extern TypeInfo HashSet_1_t1815_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull HashSet_1_t1815_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &HashSet_1_t1815_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* HashSet_1_t1815_Il2CppGenericParametersArray[1] = 
{
	&HashSet_1_t1815_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer HashSet_1_t1815_Il2CppGenericContainer = { { NULL, NULL }, NULL, &HashSet_1_t1815_il2cpp_TypeInfo, 1, 0, HashSet_1_t1815_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::.ctor()
MethodInfo HashSet_1__ctor_m7697_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerable_1_t1836_0_0_0;
extern Il2CppType IEnumerable_1_t1836_0_0_0;
extern Il2CppType IEqualityComparer_1_t1837_0_0_0;
extern Il2CppType IEqualityComparer_1_t1837_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1__ctor_m7698_ParameterInfos[] = 
{
	{"collection", 0, 134217729, 0, &IEnumerable_1_t1836_0_0_0},
	{"comparer", 1, 134217730, 0, &IEqualityComparer_1_t1837_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
MethodInfo HashSet_1__ctor_m7698_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1__ctor_m7698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1__ctor_m7699_ParameterInfos[] = 
{
	{"info", 0, 134217731, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134217732, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo HashSet_1__ctor_m7699_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1__ctor_m7699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1838_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7700_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1838_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7701_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1839_0_0_0;
extern Il2CppType TU5BU5D_t1839_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_ParameterInfos[] = 
{
	{"array", 0, 134217733, 0, &TU5BU5D_t1839_0_0_0},
	{"index", 1, 134217734, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_ParameterInfos[] = 
{
	{"item", 0, 134217735, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7704_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::get_Count()
MethodInfo HashSet_1_get_Count_m7705_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType IEqualityComparer_1_t1837_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_Init_m7706_ParameterInfos[] = 
{
	{"capacity", 0, 134217736, 0, &Int32_t189_0_0_0},
	{"comparer", 1, 134217737, 0, &IEqualityComparer_1_t1837_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
MethodInfo HashSet_1_Init_m7706_MethodInfo = 
{
	"Init"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_Init_m7706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_InitArrays_m7707_ParameterInfos[] = 
{
	{"size", 0, 134217738, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::InitArrays(System.Int32)
MethodInfo HashSet_1_InitArrays_m7707_MethodInfo = 
{
	"InitArrays"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_InitArrays_m7707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_SlotsContainsAt_m7708_ParameterInfos[] = 
{
	{"index", 0, 134217739, 0, &Int32_t189_0_0_0},
	{"hash", 1, 134217740, 0, &Int32_t189_0_0_0},
	{"item", 2, 134217741, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::SlotsContainsAt(System.Int32,System.Int32,T)
MethodInfo HashSet_1_SlotsContainsAt_m7708_MethodInfo = 
{
	"SlotsContainsAt"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_SlotsContainsAt_m7708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1839_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_CopyTo_m7709_ParameterInfos[] = 
{
	{"array", 0, 134217742, 0, &TU5BU5D_t1839_0_0_0},
	{"index", 1, 134217743, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
MethodInfo HashSet_1_CopyTo_m7709_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_CopyTo_m7709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1839_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_CopyTo_m7710_ParameterInfos[] = 
{
	{"array", 0, 134217744, 0, &TU5BU5D_t1839_0_0_0},
	{"index", 1, 134217745, 0, &Int32_t189_0_0_0},
	{"count", 2, 134217746, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
MethodInfo HashSet_1_CopyTo_m7710_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_CopyTo_m7710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Resize()
MethodInfo HashSet_1_Resize_m7711_MethodInfo = 
{
	"Resize"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_GetLinkHashCode_m7712_ParameterInfos[] = 
{
	{"index", 0, 134217747, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::GetLinkHashCode(System.Int32)
MethodInfo HashSet_1_GetLinkHashCode_m7712_MethodInfo = 
{
	"GetLinkHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_GetLinkHashCode_m7712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_GetItemHashCode_m7713_ParameterInfos[] = 
{
	{"item", 0, 134217748, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::GetItemHashCode(T)
MethodInfo HashSet_1_GetItemHashCode_m7713_MethodInfo = 
{
	"GetItemHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_GetItemHashCode_m7713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_Add_m7714_ParameterInfos[] = 
{
	{"item", 0, 134217749, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Add(T)
MethodInfo HashSet_1_Add_m7714_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_Add_m7714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Clear()
MethodInfo HashSet_1_Clear_m7715_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_Contains_m7716_ParameterInfos[] = 
{
	{"item", 0, 134217750, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
MethodInfo HashSet_1_Contains_m7716_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_Contains_m7716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1815_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_Remove_m7717_ParameterInfos[] = 
{
	{"item", 0, 134217751, 0, &HashSet_1_t1815_gp_0_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
MethodInfo HashSet_1_Remove_m7717_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_Remove_m7717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_GetObjectData_m7718_ParameterInfos[] = 
{
	{"info", 0, 134217752, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134217753, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo HashSet_1_GetObjectData_m7718_MethodInfo = 
{
	"GetObjectData"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_GetObjectData_m7718_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo HashSet_1_t1815_HashSet_1_OnDeserialization_m7719_ParameterInfos[] = 
{
	{"sender", 0, 134217754, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
MethodInfo HashSet_1_OnDeserialization_m7719_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1815_HashSet_1_OnDeserialization_m7719_ParameterInfos/* parameters */
	, 5/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HashSet_1_t1815_MethodInfos[] =
{
	&HashSet_1__ctor_m7697_MethodInfo,
	&HashSet_1__ctor_m7698_MethodInfo,
	&HashSet_1__ctor_m7699_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7700_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7701_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7704_MethodInfo,
	&HashSet_1_get_Count_m7705_MethodInfo,
	&HashSet_1_Init_m7706_MethodInfo,
	&HashSet_1_InitArrays_m7707_MethodInfo,
	&HashSet_1_SlotsContainsAt_m7708_MethodInfo,
	&HashSet_1_CopyTo_m7709_MethodInfo,
	&HashSet_1_CopyTo_m7710_MethodInfo,
	&HashSet_1_Resize_m7711_MethodInfo,
	&HashSet_1_GetLinkHashCode_m7712_MethodInfo,
	&HashSet_1_GetItemHashCode_m7713_MethodInfo,
	&HashSet_1_Add_m7714_MethodInfo,
	&HashSet_1_Clear_m7715_MethodInfo,
	&HashSet_1_Contains_m7716_MethodInfo,
	&HashSet_1_Remove_m7717_MethodInfo,
	&HashSet_1_GetObjectData_m7718_MethodInfo,
	&HashSet_1_OnDeserialization_m7719_MethodInfo,
	NULL
};
extern Il2CppType Int32U5BU5D_t107_0_0_1;
FieldInfo HashSet_1_t1815____table_0_FieldInfo = 
{
	"table"/* name */
	, &Int32U5BU5D_t107_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType LinkU5BU5D_t1840_0_0_1;
FieldInfo HashSet_1_t1815____links_1_FieldInfo = 
{
	"links"/* name */
	, &LinkU5BU5D_t1840_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TU5BU5D_t1839_0_0_1;
FieldInfo HashSet_1_t1815____slots_2_FieldInfo = 
{
	"slots"/* name */
	, &TU5BU5D_t1839_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo HashSet_1_t1815____touched_3_FieldInfo = 
{
	"touched"/* name */
	, &Int32_t189_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo HashSet_1_t1815____empty_slot_4_FieldInfo = 
{
	"empty_slot"/* name */
	, &Int32_t189_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo HashSet_1_t1815____count_5_FieldInfo = 
{
	"count"/* name */
	, &Int32_t189_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo HashSet_1_t1815____threshold_6_FieldInfo = 
{
	"threshold"/* name */
	, &Int32_t189_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEqualityComparer_1_t1837_0_0_1;
FieldInfo HashSet_1_t1815____comparer_7_FieldInfo = 
{
	"comparer"/* name */
	, &IEqualityComparer_1_t1837_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SerializationInfo_t1673_0_0_1;
FieldInfo HashSet_1_t1815____si_8_FieldInfo = 
{
	"si"/* name */
	, &SerializationInfo_t1673_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo HashSet_1_t1815____generation_9_FieldInfo = 
{
	"generation"/* name */
	, &Int32_t189_0_0_1/* type */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* HashSet_1_t1815_FieldInfos[] =
{
	&HashSet_1_t1815____table_0_FieldInfo,
	&HashSet_1_t1815____links_1_FieldInfo,
	&HashSet_1_t1815____slots_2_FieldInfo,
	&HashSet_1_t1815____touched_3_FieldInfo,
	&HashSet_1_t1815____empty_slot_4_FieldInfo,
	&HashSet_1_t1815____count_5_FieldInfo,
	&HashSet_1_t1815____threshold_6_FieldInfo,
	&HashSet_1_t1815____comparer_7_FieldInfo,
	&HashSet_1_t1815____si_8_FieldInfo,
	&HashSet_1_t1815____generation_9_FieldInfo,
	NULL
};
extern MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7701_MethodInfo;
static PropertyInfo HashSet_1_t1815____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7701_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo HashSet_1_get_Count_m7705_MethodInfo;
static PropertyInfo HashSet_1_t1815____Count_PropertyInfo = 
{
	&HashSet_1_t1815_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &HashSet_1_get_Count_m7705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* HashSet_1_t1815_PropertyInfos[] =
{
	&HashSet_1_t1815____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&HashSet_1_t1815____Count_PropertyInfo,
	NULL
};
static const Il2CppType* HashSet_1_t1815_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Link_t1816_0_0_0,
	&Enumerator_t1817_0_0_0,
	&PrimeHelper_t1818_0_0_0,
};
extern MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_MethodInfo;
extern MethodInfo HashSet_1_Clear_m7715_MethodInfo;
extern MethodInfo HashSet_1_Contains_m7716_MethodInfo;
extern MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_MethodInfo;
extern MethodInfo HashSet_1_Remove_m7717_MethodInfo;
extern MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7700_MethodInfo;
extern MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7704_MethodInfo;
extern MethodInfo HashSet_1_GetObjectData_m7718_MethodInfo;
extern MethodInfo HashSet_1_OnDeserialization_m7719_MethodInfo;
extern MethodInfo HashSet_1_CopyTo_m7709_MethodInfo;
static Il2CppMethodReference HashSet_1_t1815_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&HashSet_1_get_Count_m7705_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7701_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7703_MethodInfo,
	&HashSet_1_Clear_m7715_MethodInfo,
	&HashSet_1_Contains_m7716_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7702_MethodInfo,
	&HashSet_1_Remove_m7717_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7700_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7704_MethodInfo,
	&HashSet_1_GetObjectData_m7718_MethodInfo,
	&HashSet_1_OnDeserialization_m7719_MethodInfo,
	&HashSet_1_CopyTo_m7709_MethodInfo,
	&HashSet_1_GetObjectData_m7718_MethodInfo,
	&HashSet_1_OnDeserialization_m7719_MethodInfo,
};
static bool HashSet_1_t1815_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICollection_1_t1841_0_0_0;
extern Il2CppType IEnumerable_t38_0_0_0;
extern Il2CppType ISerializable_t310_0_0_0;
extern Il2CppType IDeserializationCallback_t1842_0_0_0;
static const Il2CppType* HashSet_1_t1815_InterfacesTypeInfos[] = 
{
	&ICollection_1_t1841_0_0_0,
	&IEnumerable_1_t1836_0_0_0,
	&IEnumerable_t38_0_0_0,
	&ISerializable_t310_0_0_0,
	&IDeserializationCallback_t1842_0_0_0,
};
static Il2CppInterfaceOffsetPair HashSet_1_t1815_InterfacesOffsets[] = 
{
	{ &ICollection_1_t1841_0_0_0, 4},
	{ &IEnumerable_1_t1836_0_0_0, 11},
	{ &IEnumerable_t38_0_0_0, 12},
	{ &ISerializable_t310_0_0_0, 13},
	{ &IDeserializationCallback_t1842_0_0_0, 14},
};
extern Il2CppGenericMethod HashSet_1_Init_m7803_GenericMethod;
extern Il2CppType IEnumerator_1_t1838_0_0_0;
extern Il2CppGenericMethod HashSet_1_Add_m7804_GenericMethod;
extern Il2CppType Enumerator_t1843_0_0_0;
extern Il2CppGenericMethod Enumerator__ctor_m7805_GenericMethod;
extern Il2CppGenericMethod HashSet_1_CopyTo_m7806_GenericMethod;
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m7807_GenericMethod;
extern Il2CppGenericMethod HashSet_1_InitArrays_m7808_GenericMethod;
extern Il2CppType LinkU5BU5D_t1840_0_0_0;
extern Il2CppGenericMethod HashSet_1_CopyTo_m7809_GenericMethod;
extern Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7810_GenericMethod;
extern Il2CppGenericMethod PrimeHelper_ToPrime_m7811_GenericMethod;
extern Il2CppGenericMethod HashSet_1_GetItemHashCode_m7812_GenericMethod;
extern Il2CppGenericMethod HashSet_1_SlotsContainsAt_m7813_GenericMethod;
extern Il2CppGenericMethod HashSet_1_Resize_m7814_GenericMethod;
static Il2CppRGCTXDefinition HashSet_1_t1815_RGCTXData[21] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Init_m7803_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1841_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1836_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1838_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Add_m7804_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &Enumerator_t1843_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator__ctor_m7805_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7806_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7807_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_InitArrays_m7808_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &LinkU5BU5D_t1840_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, &TU5BU5D_t1839_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, &HashSet_1_t1815_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEqualityComparer_1_t1837_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7809_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7810_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_ToPrime_m7811_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetItemHashCode_m7812_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_SlotsContainsAt_m7813_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Resize_m7814_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType HashSet_1_t1815_1_0_0;
struct HashSet_1_t1815;
const Il2CppTypeDefinitionMetadata HashSet_1_t1815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, HashSet_1_t1815_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, HashSet_1_t1815_InterfacesTypeInfos/* implementedInterfaces */
	, HashSet_1_t1815_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashSet_1_t1815_VTable/* vtableMethods */
	, HashSet_1_t1815_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, HashSet_1_t1815_RGCTXData/* rgctxDefinition */

};
TypeInfo HashSet_1_t1815_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashSet`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, HashSet_1_t1815_MethodInfos/* methods */
	, HashSet_1_t1815_PropertyInfos/* properties */
	, HashSet_1_t1815_FieldInfos/* fields */
	, NULL/* events */
	, &HashSet_1_t1815_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HashSet_1_t1815_0_0_0/* byval_arg */
	, &HashSet_1_t1815_1_0_0/* this_arg */
	, &HashSet_1_t1815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &HashSet_1_t1815_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 18/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t1802_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1802_Check_Source_m7680_ParameterInfos[] = 
{
	{"source", 0, 134217759, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::Source(System.Object)
MethodInfo Check_Source_m7680_MethodInfo = 
{
	"Source"/* name */
	, (methodPointerType)&Check_Source_m7680/* method */
	, &Check_t1802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Check_t1802_Check_Source_m7680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1802_Check_SourceAndSelector_m7681_ParameterInfos[] = 
{
	{"source", 0, 134217760, 0, &Object_t_0_0_0},
	{"selector", 1, 134217761, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndSelector(System.Object,System.Object)
MethodInfo Check_SourceAndSelector_m7681_MethodInfo = 
{
	"SourceAndSelector"/* name */
	, (methodPointerType)&Check_SourceAndSelector_m7681/* method */
	, &Check_t1802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Check_t1802_Check_SourceAndSelector_m7681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1802_Check_SourceAndPredicate_m7682_ParameterInfos[] = 
{
	{"source", 0, 134217762, 0, &Object_t_0_0_0},
	{"predicate", 1, 134217763, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
MethodInfo Check_SourceAndPredicate_m7682_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m7682/* method */
	, &Check_t1802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Check_t1802_Check_SourceAndPredicate_m7682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1802_Check_FirstAndSecond_m7683_ParameterInfos[] = 
{
	{"first", 0, 134217764, 0, &Object_t_0_0_0},
	{"second", 1, 134217765, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::FirstAndSecond(System.Object,System.Object)
MethodInfo Check_FirstAndSecond_m7683_MethodInfo = 
{
	"FirstAndSecond"/* name */
	, (methodPointerType)&Check_FirstAndSecond_m7683/* method */
	, &Check_t1802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Check_t1802_Check_FirstAndSecond_m7683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1802_Check_SourceAndKeyElementSelectors_m7684_ParameterInfos[] = 
{
	{"source", 0, 134217766, 0, &Object_t_0_0_0},
	{"keySelector", 1, 134217767, 0, &Object_t_0_0_0},
	{"elementSelector", 2, 134217768, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndKeyElementSelectors(System.Object,System.Object,System.Object)
MethodInfo Check_SourceAndKeyElementSelectors_m7684_MethodInfo = 
{
	"SourceAndKeyElementSelectors"/* name */
	, (methodPointerType)&Check_SourceAndKeyElementSelectors_m7684/* method */
	, &Check_t1802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* invoker_method */
	, Check_t1802_Check_SourceAndKeyElementSelectors_m7684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Check_t1802_MethodInfos[] =
{
	&Check_Source_m7680_MethodInfo,
	&Check_SourceAndSelector_m7681_MethodInfo,
	&Check_SourceAndPredicate_m7682_MethodInfo,
	&Check_FirstAndSecond_m7683_MethodInfo,
	&Check_SourceAndKeyElementSelectors_m7684_MethodInfo,
	NULL
};
static Il2CppMethodReference Check_t1802_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Check_t1802_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Check_t1802_0_0_0;
extern Il2CppType Check_t1802_1_0_0;
struct Check_t1802;
const Il2CppTypeDefinitionMetadata Check_t1802_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t1802_VTable/* vtableMethods */
	, Check_t1802_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Check_t1802_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t1802_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Check_t1802_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t1802_0_0_0/* byval_arg */
	, &Check_t1802_1_0_0/* this_arg */
	, &Check_t1802_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1802)/* instance_size */
	, sizeof (Check_t1802)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_Fallback.h"
// Metadata Definition System.Linq.Enumerable/Fallback
extern TypeInfo Fallback_t1803_il2cpp_TypeInfo;
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_FallbackMethodDeclarations.h"
static MethodInfo* Fallback_t1803_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo Fallback_t1803____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &Fallback_t1803_il2cpp_TypeInfo/* parent */
	, offsetof(Fallback_t1803, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Fallback_t1803_0_0_32854;
FieldInfo Fallback_t1803____Default_2_FieldInfo = 
{
	"Default"/* name */
	, &Fallback_t1803_0_0_32854/* type */
	, &Fallback_t1803_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Fallback_t1803_0_0_32854;
FieldInfo Fallback_t1803____Throw_3_FieldInfo = 
{
	"Throw"/* name */
	, &Fallback_t1803_0_0_32854/* type */
	, &Fallback_t1803_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Fallback_t1803_FieldInfos[] =
{
	&Fallback_t1803____value___1_FieldInfo,
	&Fallback_t1803____Default_2_FieldInfo,
	&Fallback_t1803____Throw_3_FieldInfo,
	NULL
};
static const int32_t Fallback_t1803____Default_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry Fallback_t1803____Default_2_DefaultValue = 
{
	&Fallback_t1803____Default_2_FieldInfo/* field */
	, { (char*)&Fallback_t1803____Default_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t Fallback_t1803____Throw_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry Fallback_t1803____Throw_3_DefaultValue = 
{
	&Fallback_t1803____Throw_3_FieldInfo/* field */
	, { (char*)&Fallback_t1803____Throw_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Fallback_t1803_FieldDefaultValues[] = 
{
	&Fallback_t1803____Default_2_DefaultValue,
	&Fallback_t1803____Throw_3_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference Fallback_t1803_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool Fallback_t1803_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair Fallback_t1803_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Fallback_t1803_0_0_0;
extern Il2CppType Fallback_t1803_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
extern TypeInfo Enumerable_t219_il2cpp_TypeInfo;
extern Il2CppType Enumerable_t219_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Fallback_t1803_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Fallback_t1803_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, Fallback_t1803_VTable/* vtableMethods */
	, Fallback_t1803_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Fallback_t1803_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Fallback"/* name */
	, ""/* namespaze */
	, Fallback_t1803_MethodInfos/* methods */
	, NULL/* properties */
	, Fallback_t1803_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Fallback_t1803_0_0_0/* byval_arg */
	, &Fallback_t1803_1_0_0/* this_arg */
	, &Fallback_t1803_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Fallback_t1803_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Fallback_t1803)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Fallback_t1803)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/Function`1
extern TypeInfo Function_1_t1819_il2cpp_TypeInfo;
extern Il2CppGenericContainer Function_1_t1819_Il2CppGenericContainer;
extern TypeInfo Function_1_t1819_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Function_1_t1819_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Function_1_t1819_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* Function_1_t1819_Il2CppGenericParametersArray[1] = 
{
	&Function_1_t1819_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Function_1_t1819_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Function_1_t1819_il2cpp_TypeInfo, 1, 0, Function_1_t1819_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/Function`1::.cctor()
MethodInfo Function_1__cctor_m7749_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &Function_1_t1819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Function_1_t1819_gp_0_0_0_0;
extern Il2CppType Function_1_t1819_gp_0_0_0_0;
static ParameterInfo Function_1_t1819_Function_1_U3CIdentityU3Em__77_m7750_ParameterInfos[] = 
{
	{"t", 0, 134217808, 0, &Function_1_t1819_gp_0_0_0_0},
};
extern Il2CppType Function_1_t1819_gp_0_0_0_0;
// T System.Linq.Enumerable/Function`1::<Identity>m__77(T)
MethodInfo Function_1_U3CIdentityU3Em__77_m7750_MethodInfo = 
{
	"<Identity>m__77"/* name */
	, NULL/* method */
	, &Function_1_t1819_il2cpp_TypeInfo/* declaring_type */
	, &Function_1_t1819_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Function_1_t1819_Function_1_U3CIdentityU3Em__77_m7750_ParameterInfos/* parameters */
	, 27/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Function_1_t1819_MethodInfos[] =
{
	&Function_1__cctor_m7749_MethodInfo,
	&Function_1_U3CIdentityU3Em__77_m7750_MethodInfo,
	NULL
};
extern Il2CppType Func_2_t1845_0_0_54;
FieldInfo Function_1_t1819____Identity_0_FieldInfo = 
{
	"Identity"/* name */
	, &Func_2_t1845_0_0_54/* type */
	, &Function_1_t1819_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1845_0_0_17;
FieldInfo Function_1_t1819____U3CU3Ef__amU24cache1_1_FieldInfo = 
{
	"<>f__am$cache1"/* name */
	, &Func_2_t1845_0_0_17/* type */
	, &Function_1_t1819_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 26/* custom_attributes_cache */

};
static FieldInfo* Function_1_t1819_FieldInfos[] =
{
	&Function_1_t1819____Identity_0_FieldInfo,
	&Function_1_t1819____U3CU3Ef__amU24cache1_1_FieldInfo,
	NULL
};
static Il2CppMethodReference Function_1_t1819_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Function_1_t1819_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType Function_1_t1846_0_0_0;
extern Il2CppGenericMethod Function_1_U3CIdentityU3Em__77_m7815_GenericMethod;
extern Il2CppType Func_2_t1845_0_0_0;
extern Il2CppGenericMethod Func_2__ctor_m7816_GenericMethod;
static Il2CppRGCTXDefinition Function_1_t1819_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &Function_1_t1846_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Function_1_U3CIdentityU3Em__77_m7815_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &Func_2_t1845_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2__ctor_m7816_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Function_1_t1819_0_0_0;
extern Il2CppType Function_1_t1819_1_0_0;
struct Function_1_t1819;
const Il2CppTypeDefinitionMetadata Function_1_t1819_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Function_1_t1819_VTable/* vtableMethods */
	, Function_1_t1819_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, Function_1_t1819_RGCTXData/* rgctxDefinition */

};
TypeInfo Function_1_t1819_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Function`1"/* name */
	, ""/* namespaze */
	, Function_1_t1819_MethodInfos/* methods */
	, NULL/* properties */
	, Function_1_t1819_FieldInfos/* fields */
	, NULL/* events */
	, &Function_1_t1819_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Function_1_t1819_0_0_0/* byval_arg */
	, &Function_1_t1819_1_0_0/* this_arg */
	, &Function_1_t1819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Function_1_t1819_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo;
extern Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericContainer;
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo, 1, 0, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::.ctor()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7751_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_0;
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TResult>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 29/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 30/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 31/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1848_0_0_0;
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TResult>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1848_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 32/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::MoveNext()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7756_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::Dispose()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 33/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_MethodInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7751_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7756_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757_MethodInfo,
	NULL
};
extern Il2CppType IEnumerable_t38_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____source_0_FieldInfo = 
{
	"source"/* name */
	, &IEnumerable_t38_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_t37_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CU24s_41U3E__0_1_FieldInfo = 
{
	"<$s_41>__0"/* name */
	, &IEnumerator_t37_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CelementU3E__1_2_FieldInfo = 
{
	"<element>__1"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U24PC_3_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U24current_4_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_t38_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CU24U3Esource_5_FieldInfo = 
{
	"<$>source"/* name */
	, &IEnumerable_t38_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_FieldInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____source_0_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CU24s_41U3E__0_1_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CelementU3E__1_2_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U24PC_3_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U24current_4_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____U3CU24U3Esource_5_FieldInfo,
	NULL
};
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752_MethodInfo;
static PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TResult>.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753_MethodInfo;
static PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_PropertyInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7756_MethodInfo;
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757_MethodInfo;
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754_MethodInfo;
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755_MethodInfo;
static Il2CppMethodReference U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7753_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7756_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7757_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7754_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7755_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7752_MethodInfo,
};
static bool U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_1_t1849_0_0_0;
extern Il2CppType IEnumerator_1_t1848_0_0_0;
static const Il2CppType* U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDisposable_t191_0_0_0,
	&IEnumerable_t38_0_0_0,
	&IEnumerable_1_t1849_0_0_0,
	&IEnumerator_1_t1848_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDisposable_t191_0_0_0, 6},
	{ &IEnumerable_t38_0_0_0, 7},
	{ &IEnumerable_1_t1849_0_0_0, 8},
	{ &IEnumerator_1_t1848_0_0_0, 9},
};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_0;
extern Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7817_GenericMethod;
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1850_0_0_0;
extern Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7818_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7817_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1850_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7818_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_0_0_0;
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_1_0_0;
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1820;
const Il2CppTypeDefinitionMetadata U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_VTable/* vtableMethods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_RGCTXData/* rgctxDefinition */

};
TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateCastIterator>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_MethodInfos/* methods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_PropertyInfos/* properties */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_FieldInfos/* fields */
	, NULL/* events */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 28/* custom_attributes_cache */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_0_0_0/* byval_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_1_0_0/* this_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1
extern TypeInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo;
extern Il2CppGenericContainer U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericContainer;
extern TypeInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo, 1, 0, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::.ctor()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7758_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 35/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 36/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 37/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1852_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1852_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 38/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::MoveNext()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m7763_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1::Dispose()
MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 39/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_MethodInfos[] =
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7758_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m7763_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764_MethodInfo,
	NULL
};
extern Il2CppType IEnumerable_1_t1853_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____second_0_FieldInfo = 
{
	"second"/* name */
	, &IEnumerable_1_t1853_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEqualityComparer_1_t1854_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____comparer_1_FieldInfo = 
{
	"comparer"/* name */
	, &IEqualityComparer_1_t1854_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType HashSet_1_t1855_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CitemsU3E__0_2_FieldInfo = 
{
	"<items>__0"/* name */
	, &HashSet_1_t1855_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1853_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____first_3_FieldInfo = 
{
	"first"/* name */
	, &IEnumerable_1_t1853_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_1_t1852_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24s_49U3E__1_4_FieldInfo = 
{
	"<$s_49>__1"/* name */
	, &IEnumerator_1_t1852_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CelementU3E__2_5_FieldInfo = 
{
	"<element>__2"/* name */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U24PC_6_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U24current_7_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1853_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Esecond_8_FieldInfo = 
{
	"<$>second"/* name */
	, &IEnumerable_1_t1853_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEqualityComparer_1_t1854_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Ecomparer_9_FieldInfo = 
{
	"<$>comparer"/* name */
	, &IEqualityComparer_1_t1854_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1853_0_0_3;
FieldInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Efirst_10_FieldInfo = 
{
	"<$>first"/* name */
	, &IEnumerable_1_t1853_0_0_3/* type */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_FieldInfos[] =
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____second_0_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____comparer_1_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CitemsU3E__0_2_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____first_3_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24s_49U3E__1_4_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CelementU3E__2_5_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U24PC_6_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U24current_7_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Esecond_8_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Ecomparer_9_FieldInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____U3CU24U3Efirst_10_FieldInfo,
	NULL
};
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759_MethodInfo;
static PropertyInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760_MethodInfo;
static PropertyInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_PropertyInfos[] =
{
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m7763_MethodInfo;
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764_MethodInfo;
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761_MethodInfo;
extern MethodInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762_MethodInfo;
static Il2CppMethodReference U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m7760_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m7763_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m7764_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m7761_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7762_MethodInfo,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7759_MethodInfo,
};
static bool U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_1_t1853_0_0_0;
extern Il2CppType IEnumerator_1_t1852_0_0_0;
static const Il2CppType* U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDisposable_t191_0_0_0,
	&IEnumerable_t38_0_0_0,
	&IEnumerable_1_t1853_0_0_0,
	&IEnumerator_1_t1852_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDisposable_t191_0_0_0, 6},
	{ &IEnumerable_t38_0_0_0, 7},
	{ &IEnumerable_1_t1853_0_0_0, 8},
	{ &IEnumerator_1_t1852_0_0_0, 9},
};
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_0;
extern Il2CppGenericMethod U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7819_GenericMethod;
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1856_0_0_0;
extern Il2CppGenericMethod U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7820_GenericMethod;
extern Il2CppType HashSet_1_t1855_0_0_0;
extern Il2CppGenericMethod HashSet_1__ctor_m7821_GenericMethod;
extern Il2CppGenericMethod Enumerable_Contains_TisTSource_t1851_m7822_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7819_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1856_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7820_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &HashSet_1_t1855_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1__ctor_m7821_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1853_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1852_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Contains_TisTSource_t1851_m7822_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_0_0_0;
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_1_0_0;
struct U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821;
const Il2CppTypeDefinitionMetadata U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_VTable/* vtableMethods */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_RGCTXData/* rgctxDefinition */

};
TypeInfo U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateExceptIterator>c__Iterator4`1"/* name */
	, ""/* namespaze */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_MethodInfos/* methods */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_PropertyInfos/* properties */
	, U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_FieldInfos/* fields */
	, NULL/* events */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 34/* custom_attributes_cache */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_0_0_0/* byval_arg */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_1_0_0/* this_arg */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2
extern TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo;
extern Il2CppGenericContainer U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericContainer;
extern TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TResult_1_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericContainer, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericParametersArray[2] = 
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo, 2, 0, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::.ctor()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7765_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0;
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TResult>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 41/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 42/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 43/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1859_0_0_0;
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TResult>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1859_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 44/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::MoveNext()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m7770_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2::Dispose()
MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 45/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_MethodInfos[] =
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7765_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m7770_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771_MethodInfo,
	NULL
};
extern Il2CppType IEnumerable_1_t1860_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____source_0_FieldInfo = 
{
	"source"/* name */
	, &IEnumerable_1_t1860_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_1_t1861_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24s_70U3E__0_1_FieldInfo = 
{
	"<$s_70>__0"/* name */
	, &IEnumerator_1_t1861_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_0_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CelementU3E__1_2_FieldInfo = 
{
	"<element>__1"/* name */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_0_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1862_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____selector_3_FieldInfo = 
{
	"selector"/* name */
	, &Func_2_t1862_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U24PC_4_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U24current_5_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1860_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24U3Esource_6_FieldInfo = 
{
	"<$>source"/* name */
	, &IEnumerable_1_t1860_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1862_0_0_3;
FieldInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24U3Eselector_7_FieldInfo = 
{
	"<$>selector"/* name */
	, &Func_2_t1862_0_0_3/* type */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_FieldInfos[] =
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____source_0_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24s_70U3E__0_1_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CelementU3E__1_2_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____selector_3_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U24PC_4_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U24current_5_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24U3Esource_6_FieldInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____U3CU24U3Eselector_7_FieldInfo,
	NULL
};
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766_MethodInfo;
static PropertyInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo = 
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TResult>.Current"/* name */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767_MethodInfo;
static PropertyInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_PropertyInfos[] =
{
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m7770_MethodInfo;
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771_MethodInfo;
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768_MethodInfo;
extern MethodInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769_MethodInfo;
static Il2CppMethodReference U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m7767_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m7770_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m7771_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m7768_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7769_MethodInfo,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7766_MethodInfo,
};
static bool U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_1_t1863_0_0_0;
extern Il2CppType IEnumerator_1_t1859_0_0_0;
static const Il2CppType* U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDisposable_t191_0_0_0,
	&IEnumerable_t38_0_0_0,
	&IEnumerable_1_t1863_0_0_0,
	&IEnumerator_1_t1859_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDisposable_t191_0_0_0, 6},
	{ &IEnumerable_t38_0_0_0, 7},
	{ &IEnumerable_1_t1863_0_0_0, 8},
	{ &IEnumerator_1_t1859_0_0_0, 9},
};
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0;
extern Il2CppGenericMethod U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7823_GenericMethod;
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1864_0_0_0;
extern Il2CppGenericMethod U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7824_GenericMethod;
extern Il2CppType IEnumerable_1_t1860_0_0_0;
extern Il2CppType IEnumerator_1_t1861_0_0_0;
extern Il2CppGenericMethod Func_2_Invoke_m7825_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7823_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1864_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7824_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1860_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1861_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7825_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_0_0_0;
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_1_0_0;
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822;
const Il2CppTypeDefinitionMetadata U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_VTable/* vtableMethods */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_RGCTXData/* rgctxDefinition */

};
TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateSelectIterator>c__Iterator10`2"/* name */
	, ""/* namespaze */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_MethodInfos/* methods */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_PropertyInfos/* properties */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_FieldInfos/* fields */
	, NULL/* events */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 40/* custom_attributes_cache */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_0_0_0/* byval_arg */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_1_0_0/* this_arg */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo;
extern Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericParametersArray };
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7772_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 47/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 48/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 49/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1866_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1866_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 50/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7777_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 51/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7772_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7777_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778_MethodInfo,
	NULL
};
extern Il2CppType IEnumerable_1_t1867_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____source_0_FieldInfo = 
{
	"source"/* name */
	, &IEnumerable_1_t1867_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_1_t1866_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24s_97U3E__0_1_FieldInfo = 
{
	"<$s_97>__0"/* name */
	, &IEnumerator_1_t1866_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CelementU3E__1_2_FieldInfo = 
{
	"<element>__1"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1868_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____predicate_3_FieldInfo = 
{
	"predicate"/* name */
	, &Func_2_t1868_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U24PC_4_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U24current_5_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1867_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24U3Esource_6_FieldInfo = 
{
	"<$>source"/* name */
	, &IEnumerable_1_t1867_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1868_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24U3Epredicate_7_FieldInfo = 
{
	"<$>predicate"/* name */
	, &Func_2_t1868_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, 0/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_FieldInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____source_0_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24s_97U3E__0_1_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CelementU3E__1_2_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____predicate_3_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U24PC_4_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U24current_5_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24U3Esource_6_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____U3CU24U3Epredicate_7_FieldInfo,
	NULL
};
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773_MethodInfo;
static PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774_MethodInfo;
static PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7777_MethodInfo;
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778_MethodInfo;
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775_MethodInfo;
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776_MethodInfo;
static Il2CppMethodReference U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7774_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7777_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7778_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7775_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7776_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7773_MethodInfo,
};
static bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_1_t1867_0_0_0;
extern Il2CppType IEnumerator_1_t1866_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDisposable_t191_0_0_0,
	&IEnumerable_t38_0_0_0,
	&IEnumerable_1_t1867_0_0_0,
	&IEnumerator_1_t1866_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDisposable_t191_0_0_0, 6},
	{ &IEnumerable_t38_0_0_0, 7},
	{ &IEnumerable_1_t1867_0_0_0, 8},
	{ &IEnumerator_1_t1866_0_0_0, 9},
};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0;
extern Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7826_GenericMethod;
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1869_0_0_0;
extern Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7827_GenericMethod;
extern Il2CppGenericMethod Func_2_Invoke_m7828_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7826_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1869_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7827_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1867_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1866_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7828_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_0_0_0;
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_DefinitionMetadata = 
{
	&Enumerable_t219_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_RGCTXData/* rgctxDefinition */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_PropertyInfos/* properties */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_FieldInfos/* fields */
	, NULL/* events */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 46/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern Il2CppType IEnumerable_t38_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Cast_m7730_ParameterInfos[] = 
{
	{"source", 0, 134217769, 0, &IEnumerable_t38_0_0_0},
};
extern Il2CppType IEnumerable_1_t1870_0_0_0;
extern Il2CppGenericContainer Enumerable_Cast_m7730_Il2CppGenericContainer;
extern TypeInfo Enumerable_Cast_m7730_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Cast_m7730_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Cast_m7730_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Cast_m7730_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Cast_m7730_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Cast_m7730_MethodInfo;
Il2CppGenericContainer Enumerable_Cast_m7730_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Cast_m7730_MethodInfo, 1, 1, Enumerable_Cast_m7730_Il2CppGenericParametersArray };
extern Il2CppType IEnumerable_1_t1870_0_0_0;
extern Il2CppGenericMethod Enumerable_CreateCastIterator_TisTResult_t1871_m7829_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Cast_m7730_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1870_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateCastIterator_TisTResult_t1871_m7829_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
MethodInfo Enumerable_Cast_m7730_MethodInfo = 
{
	"Cast"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1870_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Cast_m7730_ParameterInfos/* parameters */
	, 7/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, Enumerable_Cast_m7730_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Cast_m7730_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_t38_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_CreateCastIterator_m7731_ParameterInfos[] = 
{
	{"source", 0, 134217770, 0, &IEnumerable_t38_0_0_0},
};
extern Il2CppType IEnumerable_1_t1872_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateCastIterator_m7731_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateCastIterator_m7731_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateCastIterator_m7731_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateCastIterator_m7731_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateCastIterator_m7731_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateCastIterator_m7731_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateCastIterator_m7731_MethodInfo;
Il2CppGenericContainer Enumerable_CreateCastIterator_m7731_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateCastIterator_m7731_MethodInfo, 1, 1, Enumerable_CreateCastIterator_m7731_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1874_0_0_0;
extern Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7830_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateCastIterator_m7731_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1874_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7830_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CreateCastIterator(System.Collections.IEnumerable)
MethodInfo Enumerable_CreateCastIterator_m7731_MethodInfo = 
{
	"CreateCastIterator"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1872_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_CreateCastIterator_m7731_ParameterInfos/* parameters */
	, 8/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, Enumerable_CreateCastIterator_m7731_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateCastIterator_m7731_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1875_0_0_0;
extern Il2CppType IEnumerable_1_t1875_0_0_0;
extern Il2CppType Enumerable_Contains_m7732_gp_0_0_0_0;
extern Il2CppType Enumerable_Contains_m7732_gp_0_0_0_0;
extern Il2CppType IEqualityComparer_1_t1877_0_0_0;
extern Il2CppType IEqualityComparer_1_t1877_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Contains_m7732_ParameterInfos[] = 
{
	{"source", 0, 134217771, 0, &IEnumerable_1_t1875_0_0_0},
	{"value", 1, 134217772, 0, &Enumerable_Contains_m7732_gp_0_0_0_0},
	{"comparer", 2, 134217773, 0, &IEqualityComparer_1_t1877_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppGenericContainer Enumerable_Contains_m7732_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m7732_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Contains_m7732_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Contains_m7732_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Contains_m7732_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m7732_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Contains_m7732_MethodInfo;
Il2CppGenericContainer Enumerable_Contains_m7732_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Contains_m7732_MethodInfo, 1, 1, Enumerable_Contains_m7732_Il2CppGenericParametersArray };
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m7831_GenericMethod;
extern Il2CppType IEnumerator_1_t1878_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Contains_m7732_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7831_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1875_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1878_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEqualityComparer_1_t1877_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
MethodInfo Enumerable_Contains_m7732_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Contains_m7732_ParameterInfos/* parameters */
	, 9/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, Enumerable_Contains_m7732_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m7732_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1879_0_0_0;
extern Il2CppType IEnumerable_1_t1879_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Count_m7733_ParameterInfos[] = 
{
	{"source", 0, 134217774, 0, &IEnumerable_1_t1879_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppGenericContainer Enumerable_Count_m7733_Il2CppGenericContainer;
extern TypeInfo Enumerable_Count_m7733_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Count_m7733_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Count_m7733_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Count_m7733_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Count_m7733_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Count_m7733_MethodInfo;
Il2CppGenericContainer Enumerable_Count_m7733_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Count_m7733_MethodInfo, 1, 1, Enumerable_Count_m7733_Il2CppGenericParametersArray };
extern Il2CppType ICollection_1_t1881_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Count_m7733_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1881_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1879_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_Count_m7733_MethodInfo = 
{
	"Count"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Count_m7733_ParameterInfos/* parameters */
	, 10/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, Enumerable_Count_m7733_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Count_m7733_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1882_0_0_0;
extern Il2CppType IEnumerable_1_t1882_0_0_0;
extern Il2CppType IEnumerable_1_t1882_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Except_m7734_ParameterInfos[] = 
{
	{"first", 0, 134217775, 0, &IEnumerable_1_t1882_0_0_0},
	{"second", 1, 134217776, 0, &IEnumerable_1_t1882_0_0_0},
};
extern Il2CppType IEnumerable_1_t1882_0_0_0;
extern Il2CppGenericContainer Enumerable_Except_m7734_Il2CppGenericContainer;
extern TypeInfo Enumerable_Except_m7734_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Except_m7734_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Except_m7734_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Except_m7734_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Except_m7734_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Except_m7734_MethodInfo;
Il2CppGenericContainer Enumerable_Except_m7734_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Except_m7734_MethodInfo, 1, 1, Enumerable_Except_m7734_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_Except_TisTSource_t1883_m7832_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Except_m7734_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Except_TisTSource_t1883_m7832_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_Except_m7734_MethodInfo = 
{
	"Except"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1882_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Except_m7734_ParameterInfos/* parameters */
	, 11/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, Enumerable_Except_m7734_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Except_m7734_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1884_0_0_0;
extern Il2CppType IEnumerable_1_t1884_0_0_0;
extern Il2CppType IEnumerable_1_t1884_0_0_0;
extern Il2CppType IEqualityComparer_1_t1885_0_0_0;
extern Il2CppType IEqualityComparer_1_t1885_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Except_m7735_ParameterInfos[] = 
{
	{"first", 0, 134217777, 0, &IEnumerable_1_t1884_0_0_0},
	{"second", 1, 134217778, 0, &IEnumerable_1_t1884_0_0_0},
	{"comparer", 2, 134217779, 0, &IEqualityComparer_1_t1885_0_0_0},
};
extern Il2CppType IEnumerable_1_t1884_0_0_0;
extern Il2CppGenericContainer Enumerable_Except_m7735_Il2CppGenericContainer;
extern TypeInfo Enumerable_Except_m7735_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Except_m7735_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Except_m7735_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Except_m7735_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Except_m7735_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Except_m7735_MethodInfo;
Il2CppGenericContainer Enumerable_Except_m7735_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Except_m7735_MethodInfo, 1, 1, Enumerable_Except_m7735_Il2CppGenericParametersArray };
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m7833_GenericMethod;
extern Il2CppGenericMethod Enumerable_CreateExceptIterator_TisTSource_t1886_m7834_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Except_m7735_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7833_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateExceptIterator_TisTSource_t1886_m7834_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
MethodInfo Enumerable_Except_m7735_MethodInfo = 
{
	"Except"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1884_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Except_m7735_ParameterInfos/* parameters */
	, 12/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, Enumerable_Except_m7735_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Except_m7735_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1887_0_0_0;
extern Il2CppType IEnumerable_1_t1887_0_0_0;
extern Il2CppType IEnumerable_1_t1887_0_0_0;
extern Il2CppType IEqualityComparer_1_t1888_0_0_0;
extern Il2CppType IEqualityComparer_1_t1888_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_CreateExceptIterator_m7736_ParameterInfos[] = 
{
	{"first", 0, 134217780, 0, &IEnumerable_1_t1887_0_0_0},
	{"second", 1, 134217781, 0, &IEnumerable_1_t1887_0_0_0},
	{"comparer", 2, 134217782, 0, &IEqualityComparer_1_t1888_0_0_0},
};
extern Il2CppType IEnumerable_1_t1887_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateExceptIterator_m7736_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateExceptIterator_m7736_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateExceptIterator_m7736_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateExceptIterator_m7736_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateExceptIterator_m7736_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateExceptIterator_m7736_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateExceptIterator_m7736_MethodInfo;
Il2CppGenericContainer Enumerable_CreateExceptIterator_m7736_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateExceptIterator_m7736_MethodInfo, 1, 1, Enumerable_CreateExceptIterator_m7736_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateExceptIteratorU3Ec__Iterator4_1_t1890_0_0_0;
extern Il2CppGenericMethod U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7835_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateExceptIterator_m7736_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateExceptIteratorU3Ec__Iterator4_1_t1890_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m7835_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
MethodInfo Enumerable_CreateExceptIterator_m7736_MethodInfo = 
{
	"CreateExceptIterator"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1887_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_CreateExceptIterator_m7736_ParameterInfos/* parameters */
	, 13/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, Enumerable_CreateExceptIterator_m7736_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateExceptIterator_m7736_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1891_0_0_0;
extern Il2CppType IEnumerable_1_t1891_0_0_0;
extern Il2CppType Func_2_t1892_0_0_0;
extern Il2CppType Func_2_t1892_0_0_0;
extern Il2CppType Fallback_t1803_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_First_m7737_ParameterInfos[] = 
{
	{"source", 0, 134217783, 0, &IEnumerable_1_t1891_0_0_0},
	{"predicate", 1, 134217784, 0, &Func_2_t1892_0_0_0},
	{"fallback", 2, 134217785, 0, &Fallback_t1803_0_0_0},
};
extern Il2CppType Enumerable_First_m7737_gp_0_0_0_0;
extern Il2CppGenericContainer Enumerable_First_m7737_Il2CppGenericContainer;
extern TypeInfo Enumerable_First_m7737_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_First_m7737_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_First_m7737_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_First_m7737_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_First_m7737_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_First_m7737_MethodInfo;
Il2CppGenericContainer Enumerable_First_m7737_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_First_m7737_MethodInfo, 1, 1, Enumerable_First_m7737_Il2CppGenericParametersArray };
extern Il2CppType IEnumerator_1_t1894_0_0_0;
extern Il2CppGenericMethod Func_2_Invoke_m7836_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_First_m7737_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1891_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1894_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7836_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
MethodInfo Enumerable_First_m7737_MethodInfo = 
{
	"First"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_First_m7737_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_First_m7737_ParameterInfos/* parameters */
	, 14/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, Enumerable_First_m7737_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_First_m7737_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1895_0_0_0;
extern Il2CppType IEnumerable_1_t1895_0_0_0;
extern Il2CppType Func_2_t1896_0_0_0;
extern Il2CppType Func_2_t1896_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_FirstOrDefault_m7738_ParameterInfos[] = 
{
	{"source", 0, 134217786, 0, &IEnumerable_1_t1895_0_0_0},
	{"predicate", 1, 134217787, 0, &Func_2_t1896_0_0_0},
};
extern Il2CppType Enumerable_FirstOrDefault_m7738_gp_0_0_0_0;
extern Il2CppGenericContainer Enumerable_FirstOrDefault_m7738_Il2CppGenericContainer;
extern TypeInfo Enumerable_FirstOrDefault_m7738_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_FirstOrDefault_m7738_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_FirstOrDefault_m7738_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_FirstOrDefault_m7738_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_FirstOrDefault_m7738_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_FirstOrDefault_m7738_MethodInfo;
Il2CppGenericContainer Enumerable_FirstOrDefault_m7738_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_FirstOrDefault_m7738_MethodInfo, 1, 1, Enumerable_FirstOrDefault_m7738_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_First_TisTSource_t1897_m7837_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_FirstOrDefault_m7738_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_First_TisTSource_t1897_m7837_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
MethodInfo Enumerable_FirstOrDefault_m7738_MethodInfo = 
{
	"FirstOrDefault"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_FirstOrDefault_m7738_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_FirstOrDefault_m7738_ParameterInfos/* parameters */
	, 15/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, Enumerable_FirstOrDefault_m7738_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_FirstOrDefault_m7738_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1898_0_0_0;
extern Il2CppType IEnumerable_1_t1898_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_LongCount_m7739_ParameterInfos[] = 
{
	{"source", 0, 134217788, 0, &IEnumerable_1_t1898_0_0_0},
};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppGenericContainer Enumerable_LongCount_m7739_Il2CppGenericContainer;
extern TypeInfo Enumerable_LongCount_m7739_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_LongCount_m7739_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_LongCount_m7739_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_LongCount_m7739_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_LongCount_m7739_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_LongCount_m7739_MethodInfo;
Il2CppGenericContainer Enumerable_LongCount_m7739_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_LongCount_m7739_MethodInfo, 1, 1, Enumerable_LongCount_m7739_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition Enumerable_LongCount_m7739_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1898_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Int64 System.Linq.Enumerable::LongCount(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_LongCount_m7739_MethodInfo = 
{
	"LongCount"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t233_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_LongCount_m7739_ParameterInfos/* parameters */
	, 16/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, Enumerable_LongCount_m7739_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_LongCount_m7739_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1900_0_0_0;
extern Il2CppType IEnumerable_1_t1900_0_0_0;
extern Il2CppType Func_2_t1901_0_0_0;
extern Il2CppType Func_2_t1901_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Select_m7740_ParameterInfos[] = 
{
	{"source", 0, 134217789, 0, &IEnumerable_1_t1900_0_0_0},
	{"selector", 1, 134217790, 0, &Func_2_t1901_0_0_0},
};
extern Il2CppType IEnumerable_1_t1902_0_0_0;
extern Il2CppGenericContainer Enumerable_Select_m7740_Il2CppGenericContainer;
extern TypeInfo Enumerable_Select_m7740_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Select_m7740_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Select_m7740_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo Enumerable_Select_m7740_gp_TResult_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Select_m7740_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Select_m7740_Il2CppGenericContainer, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Select_m7740_Il2CppGenericParametersArray[2] = 
{
	&Enumerable_Select_m7740_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_Select_m7740_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Select_m7740_MethodInfo;
Il2CppGenericContainer Enumerable_Select_m7740_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Select_m7740_MethodInfo, 2, 1, Enumerable_Select_m7740_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_CreateSelectIterator_TisTSource_t1903_TisTResult_t1904_m7838_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Select_m7740_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateSelectIterator_TisTSource_t1903_TisTResult_t1904_m7838_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
MethodInfo Enumerable_Select_m7740_MethodInfo = 
{
	"Select"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1902_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Select_m7740_ParameterInfos/* parameters */
	, 17/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, Enumerable_Select_m7740_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Select_m7740_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1905_0_0_0;
extern Il2CppType IEnumerable_1_t1905_0_0_0;
extern Il2CppType Func_2_t1906_0_0_0;
extern Il2CppType Func_2_t1906_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_CreateSelectIterator_m7741_ParameterInfos[] = 
{
	{"source", 0, 134217791, 0, &IEnumerable_1_t1905_0_0_0},
	{"selector", 1, 134217792, 0, &Func_2_t1906_0_0_0},
};
extern Il2CppType IEnumerable_1_t1907_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateSelectIterator_m7741_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateSelectIterator_m7741_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateSelectIterator_m7741_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateSelectIterator_m7741_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo Enumerable_CreateSelectIterator_m7741_gp_TResult_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateSelectIterator_m7741_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateSelectIterator_m7741_Il2CppGenericContainer, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateSelectIterator_m7741_Il2CppGenericParametersArray[2] = 
{
	&Enumerable_CreateSelectIterator_m7741_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_CreateSelectIterator_m7741_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateSelectIterator_m7741_MethodInfo;
Il2CppGenericContainer Enumerable_CreateSelectIterator_m7741_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateSelectIterator_m7741_MethodInfo, 2, 1, Enumerable_CreateSelectIterator_m7741_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t1910_0_0_0;
extern Il2CppGenericMethod U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7839_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateSelectIterator_m7741_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t1910_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m7839_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CreateSelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
MethodInfo Enumerable_CreateSelectIterator_m7741_MethodInfo = 
{
	"CreateSelectIterator"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1907_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_CreateSelectIterator_m7741_ParameterInfos/* parameters */
	, 18/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, Enumerable_CreateSelectIterator_m7741_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateSelectIterator_m7741_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1911_0_0_0;
extern Il2CppType IEnumerable_1_t1911_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_ToArray_m7742_ParameterInfos[] = 
{
	{"source", 0, 134217793, 0, &IEnumerable_1_t1911_0_0_0},
};
extern Il2CppType TSourceU5BU5D_t1912_0_0_0;
extern Il2CppGenericContainer Enumerable_ToArray_m7742_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToArray_m7742_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToArray_m7742_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToArray_m7742_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToArray_m7742_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToArray_m7742_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToArray_m7742_MethodInfo;
Il2CppGenericContainer Enumerable_ToArray_m7742_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToArray_m7742_MethodInfo, 1, 1, Enumerable_ToArray_m7742_Il2CppGenericParametersArray };
extern Il2CppType ICollection_1_t1914_0_0_0;
extern Il2CppType TSourceU5BU5D_t1912_0_0_0;
extern Il2CppType List_1_t1915_0_0_0;
extern Il2CppGenericMethod List_1__ctor_m7840_GenericMethod;
extern Il2CppGenericMethod List_1_ToArray_m7841_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToArray_m7742_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1914_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &TSourceU5BU5D_t1912_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, &List_1_t1915_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7840_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_ToArray_m7841_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_ToArray_m7742_MethodInfo = 
{
	"ToArray"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &TSourceU5BU5D_t1912_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_ToArray_m7742_ParameterInfos/* parameters */
	, 19/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, Enumerable_ToArray_m7742_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToArray_m7742_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1916_0_0_0;
extern Il2CppType IEnumerable_1_t1916_0_0_0;
extern Il2CppType Func_2_t1917_0_0_0;
extern Il2CppType Func_2_t1917_0_0_0;
extern Il2CppType Func_2_t1918_0_0_0;
extern Il2CppType Func_2_t1918_0_0_0;
extern Il2CppType IEqualityComparer_1_t1919_0_0_0;
extern Il2CppType IEqualityComparer_1_t1919_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_ToDictionary_m7743_ParameterInfos[] = 
{
	{"source", 0, 134217794, 0, &IEnumerable_1_t1916_0_0_0},
	{"keySelector", 1, 134217795, 0, &Func_2_t1917_0_0_0},
	{"elementSelector", 2, 134217796, 0, &Func_2_t1918_0_0_0},
	{"comparer", 3, 134217797, 0, &IEqualityComparer_1_t1919_0_0_0},
};
extern Il2CppType Dictionary_2_t1920_0_0_0;
extern Il2CppGenericContainer Enumerable_ToDictionary_m7743_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToDictionary_m7743_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7743_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7743_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo Enumerable_ToDictionary_m7743_gp_TKey_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7743_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7743_Il2CppGenericContainer, 1}, {NULL, "TKey", 0, 0, NULL} };
extern TypeInfo Enumerable_ToDictionary_m7743_gp_TElement_2_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7743_gp_TElement_2_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7743_Il2CppGenericContainer, 2}, {NULL, "TElement", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToDictionary_m7743_Il2CppGenericParametersArray[3] = 
{
	&Enumerable_ToDictionary_m7743_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_ToDictionary_m7743_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_ToDictionary_m7743_gp_TElement_2_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToDictionary_m7743_MethodInfo;
Il2CppGenericContainer Enumerable_ToDictionary_m7743_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToDictionary_m7743_MethodInfo, 3, 1, Enumerable_ToDictionary_m7743_Il2CppGenericParametersArray };
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m7842_GenericMethod;
extern Il2CppType Dictionary_2_t1920_0_0_0;
extern Il2CppGenericMethod Dictionary_2__ctor_m7843_GenericMethod;
extern Il2CppType IEnumerator_1_t1924_0_0_0;
extern Il2CppGenericMethod Func_2_Invoke_m7844_GenericMethod;
extern Il2CppGenericMethod Func_2_Invoke_m7845_GenericMethod;
extern Il2CppGenericMethod Dictionary_2_Add_m7846_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToDictionary_m7743_RGCTXData[9] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7842_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &Dictionary_2_t1920_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m7843_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1916_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerator_1_t1924_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7844_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7845_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m7846_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
MethodInfo Enumerable_ToDictionary_m7743_MethodInfo = 
{
	"ToDictionary"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t1920_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_ToDictionary_m7743_ParameterInfos/* parameters */
	, 20/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, Enumerable_ToDictionary_m7743_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToDictionary_m7743_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1925_0_0_0;
extern Il2CppType IEnumerable_1_t1925_0_0_0;
extern Il2CppType Func_2_t1926_0_0_0;
extern Il2CppType Func_2_t1926_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_ToDictionary_m7744_ParameterInfos[] = 
{
	{"source", 0, 134217798, 0, &IEnumerable_1_t1925_0_0_0},
	{"keySelector", 1, 134217799, 0, &Func_2_t1926_0_0_0},
};
extern Il2CppType Dictionary_2_t1927_0_0_0;
extern Il2CppGenericContainer Enumerable_ToDictionary_m7744_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToDictionary_m7744_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7744_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7744_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo Enumerable_ToDictionary_m7744_gp_TKey_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7744_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7744_Il2CppGenericContainer, 1}, {NULL, "TKey", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToDictionary_m7744_Il2CppGenericParametersArray[2] = 
{
	&Enumerable_ToDictionary_m7744_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_ToDictionary_m7744_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToDictionary_m7744_MethodInfo;
Il2CppGenericContainer Enumerable_ToDictionary_m7744_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToDictionary_m7744_MethodInfo, 2, 1, Enumerable_ToDictionary_m7744_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_ToDictionary_TisTSource_t1928_TisTKey_t1929_m7847_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToDictionary_m7744_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_ToDictionary_TisTSource_t1928_TisTKey_t1929_m7847_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.Dictionary`2<TKey,TSource> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
MethodInfo Enumerable_ToDictionary_m7744_MethodInfo = 
{
	"ToDictionary"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t1927_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_ToDictionary_m7744_ParameterInfos/* parameters */
	, 21/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, Enumerable_ToDictionary_m7744_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToDictionary_m7744_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1930_0_0_0;
extern Il2CppType IEnumerable_1_t1930_0_0_0;
extern Il2CppType Func_2_t1931_0_0_0;
extern Il2CppType Func_2_t1931_0_0_0;
extern Il2CppType IEqualityComparer_1_t1932_0_0_0;
extern Il2CppType IEqualityComparer_1_t1932_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_ToDictionary_m7745_ParameterInfos[] = 
{
	{"source", 0, 134217800, 0, &IEnumerable_1_t1930_0_0_0},
	{"keySelector", 1, 134217801, 0, &Func_2_t1931_0_0_0},
	{"comparer", 2, 134217802, 0, &IEqualityComparer_1_t1932_0_0_0},
};
extern Il2CppType Dictionary_2_t1933_0_0_0;
extern Il2CppGenericContainer Enumerable_ToDictionary_m7745_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToDictionary_m7745_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7745_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7745_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
extern TypeInfo Enumerable_ToDictionary_m7745_gp_TKey_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToDictionary_m7745_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToDictionary_m7745_Il2CppGenericContainer, 1}, {NULL, "TKey", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToDictionary_m7745_Il2CppGenericParametersArray[2] = 
{
	&Enumerable_ToDictionary_m7745_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
	&Enumerable_ToDictionary_m7745_gp_TKey_1_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToDictionary_m7745_MethodInfo;
Il2CppGenericContainer Enumerable_ToDictionary_m7745_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToDictionary_m7745_MethodInfo, 2, 1, Enumerable_ToDictionary_m7745_Il2CppGenericParametersArray };
extern Il2CppType Function_1_t1936_0_0_0;
extern Il2CppGenericMethod Enumerable_ToDictionary_TisTSource_t1934_TisTKey_t1935_TisTSource_t1934_m7848_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToDictionary_m7745_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &Function_1_t1936_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_ToDictionary_TisTSource_t1934_TisTKey_t1935_TisTSource_t1934_m7848_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.Dictionary`2<TKey,TSource> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
MethodInfo Enumerable_ToDictionary_m7745_MethodInfo = 
{
	"ToDictionary"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t1933_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_ToDictionary_m7745_ParameterInfos/* parameters */
	, 22/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, Enumerable_ToDictionary_m7745_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToDictionary_m7745_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1937_0_0_0;
extern Il2CppType IEnumerable_1_t1937_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_ToList_m7746_ParameterInfos[] = 
{
	{"source", 0, 134217803, 0, &IEnumerable_1_t1937_0_0_0},
};
extern Il2CppType List_1_t1938_0_0_0;
extern Il2CppGenericContainer Enumerable_ToList_m7746_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToList_m7746_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToList_m7746_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToList_m7746_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToList_m7746_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToList_m7746_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToList_m7746_MethodInfo;
Il2CppGenericContainer Enumerable_ToList_m7746_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToList_m7746_MethodInfo, 1, 1, Enumerable_ToList_m7746_Il2CppGenericParametersArray };
extern Il2CppType List_1_t1938_0_0_0;
extern Il2CppGenericMethod List_1__ctor_m7849_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToList_m7746_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &List_1_t1938_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7849_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_ToList_m7746_MethodInfo = 
{
	"ToList"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1938_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_ToList_m7746_ParameterInfos/* parameters */
	, 23/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, Enumerable_ToList_m7746_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToList_m7746_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1940_0_0_0;
extern Il2CppType IEnumerable_1_t1940_0_0_0;
extern Il2CppType Func_2_t1941_0_0_0;
extern Il2CppType Func_2_t1941_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_Where_m7747_ParameterInfos[] = 
{
	{"source", 0, 134217804, 0, &IEnumerable_1_t1940_0_0_0},
	{"predicate", 1, 134217805, 0, &Func_2_t1941_0_0_0},
};
extern Il2CppType IEnumerable_1_t1940_0_0_0;
extern Il2CppGenericContainer Enumerable_Where_m7747_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m7747_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Where_m7747_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Where_m7747_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Where_m7747_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m7747_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Where_m7747_MethodInfo;
Il2CppGenericContainer Enumerable_Where_m7747_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Where_m7747_MethodInfo, 1, 1, Enumerable_Where_m7747_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t1942_m7850_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m7747_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t1942_m7850_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
MethodInfo Enumerable_Where_m7747_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1940_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_Where_m7747_ParameterInfos/* parameters */
	, 24/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, Enumerable_Where_m7747_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m7747_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1943_0_0_0;
extern Il2CppType IEnumerable_1_t1943_0_0_0;
extern Il2CppType Func_2_t1944_0_0_0;
extern Il2CppType Func_2_t1944_0_0_0;
static ParameterInfo Enumerable_t219_Enumerable_CreateWhereIterator_m7748_ParameterInfos[] = 
{
	{"source", 0, 134217806, 0, &IEnumerable_1_t1943_0_0_0},
	{"predicate", 1, 134217807, 0, &Func_2_t1944_0_0_0},
};
extern Il2CppType IEnumerable_1_t1943_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateWhereIterator_m7748_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m7748_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateWhereIterator_m7748_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateWhereIterator_m7748_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateWhereIterator_m7748_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m7748_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateWhereIterator_m7748_MethodInfo;
Il2CppGenericContainer Enumerable_CreateWhereIterator_m7748_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateWhereIterator_m7748_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m7748_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1946_0_0_0;
extern Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7851_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m7748_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1946_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7851_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
MethodInfo Enumerable_CreateWhereIterator_m7748_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t219_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1943_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t219_Enumerable_CreateWhereIterator_m7748_ParameterInfos/* parameters */
	, 25/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, Enumerable_CreateWhereIterator_m7748_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m7748_Il2CppGenericContainer/* genericContainer */

};
static MethodInfo* Enumerable_t219_MethodInfos[] =
{
	&Enumerable_Cast_m7730_MethodInfo,
	&Enumerable_CreateCastIterator_m7731_MethodInfo,
	&Enumerable_Contains_m7732_MethodInfo,
	&Enumerable_Count_m7733_MethodInfo,
	&Enumerable_Except_m7734_MethodInfo,
	&Enumerable_Except_m7735_MethodInfo,
	&Enumerable_CreateExceptIterator_m7736_MethodInfo,
	&Enumerable_First_m7737_MethodInfo,
	&Enumerable_FirstOrDefault_m7738_MethodInfo,
	&Enumerable_LongCount_m7739_MethodInfo,
	&Enumerable_Select_m7740_MethodInfo,
	&Enumerable_CreateSelectIterator_m7741_MethodInfo,
	&Enumerable_ToArray_m7742_MethodInfo,
	&Enumerable_ToDictionary_m7743_MethodInfo,
	&Enumerable_ToDictionary_m7744_MethodInfo,
	&Enumerable_ToDictionary_m7745_MethodInfo,
	&Enumerable_ToList_m7746_MethodInfo,
	&Enumerable_Where_m7747_MethodInfo,
	&Enumerable_CreateWhereIterator_m7748_MethodInfo,
	NULL
};
static const Il2CppType* Enumerable_t219_il2cpp_TypeInfo__nestedTypes[6] =
{
	&Fallback_t1803_0_0_0,
	&Function_1_t1819_0_0_0,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_0_0_0,
	&U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_0_0_0,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_0_0_0,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_0_0_0,
};
static Il2CppMethodReference Enumerable_t219_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Enumerable_t219_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Enumerable_t219_1_0_0;
struct Enumerable_t219;
const Il2CppTypeDefinitionMetadata Enumerable_t219_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t219_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t219_VTable/* vtableMethods */
	, Enumerable_t219_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Enumerable_t219_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t219_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Enumerable_t219_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 6/* custom_attributes_cache */
	, &Enumerable_t219_0_0_0/* byval_arg */
	, &Enumerable_t219_1_0_0/* this_arg */
	, &Enumerable_t219_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t219)/* instance_size */
	, sizeof (Enumerable_t219)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action
#include "System_Core_System_Action.h"
// Metadata Definition System.Action
extern TypeInfo Action_t588_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Action_t588_Action__ctor_m3796_ParameterInfos[] = 
{
	{"object", 0, 134217809, 0, &Object_t_0_0_0},
	{"method", 1, 134217810, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
MethodInfo Action__ctor_m3796_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action__ctor_m3796/* method */
	, &Action_t588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, Action_t588_Action__ctor_m3796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::Invoke()
MethodInfo Action_Invoke_m7685_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_Invoke_m7685/* method */
	, &Action_t588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_t588_Action_BeginInvoke_m7686_ParameterInfos[] = 
{
	{"callback", 0, 134217811, 0, &AsyncCallback_t20_0_0_0},
	{"object", 1, 134217812, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo Action_BeginInvoke_m7686_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_BeginInvoke_m7686/* method */
	, &Action_t588_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Action_t588_Action_BeginInvoke_m7686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Action_t588_Action_EndInvoke_m7687_ParameterInfos[] = 
{
	{"result", 0, 134217813, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::EndInvoke(System.IAsyncResult)
MethodInfo Action_EndInvoke_m7687_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_EndInvoke_m7687/* method */
	, &Action_t588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Action_t588_Action_EndInvoke_m7687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Action_t588_MethodInfos[] =
{
	&Action__ctor_m3796_MethodInfo,
	&Action_Invoke_m7685_MethodInfo,
	&Action_BeginInvoke_m7686_MethodInfo,
	&Action_EndInvoke_m7687_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m1272_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m1273_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m1274_MethodInfo;
extern MethodInfo Delegate_Clone_m1275_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m1276_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m1277_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m1278_MethodInfo;
extern MethodInfo Action_Invoke_m7685_MethodInfo;
extern MethodInfo Action_BeginInvoke_m7686_MethodInfo;
extern MethodInfo Action_EndInvoke_m7687_MethodInfo;
static Il2CppMethodReference Action_t588_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Action_Invoke_m7685_MethodInfo,
	&Action_BeginInvoke_m7686_MethodInfo,
	&Action_EndInvoke_m7687_MethodInfo,
};
static bool Action_t588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICloneable_t309_0_0_0;
static Il2CppInterfaceOffsetPair Action_t588_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Action_t588_0_0_0;
extern Il2CppType Action_t588_1_0_0;
extern Il2CppType MulticastDelegate_t22_0_0_0;
struct Action_t588;
const Il2CppTypeDefinitionMetadata Action_t588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_t588_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Action_t588_VTable/* vtableMethods */
	, Action_t588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Action_t588_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, "System"/* namespaze */
	, Action_t588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Action_t588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_t588_0_0_0/* byval_arg */
	, &Action_t588_1_0_0/* this_arg */
	, &Action_t588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Action_t588/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t588)/* instance_size */
	, sizeof (Action_t588)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`2
extern TypeInfo Action_2_t1824_il2cpp_TypeInfo;
extern Il2CppGenericContainer Action_2_t1824_Il2CppGenericContainer;
extern TypeInfo Action_2_t1824_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_2_t1824_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &Action_2_t1824_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo Action_2_t1824_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_2_t1824_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &Action_2_t1824_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* Action_2_t1824_Il2CppGenericParametersArray[2] = 
{
	&Action_2_t1824_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Action_2_t1824_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Action_2_t1824_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Action_2_t1824_il2cpp_TypeInfo, 2, 0, Action_2_t1824_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Action_2_t1824_Action_2__ctor_m7779_ParameterInfos[] = 
{
	{"object", 0, 134217814, 0, &Object_t_0_0_0},
	{"method", 1, 134217815, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`2::.ctor(System.Object,System.IntPtr)
MethodInfo Action_2__ctor_m7779_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_2_t1824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_2_t1824_Action_2__ctor_m7779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_2_t1824_gp_0_0_0_0;
extern Il2CppType Action_2_t1824_gp_0_0_0_0;
extern Il2CppType Action_2_t1824_gp_1_0_0_0;
extern Il2CppType Action_2_t1824_gp_1_0_0_0;
static ParameterInfo Action_2_t1824_Action_2_Invoke_m7780_ParameterInfos[] = 
{
	{"arg1", 0, 134217816, 0, &Action_2_t1824_gp_0_0_0_0},
	{"arg2", 1, 134217817, 0, &Action_2_t1824_gp_1_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`2::Invoke(T1,T2)
MethodInfo Action_2_Invoke_m7780_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_2_t1824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_2_t1824_Action_2_Invoke_m7780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_2_t1824_gp_0_0_0_0;
extern Il2CppType Action_2_t1824_gp_1_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_2_t1824_Action_2_BeginInvoke_m7781_ParameterInfos[] = 
{
	{"arg1", 0, 134217818, 0, &Action_2_t1824_gp_0_0_0_0},
	{"arg2", 1, 134217819, 0, &Action_2_t1824_gp_1_0_0_0},
	{"callback", 2, 134217820, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134217821, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult System.Action`2::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
MethodInfo Action_2_BeginInvoke_m7781_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_2_t1824_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_2_t1824_Action_2_BeginInvoke_m7781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Action_2_t1824_Action_2_EndInvoke_m7782_ParameterInfos[] = 
{
	{"result", 0, 134217822, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`2::EndInvoke(System.IAsyncResult)
MethodInfo Action_2_EndInvoke_m7782_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_2_t1824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_2_t1824_Action_2_EndInvoke_m7782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Action_2_t1824_MethodInfos[] =
{
	&Action_2__ctor_m7779_MethodInfo,
	&Action_2_Invoke_m7780_MethodInfo,
	&Action_2_BeginInvoke_m7781_MethodInfo,
	&Action_2_EndInvoke_m7782_MethodInfo,
	NULL
};
extern MethodInfo Action_2_Invoke_m7780_MethodInfo;
extern MethodInfo Action_2_BeginInvoke_m7781_MethodInfo;
extern MethodInfo Action_2_EndInvoke_m7782_MethodInfo;
static Il2CppMethodReference Action_2_t1824_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Action_2_Invoke_m7780_MethodInfo,
	&Action_2_BeginInvoke_m7781_MethodInfo,
	&Action_2_EndInvoke_m7782_MethodInfo,
};
static bool Action_2_t1824_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_2_t1824_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Action_2_t1824_0_0_0;
extern Il2CppType Action_2_t1824_1_0_0;
struct Action_2_t1824;
const Il2CppTypeDefinitionMetadata Action_2_t1824_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_2_t1824_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Action_2_t1824_VTable/* vtableMethods */
	, Action_2_t1824_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Action_2_t1824_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`2"/* name */
	, "System"/* namespaze */
	, Action_2_t1824_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Action_2_t1824_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_2_t1824_0_0_0/* byval_arg */
	, &Action_2_t1824_1_0_0/* this_arg */
	, &Action_2_t1824_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_2_t1824_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`3
extern TypeInfo Action_3_t1825_il2cpp_TypeInfo;
extern Il2CppGenericContainer Action_3_t1825_Il2CppGenericContainer;
extern TypeInfo Action_3_t1825_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_3_t1825_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &Action_3_t1825_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo Action_3_t1825_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_3_t1825_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &Action_3_t1825_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo Action_3_t1825_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_3_t1825_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &Action_3_t1825_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* Action_3_t1825_Il2CppGenericParametersArray[3] = 
{
	&Action_3_t1825_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Action_3_t1825_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Action_3_t1825_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Action_3_t1825_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Action_3_t1825_il2cpp_TypeInfo, 3, 0, Action_3_t1825_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Action_3_t1825_Action_3__ctor_m7783_ParameterInfos[] = 
{
	{"object", 0, 134217823, 0, &Object_t_0_0_0},
	{"method", 1, 134217824, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`3::.ctor(System.Object,System.IntPtr)
MethodInfo Action_3__ctor_m7783_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_3_t1825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_3_t1825_Action_3__ctor_m7783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_3_t1825_gp_0_0_0_0;
extern Il2CppType Action_3_t1825_gp_0_0_0_0;
extern Il2CppType Action_3_t1825_gp_1_0_0_0;
extern Il2CppType Action_3_t1825_gp_1_0_0_0;
extern Il2CppType Action_3_t1825_gp_2_0_0_0;
extern Il2CppType Action_3_t1825_gp_2_0_0_0;
static ParameterInfo Action_3_t1825_Action_3_Invoke_m7784_ParameterInfos[] = 
{
	{"arg1", 0, 134217825, 0, &Action_3_t1825_gp_0_0_0_0},
	{"arg2", 1, 134217826, 0, &Action_3_t1825_gp_1_0_0_0},
	{"arg3", 2, 134217827, 0, &Action_3_t1825_gp_2_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`3::Invoke(T1,T2,T3)
MethodInfo Action_3_Invoke_m7784_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_3_t1825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_3_t1825_Action_3_Invoke_m7784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_3_t1825_gp_0_0_0_0;
extern Il2CppType Action_3_t1825_gp_1_0_0_0;
extern Il2CppType Action_3_t1825_gp_2_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_3_t1825_Action_3_BeginInvoke_m7785_ParameterInfos[] = 
{
	{"arg1", 0, 134217828, 0, &Action_3_t1825_gp_0_0_0_0},
	{"arg2", 1, 134217829, 0, &Action_3_t1825_gp_1_0_0_0},
	{"arg3", 2, 134217830, 0, &Action_3_t1825_gp_2_0_0_0},
	{"callback", 3, 134217831, 0, &AsyncCallback_t20_0_0_0},
	{"object", 4, 134217832, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult System.Action`3::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
MethodInfo Action_3_BeginInvoke_m7785_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_3_t1825_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_3_t1825_Action_3_BeginInvoke_m7785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Action_3_t1825_Action_3_EndInvoke_m7786_ParameterInfos[] = 
{
	{"result", 0, 134217833, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`3::EndInvoke(System.IAsyncResult)
MethodInfo Action_3_EndInvoke_m7786_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_3_t1825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_3_t1825_Action_3_EndInvoke_m7786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Action_3_t1825_MethodInfos[] =
{
	&Action_3__ctor_m7783_MethodInfo,
	&Action_3_Invoke_m7784_MethodInfo,
	&Action_3_BeginInvoke_m7785_MethodInfo,
	&Action_3_EndInvoke_m7786_MethodInfo,
	NULL
};
extern MethodInfo Action_3_Invoke_m7784_MethodInfo;
extern MethodInfo Action_3_BeginInvoke_m7785_MethodInfo;
extern MethodInfo Action_3_EndInvoke_m7786_MethodInfo;
static Il2CppMethodReference Action_3_t1825_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Action_3_Invoke_m7784_MethodInfo,
	&Action_3_BeginInvoke_m7785_MethodInfo,
	&Action_3_EndInvoke_m7786_MethodInfo,
};
static bool Action_3_t1825_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_3_t1825_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Action_3_t1825_0_0_0;
extern Il2CppType Action_3_t1825_1_0_0;
struct Action_3_t1825;
const Il2CppTypeDefinitionMetadata Action_3_t1825_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_3_t1825_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Action_3_t1825_VTable/* vtableMethods */
	, Action_3_t1825_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Action_3_t1825_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`3"/* name */
	, "System"/* namespaze */
	, Action_3_t1825_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Action_3_t1825_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_3_t1825_0_0_0/* byval_arg */
	, &Action_3_t1825_1_0_0/* this_arg */
	, &Action_3_t1825_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_3_t1825_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`4
extern TypeInfo Action_4_t1826_il2cpp_TypeInfo;
extern Il2CppGenericContainer Action_4_t1826_Il2CppGenericContainer;
extern TypeInfo Action_4_t1826_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_4_t1826_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &Action_4_t1826_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo Action_4_t1826_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_4_t1826_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &Action_4_t1826_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo Action_4_t1826_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_4_t1826_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &Action_4_t1826_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
extern TypeInfo Action_4_t1826_gp_T4_3_il2cpp_TypeInfo;
Il2CppGenericParamFull Action_4_t1826_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { { &Action_4_t1826_Il2CppGenericContainer, 3}, {NULL, "T4", 0, 0, NULL} };
static Il2CppGenericParamFull* Action_4_t1826_Il2CppGenericParametersArray[4] = 
{
	&Action_4_t1826_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Action_4_t1826_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Action_4_t1826_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&Action_4_t1826_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Action_4_t1826_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Action_4_t1826_il2cpp_TypeInfo, 4, 0, Action_4_t1826_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Action_4_t1826_Action_4__ctor_m7787_ParameterInfos[] = 
{
	{"object", 0, 134217834, 0, &Object_t_0_0_0},
	{"method", 1, 134217835, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`4::.ctor(System.Object,System.IntPtr)
MethodInfo Action_4__ctor_m7787_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_4_t1826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_4_t1826_Action_4__ctor_m7787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_4_t1826_gp_0_0_0_0;
extern Il2CppType Action_4_t1826_gp_0_0_0_0;
extern Il2CppType Action_4_t1826_gp_1_0_0_0;
extern Il2CppType Action_4_t1826_gp_1_0_0_0;
extern Il2CppType Action_4_t1826_gp_2_0_0_0;
extern Il2CppType Action_4_t1826_gp_2_0_0_0;
extern Il2CppType Action_4_t1826_gp_3_0_0_0;
extern Il2CppType Action_4_t1826_gp_3_0_0_0;
static ParameterInfo Action_4_t1826_Action_4_Invoke_m7788_ParameterInfos[] = 
{
	{"arg1", 0, 134217836, 0, &Action_4_t1826_gp_0_0_0_0},
	{"arg2", 1, 134217837, 0, &Action_4_t1826_gp_1_0_0_0},
	{"arg3", 2, 134217838, 0, &Action_4_t1826_gp_2_0_0_0},
	{"arg4", 3, 134217839, 0, &Action_4_t1826_gp_3_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`4::Invoke(T1,T2,T3,T4)
MethodInfo Action_4_Invoke_m7788_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_4_t1826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_4_t1826_Action_4_Invoke_m7788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_4_t1826_gp_0_0_0_0;
extern Il2CppType Action_4_t1826_gp_1_0_0_0;
extern Il2CppType Action_4_t1826_gp_2_0_0_0;
extern Il2CppType Action_4_t1826_gp_3_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_4_t1826_Action_4_BeginInvoke_m7789_ParameterInfos[] = 
{
	{"arg1", 0, 134217840, 0, &Action_4_t1826_gp_0_0_0_0},
	{"arg2", 1, 134217841, 0, &Action_4_t1826_gp_1_0_0_0},
	{"arg3", 2, 134217842, 0, &Action_4_t1826_gp_2_0_0_0},
	{"arg4", 3, 134217843, 0, &Action_4_t1826_gp_3_0_0_0},
	{"callback", 4, 134217844, 0, &AsyncCallback_t20_0_0_0},
	{"object", 5, 134217845, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult System.Action`4::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
MethodInfo Action_4_BeginInvoke_m7789_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_4_t1826_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_4_t1826_Action_4_BeginInvoke_m7789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Action_4_t1826_Action_4_EndInvoke_m7790_ParameterInfos[] = 
{
	{"result", 0, 134217846, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Action`4::EndInvoke(System.IAsyncResult)
MethodInfo Action_4_EndInvoke_m7790_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_4_t1826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_4_t1826_Action_4_EndInvoke_m7790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Action_4_t1826_MethodInfos[] =
{
	&Action_4__ctor_m7787_MethodInfo,
	&Action_4_Invoke_m7788_MethodInfo,
	&Action_4_BeginInvoke_m7789_MethodInfo,
	&Action_4_EndInvoke_m7790_MethodInfo,
	NULL
};
extern MethodInfo Action_4_Invoke_m7788_MethodInfo;
extern MethodInfo Action_4_BeginInvoke_m7789_MethodInfo;
extern MethodInfo Action_4_EndInvoke_m7790_MethodInfo;
static Il2CppMethodReference Action_4_t1826_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Action_4_Invoke_m7788_MethodInfo,
	&Action_4_BeginInvoke_m7789_MethodInfo,
	&Action_4_EndInvoke_m7790_MethodInfo,
};
static bool Action_4_t1826_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_4_t1826_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Action_4_t1826_0_0_0;
extern Il2CppType Action_4_t1826_1_0_0;
struct Action_4_t1826;
const Il2CppTypeDefinitionMetadata Action_4_t1826_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_4_t1826_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Action_4_t1826_VTable/* vtableMethods */
	, Action_4_t1826_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Action_4_t1826_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`4"/* name */
	, "System"/* namespaze */
	, Action_4_t1826_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Action_4_t1826_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_4_t1826_0_0_0/* byval_arg */
	, &Action_4_t1826_1_0_0/* this_arg */
	, &Action_4_t1826_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_4_t1826_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t1827_il2cpp_TypeInfo;
extern Il2CppGenericContainer Func_2_t1827_Il2CppGenericContainer;
extern TypeInfo Func_2_t1827_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_2_t1827_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Func_2_t1827_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
extern TypeInfo Func_2_t1827_gp_TResult_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_2_t1827_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { { &Func_2_t1827_Il2CppGenericContainer, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Func_2_t1827_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t1827_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t1827_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Func_2_t1827_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Func_2_t1827_il2cpp_TypeInfo, 2, 0, Func_2_t1827_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Func_2_t1827_Func_2__ctor_m7791_ParameterInfos[] = 
{
	{"object", 0, 134217847, 0, &Object_t_0_0_0},
	{"method", 1, 134217848, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
MethodInfo Func_2__ctor_m7791_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t1827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1827_Func_2__ctor_m7791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_2_t1827_gp_0_0_0_0;
extern Il2CppType Func_2_t1827_gp_0_0_0_0;
static ParameterInfo Func_2_t1827_Func_2_Invoke_m7792_ParameterInfos[] = 
{
	{"arg1", 0, 134217849, 0, &Func_2_t1827_gp_0_0_0_0},
};
extern Il2CppType Func_2_t1827_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
MethodInfo Func_2_Invoke_m7792_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t1827_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1827_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1827_Func_2_Invoke_m7792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_2_t1827_gp_0_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t1827_Func_2_BeginInvoke_m7793_ParameterInfos[] = 
{
	{"arg1", 0, 134217850, 0, &Func_2_t1827_gp_0_0_0_0},
	{"callback", 1, 134217851, 0, &AsyncCallback_t20_0_0_0},
	{"object", 2, 134217852, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Func_2_BeginInvoke_m7793_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1827_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1827_Func_2_BeginInvoke_m7793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Func_2_t1827_Func_2_EndInvoke_m7794_ParameterInfos[] = 
{
	{"result", 0, 134217853, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Func_2_t1827_gp_1_0_0_0;
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
MethodInfo Func_2_EndInvoke_m7794_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1827_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1827_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1827_Func_2_EndInvoke_m7794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Func_2_t1827_MethodInfos[] =
{
	&Func_2__ctor_m7791_MethodInfo,
	&Func_2_Invoke_m7792_MethodInfo,
	&Func_2_BeginInvoke_m7793_MethodInfo,
	&Func_2_EndInvoke_m7794_MethodInfo,
	NULL
};
extern MethodInfo Func_2_Invoke_m7792_MethodInfo;
extern MethodInfo Func_2_BeginInvoke_m7793_MethodInfo;
extern MethodInfo Func_2_EndInvoke_m7794_MethodInfo;
static Il2CppMethodReference Func_2_t1827_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Func_2_Invoke_m7792_MethodInfo,
	&Func_2_BeginInvoke_m7793_MethodInfo,
	&Func_2_EndInvoke_m7794_MethodInfo,
};
static bool Func_2_t1827_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Func_2_t1827_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_2_t1827_0_0_0;
extern Il2CppType Func_2_t1827_1_0_0;
struct Func_2_t1827;
const Il2CppTypeDefinitionMetadata Func_2_t1827_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t1827_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Func_2_t1827_VTable/* vtableMethods */
	, Func_2_t1827_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Func_2_t1827_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t1827_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Func_2_t1827_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t1827_0_0_0/* byval_arg */
	, &Func_2_t1827_1_0_0/* this_arg */
	, &Func_2_t1827_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_2_t1827_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Func`3
extern TypeInfo Func_3_t1828_il2cpp_TypeInfo;
extern Il2CppGenericContainer Func_3_t1828_Il2CppGenericContainer;
extern TypeInfo Func_3_t1828_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_3_t1828_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &Func_3_t1828_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo Func_3_t1828_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_3_t1828_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &Func_3_t1828_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo Func_3_t1828_gp_TResult_2_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_3_t1828_gp_TResult_2_il2cpp_TypeInfo_GenericParamFull = { { &Func_3_t1828_Il2CppGenericContainer, 2}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Func_3_t1828_Il2CppGenericParametersArray[3] = 
{
	&Func_3_t1828_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_3_t1828_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Func_3_t1828_gp_TResult_2_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer Func_3_t1828_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Func_3_t1828_il2cpp_TypeInfo, 3, 0, Func_3_t1828_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo Func_3_t1828_Func_3__ctor_m7795_ParameterInfos[] = 
{
	{"object", 0, 134217854, 0, &Object_t_0_0_0},
	{"method", 1, 134217855, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
// System.Void System.Func`3::.ctor(System.Object,System.IntPtr)
MethodInfo Func_3__ctor_m7795_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_3_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_3_t1828_Func_3__ctor_m7795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_3_t1828_gp_0_0_0_0;
extern Il2CppType Func_3_t1828_gp_0_0_0_0;
extern Il2CppType Func_3_t1828_gp_1_0_0_0;
extern Il2CppType Func_3_t1828_gp_1_0_0_0;
static ParameterInfo Func_3_t1828_Func_3_Invoke_m7796_ParameterInfos[] = 
{
	{"arg1", 0, 134217856, 0, &Func_3_t1828_gp_0_0_0_0},
	{"arg2", 1, 134217857, 0, &Func_3_t1828_gp_1_0_0_0},
};
extern Il2CppType Func_3_t1828_gp_2_0_0_0;
// TResult System.Func`3::Invoke(T1,T2)
MethodInfo Func_3_Invoke_m7796_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_3_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Func_3_t1828_gp_2_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_3_t1828_Func_3_Invoke_m7796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_3_t1828_gp_0_0_0_0;
extern Il2CppType Func_3_t1828_gp_1_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_3_t1828_Func_3_BeginInvoke_m7797_ParameterInfos[] = 
{
	{"arg1", 0, 134217858, 0, &Func_3_t1828_gp_0_0_0_0},
	{"arg2", 1, 134217859, 0, &Func_3_t1828_gp_1_0_0_0},
	{"callback", 2, 134217860, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134217861, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
// System.IAsyncResult System.Func`3::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
MethodInfo Func_3_BeginInvoke_m7797_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_3_t1828_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_3_t1828_Func_3_BeginInvoke_m7797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo Func_3_t1828_Func_3_EndInvoke_m7798_ParameterInfos[] = 
{
	{"result", 0, 134217862, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Func_3_t1828_gp_2_0_0_0;
// TResult System.Func`3::EndInvoke(System.IAsyncResult)
MethodInfo Func_3_EndInvoke_m7798_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_3_t1828_il2cpp_TypeInfo/* declaring_type */
	, &Func_3_t1828_gp_2_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_3_t1828_Func_3_EndInvoke_m7798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Func_3_t1828_MethodInfos[] =
{
	&Func_3__ctor_m7795_MethodInfo,
	&Func_3_Invoke_m7796_MethodInfo,
	&Func_3_BeginInvoke_m7797_MethodInfo,
	&Func_3_EndInvoke_m7798_MethodInfo,
	NULL
};
extern MethodInfo Func_3_Invoke_m7796_MethodInfo;
extern MethodInfo Func_3_BeginInvoke_m7797_MethodInfo;
extern MethodInfo Func_3_EndInvoke_m7798_MethodInfo;
static Il2CppMethodReference Func_3_t1828_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&Func_3_Invoke_m7796_MethodInfo,
	&Func_3_BeginInvoke_m7797_MethodInfo,
	&Func_3_EndInvoke_m7798_MethodInfo,
};
static bool Func_3_t1828_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Func_3_t1828_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_3_t1828_0_0_0;
extern Il2CppType Func_3_t1828_1_0_0;
struct Func_3_t1828;
const Il2CppTypeDefinitionMetadata Func_3_t1828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_3_t1828_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, Func_3_t1828_VTable/* vtableMethods */
	, Func_3_t1828_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Func_3_t1828_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`3"/* name */
	, "System"/* namespaze */
	, Func_3_t1828_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Func_3_t1828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_3_t1828_0_0_0/* byval_arg */
	, &Func_3_t1828_1_0_0/* this_arg */
	, &Func_3_t1828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_3_t1828_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t1804_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"
static MethodInfo* U24ArrayTypeU24136_t1804_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU24136_t1804_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU24136_t1804_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U24ArrayTypeU24136_t1804_0_0_0;
extern Il2CppType U24ArrayTypeU24136_t1804_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t1805_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t1804_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU24136_t1804_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t1804_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU24136_t1804_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t1804_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU24136_t1804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t1804_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t1804_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t1804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t1804_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1804_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1804_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t1804)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t1804)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t1804_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static MethodInfo* U3CPrivateImplementationDetailsU3E_t1805_MethodInfos[] =
{
	NULL
};
extern Il2CppType U24ArrayTypeU24136_t1804_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_FieldInfo = 
{
	"$$field-0"/* name */
	, &U24ArrayTypeU24136_t1804_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t1805_StaticFields, ___U24U24fieldU2D0_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3E_t1805_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_DefaultValueData[] = { 0xB, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x67, 0x18, 0x0, 0x0, 0x9B, 0x24, 0x0, 0x0, 0xE9, 0x36, 0x0, 0x0, 0x61, 0x52, 0x0, 0x0, 0x8B, 0x7B, 0x0, 0x0, 0x47, 0xB9, 0x0, 0x0, 0xE7, 0x15, 0x1, 0x0, 0xE1, 0xA0, 0x1, 0x0, 0x49, 0x71, 0x2, 0x0, 0xE5, 0xA9, 0x3, 0x0, 0xE3, 0x7E, 0x5, 0x0, 0x39, 0x3E, 0x8, 0x0, 0x67, 0x5D, 0xC, 0x0, 0x9, 0x8C, 0x12, 0x0, 0xFF, 0xD1, 0x1B, 0x0, 0x13, 0xBB, 0x29, 0x0, 0x8B, 0x98, 0x3E, 0x0, 0xC1, 0xE4, 0x5D, 0x0, 0x21, 0xD7, 0x8C, 0x0, 0xAB, 0x42, 0xD3, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_DefaultValueData, &U24ArrayTypeU24136_t1804_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3E_t1805_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3E_t1805____U24U24fieldU2D0_0_DefaultValue,
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U24ArrayTypeU24136_t1804_0_0_0,
};
static Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1805_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1805_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t1805_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1805;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1805_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1805_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1805_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3E_t1805_FieldInfos/* fields */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 52/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1805_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3E_t1805_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1805)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1805)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1805_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
