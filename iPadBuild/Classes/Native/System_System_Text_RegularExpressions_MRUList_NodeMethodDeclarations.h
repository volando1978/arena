﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MRUList/Node
struct Node_t2074;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
extern "C" void Node__ctor_m8379 (Node_t2074 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
