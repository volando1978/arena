﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718;

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>::.ctor()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19728_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19728(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19728_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>::<>m__F()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19729_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19729(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19729_gshared)(__this, method)
