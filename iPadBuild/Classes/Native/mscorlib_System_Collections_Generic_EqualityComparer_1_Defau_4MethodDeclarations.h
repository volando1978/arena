﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>
struct DefaultComparer_t3813;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::.ctor()
extern "C" void DefaultComparer__ctor_m20856_gshared (DefaultComparer_t3813 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m20856(__this, method) (( void (*) (DefaultComparer_t3813 *, MethodInfo*))DefaultComparer__ctor_m20856_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m20857_gshared (DefaultComparer_t3813 * __this, IntPtr_t ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m20857(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3813 *, IntPtr_t, MethodInfo*))DefaultComparer_GetHashCode_m20857_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.IntPtr>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m20858_gshared (DefaultComparer_t3813 * __this, IntPtr_t ___x, IntPtr_t ___y, MethodInfo* method);
#define DefaultComparer_Equals_m20858(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3813 *, IntPtr_t, IntPtr_t, MethodInfo*))DefaultComparer_Equals_m20858_gshared)(__this, ___x, ___y, method)
