﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t1520;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct  KeyValuePair_2_t4067 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::value
	LayoutCache_t1520 * ___value_1;
};
