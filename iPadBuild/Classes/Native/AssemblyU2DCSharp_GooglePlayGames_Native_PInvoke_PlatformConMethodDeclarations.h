﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PlatformConfiguration
struct PlatformConfiguration_t669;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.PlatformConfiguration::.ctor(System.IntPtr)
extern "C" void PlatformConfiguration__ctor_m2868 (PlatformConfiguration_t669 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.PlatformConfiguration::AsHandle()
extern "C" HandleRef_t657  PlatformConfiguration_AsHandle_m2869 (PlatformConfiguration_t669 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
