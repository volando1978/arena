﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37
struct U3CAcceptInvitationU3Ec__AnonStorey37_t605;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey37__ctor_m2469 (U3CAcceptInvitationU3Ec__AnonStorey37_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
