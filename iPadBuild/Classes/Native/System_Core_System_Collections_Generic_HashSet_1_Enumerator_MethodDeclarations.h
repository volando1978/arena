﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct Enumerator_t3582;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3580;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m17650_gshared (Enumerator_t3582 * __this, HashSet_1_t3580 * ___hashset, MethodInfo* method);
#define Enumerator__ctor_m17650(__this, ___hashset, method) (( void (*) (Enumerator_t3582 *, HashSet_1_t3580 *, MethodInfo*))Enumerator__ctor_m17650_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared (Enumerator_t3582 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17651(__this, method) (( Object_t * (*) (Enumerator_t3582 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17652_gshared (Enumerator_t3582 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m17652(__this, method) (( bool (*) (Enumerator_t3582 *, MethodInfo*))Enumerator_MoveNext_m17652_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17653_gshared (Enumerator_t3582 * __this, MethodInfo* method);
#define Enumerator_get_Current_m17653(__this, method) (( Object_t * (*) (Enumerator_t3582 *, MethodInfo*))Enumerator_get_Current_m17653_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17654_gshared (Enumerator_t3582 * __this, MethodInfo* method);
#define Enumerator_Dispose_m17654(__this, method) (( void (*) (Enumerator_t3582 *, MethodInfo*))Enumerator_Dispose_m17654_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m17655_gshared (Enumerator_t3582 * __this, MethodInfo* method);
#define Enumerator_CheckState_m17655(__this, method) (( void (*) (Enumerator_t3582 *, MethodInfo*))Enumerator_CheckState_m17655_gshared)(__this, method)
