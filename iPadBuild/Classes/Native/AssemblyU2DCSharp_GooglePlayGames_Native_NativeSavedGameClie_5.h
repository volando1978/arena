﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t621;
// GooglePlayGames.Native.NativeSavedGameClient
struct NativeSavedGameClient_t624;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45
struct  U3CInternalManualOpenU3Ec__AnonStorey45_t622  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata> GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::completedCallback
	Action_2_t612 * ___completedCallback_0;
	// System.String GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::filename
	String_t* ___filename_1;
	// GooglePlayGames.BasicApi.DataSource GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::source
	int32_t ___source_2;
	// System.Boolean GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::prefetchDataOnConflict
	bool ___prefetchDataOnConflict_3;
	// GooglePlayGames.BasicApi.SavedGame.ConflictCallback GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::conflictCallback
	ConflictCallback_t621 * ___conflictCallback_4;
	// GooglePlayGames.Native.NativeSavedGameClient GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::<>f__this
	NativeSavedGameClient_t624 * ___U3CU3Ef__this_5;
};
