﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t4170;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Byte>
struct KeyCollection_t4173;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Byte>
struct ValueCollection_t4177;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3482;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct KeyValuePair_2U5BU5D_t4535;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>
struct IEnumerator_1_t4536;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__28.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor()
extern "C" void Dictionary_2__ctor_m25527_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2__ctor_m25527(__this, method) (( void (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2__ctor_m25527_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m25528_gshared (Dictionary_2_t4170 * __this, Object_t* ___comparer, MethodInfo* method);
#define Dictionary_2__ctor_m25528(__this, ___comparer, method) (( void (*) (Dictionary_2_t4170 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m25528_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m25530_gshared (Dictionary_2_t4170 * __this, int32_t ___capacity, MethodInfo* method);
#define Dictionary_2__ctor_m25530(__this, ___capacity, method) (( void (*) (Dictionary_2_t4170 *, int32_t, MethodInfo*))Dictionary_2__ctor_m25530_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m25532_gshared (Dictionary_2_t4170 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2__ctor_m25532(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t4170 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m25532_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m25534_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m25534(__this, method) (( Object_t * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m25534_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m25536_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25536(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25536_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25538_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25538(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25538_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25540_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m25540(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25540_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25542_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m25542(__this, ___key, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25542_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25544_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25544(__this, method) (( bool (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25544_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25546_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25546(__this, method) (( Object_t * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25546_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25548_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25548(__this, method) (( bool (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25550_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2_t4171  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25550(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t4170 *, KeyValuePair_2_t4171 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25550_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25552_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2_t4171  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25552(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t4170 *, KeyValuePair_2_t4171 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25552_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25554_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2U5BU5D_t4535* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25554(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t4170 *, KeyValuePair_2U5BU5D_t4535*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25554_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25556_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2_t4171  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25556(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t4170 *, KeyValuePair_2_t4171 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25556_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25558_gshared (Dictionary_2_t4170 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25558(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t4170 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25558_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25560_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25560(__this, method) (( Object_t * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25560_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25562_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25562(__this, method) (( Object_t* (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25562_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25564_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25564(__this, method) (( Object_t * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25564_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m25566_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_get_Count_m25566(__this, method) (( int32_t (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_get_Count_m25566_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Item(TKey)
extern "C" uint8_t Dictionary_2_get_Item_m25568_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_get_Item_m25568(__this, ___key, method) (( uint8_t (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_get_Item_m25568_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m25570_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_set_Item_m25570(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, uint8_t, MethodInfo*))Dictionary_2_set_Item_m25570_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m25572_gshared (Dictionary_2_t4170 * __this, int32_t ___capacity, Object_t* ___hcp, MethodInfo* method);
#define Dictionary_2_Init_m25572(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t4170 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m25572_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m25574_gshared (Dictionary_2_t4170 * __this, int32_t ___size, MethodInfo* method);
#define Dictionary_2_InitArrays_m25574(__this, ___size, method) (( void (*) (Dictionary_2_t4170 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m25574_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m25576_gshared (Dictionary_2_t4170 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyToCheck_m25576(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t4170 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m25576_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t4171  Dictionary_2_make_pair_m25578_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_make_pair_m25578(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t4171  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, MethodInfo*))Dictionary_2_make_pair_m25578_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m25580_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_pick_key_m25580(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint8_t, MethodInfo*))Dictionary_2_pick_key_m25580_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_value(TKey,TValue)
extern "C" uint8_t Dictionary_2_pick_value_m25582_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_pick_value_m25582(__this /* static, unused */, ___key, ___value, method) (( uint8_t (*) (Object_t * /* static, unused */, Object_t *, uint8_t, MethodInfo*))Dictionary_2_pick_value_m25582_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m25584_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2U5BU5D_t4535* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyTo_m25584(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t4170 *, KeyValuePair_2U5BU5D_t4535*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m25584_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Resize()
extern "C" void Dictionary_2_Resize_m25586_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_Resize_m25586(__this, method) (( void (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_Resize_m25586_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m25588_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_Add_m25588(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, uint8_t, MethodInfo*))Dictionary_2_Add_m25588_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Clear()
extern "C" void Dictionary_2_Clear_m25590_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_Clear_m25590(__this, method) (( void (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_Clear_m25590_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m25592_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ContainsKey_m25592(__this, ___key, method) (( bool (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_ContainsKey_m25592_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m25594_gshared (Dictionary_2_t4170 * __this, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_ContainsValue_m25594(__this, ___value, method) (( bool (*) (Dictionary_2_t4170 *, uint8_t, MethodInfo*))Dictionary_2_ContainsValue_m25594_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m25596_gshared (Dictionary_2_t4170 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2_GetObjectData_m25596(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t4170 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m25596_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m25598_gshared (Dictionary_2_t4170 * __this, Object_t * ___sender, MethodInfo* method);
#define Dictionary_2_OnDeserialization_m25598(__this, ___sender, method) (( void (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m25598_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m25600_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_Remove_m25600(__this, ___key, method) (( bool (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_Remove_m25600_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m25602_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, uint8_t* ___value, MethodInfo* method);
#define Dictionary_2_TryGetValue_m25602(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t4170 *, Object_t *, uint8_t*, MethodInfo*))Dictionary_2_TryGetValue_m25602_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Keys()
extern "C" KeyCollection_t4173 * Dictionary_2_get_Keys_m25604_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_get_Keys_m25604(__this, method) (( KeyCollection_t4173 * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_get_Keys_m25604_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Values()
extern "C" ValueCollection_t4177 * Dictionary_2_get_Values_m25606_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_get_Values_m25606(__this, method) (( ValueCollection_t4177 * (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_get_Values_m25606_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m25608_gshared (Dictionary_2_t4170 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ToTKey_m25608(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m25608_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTValue(System.Object)
extern "C" uint8_t Dictionary_2_ToTValue_m25610_gshared (Dictionary_2_t4170 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ToTValue_m25610(__this, ___value, method) (( uint8_t (*) (Dictionary_2_t4170 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m25610_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m25612_gshared (Dictionary_2_t4170 * __this, KeyValuePair_2_t4171  ___pair, MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m25612(__this, ___pair, method) (( bool (*) (Dictionary_2_t4170 *, KeyValuePair_2_t4171 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25612_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetEnumerator()
extern "C" Enumerator_t4175  Dictionary_2_GetEnumerator_m25614_gshared (Dictionary_2_t4170 * __this, MethodInfo* method);
#define Dictionary_2_GetEnumerator_m25614(__this, method) (( Enumerator_t4175  (*) (Dictionary_2_t4170 *, MethodInfo*))Dictionary_2_GetEnumerator_m25614_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2128  Dictionary_2_U3CCopyToU3Em__0_m25616_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m25616(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25616_gshared)(__this /* static, unused */, ___key, ___value, method)
