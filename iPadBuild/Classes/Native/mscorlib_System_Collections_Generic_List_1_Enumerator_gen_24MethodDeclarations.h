﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>
struct Enumerator_t3733;
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
struct List_1_t868;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m19862(__this, ___l, method) (( void (*) (Enumerator_t3733 *, List_1_t868 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19863(__this, method) (( Object_t * (*) (Enumerator_t3733 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::Dispose()
#define Enumerator_Dispose_m19864(__this, method) (( void (*) (Enumerator_t3733 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::VerifyState()
#define Enumerator_VerifyState_m19865(__this, method) (( void (*) (Enumerator_t3733 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::MoveNext()
#define Enumerator_MoveNext_m19866(__this, method) (( bool (*) (Enumerator_t3733 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::get_Current()
#define Enumerator_get_Current_m19867(__this, method) (( NativeEvent_t674 * (*) (Enumerator_t3733 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
