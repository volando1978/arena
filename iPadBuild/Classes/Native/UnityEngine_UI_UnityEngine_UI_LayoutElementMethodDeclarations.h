﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.LayoutElement
struct LayoutElement_t1320;

// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern "C" void LayoutElement__ctor_m5596 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern "C" bool LayoutElement_get_ignoreLayout_m5597 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern "C" void LayoutElement_set_ignoreLayout_m5598 (LayoutElement_t1320 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m5599 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern "C" void LayoutElement_CalculateLayoutInputVertical_m5600 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern "C" float LayoutElement_get_minWidth_m5601 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern "C" void LayoutElement_set_minWidth_m5602 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern "C" float LayoutElement_get_minHeight_m5603 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern "C" void LayoutElement_set_minHeight_m5604 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern "C" float LayoutElement_get_preferredWidth_m5605 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern "C" void LayoutElement_set_preferredWidth_m5606 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern "C" float LayoutElement_get_preferredHeight_m5607 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern "C" void LayoutElement_set_preferredHeight_m5608 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern "C" float LayoutElement_get_flexibleWidth_m5609 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern "C" void LayoutElement_set_flexibleWidth_m5610 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern "C" float LayoutElement_get_flexibleHeight_m5611 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern "C" void LayoutElement_set_flexibleHeight_m5612 (LayoutElement_t1320 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern "C" int32_t LayoutElement_get_layoutPriority_m5613 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern "C" void LayoutElement_OnEnable_m5614 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern "C" void LayoutElement_OnTransformParentChanged_m5615 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern "C" void LayoutElement_OnDisable_m5616 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m5617 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m5618 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern "C" void LayoutElement_SetDirty_m5619 (LayoutElement_t1320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
