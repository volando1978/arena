﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
struct Builder_t365;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"

// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::.ctor()
extern "C" void Builder__ctor_m1465 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableSavedGames()
extern "C" Builder_t365 * Builder_EnableSavedGames_m1466 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::EnableDeprecatedCloudSave()
extern "C" Builder_t365 * Builder_EnableDeprecatedCloudSave_m1467 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C" Builder_t365 * Builder_WithInvitationDelegate_m1468 (Builder_t365 * __this, InvitationReceivedDelegate_t363 * ___invitationDelegate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::WithMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate)
extern "C" Builder_t365 * Builder_WithMatchDelegate_m1469 (Builder_t365 * __this, MatchDelegate_t364 * ___matchDelegate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableSaveGames()
extern "C" bool Builder_HasEnableSaveGames_m1470 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::HasEnableDeprecatedCloudSave()
extern "C" bool Builder_HasEnableDeprecatedCloudSave_m1471 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetMatchDelegate()
extern "C" MatchDelegate_t364 * Builder_GetMatchDelegate_m1472 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::GetInvitationDelegate()
extern "C" InvitationReceivedDelegate_t363 * Builder_GetInvitationDelegate_m1473 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::Build()
extern "C" PlayGamesClientConfiguration_t366  Builder_Build_m1474 (Builder_t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mInvitationDelegate>m__1(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void Builder_U3CmInvitationDelegateU3Em__1_m1475 (Object_t * __this /* static, unused */, Invitation_t341 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<mMatchDelegate>m__2(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void Builder_U3CmMatchDelegateU3Em__2_m1476 (Object_t * __this /* static, unused */, TurnBasedMatch_t353 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
