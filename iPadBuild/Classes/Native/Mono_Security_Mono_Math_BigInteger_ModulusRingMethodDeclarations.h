﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t2192;
// Mono.Math.BigInteger
struct BigInteger_t2191;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m8993 (ModulusRing_t2192 * __this, BigInteger_t2191 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m8994 (ModulusRing_t2192 * __this, BigInteger_t2191 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2191 * ModulusRing_Multiply_m8995 (ModulusRing_t2192 * __this, BigInteger_t2191 * ___a, BigInteger_t2191 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2191 * ModulusRing_Difference_m8996 (ModulusRing_t2192 * __this, BigInteger_t2191 * ___a, BigInteger_t2191 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2191 * ModulusRing_Pow_m8997 (ModulusRing_t2192 * __this, BigInteger_t2191 * ___a, BigInteger_t2191 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t2191 * ModulusRing_Pow_m8998 (ModulusRing_t2192 * __this, uint32_t ___b, BigInteger_t2191 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
