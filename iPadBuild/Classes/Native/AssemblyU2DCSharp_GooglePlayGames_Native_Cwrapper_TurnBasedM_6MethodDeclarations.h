﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
struct PlayerSelectUICallback_t500;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void PlayerSelectUICallback__ctor_m2195 (PlayerSelectUICallback_t500 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void PlayerSelectUICallback_Invoke_m2196 (PlayerSelectUICallback_t500 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PlayerSelectUICallback_t500(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * PlayerSelectUICallback_BeginInvoke_m2197 (PlayerSelectUICallback_t500 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C" void PlayerSelectUICallback_EndInvoke_m2198 (PlayerSelectUICallback_t500 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
