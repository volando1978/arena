﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t592;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31
struct  U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31::config
	RealtimeRoomConfig_t592 * ___config_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31::<>f__ref$47
	U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598 * ___U3CU3Ef__refU2447_1;
};
