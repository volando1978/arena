﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaEditorScript
struct SoomlaEditorScript_t14;
// System.String
struct String_t;

// System.Void Soomla.SoomlaEditorScript::.ctor()
extern "C" void SoomlaEditorScript__ctor_m21 (SoomlaEditorScript_t14 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SoomlaEditorScript::.cctor()
extern "C" void SoomlaEditorScript__cctor_m22 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.SoomlaEditorScript Soomla.SoomlaEditorScript::get_Instance()
extern "C" SoomlaEditorScript_t14 * SoomlaEditorScript_get_Instance_m23 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SoomlaEditorScript::DirtyEditor()
extern "C" void SoomlaEditorScript_DirtyEditor_m24 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SoomlaEditorScript::setSettingsValue(System.String,System.String)
extern "C" void SoomlaEditorScript_setSettingsValue_m25 (SoomlaEditorScript_t14 * __this, String_t* ___key, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
