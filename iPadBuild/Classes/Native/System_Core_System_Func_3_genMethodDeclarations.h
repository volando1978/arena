﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>
struct Func_3_t3633;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Action`1<System.Object>
struct Action_1_t3422;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
// System.Func`3<System.Object,System.Object,System.Object>
#include "System_Core_System_Func_3_gen_0MethodDeclarations.h"
#define Func_3__ctor_m18531(__this, ___object, ___method, method) (( void (*) (Func_3_t3633 *, Object_t *, IntPtr_t, MethodInfo*))Func_3__ctor_m18532_gshared)(__this, ___object, ___method, method)
// TResult System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>::Invoke(T1,T2)
#define Func_3_Invoke_m18533(__this, ___arg1, ___arg2, method) (( Object_t * (*) (Func_3_t3633 *, Object_t *, Action_1_t3422 *, MethodInfo*))Func_3_Invoke_m18534_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m18535(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Func_3_t3633 *, Object_t *, Action_1_t3422 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_3_BeginInvoke_m18536_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// TResult System.Func`3<System.Object,System.Action`1<System.Object>,System.Collections.IEnumerator>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m18537(__this, ___result, method) (( Object_t * (*) (Func_3_t3633 *, Object_t *, MethodInfo*))Func_3_EndInvoke_m18538_gshared)(__this, ___result, method)
