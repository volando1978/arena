﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.Object
struct Object_t;

// System.Void UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::.ctor()
extern "C" void UnityKeyValuePair_2__ctor_m15545_gshared (UnityKeyValuePair_2_t3412 * __this, MethodInfo* method);
#define UnityKeyValuePair_2__ctor_m15545(__this, method) (( void (*) (UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityKeyValuePair_2__ctor_m15545_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::.ctor(K,V)
extern "C" void UnityKeyValuePair_2__ctor_m15546_gshared (UnityKeyValuePair_2_t3412 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define UnityKeyValuePair_2__ctor_m15546(__this, ___key, ___value, method) (( void (*) (UnityKeyValuePair_2_t3412 *, Object_t *, Object_t *, MethodInfo*))UnityKeyValuePair_2__ctor_m15546_gshared)(__this, ___key, ___value, method)
// K UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * UnityKeyValuePair_2_get_Key_m15547_gshared (UnityKeyValuePair_2_t3412 * __this, MethodInfo* method);
#define UnityKeyValuePair_2_get_Key_m15547(__this, method) (( Object_t * (*) (UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityKeyValuePair_2_get_Key_m15547_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::set_Key(K)
extern "C" void UnityKeyValuePair_2_set_Key_m15548_gshared (UnityKeyValuePair_2_t3412 * __this, Object_t * ___value, MethodInfo* method);
#define UnityKeyValuePair_2_set_Key_m15548(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t3412 *, Object_t *, MethodInfo*))UnityKeyValuePair_2_set_Key_m15548_gshared)(__this, ___value, method)
// V UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * UnityKeyValuePair_2_get_Value_m15549_gshared (UnityKeyValuePair_2_t3412 * __this, MethodInfo* method);
#define UnityKeyValuePair_2_get_Value_m15549(__this, method) (( Object_t * (*) (UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityKeyValuePair_2_get_Value_m15549_gshared)(__this, method)
// System.Void UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>::set_Value(V)
extern "C" void UnityKeyValuePair_2_set_Value_m15550_gshared (UnityKeyValuePair_2_t3412 * __this, Object_t * ___value, MethodInfo* method);
#define UnityKeyValuePair_2_set_Value_m15550(__this, ___value, method) (( void (*) (UnityKeyValuePair_2_t3412 *, Object_t *, MethodInfo*))UnityKeyValuePair_2_set_Value_m15550_gshared)(__this, ___value, method)
