﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t1231;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t26;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m5866_gshared (TweenRunner_1_t1231 * __this, MethodInfo* method);
#define TweenRunner_1__ctor_m5866(__this, method) (( void (*) (TweenRunner_1_t1231 *, MethodInfo*))TweenRunner_1__ctor_m5866_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m23049_gshared (Object_t * __this /* static, unused */, ColorTween_t1209  ___tweenInfo, MethodInfo* method);
#define TweenRunner_1_Start_m23049(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t1209 , MethodInfo*))TweenRunner_1_Start_m23049_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m5867_gshared (TweenRunner_1_t1231 * __this, MonoBehaviour_t26 * ___coroutineContainer, MethodInfo* method);
#define TweenRunner_1_Init_m5867(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t1231 *, MonoBehaviour_t26 *, MethodInfo*))TweenRunner_1_Init_m5867_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m5889_gshared (TweenRunner_1_t1231 * __this, ColorTween_t1209  ___info, MethodInfo* method);
#define TweenRunner_1_StartTween_m5889(__this, ___info, method) (( void (*) (TweenRunner_1_t1231 *, ColorTween_t1209 , MethodInfo*))TweenRunner_1_StartTween_m5889_gshared)(__this, ___info, method)
