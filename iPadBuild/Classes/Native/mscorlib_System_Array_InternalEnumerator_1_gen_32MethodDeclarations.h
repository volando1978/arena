﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t3940;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22532_gshared (InternalEnumerator_1_t3940 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m22532(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3940 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m22532_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22533_gshared (InternalEnumerator_1_t3940 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22533(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3940 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22533_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22534_gshared (InternalEnumerator_1_t3940 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22534(__this, method) (( void (*) (InternalEnumerator_1_t3940 *, MethodInfo*))InternalEnumerator_1_Dispose_m22534_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22535_gshared (InternalEnumerator_1_t3940 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22535(__this, method) (( bool (*) (InternalEnumerator_1_t3940 *, MethodInfo*))InternalEnumerator_1_MoveNext_m22535_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t1378  InternalEnumerator_1_get_Current_m22536_gshared (InternalEnumerator_1_t3940 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22536(__this, method) (( RaycastHit2D_t1378  (*) (InternalEnumerator_1_t3940 *, MethodInfo*))InternalEnumerator_1_get_Current_m22536_gshared)(__this, method)
