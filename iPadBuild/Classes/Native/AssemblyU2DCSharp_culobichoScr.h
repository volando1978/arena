﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// headScr
struct headScr_t781;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// culobichoScr
struct  culobichoScr_t782  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject culobichoScr::target
	GameObject_t144 * ___target_2;
	// headScr culobichoScr::_target
	headScr_t781 * ____target_3;
	// System.Single culobichoScr::maxSpeed
	float ___maxSpeed_4;
	// System.Single culobichoScr::maxAngleSpeed
	float ___maxAngleSpeed_5;
};
