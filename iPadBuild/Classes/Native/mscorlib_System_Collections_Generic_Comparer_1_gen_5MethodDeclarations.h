﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t3973;
// System.Object
struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Comparer_1__ctor_m23043_gshared (Comparer_1_t3973 * __this, MethodInfo* method);
#define Comparer_1__ctor_m23043(__this, method) (( void (*) (Comparer_1_t3973 *, MethodInfo*))Comparer_1__ctor_m23043_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern "C" void Comparer_1__cctor_m23044_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1__cctor_m23044(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1__cctor_m23044_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m23045_gshared (Comparer_1_t3973 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m23045(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3973 *, Object_t *, Object_t *, MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m23045_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C" Comparer_1_t3973 * Comparer_1_get_Default_m23046_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1_get_Default_m23046(__this /* static, unused */, method) (( Comparer_1_t3973 * (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1_get_Default_m23046_gshared)(__this /* static, unused */, method)
