﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.VirtualGood>
struct List_1_t116;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>
struct  Enumerator_t217 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::l
	List_1_t116 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::current
	VirtualGood_t79 * ___current_3;
};
