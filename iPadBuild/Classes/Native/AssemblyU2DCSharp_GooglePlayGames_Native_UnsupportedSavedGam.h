﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.UnsupportedSavedGamesClient
struct  UnsupportedSavedGamesClient_t714  : public Object_t
{
	// System.String GooglePlayGames.Native.UnsupportedSavedGamesClient::mMessage
	String_t* ___mMessage_0;
};
