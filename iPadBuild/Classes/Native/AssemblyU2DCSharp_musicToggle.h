﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t720;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t721;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// musicToggle
struct  musicToggle_t819  : public MonoBehaviour_t26
{
	// UnityEngine.UI.Toggle musicToggle::tog
	Toggle_t720 * ___tog_2;
};
struct musicToggle_t819_StaticFields{
	// UnityEngine.Events.UnityAction`1<System.Boolean> musicToggle::<>f__am$cache1
	UnityAction_1_t721 * ___U3CU3Ef__amU24cache1_3;
};
