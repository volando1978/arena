﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// newWeaponRoomSrc
struct newWeaponRoomSrc_t821;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// newWeaponRoomSrc/<animSpin>c__Iterator11
struct  U3CanimSpinU3Ec__Iterator11_t822  : public Object_t
{
	// UnityEngine.Vector3 newWeaponRoomSrc/<animSpin>c__Iterator11::<z>__0
	Vector3_t758  ___U3CzU3E__0_0;
	// System.Int32 newWeaponRoomSrc/<animSpin>c__Iterator11::$PC
	int32_t ___U24PC_1;
	// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::$current
	Object_t * ___U24current_2;
	// newWeaponRoomSrc newWeaponRoomSrc/<animSpin>c__Iterator11::<>f__this
	newWeaponRoomSrc_t821 * ___U3CU3Ef__this_3;
};
