﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct List_1_t918;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct IEnumerable_1_t917;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct IEnumerator_1_t4367;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct ICollection_1_t4368;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct ReadOnlyCollection_1_t3684;
// GooglePlayGames.BasicApi.Quests.IQuest[]
struct IQuestU5BU5D_t3682;
// System.Predicate`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct Predicate_1_t3685;
// System.Action`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct Action_1_t3686;
// System.Comparison`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct Comparison_1_t3688;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Quests.IQuest>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m19292(__this, method) (( void (*) (List_1_t918 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19293(__this, ___collection, method) (( void (*) (List_1_t918 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::.ctor(System.Int32)
#define List_1__ctor_m19294(__this, ___capacity, method) (( void (*) (List_1_t918 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::.cctor()
#define List_1__cctor_m19295(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19296(__this, method) (( Object_t* (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19297(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t918 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19298(__this, method) (( Object_t * (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19299(__this, ___item, method) (( int32_t (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19300(__this, ___item, method) (( bool (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19301(__this, ___item, method) (( int32_t (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19302(__this, ___index, ___item, method) (( void (*) (List_1_t918 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19303(__this, ___item, method) (( void (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19304(__this, method) (( bool (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19305(__this, method) (( bool (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19306(__this, method) (( Object_t * (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19307(__this, method) (( bool (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19308(__this, method) (( bool (*) (List_1_t918 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19309(__this, ___index, method) (( Object_t * (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19310(__this, ___index, ___value, method) (( void (*) (List_1_t918 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Add(T)
#define List_1_Add_m19311(__this, ___item, method) (( void (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19312(__this, ___newCount, method) (( void (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19313(__this, ___collection, method) (( void (*) (List_1_t918 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19314(__this, ___enumerable, method) (( void (*) (List_1_t918 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19315(__this, ___collection, method) (( void (*) (List_1_t918 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::AsReadOnly()
#define List_1_AsReadOnly_m19316(__this, method) (( ReadOnlyCollection_1_t3684 * (*) (List_1_t918 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Clear()
#define List_1_Clear_m19317(__this, method) (( void (*) (List_1_t918 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Contains(T)
#define List_1_Contains_m19318(__this, ___item, method) (( bool (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19319(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t918 *, IQuestU5BU5D_t3682*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Find(System.Predicate`1<T>)
#define List_1_Find_m19320(__this, ___match, method) (( Object_t * (*) (List_1_t918 *, Predicate_1_t3685 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19321(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3685 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m19322(__this, ___match, method) (( int32_t (*) (List_1_t918 *, Predicate_1_t3685 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19323(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t918 *, int32_t, int32_t, Predicate_1_t3685 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m19324(__this, ___action, method) (( void (*) (List_1_t918 *, Action_1_t3686 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::GetEnumerator()
#define List_1_GetEnumerator_m19325(__this, method) (( Enumerator_t3687  (*) (List_1_t918 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::IndexOf(T)
#define List_1_IndexOf_m19326(__this, ___item, method) (( int32_t (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19327(__this, ___start, ___delta, method) (( void (*) (List_1_t918 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19328(__this, ___index, method) (( void (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Insert(System.Int32,T)
#define List_1_Insert_m19329(__this, ___index, ___item, method) (( void (*) (List_1_t918 *, int32_t, Object_t *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19330(__this, ___collection, method) (( void (*) (List_1_t918 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Remove(T)
#define List_1_Remove_m19331(__this, ___item, method) (( bool (*) (List_1_t918 *, Object_t *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19332(__this, ___match, method) (( int32_t (*) (List_1_t918 *, Predicate_1_t3685 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19333(__this, ___index, method) (( void (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Reverse()
#define List_1_Reverse_m19334(__this, method) (( void (*) (List_1_t918 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Sort()
#define List_1_Sort_m19335(__this, method) (( void (*) (List_1_t918 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19336(__this, ___comparison, method) (( void (*) (List_1_t918 *, Comparison_1_t3688 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::ToArray()
#define List_1_ToArray_m19337(__this, method) (( IQuestU5BU5D_t3682* (*) (List_1_t918 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::TrimExcess()
#define List_1_TrimExcess_m19338(__this, method) (( void (*) (List_1_t918 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::get_Capacity()
#define List_1_get_Capacity_m19339(__this, method) (( int32_t (*) (List_1_t918 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19340(__this, ___value, method) (( void (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::get_Count()
#define List_1_get_Count_m19341(__this, method) (( int32_t (*) (List_1_t918 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::get_Item(System.Int32)
#define List_1_get_Item_m19342(__this, ___index, method) (( Object_t * (*) (List_1_t918 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::set_Item(System.Int32,T)
#define List_1_set_Item_m19343(__this, ___index, ___value, method) (( void (*) (List_1_t918 *, int32_t, Object_t *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
