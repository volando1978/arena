﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct IEnumerable_1_t902;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct IEnumerator_1_t4353;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct ICollection_1_t4354;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct ReadOnlyCollection_1_t3649;
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
struct ParticipantU5BU5D_t3647;
// System.Predicate`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Predicate_1_t3650;
// System.Action`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Action_1_t3651;
// System.Comparison`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Comparison_1_t3652;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3797(__this, method) (( void (*) (List_1_t351 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18758(__this, ___collection, method) (( void (*) (List_1_t351 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Int32)
#define List_1__ctor_m18759(__this, ___capacity, method) (( void (*) (List_1_t351 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::.cctor()
#define List_1__cctor_m18760(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18761(__this, method) (( Object_t* (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18762(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t351 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18763(__this, method) (( Object_t * (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18764(__this, ___item, method) (( int32_t (*) (List_1_t351 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18765(__this, ___item, method) (( bool (*) (List_1_t351 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18766(__this, ___item, method) (( int32_t (*) (List_1_t351 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18767(__this, ___index, ___item, method) (( void (*) (List_1_t351 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18768(__this, ___item, method) (( void (*) (List_1_t351 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18769(__this, method) (( bool (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18770(__this, method) (( bool (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18771(__this, method) (( Object_t * (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18772(__this, method) (( bool (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18773(__this, method) (( bool (*) (List_1_t351 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18774(__this, ___index, method) (( Object_t * (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18775(__this, ___index, ___value, method) (( void (*) (List_1_t351 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Add(T)
#define List_1_Add_m18776(__this, ___item, method) (( void (*) (List_1_t351 *, Participant_t340 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18777(__this, ___newCount, method) (( void (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18778(__this, ___collection, method) (( void (*) (List_1_t351 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18779(__this, ___enumerable, method) (( void (*) (List_1_t351 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18780(__this, ___collection, method) (( void (*) (List_1_t351 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::AsReadOnly()
#define List_1_AsReadOnly_m18781(__this, method) (( ReadOnlyCollection_1_t3649 * (*) (List_1_t351 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Clear()
#define List_1_Clear_m18782(__this, method) (( void (*) (List_1_t351 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Contains(T)
#define List_1_Contains_m18783(__this, ___item, method) (( bool (*) (List_1_t351 *, Participant_t340 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18784(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t351 *, ParticipantU5BU5D_t3647*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Find(System.Predicate`1<T>)
#define List_1_Find_m18785(__this, ___match, method) (( Participant_t340 * (*) (List_1_t351 *, Predicate_1_t3650 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18786(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3650 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m18787(__this, ___match, method) (( int32_t (*) (List_1_t351 *, Predicate_1_t3650 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18788(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t351 *, int32_t, int32_t, Predicate_1_t3650 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m18789(__this, ___action, method) (( void (*) (List_1_t351 *, Action_1_t3651 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::GetEnumerator()
#define List_1_GetEnumerator_m3700(__this, method) (( Enumerator_t904  (*) (List_1_t351 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::IndexOf(T)
#define List_1_IndexOf_m18790(__this, ___item, method) (( int32_t (*) (List_1_t351 *, Participant_t340 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18791(__this, ___start, ___delta, method) (( void (*) (List_1_t351 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18792(__this, ___index, method) (( void (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Insert(System.Int32,T)
#define List_1_Insert_m18793(__this, ___index, ___item, method) (( void (*) (List_1_t351 *, int32_t, Participant_t340 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18794(__this, ___collection, method) (( void (*) (List_1_t351 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Remove(T)
#define List_1_Remove_m18795(__this, ___item, method) (( bool (*) (List_1_t351 *, Participant_t340 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18796(__this, ___match, method) (( int32_t (*) (List_1_t351 *, Predicate_1_t3650 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18797(__this, ___index, method) (( void (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Reverse()
#define List_1_Reverse_m18798(__this, method) (( void (*) (List_1_t351 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Sort()
#define List_1_Sort_m3699(__this, method) (( void (*) (List_1_t351 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18799(__this, ___comparison, method) (( void (*) (List_1_t351 *, Comparison_1_t3652 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::ToArray()
#define List_1_ToArray_m18800(__this, method) (( ParticipantU5BU5D_t3647* (*) (List_1_t351 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::TrimExcess()
#define List_1_TrimExcess_m18801(__this, method) (( void (*) (List_1_t351 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Capacity()
#define List_1_get_Capacity_m18802(__this, method) (( int32_t (*) (List_1_t351 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18803(__this, ___value, method) (( void (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Count()
#define List_1_get_Count_m18804(__this, method) (( int32_t (*) (List_1_t351 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Item(System.Int32)
#define List_1_get_Item_m18805(__this, ___index, method) (( Participant_t340 * (*) (List_1_t351 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::set_Item(System.Int32,T)
#define List_1_set_Item_m18806(__this, ___index, ___value, method) (( void (*) (List_1_t351 *, int32_t, Participant_t340 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
