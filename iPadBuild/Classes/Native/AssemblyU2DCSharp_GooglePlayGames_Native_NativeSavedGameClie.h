﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t610;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// System.Action
struct Action_t588;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
struct  NativeConflictResolver_t613  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.SnapshotManager GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mManager
	SnapshotManager_t610 * ___mManager_0;
	// System.String GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mConflictId
	String_t* ___mConflictId_1;
	// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mOriginal
	NativeSnapshotMetadata_t611 * ___mOriginal_2;
	// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mUnmerged
	NativeSnapshotMetadata_t611 * ___mUnmerged_3;
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata> GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mCompleteCallback
	Action_2_t612 * ___mCompleteCallback_4;
	// System.Action GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::mRetryFileOpen
	Action_t588 * ___mRetryFileOpen_5;
};
