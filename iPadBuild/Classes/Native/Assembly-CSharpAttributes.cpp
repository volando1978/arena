﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void TurnBasedMatch_t353_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void TurnBasedMatch_t353_CustomAttributesCacheGenerator_TurnBasedMatch_U3CToStringU3Em__0_m1419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Builder_t365_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Builder_t365_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Builder_t365_CustomAttributesCacheGenerator_Builder_U3CmInvitationDelegateU3Em__1_m1475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Builder_t365_CustomAttributesCacheGenerator_Builder_U3CmMatchDelegateU3Em__2_m1476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void QuestFetchFlags_t369_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PlayGamesHelperObject_t392_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PlayGamesHelperObject_t392_CustomAttributesCacheGenerator_PlayGamesHelperObject_U3CUpdateU3Em__3_m1609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_1U3Em__4_m4421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_2U3Em__6_m4422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_3U3Em__8_m4423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToOnGameThreadU3Ec__AnonStorey14_1_t1000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToOnGameThreadU3Ec__AnonStorey16_2_t1002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToOnGameThreadU3Ec__AnonStorey18_3_t1004_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache12(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_m4438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CInitializeGameServicesU3Em__10_m2288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CUnlockAchievementU3Em__13_m2289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CRevealAchievementU3Em__15_m2290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t1006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t1007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CUnlockAchievementU3Ec__AnonStorey1D_t526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRevealAchievementU3Ec__AnonStorey1E_t527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CUpdateAchievementU3Ec__AnonStorey1F_t528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CIncrementAchievementU3Ec__AnonStorey20_t529_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CShowAchievementsUIU3Ec__AnonStorey21_t531_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CShowLeaderboardUIU3Ec__AnonStorey22_t532_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFetchAllEventsU3Ec__AnonStorey24_t545_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFetchEventU3Ec__AnonStorey25_t547_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFetchU3Ec__AnonStorey26_t551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFetchMatchingStateU3Ec__AnonStorey27_t553_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFromQuestUICallbackU3Ec__AnonStorey28_t555_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptU3Ec__AnonStorey29_t557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CClaimMilestoneU3Ec__AnonStorey2A_t559_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void OnGameThreadForwardingListener_t563_CustomAttributesCacheGenerator_OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRoomSetupProgressU3Ec__AnonStorey3B_t567_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRoomConnectedU3Ec__AnonStorey3C_t568_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPeersConnectedU3Ec__AnonStorey3D_t569_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPeersDisconnectedU3Ec__AnonStorey3E_t570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CParticipantLeftU3Ec__AnonStorey40_t572_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ConnectingState_t584_CustomAttributesCacheGenerator_ConnectingState_U3CLeaveRoomU3Em__3B_m2418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ConnectingState_t584_CustomAttributesCacheGenerator_ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CLeaveRoomU3Em__45_m2436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LeavingRoom_t589_CustomAttributesCacheGenerator_LeavingRoom_U3COnStateEnteredU3Em__46_m2443(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AbortingRoomCreationState_t590_CustomAttributesCacheGenerator_AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateQuickGameU3Ec__AnonStorey2D_t591_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateQuickGameU3Ec__AnonStorey2B_t593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateQuickGameU3Ec__AnonStorey2C_t595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CHelperForSessionU3Ec__AnonStorey2E_t596_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptFromInboxU3Ec__AnonStorey33_t602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptInvitationU3Ec__AnonStorey37_t605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptInvitationU3Ec__AnonStorey36_t606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CDeclineInvitationU3Ec__AnonStorey3A_t609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeConflictResolver_t613_CustomAttributesCacheGenerator_NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Prefetcher_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Prefetcher_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Prefetcher_t615_CustomAttributesCacheGenerator_Prefetcher_U3COnOriginalDataReadU3Em__55_m2503(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Prefetcher_t615_CustomAttributesCacheGenerator_Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToOnGameThreadU3Ec__AnonStorey43_t619_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CInternalManualOpenU3Ec__AnonStorey45_t622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CReadBinaryDataU3Ec__AnonStorey47_t626_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCommitUpdateU3Ec__AnonStorey49_t629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToOnGameThreadU3Ec__AnonStorey4B_2_t1008_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMultiplayerClient_t535_CustomAttributesCacheGenerator_NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptFromInboxU3Ec__AnonStorey4F_t635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcceptInvitationU3Ec__AnonStorey50_t636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFindInvitationWithIdU3Ec__AnonStorey51_t638_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CTakeTurnU3Ec__AnonStorey53_t640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CFinishU3Ec__AnonStorey56_t645_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CLeaveU3Ec__AnonStorey58_t647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CLeaveDuringTurnU3Ec__AnonStorey59_t648_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCancelU3Ec__AnonStorey5A_t649_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRematchU3Ec__AnonStorey5B_t650_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute.h"
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttributeMethodDeclarations.h"
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_0.h"
extern const Il2CppType* FetchAllCallback_t398_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void AchievementManager_t656_CustomAttributesCacheGenerator_AchievementManager_InternalFetchAllCallback_m2618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchAllCallback_t398_0_0_0_var = il2cpp_codegen_type_from_index(872);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchAllCallback_t398_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Achievemen_1.h"
extern const Il2CppType* FetchCallback_t399_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void AchievementManager_t656_CustomAttributesCacheGenerator_AchievementManager_InternalFetchCallback_m2620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchCallback_t399_0_0_0_var = il2cpp_codegen_type_from_index(874);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchCallback_t399_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchAllResponse_t655_CustomAttributesCacheGenerator_FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Callbacks_t661_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.PInvoke.Callbacks/ShowUICallbackInternal
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_S.h"
extern const Il2CppType* ShowUICallbackInternal_t659_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void Callbacks_t661_CustomAttributesCacheGenerator_Callbacks_InternalShowUICallback_m2637(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ShowUICallbackInternal_t659_0_0_0_var = il2cpp_codegen_type_from_index(1067);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(ShowUICallbackInternal_t659_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Callbacks_t661_CustomAttributesCacheGenerator_Callbacks_U3CNoopUICallbackU3Em__72_m2640(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToIntPtrU3Ec__AnonStorey5C_1_t1011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToIntPtrU3Ec__AnonStorey5D_2_t1012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t1013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t1015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag.h"
extern const Il2CppType* FetchAllCallback_t419_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void EventManager_t548_CustomAttributesCacheGenerator_EventManager_InternalFetchAllCallback_m2657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchAllCallback_t419_0_0_0_var = il2cpp_codegen_type_from_index(880);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchAllCallback_t419_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_EventManag_0.h"
extern const Il2CppType* FetchCallback_t420_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void EventManager_t548_CustomAttributesCacheGenerator_EventManager_InternalFetchCallback_m2659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchCallback_t420_0_0_0_var = il2cpp_codegen_type_from_index(882);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchCallback_t420_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchAllResponse_t664_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchAllResponse_t664_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__79_m2653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchAllResponse_t664_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__7A_m2654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_1.h"
extern const Il2CppType* OnAuthActionFinishedCallback_t404_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalAuthFinishedCallback_m2681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnAuthActionFinishedCallback_t404_0_0_0_var = il2cpp_codegen_type_from_index(887);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnAuthActionFinishedCallback_t404_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.Builder/OnAuthActionStartedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_0.h"
extern const Il2CppType* OnAuthActionStartedCallback_t403_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalAuthStartedCallback_m2683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnAuthActionStartedCallback_t403_0_0_0_var = il2cpp_codegen_type_from_index(888);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnAuthActionStartedCallback_t403_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_3.h"
extern const Il2CppType* OnTurnBasedMatchEventCallback_t406_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnTurnBasedMatchEventCallback_t406_0_0_0_var = il2cpp_codegen_type_from_index(890);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnTurnBasedMatchEventCallback_t406_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.Builder/OnMultiplayerInvitationEventCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Builder_On_2.h"
extern const Il2CppType* OnMultiplayerInvitationEventCallback_t405_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnMultiplayerInvitationEventCallback_t405_0_0_0_var = il2cpp_codegen_type_from_index(891);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnMultiplayerInvitationEventCallback_t405_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MultiplayerInvitation_t601_CustomAttributesCacheGenerator_MultiplayerInvitation_U3CIdU3Em__7B_m2710(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MultiplayerParticipant_t672_CustomAttributesCacheGenerator_MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MultiplayerParticipant_t672_CustomAttributesCacheGenerator_MultiplayerParticipant_U3CIdU3Em__7D_m2724(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CDescriptionU3Em__6E_m2735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CIdU3Em__6F_m2736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CNameU3Em__70_m2737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_IdU3Em__7E_m2747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_NameU3Em__7F_m2748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_DescriptionU3Em__80_m2749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_ImageUrlU3Em__81_m2750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CIdU3Em__82_m2757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CNameU3Em__83_m2758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CAvatarURLU3Em__84_m2759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_IdU3Em__85_m2775(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_NameU3Em__86_m2776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_DescriptionU3Em__87_m2777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_BannerUrlU3Em__88_m2778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_IconUrlU3Em__89_m2779(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeRealTimeRoom_t574_CustomAttributesCacheGenerator_NativeRealTimeRoom_U3CIdU3Em__8E_m2803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeRealTimeRoom_t574_CustomAttributesCacheGenerator_NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CDataU3Em__96_m2849(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CIdU3Em__97_m2850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void PInvokeUtilities_t682_CustomAttributesCacheGenerator_PInvokeUtilities_ToEnumerable_m4472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m4481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m4482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_Dispose_m4485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_Reset_m4486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_PlayerMana.h"
extern const Il2CppType* FetchSelfCallback_t439_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void PlayerManager_t685_CustomAttributesCacheGenerator_PlayerManager_InternalFetchSelfCallback_m2877(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchSelfCallback_t439_0_0_0_var = il2cpp_codegen_type_from_index(911);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchSelfCallback_t439_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag.h"
extern const Il2CppType* FetchCallback_t444_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalFetchCallback_m2924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchCallback_t444_0_0_0_var = il2cpp_codegen_type_from_index(916);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchCallback_t444_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_0.h"
extern const Il2CppType* FetchListCallback_t445_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalFetchListCallback_m2926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchListCallback_t445_0_0_0_var = il2cpp_codegen_type_from_index(918);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchListCallback_t445_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_3.h"
extern const Il2CppType* QuestUICallback_t448_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalQuestUICallback_m2929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuestUICallback_t448_0_0_0_var = il2cpp_codegen_type_from_index(920);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(QuestUICallback_t448_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_1.h"
extern const Il2CppType* AcceptCallback_t446_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalAcceptCallback_m2931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AcceptCallback_t446_0_0_0_var = il2cpp_codegen_type_from_index(922);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(AcceptCallback_t446_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_2.h"
extern const Il2CppType* ClaimMilestoneCallback_t447_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalClaimMilestoneCallback_m2933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClaimMilestoneCallback_t447_0_0_0_var = il2cpp_codegen_type_from_index(924);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(ClaimMilestoneCallback_t447_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchListResponse_t689_CustomAttributesCacheGenerator_FetchListResponse_U3CDataU3Em__99_m2901(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv.h"
extern const Il2CppType* OnRoomStatusChangedCallback_t451_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnRoomStatusChangedCallback_m2939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnRoomStatusChangedCallback_t451_0_0_0_var = il2cpp_codegen_type_from_index(926);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnRoomStatusChangedCallback_t451_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_0.h"
extern const Il2CppType* OnRoomConnectedSetChangedCallback_t452_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnRoomConnectedSetChangedCallback_m2941(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnRoomConnectedSetChangedCallback_t452_0_0_0_var = il2cpp_codegen_type_from_index(927);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnRoomConnectedSetChangedCallback_t452_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_1.h"
extern const Il2CppType* OnP2PConnectedCallback_t453_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnP2PConnectedCallback_m2943(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnP2PConnectedCallback_t453_0_0_0_var = il2cpp_codegen_type_from_index(928);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnP2PConnectedCallback_t453_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_2.h"
extern const Il2CppType* OnP2PDisconnectedCallback_t454_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnP2PDisconnectedCallback_m2945(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnP2PDisconnectedCallback_t454_0_0_0_var = il2cpp_codegen_type_from_index(929);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnP2PDisconnectedCallback_t454_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_3.h"
extern const Il2CppType* OnParticipantStatusChangedCallback_t455_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnParticipantStatusChangedCallback_m2947(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnParticipantStatusChangedCallback_t455_0_0_0_var = il2cpp_codegen_type_from_index(930);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnParticipantStatusChangedCallback_t455_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_4.h"
extern const Il2CppType* OnDataReceivedCallback_t456_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnDataReceived_m2950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OnDataReceivedCallback_t456_0_0_0_var = il2cpp_codegen_type_from_index(931);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OnDataReceivedCallback_t456_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CToCallbackPointerU3Ec__AnonStorey63_t694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_3.h"
extern const Il2CppType* PlayerSelectUICallback_t462_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalPlayerSelectUIcallback_m2979(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerSelectUICallback_t462_0_0_0_var = il2cpp_codegen_type_from_index(936);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(PlayerSelectUICallback_t462_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu.h"
extern const Il2CppType* RealTimeRoomCallback_t458_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalRealTimeRoomCallback_m2980(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeRoomCallback_t458_0_0_0_var = il2cpp_codegen_type_from_index(935);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(RealTimeRoomCallback_t458_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_2.h"
extern const Il2CppType* RoomInboxUICallback_t461_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalRoomInboxUICallback_m2981(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RoomInboxUICallback_t461_0_0_0_var = il2cpp_codegen_type_from_index(938);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(RoomInboxUICallback_t461_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_4.h"
extern const Il2CppType* WaitingRoomUICallback_t463_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalWaitingRoomUICallback_m2984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitingRoomUICallback_t463_0_0_0_var = il2cpp_codegen_type_from_index(940);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(WaitingRoomUICallback_t463_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_5.h"
extern const Il2CppType* FetchInvitationsCallback_t464_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalFetchInvitationsCallback_m2985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchInvitationsCallback_t464_0_0_0_var = il2cpp_codegen_type_from_index(942);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchInvitationsCallback_t464_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_0.h"
extern const Il2CppType* LeaveRoomCallback_t459_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalLeaveRoomCallback_m2987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LeaveRoomCallback_t459_0_0_0_var = il2cpp_codegen_type_from_index(944);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(LeaveRoomCallback_t459_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_1.h"
extern const Il2CppType* SendReliableMessageCallback_t460_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalSendReliableMessageCallback_m2992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SendReliableMessageCallback_t460_0_0_0_var = il2cpp_codegen_type_from_index(945);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(SendReliableMessageCallback_t460_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_U3CSendUnreliableMessageToSpecificParticipantsU3Em__9B_m2996(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchInvitationsResponse_t698_CustomAttributesCacheGenerator_FetchInvitationsResponse_U3CInvitationsU3Em__9C_m2975(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa.h"
extern const Il2CppType* FetchAllCallback_t473_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalFetchAllCallback_m3048(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FetchAllCallback_t473_0_0_0_var = il2cpp_codegen_type_from_index(951);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(FetchAllCallback_t473_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_3.h"
extern const Il2CppType* SnapshotSelectUICallback_t477_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalSnapshotSelectUICallback_m3050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SnapshotSelectUICallback_t477_0_0_0_var = il2cpp_codegen_type_from_index(953);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(SnapshotSelectUICallback_t477_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_0.h"
extern const Il2CppType* OpenCallback_t474_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalOpenCallback_m3052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OpenCallback_t474_0_0_0_var = il2cpp_codegen_type_from_index(955);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(OpenCallback_t474_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_1.h"
extern const Il2CppType* CommitCallback_t475_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalCommitCallback_m3055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CommitCallback_t475_0_0_0_var = il2cpp_codegen_type_from_index(957);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(CommitCallback_t475_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_2.h"
extern const Il2CppType* ReadCallback_t476_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalReadCallback_m3058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReadCallback_t476_0_0_0_var = il2cpp_codegen_type_from_index(959);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(ReadCallback_t476_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void OpenResponse_t701_CustomAttributesCacheGenerator_OpenResponse_U3CConflictIdU3Em__9D_m3019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void FetchAllResponse_t702_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__9E_m3026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ReadResponse_t704_CustomAttributesCacheGenerator_ReadResponse_U3CDataU3Em__9F_m3039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_2.h"
extern const Il2CppType* TurnBasedMatchCallback_t496_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalTurnBasedMatchCallback_m3082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TurnBasedMatchCallback_t496_0_0_0_var = il2cpp_codegen_type_from_index(961);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(TurnBasedMatchCallback_t496_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_6.h"
extern const Il2CppType* PlayerSelectUICallback_t500_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalPlayerSelectUIcallback_m3085(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerSelectUICallback_t500_0_0_0_var = il2cpp_codegen_type_from_index(962);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(PlayerSelectUICallback_t500_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_4.h"
extern const Il2CppType* TurnBasedMatchesCallback_t498_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalTurnBasedMatchesCallback_m3087(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TurnBasedMatchesCallback_t498_0_0_0_var = il2cpp_codegen_type_from_index(963);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(TurnBasedMatchesCallback_t498_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_5.h"
extern const Il2CppType* MatchInboxUICallback_t499_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalMatchInboxUICallback_m3091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MatchInboxUICallback_t499_0_0_0_var = il2cpp_codegen_type_from_index(965);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(MatchInboxUICallback_t499_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_3.h"
extern const Il2CppType* MultiplayerStatusCallback_t497_0_0_0_var;
extern TypeInfo* MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var;
void TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalMultiplayerStatusCallback_m3093(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerStatusCallback_t497_0_0_0_var = il2cpp_codegen_type_from_index(967);
		MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoPInvokeCallbackAttribute_t1010 * tmp;
		tmp = (MonoPInvokeCallbackAttribute_t1010 *)il2cpp_codegen_object_new (MonoPInvokeCallbackAttribute_t1010_il2cpp_TypeInfo_var);
		MonoPInvokeCallbackAttribute__ctor_m4449(tmp, il2cpp_codegen_type_get_object(MultiplayerStatusCallback_t497_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void TurnBasedMatchesResponse_t708_CustomAttributesCacheGenerator_TurnBasedMatchesResponse_U3CInvitationsU3Em__A0_m3075(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AudioToggle_t722_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AudioToggle_t722_CustomAttributesCacheGenerator_AudioToggle_U3CStartU3Em__A2_m3142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void BackgroundScript_t723_CustomAttributesCacheGenerator_tilePrefab(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void ExtensionMethods_t729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void ExtensionMethods_t729_CustomAttributesCacheGenerator_ExtensionMethods_Search_m3158(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GameCenterSingleton_t730_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GameCenterSingleton_t730_CustomAttributesCacheGenerator_GameCenterSingleton_U3CReportScoreU3Em__A3_m3177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CReportAchievementProgressU3Ec__AnonStorey65_t731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void HUD_t733_CustomAttributesCacheGenerator_HUD_run_m3195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void HUD_t733_CustomAttributesCacheGenerator_HUD_runOut_m3196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3179(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3180(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_Dispose_m3182(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_Reset_m3183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3185(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_Dispose_m3188(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_Reset_m3189(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void InterludeScr_t741_CustomAttributesCacheGenerator_InterludeScr_run_m3225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3216(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3217(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_Dispose_m3219(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_Reset_m3220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void LaserControl_t745_CustomAttributesCacheGenerator_LaserControl_showLaser_m3237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3230(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_Dispose_m3233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_Reset_m3234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void RaycastScr_t751_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void SoundManager_t756_CustomAttributesCacheGenerator_library(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreMenu_t757_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreMenu_t757_CustomAttributesCacheGenerator_StoreMenu_U3COnGUIU3Em__A5_m3294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void WinTextScript_t760_CustomAttributesCacheGenerator_winTexts(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void cameraScript_t774_CustomAttributesCacheGenerator_cameraScript_shake_m3348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void cameraScript_t774_CustomAttributesCacheGenerator_cameraScript_shakeSmall_m3349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_Dispose_m3338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_Reset_m3339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_Dispose_m3344(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_Reset_m3345(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_currentState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_newWeaponFlag(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_startGame_m3459(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_wait_m3461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_waitForTouchDown_m3462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_waitTime_m3463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_runTutorial_m3464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameControl_t794_CustomAttributesCacheGenerator_gameControl_ChangeLevelWait_m3465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_Dispose_m3417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_Reset_m3418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_Dispose_m3423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_Reset_m3424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_Dispose_m3435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_Reset_m3436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_Dispose_m3441(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_Reset_m3442(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameOverSrc_t801_CustomAttributesCacheGenerator_gameOverSrc_run_m3503(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void gameOverSrc_t801_CustomAttributesCacheGenerator_gameOverSrc_Show_m3504(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_Dispose_m3492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_Reset_m3493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_Dispose_m3498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_Reset_m3499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void globales_t807_CustomAttributesCacheGenerator_frameRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void globales_t807_CustomAttributesCacheGenerator_globales_sleep_m3546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_Dispose_m3515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_Reset_m3516(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void logoScene_t812_CustomAttributesCacheGenerator_logoScene_wait_m3563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_Dispose_m3559(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_Reset_m3560(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void musicToggle_t819_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void musicToggle_t819_CustomAttributesCacheGenerator_musicToggle_U3CStartU3Em__A6_m3586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_weaponsSprites(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_isNewWeaponReady(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_newWeaponRoomSrc_animSpin_m3608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_newWeaponRoomSrc_waiting_m3609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_Dispose_m3597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_Reset_m3598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_Dispose_m3603(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_Reset_m3604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void playerMovement_t830_CustomAttributesCacheGenerator_playerMovement_moveCoin_m3636(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_Dispose_m3630(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_Reset_m3631(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[341] = 
{
	NULL,
	g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator,
	TurnBasedMatch_t353_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	TurnBasedMatch_t353_CustomAttributesCacheGenerator_TurnBasedMatch_U3CToStringU3Em__0_m1419,
	Builder_t365_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	Builder_t365_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	Builder_t365_CustomAttributesCacheGenerator_Builder_U3CmInvitationDelegateU3Em__1_m1475,
	Builder_t365_CustomAttributesCacheGenerator_Builder_U3CmMatchDelegateU3Em__2_m1476,
	QuestFetchFlags_t369_CustomAttributesCacheGenerator,
	PlayGamesHelperObject_t392_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	PlayGamesHelperObject_t392_CustomAttributesCacheGenerator_PlayGamesHelperObject_U3CUpdateU3Em__3_m1609,
	CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_1U3Em__4_m4421,
	CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_2U3Em__6_m4422,
	CallbackUtils_t395_CustomAttributesCacheGenerator_CallbackUtils_U3CToOnGameThread_3U3Em__8_m4423,
	U3CToOnGameThreadU3Ec__AnonStorey14_1_t1000_CustomAttributesCacheGenerator,
	U3CToOnGameThreadU3Ec__AnonStorey16_2_t1002_CustomAttributesCacheGenerator,
	U3CToOnGameThreadU3Ec__AnonStorey18_3_t1004_CustomAttributesCacheGenerator,
	NativeClient_t524_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache12,
	NativeClient_t524_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13,
	NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_m4438,
	NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CInitializeGameServicesU3Em__10_m2288,
	NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CUnlockAchievementU3Em__13_m2289,
	NativeClient_t524_CustomAttributesCacheGenerator_NativeClient_U3CRevealAchievementU3Em__15_m2290,
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t1006_CustomAttributesCacheGenerator,
	U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t1007_CustomAttributesCacheGenerator,
	U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525_CustomAttributesCacheGenerator,
	U3CUnlockAchievementU3Ec__AnonStorey1D_t526_CustomAttributesCacheGenerator,
	U3CRevealAchievementU3Ec__AnonStorey1E_t527_CustomAttributesCacheGenerator,
	U3CUpdateAchievementU3Ec__AnonStorey1F_t528_CustomAttributesCacheGenerator,
	U3CIncrementAchievementU3Ec__AnonStorey20_t529_CustomAttributesCacheGenerator,
	U3CShowAchievementsUIU3Ec__AnonStorey21_t531_CustomAttributesCacheGenerator,
	U3CShowLeaderboardUIU3Ec__AnonStorey22_t532_CustomAttributesCacheGenerator,
	U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533_CustomAttributesCacheGenerator,
	U3CFetchAllEventsU3Ec__AnonStorey24_t545_CustomAttributesCacheGenerator,
	U3CFetchEventU3Ec__AnonStorey25_t547_CustomAttributesCacheGenerator,
	U3CFetchU3Ec__AnonStorey26_t551_CustomAttributesCacheGenerator,
	U3CFetchMatchingStateU3Ec__AnonStorey27_t553_CustomAttributesCacheGenerator,
	U3CFromQuestUICallbackU3Ec__AnonStorey28_t555_CustomAttributesCacheGenerator,
	U3CAcceptU3Ec__AnonStorey29_t557_CustomAttributesCacheGenerator,
	U3CClaimMilestoneU3Ec__AnonStorey2A_t559_CustomAttributesCacheGenerator,
	OnGameThreadForwardingListener_t563_CustomAttributesCacheGenerator_OnGameThreadForwardingListener_U3CLeftRoomU3Em__32_m2372,
	U3CRoomSetupProgressU3Ec__AnonStorey3B_t567_CustomAttributesCacheGenerator,
	U3CRoomConnectedU3Ec__AnonStorey3C_t568_CustomAttributesCacheGenerator,
	U3CPeersConnectedU3Ec__AnonStorey3D_t569_CustomAttributesCacheGenerator,
	U3CPeersDisconnectedU3Ec__AnonStorey3E_t570_CustomAttributesCacheGenerator,
	U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571_CustomAttributesCacheGenerator,
	U3CParticipantLeftU3Ec__AnonStorey40_t572_CustomAttributesCacheGenerator,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__37_m2401,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__38_m2402,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CUpdateCurrentRoomU3Em__39_m2403,
	MessagingEnabledState_t580_CustomAttributesCacheGenerator_MessagingEnabledState_U3CGetConnectedParticipantsU3Em__3A_m2404,
	ConnectingState_t584_CustomAttributesCacheGenerator_ConnectingState_U3CLeaveRoomU3Em__3B_m2418,
	ConnectingState_t584_CustomAttributesCacheGenerator_ConnectingState_U3CShowWaitingRoomUIU3Em__3C_m2419,
	ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ActiveState_t586_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432,
	ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433,
	ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434,
	ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435,
	ActiveState_t586_CustomAttributesCacheGenerator_ActiveState_U3CLeaveRoomU3Em__45_m2436,
	U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585_CustomAttributesCacheGenerator,
	LeavingRoom_t589_CustomAttributesCacheGenerator_LeavingRoom_U3COnStateEnteredU3Em__46_m2443,
	AbortingRoomCreationState_t590_CustomAttributesCacheGenerator_AbortingRoomCreationState_U3CHandleRoomResponseU3Em__47_m2447,
	U3CCreateQuickGameU3Ec__AnonStorey2D_t591_CustomAttributesCacheGenerator,
	U3CCreateQuickGameU3Ec__AnonStorey2B_t593_CustomAttributesCacheGenerator,
	U3CCreateQuickGameU3Ec__AnonStorey2C_t595_CustomAttributesCacheGenerator,
	U3CHelperForSessionU3Ec__AnonStorey2E_t596_CustomAttributesCacheGenerator,
	U3CCreateWithInvitationScreenU3Ec__AnonStorey30_t597_CustomAttributesCacheGenerator,
	U3CCreateWithInvitationScreenU3Ec__AnonStorey2F_t598_CustomAttributesCacheGenerator,
	U3CAcceptFromInboxU3Ec__AnonStorey33_t602_CustomAttributesCacheGenerator,
	U3CAcceptInvitationU3Ec__AnonStorey37_t605_CustomAttributesCacheGenerator,
	U3CAcceptInvitationU3Ec__AnonStorey36_t606_CustomAttributesCacheGenerator,
	U3CDeclineInvitationU3Ec__AnonStorey3A_t609_CustomAttributesCacheGenerator,
	NativeConflictResolver_t613_CustomAttributesCacheGenerator_NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498,
	Prefetcher_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	Prefetcher_t615_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache8,
	Prefetcher_t615_CustomAttributesCacheGenerator_Prefetcher_U3COnOriginalDataReadU3Em__55_m2503,
	Prefetcher_t615_CustomAttributesCacheGenerator_Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504,
	U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616_CustomAttributesCacheGenerator,
	U3CToOnGameThreadU3Ec__AnonStorey43_t619_CustomAttributesCacheGenerator,
	U3CInternalManualOpenU3Ec__AnonStorey45_t622_CustomAttributesCacheGenerator,
	U3CReadBinaryDataU3Ec__AnonStorey47_t626_CustomAttributesCacheGenerator,
	U3CShowSelectSavedGameUIU3Ec__AnonStorey48_t628_CustomAttributesCacheGenerator,
	U3CCommitUpdateU3Ec__AnonStorey49_t629_CustomAttributesCacheGenerator,
	U3CFetchAllSavedGamesU3Ec__AnonStorey4A_t631_CustomAttributesCacheGenerator,
	U3CToOnGameThreadU3Ec__AnonStorey4B_2_t1008_CustomAttributesCacheGenerator,
	NativeTurnBasedMultiplayerClient_t535_CustomAttributesCacheGenerator_NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600,
	U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633_CustomAttributesCacheGenerator,
	U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634_CustomAttributesCacheGenerator,
	U3CAcceptFromInboxU3Ec__AnonStorey4F_t635_CustomAttributesCacheGenerator,
	U3CAcceptInvitationU3Ec__AnonStorey50_t636_CustomAttributesCacheGenerator,
	U3CFindInvitationWithIdU3Ec__AnonStorey51_t638_CustomAttributesCacheGenerator,
	U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639_CustomAttributesCacheGenerator,
	U3CTakeTurnU3Ec__AnonStorey53_t640_CustomAttributesCacheGenerator,
	U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642_CustomAttributesCacheGenerator,
	U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644_CustomAttributesCacheGenerator,
	U3CFinishU3Ec__AnonStorey56_t645_CustomAttributesCacheGenerator,
	U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646_CustomAttributesCacheGenerator,
	U3CLeaveU3Ec__AnonStorey58_t647_CustomAttributesCacheGenerator,
	U3CLeaveDuringTurnU3Ec__AnonStorey59_t648_CustomAttributesCacheGenerator,
	U3CCancelU3Ec__AnonStorey5A_t649_CustomAttributesCacheGenerator,
	U3CRematchU3Ec__AnonStorey5B_t650_CustomAttributesCacheGenerator,
	AchievementManager_t656_CustomAttributesCacheGenerator_AchievementManager_InternalFetchAllCallback_m2618,
	AchievementManager_t656_CustomAttributesCacheGenerator_AchievementManager_InternalFetchCallback_m2620,
	FetchAllResponse_t655_CustomAttributesCacheGenerator_FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614,
	Callbacks_t661_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	Callbacks_t661_CustomAttributesCacheGenerator_Callbacks_InternalShowUICallback_m2637,
	Callbacks_t661_CustomAttributesCacheGenerator_Callbacks_U3CNoopUICallbackU3Em__72_m2640,
	U3CToIntPtrU3Ec__AnonStorey5C_1_t1011_CustomAttributesCacheGenerator,
	U3CToIntPtrU3Ec__AnonStorey5D_2_t1012_CustomAttributesCacheGenerator,
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t1013_CustomAttributesCacheGenerator,
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t1015_CustomAttributesCacheGenerator,
	EventManager_t548_CustomAttributesCacheGenerator_EventManager_InternalFetchAllCallback_m2657,
	EventManager_t548_CustomAttributesCacheGenerator_EventManager_InternalFetchCallback_m2659,
	FetchAllResponse_t664_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	FetchAllResponse_t664_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__79_m2653,
	FetchAllResponse_t664_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__7A_m2654,
	GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalAuthFinishedCallback_m2681,
	GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalAuthStartedCallback_m2683,
	GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalOnTurnBasedMatchEventCallback_m2685,
	GameServicesBuilder_t667_CustomAttributesCacheGenerator_GameServicesBuilder_InternalOnMultiplayerInvitationEventCallback_m2687,
	MultiplayerInvitation_t601_CustomAttributesCacheGenerator_MultiplayerInvitation_U3CIdU3Em__7B_m2710,
	MultiplayerParticipant_t672_CustomAttributesCacheGenerator_MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723,
	MultiplayerParticipant_t672_CustomAttributesCacheGenerator_MultiplayerParticipant_U3CIdU3Em__7D_m2724,
	NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CDescriptionU3Em__6E_m2735,
	NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CIdU3Em__6F_m2736,
	NativeAchievement_t673_CustomAttributesCacheGenerator_NativeAchievement_U3CNameU3Em__70_m2737,
	NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_IdU3Em__7E_m2747,
	NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_NameU3Em__7F_m2748,
	NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_DescriptionU3Em__80_m2749,
	NativeEvent_t674_CustomAttributesCacheGenerator_NativeEvent_U3Cget_ImageUrlU3Em__81_m2750,
	NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CIdU3Em__82_m2757,
	NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CNameU3Em__83_m2758,
	NativePlayer_t675_CustomAttributesCacheGenerator_NativePlayer_U3CAvatarURLU3Em__84_m2759,
	NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_IdU3Em__85_m2775,
	NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_NameU3Em__86_m2776,
	NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_DescriptionU3Em__87_m2777,
	NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_BannerUrlU3Em__88_m2778,
	NativeQuest_t677_CustomAttributesCacheGenerator_NativeQuest_U3Cget_IconUrlU3Em__89_m2779,
	NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792,
	NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793,
	NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794,
	NativeQuestMilestone_t676_CustomAttributesCacheGenerator_NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795,
	NativeRealTimeRoom_t574_CustomAttributesCacheGenerator_NativeRealTimeRoom_U3CIdU3Em__8E_m2803,
	NativeRealTimeRoom_t574_CustomAttributesCacheGenerator_NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804,
	NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814,
	NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815,
	NativeSnapshotMetadata_t611_CustomAttributesCacheGenerator_NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816,
	NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CParticipantsU3Em__93_m2846,
	NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CDescriptionU3Em__94_m2847,
	NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CRematchIdU3Em__95_m2848,
	NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CDataU3Em__96_m2849,
	NativeTurnBasedMatch_t680_CustomAttributesCacheGenerator_NativeTurnBasedMatch_U3CIdU3Em__97_m2850,
	PInvokeUtilities_t682_CustomAttributesCacheGenerator_PInvokeUtilities_ToEnumerable_m4472,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4480,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m4481,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m4482,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4483,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_Dispose_m4485,
	U3CToEnumerableU3Ec__Iterator0_1_t1018_CustomAttributesCacheGenerator_U3CToEnumerableU3Ec__Iterator0_1_Reset_m4486,
	PlayerManager_t685_CustomAttributesCacheGenerator_PlayerManager_InternalFetchSelfCallback_m2877,
	U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687_CustomAttributesCacheGenerator,
	QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalFetchCallback_m2924,
	QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalFetchListCallback_m2926,
	QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalQuestUICallback_m2929,
	QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalAcceptCallback_m2931,
	QuestManager_t560_CustomAttributesCacheGenerator_QuestManager_InternalClaimMilestoneCallback_m2933,
	FetchListResponse_t689_CustomAttributesCacheGenerator_FetchListResponse_U3CDataU3Em__99_m2901,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnRoomStatusChangedCallback_m2939,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnRoomConnectedSetChangedCallback_m2941,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnP2PConnectedCallback_m2943,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnP2PDisconnectedCallback_m2945,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnParticipantStatusChangedCallback_m2947,
	RealTimeEventListenerHelper_t594_CustomAttributesCacheGenerator_RealTimeEventListenerHelper_InternalOnDataReceived_m2950,
	U3CToCallbackPointerU3Ec__AnonStorey63_t694_CustomAttributesCacheGenerator,
	RealtimeManager_t564_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalPlayerSelectUIcallback_m2979,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalRealTimeRoomCallback_m2980,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalRoomInboxUICallback_m2981,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalWaitingRoomUICallback_m2984,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalFetchInvitationsCallback_m2985,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalLeaveRoomCallback_m2987,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_InternalSendReliableMessageCallback_m2992,
	RealtimeManager_t564_CustomAttributesCacheGenerator_RealtimeManager_U3CSendUnreliableMessageToSpecificParticipantsU3Em__9B_m2996,
	FetchInvitationsResponse_t698_CustomAttributesCacheGenerator_FetchInvitationsResponse_U3CInvitationsU3Em__9C_m2975,
	SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalFetchAllCallback_m3048,
	SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalSnapshotSelectUICallback_m3050,
	SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalOpenCallback_m3052,
	SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalCommitCallback_m3055,
	SnapshotManager_t610_CustomAttributesCacheGenerator_SnapshotManager_InternalReadCallback_m3058,
	OpenResponse_t701_CustomAttributesCacheGenerator_OpenResponse_U3CConflictIdU3Em__9D_m3019,
	FetchAllResponse_t702_CustomAttributesCacheGenerator_FetchAllResponse_U3CDataU3Em__9E_m3026,
	ReadResponse_t704_CustomAttributesCacheGenerator_ReadResponse_U3CDataU3Em__9F_m3039,
	TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalTurnBasedMatchCallback_m3082,
	TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalPlayerSelectUIcallback_m3085,
	TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalTurnBasedMatchesCallback_m3087,
	TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalMatchInboxUICallback_m3091,
	TurnBasedManager_t651_CustomAttributesCacheGenerator_TurnBasedManager_InternalMultiplayerStatusCallback_m3093,
	TurnBasedMatchesResponse_t708_CustomAttributesCacheGenerator_TurnBasedMatchesResponse_U3CInvitationsU3Em__A0_m3075,
	U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711_CustomAttributesCacheGenerator,
	AudioToggle_t722_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	AudioToggle_t722_CustomAttributesCacheGenerator_AudioToggle_U3CStartU3Em__A2_m3142,
	BackgroundScript_t723_CustomAttributesCacheGenerator_tilePrefab,
	ExtensionMethods_t729_CustomAttributesCacheGenerator,
	ExtensionMethods_t729_CustomAttributesCacheGenerator_ExtensionMethods_Search_m3158,
	GameCenterSingleton_t730_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	GameCenterSingleton_t730_CustomAttributesCacheGenerator_GameCenterSingleton_U3CReportScoreU3Em__A3_m3177,
	U3CReportAchievementProgressU3Ec__AnonStorey65_t731_CustomAttributesCacheGenerator,
	HUD_t733_CustomAttributesCacheGenerator_HUD_run_m3195,
	HUD_t733_CustomAttributesCacheGenerator_HUD_runOut_m3196,
	U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator,
	U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3179,
	U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3180,
	U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_Dispose_m3182,
	U3CrunU3Ec__Iterator1_t734_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator1_Reset_m3183,
	U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator,
	U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3185,
	U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3186,
	U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_Dispose_m3188,
	U3CrunOutU3Ec__Iterator2_t735_CustomAttributesCacheGenerator_U3CrunOutU3Ec__Iterator2_Reset_m3189,
	InterludeScr_t741_CustomAttributesCacheGenerator_InterludeScr_run_m3225,
	U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator,
	U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3216,
	U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3217,
	U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_Dispose_m3219,
	U3CrunU3Ec__Iterator3_t742_CustomAttributesCacheGenerator_U3CrunU3Ec__Iterator3_Reset_m3220,
	LaserControl_t745_CustomAttributesCacheGenerator_LaserControl_showLaser_m3237,
	U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator,
	U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3230,
	U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3231,
	U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_Dispose_m3233,
	U3CshowLaserU3Ec__Iterator4_t746_CustomAttributesCacheGenerator_U3CshowLaserU3Ec__Iterator4_Reset_m3234,
	RaycastScr_t751_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	SoundManager_t756_CustomAttributesCacheGenerator_library,
	StoreMenu_t757_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache25,
	StoreMenu_t757_CustomAttributesCacheGenerator_StoreMenu_U3COnGUIU3Em__A5_m3294,
	WinTextScript_t760_CustomAttributesCacheGenerator_winTexts,
	cameraScript_t774_CustomAttributesCacheGenerator_cameraScript_shake_m3348,
	cameraScript_t774_CustomAttributesCacheGenerator_cameraScript_shakeSmall_m3349,
	U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator,
	U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335,
	U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336,
	U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_Dispose_m3338,
	U3CshakeU3Ec__Iterator5_t775_CustomAttributesCacheGenerator_U3CshakeU3Ec__Iterator5_Reset_m3339,
	U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator,
	U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341,
	U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342,
	U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_Dispose_m3344,
	U3CshakeSmallU3Ec__Iterator6_t776_CustomAttributesCacheGenerator_U3CshakeSmallU3Ec__Iterator6_Reset_m3345,
	gameControl_t794_CustomAttributesCacheGenerator_currentState,
	gameControl_t794_CustomAttributesCacheGenerator_newWeaponFlag,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_startGame_m3459,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_wait_m3461,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_waitForTouchDown_m3462,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_waitTime_m3463,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_runTutorial_m3464,
	gameControl_t794_CustomAttributesCacheGenerator_gameControl_ChangeLevelWait_m3465,
	U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator,
	U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414,
	U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415,
	U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_Dispose_m3417,
	U3CstartGameU3Ec__Iterator7_t795_CustomAttributesCacheGenerator_U3CstartGameU3Ec__Iterator7_Reset_m3418,
	U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator,
	U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420,
	U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421,
	U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_Dispose_m3423,
	U3CwaitU3Ec__Iterator8_t796_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator8_Reset_m3424,
	U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator,
	U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426,
	U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427,
	U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429,
	U3CwaitForTouchDownU3Ec__Iterator9_t797_CustomAttributesCacheGenerator_U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430,
	U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator,
	U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432,
	U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433,
	U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_Dispose_m3435,
	U3CwaitTimeU3Ec__IteratorA_t798_CustomAttributesCacheGenerator_U3CwaitTimeU3Ec__IteratorA_Reset_m3436,
	U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator,
	U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438,
	U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439,
	U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_Dispose_m3441,
	U3CrunTutorialU3Ec__IteratorB_t799_CustomAttributesCacheGenerator_U3CrunTutorialU3Ec__IteratorB_Reset_m3442,
	U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator,
	U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444,
	U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445,
	U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447,
	U3CChangeLevelWaitU3Ec__IteratorC_t800_CustomAttributesCacheGenerator_U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448,
	gameOverSrc_t801_CustomAttributesCacheGenerator_gameOverSrc_run_m3503,
	gameOverSrc_t801_CustomAttributesCacheGenerator_gameOverSrc_Show_m3504,
	U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator,
	U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489,
	U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490,
	U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_Dispose_m3492,
	U3CrunU3Ec__IteratorD_t802_CustomAttributesCacheGenerator_U3CrunU3Ec__IteratorD_Reset_m3493,
	U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator,
	U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495,
	U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496,
	U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_Dispose_m3498,
	U3CShowU3Ec__IteratorE_t803_CustomAttributesCacheGenerator_U3CShowU3Ec__IteratorE_Reset_m3499,
	globales_t807_CustomAttributesCacheGenerator_frameRate,
	globales_t807_CustomAttributesCacheGenerator_globales_sleep_m3546,
	U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator,
	U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512,
	U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513,
	U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_Dispose_m3515,
	U3CsleepU3Ec__IteratorF_t805_CustomAttributesCacheGenerator_U3CsleepU3Ec__IteratorF_Reset_m3516,
	logoScene_t812_CustomAttributesCacheGenerator_logoScene_wait_m3563,
	U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator,
	U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556,
	U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557,
	U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_Dispose_m3559,
	U3CwaitU3Ec__Iterator10_t813_CustomAttributesCacheGenerator_U3CwaitU3Ec__Iterator10_Reset_m3560,
	musicToggle_t819_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	musicToggle_t819_CustomAttributesCacheGenerator_musicToggle_U3CStartU3Em__A6_m3586,
	newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_weaponsSprites,
	newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_isNewWeaponReady,
	newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_newWeaponRoomSrc_animSpin_m3608,
	newWeaponRoomSrc_t821_CustomAttributesCacheGenerator_newWeaponRoomSrc_waiting_m3609,
	U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator,
	U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594,
	U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595,
	U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_Dispose_m3597,
	U3CanimSpinU3Ec__Iterator11_t822_CustomAttributesCacheGenerator_U3CanimSpinU3Ec__Iterator11_Reset_m3598,
	U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator,
	U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600,
	U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601,
	U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_Dispose_m3603,
	U3CwaitingU3Ec__Iterator12_t823_CustomAttributesCacheGenerator_U3CwaitingU3Ec__Iterator12_Reset_m3604,
	playerMovement_t830_CustomAttributesCacheGenerator_playerMovement_moveCoin_m3636,
	U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator,
	U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627,
	U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628,
	U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_Dispose_m3630,
	U3CmoveCoinU3Ec__Iterator13_t829_CustomAttributesCacheGenerator_U3CmoveCoinU3Ec__Iterator13_Reset_m3631,
};
