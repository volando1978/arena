﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
struct SavedGameMetadataUpdate_t378;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_0.h"

// System.Void GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::.ctor(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder)
extern "C" void SavedGameMetadataUpdate__ctor_m1487 (SavedGameMetadataUpdate_t378 * __this, Builder_t376  ___builder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsDescriptionUpdated()
extern "C" bool SavedGameMetadataUpdate_get_IsDescriptionUpdated_m1488 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedDescription()
extern "C" String_t* SavedGameMetadataUpdate_get_UpdatedDescription_m1489 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsCoverImageUpdated()
extern "C" bool SavedGameMetadataUpdate_get_IsCoverImageUpdated_m1490 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPngCoverImage()
extern "C" ByteU5BU5D_t350* SavedGameMetadataUpdate_get_UpdatedPngCoverImage_m1491 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_IsPlayedTimeUpdated()
extern "C" bool SavedGameMetadataUpdate_get_IsPlayedTimeUpdated_m1492 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.TimeSpan> GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate::get_UpdatedPlayedTime()
extern "C" Nullable_1_t377  SavedGameMetadataUpdate_get_UpdatedPlayedTime_m1493 (SavedGameMetadataUpdate_t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
