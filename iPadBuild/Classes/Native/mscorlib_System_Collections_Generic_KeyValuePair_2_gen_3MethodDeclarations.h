﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
struct KeyValuePair_2_t3463;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15906(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3463 *, String_t*, Object_t *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m15907(__this, method) (( String_t* (*) (KeyValuePair_2_t3463 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15908(__this, ___value, method) (( void (*) (KeyValuePair_2_t3463 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m15909(__this, method) (( Object_t * (*) (KeyValuePair_2_t3463 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15910(__this, ___value, method) (( void (*) (KeyValuePair_2_t3463 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::ToString()
#define KeyValuePair_2_ToString_m15911(__this, method) (( String_t* (*) (KeyValuePair_2_t3463 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
