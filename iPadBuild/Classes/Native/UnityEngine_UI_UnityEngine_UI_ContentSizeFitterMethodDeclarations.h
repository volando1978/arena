﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t1312;
// UnityEngine.RectTransform
struct RectTransform_t1227;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"

// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern "C" void ContentSizeFitter__ctor_m5551 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern "C" int32_t ContentSizeFitter_get_horizontalFit_m5552 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern "C" void ContentSizeFitter_set_horizontalFit_m5553 (ContentSizeFitter_t1312 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern "C" int32_t ContentSizeFitter_get_verticalFit_m5554 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern "C" void ContentSizeFitter_set_verticalFit_m5555 (ContentSizeFitter_t1312 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern "C" RectTransform_t1227 * ContentSizeFitter_get_rectTransform_m5556 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern "C" void ContentSizeFitter_OnEnable_m5557 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern "C" void ContentSizeFitter_OnDisable_m5558 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m5559 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m5560 (ContentSizeFitter_t1312 * __this, int32_t ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m5561 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern "C" void ContentSizeFitter_SetLayoutVertical_m5562 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern "C" void ContentSizeFitter_SetDirty_m5563 (ContentSizeFitter_t1312 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
