﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestState.h"
// GooglePlayGames.BasicApi.Quests.QuestState
struct  QuestState_t367 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.QuestState::value__
	int32_t ___value___1;
};
