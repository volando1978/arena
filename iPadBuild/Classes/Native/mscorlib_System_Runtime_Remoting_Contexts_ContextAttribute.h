﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.Remoting.Contexts.ContextAttribute
struct  ContextAttribute_t2589  : public Attribute_t1546
{
	// System.String System.Runtime.Remoting.Contexts.ContextAttribute::AttributeName
	String_t* ___AttributeName_0;
};
