﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCurrencyStorage
struct VirtualCurrencyStorage_t69;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.VirtualCurrencyStorage::.ctor()
extern "C" void VirtualCurrencyStorage__ctor_m540 (VirtualCurrencyStorage_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualCurrencyStorage::.cctor()
extern "C" void VirtualCurrencyStorage__cctor_m541 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualCurrencyStorage Soomla.Store.VirtualCurrencyStorage::get_instance()
extern "C" VirtualCurrencyStorage_t69 * VirtualCurrencyStorage_get_instance_m542 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::GetBalance(Soomla.Store.VirtualItem)
extern "C" int32_t VirtualCurrencyStorage_GetBalance_m543 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::SetBalance(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualCurrencyStorage_SetBalance_m544 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___balance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::SetBalance(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorage_SetBalance_m545 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::Add(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualCurrencyStorage_Add_m546 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::Add(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorage_Add_m547 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::Remove(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualCurrencyStorage_Remove_m548 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorage::Remove(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorage_Remove_m549 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
