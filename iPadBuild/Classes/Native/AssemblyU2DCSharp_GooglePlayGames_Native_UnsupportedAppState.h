﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.UnsupportedAppStateClient
struct  UnsupportedAppStateClient_t713  : public Object_t
{
	// System.String GooglePlayGames.Native.UnsupportedAppStateClient::mMessage
	String_t* ___mMessage_0;
};
