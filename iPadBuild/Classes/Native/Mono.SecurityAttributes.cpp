﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var;
void g_Mono_Security_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2347);
		AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2343);
		AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2346);
		AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2342);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2345);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2945);
		AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3628);
		AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3629);
		NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3631);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 12;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyCopyrightAttribute_t1423 * tmp;
		tmp = (AssemblyCopyrightAttribute_t1423 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m6179(tmp, il2cpp_codegen_string_new_wrapper("(c) 2003-2004 Various Authors"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t1419 * tmp;
		tmp = (AssemblyDescriptionAttribute_t1419 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m6175(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t1422 * tmp;
		tmp = (AssemblyProductAttribute_t1422 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m6178(tmp, il2cpp_codegen_string_new_wrapper("MONO CLI"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t1418 * tmp;
		tmp = (AssemblyTitleAttribute_t1418 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m6174(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t1421 * tmp;
		tmp = (AssemblyCompanyAttribute_t1421 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m6177(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("System, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1811 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1811 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7693(tmp, il2cpp_codegen_string_new_wrapper("../mono.pub"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1812 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1812 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7694(tmp, true, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1814 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1814 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7696(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9014(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_SetBit_m9025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_SetBit_m9026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_ToString_m9029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_ToString_m9030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m9040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m9044(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Equality_m9050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m9051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void ModulusRing_t2192_CustomAttributesCacheGenerator_ModulusRing_Pow_m8998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ASN1_t2131_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void X509CertificateCollection_t2153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void X509ChainStatusFlags_t2223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void X509Crl_t2133_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void X509Crl_t2133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t2156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ExtendedKeyUsageExtension_t2226_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void KeyUsages_t2228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void CertTypes_t2230_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void CipherSuiteCollection_t2243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void HttpsClientStream_t2261_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void HttpsClientStream_t2261_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void HttpsClientStream_t2261_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m9481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void HttpsClientStream_t2261_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m9482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void RSASslSignatureDeformatter_t2266_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void RSASslSignatureFormatter_t2268_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void SecurityProtocolType_t2271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t2306_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_Mono_Security_Assembly_AttributeGenerators[39] = 
{
	NULL,
	g_Mono_Security_Assembly_CustomAttributesCacheGenerator,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9014,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9016,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger__ctor_m9018,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_SetBit_m9025,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_SetBit_m9026,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_ToString_m9029,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_ToString_m9030,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m9040,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m9044,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Equality_m9050,
	BigInteger_t2191_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m9051,
	ModulusRing_t2192_CustomAttributesCacheGenerator_ModulusRing_Pow_m8998,
	ASN1_t2131_CustomAttributesCacheGenerator,
	PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5,
	PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6,
	PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7,
	PKCS12_t2152_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10,
	X509Certificate_t2024_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11,
	X509CertificateCollection_t2153_CustomAttributesCacheGenerator,
	X509ChainStatusFlags_t2223_CustomAttributesCacheGenerator,
	X509Crl_t2133_CustomAttributesCacheGenerator,
	X509Crl_t2133_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13,
	X509ExtensionCollection_t2156_CustomAttributesCacheGenerator,
	ExtendedKeyUsageExtension_t2226_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14,
	KeyUsages_t2228_CustomAttributesCacheGenerator,
	CertTypes_t2230_CustomAttributesCacheGenerator,
	CipherSuiteCollection_t2243_CustomAttributesCacheGenerator,
	HttpsClientStream_t2261_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	HttpsClientStream_t2261_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	HttpsClientStream_t2261_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m9481,
	HttpsClientStream_t2261_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m9482,
	RSASslSignatureDeformatter_t2266_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15,
	RSASslSignatureFormatter_t2268_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16,
	SecurityProtocolType_t2271_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2306_CustomAttributesCacheGenerator,
};
