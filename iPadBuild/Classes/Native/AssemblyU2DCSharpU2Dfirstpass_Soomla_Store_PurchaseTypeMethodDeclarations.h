﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// System.String
struct String_t;

// System.Void Soomla.Store.PurchaseType::.ctor()
extern "C" void PurchaseType__ctor_m671 (PurchaseType_t123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchaseType::Buy(System.String)
// System.Boolean Soomla.Store.PurchaseType::CanAfford()
