﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMatch
struct TurnBasedMatch_t493;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0.h"

// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_AutomatchingSlotsAvailable_m2138 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t TurnBasedMatch_TurnBasedMatch_CreationTime_m2139 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Participants_Length_m2140 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_Participants_GetElement_m2141 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Version(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Version_m2142 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_ParticipantResults(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_ParticipantResults_m2143 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Status(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t TurnBasedMatch_TurnBasedMatch_Status_m2144 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Description_m2145 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PendingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_PendingParticipant_m2146 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Variant_m2147 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasPreviousMatchData(System.Runtime.InteropServices.HandleRef)
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasPreviousMatchData_m2148 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Data(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Data_m2149 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_LastUpdatingParticipant_m2150 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasData(System.Runtime.InteropServices.HandleRef)
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasData_m2151 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_SuggestedNextParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_SuggestedNextParticipant_m2152 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PreviousMatchData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_PreviousMatchData_m2153 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdateTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t TurnBasedMatch_TurnBasedMatch_LastUpdateTime_m2154 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_RematchId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_RematchId_m2155 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Number(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Number_m2156 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasRematchId(System.Runtime.InteropServices.HandleRef)
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasRematchId_m2157 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool TurnBasedMatch_TurnBasedMatch_Valid_m2158 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_CreatingParticipant_m2159 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Id_m2160 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatch_TurnBasedMatch_Dispose_m2161 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
