﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>
struct SoomlaEntity_1_t127;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Object
struct Object_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::.ctor(System.String)
// Soomla.SoomlaEntity`1<System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_SoomlaEntity_1_gen_1MethodDeclarations.h"
#define SoomlaEntity_1__ctor_m17267(__this, ___id, method) (( void (*) (SoomlaEntity_1_t127 *, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16331_gshared)(__this, ___id, method)
// System.Void Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::.ctor(System.String,System.String,System.String)
#define SoomlaEntity_1__ctor_m1028(__this, ___id, ___name, ___description, method) (( void (*) (SoomlaEntity_1_t127 *, String_t*, String_t*, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16332_gshared)(__this, ___id, ___name, ___description, method)
// System.Void Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::.ctor(JSONObject)
#define SoomlaEntity_1__ctor_m1029(__this, ___jsonEntity, method) (( void (*) (SoomlaEntity_1_t127 *, JSONObject_t30 *, MethodInfo*))SoomlaEntity_1__ctor_m16333_gshared)(__this, ___jsonEntity, method)
// System.String Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::get_ID()
#define SoomlaEntity_1_get_ID_m17268(__this, method) (( String_t* (*) (SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_get_ID_m16334_gshared)(__this, method)
// JSONObject Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::toJSONObject()
#define SoomlaEntity_1_toJSONObject_m1027(__this, method) (( JSONObject_t30 * (*) (SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_toJSONObject_m16335_gshared)(__this, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::Equals(System.Object)
#define SoomlaEntity_1_Equals_m17269(__this, ___obj, method) (( bool (*) (SoomlaEntity_1_t127 *, Object_t *, MethodInfo*))SoomlaEntity_1_Equals_m16336_gshared)(__this, ___obj, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::Equals(Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_Equals_m17270(__this, ___g, method) (( bool (*) (SoomlaEntity_1_t127 *, SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_Equals_m16338_gshared)(__this, ___g, method)
// System.Int32 Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::GetHashCode()
#define SoomlaEntity_1_GetHashCode_m1030(__this, method) (( int32_t (*) (SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_GetHashCode_m16339_gshared)(__this, method)
// T Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::Clone(System.String)
#define SoomlaEntity_1_Clone_m1310(__this, ___newId, method) (( VirtualItem_t125 * (*) (SoomlaEntity_1_t127 *, String_t*, MethodInfo*))SoomlaEntity_1_Clone_m16340_gshared)(__this, ___newId, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::op_Equality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_op_Equality_m964(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t127 *, SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_op_Equality_m16341_gshared)(__this /* static, unused */, ___a, ___b, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>::op_Inequality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_op_Inequality_m977(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t127 *, SoomlaEntity_1_t127 *, MethodInfo*))SoomlaEntity_1_op_Inequality_m16343_gshared)(__this /* static, unused */, ___a, ___b, method)
