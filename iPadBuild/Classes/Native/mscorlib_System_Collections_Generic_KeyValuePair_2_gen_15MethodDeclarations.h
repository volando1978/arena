﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct KeyValuePair_2_t3714;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m19682(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3714 *, String_t*, Achievement_t333 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Key()
#define KeyValuePair_2_get_Key_m19683(__this, method) (( String_t* (*) (KeyValuePair_2_t3714 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19684(__this, ___value, method) (( void (*) (KeyValuePair_2_t3714 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Value()
#define KeyValuePair_2_get_Value_m19685(__this, method) (( Achievement_t333 * (*) (KeyValuePair_2_t3714 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19686(__this, ___value, method) (( void (*) (KeyValuePair_2_t3714 *, Achievement_t333 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::ToString()
#define KeyValuePair_2_ToString_m19687(__this, method) (( String_t* (*) (KeyValuePair_2_t3714 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
