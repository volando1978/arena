﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct  AdvertisingResult_t354 
{
	// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mStatus
	int32_t ___mStatus_0;
	// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::mLocalEndpointName
	String_t* ___mLocalEndpointName_1;
};
// Native definition for marshalling of: GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t354_marshaled
{
	int32_t ___mStatus_0;
	char* ___mLocalEndpointName_1;
};
