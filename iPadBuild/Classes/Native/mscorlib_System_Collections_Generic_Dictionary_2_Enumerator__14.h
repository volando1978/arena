﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct Dictionary_2_t344;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct  Enumerator_t3680 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::dictionary
	Dictionary_2_t344 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::current
	KeyValuePair_2_t3677  ___current_3;
};
