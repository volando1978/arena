﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder
struct RealtimeRoomConfigBuilder_t700;
// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse
struct PlayerSelectUIResponse_t686;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t592;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::.ctor(System.IntPtr)
extern "C" void RealtimeRoomConfigBuilder__ctor_m3000 (RealtimeRoomConfigBuilder_t700 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::PopulateFromUIResponse(GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_PopulateFromUIResponse_m3001 (RealtimeRoomConfigBuilder_t700 * __this, PlayerSelectUIResponse_t686 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetVariant(System.UInt32)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_SetVariant_m3002 (RealtimeRoomConfigBuilder_t700 * __this, uint32_t ___variantValue, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::AddInvitedPlayer(System.String)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_AddInvitedPlayer_m3003 (RealtimeRoomConfigBuilder_t700 * __this, String_t* ___playerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetExclusiveBitMask(System.UInt64)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_SetExclusiveBitMask_m3004 (RealtimeRoomConfigBuilder_t700 * __this, uint64_t ___bitmask, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetMinimumAutomatchingPlayers(System.UInt32)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_SetMinimumAutomatchingPlayers_m3005 (RealtimeRoomConfigBuilder_t700 * __this, uint32_t ___minimum, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::SetMaximumAutomatchingPlayers(System.UInt32)
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_SetMaximumAutomatchingPlayers_m3006 (RealtimeRoomConfigBuilder_t700 * __this, uint32_t ___maximum, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::Build()
extern "C" RealtimeRoomConfig_t592 * RealtimeRoomConfigBuilder_Build_m3007 (RealtimeRoomConfigBuilder_t700 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealtimeRoomConfigBuilder_CallDispose_m3008 (RealtimeRoomConfigBuilder_t700 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder GooglePlayGames.Native.PInvoke.RealtimeRoomConfigBuilder::Create()
extern "C" RealtimeRoomConfigBuilder_t700 * RealtimeRoomConfigBuilder_Create_m3009 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
