﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Boolean>
struct Action_1_t98;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A
struct  U3CCancelU3Ec__AnonStorey5A_t649  : public Object_t
{
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::callback
	Action_1_t98 * ___callback_0;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_1;
};
