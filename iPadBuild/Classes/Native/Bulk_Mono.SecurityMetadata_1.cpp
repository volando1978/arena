﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t2290_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate__ctor_m9761_ParameterInfos[] = 
{
	{"context", 0, 134218531, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218532, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerCertificate__ctor_m9761_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificate__ctor_m9761/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate__ctor_m9761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Update()
MethodInfo TlsServerCertificate_Update_m9762_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificate_Update_m9762/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsSsl3()
MethodInfo TlsServerCertificate_ProcessAsSsl3_m9763_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsSsl3_m9763/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsTls1()
MethodInfo TlsServerCertificate_ProcessAsTls1_m9764_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsTls1_m9764/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2024_0_0_0;
extern Il2CppType X509Certificate_t2024_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate_checkCertificateUsage_m9765_ParameterInfos[] = 
{
	{"cert", 0, 134218533, 0, &X509Certificate_t2024_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
MethodInfo TlsServerCertificate_checkCertificateUsage_m9765_MethodInfo = 
{
	"checkCertificateUsage"/* name */
	, (methodPointerType)&TlsServerCertificate_checkCertificateUsage_m9765/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate_checkCertificateUsage_m9765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509CertificateCollection_t2153_0_0_0;
extern Il2CppType X509CertificateCollection_t2153_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate_validateCertificates_m9766_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, 0, &X509CertificateCollection_t2153_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
MethodInfo TlsServerCertificate_validateCertificates_m9766_MethodInfo = 
{
	"validateCertificates"/* name */
	, (methodPointerType)&TlsServerCertificate_validateCertificates_m9766/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate_validateCertificates_m9766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2024_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate_checkServerIdentity_m9767_ParameterInfos[] = 
{
	{"cert", 0, 134218535, 0, &X509Certificate_t2024_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkServerIdentity(Mono.Security.X509.X509Certificate)
MethodInfo TlsServerCertificate_checkServerIdentity_m9767_MethodInfo = 
{
	"checkServerIdentity"/* name */
	, (methodPointerType)&TlsServerCertificate_checkServerIdentity_m9767/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate_checkServerIdentity_m9767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate_checkDomainName_m9768_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkDomainName(System.String)
MethodInfo TlsServerCertificate_checkDomainName_m9768_MethodInfo = 
{
	"checkDomainName"/* name */
	, (methodPointerType)&TlsServerCertificate_checkDomainName_m9768/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate_checkDomainName_m9768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo TlsServerCertificate_t2290_TlsServerCertificate_Match_m9769_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, 0, &String_t_0_0_0},
	{"pattern", 1, 134218538, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Match(System.String,System.String)
MethodInfo TlsServerCertificate_Match_m9769_MethodInfo = 
{
	"Match"/* name */
	, (methodPointerType)&TlsServerCertificate_Match_m9769/* method */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t2290_TlsServerCertificate_Match_m9769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerCertificate_t2290_MethodInfos[] =
{
	&TlsServerCertificate__ctor_m9761_MethodInfo,
	&TlsServerCertificate_Update_m9762_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m9763_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m9764_MethodInfo,
	&TlsServerCertificate_checkCertificateUsage_m9765_MethodInfo,
	&TlsServerCertificate_validateCertificates_m9766_MethodInfo,
	&TlsServerCertificate_checkServerIdentity_m9767_MethodInfo,
	&TlsServerCertificate_checkDomainName_m9768_MethodInfo,
	&TlsServerCertificate_Match_m9769_MethodInfo,
	NULL
};
extern Il2CppType X509CertificateCollection_t2153_0_0_1;
FieldInfo TlsServerCertificate_t2290____certificates_9_FieldInfo = 
{
	"certificates"/* name */
	, &X509CertificateCollection_t2153_0_0_1/* type */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerCertificate_t2290, ___certificates_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TlsServerCertificate_t2290_FieldInfos[] =
{
	&TlsServerCertificate_t2290____certificates_9_FieldInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
extern MethodInfo Stream_Dispose_m9933_MethodInfo;
extern MethodInfo TlsStream_get_CanRead_m9702_MethodInfo;
extern MethodInfo TlsStream_get_CanSeek_m9703_MethodInfo;
extern MethodInfo TlsStream_get_CanWrite_m9701_MethodInfo;
extern MethodInfo TlsStream_get_Length_m9706_MethodInfo;
extern MethodInfo TlsStream_get_Position_m9704_MethodInfo;
extern MethodInfo TlsStream_set_Position_m9705_MethodInfo;
extern MethodInfo Stream_Dispose_m9886_MethodInfo;
extern MethodInfo Stream_Close_m9885_MethodInfo;
extern MethodInfo TlsStream_Flush_m9719_MethodInfo;
extern MethodInfo TlsStream_Read_m9722_MethodInfo;
extern MethodInfo Stream_ReadByte_m9934_MethodInfo;
extern MethodInfo TlsStream_Seek_m9721_MethodInfo;
extern MethodInfo TlsStream_SetLength_m9720_MethodInfo;
extern MethodInfo TlsStream_Write_m9723_MethodInfo;
extern MethodInfo Stream_WriteByte_m9935_MethodInfo;
extern MethodInfo Stream_BeginRead_m9936_MethodInfo;
extern MethodInfo Stream_BeginWrite_m9937_MethodInfo;
extern MethodInfo Stream_EndRead_m9938_MethodInfo;
extern MethodInfo Stream_EndWrite_m9939_MethodInfo;
extern MethodInfo TlsServerCertificate_ProcessAsTls1_m9764_MethodInfo;
extern MethodInfo TlsServerCertificate_ProcessAsSsl3_m9763_MethodInfo;
extern MethodInfo TlsServerCertificate_Update_m9762_MethodInfo;
extern MethodInfo HandshakeMessage_EncodeMessage_m9732_MethodInfo;
static Il2CppMethodReference TlsServerCertificate_t2290_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m9764_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m9763_MethodInfo,
	&TlsServerCertificate_Update_m9762_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerCertificate_t2290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IDisposable_t191_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t2290_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerCertificate_t2290_0_0_0;
extern Il2CppType TlsServerCertificate_t2290_1_0_0;
extern Il2CppType HandshakeMessage_t2264_0_0_0;
struct TlsServerCertificate_t2290;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t2290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t2290_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerCertificate_t2290_VTable/* vtableMethods */
	, TlsServerCertificate_t2290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerCertificate_t2290_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificate_t2290_MethodInfos/* methods */
	, NULL/* properties */
	, TlsServerCertificate_t2290_FieldInfos/* fields */
	, NULL/* events */
	, &TlsServerCertificate_t2290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t2290_0_0_0/* byval_arg */
	, &TlsServerCertificate_t2290_1_0_0/* this_arg */
	, &TlsServerCertificate_t2290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t2290)/* instance_size */
	, sizeof (TlsServerCertificate_t2290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t2291_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerCertificateRequest_t2291_TlsServerCertificateRequest__ctor_m9770_ParameterInfos[] = 
{
	{"context", 0, 134218539, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218540, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerCertificateRequest__ctor_m9770_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificateRequest__ctor_m9770/* method */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerCertificateRequest_t2291_TlsServerCertificateRequest__ctor_m9770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::Update()
MethodInfo TlsServerCertificateRequest_Update_m9771_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_Update_m9771/* method */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsSsl3()
MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m9772_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsSsl3_m9772/* method */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsTls1()
MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m9773_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsTls1_m9773/* method */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerCertificateRequest_t2291_MethodInfos[] =
{
	&TlsServerCertificateRequest__ctor_m9770_MethodInfo,
	&TlsServerCertificateRequest_Update_m9771_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m9772_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m9773_MethodInfo,
	NULL
};
extern Il2CppType ClientCertificateTypeU5BU5D_t2282_0_0_1;
FieldInfo TlsServerCertificateRequest_t2291____certificateTypes_9_FieldInfo = 
{
	"certificateTypes"/* name */
	, &ClientCertificateTypeU5BU5D_t2282_0_0_1/* type */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerCertificateRequest_t2291, ___certificateTypes_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo TlsServerCertificateRequest_t2291____distinguisedNames_10_FieldInfo = 
{
	"distinguisedNames"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerCertificateRequest_t2291, ___distinguisedNames_10)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TlsServerCertificateRequest_t2291_FieldInfos[] =
{
	&TlsServerCertificateRequest_t2291____certificateTypes_9_FieldInfo,
	&TlsServerCertificateRequest_t2291____distinguisedNames_10_FieldInfo,
	NULL
};
extern MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m9773_MethodInfo;
extern MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m9772_MethodInfo;
extern MethodInfo TlsServerCertificateRequest_Update_m9771_MethodInfo;
static Il2CppMethodReference TlsServerCertificateRequest_t2291_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m9773_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m9772_MethodInfo,
	&TlsServerCertificateRequest_Update_m9771_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerCertificateRequest_t2291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t2291_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerCertificateRequest_t2291_0_0_0;
extern Il2CppType TlsServerCertificateRequest_t2291_1_0_0;
struct TlsServerCertificateRequest_t2291;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t2291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t2291_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerCertificateRequest_t2291_VTable/* vtableMethods */
	, TlsServerCertificateRequest_t2291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerCertificateRequest_t2291_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificateRequest_t2291_MethodInfos/* methods */
	, NULL/* properties */
	, TlsServerCertificateRequest_t2291_FieldInfos/* fields */
	, NULL/* events */
	, &TlsServerCertificateRequest_t2291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t2291_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t2291_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t2291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t2291)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t2291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t2292_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerFinished_t2292_TlsServerFinished__ctor_m9774_ParameterInfos[] = 
{
	{"context", 0, 134218541, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218542, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerFinished__ctor_m9774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerFinished__ctor_m9774/* method */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerFinished_t2292_TlsServerFinished__ctor_m9774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.cctor()
MethodInfo TlsServerFinished__cctor_m9775_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TlsServerFinished__cctor_m9775/* method */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Update()
MethodInfo TlsServerFinished_Update_m9776_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerFinished_Update_m9776/* method */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsSsl3()
MethodInfo TlsServerFinished_ProcessAsSsl3_m9777_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsSsl3_m9777/* method */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsTls1()
MethodInfo TlsServerFinished_ProcessAsTls1_m9778_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsTls1_m9778/* method */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerFinished_t2292_MethodInfos[] =
{
	&TlsServerFinished__ctor_m9774_MethodInfo,
	&TlsServerFinished__cctor_m9775_MethodInfo,
	&TlsServerFinished_Update_m9776_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m9777_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m9778_MethodInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t350_0_0_17;
FieldInfo TlsServerFinished_t2292____Ssl3Marker_9_FieldInfo = 
{
	"Ssl3Marker"/* name */
	, &ByteU5BU5D_t350_0_0_17/* type */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerFinished_t2292_StaticFields, ___Ssl3Marker_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TlsServerFinished_t2292_FieldInfos[] =
{
	&TlsServerFinished_t2292____Ssl3Marker_9_FieldInfo,
	NULL
};
extern MethodInfo TlsServerFinished_ProcessAsTls1_m9778_MethodInfo;
extern MethodInfo TlsServerFinished_ProcessAsSsl3_m9777_MethodInfo;
extern MethodInfo TlsServerFinished_Update_m9776_MethodInfo;
static Il2CppMethodReference TlsServerFinished_t2292_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m9778_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m9777_MethodInfo,
	&TlsServerFinished_Update_m9776_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerFinished_t2292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t2292_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerFinished_t2292_0_0_0;
extern Il2CppType TlsServerFinished_t2292_1_0_0;
struct TlsServerFinished_t2292;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t2292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t2292_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerFinished_t2292_VTable/* vtableMethods */
	, TlsServerFinished_t2292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerFinished_t2292_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerFinished_t2292_MethodInfos/* methods */
	, NULL/* properties */
	, TlsServerFinished_t2292_FieldInfos/* fields */
	, NULL/* events */
	, &TlsServerFinished_t2292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t2292_0_0_0/* byval_arg */
	, &TlsServerFinished_t2292_1_0_0/* this_arg */
	, &TlsServerFinished_t2292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t2292)/* instance_size */
	, sizeof (TlsServerFinished_t2292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t2292_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t2293_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerHello_t2293_TlsServerHello__ctor_m9779_ParameterInfos[] = 
{
	{"context", 0, 134218543, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218544, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerHello__ctor_m9779_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHello__ctor_m9779/* method */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerHello_t2293_TlsServerHello__ctor_m9779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::Update()
MethodInfo TlsServerHello_Update_m9780_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerHello_Update_m9780/* method */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsSsl3()
MethodInfo TlsServerHello_ProcessAsSsl3_m9781_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsSsl3_m9781/* method */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsTls1()
MethodInfo TlsServerHello_ProcessAsTls1_m9782_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsTls1_m9782/* method */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int16_t238_0_0_0;
extern Il2CppType Int16_t238_0_0_0;
static ParameterInfo TlsServerHello_t2293_TlsServerHello_processProtocol_m9783_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, 0, &Int16_t238_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::processProtocol(System.Int16)
MethodInfo TlsServerHello_processProtocol_m9783_MethodInfo = 
{
	"processProtocol"/* name */
	, (methodPointerType)&TlsServerHello_processProtocol_m9783/* method */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int16_t238/* invoker_method */
	, TlsServerHello_t2293_TlsServerHello_processProtocol_m9783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerHello_t2293_MethodInfos[] =
{
	&TlsServerHello__ctor_m9779_MethodInfo,
	&TlsServerHello_Update_m9780_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m9781_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m9782_MethodInfo,
	&TlsServerHello_processProtocol_m9783_MethodInfo,
	NULL
};
extern Il2CppType SecurityCompressionType_t2270_0_0_1;
FieldInfo TlsServerHello_t2293____compressionMethod_9_FieldInfo = 
{
	"compressionMethod"/* name */
	, &SecurityCompressionType_t2270_0_0_1/* type */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerHello_t2293, ___compressionMethod_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo TlsServerHello_t2293____random_10_FieldInfo = 
{
	"random"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerHello_t2293, ___random_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo TlsServerHello_t2293____sessionId_11_FieldInfo = 
{
	"sessionId"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerHello_t2293, ___sessionId_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CipherSuite_t2242_0_0_1;
FieldInfo TlsServerHello_t2293____cipherSuite_12_FieldInfo = 
{
	"cipherSuite"/* name */
	, &CipherSuite_t2242_0_0_1/* type */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerHello_t2293, ___cipherSuite_12)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TlsServerHello_t2293_FieldInfos[] =
{
	&TlsServerHello_t2293____compressionMethod_9_FieldInfo,
	&TlsServerHello_t2293____random_10_FieldInfo,
	&TlsServerHello_t2293____sessionId_11_FieldInfo,
	&TlsServerHello_t2293____cipherSuite_12_FieldInfo,
	NULL
};
extern MethodInfo TlsServerHello_ProcessAsTls1_m9782_MethodInfo;
extern MethodInfo TlsServerHello_ProcessAsSsl3_m9781_MethodInfo;
extern MethodInfo TlsServerHello_Update_m9780_MethodInfo;
static Il2CppMethodReference TlsServerHello_t2293_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m9782_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m9781_MethodInfo,
	&TlsServerHello_Update_m9780_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerHello_t2293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t2293_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerHello_t2293_0_0_0;
extern Il2CppType TlsServerHello_t2293_1_0_0;
struct TlsServerHello_t2293;
const Il2CppTypeDefinitionMetadata TlsServerHello_t2293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t2293_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerHello_t2293_VTable/* vtableMethods */
	, TlsServerHello_t2293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerHello_t2293_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHello_t2293_MethodInfos/* methods */
	, NULL/* properties */
	, TlsServerHello_t2293_FieldInfos/* fields */
	, NULL/* events */
	, &TlsServerHello_t2293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t2293_0_0_0/* byval_arg */
	, &TlsServerHello_t2293_1_0_0/* this_arg */
	, &TlsServerHello_t2293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t2293)/* instance_size */
	, sizeof (TlsServerHello_t2293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t2294_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerHelloDone_t2294_TlsServerHelloDone__ctor_m9784_ParameterInfos[] = 
{
	{"context", 0, 134218546, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218547, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerHelloDone__ctor_m9784_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHelloDone__ctor_m9784/* method */
	, &TlsServerHelloDone_t2294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerHelloDone_t2294_TlsServerHelloDone__ctor_m9784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsSsl3()
MethodInfo TlsServerHelloDone_ProcessAsSsl3_m9785_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsSsl3_m9785/* method */
	, &TlsServerHelloDone_t2294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsTls1()
MethodInfo TlsServerHelloDone_ProcessAsTls1_m9786_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsTls1_m9786/* method */
	, &TlsServerHelloDone_t2294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerHelloDone_t2294_MethodInfos[] =
{
	&TlsServerHelloDone__ctor_m9784_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m9785_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m9786_MethodInfo,
	NULL
};
extern MethodInfo TlsServerHelloDone_ProcessAsTls1_m9786_MethodInfo;
extern MethodInfo TlsServerHelloDone_ProcessAsSsl3_m9785_MethodInfo;
extern MethodInfo HandshakeMessage_Update_m9731_MethodInfo;
static Il2CppMethodReference TlsServerHelloDone_t2294_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m9786_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m9785_MethodInfo,
	&HandshakeMessage_Update_m9731_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerHelloDone_t2294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t2294_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerHelloDone_t2294_0_0_0;
extern Il2CppType TlsServerHelloDone_t2294_1_0_0;
struct TlsServerHelloDone_t2294;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t2294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t2294_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerHelloDone_t2294_VTable/* vtableMethods */
	, TlsServerHelloDone_t2294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerHelloDone_t2294_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHelloDone_t2294_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &TlsServerHelloDone_t2294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t2294_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t2294_1_0_0/* this_arg */
	, &TlsServerHelloDone_t2294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t2294)/* instance_size */
	, sizeof (TlsServerHelloDone_t2294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t2295_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
extern Il2CppType Context_t2240_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo TlsServerKeyExchange_t2295_TlsServerKeyExchange__ctor_m9787_ParameterInfos[] = 
{
	{"context", 0, 134218548, 0, &Context_t2240_0_0_0},
	{"buffer", 1, 134218549, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
MethodInfo TlsServerKeyExchange__ctor_m9787_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerKeyExchange__ctor_m9787/* method */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TlsServerKeyExchange_t2295_TlsServerKeyExchange__ctor_m9787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::Update()
MethodInfo TlsServerKeyExchange_Update_m9788_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerKeyExchange_Update_m9788/* method */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsSsl3()
MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m9789_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsSsl3_m9789/* method */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsTls1()
MethodInfo TlsServerKeyExchange_ProcessAsTls1_m9790_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsTls1_m9790/* method */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::verifySignature()
MethodInfo TlsServerKeyExchange_verifySignature_m9791_MethodInfo = 
{
	"verifySignature"/* name */
	, (methodPointerType)&TlsServerKeyExchange_verifySignature_m9791/* method */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TlsServerKeyExchange_t2295_MethodInfos[] =
{
	&TlsServerKeyExchange__ctor_m9787_MethodInfo,
	&TlsServerKeyExchange_Update_m9788_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m9789_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m9790_MethodInfo,
	&TlsServerKeyExchange_verifySignature_m9791_MethodInfo,
	NULL
};
extern Il2CppType RSAParameters_t2147_0_0_1;
FieldInfo TlsServerKeyExchange_t2295____rsaParams_9_FieldInfo = 
{
	"rsaParams"/* name */
	, &RSAParameters_t2147_0_0_1/* type */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerKeyExchange_t2295, ___rsaParams_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo TlsServerKeyExchange_t2295____signedParams_10_FieldInfo = 
{
	"signedParams"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* parent */
	, offsetof(TlsServerKeyExchange_t2295, ___signedParams_10)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TlsServerKeyExchange_t2295_FieldInfos[] =
{
	&TlsServerKeyExchange_t2295____rsaParams_9_FieldInfo,
	&TlsServerKeyExchange_t2295____signedParams_10_FieldInfo,
	NULL
};
extern MethodInfo TlsServerKeyExchange_ProcessAsTls1_m9790_MethodInfo;
extern MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m9789_MethodInfo;
extern MethodInfo TlsServerKeyExchange_Update_m9788_MethodInfo;
static Il2CppMethodReference TlsServerKeyExchange_t2295_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Stream_Dispose_m9933_MethodInfo,
	&TlsStream_get_CanRead_m9702_MethodInfo,
	&TlsStream_get_CanSeek_m9703_MethodInfo,
	&TlsStream_get_CanWrite_m9701_MethodInfo,
	&TlsStream_get_Length_m9706_MethodInfo,
	&TlsStream_get_Position_m9704_MethodInfo,
	&TlsStream_set_Position_m9705_MethodInfo,
	&Stream_Dispose_m9886_MethodInfo,
	&Stream_Close_m9885_MethodInfo,
	&TlsStream_Flush_m9719_MethodInfo,
	&TlsStream_Read_m9722_MethodInfo,
	&Stream_ReadByte_m9934_MethodInfo,
	&TlsStream_Seek_m9721_MethodInfo,
	&TlsStream_SetLength_m9720_MethodInfo,
	&TlsStream_Write_m9723_MethodInfo,
	&Stream_WriteByte_m9935_MethodInfo,
	&Stream_BeginRead_m9936_MethodInfo,
	&Stream_BeginWrite_m9937_MethodInfo,
	&Stream_EndRead_m9938_MethodInfo,
	&Stream_EndWrite_m9939_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m9790_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m9789_MethodInfo,
	&TlsServerKeyExchange_Update_m9788_MethodInfo,
	&HandshakeMessage_EncodeMessage_m9732_MethodInfo,
};
static bool TlsServerKeyExchange_t2295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t2295_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType TlsServerKeyExchange_t2295_0_0_0;
extern Il2CppType TlsServerKeyExchange_t2295_1_0_0;
struct TlsServerKeyExchange_t2295;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t2295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t2295_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t2264_0_0_0/* parent */
	, TlsServerKeyExchange_t2295_VTable/* vtableMethods */
	, TlsServerKeyExchange_t2295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TlsServerKeyExchange_t2295_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerKeyExchange_t2295_MethodInfos/* methods */
	, NULL/* properties */
	, TlsServerKeyExchange_t2295_FieldInfos/* fields */
	, NULL/* events */
	, &TlsServerKeyExchange_t2295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t2295_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t2295_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t2295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t2295)/* instance_size */
	, sizeof (TlsServerKeyExchange_t2295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t2296_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo PrimalityTest_t2296_PrimalityTest__ctor_m9792_ParameterInfos[] = 
{
	{"object", 0, 134218550, 0, &Object_t_0_0_0},
	{"method", 1, 134218551, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
MethodInfo PrimalityTest__ctor_m9792_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m9792/* method */
	, &PrimalityTest_t2296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t2296_PrimalityTest__ctor_m9792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BigInteger_t2191_0_0_0;
extern Il2CppType BigInteger_t2191_0_0_0;
extern Il2CppType ConfidenceFactor_t2196_0_0_0;
extern Il2CppType ConfidenceFactor_t2196_0_0_0;
static ParameterInfo PrimalityTest_t2296_PrimalityTest_Invoke_m9793_ParameterInfos[] = 
{
	{"bi", 0, 134218552, 0, &BigInteger_t2191_0_0_0},
	{"confidence", 1, 134218553, 0, &ConfidenceFactor_t2196_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
MethodInfo PrimalityTest_Invoke_m9793_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m9793/* method */
	, &PrimalityTest_t2296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Int32_t189/* invoker_method */
	, PrimalityTest_t2296_PrimalityTest_Invoke_m9793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BigInteger_t2191_0_0_0;
extern Il2CppType ConfidenceFactor_t2196_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo PrimalityTest_t2296_PrimalityTest_BeginInvoke_m9794_ParameterInfos[] = 
{
	{"bi", 0, 134218554, 0, &BigInteger_t2191_0_0_0},
	{"confidence", 1, 134218555, 0, &ConfidenceFactor_t2196_0_0_0},
	{"callback", 2, 134218556, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134218557, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
MethodInfo PrimalityTest_BeginInvoke_m9794_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m9794/* method */
	, &PrimalityTest_t2296_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t2296_PrimalityTest_BeginInvoke_m9794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo PrimalityTest_t2296_PrimalityTest_EndInvoke_m9795_ParameterInfos[] = 
{
	{"result", 0, 134218558, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
MethodInfo PrimalityTest_EndInvoke_m9795_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m9795/* method */
	, &PrimalityTest_t2296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, PrimalityTest_t2296_PrimalityTest_EndInvoke_m9795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PrimalityTest_t2296_MethodInfos[] =
{
	&PrimalityTest__ctor_m9792_MethodInfo,
	&PrimalityTest_Invoke_m9793_MethodInfo,
	&PrimalityTest_BeginInvoke_m9794_MethodInfo,
	&PrimalityTest_EndInvoke_m9795_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m1272_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m1273_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m1274_MethodInfo;
extern MethodInfo Delegate_Clone_m1275_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m1276_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m1277_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m1278_MethodInfo;
extern MethodInfo PrimalityTest_Invoke_m9793_MethodInfo;
extern MethodInfo PrimalityTest_BeginInvoke_m9794_MethodInfo;
extern MethodInfo PrimalityTest_EndInvoke_m9795_MethodInfo;
static Il2CppMethodReference PrimalityTest_t2296_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&PrimalityTest_Invoke_m9793_MethodInfo,
	&PrimalityTest_BeginInvoke_m9794_MethodInfo,
	&PrimalityTest_EndInvoke_m9795_MethodInfo,
};
static bool PrimalityTest_t2296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICloneable_t309_0_0_0;
extern Il2CppType ISerializable_t310_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t2296_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType PrimalityTest_t2296_0_0_0;
extern Il2CppType PrimalityTest_t2296_1_0_0;
extern Il2CppType MulticastDelegate_t22_0_0_0;
struct PrimalityTest_t2296;
const Il2CppTypeDefinitionMetadata PrimalityTest_t2296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t2296_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, PrimalityTest_t2296_VTable/* vtableMethods */
	, PrimalityTest_t2296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PrimalityTest_t2296_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t2296_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &PrimalityTest_t2296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t2296_0_0_0/* byval_arg */
	, &PrimalityTest_t2296_1_0_0/* this_arg */
	, &PrimalityTest_t2296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t2296/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t2296)/* instance_size */
	, sizeof (PrimalityTest_t2296)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t2274_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo CertificateValidationCallback_t2274_CertificateValidationCallback__ctor_m9796_ParameterInfos[] = 
{
	{"object", 0, 134218559, 0, &Object_t_0_0_0},
	{"method", 1, 134218560, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback::.ctor(System.Object,System.IntPtr)
MethodInfo CertificateValidationCallback__ctor_m9796_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback__ctor_m9796/* method */
	, &CertificateValidationCallback_t2274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback_t2274_CertificateValidationCallback__ctor_m9796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
static ParameterInfo CertificateValidationCallback_t2274_CertificateValidationCallback_Invoke_m9797_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, 0, &X509Certificate_t2026_0_0_0},
	{"certificateErrors", 1, 134218562, 0, &Int32U5BU5D_t107_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
MethodInfo CertificateValidationCallback_Invoke_m9797_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_Invoke_m9797/* method */
	, &CertificateValidationCallback_t2274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t2274_CertificateValidationCallback_Invoke_m9797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CertificateValidationCallback_t2274_CertificateValidationCallback_BeginInvoke_m9798_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, 0, &X509Certificate_t2026_0_0_0},
	{"certificateErrors", 1, 134218564, 0, &Int32U5BU5D_t107_0_0_0},
	{"callback", 2, 134218565, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134218566, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[],System.AsyncCallback,System.Object)
MethodInfo CertificateValidationCallback_BeginInvoke_m9798_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_BeginInvoke_m9798/* method */
	, &CertificateValidationCallback_t2274_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t2274_CertificateValidationCallback_BeginInvoke_m9798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo CertificateValidationCallback_t2274_CertificateValidationCallback_EndInvoke_m9799_ParameterInfos[] = 
{
	{"result", 0, 134218567, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::EndInvoke(System.IAsyncResult)
MethodInfo CertificateValidationCallback_EndInvoke_m9799_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_EndInvoke_m9799/* method */
	, &CertificateValidationCallback_t2274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, CertificateValidationCallback_t2274_CertificateValidationCallback_EndInvoke_m9799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CertificateValidationCallback_t2274_MethodInfos[] =
{
	&CertificateValidationCallback__ctor_m9796_MethodInfo,
	&CertificateValidationCallback_Invoke_m9797_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m9798_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m9799_MethodInfo,
	NULL
};
extern MethodInfo CertificateValidationCallback_Invoke_m9797_MethodInfo;
extern MethodInfo CertificateValidationCallback_BeginInvoke_m9798_MethodInfo;
extern MethodInfo CertificateValidationCallback_EndInvoke_m9799_MethodInfo;
static Il2CppMethodReference CertificateValidationCallback_t2274_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&CertificateValidationCallback_Invoke_m9797_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m9798_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m9799_MethodInfo,
};
static bool CertificateValidationCallback_t2274_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t2274_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType CertificateValidationCallback_t2274_0_0_0;
extern Il2CppType CertificateValidationCallback_t2274_1_0_0;
struct CertificateValidationCallback_t2274;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t2274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t2274_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, CertificateValidationCallback_t2274_VTable/* vtableMethods */
	, CertificateValidationCallback_t2274_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CertificateValidationCallback_t2274_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback_t2274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CertificateValidationCallback_t2274_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t2274_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t2274_1_0_0/* this_arg */
	, &CertificateValidationCallback_t2274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t2274/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t2274)/* instance_size */
	, sizeof (CertificateValidationCallback_t2274)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t2275_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo CertificateValidationCallback2_t2275_CertificateValidationCallback2__ctor_m9800_ParameterInfos[] = 
{
	{"object", 0, 134218568, 0, &Object_t_0_0_0},
	{"method", 1, 134218569, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback2::.ctor(System.Object,System.IntPtr)
MethodInfo CertificateValidationCallback2__ctor_m9800_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback2__ctor_m9800/* method */
	, &CertificateValidationCallback2_t2275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback2_t2275_CertificateValidationCallback2__ctor_m9800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509CertificateCollection_t2153_0_0_0;
static ParameterInfo CertificateValidationCallback2_t2275_CertificateValidationCallback2_Invoke_m9801_ParameterInfos[] = 
{
	{"collection", 0, 134218570, 0, &X509CertificateCollection_t2153_0_0_0},
};
extern Il2CppType ValidationResult_t2273_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::Invoke(Mono.Security.X509.X509CertificateCollection)
MethodInfo CertificateValidationCallback2_Invoke_m9801_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_Invoke_m9801/* method */
	, &CertificateValidationCallback2_t2275_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t2273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t2275_CertificateValidationCallback2_Invoke_m9801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509CertificateCollection_t2153_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CertificateValidationCallback2_t2275_CertificateValidationCallback2_BeginInvoke_m9802_ParameterInfos[] = 
{
	{"collection", 0, 134218571, 0, &X509CertificateCollection_t2153_0_0_0},
	{"callback", 1, 134218572, 0, &AsyncCallback_t20_0_0_0},
	{"object", 2, 134218573, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::BeginInvoke(Mono.Security.X509.X509CertificateCollection,System.AsyncCallback,System.Object)
MethodInfo CertificateValidationCallback2_BeginInvoke_m9802_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_BeginInvoke_m9802/* method */
	, &CertificateValidationCallback2_t2275_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t2275_CertificateValidationCallback2_BeginInvoke_m9802_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo CertificateValidationCallback2_t2275_CertificateValidationCallback2_EndInvoke_m9803_ParameterInfos[] = 
{
	{"result", 0, 134218574, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType ValidationResult_t2273_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::EndInvoke(System.IAsyncResult)
MethodInfo CertificateValidationCallback2_EndInvoke_m9803_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_EndInvoke_m9803/* method */
	, &CertificateValidationCallback2_t2275_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t2273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t2275_CertificateValidationCallback2_EndInvoke_m9803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CertificateValidationCallback2_t2275_MethodInfos[] =
{
	&CertificateValidationCallback2__ctor_m9800_MethodInfo,
	&CertificateValidationCallback2_Invoke_m9801_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m9802_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m9803_MethodInfo,
	NULL
};
extern MethodInfo CertificateValidationCallback2_Invoke_m9801_MethodInfo;
extern MethodInfo CertificateValidationCallback2_BeginInvoke_m9802_MethodInfo;
extern MethodInfo CertificateValidationCallback2_EndInvoke_m9803_MethodInfo;
static Il2CppMethodReference CertificateValidationCallback2_t2275_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&CertificateValidationCallback2_Invoke_m9801_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m9802_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m9803_MethodInfo,
};
static bool CertificateValidationCallback2_t2275_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t2275_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType CertificateValidationCallback2_t2275_0_0_0;
extern Il2CppType CertificateValidationCallback2_t2275_1_0_0;
struct CertificateValidationCallback2_t2275;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t2275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t2275_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, CertificateValidationCallback2_t2275_VTable/* vtableMethods */
	, CertificateValidationCallback2_t2275_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CertificateValidationCallback2_t2275_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback2_t2275_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CertificateValidationCallback2_t2275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t2275_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t2275_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t2275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t2275/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t2275)/* instance_size */
	, sizeof (CertificateValidationCallback2_t2275)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t2259_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo CertificateSelectionCallback_t2259_CertificateSelectionCallback__ctor_m9804_ParameterInfos[] = 
{
	{"object", 0, 134218575, 0, &Object_t_0_0_0},
	{"method", 1, 134218576, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
MethodInfo CertificateSelectionCallback__ctor_m9804_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateSelectionCallback__ctor_m9804/* method */
	, &CertificateSelectionCallback_t2259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, CertificateSelectionCallback_t2259_CertificateSelectionCallback__ctor_m9804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509CertificateCollection_t1999_0_0_0;
extern Il2CppType X509CertificateCollection_t1999_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType X509CertificateCollection_t1999_0_0_0;
static ParameterInfo CertificateSelectionCallback_t2259_CertificateSelectionCallback_Invoke_m9805_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, 0, &X509CertificateCollection_t1999_0_0_0},
	{"serverCertificate", 1, 134218578, 0, &X509Certificate_t2026_0_0_0},
	{"targetHost", 2, 134218579, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, 0, &X509CertificateCollection_t1999_0_0_0},
};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
MethodInfo CertificateSelectionCallback_Invoke_m9805_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_Invoke_m9805/* method */
	, &CertificateSelectionCallback_t2259_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t2026_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t2259_CertificateSelectionCallback_Invoke_m9805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509CertificateCollection_t1999_0_0_0;
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType X509CertificateCollection_t1999_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CertificateSelectionCallback_t2259_CertificateSelectionCallback_BeginInvoke_m9806_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, 0, &X509CertificateCollection_t1999_0_0_0},
	{"serverCertificate", 1, 134218582, 0, &X509Certificate_t2026_0_0_0},
	{"targetHost", 2, 134218583, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, 0, &X509CertificateCollection_t1999_0_0_0},
	{"callback", 4, 134218585, 0, &AsyncCallback_t20_0_0_0},
	{"object", 5, 134218586, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateSelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.AsyncCallback,System.Object)
MethodInfo CertificateSelectionCallback_BeginInvoke_m9806_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_BeginInvoke_m9806/* method */
	, &CertificateSelectionCallback_t2259_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t2259_CertificateSelectionCallback_BeginInvoke_m9806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo CertificateSelectionCallback_t2259_CertificateSelectionCallback_EndInvoke_m9807_ParameterInfos[] = 
{
	{"result", 0, 134218587, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::EndInvoke(System.IAsyncResult)
MethodInfo CertificateSelectionCallback_EndInvoke_m9807_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_EndInvoke_m9807/* method */
	, &CertificateSelectionCallback_t2259_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t2026_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t2259_CertificateSelectionCallback_EndInvoke_m9807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CertificateSelectionCallback_t2259_MethodInfos[] =
{
	&CertificateSelectionCallback__ctor_m9804_MethodInfo,
	&CertificateSelectionCallback_Invoke_m9805_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m9806_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m9807_MethodInfo,
	NULL
};
extern MethodInfo CertificateSelectionCallback_Invoke_m9805_MethodInfo;
extern MethodInfo CertificateSelectionCallback_BeginInvoke_m9806_MethodInfo;
extern MethodInfo CertificateSelectionCallback_EndInvoke_m9807_MethodInfo;
static Il2CppMethodReference CertificateSelectionCallback_t2259_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&CertificateSelectionCallback_Invoke_m9805_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m9806_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m9807_MethodInfo,
};
static bool CertificateSelectionCallback_t2259_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t2259_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType CertificateSelectionCallback_t2259_0_0_0;
extern Il2CppType CertificateSelectionCallback_t2259_1_0_0;
struct CertificateSelectionCallback_t2259;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t2259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t2259_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, CertificateSelectionCallback_t2259_VTable/* vtableMethods */
	, CertificateSelectionCallback_t2259_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CertificateSelectionCallback_t2259_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateSelectionCallback_t2259_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CertificateSelectionCallback_t2259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t2259_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t2259_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t2259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t2259/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t2259)/* instance_size */
	, sizeof (CertificateSelectionCallback_t2259)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback__ctor_m9808_ParameterInfos[] = 
{
	{"object", 0, 134218588, 0, &Object_t_0_0_0},
	{"method", 1, 134218589, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::.ctor(System.Object,System.IntPtr)
MethodInfo PrivateKeySelectionCallback__ctor_m9808_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback__ctor_m9808/* method */
	, &PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback__ctor_m9808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_Invoke_m9809_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, 0, &X509Certificate_t2026_0_0_0},
	{"targetHost", 1, 134218591, 0, &String_t_0_0_0},
};
extern Il2CppType AsymmetricAlgorithm_t2013_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
MethodInfo PrivateKeySelectionCallback_Invoke_m9809_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_Invoke_m9809/* method */
	, &PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_Invoke_m9809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType X509Certificate_t2026_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_BeginInvoke_m9810_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, 0, &X509Certificate_t2026_0_0_0},
	{"targetHost", 1, 134218593, 0, &String_t_0_0_0},
	{"callback", 2, 134218594, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134218595, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.AsyncCallback,System.Object)
MethodInfo PrivateKeySelectionCallback_BeginInvoke_m9810_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_BeginInvoke_m9810/* method */
	, &PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_BeginInvoke_m9810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_EndInvoke_m9811_ParameterInfos[] = 
{
	{"result", 0, 134218596, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType AsymmetricAlgorithm_t2013_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::EndInvoke(System.IAsyncResult)
MethodInfo PrivateKeySelectionCallback_EndInvoke_m9811_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_EndInvoke_m9811/* method */
	, &PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t2013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t2260_PrivateKeySelectionCallback_EndInvoke_m9811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PrivateKeySelectionCallback_t2260_MethodInfos[] =
{
	&PrivateKeySelectionCallback__ctor_m9808_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m9809_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m9810_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m9811_MethodInfo,
	NULL
};
extern MethodInfo PrivateKeySelectionCallback_Invoke_m9809_MethodInfo;
extern MethodInfo PrivateKeySelectionCallback_BeginInvoke_m9810_MethodInfo;
extern MethodInfo PrivateKeySelectionCallback_EndInvoke_m9811_MethodInfo;
static Il2CppMethodReference PrivateKeySelectionCallback_t2260_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m9809_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m9810_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m9811_MethodInfo,
};
static bool PrivateKeySelectionCallback_t2260_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t2260_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType PrivateKeySelectionCallback_t2260_0_0_0;
extern Il2CppType PrivateKeySelectionCallback_t2260_1_0_0;
struct PrivateKeySelectionCallback_t2260;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t2260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t2260_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, PrivateKeySelectionCallback_t2260_VTable/* vtableMethods */
	, PrivateKeySelectionCallback_t2260_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, PrivateKeySelectionCallback_t2260_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t2260_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t2260_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t2260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t2260/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t2260)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t2260)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t2297_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static MethodInfo* U24ArrayTypeU243132_t2297_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference U24ArrayTypeU243132_t2297_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU243132_t2297_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU243132_t2297_0_0_0;
extern Il2CppType U24ArrayTypeU243132_t2297_1_0_0;
extern Il2CppType ValueType_t329_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t2306_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t2297_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU243132_t2297_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t2297_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU243132_t2297_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t2297_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU243132_t2297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t2297_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t2297_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t2297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t2297_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2297_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2297_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t2297)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t2297)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t2297_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t2298_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU24256_t2298_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU24256_t2298_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU24256_t2298_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU24256_t2298_0_0_0;
extern Il2CppType U24ArrayTypeU24256_t2298_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t2298_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU24256_t2298_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t2298_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU24256_t2298_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t2298_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU24256_t2298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t2298_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t2298_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t2298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t2298_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2298_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2298_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t2298)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t2298)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t2298_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t2299_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2420_t2299_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2420_t2299_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2420_t2299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2420_t2299_0_0_0;
extern Il2CppType U24ArrayTypeU2420_t2299_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t2299_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2420_t2299_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t2299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2420_t2299_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t2299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2420_t2299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t2299_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t2299_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t2299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t2299_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2299_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2299_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t2299)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t2299)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t2299_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t2300_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2432_t2300_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2432_t2300_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2432_t2300_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2432_t2300_0_0_0;
extern Il2CppType U24ArrayTypeU2432_t2300_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t2300_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2432_t2300_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t2300_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2432_t2300_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t2300_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2432_t2300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t2300_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t2300_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t2300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t2300_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2300_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2300_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t2300)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t2300)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t2300_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t2301_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2448_t2301_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2448_t2301_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2448_t2301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2448_t2301_0_0_0;
extern Il2CppType U24ArrayTypeU2448_t2301_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t2301_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2448_t2301_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t2301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2448_t2301_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t2301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2448_t2301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t2301_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t2301_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t2301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t2301_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2301_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2301_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t2301)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t2301)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t2301_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t2302_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2464_t2302_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2464_t2302_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2464_t2302_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2464_t2302_0_0_0;
extern Il2CppType U24ArrayTypeU2464_t2302_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t2302_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2464_t2302_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t2302_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2464_t2302_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t2302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2464_t2302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t2302_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t2302_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t2302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t2302_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2302_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2302_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t2302)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t2302)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t2302_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2303_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2412_t2303_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2412_t2303_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2412_t2303_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2412_t2303_0_0_0;
extern Il2CppType U24ArrayTypeU2412_t2303_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2303_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2412_t2303_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t2303_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2412_t2303_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t2303_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2412_t2303_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2303_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2303_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2303_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2303_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2303_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2303)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2303)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2303_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t2304_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU2416_t2304_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU2416_t2304_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU2416_t2304_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU2416_t2304_0_0_0;
extern Il2CppType U24ArrayTypeU2416_t2304_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t2304_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU2416_t2304_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t2304_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU2416_t2304_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t2304_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU2416_t2304_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t2304_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t2304_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t2304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t2304_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2304_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2304_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t2304)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t2304)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t2304_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t2305_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static MethodInfo* U24ArrayTypeU244_t2305_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference U24ArrayTypeU244_t2305_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU244_t2305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U24ArrayTypeU244_t2305_0_0_0;
extern Il2CppType U24ArrayTypeU244_t2305_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t2305_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU244_t2305_VTable/* vtableMethods */
	, U24ArrayTypeU244_t2305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU244_t2305_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU244_t2305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU244_t2305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t2305_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t2305_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t2305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t2305_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t2305_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t2305_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t2305)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t2305)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t2305_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static MethodInfo* U3CPrivateImplementationDetailsU3E_t2306_MethodInfos[] =
{
	NULL
};
extern Il2CppType U24ArrayTypeU243132_t2297_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_FieldInfo = 
{
	"$$field-0"/* name */
	, &U24ArrayTypeU243132_t2297_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU24256_t2298_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_FieldInfo = 
{
	"$$field-5"/* name */
	, &U24ArrayTypeU24256_t2298_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D5_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2420_t2299_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_FieldInfo = 
{
	"$$field-6"/* name */
	, &U24ArrayTypeU2420_t2299_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D6_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2432_t2300_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_FieldInfo = 
{
	"$$field-7"/* name */
	, &U24ArrayTypeU2432_t2300_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D7_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2448_t2301_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_FieldInfo = 
{
	"$$field-8"/* name */
	, &U24ArrayTypeU2448_t2301_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D8_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2464_t2302_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_FieldInfo = 
{
	"$$field-9"/* name */
	, &U24ArrayTypeU2464_t2302_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D9_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2464_t2302_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_FieldInfo = 
{
	"$$field-11"/* name */
	, &U24ArrayTypeU2464_t2302_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D11_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2464_t2302_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_FieldInfo = 
{
	"$$field-12"/* name */
	, &U24ArrayTypeU2464_t2302_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D12_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2464_t2302_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_FieldInfo = 
{
	"$$field-13"/* name */
	, &U24ArrayTypeU2464_t2302_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D13_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2412_t2303_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_FieldInfo = 
{
	"$$field-14"/* name */
	, &U24ArrayTypeU2412_t2303_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D14_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2412_t2303_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_FieldInfo = 
{
	"$$field-15"/* name */
	, &U24ArrayTypeU2412_t2303_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D15_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2412_t2303_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_FieldInfo = 
{
	"$$field-16"/* name */
	, &U24ArrayTypeU2412_t2303_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D16_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU2416_t2304_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_FieldInfo = 
{
	"$$field-17"/* name */
	, &U24ArrayTypeU2416_t2304_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D17_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU244_t2305_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_FieldInfo = 
{
	"$$field-21"/* name */
	, &U24ArrayTypeU244_t2305_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D21_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType U24ArrayTypeU244_t2305_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_FieldInfo = 
{
	"$$field-22"/* name */
	, &U24ArrayTypeU244_t2305_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields, ___U24U24fieldU2D22_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3E_t2306_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_FieldInfo,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_DefaultValueData[] = { 0x2, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0, 0x5, 0x0, 0x0, 0x0, 0x7, 0x0, 0x0, 0x0, 0xB, 0x0, 0x0, 0x0, 0xD, 0x0, 0x0, 0x0, 0x11, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x17, 0x0, 0x0, 0x0, 0x1D, 0x0, 0x0, 0x0, 0x1F, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x29, 0x0, 0x0, 0x0, 0x2B, 0x0, 0x0, 0x0, 0x2F, 0x0, 0x0, 0x0, 0x35, 0x0, 0x0, 0x0, 0x3B, 0x0, 0x0, 0x0, 0x3D, 0x0, 0x0, 0x0, 0x43, 0x0, 0x0, 0x0, 0x47, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x4F, 0x0, 0x0, 0x0, 0x53, 0x0, 0x0, 0x0, 0x59, 0x0, 0x0, 0x0, 0x61, 0x0, 0x0, 0x0, 0x65, 0x0, 0x0, 0x0, 0x67, 0x0, 0x0, 0x0, 0x6B, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0x71, 0x0, 0x0, 0x0, 0x7F, 0x0, 0x0, 0x0, 0x83, 0x0, 0x0, 0x0, 0x89, 0x0, 0x0, 0x0, 0x8B, 0x0, 0x0, 0x0, 0x95, 0x0, 0x0, 0x0, 0x97, 0x0, 0x0, 0x0, 0x9D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xA7, 0x0, 0x0, 0x0, 0xAD, 0x0, 0x0, 0x0, 0xB3, 0x0, 0x0, 0x0, 0xB5, 0x0, 0x0, 0x0, 0xBF, 0x0, 0x0, 0x0, 0xC1, 0x0, 0x0, 0x0, 0xC5, 0x0, 0x0, 0x0, 0xC7, 0x0, 0x0, 0x0, 0xD3, 0x0, 0x0, 0x0, 0xDF, 0x0, 0x0, 0x0, 0xE3, 0x0, 0x0, 0x0, 0xE5, 0x0, 0x0, 0x0, 0xE9, 0x0, 0x0, 0x0, 0xEF, 0x0, 0x0, 0x0, 0xF1, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x7, 0x1, 0x0, 0x0, 0xD, 0x1, 0x0, 0x0, 0xF, 0x1, 0x0, 0x0, 0x15, 0x1, 0x0, 0x0, 0x19, 0x1, 0x0, 0x0, 0x1B, 0x1, 0x0, 0x0, 0x25, 0x1, 0x0, 0x0, 0x33, 0x1, 0x0, 0x0, 0x37, 0x1, 0x0, 0x0, 0x39, 0x1, 0x0, 0x0, 0x3D, 0x1, 0x0, 0x0, 0x4B, 0x1, 0x0, 0x0, 0x51, 0x1, 0x0, 0x0, 0x5B, 0x1, 0x0, 0x0, 0x5D, 0x1, 0x0, 0x0, 0x61, 0x1, 0x0, 0x0, 0x67, 0x1, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x75, 0x1, 0x0, 0x0, 0x7B, 0x1, 0x0, 0x0, 0x7F, 0x1, 0x0, 0x0, 0x85, 0x1, 0x0, 0x0, 0x8D, 0x1, 0x0, 0x0, 0x91, 0x1, 0x0, 0x0, 0x99, 0x1, 0x0, 0x0, 0xA3, 0x1, 0x0, 0x0, 0xA5, 0x1, 0x0, 0x0, 0xAF, 0x1, 0x0, 0x0, 0xB1, 0x1, 0x0, 0x0, 0xB7, 0x1, 0x0, 0x0, 0xBB, 0x1, 0x0, 0x0, 0xC1, 0x1, 0x0, 0x0, 0xC9, 0x1, 0x0, 0x0, 0xCD, 0x1, 0x0, 0x0, 0xCF, 0x1, 0x0, 0x0, 0xD3, 0x1, 0x0, 0x0, 0xDF, 0x1, 0x0, 0x0, 0xE7, 0x1, 0x0, 0x0, 0xEB, 0x1, 0x0, 0x0, 0xF3, 0x1, 0x0, 0x0, 0xF7, 0x1, 0x0, 0x0, 0xFD, 0x1, 0x0, 0x0, 0x9, 0x2, 0x0, 0x0, 0xB, 0x2, 0x0, 0x0, 0x1D, 0x2, 0x0, 0x0, 0x23, 0x2, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x33, 0x2, 0x0, 0x0, 0x39, 0x2, 0x0, 0x0, 0x3B, 0x2, 0x0, 0x0, 0x41, 0x2, 0x0, 0x0, 0x4B, 0x2, 0x0, 0x0, 0x51, 0x2, 0x0, 0x0, 0x57, 0x2, 0x0, 0x0, 0x59, 0x2, 0x0, 0x0, 0x5F, 0x2, 0x0, 0x0, 0x65, 0x2, 0x0, 0x0, 0x69, 0x2, 0x0, 0x0, 0x6B, 0x2, 0x0, 0x0, 0x77, 0x2, 0x0, 0x0, 0x81, 0x2, 0x0, 0x0, 0x83, 0x2, 0x0, 0x0, 0x87, 0x2, 0x0, 0x0, 0x8D, 0x2, 0x0, 0x0, 0x93, 0x2, 0x0, 0x0, 0x95, 0x2, 0x0, 0x0, 0xA1, 0x2, 0x0, 0x0, 0xA5, 0x2, 0x0, 0x0, 0xAB, 0x2, 0x0, 0x0, 0xB3, 0x2, 0x0, 0x0, 0xBD, 0x2, 0x0, 0x0, 0xC5, 0x2, 0x0, 0x0, 0xCF, 0x2, 0x0, 0x0, 0xD7, 0x2, 0x0, 0x0, 0xDD, 0x2, 0x0, 0x0, 0xE3, 0x2, 0x0, 0x0, 0xE7, 0x2, 0x0, 0x0, 0xEF, 0x2, 0x0, 0x0, 0xF5, 0x2, 0x0, 0x0, 0xF9, 0x2, 0x0, 0x0, 0x1, 0x3, 0x0, 0x0, 0x5, 0x3, 0x0, 0x0, 0x13, 0x3, 0x0, 0x0, 0x1D, 0x3, 0x0, 0x0, 0x29, 0x3, 0x0, 0x0, 0x2B, 0x3, 0x0, 0x0, 0x35, 0x3, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0x3B, 0x3, 0x0, 0x0, 0x3D, 0x3, 0x0, 0x0, 0x47, 0x3, 0x0, 0x0, 0x55, 0x3, 0x0, 0x0, 0x59, 0x3, 0x0, 0x0, 0x5B, 0x3, 0x0, 0x0, 0x5F, 0x3, 0x0, 0x0, 0x6D, 0x3, 0x0, 0x0, 0x71, 0x3, 0x0, 0x0, 0x73, 0x3, 0x0, 0x0, 0x77, 0x3, 0x0, 0x0, 0x8B, 0x3, 0x0, 0x0, 0x8F, 0x3, 0x0, 0x0, 0x97, 0x3, 0x0, 0x0, 0xA1, 0x3, 0x0, 0x0, 0xA9, 0x3, 0x0, 0x0, 0xAD, 0x3, 0x0, 0x0, 0xB3, 0x3, 0x0, 0x0, 0xB9, 0x3, 0x0, 0x0, 0xC7, 0x3, 0x0, 0x0, 0xCB, 0x3, 0x0, 0x0, 0xD1, 0x3, 0x0, 0x0, 0xD7, 0x3, 0x0, 0x0, 0xDF, 0x3, 0x0, 0x0, 0xE5, 0x3, 0x0, 0x0, 0xF1, 0x3, 0x0, 0x0, 0xF5, 0x3, 0x0, 0x0, 0xFB, 0x3, 0x0, 0x0, 0xFD, 0x3, 0x0, 0x0, 0x7, 0x4, 0x0, 0x0, 0x9, 0x4, 0x0, 0x0, 0xF, 0x4, 0x0, 0x0, 0x19, 0x4, 0x0, 0x0, 0x1B, 0x4, 0x0, 0x0, 0x25, 0x4, 0x0, 0x0, 0x27, 0x4, 0x0, 0x0, 0x2D, 0x4, 0x0, 0x0, 0x3F, 0x4, 0x0, 0x0, 0x43, 0x4, 0x0, 0x0, 0x45, 0x4, 0x0, 0x0, 0x49, 0x4, 0x0, 0x0, 0x4F, 0x4, 0x0, 0x0, 0x55, 0x4, 0x0, 0x0, 0x5D, 0x4, 0x0, 0x0, 0x63, 0x4, 0x0, 0x0, 0x69, 0x4, 0x0, 0x0, 0x7F, 0x4, 0x0, 0x0, 0x81, 0x4, 0x0, 0x0, 0x8B, 0x4, 0x0, 0x0, 0x93, 0x4, 0x0, 0x0, 0x9D, 0x4, 0x0, 0x0, 0xA3, 0x4, 0x0, 0x0, 0xA9, 0x4, 0x0, 0x0, 0xB1, 0x4, 0x0, 0x0, 0xBD, 0x4, 0x0, 0x0, 0xC1, 0x4, 0x0, 0x0, 0xC7, 0x4, 0x0, 0x0, 0xCD, 0x4, 0x0, 0x0, 0xCF, 0x4, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0xE1, 0x4, 0x0, 0x0, 0xEB, 0x4, 0x0, 0x0, 0xFD, 0x4, 0x0, 0x0, 0xFF, 0x4, 0x0, 0x0, 0x3, 0x5, 0x0, 0x0, 0x9, 0x5, 0x0, 0x0, 0xB, 0x5, 0x0, 0x0, 0x11, 0x5, 0x0, 0x0, 0x15, 0x5, 0x0, 0x0, 0x17, 0x5, 0x0, 0x0, 0x1B, 0x5, 0x0, 0x0, 0x27, 0x5, 0x0, 0x0, 0x29, 0x5, 0x0, 0x0, 0x2F, 0x5, 0x0, 0x0, 0x51, 0x5, 0x0, 0x0, 0x57, 0x5, 0x0, 0x0, 0x5D, 0x5, 0x0, 0x0, 0x65, 0x5, 0x0, 0x0, 0x77, 0x5, 0x0, 0x0, 0x81, 0x5, 0x0, 0x0, 0x8F, 0x5, 0x0, 0x0, 0x93, 0x5, 0x0, 0x0, 0x95, 0x5, 0x0, 0x0, 0x99, 0x5, 0x0, 0x0, 0x9F, 0x5, 0x0, 0x0, 0xA7, 0x5, 0x0, 0x0, 0xAB, 0x5, 0x0, 0x0, 0xAD, 0x5, 0x0, 0x0, 0xB3, 0x5, 0x0, 0x0, 0xBF, 0x5, 0x0, 0x0, 0xC9, 0x5, 0x0, 0x0, 0xCB, 0x5, 0x0, 0x0, 0xCF, 0x5, 0x0, 0x0, 0xD1, 0x5, 0x0, 0x0, 0xD5, 0x5, 0x0, 0x0, 0xDB, 0x5, 0x0, 0x0, 0xE7, 0x5, 0x0, 0x0, 0xF3, 0x5, 0x0, 0x0, 0xFB, 0x5, 0x0, 0x0, 0x7, 0x6, 0x0, 0x0, 0xD, 0x6, 0x0, 0x0, 0x11, 0x6, 0x0, 0x0, 0x17, 0x6, 0x0, 0x0, 0x1F, 0x6, 0x0, 0x0, 0x23, 0x6, 0x0, 0x0, 0x2B, 0x6, 0x0, 0x0, 0x2F, 0x6, 0x0, 0x0, 0x3D, 0x6, 0x0, 0x0, 0x41, 0x6, 0x0, 0x0, 0x47, 0x6, 0x0, 0x0, 0x49, 0x6, 0x0, 0x0, 0x4D, 0x6, 0x0, 0x0, 0x53, 0x6, 0x0, 0x0, 0x55, 0x6, 0x0, 0x0, 0x5B, 0x6, 0x0, 0x0, 0x65, 0x6, 0x0, 0x0, 0x79, 0x6, 0x0, 0x0, 0x7F, 0x6, 0x0, 0x0, 0x83, 0x6, 0x0, 0x0, 0x85, 0x6, 0x0, 0x0, 0x9D, 0x6, 0x0, 0x0, 0xA1, 0x6, 0x0, 0x0, 0xA3, 0x6, 0x0, 0x0, 0xAD, 0x6, 0x0, 0x0, 0xB9, 0x6, 0x0, 0x0, 0xBB, 0x6, 0x0, 0x0, 0xC5, 0x6, 0x0, 0x0, 0xCD, 0x6, 0x0, 0x0, 0xD3, 0x6, 0x0, 0x0, 0xD9, 0x6, 0x0, 0x0, 0xDF, 0x6, 0x0, 0x0, 0xF1, 0x6, 0x0, 0x0, 0xF7, 0x6, 0x0, 0x0, 0xFB, 0x6, 0x0, 0x0, 0xFD, 0x6, 0x0, 0x0, 0x9, 0x7, 0x0, 0x0, 0x13, 0x7, 0x0, 0x0, 0x1F, 0x7, 0x0, 0x0, 0x27, 0x7, 0x0, 0x0, 0x37, 0x7, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0x4B, 0x7, 0x0, 0x0, 0x4F, 0x7, 0x0, 0x0, 0x51, 0x7, 0x0, 0x0, 0x55, 0x7, 0x0, 0x0, 0x57, 0x7, 0x0, 0x0, 0x61, 0x7, 0x0, 0x0, 0x6D, 0x7, 0x0, 0x0, 0x73, 0x7, 0x0, 0x0, 0x79, 0x7, 0x0, 0x0, 0x8B, 0x7, 0x0, 0x0, 0x8D, 0x7, 0x0, 0x0, 0x9D, 0x7, 0x0, 0x0, 0x9F, 0x7, 0x0, 0x0, 0xB5, 0x7, 0x0, 0x0, 0xBB, 0x7, 0x0, 0x0, 0xC3, 0x7, 0x0, 0x0, 0xC9, 0x7, 0x0, 0x0, 0xCD, 0x7, 0x0, 0x0, 0xCF, 0x7, 0x0, 0x0, 0xD3, 0x7, 0x0, 0x0, 0xDB, 0x7, 0x0, 0x0, 0xE1, 0x7, 0x0, 0x0, 0xEB, 0x7, 0x0, 0x0, 0xED, 0x7, 0x0, 0x0, 0xF7, 0x7, 0x0, 0x0, 0x5, 0x8, 0x0, 0x0, 0xF, 0x8, 0x0, 0x0, 0x15, 0x8, 0x0, 0x0, 0x21, 0x8, 0x0, 0x0, 0x23, 0x8, 0x0, 0x0, 0x27, 0x8, 0x0, 0x0, 0x29, 0x8, 0x0, 0x0, 0x33, 0x8, 0x0, 0x0, 0x3F, 0x8, 0x0, 0x0, 0x41, 0x8, 0x0, 0x0, 0x51, 0x8, 0x0, 0x0, 0x53, 0x8, 0x0, 0x0, 0x59, 0x8, 0x0, 0x0, 0x5D, 0x8, 0x0, 0x0, 0x5F, 0x8, 0x0, 0x0, 0x69, 0x8, 0x0, 0x0, 0x71, 0x8, 0x0, 0x0, 0x83, 0x8, 0x0, 0x0, 0x9B, 0x8, 0x0, 0x0, 0x9F, 0x8, 0x0, 0x0, 0xA5, 0x8, 0x0, 0x0, 0xAD, 0x8, 0x0, 0x0, 0xBD, 0x8, 0x0, 0x0, 0xBF, 0x8, 0x0, 0x0, 0xC3, 0x8, 0x0, 0x0, 0xCB, 0x8, 0x0, 0x0, 0xDB, 0x8, 0x0, 0x0, 0xDD, 0x8, 0x0, 0x0, 0xE1, 0x8, 0x0, 0x0, 0xE9, 0x8, 0x0, 0x0, 0xEF, 0x8, 0x0, 0x0, 0xF5, 0x8, 0x0, 0x0, 0xF9, 0x8, 0x0, 0x0, 0x5, 0x9, 0x0, 0x0, 0x7, 0x9, 0x0, 0x0, 0x1D, 0x9, 0x0, 0x0, 0x23, 0x9, 0x0, 0x0, 0x25, 0x9, 0x0, 0x0, 0x2B, 0x9, 0x0, 0x0, 0x2F, 0x9, 0x0, 0x0, 0x35, 0x9, 0x0, 0x0, 0x43, 0x9, 0x0, 0x0, 0x49, 0x9, 0x0, 0x0, 0x4D, 0x9, 0x0, 0x0, 0x4F, 0x9, 0x0, 0x0, 0x55, 0x9, 0x0, 0x0, 0x59, 0x9, 0x0, 0x0, 0x5F, 0x9, 0x0, 0x0, 0x6B, 0x9, 0x0, 0x0, 0x71, 0x9, 0x0, 0x0, 0x77, 0x9, 0x0, 0x0, 0x85, 0x9, 0x0, 0x0, 0x89, 0x9, 0x0, 0x0, 0x8F, 0x9, 0x0, 0x0, 0x9B, 0x9, 0x0, 0x0, 0xA3, 0x9, 0x0, 0x0, 0xA9, 0x9, 0x0, 0x0, 0xAD, 0x9, 0x0, 0x0, 0xC7, 0x9, 0x0, 0x0, 0xD9, 0x9, 0x0, 0x0, 0xE3, 0x9, 0x0, 0x0, 0xEB, 0x9, 0x0, 0x0, 0xEF, 0x9, 0x0, 0x0, 0xF5, 0x9, 0x0, 0x0, 0xF7, 0x9, 0x0, 0x0, 0xFD, 0x9, 0x0, 0x0, 0x13, 0xA, 0x0, 0x0, 0x1F, 0xA, 0x0, 0x0, 0x21, 0xA, 0x0, 0x0, 0x31, 0xA, 0x0, 0x0, 0x39, 0xA, 0x0, 0x0, 0x3D, 0xA, 0x0, 0x0, 0x49, 0xA, 0x0, 0x0, 0x57, 0xA, 0x0, 0x0, 0x61, 0xA, 0x0, 0x0, 0x63, 0xA, 0x0, 0x0, 0x67, 0xA, 0x0, 0x0, 0x6F, 0xA, 0x0, 0x0, 0x75, 0xA, 0x0, 0x0, 0x7B, 0xA, 0x0, 0x0, 0x7F, 0xA, 0x0, 0x0, 0x81, 0xA, 0x0, 0x0, 0x85, 0xA, 0x0, 0x0, 0x8B, 0xA, 0x0, 0x0, 0x93, 0xA, 0x0, 0x0, 0x97, 0xA, 0x0, 0x0, 0x99, 0xA, 0x0, 0x0, 0x9F, 0xA, 0x0, 0x0, 0xA9, 0xA, 0x0, 0x0, 0xAB, 0xA, 0x0, 0x0, 0xB5, 0xA, 0x0, 0x0, 0xBD, 0xA, 0x0, 0x0, 0xC1, 0xA, 0x0, 0x0, 0xCF, 0xA, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0xE5, 0xA, 0x0, 0x0, 0xE7, 0xA, 0x0, 0x0, 0xED, 0xA, 0x0, 0x0, 0xF1, 0xA, 0x0, 0x0, 0xF3, 0xA, 0x0, 0x0, 0x3, 0xB, 0x0, 0x0, 0x11, 0xB, 0x0, 0x0, 0x15, 0xB, 0x0, 0x0, 0x1B, 0xB, 0x0, 0x0, 0x23, 0xB, 0x0, 0x0, 0x29, 0xB, 0x0, 0x0, 0x2D, 0xB, 0x0, 0x0, 0x3F, 0xB, 0x0, 0x0, 0x47, 0xB, 0x0, 0x0, 0x51, 0xB, 0x0, 0x0, 0x57, 0xB, 0x0, 0x0, 0x5D, 0xB, 0x0, 0x0, 0x65, 0xB, 0x0, 0x0, 0x6F, 0xB, 0x0, 0x0, 0x7B, 0xB, 0x0, 0x0, 0x89, 0xB, 0x0, 0x0, 0x8D, 0xB, 0x0, 0x0, 0x93, 0xB, 0x0, 0x0, 0x99, 0xB, 0x0, 0x0, 0x9B, 0xB, 0x0, 0x0, 0xB7, 0xB, 0x0, 0x0, 0xB9, 0xB, 0x0, 0x0, 0xC3, 0xB, 0x0, 0x0, 0xCB, 0xB, 0x0, 0x0, 0xCF, 0xB, 0x0, 0x0, 0xDD, 0xB, 0x0, 0x0, 0xE1, 0xB, 0x0, 0x0, 0xE9, 0xB, 0x0, 0x0, 0xF5, 0xB, 0x0, 0x0, 0xFB, 0xB, 0x0, 0x0, 0x7, 0xC, 0x0, 0x0, 0xB, 0xC, 0x0, 0x0, 0x11, 0xC, 0x0, 0x0, 0x25, 0xC, 0x0, 0x0, 0x2F, 0xC, 0x0, 0x0, 0x31, 0xC, 0x0, 0x0, 0x41, 0xC, 0x0, 0x0, 0x5B, 0xC, 0x0, 0x0, 0x5F, 0xC, 0x0, 0x0, 0x61, 0xC, 0x0, 0x0, 0x6D, 0xC, 0x0, 0x0, 0x73, 0xC, 0x0, 0x0, 0x77, 0xC, 0x0, 0x0, 0x83, 0xC, 0x0, 0x0, 0x89, 0xC, 0x0, 0x0, 0x91, 0xC, 0x0, 0x0, 0x95, 0xC, 0x0, 0x0, 0x9D, 0xC, 0x0, 0x0, 0xB3, 0xC, 0x0, 0x0, 0xB5, 0xC, 0x0, 0x0, 0xB9, 0xC, 0x0, 0x0, 0xBB, 0xC, 0x0, 0x0, 0xC7, 0xC, 0x0, 0x0, 0xE3, 0xC, 0x0, 0x0, 0xE5, 0xC, 0x0, 0x0, 0xEB, 0xC, 0x0, 0x0, 0xF1, 0xC, 0x0, 0x0, 0xF7, 0xC, 0x0, 0x0, 0xFB, 0xC, 0x0, 0x0, 0x1, 0xD, 0x0, 0x0, 0x3, 0xD, 0x0, 0x0, 0xF, 0xD, 0x0, 0x0, 0x13, 0xD, 0x0, 0x0, 0x1F, 0xD, 0x0, 0x0, 0x21, 0xD, 0x0, 0x0, 0x2B, 0xD, 0x0, 0x0, 0x2D, 0xD, 0x0, 0x0, 0x3D, 0xD, 0x0, 0x0, 0x3F, 0xD, 0x0, 0x0, 0x4F, 0xD, 0x0, 0x0, 0x55, 0xD, 0x0, 0x0, 0x69, 0xD, 0x0, 0x0, 0x79, 0xD, 0x0, 0x0, 0x81, 0xD, 0x0, 0x0, 0x85, 0xD, 0x0, 0x0, 0x87, 0xD, 0x0, 0x0, 0x8B, 0xD, 0x0, 0x0, 0x8D, 0xD, 0x0, 0x0, 0xA3, 0xD, 0x0, 0x0, 0xAB, 0xD, 0x0, 0x0, 0xB7, 0xD, 0x0, 0x0, 0xBD, 0xD, 0x0, 0x0, 0xC7, 0xD, 0x0, 0x0, 0xC9, 0xD, 0x0, 0x0, 0xCD, 0xD, 0x0, 0x0, 0xD3, 0xD, 0x0, 0x0, 0xD5, 0xD, 0x0, 0x0, 0xDB, 0xD, 0x0, 0x0, 0xE5, 0xD, 0x0, 0x0, 0xE7, 0xD, 0x0, 0x0, 0xF3, 0xD, 0x0, 0x0, 0xFD, 0xD, 0x0, 0x0, 0xFF, 0xD, 0x0, 0x0, 0x9, 0xE, 0x0, 0x0, 0x17, 0xE, 0x0, 0x0, 0x1D, 0xE, 0x0, 0x0, 0x21, 0xE, 0x0, 0x0, 0x27, 0xE, 0x0, 0x0, 0x2F, 0xE, 0x0, 0x0, 0x35, 0xE, 0x0, 0x0, 0x3B, 0xE, 0x0, 0x0, 0x4B, 0xE, 0x0, 0x0, 0x57, 0xE, 0x0, 0x0, 0x59, 0xE, 0x0, 0x0, 0x5D, 0xE, 0x0, 0x0, 0x6B, 0xE, 0x0, 0x0, 0x71, 0xE, 0x0, 0x0, 0x75, 0xE, 0x0, 0x0, 0x7D, 0xE, 0x0, 0x0, 0x87, 0xE, 0x0, 0x0, 0x8F, 0xE, 0x0, 0x0, 0x95, 0xE, 0x0, 0x0, 0x9B, 0xE, 0x0, 0x0, 0xB1, 0xE, 0x0, 0x0, 0xB7, 0xE, 0x0, 0x0, 0xB9, 0xE, 0x0, 0x0, 0xC3, 0xE, 0x0, 0x0, 0xD1, 0xE, 0x0, 0x0, 0xD5, 0xE, 0x0, 0x0, 0xDB, 0xE, 0x0, 0x0, 0xED, 0xE, 0x0, 0x0, 0xEF, 0xE, 0x0, 0x0, 0xF9, 0xE, 0x0, 0x0, 0x7, 0xF, 0x0, 0x0, 0xB, 0xF, 0x0, 0x0, 0xD, 0xF, 0x0, 0x0, 0x17, 0xF, 0x0, 0x0, 0x25, 0xF, 0x0, 0x0, 0x29, 0xF, 0x0, 0x0, 0x31, 0xF, 0x0, 0x0, 0x43, 0xF, 0x0, 0x0, 0x47, 0xF, 0x0, 0x0, 0x4D, 0xF, 0x0, 0x0, 0x4F, 0xF, 0x0, 0x0, 0x53, 0xF, 0x0, 0x0, 0x59, 0xF, 0x0, 0x0, 0x5B, 0xF, 0x0, 0x0, 0x67, 0xF, 0x0, 0x0, 0x6B, 0xF, 0x0, 0x0, 0x7F, 0xF, 0x0, 0x0, 0x95, 0xF, 0x0, 0x0, 0xA1, 0xF, 0x0, 0x0, 0xA3, 0xF, 0x0, 0x0, 0xA7, 0xF, 0x0, 0x0, 0xAD, 0xF, 0x0, 0x0, 0xB3, 0xF, 0x0, 0x0, 0xB5, 0xF, 0x0, 0x0, 0xBB, 0xF, 0x0, 0x0, 0xD1, 0xF, 0x0, 0x0, 0xD3, 0xF, 0x0, 0x0, 0xD9, 0xF, 0x0, 0x0, 0xE9, 0xF, 0x0, 0x0, 0xEF, 0xF, 0x0, 0x0, 0xFB, 0xF, 0x0, 0x0, 0xFD, 0xF, 0x0, 0x0, 0x3, 0x10, 0x0, 0x0, 0xF, 0x10, 0x0, 0x0, 0x1F, 0x10, 0x0, 0x0, 0x21, 0x10, 0x0, 0x0, 0x25, 0x10, 0x0, 0x0, 0x2B, 0x10, 0x0, 0x0, 0x39, 0x10, 0x0, 0x0, 0x3D, 0x10, 0x0, 0x0, 0x3F, 0x10, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x69, 0x10, 0x0, 0x0, 0x73, 0x10, 0x0, 0x0, 0x79, 0x10, 0x0, 0x0, 0x7B, 0x10, 0x0, 0x0, 0x85, 0x10, 0x0, 0x0, 0x87, 0x10, 0x0, 0x0, 0x91, 0x10, 0x0, 0x0, 0x93, 0x10, 0x0, 0x0, 0x9D, 0x10, 0x0, 0x0, 0xA3, 0x10, 0x0, 0x0, 0xA5, 0x10, 0x0, 0x0, 0xAF, 0x10, 0x0, 0x0, 0xB1, 0x10, 0x0, 0x0, 0xBB, 0x10, 0x0, 0x0, 0xC1, 0x10, 0x0, 0x0, 0xC9, 0x10, 0x0, 0x0, 0xE7, 0x10, 0x0, 0x0, 0xF1, 0x10, 0x0, 0x0, 0xF3, 0x10, 0x0, 0x0, 0xFD, 0x10, 0x0, 0x0, 0x5, 0x11, 0x0, 0x0, 0xB, 0x11, 0x0, 0x0, 0x15, 0x11, 0x0, 0x0, 0x27, 0x11, 0x0, 0x0, 0x2D, 0x11, 0x0, 0x0, 0x39, 0x11, 0x0, 0x0, 0x45, 0x11, 0x0, 0x0, 0x47, 0x11, 0x0, 0x0, 0x59, 0x11, 0x0, 0x0, 0x5F, 0x11, 0x0, 0x0, 0x63, 0x11, 0x0, 0x0, 0x69, 0x11, 0x0, 0x0, 0x6F, 0x11, 0x0, 0x0, 0x81, 0x11, 0x0, 0x0, 0x83, 0x11, 0x0, 0x0, 0x8D, 0x11, 0x0, 0x0, 0x9B, 0x11, 0x0, 0x0, 0xA1, 0x11, 0x0, 0x0, 0xA5, 0x11, 0x0, 0x0, 0xA7, 0x11, 0x0, 0x0, 0xAB, 0x11, 0x0, 0x0, 0xC3, 0x11, 0x0, 0x0, 0xC5, 0x11, 0x0, 0x0, 0xD1, 0x11, 0x0, 0x0, 0xD7, 0x11, 0x0, 0x0, 0xE7, 0x11, 0x0, 0x0, 0xEF, 0x11, 0x0, 0x0, 0xF5, 0x11, 0x0, 0x0, 0xFB, 0x11, 0x0, 0x0, 0xD, 0x12, 0x0, 0x0, 0x1D, 0x12, 0x0, 0x0, 0x1F, 0x12, 0x0, 0x0, 0x23, 0x12, 0x0, 0x0, 0x29, 0x12, 0x0, 0x0, 0x2B, 0x12, 0x0, 0x0, 0x31, 0x12, 0x0, 0x0, 0x37, 0x12, 0x0, 0x0, 0x41, 0x12, 0x0, 0x0, 0x47, 0x12, 0x0, 0x0, 0x53, 0x12, 0x0, 0x0, 0x5F, 0x12, 0x0, 0x0, 0x71, 0x12, 0x0, 0x0, 0x73, 0x12, 0x0, 0x0, 0x79, 0x12, 0x0, 0x0, 0x7D, 0x12, 0x0, 0x0, 0x8F, 0x12, 0x0, 0x0, 0x97, 0x12, 0x0, 0x0, 0xAF, 0x12, 0x0, 0x0, 0xB3, 0x12, 0x0, 0x0, 0xB5, 0x12, 0x0, 0x0, 0xB9, 0x12, 0x0, 0x0, 0xBF, 0x12, 0x0, 0x0, 0xC1, 0x12, 0x0, 0x0, 0xCD, 0x12, 0x0, 0x0, 0xD1, 0x12, 0x0, 0x0, 0xDF, 0x12, 0x0, 0x0, 0xFD, 0x12, 0x0, 0x0, 0x7, 0x13, 0x0, 0x0, 0xD, 0x13, 0x0, 0x0, 0x19, 0x13, 0x0, 0x0, 0x27, 0x13, 0x0, 0x0, 0x2D, 0x13, 0x0, 0x0, 0x37, 0x13, 0x0, 0x0, 0x43, 0x13, 0x0, 0x0, 0x45, 0x13, 0x0, 0x0, 0x49, 0x13, 0x0, 0x0, 0x4F, 0x13, 0x0, 0x0, 0x57, 0x13, 0x0, 0x0, 0x5D, 0x13, 0x0, 0x0, 0x67, 0x13, 0x0, 0x0, 0x69, 0x13, 0x0, 0x0, 0x6D, 0x13, 0x0, 0x0, 0x7B, 0x13, 0x0, 0x0, 0x81, 0x13, 0x0, 0x0, 0x87, 0x13, 0x0, 0x0, 0x8B, 0x13, 0x0, 0x0, 0x91, 0x13, 0x0, 0x0, 0x93, 0x13, 0x0, 0x0, 0x9D, 0x13, 0x0, 0x0, 0x9F, 0x13, 0x0, 0x0, 0xAF, 0x13, 0x0, 0x0, 0xBB, 0x13, 0x0, 0x0, 0xC3, 0x13, 0x0, 0x0, 0xD5, 0x13, 0x0, 0x0, 0xD9, 0x13, 0x0, 0x0, 0xDF, 0x13, 0x0, 0x0, 0xEB, 0x13, 0x0, 0x0, 0xED, 0x13, 0x0, 0x0, 0xF3, 0x13, 0x0, 0x0, 0xF9, 0x13, 0x0, 0x0, 0xFF, 0x13, 0x0, 0x0, 0x1B, 0x14, 0x0, 0x0, 0x21, 0x14, 0x0, 0x0, 0x2F, 0x14, 0x0, 0x0, 0x33, 0x14, 0x0, 0x0, 0x3B, 0x14, 0x0, 0x0, 0x45, 0x14, 0x0, 0x0, 0x4D, 0x14, 0x0, 0x0, 0x59, 0x14, 0x0, 0x0, 0x6B, 0x14, 0x0, 0x0, 0x6F, 0x14, 0x0, 0x0, 0x71, 0x14, 0x0, 0x0, 0x75, 0x14, 0x0, 0x0, 0x8D, 0x14, 0x0, 0x0, 0x99, 0x14, 0x0, 0x0, 0x9F, 0x14, 0x0, 0x0, 0xA1, 0x14, 0x0, 0x0, 0xB1, 0x14, 0x0, 0x0, 0xB7, 0x14, 0x0, 0x0, 0xBD, 0x14, 0x0, 0x0, 0xCB, 0x14, 0x0, 0x0, 0xD5, 0x14, 0x0, 0x0, 0xE3, 0x14, 0x0, 0x0, 0xE7, 0x14, 0x0, 0x0, 0x5, 0x15, 0x0, 0x0, 0xB, 0x15, 0x0, 0x0, 0x11, 0x15, 0x0, 0x0, 0x17, 0x15, 0x0, 0x0, 0x1F, 0x15, 0x0, 0x0, 0x25, 0x15, 0x0, 0x0, 0x29, 0x15, 0x0, 0x0, 0x2B, 0x15, 0x0, 0x0, 0x37, 0x15, 0x0, 0x0, 0x3D, 0x15, 0x0, 0x0, 0x41, 0x15, 0x0, 0x0, 0x43, 0x15, 0x0, 0x0, 0x49, 0x15, 0x0, 0x0, 0x5F, 0x15, 0x0, 0x0, 0x65, 0x15, 0x0, 0x0, 0x67, 0x15, 0x0, 0x0, 0x6B, 0x15, 0x0, 0x0, 0x7D, 0x15, 0x0, 0x0, 0x7F, 0x15, 0x0, 0x0, 0x83, 0x15, 0x0, 0x0, 0x8F, 0x15, 0x0, 0x0, 0x91, 0x15, 0x0, 0x0, 0x97, 0x15, 0x0, 0x0, 0x9B, 0x15, 0x0, 0x0, 0xB5, 0x15, 0x0, 0x0, 0xBB, 0x15, 0x0, 0x0, 0xC1, 0x15, 0x0, 0x0, 0xC5, 0x15, 0x0, 0x0, 0xCD, 0x15, 0x0, 0x0, 0xD7, 0x15, 0x0, 0x0, 0xF7, 0x15, 0x0, 0x0, 0x7, 0x16, 0x0, 0x0, 0x9, 0x16, 0x0, 0x0, 0xF, 0x16, 0x0, 0x0, 0x13, 0x16, 0x0, 0x0, 0x15, 0x16, 0x0, 0x0, 0x19, 0x16, 0x0, 0x0, 0x1B, 0x16, 0x0, 0x0, 0x25, 0x16, 0x0, 0x0, 0x33, 0x16, 0x0, 0x0, 0x39, 0x16, 0x0, 0x0, 0x3D, 0x16, 0x0, 0x0, 0x45, 0x16, 0x0, 0x0, 0x4F, 0x16, 0x0, 0x0, 0x55, 0x16, 0x0, 0x0, 0x69, 0x16, 0x0, 0x0, 0x6D, 0x16, 0x0, 0x0, 0x6F, 0x16, 0x0, 0x0, 0x75, 0x16, 0x0, 0x0, 0x93, 0x16, 0x0, 0x0, 0x97, 0x16, 0x0, 0x0, 0x9F, 0x16, 0x0, 0x0, 0xA9, 0x16, 0x0, 0x0, 0xAF, 0x16, 0x0, 0x0, 0xB5, 0x16, 0x0, 0x0, 0xBD, 0x16, 0x0, 0x0, 0xC3, 0x16, 0x0, 0x0, 0xCF, 0x16, 0x0, 0x0, 0xD3, 0x16, 0x0, 0x0, 0xD9, 0x16, 0x0, 0x0, 0xDB, 0x16, 0x0, 0x0, 0xE1, 0x16, 0x0, 0x0, 0xE5, 0x16, 0x0, 0x0, 0xEB, 0x16, 0x0, 0x0, 0xED, 0x16, 0x0, 0x0, 0xF7, 0x16, 0x0, 0x0, 0xF9, 0x16, 0x0, 0x0, 0x9, 0x17, 0x0, 0x0, 0xF, 0x17, 0x0, 0x0, 0x23, 0x17, 0x0, 0x0, 0x27, 0x17, 0x0, 0x0, 0x33, 0x17, 0x0, 0x0, 0x41, 0x17, 0x0, 0x0, 0x5D, 0x17, 0x0, 0x0, 0x63, 0x17, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_DefaultValueData, &U24ArrayTypeU243132_t2297_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_DefaultValueData[] = { 0x29, 0x2E, 0x43, 0xC9, 0xA2, 0xD8, 0x7C, 0x1, 0x3D, 0x36, 0x54, 0xA1, 0xEC, 0xF0, 0x6, 0x13, 0x62, 0xA7, 0x5, 0xF3, 0xC0, 0xC7, 0x73, 0x8C, 0x98, 0x93, 0x2B, 0xD9, 0xBC, 0x4C, 0x82, 0xCA, 0x1E, 0x9B, 0x57, 0x3C, 0xFD, 0xD4, 0xE0, 0x16, 0x67, 0x42, 0x6F, 0x18, 0x8A, 0x17, 0xE5, 0x12, 0xBE, 0x4E, 0xC4, 0xD6, 0xDA, 0x9E, 0xDE, 0x49, 0xA0, 0xFB, 0xF5, 0x8E, 0xBB, 0x2F, 0xEE, 0x7A, 0xA9, 0x68, 0x79, 0x91, 0x15, 0xB2, 0x7, 0x3F, 0x94, 0xC2, 0x10, 0x89, 0xB, 0x22, 0x5F, 0x21, 0x80, 0x7F, 0x5D, 0x9A, 0x5A, 0x90, 0x32, 0x27, 0x35, 0x3E, 0xCC, 0xE7, 0xBF, 0xF7, 0x97, 0x3, 0xFF, 0x19, 0x30, 0xB3, 0x48, 0xA5, 0xB5, 0xD1, 0xD7, 0x5E, 0x92, 0x2A, 0xAC, 0x56, 0xAA, 0xC6, 0x4F, 0xB8, 0x38, 0xD2, 0x96, 0xA4, 0x7D, 0xB6, 0x76, 0xFC, 0x6B, 0xE2, 0x9C, 0x74, 0x4, 0xF1, 0x45, 0x9D, 0x70, 0x59, 0x64, 0x71, 0x87, 0x20, 0x86, 0x5B, 0xCF, 0x65, 0xE6, 0x2D, 0xA8, 0x2, 0x1B, 0x60, 0x25, 0xAD, 0xAE, 0xB0, 0xB9, 0xF6, 0x1C, 0x46, 0x61, 0x69, 0x34, 0x40, 0x7E, 0xF, 0x55, 0x47, 0xA3, 0x23, 0xDD, 0x51, 0xAF, 0x3A, 0xC3, 0x5C, 0xF9, 0xCE, 0xBA, 0xC5, 0xEA, 0x26, 0x2C, 0x53, 0xD, 0x6E, 0x85, 0x28, 0x84, 0x9, 0xD3, 0xDF, 0xCD, 0xF4, 0x41, 0x81, 0x4D, 0x52, 0x6A, 0xDC, 0x37, 0xC8, 0x6C, 0xC1, 0xAB, 0xFA, 0x24, 0xE1, 0x7B, 0x8, 0xC, 0xBD, 0xB1, 0x4A, 0x78, 0x88, 0x95, 0x8B, 0xE3, 0x63, 0xE8, 0x6D, 0xE9, 0xCB, 0xD5, 0xFE, 0x3B, 0x0, 0x1D, 0x39, 0xF2, 0xEF, 0xB7, 0xE, 0x66, 0x58, 0xD0, 0xE4, 0xA6, 0x77, 0x72, 0xF8, 0xEB, 0x75, 0x4B, 0xA, 0x31, 0x44, 0x50, 0xB4, 0x8F, 0xED, 0x1F, 0x1A, 0xDB, 0x99, 0x8D, 0x33, 0x9F, 0x11, 0x83, 0x14 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_DefaultValueData, &U24ArrayTypeU24256_t2298_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_DefaultValueData[] = { 0xDA, 0x39, 0xA3, 0xEE, 0x5E, 0x6B, 0x4B, 0xD, 0x32, 0x55, 0xBF, 0xEF, 0x95, 0x60, 0x18, 0x90, 0xAF, 0xD8, 0x7, 0x9 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_DefaultValueData, &U24ArrayTypeU2420_t2299_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_DefaultValueData[] = { 0xE3, 0xB0, 0xC4, 0x42, 0x98, 0xFC, 0x1C, 0x14, 0x9A, 0xFB, 0xF4, 0xC8, 0x99, 0x6F, 0xB9, 0x24, 0x27, 0xAE, 0x41, 0xE4, 0x64, 0x9B, 0x93, 0x4C, 0xA4, 0x95, 0x99, 0x1B, 0x78, 0x52, 0xB8, 0x55 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_DefaultValueData, &U24ArrayTypeU2432_t2300_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_DefaultValueData[] = { 0x38, 0xB0, 0x60, 0xA7, 0x51, 0xAC, 0x96, 0x38, 0x4C, 0xD9, 0x32, 0x7E, 0xB1, 0xB1, 0xE3, 0x6A, 0x21, 0xFD, 0xB7, 0x11, 0x14, 0xBE, 0x7, 0x43, 0x4C, 0xC, 0xC7, 0xBF, 0x63, 0xF6, 0xE1, 0xDA, 0x27, 0x4E, 0xDE, 0xBF, 0xE7, 0x6F, 0x65, 0xFB, 0xD5, 0x1A, 0xD2, 0xF1, 0x48, 0x98, 0xB9, 0x5B };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_DefaultValueData, &U24ArrayTypeU2448_t2301_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_DefaultValueData[] = { 0xCF, 0x83, 0xE1, 0x35, 0x7E, 0xEF, 0xB8, 0xBD, 0xF1, 0x54, 0x28, 0x50, 0xD6, 0x6D, 0x80, 0x7, 0xD6, 0x20, 0xE4, 0x5, 0xB, 0x57, 0x15, 0xDC, 0x83, 0xF4, 0xA9, 0x21, 0xD3, 0x6C, 0xE9, 0xCE, 0x47, 0xD0, 0xD1, 0x3C, 0x5D, 0x85, 0xF2, 0xB0, 0xFF, 0x83, 0x18, 0xD2, 0x87, 0x7E, 0xEC, 0x2F, 0x63, 0xB9, 0x31, 0xBD, 0x47, 0x41, 0x7A, 0x81, 0xA5, 0x38, 0x32, 0x7A, 0xF9, 0x27, 0xDA, 0x3E };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_DefaultValueData, &U24ArrayTypeU2464_t2302_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_DefaultValueData[] = { 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1, 0x1 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_DefaultValueData, &U24ArrayTypeU2464_t2302_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_DefaultValueData[] = { 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_DefaultValueData, &U24ArrayTypeU2464_t2302_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_DefaultValueData[] = { 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_DefaultValueData, &U24ArrayTypeU2464_t2302_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x19, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_DefaultValueData, &U24ArrayTypeU2412_t2303_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_DefaultValueData[] = { 0x9, 0x92, 0x26, 0x89, 0x93, 0xF2, 0x2C, 0x64, 0x1, 0x1, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_DefaultValueData, &U24ArrayTypeU2412_t2303_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_DefaultValueData[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0xD, 0x1, 0x9, 0x1, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_DefaultValueData, &U24ArrayTypeU2412_t2303_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_DefaultValueData[] = { 0x2C, 0x0, 0x2B, 0x0, 0x22, 0x0, 0x5C, 0x0, 0x3C, 0x0, 0x3E, 0x0, 0x3B, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_DefaultValueData, &U24ArrayTypeU2416_t2304_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_DefaultValueData[] = { 0x43, 0x4C, 0x4E, 0x54 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_DefaultValueData, &U24ArrayTypeU244_t2305_0_0_0 }/* value */

};
static const uint8_t U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_DefaultValueData[] = { 0x53, 0x52, 0x56, 0x52 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_DefaultValueData, &U24ArrayTypeU244_t2305_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3E_t2306_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D0_0_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D5_1_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D6_2_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D7_3_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D8_4_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D9_5_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D11_6_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D12_7_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D13_8_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D14_9_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D15_10_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D16_11_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D17_12_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D21_13_DefaultValue,
	&U3CPrivateImplementationDetailsU3E_t2306____U24U24fieldU2D22_14_DefaultValue,
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t2297_0_0_0,
	&U24ArrayTypeU24256_t2298_0_0_0,
	&U24ArrayTypeU2420_t2299_0_0_0,
	&U24ArrayTypeU2432_t2300_0_0_0,
	&U24ArrayTypeU2448_t2301_0_0_0,
	&U24ArrayTypeU2464_t2302_0_0_0,
	&U24ArrayTypeU2412_t2303_0_0_0,
	&U24ArrayTypeU2416_t2304_0_0_0,
	&U24ArrayTypeU244_t2305_0_0_0,
};
static Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t2306_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t2306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t2306_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2306;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2306_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t2306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t2306_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3E_t2306_FieldInfos/* fields */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 38/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2306_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2306_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3E_t2306_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2306)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2306_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
