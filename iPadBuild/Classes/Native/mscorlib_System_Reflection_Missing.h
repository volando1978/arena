﻿#pragma once
#include <stdint.h>
// System.Reflection.Missing
struct Missing_t2539;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Missing
struct  Missing_t2539  : public Object_t
{
};
struct Missing_t2539_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t2539 * ___Value_0;
};
