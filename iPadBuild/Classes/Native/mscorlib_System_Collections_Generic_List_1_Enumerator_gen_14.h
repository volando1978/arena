﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct List_1_t3413;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct  Enumerator_t3434 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::l
	List_1_t3413 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::current
	UnityKeyValuePair_2_t3412 * ___current_3;
};
