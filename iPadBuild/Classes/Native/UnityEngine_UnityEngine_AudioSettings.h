﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t1579;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AudioSettings
struct  AudioSettings_t1580  : public Object_t
{
};
struct AudioSettings_t1580_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t1579 * ___OnAudioConfigurationChanged_0;
};
