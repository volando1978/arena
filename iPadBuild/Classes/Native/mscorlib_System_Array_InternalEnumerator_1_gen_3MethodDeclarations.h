﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int32>
struct InternalEnumerator_1_t3425;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15521_gshared (InternalEnumerator_1_t3425 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m15521(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3425 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15521_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522_gshared (InternalEnumerator_1_t3425 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3425 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15523_gshared (InternalEnumerator_1_t3425 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15523(__this, method) (( void (*) (InternalEnumerator_1_t3425 *, MethodInfo*))InternalEnumerator_1_Dispose_m15523_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15524_gshared (InternalEnumerator_1_t3425 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15524(__this, method) (( bool (*) (InternalEnumerator_1_t3425 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15524_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m15525_gshared (InternalEnumerator_1_t3425 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15525(__this, method) (( int32_t (*) (InternalEnumerator_1_t3425 *, MethodInfo*))InternalEnumerator_1_get_Current_m15525_gshared)(__this, method)
