﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.Object,System.IntPtr>
struct Func_2_t963;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_2__ctor_m20701_gshared (Func_2_t963 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Func_2__ctor_m20701(__this, ___object, ___method, method) (( void (*) (Func_2_t963 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20701_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.Object,System.IntPtr>::Invoke(T)
extern "C" IntPtr_t Func_2_Invoke_m20703_gshared (Func_2_t963 * __this, Object_t * ___arg1, MethodInfo* method);
#define Func_2_Invoke_m20703(__this, ___arg1, method) (( IntPtr_t (*) (Func_2_t963 *, Object_t *, MethodInfo*))Func_2_Invoke_m20703_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.Object,System.IntPtr>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Func_2_BeginInvoke_m20705_gshared (Func_2_t963 * __this, Object_t * ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Func_2_BeginInvoke_m20705(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t963 *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20705_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C" IntPtr_t Func_2_EndInvoke_m20707_gshared (Func_2_t963 * __this, Object_t * ___result, MethodInfo* method);
#define Func_2_EndInvoke_m20707(__this, ___result, method) (( IntPtr_t (*) (Func_2_t963 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20707_gshared)(__this, ___result, method)
