﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2<System.Object,System.Object>
struct UnityDictionary_2_t3406;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct List_1_t3413;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t4257;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t4258;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3438;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4259;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::.ctor()
extern "C" void UnityDictionary_2__ctor_m15264_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2__ctor_m15264(__this, method) (( void (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2__ctor_m15264_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UnityDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265(__this, method) (( Object_t * (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared)(__this, method)
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_KeyValuePairs()
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::set_KeyValuePairs(System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>>)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::SetKeyValuePair(K,V)
// V UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_Item(K)
extern "C" Object_t * UnityDictionary_2_get_Item_m15266_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, MethodInfo* method);
#define UnityDictionary_2_get_Item_m15266(__this, ___key, method) (( Object_t * (*) (UnityDictionary_2_t3406 *, Object_t *, MethodInfo*))UnityDictionary_2_get_Item_m15266_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::set_Item(K,V)
extern "C" void UnityDictionary_2_set_Item_m15267_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define UnityDictionary_2_set_Item_m15267(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3406 *, Object_t *, Object_t *, MethodInfo*))UnityDictionary_2_set_Item_m15267_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::Add(K,V)
extern "C" void UnityDictionary_2_Add_m15268_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define UnityDictionary_2_Add_m15268(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3406 *, Object_t *, Object_t *, MethodInfo*))UnityDictionary_2_Add_m15268_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<K,V>)
extern "C" void UnityDictionary_2_Add_m15269_gshared (UnityDictionary_2_t3406 * __this, KeyValuePair_2_t3407  ___kvp, MethodInfo* method);
#define UnityDictionary_2_Add_m15269(__this, ___kvp, method) (( void (*) (UnityDictionary_2_t3406 *, KeyValuePair_2_t3407 , MethodInfo*))UnityDictionary_2_Add_m15269_gshared)(__this, ___kvp, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::TryGetValue(K,V&)
extern "C" bool UnityDictionary_2_TryGetValue_m15270_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, Object_t ** ___value, MethodInfo* method);
#define UnityDictionary_2_TryGetValue_m15270(__this, ___key, ___value, method) (( bool (*) (UnityDictionary_2_t3406 *, Object_t *, Object_t **, MethodInfo*))UnityDictionary_2_TryGetValue_m15270_gshared)(__this, ___key, ___value, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<K,V>)
extern "C" bool UnityDictionary_2_Remove_m15271_gshared (UnityDictionary_2_t3406 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define UnityDictionary_2_Remove_m15271(__this, ___item, method) (( bool (*) (UnityDictionary_2_t3406 *, KeyValuePair_2_t3407 , MethodInfo*))UnityDictionary_2_Remove_m15271_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::Remove(K)
extern "C" bool UnityDictionary_2_Remove_m15272_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, MethodInfo* method);
#define UnityDictionary_2_Remove_m15272(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3406 *, Object_t *, MethodInfo*))UnityDictionary_2_Remove_m15272_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::Clear()
extern "C" void UnityDictionary_2_Clear_m15273_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_Clear_m15273(__this, method) (( void (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_Clear_m15273_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::ContainsKey(K)
extern "C" bool UnityDictionary_2_ContainsKey_m15274_gshared (UnityDictionary_2_t3406 * __this, Object_t * ___key, MethodInfo* method);
#define UnityDictionary_2_ContainsKey_m15274(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3406 *, Object_t *, MethodInfo*))UnityDictionary_2_ContainsKey_m15274_gshared)(__this, ___key, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<K,V>)
extern "C" bool UnityDictionary_2_Contains_m15275_gshared (UnityDictionary_2_t3406 * __this, KeyValuePair_2_t3407  ___kvp, MethodInfo* method);
#define UnityDictionary_2_Contains_m15275(__this, ___kvp, method) (( bool (*) (UnityDictionary_2_t3406 *, KeyValuePair_2_t3407 , MethodInfo*))UnityDictionary_2_Contains_m15275_gshared)(__this, ___kvp, method)
// System.Int32 UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t UnityDictionary_2_get_Count_m15276_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_Count_m15276(__this, method) (( int32_t (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_Count_m15276_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<K,V>[],System.Int32)
extern "C" void UnityDictionary_2_CopyTo_m15277_gshared (UnityDictionary_2_t3406 * __this, KeyValuePair_2U5BU5D_t3438* ___array, int32_t ___index, MethodInfo* method);
#define UnityDictionary_2_CopyTo_m15277(__this, ___array, ___index, method) (( void (*) (UnityDictionary_2_t3406 *, KeyValuePair_2U5BU5D_t3438*, int32_t, MethodInfo*))UnityDictionary_2_CopyTo_m15277_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* UnityDictionary_2_GetEnumerator_m15278_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_GetEnumerator_m15278(__this, method) (( Object_t* (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_GetEnumerator_m15278_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<K> UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* UnityDictionary_2_get_Keys_m15279_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_Keys_m15279(__this, method) (( Object_t* (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_Keys_m15279_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<V> UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* UnityDictionary_2_get_Values_m15280_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_Values_m15280(__this, method) (( Object_t* (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_Values_m15280_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_Items()
extern "C" Object_t* UnityDictionary_2_get_Items_m15282_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_Items_m15282(__this, method) (( Object_t* (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_Items_m15282_gshared)(__this, method)
// V UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_SyncRoot()
extern "C" Object_t * UnityDictionary_2_get_SyncRoot_m15284_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_SyncRoot_m15284(__this, method) (( Object_t * (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_SyncRoot_m15284_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_IsFixedSize()
extern "C" bool UnityDictionary_2_get_IsFixedSize_m15286_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_IsFixedSize_m15286(__this, method) (( bool (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_IsFixedSize_m15286_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool UnityDictionary_2_get_IsReadOnly_m15287_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_IsReadOnly_m15287(__this, method) (( bool (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_IsReadOnly_m15287_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.Object,System.Object>::get_IsSynchronized()
extern "C" bool UnityDictionary_2_get_IsSynchronized_m15289_gshared (UnityDictionary_2_t3406 * __this, MethodInfo* method);
#define UnityDictionary_2_get_IsSynchronized_m15289(__this, method) (( bool (*) (UnityDictionary_2_t3406 *, MethodInfo*))UnityDictionary_2_get_IsSynchronized_m15289_gshared)(__this, method)
// K UnityEngine.UnityDictionary`2<System.Object,System.Object>::<get_Keys>m__0(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" Object_t * UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared (Object_t * __this /* static, unused */, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define UnityDictionary_2_U3Cget_KeysU3Em__0_m15291(__this /* static, unused */, ___x, method) (( Object_t * (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared)(__this /* static, unused */, ___x, method)
// V UnityEngine.UnityDictionary`2<System.Object,System.Object>::<get_Values>m__1(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" Object_t * UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared (Object_t * __this /* static, unused */, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293(__this /* static, unused */, ___x, method) (( Object_t * (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.Object,System.Object>::<get_Items>m__2(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" KeyValuePair_2_t3407  UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared (Object_t * __this /* static, unused */, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t3407  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.Object,System.Object>::<CopyTo>m__6(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" KeyValuePair_2_t3407  UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared (Object_t * __this /* static, unused */, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define UnityDictionary_2_U3CCopyToU3Em__6_m15297(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t3407  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3412 *, MethodInfo*))UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared)(__this /* static, unused */, ___x, method)
