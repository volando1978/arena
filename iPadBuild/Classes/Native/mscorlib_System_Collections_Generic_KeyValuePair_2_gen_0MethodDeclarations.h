﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>
struct KeyValuePair_2_t198;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16578(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t198 *, String_t*, JSONObject_t30 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::get_Key()
#define KeyValuePair_2_get_Key_m891(__this, method) (( String_t* (*) (KeyValuePair_2_t198 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16579(__this, ___value, method) (( void (*) (KeyValuePair_2_t198 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::get_Value()
#define KeyValuePair_2_get_Value_m892(__this, method) (( JSONObject_t30 * (*) (KeyValuePair_2_t198 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16580(__this, ___value, method) (( void (*) (KeyValuePair_2_t198 *, JSONObject_t30 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,JSONObject>::ToString()
#define KeyValuePair_2_ToString_m16581(__this, method) (( String_t* (*) (KeyValuePair_2_t198 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
