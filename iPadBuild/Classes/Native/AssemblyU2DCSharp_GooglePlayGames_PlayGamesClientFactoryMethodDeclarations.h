﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesClientFactory
struct PlayGamesClientFactory_t715;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t387;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"

// System.Void GooglePlayGames.PlayGamesClientFactory::.ctor()
extern "C" void PlayGamesClientFactory__ctor_m3132 (PlayGamesClientFactory_t715 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.IPlayGamesClient GooglePlayGames.PlayGamesClientFactory::GetPlatformPlayGamesClient(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C" Object_t * PlayGamesClientFactory_GetPlatformPlayGamesClient_m3133 (Object_t * __this /* static, unused */, PlayGamesClientConfiguration_t366  ___config, MethodInfo* method) IL2CPP_METHOD_ATTR;
