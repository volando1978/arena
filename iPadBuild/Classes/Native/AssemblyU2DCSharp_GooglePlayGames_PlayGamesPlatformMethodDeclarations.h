﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesPlatform
struct PlayGamesPlatform_t382;
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient
struct INearbyConnectionClient_t388;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient
struct IRealTimeMultiplayerClient_t843;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient
struct ITurnBasedMultiplayerClient_t844;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
struct ISavedGameClient_t537;
// GooglePlayGames.BasicApi.Events.IEventsClient
struct IEventsClient_t538;
// GooglePlayGames.BasicApi.Quests.IQuestsClient
struct IQuestsClient_t539;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t851;
// GooglePlayGames.BasicApi.IPlayGamesClient
struct IPlayGamesClient_t387;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>
struct Action_1_t852;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.String[]
struct StringU5BU5D_t169;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t853;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t854;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t855;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t856;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t857;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t858;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t530;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t842;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"

// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C" void PlayGamesPlatform__ctor_m1521 (PlayGamesPlatform_t382 * __this, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::.ctor(GooglePlayGames.BasicApi.IPlayGamesClient)
extern "C" void PlayGamesPlatform__ctor_m1522 (PlayGamesPlatform_t382 * __this, Object_t * ___client, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::.cctor()
extern "C" void PlayGamesPlatform__cctor_m1523 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeInstance(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C" void PlayGamesPlatform_InitializeInstance_m1524 (Object_t * __this /* static, unused */, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::get_Instance()
extern "C" PlayGamesPlatform_t382 * PlayGamesPlatform_get_Instance_m1525 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::InitializeNearby(System.Action`1<GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient>)
extern "C" void PlayGamesPlatform_InitializeNearby_m1526 (Object_t * __this /* static, unused */, Action_1_t852 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Nearby.INearbyConnectionClient GooglePlayGames.PlayGamesPlatform::get_Nearby()
extern "C" Object_t * PlayGamesPlatform_get_Nearby_m1527 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesPlatform::get_DebugLogEnabled()
extern "C" bool PlayGamesPlatform_get_DebugLogEnabled_m1528 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::set_DebugLogEnabled(System.Boolean)
extern "C" void PlayGamesPlatform_set_DebugLogEnabled_m1529 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_RealTime()
extern "C" Object_t * PlayGamesPlatform_get_RealTime_m1530 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.PlayGamesPlatform::get_TurnBased()
extern "C" Object_t * PlayGamesPlatform_get_TurnBased_m1531 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.PlayGamesPlatform::get_SavedGame()
extern "C" Object_t * PlayGamesPlatform_get_SavedGame_m1532 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.PlayGamesPlatform::get_Events()
extern "C" Object_t * PlayGamesPlatform_get_Events_m1533 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.PlayGamesPlatform::get_Quests()
extern "C" Object_t * PlayGamesPlatform_get_Quests_m1534 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesPlatform::Activate()
extern "C" PlayGamesPlatform_t382 * PlayGamesPlatform_Activate_m1535 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::AddIdMapping(System.String,System.String)
extern "C" void PlayGamesPlatform_AddIdMapping_m1536 (PlayGamesPlatform_t382 * __this, String_t* ___fromId, String_t* ___toId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_Authenticate_m1537 (PlayGamesPlatform_t382 * __this, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void PlayGamesPlatform_Authenticate_m1538 (PlayGamesPlatform_t382 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_Authenticate_m1539 (PlayGamesPlatform_t382 * __this, Object_t * ___unused, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesPlatform::IsAuthenticated()
extern "C" bool PlayGamesPlatform_IsAuthenticated_m1540 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::SignOut()
extern "C" void PlayGamesPlatform_SignOut_m1541 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern "C" void PlayGamesPlatform_LoadUsers_m1542 (PlayGamesPlatform_t382 * __this, StringU5BU5D_t169* ___userIDs, Action_1_t853 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesPlatform::GetUserId()
extern "C" String_t* PlayGamesPlatform_GetUserId_m1543 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.PlayGamesPlatform::GetAchievement(System.String)
extern "C" Achievement_t333 * PlayGamesPlatform_GetAchievement_m1544 (PlayGamesPlatform_t382 * __this, String_t* ___achievementId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesPlatform::GetUserDisplayName()
extern "C" String_t* PlayGamesPlatform_GetUserDisplayName_m1545 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesPlatform::GetUserImageUrl()
extern "C" String_t* PlayGamesPlatform_GetUserImageUrl_m1546 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_ReportProgress_m1547 (PlayGamesPlatform_t382 * __this, String_t* ___achievementID, double ___progress, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_IncrementAchievement_m1548 (PlayGamesPlatform_t382 * __this, String_t* ___achievementID, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern "C" void PlayGamesPlatform_LoadAchievementDescriptions_m1549 (PlayGamesPlatform_t382 * __this, Action_1_t854 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern "C" void PlayGamesPlatform_LoadAchievements_m1550 (PlayGamesPlatform_t382 * __this, Action_1_t855 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.IAchievement GooglePlayGames.PlayGamesPlatform::CreateAchievement()
extern "C" Object_t * PlayGamesPlatform_CreateAchievement_m1551 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_ReportScore_m1552 (PlayGamesPlatform_t382 * __this, int64_t ___score, String_t* ___board, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern "C" void PlayGamesPlatform_LoadScores_m1553 (PlayGamesPlatform_t382 * __this, String_t* ___leaderboardID, Action_1_t857 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.ILeaderboard GooglePlayGames.PlayGamesPlatform::CreateLeaderboard()
extern "C" Object_t * PlayGamesPlatform_CreateLeaderboard_m1554 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI()
extern "C" void PlayGamesPlatform_ShowAchievementsUI_m1555 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void PlayGamesPlatform_ShowAchievementsUI_m1556 (PlayGamesPlatform_t382 * __this, Action_1_t530 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI()
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1557 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String)
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1558 (PlayGamesPlatform_t382 * __this, String_t* ___lbId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void PlayGamesPlatform_ShowLeaderboardUI_m1559 (PlayGamesPlatform_t382 * __this, String_t* ___lbId, Action_1_t530 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::SetDefaultLeaderboardForUI(System.String)
extern "C" void PlayGamesPlatform_SetDefaultLeaderboardForUI_m1560 (PlayGamesPlatform_t382 * __this, String_t* ___lbid, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_LoadFriends_m1561 (PlayGamesPlatform_t382 * __this, Object_t * ___user, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern "C" void PlayGamesPlatform_LoadScores_m1562 (PlayGamesPlatform_t382 * __this, Object_t * ___board, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern "C" bool PlayGamesPlatform_GetLoading_m1563 (PlayGamesPlatform_t382 * __this, Object_t * ___board, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void PlayGamesPlatform_LoadState_m1564 (PlayGamesPlatform_t382 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void PlayGamesPlatform_UpdateState_m1565 (PlayGamesPlatform_t382 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.ILocalUser GooglePlayGames.PlayGamesPlatform::get_localUser()
extern "C" Object_t * PlayGamesPlatform_get_localUser_m1566 (PlayGamesPlatform_t382 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesPlatform::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C" void PlayGamesPlatform_RegisterInvitationDelegate_m1567 (PlayGamesPlatform_t382 * __this, InvitationReceivedDelegate_t363 * ___deleg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesPlatform::MapId(System.String)
extern "C" String_t* PlayGamesPlatform_MapId_m1568 (PlayGamesPlatform_t382 * __this, String_t* ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
