﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
struct MatchOutcome_t345;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::.ctor()
extern "C" void MatchOutcome__ctor_m1377 (MatchOutcome_t345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult,System.UInt32)
extern "C" void MatchOutcome_SetParticipantResult_m1378 (MatchOutcome_t345 * __this, String_t* ___participantId, int32_t ___result, uint32_t ___placement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult)
extern "C" void MatchOutcome_SetParticipantResult_m1379 (MatchOutcome_t345 * __this, String_t* ___participantId, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::SetParticipantResult(System.String,System.UInt32)
extern "C" void MatchOutcome_SetParticipantResult_m1380 (MatchOutcome_t345 * __this, String_t* ___participantId, uint32_t ___placement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::get_ParticipantIds()
extern "C" List_1_t43 * MatchOutcome_get_ParticipantIds_m1381 (MatchOutcome_t345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::GetResultFor(System.String)
extern "C" int32_t MatchOutcome_GetResultFor_m1382 (MatchOutcome_t345 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::GetPlacementFor(System.String)
extern "C" uint32_t MatchOutcome_GetPlacementFor_m1383 (MatchOutcome_t345 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.MatchOutcome::ToString()
extern "C" String_t* MatchOutcome_ToString_m1384 (MatchOutcome_t345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
