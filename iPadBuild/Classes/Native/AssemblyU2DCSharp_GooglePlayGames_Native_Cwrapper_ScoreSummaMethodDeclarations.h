﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.ScoreSummary
struct ScoreSummary_t471;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.UInt64 GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_ApproximateNumberOfScores(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t ScoreSummary_ScoreSummary_ApproximateNumberOfScores_m2062 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t ScoreSummary_ScoreSummary_TimeSpan_m2063 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  ScoreSummary_ScoreSummary_LeaderboardId_m2064 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Collection(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t ScoreSummary_ScoreSummary_Collection_m2065 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScoreSummary_ScoreSummary_Valid_m2066 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_CurrentPlayerScore(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t ScoreSummary_ScoreSummary_CurrentPlayerScore_m2067 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ScoreSummary_ScoreSummary_Dispose_m2068 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
