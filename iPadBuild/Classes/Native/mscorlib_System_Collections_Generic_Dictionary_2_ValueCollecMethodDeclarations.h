﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>
struct Enumerator_t209;
// System.Object
struct Object_t;
// Soomla.Reward
struct Reward_t55;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Reward>
struct Dictionary_2_t58;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"
#define Enumerator__ctor_m17001(__this, ___host, method) (( void (*) (Enumerator_t209 *, Dictionary_2_t58 *, MethodInfo*))Enumerator__ctor_m16310_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17002(__this, method) (( Object_t * (*) (Enumerator_t209 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>::Dispose()
#define Enumerator_Dispose_m17003(__this, method) (( void (*) (Enumerator_t209 *, MethodInfo*))Enumerator_Dispose_m16312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>::MoveNext()
#define Enumerator_MoveNext_m950(__this, method) (( bool (*) (Enumerator_t209 *, MethodInfo*))Enumerator_MoveNext_m16313_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Reward>::get_Current()
#define Enumerator_get_Current_m949(__this, method) (( Reward_t55 * (*) (Enumerator_t209 *, MethodInfo*))Enumerator_get_Current_m16314_gshared)(__this, method)
