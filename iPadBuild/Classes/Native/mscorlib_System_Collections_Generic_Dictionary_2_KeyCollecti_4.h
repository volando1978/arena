﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t167;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,JSONObject>
struct  KeyCollection_t3504  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,JSONObject>::dictionary
	Dictionary_2_t167 * ___dictionary_0;
};
