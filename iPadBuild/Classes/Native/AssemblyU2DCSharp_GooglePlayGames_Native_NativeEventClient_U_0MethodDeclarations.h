﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25
struct U3CFetchEventU3Ec__AnonStorey25_t547;
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
struct FetchResponse_t662;

// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25::.ctor()
extern "C" void U3CFetchEventU3Ec__AnonStorey25__ctor_m2293 (U3CFetchEventU3Ec__AnonStorey25_t547 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25::<>m__1D(GooglePlayGames.Native.PInvoke.EventManager/FetchResponse)
extern "C" void U3CFetchEventU3Ec__AnonStorey25_U3CU3Em__1D_m2294 (U3CFetchEventU3Ec__AnonStorey25_t547 * __this, FetchResponse_t662 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
