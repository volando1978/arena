﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2914;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14426_gshared (GenericEqualityComparer_1_t2914 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14426(__this, method) (( void (*) (GenericEqualityComparer_1_t2914 *, MethodInfo*))GenericEqualityComparer_1__ctor_m14426_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m26126_gshared (GenericEqualityComparer_1_t2914 * __this, DateTimeOffset_t2793  ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m26126(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2914 *, DateTimeOffset_t2793 , MethodInfo*))GenericEqualityComparer_1_GetHashCode_m26126_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m26127_gshared (GenericEqualityComparer_1_t2914 * __this, DateTimeOffset_t2793  ___x, DateTimeOffset_t2793  ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m26127(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2914 *, DateTimeOffset_t2793 , DateTimeOffset_t2793 , MethodInfo*))GenericEqualityComparer_1_Equals_m26127_gshared)(__this, ___x, ___y, method)
