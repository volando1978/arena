﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU2448_t2301 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2448_t2301__padding[48];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$48
#pragma pack(push, tp, 1)
struct U24ArrayTypeU2448_t2301_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2448_t2301__padding[48];
	};
};
#pragma pack(pop, tp)
