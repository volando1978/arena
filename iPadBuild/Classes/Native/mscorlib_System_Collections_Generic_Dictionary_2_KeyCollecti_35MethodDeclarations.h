﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
struct Enumerator_t4174;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t4170;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25642_gshared (Enumerator_t4174 * __this, Dictionary_2_t4170 * ___host, MethodInfo* method);
#define Enumerator__ctor_m25642(__this, ___host, method) (( void (*) (Enumerator_t4174 *, Dictionary_2_t4170 *, MethodInfo*))Enumerator__ctor_m25642_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25643_gshared (Enumerator_t4174 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25643(__this, method) (( Object_t * (*) (Enumerator_t4174 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m25644_gshared (Enumerator_t4174 * __this, MethodInfo* method);
#define Enumerator_Dispose_m25644(__this, method) (( void (*) (Enumerator_t4174 *, MethodInfo*))Enumerator_Dispose_m25644_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25645_gshared (Enumerator_t4174 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m25645(__this, method) (( bool (*) (Enumerator_t4174 *, MethodInfo*))Enumerator_MoveNext_m25645_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m25646_gshared (Enumerator_t4174 * __this, MethodInfo* method);
#define Enumerator_get_Current_m25646(__this, method) (( Object_t * (*) (Enumerator_t4174 *, MethodInfo*))Enumerator_get_Current_m25646_gshared)(__this, method)
