﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
struct FetchAllResponse_t655;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// GooglePlayGames.Native.NativeAchievement
struct NativeAchievement_t673;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement>
struct IEnumerator_1_t865;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C" void FetchAllResponse__ctor_m2606 (FetchAllResponse_t655 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * FetchAllResponse_System_Collections_IEnumerable_GetEnumerator_m2607 (FetchAllResponse_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Status()
extern "C" int32_t FetchAllResponse_Status_m2608 (FetchAllResponse_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::Length()
extern "C" UIntPtr_t  FetchAllResponse_Length_m2609 (FetchAllResponse_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetElement(System.UIntPtr)
extern "C" NativeAchievement_t673 * FetchAllResponse_GetElement_m2610 (FetchAllResponse_t655 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement> GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetEnumerator()
extern "C" Object_t* FetchAllResponse_GetEnumerator_m2611 (FetchAllResponse_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchAllResponse_CallDispose_m2612 (FetchAllResponse_t655 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C" FetchAllResponse_t655 * FetchAllResponse_FromPointer_m2613 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeAchievement GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::<GetEnumerator>m__71(System.UIntPtr)
extern "C" NativeAchievement_t673 * FetchAllResponse_U3CGetEnumeratorU3Em__71_m2614 (FetchAllResponse_t655 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
