﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// paqueteScr
struct paqueteScr_t828;

// System.Void paqueteScr::.ctor()
extern "C" void paqueteScr__ctor_m3621 (paqueteScr_t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void paqueteScr::Start()
extern "C" void paqueteScr_Start_m3622 (paqueteScr_t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void paqueteScr::Update()
extern "C" void paqueteScr_Update_m3623 (paqueteScr_t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void paqueteScr::giveBullets()
extern "C" void paqueteScr_giveBullets_m3624 (paqueteScr_t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void paqueteScr::triggerAnim()
extern "C" void paqueteScr_triggerAnim_m3625 (paqueteScr_t828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
