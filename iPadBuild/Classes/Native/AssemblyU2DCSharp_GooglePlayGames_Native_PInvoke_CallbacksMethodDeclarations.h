﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks
struct Callbacks_t661;
// System.Delegate
struct Delegate_t211;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// GooglePlayGames.Native.PInvoke.Callbacks/Type
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks_T.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.Callbacks::.cctor()
extern "C" void Callbacks__cctor_m2635 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.Callbacks::ToIntPtr(System.Delegate)
extern "C" IntPtr_t Callbacks_ToIntPtr_m2636 (Object_t * __this /* static, unused */, Delegate_t211 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::InternalShowUICallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void Callbacks_InternalShowUICallback_m2637 (Object_t * __this /* static, unused */, int32_t ___status, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::PerformInternalCallback(System.String,GooglePlayGames.Native.PInvoke.Callbacks/Type,System.IntPtr,System.IntPtr)
extern "C" void Callbacks_PerformInternalCallback_m2638 (Object_t * __this /* static, unused */, String_t* ___callbackName, int32_t ___callbackType, IntPtr_t ___response, IntPtr_t ___userData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.PInvoke.Callbacks::IntPtrAndSizeToByteArray(System.IntPtr,System.UIntPtr)
extern "C" ByteU5BU5D_t350* Callbacks_IntPtrAndSizeToByteArray_m2639 (Object_t * __this /* static, unused */, IntPtr_t ___data, UIntPtr_t  ___dataLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.Callbacks::<NoopUICallback>m__72(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" void Callbacks_U3CNoopUICallbackU3Em__72_m2640 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
