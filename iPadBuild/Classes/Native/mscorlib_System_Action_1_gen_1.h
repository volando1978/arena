﻿#pragma once
#include <stdint.h>
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Soomla.Store.EquippableVG>
struct  Action_1_t93  : public MulticastDelegate_t22
{
};
