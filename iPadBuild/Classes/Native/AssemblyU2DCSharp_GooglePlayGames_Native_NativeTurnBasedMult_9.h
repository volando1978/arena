﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Boolean>
struct Action_1_t98;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57
struct  U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646  : public Object_t
{
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::callback
	Action_1_t98 * ___callback_0;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_1;
};
