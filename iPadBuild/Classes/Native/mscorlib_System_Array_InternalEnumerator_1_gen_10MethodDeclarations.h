﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Byte>
struct InternalEnumerator_1_t3551;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17272_gshared (InternalEnumerator_1_t3551 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m17272(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3551 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m17272_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared (InternalEnumerator_1_t3551 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3551 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17276_gshared (InternalEnumerator_1_t3551 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17276(__this, method) (( void (*) (InternalEnumerator_1_t3551 *, MethodInfo*))InternalEnumerator_1_Dispose_m17276_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17278_gshared (InternalEnumerator_1_t3551 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17278(__this, method) (( bool (*) (InternalEnumerator_1_t3551 *, MethodInfo*))InternalEnumerator_1_MoveNext_m17278_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m17280_gshared (InternalEnumerator_1_t3551 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17280(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3551 *, MethodInfo*))InternalEnumerator_1_get_Current_m17280_gshared)(__this, method)
