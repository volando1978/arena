﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CharacterInfo
struct CharacterInfo_t1601;
struct CharacterInfo_t1601_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m7241 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m7242 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m7243 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m7244 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m7245 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m7246 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m7247 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m7248 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomLeftUnFlipped_m7249 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomRightUnFlipped_m7250 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvTopRightUnFlipped_m7251 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t739  CharacterInfo_get_uvTopLeftUnFlipped_m7252 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomLeft_m7253 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t739  CharacterInfo_get_uvBottomRight_m7254 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t739  CharacterInfo_get_uvTopRight_m7255 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t739  CharacterInfo_get_uvTopLeft_m7256 (CharacterInfo_t1601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void CharacterInfo_t1601_marshal(const CharacterInfo_t1601& unmarshaled, CharacterInfo_t1601_marshaled& marshaled);
void CharacterInfo_t1601_marshal_back(const CharacterInfo_t1601_marshaled& marshaled, CharacterInfo_t1601& unmarshaled);
void CharacterInfo_t1601_marshal_cleanup(CharacterInfo_t1601_marshaled& marshaled);
