﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// Soomla.Store.PurchaseWithMarket
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithMarke.h"
// Metadata Definition Soomla.Store.PurchaseWithMarket
extern TypeInfo PurchaseWithMarket_t138_il2cpp_TypeInfo;
// Soomla.Store.PurchaseWithMarket
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithMarkeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Double_t234_0_0_0;
static ParameterInfo PurchaseWithMarket_t138_PurchaseWithMarket__ctor_m672_ParameterInfos[] = 
{
	{"productId", 0, 134218459, 0, &String_t_0_0_0},
	{"price", 1, 134218460, 0, &Double_t234_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithMarket::.ctor(System.String,System.Double)
MethodInfo PurchaseWithMarket__ctor_m672_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PurchaseWithMarket__ctor_m672/* method */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234/* invoker_method */
	, PurchaseWithMarket_t138_PurchaseWithMarket__ctor_m672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MarketItem_t122_0_0_0;
extern Il2CppType MarketItem_t122_0_0_0;
static ParameterInfo PurchaseWithMarket_t138_PurchaseWithMarket__ctor_m673_ParameterInfos[] = 
{
	{"marketItem", 0, 134218461, 0, &MarketItem_t122_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithMarket::.ctor(Soomla.Store.MarketItem)
MethodInfo PurchaseWithMarket__ctor_m673_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PurchaseWithMarket__ctor_m673/* method */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PurchaseWithMarket_t138_PurchaseWithMarket__ctor_m673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo PurchaseWithMarket_t138_PurchaseWithMarket_Buy_m674_ParameterInfos[] = 
{
	{"payload", 0, 134218462, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithMarket::Buy(System.String)
MethodInfo PurchaseWithMarket_Buy_m674_MethodInfo = 
{
	"Buy"/* name */
	, (methodPointerType)&PurchaseWithMarket_Buy_m674/* method */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PurchaseWithMarket_t138_PurchaseWithMarket_Buy_m674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean Soomla.Store.PurchaseWithMarket::CanAfford()
MethodInfo PurchaseWithMarket_CanAfford_m675_MethodInfo = 
{
	"CanAfford"/* name */
	, (methodPointerType)&PurchaseWithMarket_CanAfford_m675/* method */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PurchaseWithMarket_t138_MethodInfos[] =
{
	&PurchaseWithMarket__ctor_m672_MethodInfo,
	&PurchaseWithMarket__ctor_m673_MethodInfo,
	&PurchaseWithMarket_Buy_m674_MethodInfo,
	&PurchaseWithMarket_CanAfford_m675_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_32849;
FieldInfo PurchaseWithMarket_t138____TAG_1_FieldInfo = 
{
	"TAG"/* name */
	, &String_t_0_0_32849/* type */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MarketItem_t122_0_0_6;
FieldInfo PurchaseWithMarket_t138____MarketItem_2_FieldInfo = 
{
	"MarketItem"/* name */
	, &MarketItem_t122_0_0_6/* type */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* parent */
	, offsetof(PurchaseWithMarket_t138, ___MarketItem_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PurchaseWithMarket_t138_FieldInfos[] =
{
	&PurchaseWithMarket_t138____TAG_1_FieldInfo,
	&PurchaseWithMarket_t138____MarketItem_2_FieldInfo,
	NULL
};
static const uint16_t PurchaseWithMarket_t138____TAG_1_DefaultValueData[] = { 0x53, 0x4F, 0x4F, 0x4D, 0x4C, 0x41, 0x20, 0x50, 0x75, 0x72, 0x63, 0x68, 0x61, 0x73, 0x65, 0x57, 0x69, 0x74, 0x68, 0x4D, 0x61, 0x72, 0x6B, 0x65, 0x74, 0x00 };
static Il2CppFieldDefaultValueEntry PurchaseWithMarket_t138____TAG_1_DefaultValue = 
{
	&PurchaseWithMarket_t138____TAG_1_FieldInfo/* field */
	, { (char*)&PurchaseWithMarket_t138____TAG_1_DefaultValueData, &String_t_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* PurchaseWithMarket_t138_FieldDefaultValues[] = 
{
	&PurchaseWithMarket_t138____TAG_1_DefaultValue,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
extern MethodInfo PurchaseWithMarket_Buy_m674_MethodInfo;
extern MethodInfo PurchaseWithMarket_CanAfford_m675_MethodInfo;
static Il2CppMethodReference PurchaseWithMarket_t138_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&PurchaseWithMarket_Buy_m674_MethodInfo,
	&PurchaseWithMarket_CanAfford_m675_MethodInfo,
};
static bool PurchaseWithMarket_t138_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType PurchaseWithMarket_t138_0_0_0;
extern Il2CppType PurchaseWithMarket_t138_1_0_0;
extern Il2CppType PurchaseType_t123_0_0_0;
struct PurchaseWithMarket_t138;
const Il2CppTypeDefinitionMetadata PurchaseWithMarket_t138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PurchaseType_t123_0_0_0/* parent */
	, PurchaseWithMarket_t138_VTable/* vtableMethods */
	, PurchaseWithMarket_t138_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PurchaseWithMarket_t138_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PurchaseWithMarket"/* name */
	, "Soomla.Store"/* namespaze */
	, PurchaseWithMarket_t138_MethodInfos/* methods */
	, NULL/* properties */
	, PurchaseWithMarket_t138_FieldInfos/* fields */
	, NULL/* events */
	, &PurchaseWithMarket_t138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PurchaseWithMarket_t138_0_0_0/* byval_arg */
	, &PurchaseWithMarket_t138_1_0_0/* this_arg */
	, &PurchaseWithMarket_t138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, PurchaseWithMarket_t138_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PurchaseWithMarket_t138)/* instance_size */
	, sizeof (PurchaseWithMarket_t138)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithVirtu.h"
// Metadata Definition Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8
extern TypeInfo U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo;
// Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithVirtuMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::.ctor()
MethodInfo U3CBuyU3Ec__AnonStorey8__ctor_m676_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CBuyU3Ec__AnonStorey8__ctor_m676/* method */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::<>m__23()
MethodInfo U3CBuyU3Ec__AnonStorey8_U3CU3Em__23_m677_MethodInfo = 
{
	"<>m__23"/* name */
	, (methodPointerType)&U3CBuyU3Ec__AnonStorey8_U3CU3Em__23_m677/* method */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CBuyU3Ec__AnonStorey8_t140_MethodInfos[] =
{
	&U3CBuyU3Ec__AnonStorey8__ctor_m676_MethodInfo,
	&U3CBuyU3Ec__AnonStorey8_U3CU3Em__23_m677_MethodInfo,
	NULL
};
extern Il2CppType JSONObject_t30_0_0_3;
FieldInfo U3CBuyU3Ec__AnonStorey8_t140____eventJSON_0_FieldInfo = 
{
	"eventJSON"/* name */
	, &JSONObject_t30_0_0_3/* type */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* parent */
	, offsetof(U3CBuyU3Ec__AnonStorey8_t140, ___eventJSON_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_3;
FieldInfo U3CBuyU3Ec__AnonStorey8_t140____payload_1_FieldInfo = 
{
	"payload"/* name */
	, &String_t_0_0_3/* type */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* parent */
	, offsetof(U3CBuyU3Ec__AnonStorey8_t140, ___payload_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType PurchaseWithVirtualItem_t139_0_0_3;
FieldInfo U3CBuyU3Ec__AnonStorey8_t140____U3CU3Ef__this_2_FieldInfo = 
{
	"<>f__this"/* name */
	, &PurchaseWithVirtualItem_t139_0_0_3/* type */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* parent */
	, offsetof(U3CBuyU3Ec__AnonStorey8_t140, ___U3CU3Ef__this_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CBuyU3Ec__AnonStorey8_t140_FieldInfos[] =
{
	&U3CBuyU3Ec__AnonStorey8_t140____eventJSON_0_FieldInfo,
	&U3CBuyU3Ec__AnonStorey8_t140____payload_1_FieldInfo,
	&U3CBuyU3Ec__AnonStorey8_t140____U3CU3Ef__this_2_FieldInfo,
	NULL
};
static Il2CppMethodReference U3CBuyU3Ec__AnonStorey8_t140_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool U3CBuyU3Ec__AnonStorey8_t140_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType U3CBuyU3Ec__AnonStorey8_t140_0_0_0;
extern Il2CppType U3CBuyU3Ec__AnonStorey8_t140_1_0_0;
extern Il2CppType Object_t_0_0_0;
extern TypeInfo PurchaseWithVirtualItem_t139_il2cpp_TypeInfo;
extern Il2CppType PurchaseWithVirtualItem_t139_0_0_0;
struct U3CBuyU3Ec__AnonStorey8_t140;
const Il2CppTypeDefinitionMetadata U3CBuyU3Ec__AnonStorey8_t140_DefinitionMetadata = 
{
	&PurchaseWithVirtualItem_t139_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CBuyU3Ec__AnonStorey8_t140_VTable/* vtableMethods */
	, U3CBuyU3Ec__AnonStorey8_t140_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Buy>c__AnonStorey8"/* name */
	, ""/* namespaze */
	, U3CBuyU3Ec__AnonStorey8_t140_MethodInfos/* methods */
	, NULL/* properties */
	, U3CBuyU3Ec__AnonStorey8_t140_FieldInfos/* fields */
	, NULL/* events */
	, &U3CBuyU3Ec__AnonStorey8_t140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 114/* custom_attributes_cache */
	, &U3CBuyU3Ec__AnonStorey8_t140_0_0_0/* byval_arg */
	, &U3CBuyU3Ec__AnonStorey8_t140_1_0_0/* this_arg */
	, &U3CBuyU3Ec__AnonStorey8_t140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CBuyU3Ec__AnonStorey8_t140)/* instance_size */
	, sizeof (U3CBuyU3Ec__AnonStorey8_t140)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Soomla.Store.PurchaseWithVirtualItem
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithVirtu_0.h"
// Metadata Definition Soomla.Store.PurchaseWithVirtualItem
// Soomla.Store.PurchaseWithVirtualItem
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseWithVirtu_0MethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem__ctor_m678_ParameterInfos[] = 
{
	{"targetItemId", 0, 134218463, 0, &String_t_0_0_0},
	{"amount", 1, 134218464, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithVirtualItem::.ctor(System.String,System.Int32)
MethodInfo PurchaseWithVirtualItem__ctor_m678_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PurchaseWithVirtualItem__ctor_m678/* method */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem__ctor_m678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem_Buy_m679_ParameterInfos[] = 
{
	{"payload", 0, 134218465, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.Store.PurchaseWithVirtualItem::Buy(System.String)
MethodInfo PurchaseWithVirtualItem_Buy_m679_MethodInfo = 
{
	"Buy"/* name */
	, (methodPointerType)&PurchaseWithVirtualItem_Buy_m679/* method */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem_Buy_m679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean Soomla.Store.PurchaseWithVirtualItem::CanAfford()
MethodInfo PurchaseWithVirtualItem_CanAfford_m680_MethodInfo = 
{
	"CanAfford"/* name */
	, (methodPointerType)&PurchaseWithVirtualItem_CanAfford_m680/* method */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualItem_t125_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Soomla.Store.VirtualItem Soomla.Store.PurchaseWithVirtualItem::getTargetVirtualItem()
MethodInfo PurchaseWithVirtualItem_getTargetVirtualItem_m681_MethodInfo = 
{
	"getTargetVirtualItem"/* name */
	, (methodPointerType)&PurchaseWithVirtualItem_getTargetVirtualItem_m681/* method */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* declaring_type */
	, &VirtualItem_t125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualItem_t125_0_0_0;
extern Il2CppType VirtualItem_t125_0_0_0;
static ParameterInfo PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem_checkTargetBalance_m682_ParameterInfos[] = 
{
	{"item", 0, 134218466, 0, &VirtualItem_t125_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Soomla.Store.PurchaseWithVirtualItem::checkTargetBalance(Soomla.Store.VirtualItem)
MethodInfo PurchaseWithVirtualItem_checkTargetBalance_m682_MethodInfo = 
{
	"checkTargetBalance"/* name */
	, (methodPointerType)&PurchaseWithVirtualItem_checkTargetBalance_m682/* method */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, PurchaseWithVirtualItem_t139_PurchaseWithVirtualItem_checkTargetBalance_m682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PurchaseWithVirtualItem_t139_MethodInfos[] =
{
	&PurchaseWithVirtualItem__ctor_m678_MethodInfo,
	&PurchaseWithVirtualItem_Buy_m679_MethodInfo,
	&PurchaseWithVirtualItem_CanAfford_m680_MethodInfo,
	&PurchaseWithVirtualItem_getTargetVirtualItem_m681_MethodInfo,
	&PurchaseWithVirtualItem_checkTargetBalance_m682_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_32849;
FieldInfo PurchaseWithVirtualItem_t139____TAG_1_FieldInfo = 
{
	"TAG"/* name */
	, &String_t_0_0_32849/* type */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo PurchaseWithVirtualItem_t139____TargetItemId_2_FieldInfo = 
{
	"TargetItemId"/* name */
	, &String_t_0_0_6/* type */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* parent */
	, offsetof(PurchaseWithVirtualItem_t139, ___TargetItemId_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo PurchaseWithVirtualItem_t139____Amount_3_FieldInfo = 
{
	"Amount"/* name */
	, &Int32_t189_0_0_6/* type */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* parent */
	, offsetof(PurchaseWithVirtualItem_t139, ___Amount_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* PurchaseWithVirtualItem_t139_FieldInfos[] =
{
	&PurchaseWithVirtualItem_t139____TAG_1_FieldInfo,
	&PurchaseWithVirtualItem_t139____TargetItemId_2_FieldInfo,
	&PurchaseWithVirtualItem_t139____Amount_3_FieldInfo,
	NULL
};
static const uint16_t PurchaseWithVirtualItem_t139____TAG_1_DefaultValueData[] = { 0x53, 0x4F, 0x4F, 0x4D, 0x4C, 0x41, 0x20, 0x50, 0x75, 0x72, 0x63, 0x68, 0x61, 0x73, 0x65, 0x57, 0x69, 0x74, 0x68, 0x56, 0x69, 0x72, 0x74, 0x75, 0x61, 0x6C, 0x49, 0x74, 0x65, 0x6D, 0x00 };
static Il2CppFieldDefaultValueEntry PurchaseWithVirtualItem_t139____TAG_1_DefaultValue = 
{
	&PurchaseWithVirtualItem_t139____TAG_1_FieldInfo/* field */
	, { (char*)&PurchaseWithVirtualItem_t139____TAG_1_DefaultValueData, &String_t_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* PurchaseWithVirtualItem_t139_FieldDefaultValues[] = 
{
	&PurchaseWithVirtualItem_t139____TAG_1_DefaultValue,
	NULL
};
static const Il2CppType* PurchaseWithVirtualItem_t139_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CBuyU3Ec__AnonStorey8_t140_0_0_0,
};
extern MethodInfo PurchaseWithVirtualItem_Buy_m679_MethodInfo;
extern MethodInfo PurchaseWithVirtualItem_CanAfford_m680_MethodInfo;
static Il2CppMethodReference PurchaseWithVirtualItem_t139_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&PurchaseWithVirtualItem_Buy_m679_MethodInfo,
	&PurchaseWithVirtualItem_CanAfford_m680_MethodInfo,
};
static bool PurchaseWithVirtualItem_t139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType PurchaseWithVirtualItem_t139_1_0_0;
struct PurchaseWithVirtualItem_t139;
const Il2CppTypeDefinitionMetadata PurchaseWithVirtualItem_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PurchaseWithVirtualItem_t139_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PurchaseType_t123_0_0_0/* parent */
	, PurchaseWithVirtualItem_t139_VTable/* vtableMethods */
	, PurchaseWithVirtualItem_t139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo PurchaseWithVirtualItem_t139_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "PurchaseWithVirtualItem"/* name */
	, "Soomla.Store"/* namespaze */
	, PurchaseWithVirtualItem_t139_MethodInfos/* methods */
	, NULL/* properties */
	, PurchaseWithVirtualItem_t139_FieldInfos/* fields */
	, NULL/* events */
	, &PurchaseWithVirtualItem_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PurchaseWithVirtualItem_t139_0_0_0/* byval_arg */
	, &PurchaseWithVirtualItem_t139_1_0_0/* this_arg */
	, &PurchaseWithVirtualItem_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, PurchaseWithVirtualItem_t139_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PurchaseWithVirtualItem_t139)/* instance_size */
	, sizeof (PurchaseWithVirtualItem_t139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Soomla.VirtualItemReward
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_VirtualItemReward.h"
// Metadata Definition Soomla.VirtualItemReward
extern TypeInfo VirtualItemReward_t141_il2cpp_TypeInfo;
// Soomla.VirtualItemReward
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_VirtualItemRewardMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo VirtualItemReward_t141_VirtualItemReward__ctor_m683_ParameterInfos[] = 
{
	{"rewardId", 0, 134218467, 0, &String_t_0_0_0},
	{"name", 1, 134218468, 0, &String_t_0_0_0},
	{"associatedItemId", 2, 134218469, 0, &String_t_0_0_0},
	{"amount", 3, 134218470, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.VirtualItemReward::.ctor(System.String,System.String,System.String,System.Int32)
MethodInfo VirtualItemReward__ctor_m683_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualItemReward__ctor_m683/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189/* invoker_method */
	, VirtualItemReward_t141_VirtualItemReward__ctor_m683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType JSONObject_t30_0_0_0;
extern Il2CppType JSONObject_t30_0_0_0;
static ParameterInfo VirtualItemReward_t141_VirtualItemReward__ctor_m684_ParameterInfos[] = 
{
	{"jsonReward", 0, 134218471, 0, &JSONObject_t30_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.VirtualItemReward::.ctor(JSONObject)
MethodInfo VirtualItemReward__ctor_m684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualItemReward__ctor_m684/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, VirtualItemReward_t141_VirtualItemReward__ctor_m684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void Soomla.VirtualItemReward::.cctor()
MethodInfo VirtualItemReward__cctor_m685_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&VirtualItemReward__cctor_m685/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType JSONObject_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// JSONObject Soomla.VirtualItemReward::toJSONObject()
MethodInfo VirtualItemReward_toJSONObject_m686_MethodInfo = 
{
	"toJSONObject"/* name */
	, (methodPointerType)&VirtualItemReward_toJSONObject_m686/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &JSONObject_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean Soomla.VirtualItemReward::giveInner()
MethodInfo VirtualItemReward_giveInner_m687_MethodInfo = 
{
	"giveInner"/* name */
	, (methodPointerType)&VirtualItemReward_giveInner_m687/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean Soomla.VirtualItemReward::takeInner()
MethodInfo VirtualItemReward_takeInner_m688_MethodInfo = 
{
	"takeInner"/* name */
	, (methodPointerType)&VirtualItemReward_takeInner_m688/* method */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VirtualItemReward_t141_MethodInfos[] =
{
	&VirtualItemReward__ctor_m683_MethodInfo,
	&VirtualItemReward__ctor_m684_MethodInfo,
	&VirtualItemReward__cctor_m685_MethodInfo,
	&VirtualItemReward_toJSONObject_m686_MethodInfo,
	&VirtualItemReward_giveInner_m687_MethodInfo,
	&VirtualItemReward_takeInner_m688_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_17;
FieldInfo VirtualItemReward_t141____TAG_7_FieldInfo = 
{
	"TAG"/* name */
	, &String_t_0_0_17/* type */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualItemReward_t141_StaticFields, ___TAG_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo VirtualItemReward_t141____AssociatedItemId_8_FieldInfo = 
{
	"AssociatedItemId"/* name */
	, &String_t_0_0_6/* type */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualItemReward_t141, ___AssociatedItemId_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo VirtualItemReward_t141____Amount_9_FieldInfo = 
{
	"Amount"/* name */
	, &Int32_t189_0_0_6/* type */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualItemReward_t141, ___Amount_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* VirtualItemReward_t141_FieldInfos[] =
{
	&VirtualItemReward_t141____TAG_7_FieldInfo,
	&VirtualItemReward_t141____AssociatedItemId_8_FieldInfo,
	&VirtualItemReward_t141____Amount_9_FieldInfo,
	NULL
};
extern Il2CppGenericMethod SoomlaEntity_1_Equals_m1307_GenericMethod;
extern Il2CppGenericMethod SoomlaEntity_1_GetHashCode_m1308_GenericMethod;
extern MethodInfo VirtualItemReward_toJSONObject_m686_MethodInfo;
extern Il2CppGenericMethod SoomlaEntity_1_Clone_m1309_GenericMethod;
extern MethodInfo VirtualItemReward_giveInner_m687_MethodInfo;
extern MethodInfo VirtualItemReward_takeInner_m688_MethodInfo;
static Il2CppMethodReference VirtualItemReward_t141_VTable[] =
{
	&SoomlaEntity_1_Equals_m1307_GenericMethod,
	&Object_Finalize_m1177_MethodInfo,
	&SoomlaEntity_1_GetHashCode_m1308_GenericMethod,
	&Object_ToString_m1179_MethodInfo,
	&VirtualItemReward_toJSONObject_m686_MethodInfo,
	&SoomlaEntity_1_Clone_m1309_GenericMethod,
	&VirtualItemReward_giveInner_m687_MethodInfo,
	&VirtualItemReward_takeInner_m688_MethodInfo,
};
static bool VirtualItemReward_t141_VTableIsGenericMethod[] =
{
	true,
	false,
	true,
	false,
	false,
	true,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType VirtualItemReward_t141_0_0_0;
extern Il2CppType VirtualItemReward_t141_1_0_0;
extern Il2CppType Reward_t55_0_0_0;
struct VirtualItemReward_t141;
const Il2CppTypeDefinitionMetadata VirtualItemReward_t141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reward_t55_0_0_0/* parent */
	, VirtualItemReward_t141_VTable/* vtableMethods */
	, VirtualItemReward_t141_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo VirtualItemReward_t141_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualItemReward"/* name */
	, "Soomla"/* namespaze */
	, VirtualItemReward_t141_MethodInfos/* methods */
	, NULL/* properties */
	, VirtualItemReward_t141_FieldInfos/* fields */
	, NULL/* events */
	, &VirtualItemReward_t141_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualItemReward_t141_0_0_0/* byval_arg */
	, &VirtualItemReward_t141_1_0_0/* this_arg */
	, &VirtualItemReward_t141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualItemReward_t141)/* instance_size */
	, sizeof (VirtualItemReward_t141)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(VirtualItemReward_t141_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
// Metadata Definition UnityEngine.Advertisements.Advertisement/DebugLevel
extern TypeInfo DebugLevel_t142_il2cpp_TypeInfo;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_AdvMethodDeclarations.h"
static MethodInfo* DebugLevel_t142_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo DebugLevel_t142____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, offsetof(DebugLevel_t142, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____None_2_FieldInfo = 
{
	"None"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____Error_3_FieldInfo = 
{
	"Error"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____Warning_4_FieldInfo = 
{
	"Warning"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____Info_5_FieldInfo = 
{
	"Info"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____Debug_6_FieldInfo = 
{
	"Debug"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____NONE_7_FieldInfo = 
{
	"NONE"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 120/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____ERROR_8_FieldInfo = 
{
	"ERROR"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 121/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____WARNING_9_FieldInfo = 
{
	"WARNING"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 122/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____INFO_10_FieldInfo = 
{
	"INFO"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 123/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_32854;
FieldInfo DebugLevel_t142____DEBUG_11_FieldInfo = 
{
	"DEBUG"/* name */
	, &DebugLevel_t142_0_0_32854/* type */
	, &DebugLevel_t142_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 124/* custom_attributes_cache */

};
static FieldInfo* DebugLevel_t142_FieldInfos[] =
{
	&DebugLevel_t142____value___1_FieldInfo,
	&DebugLevel_t142____None_2_FieldInfo,
	&DebugLevel_t142____Error_3_FieldInfo,
	&DebugLevel_t142____Warning_4_FieldInfo,
	&DebugLevel_t142____Info_5_FieldInfo,
	&DebugLevel_t142____Debug_6_FieldInfo,
	&DebugLevel_t142____NONE_7_FieldInfo,
	&DebugLevel_t142____ERROR_8_FieldInfo,
	&DebugLevel_t142____WARNING_9_FieldInfo,
	&DebugLevel_t142____INFO_10_FieldInfo,
	&DebugLevel_t142____DEBUG_11_FieldInfo,
	NULL
};
static const int32_t DebugLevel_t142____None_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____None_2_DefaultValue = 
{
	&DebugLevel_t142____None_2_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____None_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____Error_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____Error_3_DefaultValue = 
{
	&DebugLevel_t142____Error_3_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____Error_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____Warning_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____Warning_4_DefaultValue = 
{
	&DebugLevel_t142____Warning_4_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____Warning_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____Info_5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____Info_5_DefaultValue = 
{
	&DebugLevel_t142____Info_5_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____Info_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____Debug_6_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____Debug_6_DefaultValue = 
{
	&DebugLevel_t142____Debug_6_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____Debug_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____NONE_7_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____NONE_7_DefaultValue = 
{
	&DebugLevel_t142____NONE_7_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____NONE_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____ERROR_8_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____ERROR_8_DefaultValue = 
{
	&DebugLevel_t142____ERROR_8_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____ERROR_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____WARNING_9_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____WARNING_9_DefaultValue = 
{
	&DebugLevel_t142____WARNING_9_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____WARNING_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____INFO_10_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____INFO_10_DefaultValue = 
{
	&DebugLevel_t142____INFO_10_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____INFO_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t DebugLevel_t142____DEBUG_11_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry DebugLevel_t142____DEBUG_11_DefaultValue = 
{
	&DebugLevel_t142____DEBUG_11_FieldInfo/* field */
	, { (char*)&DebugLevel_t142____DEBUG_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* DebugLevel_t142_FieldDefaultValues[] = 
{
	&DebugLevel_t142____None_2_DefaultValue,
	&DebugLevel_t142____Error_3_DefaultValue,
	&DebugLevel_t142____Warning_4_DefaultValue,
	&DebugLevel_t142____Info_5_DefaultValue,
	&DebugLevel_t142____Debug_6_DefaultValue,
	&DebugLevel_t142____NONE_7_DefaultValue,
	&DebugLevel_t142____ERROR_8_DefaultValue,
	&DebugLevel_t142____WARNING_9_DefaultValue,
	&DebugLevel_t142____INFO_10_DefaultValue,
	&DebugLevel_t142____DEBUG_11_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference DebugLevel_t142_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool DebugLevel_t142_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair DebugLevel_t142_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType DebugLevel_t142_0_0_0;
extern Il2CppType DebugLevel_t142_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
extern TypeInfo Advertisement_t143_il2cpp_TypeInfo;
extern Il2CppType Advertisement_t143_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DebugLevel_t142_DefinitionMetadata = 
{
	&Advertisement_t143_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebugLevel_t142_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, DebugLevel_t142_VTable/* vtableMethods */
	, DebugLevel_t142_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo DebugLevel_t142_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebugLevel"/* name */
	, ""/* namespaze */
	, DebugLevel_t142_MethodInfos/* methods */
	, NULL/* properties */
	, DebugLevel_t142_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DebugLevel_t142_0_0_0/* byval_arg */
	, &DebugLevel_t142_1_0_0/* this_arg */
	, &DebugLevel_t142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, DebugLevel_t142_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebugLevel_t142)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DebugLevel_t142)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Advertisements.Advertisement
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv_0.h"
// Metadata Definition UnityEngine.Advertisements.Advertisement
// UnityEngine.Advertisements.Advertisement
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv_0MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::.cctor()
MethodInfo Advertisement__cctor_m689_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Advertisement__cctor_m689/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
extern void* RuntimeInvoker_DebugLevel_t142 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Advertisements.Advertisement/DebugLevel UnityEngine.Advertisements.Advertisement::get_debugLevel()
MethodInfo Advertisement_get_debugLevel_m690_MethodInfo = 
{
	"get_debugLevel"/* name */
	, (methodPointerType)&Advertisement_get_debugLevel_m690/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &DebugLevel_t142_0_0_0/* return_type */
	, RuntimeInvoker_DebugLevel_t142/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
static ParameterInfo Advertisement_t143_Advertisement_set_debugLevel_m691_ParameterInfos[] = 
{
	{"value", 0, 134218472, 0, &DebugLevel_t142_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::set_debugLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
MethodInfo Advertisement_set_debugLevel_m691_MethodInfo = 
{
	"set_debugLevel"/* name */
	, (methodPointerType)&Advertisement_set_debugLevel_m691/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, Advertisement_t143_Advertisement_set_debugLevel_m691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
MethodInfo Advertisement_get_isSupported_m692_MethodInfo = 
{
	"get_isSupported"/* name */
	, (methodPointerType)&Advertisement_get_isSupported_m692/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
MethodInfo Advertisement_get_isInitialized_m693_MethodInfo = 
{
	"get_isInitialized"/* name */
	, (methodPointerType)&Advertisement_get_isInitialized_m693/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_4112;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Advertisement_t143_Advertisement_Initialize_m694_ParameterInfos[] = 
{
	{"appId", 0, 134218473, 0, &String_t_0_0_0},
	{"testMode", 1, 134218474, 0, &Boolean_t203_0_0_4112},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
MethodInfo Advertisement_Initialize_m694_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&Advertisement_Initialize_m694/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, Advertisement_t143_Advertisement_Initialize_m694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_4112;
extern Il2CppType ShowOptions_t153_0_0_4112;
extern Il2CppType ShowOptions_t153_0_0_0;
static ParameterInfo Advertisement_t143_Advertisement_Show_m695_ParameterInfos[] = 
{
	{"zoneId", 0, 134218475, 0, &String_t_0_0_4112},
	{"options", 1, 134218476, 0, &ShowOptions_t153_0_0_4112},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
MethodInfo Advertisement_Show_m695_MethodInfo = 
{
	"Show"/* name */
	, (methodPointerType)&Advertisement_Show_m695/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Advertisement_t143_Advertisement_Show_m695_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_allowPrecache()
MethodInfo Advertisement_get_allowPrecache_m696_MethodInfo = 
{
	"get_allowPrecache"/* name */
	, (methodPointerType)&Advertisement_get_allowPrecache_m696/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Advertisement_t143_Advertisement_set_allowPrecache_m697_ParameterInfos[] = 
{
	{"value", 0, 134218477, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::set_allowPrecache(System.Boolean)
MethodInfo Advertisement_set_allowPrecache_m697_MethodInfo = 
{
	"set_allowPrecache"/* name */
	, (methodPointerType)&Advertisement_set_allowPrecache_m697/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Advertisement_t143_Advertisement_set_allowPrecache_m697_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_4112;
static ParameterInfo Advertisement_t143_Advertisement_IsReady_m698_ParameterInfos[] = 
{
	{"zoneId", 0, 134218478, 0, &String_t_0_0_4112},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
MethodInfo Advertisement_IsReady_m698_MethodInfo = 
{
	"IsReady"/* name */
	, (methodPointerType)&Advertisement_IsReady_m698/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Advertisement_t143_Advertisement_IsReady_m698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_4112;
static ParameterInfo Advertisement_t143_Advertisement_isReady_m699_ParameterInfos[] = 
{
	{"zoneId", 0, 134218479, 0, &String_t_0_0_4112},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::isReady(System.String)
MethodInfo Advertisement_isReady_m699_MethodInfo = 
{
	"isReady"/* name */
	, (methodPointerType)&Advertisement_isReady_m699/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, Advertisement_t143_Advertisement_isReady_m699_ParameterInfos/* parameters */
	, 116/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
MethodInfo Advertisement_get_isShowing_m700_MethodInfo = 
{
	"get_isShowing"/* name */
	, (methodPointerType)&Advertisement_get_isShowing_m700/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.Advertisement::get_UnityDeveloperInternalTestMode()
MethodInfo Advertisement_get_UnityDeveloperInternalTestMode_m701_MethodInfo = 
{
	"get_UnityDeveloperInternalTestMode"/* name */
	, (methodPointerType)&Advertisement_get_UnityDeveloperInternalTestMode_m701/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 117/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Advertisement_t143_Advertisement_set_UnityDeveloperInternalTestMode_m702_ParameterInfos[] = 
{
	{"value", 0, 134218480, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Advertisement::set_UnityDeveloperInternalTestMode(System.Boolean)
MethodInfo Advertisement_set_UnityDeveloperInternalTestMode_m702_MethodInfo = 
{
	"set_UnityDeveloperInternalTestMode"/* name */
	, (methodPointerType)&Advertisement_set_UnityDeveloperInternalTestMode_m702/* method */
	, &Advertisement_t143_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Advertisement_t143_Advertisement_set_UnityDeveloperInternalTestMode_m702_ParameterInfos/* parameters */
	, 118/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Advertisement_t143_MethodInfos[] =
{
	&Advertisement__cctor_m689_MethodInfo,
	&Advertisement_get_debugLevel_m690_MethodInfo,
	&Advertisement_set_debugLevel_m691_MethodInfo,
	&Advertisement_get_isSupported_m692_MethodInfo,
	&Advertisement_get_isInitialized_m693_MethodInfo,
	&Advertisement_Initialize_m694_MethodInfo,
	&Advertisement_Show_m695_MethodInfo,
	&Advertisement_get_allowPrecache_m696_MethodInfo,
	&Advertisement_set_allowPrecache_m697_MethodInfo,
	&Advertisement_IsReady_m698_MethodInfo,
	&Advertisement_isReady_m699_MethodInfo,
	&Advertisement_get_isShowing_m700_MethodInfo,
	&Advertisement_get_UnityDeveloperInternalTestMode_m701_MethodInfo,
	&Advertisement_set_UnityDeveloperInternalTestMode_m702_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_54;
FieldInfo Advertisement_t143____version_0_FieldInfo = 
{
	"version"/* name */
	, &String_t_0_0_54/* type */
	, &Advertisement_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Advertisement_t143_StaticFields, ___version_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType DebugLevel_t142_0_0_17;
FieldInfo Advertisement_t143_____debugLevel_1_FieldInfo = 
{
	"_debugLevel"/* name */
	, &DebugLevel_t142_0_0_17/* type */
	, &Advertisement_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Advertisement_t143_StaticFields, ____debugLevel_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo Advertisement_t143____U3CUnityDeveloperInternalTestModeU3Ek__BackingField_2_FieldInfo = 
{
	"<UnityDeveloperInternalTestMode>k__BackingField"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &Advertisement_t143_il2cpp_TypeInfo/* parent */
	, offsetof(Advertisement_t143_StaticFields, ___U3CUnityDeveloperInternalTestModeU3Ek__BackingField_2)/* offset */
	, 115/* custom_attributes_cache */

};
static FieldInfo* Advertisement_t143_FieldInfos[] =
{
	&Advertisement_t143____version_0_FieldInfo,
	&Advertisement_t143_____debugLevel_1_FieldInfo,
	&Advertisement_t143____U3CUnityDeveloperInternalTestModeU3Ek__BackingField_2_FieldInfo,
	NULL
};
extern MethodInfo Advertisement_get_debugLevel_m690_MethodInfo;
extern MethodInfo Advertisement_set_debugLevel_m691_MethodInfo;
static PropertyInfo Advertisement_t143____debugLevel_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "debugLevel"/* name */
	, &Advertisement_get_debugLevel_m690_MethodInfo/* get */
	, &Advertisement_set_debugLevel_m691_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Advertisement_get_isSupported_m692_MethodInfo;
static PropertyInfo Advertisement_t143____isSupported_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "isSupported"/* name */
	, &Advertisement_get_isSupported_m692_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Advertisement_get_isInitialized_m693_MethodInfo;
static PropertyInfo Advertisement_t143____isInitialized_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "isInitialized"/* name */
	, &Advertisement_get_isInitialized_m693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Advertisement_get_allowPrecache_m696_MethodInfo;
extern MethodInfo Advertisement_set_allowPrecache_m697_MethodInfo;
static PropertyInfo Advertisement_t143____allowPrecache_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "allowPrecache"/* name */
	, &Advertisement_get_allowPrecache_m696_MethodInfo/* get */
	, &Advertisement_set_allowPrecache_m697_MethodInfo/* set */
	, 0/* attrs */
	, 119/* custom_attributes_cache */

};
extern MethodInfo Advertisement_get_isShowing_m700_MethodInfo;
static PropertyInfo Advertisement_t143____isShowing_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "isShowing"/* name */
	, &Advertisement_get_isShowing_m700_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Advertisement_get_UnityDeveloperInternalTestMode_m701_MethodInfo;
extern MethodInfo Advertisement_set_UnityDeveloperInternalTestMode_m702_MethodInfo;
static PropertyInfo Advertisement_t143____UnityDeveloperInternalTestMode_PropertyInfo = 
{
	&Advertisement_t143_il2cpp_TypeInfo/* parent */
	, "UnityDeveloperInternalTestMode"/* name */
	, &Advertisement_get_UnityDeveloperInternalTestMode_m701_MethodInfo/* get */
	, &Advertisement_set_UnityDeveloperInternalTestMode_m702_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Advertisement_t143_PropertyInfos[] =
{
	&Advertisement_t143____debugLevel_PropertyInfo,
	&Advertisement_t143____isSupported_PropertyInfo,
	&Advertisement_t143____isInitialized_PropertyInfo,
	&Advertisement_t143____allowPrecache_PropertyInfo,
	&Advertisement_t143____isShowing_PropertyInfo,
	&Advertisement_t143____UnityDeveloperInternalTestMode_PropertyInfo,
	NULL
};
static const Il2CppType* Advertisement_t143_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DebugLevel_t142_0_0_0,
};
static Il2CppMethodReference Advertisement_t143_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Advertisement_t143_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType Advertisement_t143_1_0_0;
struct Advertisement_t143;
const Il2CppTypeDefinitionMetadata Advertisement_t143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Advertisement_t143_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Advertisement_t143_VTable/* vtableMethods */
	, Advertisement_t143_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Advertisement_t143_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Advertisement"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, Advertisement_t143_MethodInfos/* methods */
	, Advertisement_t143_PropertyInfos/* properties */
	, Advertisement_t143_FieldInfos/* fields */
	, NULL/* events */
	, &Advertisement_t143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Advertisement_t143_0_0_0/* byval_arg */
	, &Advertisement_t143_1_0_0/* this_arg */
	, &Advertisement_t143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Advertisement_t143)/* instance_size */
	, sizeof (Advertisement_t143)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Advertisement_t143_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 6/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.AsyncExec
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Asy.h"
// Metadata Definition UnityEngine.Advertisements.AsyncExec
extern TypeInfo AsyncExec_t145_il2cpp_TypeInfo;
// UnityEngine.Advertisements.AsyncExec
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_AsyMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.AsyncExec::.ctor()
MethodInfo AsyncExec__ctor_m703_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsyncExec__ctor_m703/* method */
	, &AsyncExec_t145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.AsyncExec::.cctor()
MethodInfo AsyncExec__cctor_m704_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&AsyncExec__cctor_m704/* method */
	, &AsyncExec_t145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MonoBehaviour_t26_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.MonoBehaviour UnityEngine.Advertisements.AsyncExec::getImpl()
MethodInfo AsyncExec_getImpl_m705_MethodInfo = 
{
	"getImpl"/* name */
	, (methodPointerType)&AsyncExec_getImpl_m705/* method */
	, &AsyncExec_t145_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t26_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncExec_t145_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Advertisements.AsyncExec UnityEngine.Advertisements.AsyncExec::getAsyncImpl()
MethodInfo AsyncExec_getAsyncImpl_m706_MethodInfo = 
{
	"getAsyncImpl"/* name */
	, (methodPointerType)&AsyncExec_getAsyncImpl_m706/* method */
	, &AsyncExec_t145_il2cpp_TypeInfo/* declaring_type */
	, &AsyncExec_t145_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_3_t325_0_0_0;
extern Il2CppType Func_3_t325_0_0_0;
extern Il2CppType AsyncExec_runWithCallback_m1160_gp_0_0_0_0;
extern Il2CppType AsyncExec_runWithCallback_m1160_gp_0_0_0_0;
extern Il2CppType Action_1_t327_0_0_0;
extern Il2CppType Action_1_t327_0_0_0;
static ParameterInfo AsyncExec_t145_AsyncExec_runWithCallback_m1160_ParameterInfos[] = 
{
	{"asyncMethod", 0, 134218481, 0, &Func_3_t325_0_0_0},
	{"arg0", 1, 134218482, 0, &AsyncExec_runWithCallback_m1160_gp_0_0_0_0},
	{"callback", 2, 134218483, 0, &Action_1_t327_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern Il2CppGenericContainer AsyncExec_runWithCallback_m1160_Il2CppGenericContainer;
extern TypeInfo AsyncExec_runWithCallback_m1160_gp_K_0_il2cpp_TypeInfo;
Il2CppGenericParamFull AsyncExec_runWithCallback_m1160_gp_K_0_il2cpp_TypeInfo_GenericParamFull = { { &AsyncExec_runWithCallback_m1160_Il2CppGenericContainer, 0}, {NULL, "K", 0, 0, NULL} };
extern TypeInfo AsyncExec_runWithCallback_m1160_gp_T_1_il2cpp_TypeInfo;
Il2CppGenericParamFull AsyncExec_runWithCallback_m1160_gp_T_1_il2cpp_TypeInfo_GenericParamFull = { { &AsyncExec_runWithCallback_m1160_Il2CppGenericContainer, 1}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* AsyncExec_runWithCallback_m1160_Il2CppGenericParametersArray[2] = 
{
	&AsyncExec_runWithCallback_m1160_gp_K_0_il2cpp_TypeInfo_GenericParamFull,
	&AsyncExec_runWithCallback_m1160_gp_T_1_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo AsyncExec_runWithCallback_m1160_MethodInfo;
Il2CppGenericContainer AsyncExec_runWithCallback_m1160_Il2CppGenericContainer = { { NULL, NULL }, NULL, &AsyncExec_runWithCallback_m1160_MethodInfo, 2, 1, AsyncExec_runWithCallback_m1160_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Func_3_Invoke_m1318_GenericMethod;
static Il2CppRGCTXDefinition AsyncExec_runWithCallback_m1160_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_3_Invoke_m1318_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Advertisements.AsyncExec::runWithCallback(System.Func`3<K,System.Action`1<T>,System.Collections.IEnumerator>,K,System.Action`1<T>)
MethodInfo AsyncExec_runWithCallback_m1160_MethodInfo = 
{
	"runWithCallback"/* name */
	, NULL/* method */
	, &AsyncExec_t145_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, NULL/* invoker_method */
	, AsyncExec_t145_AsyncExec_runWithCallback_m1160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, AsyncExec_runWithCallback_m1160_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &AsyncExec_runWithCallback_m1160_Il2CppGenericContainer/* genericContainer */

};
static MethodInfo* AsyncExec_t145_MethodInfos[] =
{
	&AsyncExec__ctor_m703_MethodInfo,
	&AsyncExec__cctor_m704_MethodInfo,
	&AsyncExec_getImpl_m705_MethodInfo,
	&AsyncExec_getAsyncImpl_m706_MethodInfo,
	&AsyncExec_runWithCallback_m1160_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_17;
FieldInfo AsyncExec_t145____asyncExecGameObject_0_FieldInfo = 
{
	"asyncExecGameObject"/* name */
	, &GameObject_t144_0_0_17/* type */
	, &AsyncExec_t145_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncExec_t145_StaticFields, ___asyncExecGameObject_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MonoBehaviour_t26_0_0_17;
FieldInfo AsyncExec_t145____coroutineHost_1_FieldInfo = 
{
	"coroutineHost"/* name */
	, &MonoBehaviour_t26_0_0_17/* type */
	, &AsyncExec_t145_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncExec_t145_StaticFields, ___coroutineHost_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AsyncExec_t145_0_0_17;
FieldInfo AsyncExec_t145____asyncImpl_2_FieldInfo = 
{
	"asyncImpl"/* name */
	, &AsyncExec_t145_0_0_17/* type */
	, &AsyncExec_t145_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncExec_t145_StaticFields, ___asyncImpl_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo AsyncExec_t145____init_3_FieldInfo = 
{
	"init"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &AsyncExec_t145_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncExec_t145_StaticFields, ___init_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AsyncExec_t145_FieldInfos[] =
{
	&AsyncExec_t145____asyncExecGameObject_0_FieldInfo,
	&AsyncExec_t145____coroutineHost_1_FieldInfo,
	&AsyncExec_t145____asyncImpl_2_FieldInfo,
	&AsyncExec_t145____init_3_FieldInfo,
	NULL
};
static Il2CppMethodReference AsyncExec_t145_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool AsyncExec_t145_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType AsyncExec_t145_0_0_0;
extern Il2CppType AsyncExec_t145_1_0_0;
struct AsyncExec_t145;
const Il2CppTypeDefinitionMetadata AsyncExec_t145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncExec_t145_VTable/* vtableMethods */
	, AsyncExec_t145_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AsyncExec_t145_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncExec"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, AsyncExec_t145_MethodInfos/* methods */
	, NULL/* properties */
	, AsyncExec_t145_FieldInfos/* fields */
	, NULL/* events */
	, &AsyncExec_t145_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsyncExec_t145_0_0_0/* byval_arg */
	, &AsyncExec_t145_1_0_0/* this_arg */
	, &AsyncExec_t145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncExec_t145)/* instance_size */
	, sizeof (AsyncExec_t145)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AsyncExec_t145_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min.h"
// Metadata Definition UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
extern TypeInfo TOKEN_t146_il2cpp_TypeInfo;
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_MinMethodDeclarations.h"
static MethodInfo* TOKEN_t146_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TOKEN_t146____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, offsetof(TOKEN_t146, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____NONE_2_FieldInfo = 
{
	"NONE"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____CURLY_OPEN_3_FieldInfo = 
{
	"CURLY_OPEN"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____CURLY_CLOSE_4_FieldInfo = 
{
	"CURLY_CLOSE"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____SQUARED_OPEN_5_FieldInfo = 
{
	"SQUARED_OPEN"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____SQUARED_CLOSE_6_FieldInfo = 
{
	"SQUARED_CLOSE"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____COLON_7_FieldInfo = 
{
	"COLON"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____COMMA_8_FieldInfo = 
{
	"COMMA"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____STRING_9_FieldInfo = 
{
	"STRING"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____NUMBER_10_FieldInfo = 
{
	"NUMBER"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____TRUE_11_FieldInfo = 
{
	"TRUE"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____FALSE_12_FieldInfo = 
{
	"FALSE"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TOKEN_t146_0_0_32854;
FieldInfo TOKEN_t146____NULL_13_FieldInfo = 
{
	"NULL"/* name */
	, &TOKEN_t146_0_0_32854/* type */
	, &TOKEN_t146_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TOKEN_t146_FieldInfos[] =
{
	&TOKEN_t146____value___1_FieldInfo,
	&TOKEN_t146____NONE_2_FieldInfo,
	&TOKEN_t146____CURLY_OPEN_3_FieldInfo,
	&TOKEN_t146____CURLY_CLOSE_4_FieldInfo,
	&TOKEN_t146____SQUARED_OPEN_5_FieldInfo,
	&TOKEN_t146____SQUARED_CLOSE_6_FieldInfo,
	&TOKEN_t146____COLON_7_FieldInfo,
	&TOKEN_t146____COMMA_8_FieldInfo,
	&TOKEN_t146____STRING_9_FieldInfo,
	&TOKEN_t146____NUMBER_10_FieldInfo,
	&TOKEN_t146____TRUE_11_FieldInfo,
	&TOKEN_t146____FALSE_12_FieldInfo,
	&TOKEN_t146____NULL_13_FieldInfo,
	NULL
};
static const int32_t TOKEN_t146____NONE_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TOKEN_t146____NONE_2_DefaultValue = 
{
	&TOKEN_t146____NONE_2_FieldInfo/* field */
	, { (char*)&TOKEN_t146____NONE_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____CURLY_OPEN_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TOKEN_t146____CURLY_OPEN_3_DefaultValue = 
{
	&TOKEN_t146____CURLY_OPEN_3_FieldInfo/* field */
	, { (char*)&TOKEN_t146____CURLY_OPEN_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____CURLY_CLOSE_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TOKEN_t146____CURLY_CLOSE_4_DefaultValue = 
{
	&TOKEN_t146____CURLY_CLOSE_4_FieldInfo/* field */
	, { (char*)&TOKEN_t146____CURLY_CLOSE_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____SQUARED_OPEN_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TOKEN_t146____SQUARED_OPEN_5_DefaultValue = 
{
	&TOKEN_t146____SQUARED_OPEN_5_FieldInfo/* field */
	, { (char*)&TOKEN_t146____SQUARED_OPEN_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____SQUARED_CLOSE_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TOKEN_t146____SQUARED_CLOSE_6_DefaultValue = 
{
	&TOKEN_t146____SQUARED_CLOSE_6_FieldInfo/* field */
	, { (char*)&TOKEN_t146____SQUARED_CLOSE_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____COLON_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TOKEN_t146____COLON_7_DefaultValue = 
{
	&TOKEN_t146____COLON_7_FieldInfo/* field */
	, { (char*)&TOKEN_t146____COLON_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____COMMA_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry TOKEN_t146____COMMA_8_DefaultValue = 
{
	&TOKEN_t146____COMMA_8_FieldInfo/* field */
	, { (char*)&TOKEN_t146____COMMA_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____STRING_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TOKEN_t146____STRING_9_DefaultValue = 
{
	&TOKEN_t146____STRING_9_FieldInfo/* field */
	, { (char*)&TOKEN_t146____STRING_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____NUMBER_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry TOKEN_t146____NUMBER_10_DefaultValue = 
{
	&TOKEN_t146____NUMBER_10_FieldInfo/* field */
	, { (char*)&TOKEN_t146____NUMBER_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____TRUE_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TOKEN_t146____TRUE_11_DefaultValue = 
{
	&TOKEN_t146____TRUE_11_FieldInfo/* field */
	, { (char*)&TOKEN_t146____TRUE_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____FALSE_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TOKEN_t146____FALSE_12_DefaultValue = 
{
	&TOKEN_t146____FALSE_12_FieldInfo/* field */
	, { (char*)&TOKEN_t146____FALSE_12_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TOKEN_t146____NULL_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry TOKEN_t146____NULL_13_DefaultValue = 
{
	&TOKEN_t146____NULL_13_FieldInfo/* field */
	, { (char*)&TOKEN_t146____NULL_13_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TOKEN_t146_FieldDefaultValues[] = 
{
	&TOKEN_t146____NONE_2_DefaultValue,
	&TOKEN_t146____CURLY_OPEN_3_DefaultValue,
	&TOKEN_t146____CURLY_CLOSE_4_DefaultValue,
	&TOKEN_t146____SQUARED_OPEN_5_DefaultValue,
	&TOKEN_t146____SQUARED_CLOSE_6_DefaultValue,
	&TOKEN_t146____COLON_7_DefaultValue,
	&TOKEN_t146____COMMA_8_DefaultValue,
	&TOKEN_t146____STRING_9_DefaultValue,
	&TOKEN_t146____NUMBER_10_DefaultValue,
	&TOKEN_t146____TRUE_11_DefaultValue,
	&TOKEN_t146____FALSE_12_DefaultValue,
	&TOKEN_t146____NULL_13_DefaultValue,
	NULL
};
static Il2CppMethodReference TOKEN_t146_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TOKEN_t146_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TOKEN_t146_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType TOKEN_t146_0_0_0;
extern Il2CppType TOKEN_t146_1_0_0;
extern TypeInfo Parser_t148_il2cpp_TypeInfo;
extern Il2CppType Parser_t148_0_0_0;
const Il2CppTypeDefinitionMetadata TOKEN_t146_DefinitionMetadata = 
{
	&Parser_t148_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TOKEN_t146_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TOKEN_t146_VTable/* vtableMethods */
	, TOKEN_t146_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TOKEN_t146_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "TOKEN"/* name */
	, ""/* namespaze */
	, TOKEN_t146_MethodInfos/* methods */
	, NULL/* properties */
	, TOKEN_t146_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TOKEN_t146_0_0_0/* byval_arg */
	, &TOKEN_t146_1_0_0/* this_arg */
	, &TOKEN_t146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TOKEN_t146_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TOKEN_t146)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TOKEN_t146)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Advertisements.MiniJSON.Json/Parser
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_0.h"
// Metadata Definition UnityEngine.Advertisements.MiniJSON.Json/Parser
// UnityEngine.Advertisements.MiniJSON.Json/Parser
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_0MethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo Parser_t148_Parser__ctor_m707_ParameterInfos[] = 
{
	{"jsonString", 0, 134218486, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::.ctor(System.String)
MethodInfo Parser__ctor_m707_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Parser__ctor_m707/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Parser_t148_Parser__ctor_m707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern Il2CppType Char_t193_0_0_0;
static ParameterInfo Parser_t148_Parser_IsWordBreak_m708_ParameterInfos[] = 
{
	{"c", 0, 134218487, 0, &Char_t193_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.MiniJSON.Json/Parser::IsWordBreak(System.Char)
MethodInfo Parser_IsWordBreak_m708_MethodInfo = 
{
	"IsWordBreak"/* name */
	, (methodPointerType)&Parser_IsWordBreak_m708/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Int16_t238/* invoker_method */
	, Parser_t148_Parser_IsWordBreak_m708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Parser_t148_Parser_Parse_m709_ParameterInfos[] = 
{
	{"jsonString", 0, 134218488, 0, &String_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::Parse(System.String)
MethodInfo Parser_Parse_m709_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Parser_Parse_m709/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t148_Parser_Parse_m709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::Dispose()
MethodInfo Parser_Dispose_m710_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Parser_Dispose_m710/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Dictionary_2_t180_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseObject()
MethodInfo Parser_ParseObject_m711_MethodInfo = 
{
	"ParseObject"/* name */
	, (methodPointerType)&Parser_ParseObject_m711/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t180_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t181_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseArray()
MethodInfo Parser_ParseArray_m712_MethodInfo = 
{
	"ParseArray"/* name */
	, (methodPointerType)&Parser_ParseArray_m712/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseValue()
MethodInfo Parser_ParseValue_m713_MethodInfo = 
{
	"ParseValue"/* name */
	, (methodPointerType)&Parser_ParseValue_m713/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TOKEN_t146_0_0_0;
static ParameterInfo Parser_t148_Parser_ParseByToken_m714_ParameterInfos[] = 
{
	{"token", 0, 134218489, 0, &TOKEN_t146_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN)
MethodInfo Parser_ParseByToken_m714_MethodInfo = 
{
	"ParseByToken"/* name */
	, (methodPointerType)&Parser_ParseByToken_m714/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, Parser_t148_Parser_ParseByToken_m714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseString()
MethodInfo Parser_ParseString_m715_MethodInfo = 
{
	"ParseString"/* name */
	, (methodPointerType)&Parser_ParseString_m715/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseNumber()
MethodInfo Parser_ParseNumber_m716_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m716/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::EatWhitespace()
MethodInfo Parser_EatWhitespace_m717_MethodInfo = 
{
	"EatWhitespace"/* name */
	, (methodPointerType)&Parser_EatWhitespace_m717/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern void* RuntimeInvoker_Char_t193 (MethodInfo* method, void* obj, void** args);
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_PeekChar()
MethodInfo Parser_get_PeekChar_m718_MethodInfo = 
{
	"get_PeekChar"/* name */
	, (methodPointerType)&Parser_get_PeekChar_m718/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Char_t193_0_0_0/* return_type */
	, RuntimeInvoker_Char_t193/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Char_t193_0_0_0;
extern void* RuntimeInvoker_Char_t193 (MethodInfo* method, void* obj, void** args);
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextChar()
MethodInfo Parser_get_NextChar_m719_MethodInfo = 
{
	"get_NextChar"/* name */
	, (methodPointerType)&Parser_get_NextChar_m719/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &Char_t193_0_0_0/* return_type */
	, RuntimeInvoker_Char_t193/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextWord()
MethodInfo Parser_get_NextWord_m720_MethodInfo = 
{
	"get_NextWord"/* name */
	, (methodPointerType)&Parser_get_NextWord_m720/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TOKEN_t146_0_0_0;
extern void* RuntimeInvoker_TOKEN_t146 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextToken()
MethodInfo Parser_get_NextToken_m721_MethodInfo = 
{
	"get_NextToken"/* name */
	, (methodPointerType)&Parser_get_NextToken_m721/* method */
	, &Parser_t148_il2cpp_TypeInfo/* declaring_type */
	, &TOKEN_t146_0_0_0/* return_type */
	, RuntimeInvoker_TOKEN_t146/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Parser_t148_MethodInfos[] =
{
	&Parser__ctor_m707_MethodInfo,
	&Parser_IsWordBreak_m708_MethodInfo,
	&Parser_Parse_m709_MethodInfo,
	&Parser_Dispose_m710_MethodInfo,
	&Parser_ParseObject_m711_MethodInfo,
	&Parser_ParseArray_m712_MethodInfo,
	&Parser_ParseValue_m713_MethodInfo,
	&Parser_ParseByToken_m714_MethodInfo,
	&Parser_ParseString_m715_MethodInfo,
	&Parser_ParseNumber_m716_MethodInfo,
	&Parser_EatWhitespace_m717_MethodInfo,
	&Parser_get_PeekChar_m718_MethodInfo,
	&Parser_get_NextChar_m719_MethodInfo,
	&Parser_get_NextWord_m720_MethodInfo,
	&Parser_get_NextToken_m721_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_32849;
FieldInfo Parser_t148____WORD_BREAK_0_FieldInfo = 
{
	"WORD_BREAK"/* name */
	, &String_t_0_0_32849/* type */
	, &Parser_t148_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringReader_t147_0_0_1;
FieldInfo Parser_t148____json_1_FieldInfo = 
{
	"json"/* name */
	, &StringReader_t147_0_0_1/* type */
	, &Parser_t148_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t148, ___json_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo Parser_t148____U3CU3Ef__switchU24map2_2_FieldInfo = 
{
	"<>f__switch$map2"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &Parser_t148_il2cpp_TypeInfo/* parent */
	, offsetof(Parser_t148_StaticFields, ___U3CU3Ef__switchU24map2_2)/* offset */
	, 125/* custom_attributes_cache */

};
static FieldInfo* Parser_t148_FieldInfos[] =
{
	&Parser_t148____WORD_BREAK_0_FieldInfo,
	&Parser_t148____json_1_FieldInfo,
	&Parser_t148____U3CU3Ef__switchU24map2_2_FieldInfo,
	NULL
};
static const uint16_t Parser_t148____WORD_BREAK_0_DefaultValueData[] = { 0x7B, 0x7D, 0x5B, 0x5D, 0x2C, 0x3A, 0x22, 0x00 };
static Il2CppFieldDefaultValueEntry Parser_t148____WORD_BREAK_0_DefaultValue = 
{
	&Parser_t148____WORD_BREAK_0_FieldInfo/* field */
	, { (char*)&Parser_t148____WORD_BREAK_0_DefaultValueData, &String_t_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Parser_t148_FieldDefaultValues[] = 
{
	&Parser_t148____WORD_BREAK_0_DefaultValue,
	NULL
};
extern MethodInfo Parser_get_PeekChar_m718_MethodInfo;
static PropertyInfo Parser_t148____PeekChar_PropertyInfo = 
{
	&Parser_t148_il2cpp_TypeInfo/* parent */
	, "PeekChar"/* name */
	, &Parser_get_PeekChar_m718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Parser_get_NextChar_m719_MethodInfo;
static PropertyInfo Parser_t148____NextChar_PropertyInfo = 
{
	&Parser_t148_il2cpp_TypeInfo/* parent */
	, "NextChar"/* name */
	, &Parser_get_NextChar_m719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Parser_get_NextWord_m720_MethodInfo;
static PropertyInfo Parser_t148____NextWord_PropertyInfo = 
{
	&Parser_t148_il2cpp_TypeInfo/* parent */
	, "NextWord"/* name */
	, &Parser_get_NextWord_m720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Parser_get_NextToken_m721_MethodInfo;
static PropertyInfo Parser_t148____NextToken_PropertyInfo = 
{
	&Parser_t148_il2cpp_TypeInfo/* parent */
	, "NextToken"/* name */
	, &Parser_get_NextToken_m721_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Parser_t148_PropertyInfos[] =
{
	&Parser_t148____PeekChar_PropertyInfo,
	&Parser_t148____NextChar_PropertyInfo,
	&Parser_t148____NextWord_PropertyInfo,
	&Parser_t148____NextToken_PropertyInfo,
	NULL
};
static const Il2CppType* Parser_t148_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TOKEN_t146_0_0_0,
};
extern MethodInfo Parser_Dispose_m710_MethodInfo;
static Il2CppMethodReference Parser_t148_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&Parser_Dispose_m710_MethodInfo,
};
static bool Parser_t148_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IDisposable_t191_0_0_0;
static const Il2CppType* Parser_t148_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
};
static Il2CppInterfaceOffsetPair Parser_t148_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType Parser_t148_1_0_0;
extern TypeInfo Json_t150_il2cpp_TypeInfo;
extern Il2CppType Json_t150_0_0_0;
struct Parser_t148;
const Il2CppTypeDefinitionMetadata Parser_t148_DefinitionMetadata = 
{
	&Json_t150_0_0_0/* declaringType */
	, Parser_t148_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Parser_t148_InterfacesTypeInfos/* implementedInterfaces */
	, Parser_t148_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t148_VTable/* vtableMethods */
	, Parser_t148_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Parser_t148_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, ""/* namespaze */
	, Parser_t148_MethodInfos/* methods */
	, Parser_t148_PropertyInfos/* properties */
	, Parser_t148_FieldInfos/* fields */
	, NULL/* events */
	, &Parser_t148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t148_0_0_0/* byval_arg */
	, &Parser_t148_1_0_0/* this_arg */
	, &Parser_t148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, Parser_t148_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t148)/* instance_size */
	, sizeof (Parser_t148)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Parser_t148_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Advertisements.MiniJSON.Json/Serializer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_1.h"
// Metadata Definition UnityEngine.Advertisements.MiniJSON.Json/Serializer
extern TypeInfo Serializer_t149_il2cpp_TypeInfo;
// UnityEngine.Advertisements.MiniJSON.Json/Serializer
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_1MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::.ctor()
MethodInfo Serializer__ctor_m722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Serializer__ctor_m722/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Serializer_t149_Serializer_Serialize_m723_ParameterInfos[] = 
{
	{"obj", 0, 134218490, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.MiniJSON.Json/Serializer::Serialize(System.Object)
MethodInfo Serializer_Serialize_m723_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&Serializer_Serialize_m723/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Serializer_t149_Serializer_Serialize_m723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Serializer_t149_Serializer_SerializeValue_m724_ParameterInfos[] = 
{
	{"value", 0, 134218491, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeValue(System.Object)
MethodInfo Serializer_SerializeValue_m724_MethodInfo = 
{
	"SerializeValue"/* name */
	, (methodPointerType)&Serializer_SerializeValue_m724/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Serializer_t149_Serializer_SerializeValue_m724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern Il2CppType IDictionary_t182_0_0_0;
static ParameterInfo Serializer_t149_Serializer_SerializeObject_m725_ParameterInfos[] = 
{
	{"obj", 0, 134218492, 0, &IDictionary_t182_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
MethodInfo Serializer_SerializeObject_m725_MethodInfo = 
{
	"SerializeObject"/* name */
	, (methodPointerType)&Serializer_SerializeObject_m725/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Serializer_t149_Serializer_SerializeObject_m725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IList_t183_0_0_0;
extern Il2CppType IList_t183_0_0_0;
static ParameterInfo Serializer_t149_Serializer_SerializeArray_m726_ParameterInfos[] = 
{
	{"anArray", 0, 134218493, 0, &IList_t183_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
MethodInfo Serializer_SerializeArray_m726_MethodInfo = 
{
	"SerializeArray"/* name */
	, (methodPointerType)&Serializer_SerializeArray_m726/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Serializer_t149_Serializer_SerializeArray_m726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Serializer_t149_Serializer_SerializeString_m727_ParameterInfos[] = 
{
	{"str", 0, 134218494, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeString(System.String)
MethodInfo Serializer_SerializeString_m727_MethodInfo = 
{
	"SerializeString"/* name */
	, (methodPointerType)&Serializer_SerializeString_m727/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Serializer_t149_Serializer_SerializeString_m727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Serializer_t149_Serializer_SerializeOther_m728_ParameterInfos[] = 
{
	{"value", 0, 134218495, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeOther(System.Object)
MethodInfo Serializer_SerializeOther_m728_MethodInfo = 
{
	"SerializeOther"/* name */
	, (methodPointerType)&Serializer_SerializeOther_m728/* method */
	, &Serializer_t149_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Serializer_t149_Serializer_SerializeOther_m728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Serializer_t149_MethodInfos[] =
{
	&Serializer__ctor_m722_MethodInfo,
	&Serializer_Serialize_m723_MethodInfo,
	&Serializer_SerializeValue_m724_MethodInfo,
	&Serializer_SerializeObject_m725_MethodInfo,
	&Serializer_SerializeArray_m726_MethodInfo,
	&Serializer_SerializeString_m727_MethodInfo,
	&Serializer_SerializeOther_m728_MethodInfo,
	NULL
};
extern Il2CppType StringBuilder_t36_0_0_1;
FieldInfo Serializer_t149____builder_0_FieldInfo = 
{
	"builder"/* name */
	, &StringBuilder_t36_0_0_1/* type */
	, &Serializer_t149_il2cpp_TypeInfo/* parent */
	, offsetof(Serializer_t149, ___builder_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Serializer_t149_FieldInfos[] =
{
	&Serializer_t149____builder_0_FieldInfo,
	NULL
};
static Il2CppMethodReference Serializer_t149_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Serializer_t149_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType Serializer_t149_0_0_0;
extern Il2CppType Serializer_t149_1_0_0;
struct Serializer_t149;
const Il2CppTypeDefinitionMetadata Serializer_t149_DefinitionMetadata = 
{
	&Json_t150_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Serializer_t149_VTable/* vtableMethods */
	, Serializer_t149_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Serializer_t149_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Serializer"/* name */
	, ""/* namespaze */
	, Serializer_t149_MethodInfos/* methods */
	, NULL/* properties */
	, Serializer_t149_FieldInfos/* fields */
	, NULL/* events */
	, &Serializer_t149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Serializer_t149_0_0_0/* byval_arg */
	, &Serializer_t149_1_0_0/* this_arg */
	, &Serializer_t149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Serializer_t149)/* instance_size */
	, sizeof (Serializer_t149)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.MiniJSON.Json
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_2.h"
// Metadata Definition UnityEngine.Advertisements.MiniJSON.Json
// UnityEngine.Advertisements.MiniJSON.Json
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min_2MethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo Json_t150_Json_Deserialize_m729_ParameterInfos[] = 
{
	{"json", 0, 134218484, 0, &String_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Advertisements.MiniJSON.Json::Deserialize(System.String)
MethodInfo Json_Deserialize_m729_MethodInfo = 
{
	"Deserialize"/* name */
	, (methodPointerType)&Json_Deserialize_m729/* method */
	, &Json_t150_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Json_t150_Json_Deserialize_m729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Json_t150_Json_Serialize_m730_ParameterInfos[] = 
{
	{"obj", 0, 134218485, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.MiniJSON.Json::Serialize(System.Object)
MethodInfo Json_Serialize_m730_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&Json_Serialize_m730/* method */
	, &Json_t150_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Json_t150_Json_Serialize_m730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Json_t150_MethodInfos[] =
{
	&Json_Deserialize_m729_MethodInfo,
	&Json_Serialize_m730_MethodInfo,
	NULL
};
static const Il2CppType* Json_t150_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Parser_t148_0_0_0,
	&Serializer_t149_0_0_0,
};
static Il2CppMethodReference Json_t150_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Json_t150_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType Json_t150_1_0_0;
struct Json_t150;
const Il2CppTypeDefinitionMetadata Json_t150_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Json_t150_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Json_t150_VTable/* vtableMethods */
	, Json_t150_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Json_t150_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Json"/* name */
	, "UnityEngine.Advertisements.MiniJSON"/* namespaze */
	, Json_t150_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Json_t150_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Json_t150_0_0_0/* byval_arg */
	, &Json_t150_1_0_0/* this_arg */
	, &Json_t150_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Json_t150)/* instance_size */
	, sizeof (Json_t150)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.Utils
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uti.h"
// Metadata Definition UnityEngine.Advertisements.Utils
extern TypeInfo Utils_t151_il2cpp_TypeInfo;
// UnityEngine.Advertisements.Utils
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_UtiMethodDeclarations.h"
extern Il2CppType DebugLevel_t142_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Utils_t151_Utils_Log_m731_ParameterInfos[] = 
{
	{"debugLevel", 0, 134218496, 0, &DebugLevel_t142_0_0_0},
	{"message", 1, 134218497, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Utils::Log(UnityEngine.Advertisements.Advertisement/DebugLevel,System.String)
MethodInfo Utils_Log_m731_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Utils_Log_m731/* method */
	, &Utils_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_Object_t/* invoker_method */
	, Utils_t151_Utils_Log_m731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Utils_t151_Utils_LogDebug_m732_ParameterInfos[] = 
{
	{"message", 0, 134218498, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Utils::LogDebug(System.String)
MethodInfo Utils_LogDebug_m732_MethodInfo = 
{
	"LogDebug"/* name */
	, (methodPointerType)&Utils_LogDebug_m732/* method */
	, &Utils_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Utils_t151_Utils_LogDebug_m732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Utils_t151_Utils_LogInfo_m733_ParameterInfos[] = 
{
	{"message", 0, 134218499, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Utils::LogInfo(System.String)
MethodInfo Utils_LogInfo_m733_MethodInfo = 
{
	"LogInfo"/* name */
	, (methodPointerType)&Utils_LogInfo_m733/* method */
	, &Utils_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Utils_t151_Utils_LogInfo_m733_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Utils_t151_Utils_LogWarning_m734_ParameterInfos[] = 
{
	{"message", 0, 134218500, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Utils::LogWarning(System.String)
MethodInfo Utils_LogWarning_m734_MethodInfo = 
{
	"LogWarning"/* name */
	, (methodPointerType)&Utils_LogWarning_m734/* method */
	, &Utils_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Utils_t151_Utils_LogWarning_m734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Utils_t151_Utils_LogError_m735_ParameterInfos[] = 
{
	{"message", 0, 134218501, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Utils::LogError(System.String)
MethodInfo Utils_LogError_m735_MethodInfo = 
{
	"LogError"/* name */
	, (methodPointerType)&Utils_LogError_m735/* method */
	, &Utils_t151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Utils_t151_Utils_LogError_m735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Utils_t151_MethodInfos[] =
{
	&Utils_Log_m731_MethodInfo,
	&Utils_LogDebug_m732_MethodInfo,
	&Utils_LogInfo_m733_MethodInfo,
	&Utils_LogWarning_m734_MethodInfo,
	&Utils_LogError_m735_MethodInfo,
	NULL
};
static Il2CppMethodReference Utils_t151_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Utils_t151_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType Utils_t151_0_0_0;
extern Il2CppType Utils_t151_1_0_0;
struct Utils_t151;
const Il2CppTypeDefinitionMetadata Utils_t151_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Utils_t151_VTable/* vtableMethods */
	, Utils_t151_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Utils_t151_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "Utils"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, Utils_t151_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Utils_t151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Utils_t151_0_0_0/* byval_arg */
	, &Utils_t151_1_0_0/* this_arg */
	, &Utils_t151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Utils_t151)/* instance_size */
	, sizeof (Utils_t151)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.Optional.ShowOptionsExtended
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Opt.h"
// Metadata Definition UnityEngine.Advertisements.Optional.ShowOptionsExtended
extern TypeInfo ShowOptionsExtended_t152_il2cpp_TypeInfo;
// UnityEngine.Advertisements.Optional.ShowOptionsExtended
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_OptMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Optional.ShowOptionsExtended::.ctor()
MethodInfo ShowOptionsExtended__ctor_m736_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ShowOptionsExtended__ctor_m736/* method */
	, &ShowOptionsExtended_t152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.Optional.ShowOptionsExtended::get_gamerSid()
MethodInfo ShowOptionsExtended_get_gamerSid_m737_MethodInfo = 
{
	"get_gamerSid"/* name */
	, (methodPointerType)&ShowOptionsExtended_get_gamerSid_m737/* method */
	, &ShowOptionsExtended_t152_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 127/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ShowOptionsExtended_t152_ShowOptionsExtended_set_gamerSid_m738_ParameterInfos[] = 
{
	{"value", 0, 134218502, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.Optional.ShowOptionsExtended::set_gamerSid(System.String)
MethodInfo ShowOptionsExtended_set_gamerSid_m738_MethodInfo = 
{
	"set_gamerSid"/* name */
	, (methodPointerType)&ShowOptionsExtended_set_gamerSid_m738/* method */
	, &ShowOptionsExtended_t152_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ShowOptionsExtended_t152_ShowOptionsExtended_set_gamerSid_m738_ParameterInfos/* parameters */
	, 128/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ShowOptionsExtended_t152_MethodInfos[] =
{
	&ShowOptionsExtended__ctor_m736_MethodInfo,
	&ShowOptionsExtended_get_gamerSid_m737_MethodInfo,
	&ShowOptionsExtended_set_gamerSid_m738_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo ShowOptionsExtended_t152____U3CgamerSidU3Ek__BackingField_3_FieldInfo = 
{
	"<gamerSid>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &ShowOptionsExtended_t152_il2cpp_TypeInfo/* parent */
	, offsetof(ShowOptionsExtended_t152, ___U3CgamerSidU3Ek__BackingField_3)/* offset */
	, 126/* custom_attributes_cache */

};
static FieldInfo* ShowOptionsExtended_t152_FieldInfos[] =
{
	&ShowOptionsExtended_t152____U3CgamerSidU3Ek__BackingField_3_FieldInfo,
	NULL
};
extern MethodInfo ShowOptionsExtended_get_gamerSid_m737_MethodInfo;
extern MethodInfo ShowOptionsExtended_set_gamerSid_m738_MethodInfo;
static PropertyInfo ShowOptionsExtended_t152____gamerSid_PropertyInfo = 
{
	&ShowOptionsExtended_t152_il2cpp_TypeInfo/* parent */
	, "gamerSid"/* name */
	, &ShowOptionsExtended_get_gamerSid_m737_MethodInfo/* get */
	, &ShowOptionsExtended_set_gamerSid_m738_MethodInfo/* set */
	, 0/* attrs */
	, 129/* custom_attributes_cache */

};
static PropertyInfo* ShowOptionsExtended_t152_PropertyInfos[] =
{
	&ShowOptionsExtended_t152____gamerSid_PropertyInfo,
	NULL
};
static Il2CppMethodReference ShowOptionsExtended_t152_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ShowOptionsExtended_t152_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType ShowOptionsExtended_t152_0_0_0;
extern Il2CppType ShowOptionsExtended_t152_1_0_0;
struct ShowOptionsExtended_t152;
const Il2CppTypeDefinitionMetadata ShowOptionsExtended_t152_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ShowOptions_t153_0_0_0/* parent */
	, ShowOptionsExtended_t152_VTable/* vtableMethods */
	, ShowOptionsExtended_t152_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ShowOptionsExtended_t152_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShowOptionsExtended"/* name */
	, "UnityEngine.Advertisements.Optional"/* namespaze */
	, ShowOptionsExtended_t152_MethodInfos/* methods */
	, ShowOptionsExtended_t152_PropertyInfos/* properties */
	, ShowOptionsExtended_t152_FieldInfos/* fields */
	, NULL/* events */
	, &ShowOptionsExtended_t152_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShowOptionsExtended_t152_0_0_0/* byval_arg */
	, &ShowOptionsExtended_t152_1_0_0/* this_arg */
	, &ShowOptionsExtended_t152_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShowOptionsExtended_t152)/* instance_size */
	, sizeof (ShowOptionsExtended_t152)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.UnityAds
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni.h"
// Metadata Definition UnityEngine.Advertisements.UnityAds
extern TypeInfo UnityAds_t154_il2cpp_TypeInfo;
// UnityEngine.Advertisements.UnityAds
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_UniMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::.ctor()
MethodInfo UnityAds__ctor_m739_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAds__ctor_m739/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::.cctor()
MethodInfo UnityAds__cctor_m740_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UnityAds__cctor_m740/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAds_t154_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Advertisements.UnityAds UnityEngine.Advertisements.UnityAds::get_SharedInstance()
MethodInfo UnityAds_get_SharedInstance_m741_MethodInfo = 
{
	"get_SharedInstance"/* name */
	, (methodPointerType)&UnityAds_get_SharedInstance_m741/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &UnityAds_t154_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_Init_m742_ParameterInfos[] = 
{
	{"gameId", 0, 134218503, 0, &String_t_0_0_0},
	{"testModeEnabled", 1, 134218504, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::Init(System.String,System.Boolean)
MethodInfo UnityAds_Init_m742_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&UnityAds_Init_m742/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, UnityAds_t154_UnityAds_Init_m742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::Awake()
MethodInfo UnityAds_Awake_m743_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&UnityAds_Awake_m743/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::isSupported()
MethodInfo UnityAds_isSupported_m744_MethodInfo = 
{
	"isSupported"/* name */
	, (methodPointerType)&UnityAds_isSupported_m744/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::getSDKVersion()
MethodInfo UnityAds_getSDKVersion_m745_MethodInfo = 
{
	"getSDKVersion"/* name */
	, (methodPointerType)&UnityAds_getSDKVersion_m745/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_setLogLevel_m746_ParameterInfos[] = 
{
	{"logLevel", 0, 134218505, 0, &DebugLevel_t142_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
MethodInfo UnityAds_setLogLevel_m746_MethodInfo = 
{
	"setLogLevel"/* name */
	, (methodPointerType)&UnityAds_setLogLevel_m746/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UnityAds_t154_UnityAds_setLogLevel_m746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_canShowZone_m747_ParameterInfos[] = 
{
	{"zone", 0, 134218506, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::canShowZone(System.String)
MethodInfo UnityAds_canShowZone_m747_MethodInfo = 
{
	"canShowZone"/* name */
	, (methodPointerType)&UnityAds_canShowZone_m747/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_canShowZone_m747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::hasMultipleRewardItems()
MethodInfo UnityAds_hasMultipleRewardItems_m748_MethodInfo = 
{
	"hasMultipleRewardItems"/* name */
	, (methodPointerType)&UnityAds_hasMultipleRewardItems_m748/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType List_1_t43_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<System.String> UnityEngine.Advertisements.UnityAds::getRewardItemKeys()
MethodInfo UnityAds_getRewardItemKeys_m749_MethodInfo = 
{
	"getRewardItemKeys"/* name */
	, (methodPointerType)&UnityAds_getRewardItemKeys_m749/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t43_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::getDefaultRewardItemKey()
MethodInfo UnityAds_getDefaultRewardItemKey_m750_MethodInfo = 
{
	"getDefaultRewardItemKey"/* name */
	, (methodPointerType)&UnityAds_getDefaultRewardItemKey_m750/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::getCurrentRewardItemKey()
MethodInfo UnityAds_getCurrentRewardItemKey_m751_MethodInfo = 
{
	"getCurrentRewardItemKey"/* name */
	, (methodPointerType)&UnityAds_getCurrentRewardItemKey_m751/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_setRewardItemKey_m752_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218507, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::setRewardItemKey(System.String)
MethodInfo UnityAds_setRewardItemKey_m752_MethodInfo = 
{
	"setRewardItemKey"/* name */
	, (methodPointerType)&UnityAds_setRewardItemKey_m752/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_setRewardItemKey_m752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::setDefaultRewardItemAsRewardItem()
MethodInfo UnityAds_setDefaultRewardItemAsRewardItem_m753_MethodInfo = 
{
	"setDefaultRewardItemAsRewardItem"/* name */
	, (methodPointerType)&UnityAds_setDefaultRewardItemAsRewardItem_m753/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemNameKey()
MethodInfo UnityAds_getRewardItemNameKey_m754_MethodInfo = 
{
	"getRewardItemNameKey"/* name */
	, (methodPointerType)&UnityAds_getRewardItemNameKey_m754/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemPictureKey()
MethodInfo UnityAds_getRewardItemPictureKey_m755_MethodInfo = 
{
	"getRewardItemPictureKey"/* name */
	, (methodPointerType)&UnityAds_getRewardItemPictureKey_m755/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_getRewardItemDetailsWithKey_m756_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218508, 0, &String_t_0_0_0},
};
extern Il2CppType Dictionary_2_t165_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Advertisements.UnityAds::getRewardItemDetailsWithKey(System.String)
MethodInfo UnityAds_getRewardItemDetailsWithKey_m756_MethodInfo = 
{
	"getRewardItemDetailsWithKey"/* name */
	, (methodPointerType)&UnityAds_getRewardItemDetailsWithKey_m756/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Dictionary_2_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_getRewardItemDetailsWithKey_m756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_4112;
extern Il2CppType ShowOptions_t153_0_0_4112;
static ParameterInfo UnityAds_t154_UnityAds_Show_m757_ParameterInfos[] = 
{
	{"zoneId", 0, 134218509, 0, &String_t_0_0_4112},
	{"options", 1, 134218510, 0, &ShowOptions_t153_0_0_4112},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::Show(System.String,UnityEngine.Advertisements.ShowOptions)
MethodInfo UnityAds_Show_m757_MethodInfo = 
{
	"Show"/* name */
	, (methodPointerType)&UnityAds_Show_m757/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_Show_m757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_4112;
static ParameterInfo UnityAds_t154_UnityAds_show_m758_ParameterInfos[] = 
{
	{"zoneId", 0, 134218511, 0, &String_t_0_0_4112},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String)
MethodInfo UnityAds_show_m758_MethodInfo = 
{
	"show"/* name */
	, (methodPointerType)&UnityAds_show_m758/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_show_m758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_show_m759_ParameterInfos[] = 
{
	{"zoneId", 0, 134218512, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218513, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String)
MethodInfo UnityAds_show_m759_MethodInfo = 
{
	"show"/* name */
	, (methodPointerType)&UnityAds_show_m759/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_show_m759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Dictionary_2_t165_0_0_0;
extern Il2CppType Dictionary_2_t165_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_show_m760_ParameterInfos[] = 
{
	{"zoneId", 0, 134218514, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218515, 0, &String_t_0_0_0},
	{"options", 2, 134218516, 0, &Dictionary_2_t165_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
MethodInfo UnityAds_show_m760_MethodInfo = 
{
	"show"/* name */
	, (methodPointerType)&UnityAds_show_m760/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_show_m760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ShowResult_t160_0_0_0;
extern Il2CppType ShowResult_t160_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_deliverCallback_m761_ParameterInfos[] = 
{
	{"result", 0, 134218517, 0, &ShowResult_t160_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::deliverCallback(UnityEngine.Advertisements.ShowResult)
MethodInfo UnityAds_deliverCallback_m761_MethodInfo = 
{
	"deliverCallback"/* name */
	, (methodPointerType)&UnityAds_deliverCallback_m761/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UnityAds_t154_UnityAds_deliverCallback_m761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::hide()
MethodInfo UnityAds_hide_m762_MethodInfo = 
{
	"hide"/* name */
	, (methodPointerType)&UnityAds_hide_m762/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::fillRewardItemKeyData()
MethodInfo UnityAds_fillRewardItemKeyData_m763_MethodInfo = 
{
	"fillRewardItemKeyData"/* name */
	, (methodPointerType)&UnityAds_fillRewardItemKeyData_m763/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Dictionary_2_t165_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_parseOptionsDictionary_m764_ParameterInfos[] = 
{
	{"options", 0, 134218518, 0, &Dictionary_2_t165_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAds::parseOptionsDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>)
MethodInfo UnityAds_parseOptionsDictionary_m764_MethodInfo = 
{
	"parseOptionsDictionary"/* name */
	, (methodPointerType)&UnityAds_parseOptionsDictionary_m764/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_parseOptionsDictionary_m764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onHide()
MethodInfo UnityAds_onHide_m765_MethodInfo = 
{
	"onHide"/* name */
	, (methodPointerType)&UnityAds_onHide_m765/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onShow()
MethodInfo UnityAds_onShow_m766_MethodInfo = 
{
	"onShow"/* name */
	, (methodPointerType)&UnityAds_onShow_m766/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onVideoStarted()
MethodInfo UnityAds_onVideoStarted_m767_MethodInfo = 
{
	"onVideoStarted"/* name */
	, (methodPointerType)&UnityAds_onVideoStarted_m767/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAds_t154_UnityAds_onVideoCompleted_m768_ParameterInfos[] = 
{
	{"parameters", 0, 134218519, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onVideoCompleted(System.String)
MethodInfo UnityAds_onVideoCompleted_m768_MethodInfo = 
{
	"onVideoCompleted"/* name */
	, (methodPointerType)&UnityAds_onVideoCompleted_m768/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UnityAds_t154_UnityAds_onVideoCompleted_m768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onFetchCompleted()
MethodInfo UnityAds_onFetchCompleted_m769_MethodInfo = 
{
	"onFetchCompleted"/* name */
	, (methodPointerType)&UnityAds_onFetchCompleted_m769/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAds::onFetchFailed()
MethodInfo UnityAds_onFetchFailed_m770_MethodInfo = 
{
	"onFetchFailed"/* name */
	, (methodPointerType)&UnityAds_onFetchFailed_m770/* method */
	, &UnityAds_t154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAds_t154_MethodInfos[] =
{
	&UnityAds__ctor_m739_MethodInfo,
	&UnityAds__cctor_m740_MethodInfo,
	&UnityAds_get_SharedInstance_m741_MethodInfo,
	&UnityAds_Init_m742_MethodInfo,
	&UnityAds_Awake_m743_MethodInfo,
	&UnityAds_isSupported_m744_MethodInfo,
	&UnityAds_getSDKVersion_m745_MethodInfo,
	&UnityAds_setLogLevel_m746_MethodInfo,
	&UnityAds_canShowZone_m747_MethodInfo,
	&UnityAds_hasMultipleRewardItems_m748_MethodInfo,
	&UnityAds_getRewardItemKeys_m749_MethodInfo,
	&UnityAds_getDefaultRewardItemKey_m750_MethodInfo,
	&UnityAds_getCurrentRewardItemKey_m751_MethodInfo,
	&UnityAds_setRewardItemKey_m752_MethodInfo,
	&UnityAds_setDefaultRewardItemAsRewardItem_m753_MethodInfo,
	&UnityAds_getRewardItemNameKey_m754_MethodInfo,
	&UnityAds_getRewardItemPictureKey_m755_MethodInfo,
	&UnityAds_getRewardItemDetailsWithKey_m756_MethodInfo,
	&UnityAds_Show_m757_MethodInfo,
	&UnityAds_show_m758_MethodInfo,
	&UnityAds_show_m759_MethodInfo,
	&UnityAds_show_m760_MethodInfo,
	&UnityAds_deliverCallback_m761_MethodInfo,
	&UnityAds_hide_m762_MethodInfo,
	&UnityAds_fillRewardItemKeyData_m763_MethodInfo,
	&UnityAds_parseOptionsDictionary_m764_MethodInfo,
	&UnityAds_onHide_m765_MethodInfo,
	&UnityAds_onShow_m766_MethodInfo,
	&UnityAds_onVideoStarted_m767_MethodInfo,
	&UnityAds_onVideoCompleted_m768_MethodInfo,
	&UnityAds_onFetchCompleted_m769_MethodInfo,
	&UnityAds_onFetchFailed_m770_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo UnityAds_t154____isShowing_2_FieldInfo = 
{
	"isShowing"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___isShowing_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo UnityAds_t154____isInitialized_3_FieldInfo = 
{
	"isInitialized"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___isInitialized_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo UnityAds_t154____allowPrecache_4_FieldInfo = 
{
	"allowPrecache"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___allowPrecache_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo UnityAds_t154____initCalled_5_FieldInfo = 
{
	"initCalled"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___initCalled_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAds_t154_0_0_17;
FieldInfo UnityAds_t154____sharedInstance_6_FieldInfo = 
{
	"sharedInstance"/* name */
	, &UnityAds_t154_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___sharedInstance_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo UnityAds_t154_____rewardItemNameKey_7_FieldInfo = 
{
	"_rewardItemNameKey"/* name */
	, &String_t_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ____rewardItemNameKey_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo UnityAds_t154_____rewardItemPictureKey_8_FieldInfo = 
{
	"_rewardItemPictureKey"/* name */
	, &String_t_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ____rewardItemPictureKey_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo UnityAds_t154_____resultDelivered_9_FieldInfo = 
{
	"_resultDelivered"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ____resultDelivered_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Action_1_t155_0_0_17;
FieldInfo UnityAds_t154____resultCallback_10_FieldInfo = 
{
	"resultCallback"/* name */
	, &Action_1_t155_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ___resultCallback_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo UnityAds_t154_____versionString_11_FieldInfo = 
{
	"_versionString"/* name */
	, &String_t_0_0_17/* type */
	, &UnityAds_t154_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAds_t154_StaticFields, ____versionString_11)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityAds_t154_FieldInfos[] =
{
	&UnityAds_t154____isShowing_2_FieldInfo,
	&UnityAds_t154____isInitialized_3_FieldInfo,
	&UnityAds_t154____allowPrecache_4_FieldInfo,
	&UnityAds_t154____initCalled_5_FieldInfo,
	&UnityAds_t154____sharedInstance_6_FieldInfo,
	&UnityAds_t154_____rewardItemNameKey_7_FieldInfo,
	&UnityAds_t154_____rewardItemPictureKey_8_FieldInfo,
	&UnityAds_t154_____resultDelivered_9_FieldInfo,
	&UnityAds_t154____resultCallback_10_FieldInfo,
	&UnityAds_t154_____versionString_11_FieldInfo,
	NULL
};
extern MethodInfo UnityAds_get_SharedInstance_m741_MethodInfo;
static PropertyInfo UnityAds_t154____SharedInstance_PropertyInfo = 
{
	&UnityAds_t154_il2cpp_TypeInfo/* parent */
	, "SharedInstance"/* name */
	, &UnityAds_get_SharedInstance_m741_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* UnityAds_t154_PropertyInfos[] =
{
	&UnityAds_t154____SharedInstance_PropertyInfo,
	NULL
};
extern MethodInfo Object_Equals_m1199_MethodInfo;
extern MethodInfo Object_GetHashCode_m1200_MethodInfo;
extern MethodInfo Object_ToString_m1201_MethodInfo;
static Il2CppMethodReference UnityAds_t154_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool UnityAds_t154_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType UnityAds_t154_0_0_0;
extern Il2CppType UnityAds_t154_1_0_0;
extern Il2CppType MonoBehaviour_t26_0_0_0;
struct UnityAds_t154;
const Il2CppTypeDefinitionMetadata UnityAds_t154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, UnityAds_t154_VTable/* vtableMethods */
	, UnityAds_t154_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAds_t154_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAds"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, UnityAds_t154_MethodInfos/* methods */
	, UnityAds_t154_PropertyInfos/* properties */
	, UnityAds_t154_FieldInfos/* fields */
	, NULL/* events */
	, &UnityAds_t154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAds_t154_0_0_0/* byval_arg */
	, &UnityAds_t154_1_0_0/* this_arg */
	, &UnityAds_t154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAds_t154)/* instance_size */
	, sizeof (UnityAds_t154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UnityAds_t154_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.UnityAdsExternal
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_0.h"
// Metadata Definition UnityEngine.Advertisements.UnityAdsExternal
extern TypeInfo UnityAdsExternal_t157_il2cpp_TypeInfo;
// UnityEngine.Advertisements.UnityAdsExternal
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_0MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsExternal::.cctor()
MethodInfo UnityAdsExternal__cctor_m771_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UnityAdsExternal__cctor_m771/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAdsPlatform_t156_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Advertisements.UnityAdsPlatform UnityEngine.Advertisements.UnityAdsExternal::getImpl()
MethodInfo UnityAdsExternal_getImpl_m772_MethodInfo = 
{
	"getImpl"/* name */
	, (methodPointerType)&UnityAdsExternal_getImpl_m772/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &UnityAdsPlatform_t156_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_init_m773_ParameterInfos[] = 
{
	{"gameId", 0, 134218520, 0, &String_t_0_0_0},
	{"testModeEnabled", 1, 134218521, 0, &Boolean_t203_0_0_0},
	{"gameObjectName", 2, 134218522, 0, &String_t_0_0_0},
	{"unityVersion", 3, 134218523, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsExternal::init(System.String,System.Boolean,System.String,System.String)
MethodInfo UnityAdsExternal_init_m773_MethodInfo = 
{
	"init"/* name */
	, (methodPointerType)&UnityAdsExternal_init_m773/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_init_m773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_show_m774_ParameterInfos[] = 
{
	{"zoneId", 0, 134218524, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218525, 0, &String_t_0_0_0},
	{"options", 2, 134218526, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::show(System.String,System.String,System.String)
MethodInfo UnityAdsExternal_show_m774_MethodInfo = 
{
	"show"/* name */
	, (methodPointerType)&UnityAdsExternal_show_m774/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_show_m774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsExternal::hide()
MethodInfo UnityAdsExternal_hide_m775_MethodInfo = 
{
	"hide"/* name */
	, (methodPointerType)&UnityAdsExternal_hide_m775/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::isSupported()
MethodInfo UnityAdsExternal_isSupported_m776_MethodInfo = 
{
	"isSupported"/* name */
	, (methodPointerType)&UnityAdsExternal_isSupported_m776/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getSDKVersion()
MethodInfo UnityAdsExternal_getSDKVersion_m777_MethodInfo = 
{
	"getSDKVersion"/* name */
	, (methodPointerType)&UnityAdsExternal_getSDKVersion_m777/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_canShowZone_m778_ParameterInfos[] = 
{
	{"zone", 0, 134218527, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::canShowZone(System.String)
MethodInfo UnityAdsExternal_canShowZone_m778_MethodInfo = 
{
	"canShowZone"/* name */
	, (methodPointerType)&UnityAdsExternal_canShowZone_m778/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_canShowZone_m778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::hasMultipleRewardItems()
MethodInfo UnityAdsExternal_hasMultipleRewardItems_m779_MethodInfo = 
{
	"hasMultipleRewardItems"/* name */
	, (methodPointerType)&UnityAdsExternal_hasMultipleRewardItems_m779/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemKeys()
MethodInfo UnityAdsExternal_getRewardItemKeys_m780_MethodInfo = 
{
	"getRewardItemKeys"/* name */
	, (methodPointerType)&UnityAdsExternal_getRewardItemKeys_m780/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getDefaultRewardItemKey()
MethodInfo UnityAdsExternal_getDefaultRewardItemKey_m781_MethodInfo = 
{
	"getDefaultRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsExternal_getDefaultRewardItemKey_m781/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getCurrentRewardItemKey()
MethodInfo UnityAdsExternal_getCurrentRewardItemKey_m782_MethodInfo = 
{
	"getCurrentRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsExternal_getCurrentRewardItemKey_m782/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_setRewardItemKey_m783_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218528, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::setRewardItemKey(System.String)
MethodInfo UnityAdsExternal_setRewardItemKey_m783_MethodInfo = 
{
	"setRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsExternal_setRewardItemKey_m783/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_setRewardItemKey_m783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setDefaultRewardItemAsRewardItem()
MethodInfo UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784_MethodInfo = 
{
	"setDefaultRewardItemAsRewardItem"/* name */
	, (methodPointerType)&UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_getRewardItemDetailsWithKey_m785_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218529, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsWithKey(System.String)
MethodInfo UnityAdsExternal_getRewardItemDetailsWithKey_m785_MethodInfo = 
{
	"getRewardItemDetailsWithKey"/* name */
	, (methodPointerType)&UnityAdsExternal_getRewardItemDetailsWithKey_m785/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_getRewardItemDetailsWithKey_m785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsKeys()
MethodInfo UnityAdsExternal_getRewardItemDetailsKeys_m786_MethodInfo = 
{
	"getRewardItemDetailsKeys"/* name */
	, (methodPointerType)&UnityAdsExternal_getRewardItemDetailsKeys_m786/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
static ParameterInfo UnityAdsExternal_t157_UnityAdsExternal_setLogLevel_m787_ParameterInfos[] = 
{
	{"logLevel", 0, 134218530, 0, &DebugLevel_t142_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
MethodInfo UnityAdsExternal_setLogLevel_m787_MethodInfo = 
{
	"setLogLevel"/* name */
	, (methodPointerType)&UnityAdsExternal_setLogLevel_m787/* method */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UnityAdsExternal_t157_UnityAdsExternal_setLogLevel_m787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAdsExternal_t157_MethodInfos[] =
{
	&UnityAdsExternal__cctor_m771_MethodInfo,
	&UnityAdsExternal_getImpl_m772_MethodInfo,
	&UnityAdsExternal_init_m773_MethodInfo,
	&UnityAdsExternal_show_m774_MethodInfo,
	&UnityAdsExternal_hide_m775_MethodInfo,
	&UnityAdsExternal_isSupported_m776_MethodInfo,
	&UnityAdsExternal_getSDKVersion_m777_MethodInfo,
	&UnityAdsExternal_canShowZone_m778_MethodInfo,
	&UnityAdsExternal_hasMultipleRewardItems_m779_MethodInfo,
	&UnityAdsExternal_getRewardItemKeys_m780_MethodInfo,
	&UnityAdsExternal_getDefaultRewardItemKey_m781_MethodInfo,
	&UnityAdsExternal_getCurrentRewardItemKey_m782_MethodInfo,
	&UnityAdsExternal_setRewardItemKey_m783_MethodInfo,
	&UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784_MethodInfo,
	&UnityAdsExternal_getRewardItemDetailsWithKey_m785_MethodInfo,
	&UnityAdsExternal_getRewardItemDetailsKeys_m786_MethodInfo,
	&UnityAdsExternal_setLogLevel_m787_MethodInfo,
	NULL
};
extern Il2CppType UnityAdsPlatform_t156_0_0_17;
FieldInfo UnityAdsExternal_t157____impl_0_FieldInfo = 
{
	"impl"/* name */
	, &UnityAdsPlatform_t156_0_0_17/* type */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAdsExternal_t157_StaticFields, ___impl_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo UnityAdsExternal_t157____initialized_1_FieldInfo = 
{
	"initialized"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* parent */
	, offsetof(UnityAdsExternal_t157_StaticFields, ___initialized_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UnityAdsExternal_t157_FieldInfos[] =
{
	&UnityAdsExternal_t157____impl_0_FieldInfo,
	&UnityAdsExternal_t157____initialized_1_FieldInfo,
	NULL
};
static Il2CppMethodReference UnityAdsExternal_t157_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool UnityAdsExternal_t157_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType UnityAdsExternal_t157_0_0_0;
extern Il2CppType UnityAdsExternal_t157_1_0_0;
struct UnityAdsExternal_t157;
const Il2CppTypeDefinitionMetadata UnityAdsExternal_t157_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityAdsExternal_t157_VTable/* vtableMethods */
	, UnityAdsExternal_t157_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAdsExternal_t157_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAdsExternal"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, UnityAdsExternal_t157_MethodInfos/* methods */
	, NULL/* properties */
	, UnityAdsExternal_t157_FieldInfos/* fields */
	, NULL/* events */
	, &UnityAdsExternal_t157_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAdsExternal_t157_0_0_0/* byval_arg */
	, &UnityAdsExternal_t157_1_0_0/* this_arg */
	, &UnityAdsExternal_t157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAdsExternal_t157)/* instance_size */
	, sizeof (UnityAdsExternal_t157)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UnityAdsExternal_t157_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.UnityAdsIos
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_1.h"
// Metadata Definition UnityEngine.Advertisements.UnityAdsIos
extern TypeInfo UnityAdsIos_t158_il2cpp_TypeInfo;
// UnityEngine.Advertisements.UnityAdsIos
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_1MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIos::.ctor()
MethodInfo UnityAdsIos__ctor_m788_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAdsIos__ctor_m788/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_init_m789_ParameterInfos[] = 
{
	{"gameId", 0, 134218531, 0, &String_t_0_0_0},
	{"testModeEnabled", 1, 134218532, 0, &Boolean_t203_0_0_0},
	{"gameObjectName", 2, 134218533, 0, &String_t_0_0_0},
	{"unityVersion", 3, 134218534, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIos::init(System.String,System.Boolean,System.String,System.String)
MethodInfo UnityAdsIos_init_m789_MethodInfo = 
{
	"init"/* name */
	, (methodPointerType)&UnityAdsIos_init_m789/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_init_m789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_show_m790_ParameterInfos[] = 
{
	{"zoneId", 0, 134218535, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218536, 0, &String_t_0_0_0},
	{"options", 2, 134218537, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::show(System.String,System.String,System.String)
MethodInfo UnityAdsIos_show_m790_MethodInfo = 
{
	"show"/* name */
	, (methodPointerType)&UnityAdsIos_show_m790/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_show_m790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIos::hide()
MethodInfo UnityAdsIos_hide_m791_MethodInfo = 
{
	"hide"/* name */
	, (methodPointerType)&UnityAdsIos_hide_m791/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::isSupported()
MethodInfo UnityAdsIos_isSupported_m792_MethodInfo = 
{
	"isSupported"/* name */
	, (methodPointerType)&UnityAdsIos_isSupported_m792/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getSDKVersion()
MethodInfo UnityAdsIos_getSDKVersion_m793_MethodInfo = 
{
	"getSDKVersion"/* name */
	, (methodPointerType)&UnityAdsIos_getSDKVersion_m793/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_canShowZone_m794_ParameterInfos[] = 
{
	{"zone", 0, 134218538, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::canShowZone(System.String)
MethodInfo UnityAdsIos_canShowZone_m794_MethodInfo = 
{
	"canShowZone"/* name */
	, (methodPointerType)&UnityAdsIos_canShowZone_m794/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_canShowZone_m794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::hasMultipleRewardItems()
MethodInfo UnityAdsIos_hasMultipleRewardItems_m795_MethodInfo = 
{
	"hasMultipleRewardItems"/* name */
	, (methodPointerType)&UnityAdsIos_hasMultipleRewardItems_m795/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemKeys()
MethodInfo UnityAdsIos_getRewardItemKeys_m796_MethodInfo = 
{
	"getRewardItemKeys"/* name */
	, (methodPointerType)&UnityAdsIos_getRewardItemKeys_m796/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getDefaultRewardItemKey()
MethodInfo UnityAdsIos_getDefaultRewardItemKey_m797_MethodInfo = 
{
	"getDefaultRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIos_getDefaultRewardItemKey_m797/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getCurrentRewardItemKey()
MethodInfo UnityAdsIos_getCurrentRewardItemKey_m798_MethodInfo = 
{
	"getCurrentRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIos_getCurrentRewardItemKey_m798/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_setRewardItemKey_m799_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218539, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::setRewardItemKey(System.String)
MethodInfo UnityAdsIos_setRewardItemKey_m799_MethodInfo = 
{
	"setRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIos_setRewardItemKey_m799/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_setRewardItemKey_m799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIos::setDefaultRewardItemAsRewardItem()
MethodInfo UnityAdsIos_setDefaultRewardItemAsRewardItem_m800_MethodInfo = 
{
	"setDefaultRewardItemAsRewardItem"/* name */
	, (methodPointerType)&UnityAdsIos_setDefaultRewardItemAsRewardItem_m800/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_getRewardItemDetailsWithKey_m801_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218540, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsWithKey(System.String)
MethodInfo UnityAdsIos_getRewardItemDetailsWithKey_m801_MethodInfo = 
{
	"getRewardItemDetailsWithKey"/* name */
	, (methodPointerType)&UnityAdsIos_getRewardItemDetailsWithKey_m801/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_getRewardItemDetailsWithKey_m801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsKeys()
MethodInfo UnityAdsIos_getRewardItemDetailsKeys_m802_MethodInfo = 
{
	"getRewardItemDetailsKeys"/* name */
	, (methodPointerType)&UnityAdsIos_getRewardItemDetailsKeys_m802/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
static ParameterInfo UnityAdsIos_t158_UnityAdsIos_setLogLevel_m803_ParameterInfos[] = 
{
	{"logLevel", 0, 134218541, 0, &DebugLevel_t142_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIos::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
MethodInfo UnityAdsIos_setLogLevel_m803_MethodInfo = 
{
	"setLogLevel"/* name */
	, (methodPointerType)&UnityAdsIos_setLogLevel_m803/* method */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UnityAdsIos_t158_UnityAdsIos_setLogLevel_m803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAdsIos_t158_MethodInfos[] =
{
	&UnityAdsIos__ctor_m788_MethodInfo,
	&UnityAdsIos_init_m789_MethodInfo,
	&UnityAdsIos_show_m790_MethodInfo,
	&UnityAdsIos_hide_m791_MethodInfo,
	&UnityAdsIos_isSupported_m792_MethodInfo,
	&UnityAdsIos_getSDKVersion_m793_MethodInfo,
	&UnityAdsIos_canShowZone_m794_MethodInfo,
	&UnityAdsIos_hasMultipleRewardItems_m795_MethodInfo,
	&UnityAdsIos_getRewardItemKeys_m796_MethodInfo,
	&UnityAdsIos_getDefaultRewardItemKey_m797_MethodInfo,
	&UnityAdsIos_getCurrentRewardItemKey_m798_MethodInfo,
	&UnityAdsIos_setRewardItemKey_m799_MethodInfo,
	&UnityAdsIos_setDefaultRewardItemAsRewardItem_m800_MethodInfo,
	&UnityAdsIos_getRewardItemDetailsWithKey_m801_MethodInfo,
	&UnityAdsIos_getRewardItemDetailsKeys_m802_MethodInfo,
	&UnityAdsIos_setLogLevel_m803_MethodInfo,
	NULL
};
extern MethodInfo UnityAdsIos_init_m789_MethodInfo;
extern MethodInfo UnityAdsIos_show_m790_MethodInfo;
extern MethodInfo UnityAdsIos_hide_m791_MethodInfo;
extern MethodInfo UnityAdsIos_isSupported_m792_MethodInfo;
extern MethodInfo UnityAdsIos_getSDKVersion_m793_MethodInfo;
extern MethodInfo UnityAdsIos_canShowZone_m794_MethodInfo;
extern MethodInfo UnityAdsIos_hasMultipleRewardItems_m795_MethodInfo;
extern MethodInfo UnityAdsIos_getRewardItemKeys_m796_MethodInfo;
extern MethodInfo UnityAdsIos_getDefaultRewardItemKey_m797_MethodInfo;
extern MethodInfo UnityAdsIos_getCurrentRewardItemKey_m798_MethodInfo;
extern MethodInfo UnityAdsIos_setRewardItemKey_m799_MethodInfo;
extern MethodInfo UnityAdsIos_setDefaultRewardItemAsRewardItem_m800_MethodInfo;
extern MethodInfo UnityAdsIos_getRewardItemDetailsWithKey_m801_MethodInfo;
extern MethodInfo UnityAdsIos_getRewardItemDetailsKeys_m802_MethodInfo;
extern MethodInfo UnityAdsIos_setLogLevel_m803_MethodInfo;
static Il2CppMethodReference UnityAdsIos_t158_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&UnityAdsIos_init_m789_MethodInfo,
	&UnityAdsIos_show_m790_MethodInfo,
	&UnityAdsIos_hide_m791_MethodInfo,
	&UnityAdsIos_isSupported_m792_MethodInfo,
	&UnityAdsIos_getSDKVersion_m793_MethodInfo,
	&UnityAdsIos_canShowZone_m794_MethodInfo,
	&UnityAdsIos_hasMultipleRewardItems_m795_MethodInfo,
	&UnityAdsIos_getRewardItemKeys_m796_MethodInfo,
	&UnityAdsIos_getDefaultRewardItemKey_m797_MethodInfo,
	&UnityAdsIos_getCurrentRewardItemKey_m798_MethodInfo,
	&UnityAdsIos_setRewardItemKey_m799_MethodInfo,
	&UnityAdsIos_setDefaultRewardItemAsRewardItem_m800_MethodInfo,
	&UnityAdsIos_getRewardItemDetailsWithKey_m801_MethodInfo,
	&UnityAdsIos_getRewardItemDetailsKeys_m802_MethodInfo,
	&UnityAdsIos_setLogLevel_m803_MethodInfo,
};
static bool UnityAdsIos_t158_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType UnityAdsIos_t158_0_0_0;
extern Il2CppType UnityAdsIos_t158_1_0_0;
extern Il2CppType UnityAdsPlatform_t156_0_0_0;
struct UnityAdsIos_t158;
const Il2CppTypeDefinitionMetadata UnityAdsIos_t158_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UnityAdsPlatform_t156_0_0_0/* parent */
	, UnityAdsIos_t158_VTable/* vtableMethods */
	, UnityAdsIos_t158_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAdsIos_t158_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAdsIos"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, UnityAdsIos_t158_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAdsIos_t158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAdsIos_t158_0_0_0/* byval_arg */
	, &UnityAdsIos_t158_1_0_0/* this_arg */
	, &UnityAdsIos_t158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAdsIos_t158)/* instance_size */
	, sizeof (UnityAdsIos_t158)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.UnityAdsIosBridge
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_3.h"
// Metadata Definition UnityEngine.Advertisements.UnityAdsIosBridge
extern TypeInfo UnityAdsIosBridge_t159_il2cpp_TypeInfo;
// UnityEngine.Advertisements.UnityAdsIosBridge
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_3MethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsInit_m804_ParameterInfos[] = 
{
	{"gameId", 0, 134218542, 0, &String_t_0_0_0},
	{"testModeEnabled", 1, 134218543, 0, &Boolean_t203_0_0_0},
	{"debugModeEnabled", 2, 134218544, 0, &Boolean_t203_0_0_0},
	{"gameObjectName", 3, 134218545, 0, &String_t_0_0_0},
	{"unityVersion", 4, 134218546, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsInit(System.String,System.Boolean,System.Boolean,System.String,System.String)
MethodInfo UnityAdsIosBridge_UnityAdsInit_m804_MethodInfo = 
{
	"UnityAdsInit"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsInit_m804/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Object_t_Object_t/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsInit_m804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsShow_m805_ParameterInfos[] = 
{
	{"zoneId", 0, 134218547, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218548, 0, &String_t_0_0_0},
	{"options", 2, 134218549, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsShow(System.String,System.String,System.String)
MethodInfo UnityAdsIosBridge_UnityAdsShow_m805_MethodInfo = 
{
	"UnityAdsShow"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsShow_m805/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsShow_m805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHide()
MethodInfo UnityAdsIosBridge_UnityAdsHide_m806_MethodInfo = 
{
	"UnityAdsHide"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsHide_m806/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsIsSupported()
MethodInfo UnityAdsIosBridge_UnityAdsIsSupported_m807_MethodInfo = 
{
	"UnityAdsIsSupported"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsIsSupported_m807/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetSDKVersion()
MethodInfo UnityAdsIosBridge_UnityAdsGetSDKVersion_m808_MethodInfo = 
{
	"UnityAdsGetSDKVersion"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetSDKVersion_m808/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShow()
MethodInfo UnityAdsIosBridge_UnityAdsCanShow_m809_MethodInfo = 
{
	"UnityAdsCanShow"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsCanShow_m809/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsCanShowZone_m810_ParameterInfos[] = 
{
	{"zone", 0, 134218550, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShowZone(System.String)
MethodInfo UnityAdsIosBridge_UnityAdsCanShowZone_m810_MethodInfo = 
{
	"UnityAdsCanShowZone"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsCanShowZone_m810/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsCanShowZone_m810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHasMultipleRewardItems()
MethodInfo UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811_MethodInfo = 
{
	"UnityAdsHasMultipleRewardItems"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemKeys()
MethodInfo UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812_MethodInfo = 
{
	"UnityAdsGetRewardItemKeys"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetDefaultRewardItemKey()
MethodInfo UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813_MethodInfo = 
{
	"UnityAdsGetDefaultRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetCurrentRewardItemKey()
MethodInfo UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814_MethodInfo = 
{
	"UnityAdsGetCurrentRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218551, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetRewardItemKey(System.String)
MethodInfo UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815_MethodInfo = 
{
	"UnityAdsSetRewardItemKey"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDefaultRewardItemAsRewardItem()
MethodInfo UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816_MethodInfo = 
{
	"UnityAdsSetDefaultRewardItemAsRewardItem"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218552, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsWithKey(System.String)
MethodInfo UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817_MethodInfo = 
{
	"UnityAdsGetRewardItemDetailsWithKey"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsKeys()
MethodInfo UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818_MethodInfo = 
{
	"UnityAdsGetRewardItemDetailsKeys"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsSetDebugMode_m819_ParameterInfos[] = 
{
	{"debugMode", 0, 134218553, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDebugMode(System.Boolean)
MethodInfo UnityAdsIosBridge_UnityAdsSetDebugMode_m819_MethodInfo = 
{
	"UnityAdsSetDebugMode"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsSetDebugMode_m819/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, UnityAdsIosBridge_t159_UnityAdsIosBridge_UnityAdsSetDebugMode_m819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsEnableUnityDeveloperInternalTestMode()
MethodInfo UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820_MethodInfo = 
{
	"UnityAdsEnableUnityDeveloperInternalTestMode"/* name */
	, (methodPointerType)&UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820/* method */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8342/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAdsIosBridge_t159_MethodInfos[] =
{
	&UnityAdsIosBridge_UnityAdsInit_m804_MethodInfo,
	&UnityAdsIosBridge_UnityAdsShow_m805_MethodInfo,
	&UnityAdsIosBridge_UnityAdsHide_m806_MethodInfo,
	&UnityAdsIosBridge_UnityAdsIsSupported_m807_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetSDKVersion_m808_MethodInfo,
	&UnityAdsIosBridge_UnityAdsCanShow_m809_MethodInfo,
	&UnityAdsIosBridge_UnityAdsCanShowZone_m810_MethodInfo,
	&UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814_MethodInfo,
	&UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815_MethodInfo,
	&UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817_MethodInfo,
	&UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818_MethodInfo,
	&UnityAdsIosBridge_UnityAdsSetDebugMode_m819_MethodInfo,
	&UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820_MethodInfo,
	NULL
};
static Il2CppMethodReference UnityAdsIosBridge_t159_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool UnityAdsIosBridge_t159_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType UnityAdsIosBridge_t159_0_0_0;
extern Il2CppType UnityAdsIosBridge_t159_1_0_0;
struct UnityAdsIosBridge_t159;
const Il2CppTypeDefinitionMetadata UnityAdsIosBridge_t159_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityAdsIosBridge_t159_VTable/* vtableMethods */
	, UnityAdsIosBridge_t159_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAdsIosBridge_t159_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAdsIosBridge"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, UnityAdsIosBridge_t159_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAdsIosBridge_t159_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAdsIosBridge_t159_0_0_0/* byval_arg */
	, &UnityAdsIosBridge_t159_1_0_0/* this_arg */
	, &UnityAdsIosBridge_t159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAdsIosBridge_t159)/* instance_size */
	, sizeof (UnityAdsIosBridge_t159)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.UnityAdsPlatform
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_2.h"
// Metadata Definition UnityEngine.Advertisements.UnityAdsPlatform
extern TypeInfo UnityAdsPlatform_t156_il2cpp_TypeInfo;
// UnityEngine.Advertisements.UnityAdsPlatform
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_2MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::.ctor()
MethodInfo UnityAdsPlatform__ctor_m821_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAdsPlatform__ctor_m821/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_init_m1161_ParameterInfos[] = 
{
	{"gameId", 0, 134218554, 0, &String_t_0_0_0},
	{"testModeEnabled", 1, 134218555, 0, &Boolean_t203_0_0_0},
	{"gameObjectName", 2, 134218556, 0, &String_t_0_0_0},
	{"unityVersion", 3, 134218557, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::init(System.String,System.Boolean,System.String,System.String)
MethodInfo UnityAdsPlatform_init_m1161_MethodInfo = 
{
	"init"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_init_m1161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_show_m1162_ParameterInfos[] = 
{
	{"zoneId", 0, 134218558, 0, &String_t_0_0_0},
	{"rewardItemKey", 1, 134218559, 0, &String_t_0_0_0},
	{"options", 2, 134218560, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::show(System.String,System.String,System.String)
MethodInfo UnityAdsPlatform_show_m1162_MethodInfo = 
{
	"show"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_show_m1162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::hide()
MethodInfo UnityAdsPlatform_hide_m1163_MethodInfo = 
{
	"hide"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::isSupported()
MethodInfo UnityAdsPlatform_isSupported_m1164_MethodInfo = 
{
	"isSupported"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getSDKVersion()
MethodInfo UnityAdsPlatform_getSDKVersion_m1165_MethodInfo = 
{
	"getSDKVersion"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_canShowZone_m1166_ParameterInfos[] = 
{
	{"zone", 0, 134218561, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::canShowZone(System.String)
MethodInfo UnityAdsPlatform_canShowZone_m1166_MethodInfo = 
{
	"canShowZone"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_canShowZone_m1166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::hasMultipleRewardItems()
MethodInfo UnityAdsPlatform_hasMultipleRewardItems_m1167_MethodInfo = 
{
	"hasMultipleRewardItems"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemKeys()
MethodInfo UnityAdsPlatform_getRewardItemKeys_m1168_MethodInfo = 
{
	"getRewardItemKeys"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getDefaultRewardItemKey()
MethodInfo UnityAdsPlatform_getDefaultRewardItemKey_m1169_MethodInfo = 
{
	"getDefaultRewardItemKey"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getCurrentRewardItemKey()
MethodInfo UnityAdsPlatform_getCurrentRewardItemKey_m1170_MethodInfo = 
{
	"getCurrentRewardItemKey"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_setRewardItemKey_m1171_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218562, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::setRewardItemKey(System.String)
MethodInfo UnityAdsPlatform_setRewardItemKey_m1171_MethodInfo = 
{
	"setRewardItemKey"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_setRewardItemKey_m1171_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setDefaultRewardItemAsRewardItem()
MethodInfo UnityAdsPlatform_setDefaultRewardItemAsRewardItem_m1172_MethodInfo = 
{
	"setDefaultRewardItemAsRewardItem"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_getRewardItemDetailsWithKey_m1173_ParameterInfos[] = 
{
	{"rewardItemKey", 0, 134218563, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsWithKey(System.String)
MethodInfo UnityAdsPlatform_getRewardItemDetailsWithKey_m1173_MethodInfo = 
{
	"getRewardItemDetailsWithKey"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_getRewardItemDetailsWithKey_m1173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsKeys()
MethodInfo UnityAdsPlatform_getRewardItemDetailsKeys_m1174_MethodInfo = 
{
	"getRewardItemDetailsKeys"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DebugLevel_t142_0_0_0;
static ParameterInfo UnityAdsPlatform_t156_UnityAdsPlatform_setLogLevel_m1175_ParameterInfos[] = 
{
	{"logLevel", 0, 134218564, 0, &DebugLevel_t142_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
MethodInfo UnityAdsPlatform_setLogLevel_m1175_MethodInfo = 
{
	"setLogLevel"/* name */
	, NULL/* method */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, UnityAdsPlatform_t156_UnityAdsPlatform_setLogLevel_m1175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAdsPlatform_t156_MethodInfos[] =
{
	&UnityAdsPlatform__ctor_m821_MethodInfo,
	&UnityAdsPlatform_init_m1161_MethodInfo,
	&UnityAdsPlatform_show_m1162_MethodInfo,
	&UnityAdsPlatform_hide_m1163_MethodInfo,
	&UnityAdsPlatform_isSupported_m1164_MethodInfo,
	&UnityAdsPlatform_getSDKVersion_m1165_MethodInfo,
	&UnityAdsPlatform_canShowZone_m1166_MethodInfo,
	&UnityAdsPlatform_hasMultipleRewardItems_m1167_MethodInfo,
	&UnityAdsPlatform_getRewardItemKeys_m1168_MethodInfo,
	&UnityAdsPlatform_getDefaultRewardItemKey_m1169_MethodInfo,
	&UnityAdsPlatform_getCurrentRewardItemKey_m1170_MethodInfo,
	&UnityAdsPlatform_setRewardItemKey_m1171_MethodInfo,
	&UnityAdsPlatform_setDefaultRewardItemAsRewardItem_m1172_MethodInfo,
	&UnityAdsPlatform_getRewardItemDetailsWithKey_m1173_MethodInfo,
	&UnityAdsPlatform_getRewardItemDetailsKeys_m1174_MethodInfo,
	&UnityAdsPlatform_setLogLevel_m1175_MethodInfo,
	NULL
};
static Il2CppMethodReference UnityAdsPlatform_t156_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool UnityAdsPlatform_t156_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType UnityAdsPlatform_t156_1_0_0;
struct UnityAdsPlatform_t156;
const Il2CppTypeDefinitionMetadata UnityAdsPlatform_t156_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityAdsPlatform_t156_VTable/* vtableMethods */
	, UnityAdsPlatform_t156_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UnityAdsPlatform_t156_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAdsPlatform"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, UnityAdsPlatform_t156_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UnityAdsPlatform_t156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAdsPlatform_t156_0_0_0/* byval_arg */
	, &UnityAdsPlatform_t156_1_0_0/* this_arg */
	, &UnityAdsPlatform_t156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAdsPlatform_t156)/* instance_size */
	, sizeof (UnityAdsPlatform_t156)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.ShowOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho.h"
// Metadata Definition UnityEngine.Advertisements.ShowOptions
extern TypeInfo ShowOptions_t153_il2cpp_TypeInfo;
// UnityEngine.Advertisements.ShowOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_ShoMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
MethodInfo ShowOptions__ctor_m822_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ShowOptions__ctor_m822/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Advertisements.ShowOptions::get_pause()
MethodInfo ShowOptions_get_pause_m823_MethodInfo = 
{
	"get_pause"/* name */
	, (methodPointerType)&ShowOptions_get_pause_m823/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 133/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ShowOptions_t153_ShowOptions_set_pause_m824_ParameterInfos[] = 
{
	{"value", 0, 134218565, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.ShowOptions::set_pause(System.Boolean)
MethodInfo ShowOptions_set_pause_m824_MethodInfo = 
{
	"set_pause"/* name */
	, (methodPointerType)&ShowOptions_set_pause_m824/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ShowOptions_t153_ShowOptions_set_pause_m824_ParameterInfos/* parameters */
	, 134/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_1_t155_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
MethodInfo ShowOptions_get_resultCallback_m825_MethodInfo = 
{
	"get_resultCallback"/* name */
	, (methodPointerType)&ShowOptions_get_resultCallback_m825/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Action_1_t155_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 135/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Action_1_t155_0_0_0;
extern Il2CppType Action_1_t155_0_0_0;
static ParameterInfo ShowOptions_t153_ShowOptions_set_resultCallback_m826_ParameterInfos[] = 
{
	{"value", 0, 134218566, 0, &Action_1_t155_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
MethodInfo ShowOptions_set_resultCallback_m826_MethodInfo = 
{
	"set_resultCallback"/* name */
	, (methodPointerType)&ShowOptions_set_resultCallback_m826/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ShowOptions_t153_ShowOptions_set_resultCallback_m826_ParameterInfos/* parameters */
	, 136/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
MethodInfo ShowOptions_get_gamerSid_m827_MethodInfo = 
{
	"get_gamerSid"/* name */
	, (methodPointerType)&ShowOptions_get_gamerSid_m827/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 137/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ShowOptions_t153_ShowOptions_set_gamerSid_m828_ParameterInfos[] = 
{
	{"value", 0, 134218567, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
MethodInfo ShowOptions_set_gamerSid_m828_MethodInfo = 
{
	"set_gamerSid"/* name */
	, (methodPointerType)&ShowOptions_set_gamerSid_m828/* method */
	, &ShowOptions_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ShowOptions_t153_ShowOptions_set_gamerSid_m828_ParameterInfos/* parameters */
	, 138/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ShowOptions_t153_MethodInfos[] =
{
	&ShowOptions__ctor_m822_MethodInfo,
	&ShowOptions_get_pause_m823_MethodInfo,
	&ShowOptions_set_pause_m824_MethodInfo,
	&ShowOptions_get_resultCallback_m825_MethodInfo,
	&ShowOptions_set_resultCallback_m826_MethodInfo,
	&ShowOptions_get_gamerSid_m827_MethodInfo,
	&ShowOptions_set_gamerSid_m828_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ShowOptions_t153____U3CpauseU3Ek__BackingField_0_FieldInfo = 
{
	"<pause>k__BackingField"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, offsetof(ShowOptions_t153, ___U3CpauseU3Ek__BackingField_0)/* offset */
	, 130/* custom_attributes_cache */

};
extern Il2CppType Action_1_t155_0_0_1;
FieldInfo ShowOptions_t153____U3CresultCallbackU3Ek__BackingField_1_FieldInfo = 
{
	"<resultCallback>k__BackingField"/* name */
	, &Action_1_t155_0_0_1/* type */
	, &ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, offsetof(ShowOptions_t153, ___U3CresultCallbackU3Ek__BackingField_1)/* offset */
	, 131/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ShowOptions_t153____U3CgamerSidU3Ek__BackingField_2_FieldInfo = 
{
	"<gamerSid>k__BackingField"/* name */
	, &String_t_0_0_1/* type */
	, &ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, offsetof(ShowOptions_t153, ___U3CgamerSidU3Ek__BackingField_2)/* offset */
	, 132/* custom_attributes_cache */

};
static FieldInfo* ShowOptions_t153_FieldInfos[] =
{
	&ShowOptions_t153____U3CpauseU3Ek__BackingField_0_FieldInfo,
	&ShowOptions_t153____U3CresultCallbackU3Ek__BackingField_1_FieldInfo,
	&ShowOptions_t153____U3CgamerSidU3Ek__BackingField_2_FieldInfo,
	NULL
};
extern MethodInfo ShowOptions_get_pause_m823_MethodInfo;
extern MethodInfo ShowOptions_set_pause_m824_MethodInfo;
static PropertyInfo ShowOptions_t153____pause_PropertyInfo = 
{
	&ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, "pause"/* name */
	, &ShowOptions_get_pause_m823_MethodInfo/* get */
	, &ShowOptions_set_pause_m824_MethodInfo/* set */
	, 0/* attrs */
	, 139/* custom_attributes_cache */

};
extern MethodInfo ShowOptions_get_resultCallback_m825_MethodInfo;
extern MethodInfo ShowOptions_set_resultCallback_m826_MethodInfo;
static PropertyInfo ShowOptions_t153____resultCallback_PropertyInfo = 
{
	&ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, "resultCallback"/* name */
	, &ShowOptions_get_resultCallback_m825_MethodInfo/* get */
	, &ShowOptions_set_resultCallback_m826_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ShowOptions_get_gamerSid_m827_MethodInfo;
extern MethodInfo ShowOptions_set_gamerSid_m828_MethodInfo;
static PropertyInfo ShowOptions_t153____gamerSid_PropertyInfo = 
{
	&ShowOptions_t153_il2cpp_TypeInfo/* parent */
	, "gamerSid"/* name */
	, &ShowOptions_get_gamerSid_m827_MethodInfo/* get */
	, &ShowOptions_set_gamerSid_m828_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ShowOptions_t153_PropertyInfos[] =
{
	&ShowOptions_t153____pause_PropertyInfo,
	&ShowOptions_t153____resultCallback_PropertyInfo,
	&ShowOptions_t153____gamerSid_PropertyInfo,
	NULL
};
static Il2CppMethodReference ShowOptions_t153_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ShowOptions_t153_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType ShowOptions_t153_1_0_0;
struct ShowOptions_t153;
const Il2CppTypeDefinitionMetadata ShowOptions_t153_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShowOptions_t153_VTable/* vtableMethods */
	, ShowOptions_t153_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ShowOptions_t153_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShowOptions"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, ShowOptions_t153_MethodInfos/* methods */
	, ShowOptions_t153_PropertyInfos/* properties */
	, ShowOptions_t153_FieldInfos/* fields */
	, NULL/* events */
	, &ShowOptions_t153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShowOptions_t153_0_0_0/* byval_arg */
	, &ShowOptions_t153_1_0_0/* this_arg */
	, &ShowOptions_t153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShowOptions_t153)/* instance_size */
	, sizeof (ShowOptions_t153)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0.h"
// Metadata Definition UnityEngine.Advertisements.ShowResult
extern TypeInfo ShowResult_t160_il2cpp_TypeInfo;
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0MethodDeclarations.h"
static MethodInfo* ShowResult_t160_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo ShowResult_t160____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &ShowResult_t160_il2cpp_TypeInfo/* parent */
	, offsetof(ShowResult_t160, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ShowResult_t160_0_0_32854;
FieldInfo ShowResult_t160____Failed_2_FieldInfo = 
{
	"Failed"/* name */
	, &ShowResult_t160_0_0_32854/* type */
	, &ShowResult_t160_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ShowResult_t160_0_0_32854;
FieldInfo ShowResult_t160____Skipped_3_FieldInfo = 
{
	"Skipped"/* name */
	, &ShowResult_t160_0_0_32854/* type */
	, &ShowResult_t160_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ShowResult_t160_0_0_32854;
FieldInfo ShowResult_t160____Finished_4_FieldInfo = 
{
	"Finished"/* name */
	, &ShowResult_t160_0_0_32854/* type */
	, &ShowResult_t160_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ShowResult_t160_FieldInfos[] =
{
	&ShowResult_t160____value___1_FieldInfo,
	&ShowResult_t160____Failed_2_FieldInfo,
	&ShowResult_t160____Skipped_3_FieldInfo,
	&ShowResult_t160____Finished_4_FieldInfo,
	NULL
};
static const int32_t ShowResult_t160____Failed_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry ShowResult_t160____Failed_2_DefaultValue = 
{
	&ShowResult_t160____Failed_2_FieldInfo/* field */
	, { (char*)&ShowResult_t160____Failed_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ShowResult_t160____Skipped_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ShowResult_t160____Skipped_3_DefaultValue = 
{
	&ShowResult_t160____Skipped_3_FieldInfo/* field */
	, { (char*)&ShowResult_t160____Skipped_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t ShowResult_t160____Finished_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ShowResult_t160____Finished_4_DefaultValue = 
{
	&ShowResult_t160____Finished_4_FieldInfo/* field */
	, { (char*)&ShowResult_t160____Finished_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ShowResult_t160_FieldDefaultValues[] = 
{
	&ShowResult_t160____Failed_2_DefaultValue,
	&ShowResult_t160____Skipped_3_DefaultValue,
	&ShowResult_t160____Finished_4_DefaultValue,
	NULL
};
static Il2CppMethodReference ShowResult_t160_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ShowResult_t160_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ShowResult_t160_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType ShowResult_t160_1_0_0;
const Il2CppTypeDefinitionMetadata ShowResult_t160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ShowResult_t160_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ShowResult_t160_VTable/* vtableMethods */
	, ShowResult_t160_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ShowResult_t160_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShowResult"/* name */
	, "UnityEngine.Advertisements"/* namespaze */
	, ShowResult_t160_MethodInfos/* methods */
	, NULL/* properties */
	, ShowResult_t160_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShowResult_t160_0_0_0/* byval_arg */
	, &ShowResult_t160_1_0_0/* this_arg */
	, &ShowResult_t160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ShowResult_t160_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShowResult_t160)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ShowResult_t160)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$8
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$8
extern TypeInfo U24ArrayTypeU248_t161_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$8
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetailMethodDeclarations.h"
static MethodInfo* U24ArrayTypeU248_t161_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m1319_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1320_MethodInfo;
extern MethodInfo ValueType_ToString_m1321_MethodInfo;
static Il2CppMethodReference U24ArrayTypeU248_t161_VTable[] =
{
	&ValueType_Equals_m1319_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ValueType_GetHashCode_m1320_MethodInfo,
	&ValueType_ToString_m1321_MethodInfo,
};
static bool U24ArrayTypeU248_t161_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType U24ArrayTypeU248_t161_0_0_0;
extern Il2CppType U24ArrayTypeU248_t161_1_0_0;
extern Il2CppType ValueType_t329_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t162_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU248_t161_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t162_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t329_0_0_0/* parent */
	, U24ArrayTypeU248_t161_VTable/* vtableMethods */
	, U24ArrayTypeU248_t161_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U24ArrayTypeU248_t161_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$8"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU248_t161_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U24ArrayTypeU248_t161_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU248_t161_0_0_0/* byval_arg */
	, &U24ArrayTypeU248_t161_1_0_0/* this_arg */
	, &U24ArrayTypeU248_t161_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU248_t161_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU248_t161_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU248_t161_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU248_t161)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU248_t161)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU248_t161_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail_0.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail_0MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void <PrivateImplementationDetails>::.ctor()
MethodInfo U3CPrivateImplementationDetailsU3E__ctor_m829_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CPrivateImplementationDetailsU3E__ctor_m829/* method */
	, &U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CPrivateImplementationDetailsU3E_t162_MethodInfos[] =
{
	&U3CPrivateImplementationDetailsU3E__ctor_m829_MethodInfo,
	NULL
};
extern Il2CppType U24ArrayTypeU248_t161_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_FieldInfo = 
{
	"$$field-0"/* name */
	, &U24ArrayTypeU248_t161_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t162_StaticFields, ___U24U24fieldU2D0_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3E_t162_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_DefaultValueData[] = { 0x20, 0x0, 0xD, 0x0, 0xA, 0x0, 0x9, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_DefaultValueData, &U24ArrayTypeU248_t161_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3E_t162_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3E_t162____U24U24fieldU2D0_0_DefaultValue,
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U24ArrayTypeU248_t161_0_0_0,
};
static Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t162_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t162_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharpU2Dfirstpass_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t162_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t162;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t162_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t162_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharpU2Dfirstpass_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t162_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3E_t162_FieldInfos/* fields */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 140/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t162_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t162_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3E_t162_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t162)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t162_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
