﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.LeaderboardManager
struct LeaderboardManager_t434;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback
struct FetchAllCallback_t428;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback
struct FetchScoreSummaryCallback_t430;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback
struct ShowAllUICallback_t432;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback
struct FetchScorePageCallback_t429;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback
struct FetchAllScoreSummariesCallback_t431;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
struct ShowUICallback_t433;
// GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback
struct FetchCallback_t427;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllCallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_FetchAll_m1764 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t428 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummary(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScoreSummaryCallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_FetchScoreSummary_m1765 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, int32_t ___time_span, int32_t ___collection, FetchScoreSummaryCallback_t430 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ScorePageToken(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart,GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan,GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_ScorePageToken_m1766 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, int32_t ___start, int32_t ___time_span, int32_t ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowAllUICallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_ShowAllUI_m1767 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ShowAllUICallback_t432 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePage(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.IntPtr,System.UInt32,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchScorePageCallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_FetchScorePage_m1768 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, IntPtr_t ___token, uint32_t ___max_results, FetchScorePageCallback_t429 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummaries(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchAllScoreSummariesCallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllScoreSummaries_m1769 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, FetchAllScoreSummariesCallback_t431 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_ShowUI(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_ShowUI_m1770 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, ShowUICallback_t433 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.LeaderboardManager/FetchCallback,System.IntPtr)
extern "C" void LeaderboardManager_LeaderboardManager_Fetch_m1771 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___leaderboard_id, FetchCallback_t427 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_SubmitScore(System.Runtime.InteropServices.HandleRef,System.String,System.UInt64,System.String)
extern "C" void LeaderboardManager_LeaderboardManager_SubmitScore_m1772 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___leaderboard_id, uint64_t ___score, String_t* ___metadata, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void LeaderboardManager_LeaderboardManager_FetchResponse_Dispose_m1773 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchResponse_GetStatus_m1774 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchResponse_GetData_m1775 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllResponse_Dispose_m1776 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetStatus_m1777 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_Length_m1778 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllResponse_GetData_GetElement_m1779 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void LeaderboardManager_LeaderboardManager_FetchScorePageResponse_Dispose_m1780 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetStatus_m1781 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScorePageResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchScorePageResponse_GetData_m1782 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_Dispose_m1783 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetStatus_m1784 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchScoreSummaryResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchScoreSummaryResponse_GetData_m1785 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_Dispose_m1786 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetStatus_m1787 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_Length_m1788 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.LeaderboardManager::LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t LeaderboardManager_LeaderboardManager_FetchAllScoreSummariesResponse_GetData_GetElement_m1789 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
