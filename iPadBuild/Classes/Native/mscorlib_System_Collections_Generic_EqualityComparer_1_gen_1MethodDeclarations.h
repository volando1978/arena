﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct EqualityComparer_1_t3506;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt16>::.ctor()
extern "C" void EqualityComparer_1__ctor_m16620_gshared (EqualityComparer_1_t3506 * __this, MethodInfo* method);
#define EqualityComparer_1__ctor_m16620(__this, method) (( void (*) (EqualityComparer_1_t3506 *, MethodInfo*))EqualityComparer_1__ctor_m16620_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt16>::.cctor()
extern "C" void EqualityComparer_1__cctor_m16621_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1__cctor_m16621(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1__cctor_m16621_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared (EqualityComparer_1_t3506 * __this, Object_t * ___obj, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3506 *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared (EqualityComparer_1_t3506 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3506 *, Object_t *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::get_Default()
extern "C" EqualityComparer_1_t3506 * EqualityComparer_1_get_Default_m16624_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1_get_Default_m16624(__this /* static, unused */, method) (( EqualityComparer_1_t3506 * (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1_get_Default_m16624_gshared)(__this /* static, unused */, method)
