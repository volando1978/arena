﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>[]
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>[]
struct  UnityKeyValuePair_2U5BU5D_t3411  : public Array_t
{
};
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>[]
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>[]
struct  UnityKeyValuePair_2U5BU5D_t3455  : public Array_t
{
};
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>[]
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>[]
struct  UnityKeyValuePair_2U5BU5D_t3464  : public Array_t
{
};
// ObjectKvp[]
// ObjectKvp[]
struct  ObjectKvpU5BU5D_t3472  : public Array_t
{
};
// JSONObject[]
// JSONObject[]
struct  JSONObjectU5BU5D_t168  : public Array_t
{
};
struct JSONObjectU5BU5D_t168_StaticFields{
};
// Soomla.Schedule/DateTimeRange[]
// Soomla.Schedule/DateTimeRange[]
struct  DateTimeRangeU5BU5D_t3509  : public Array_t
{
};
// Soomla.Reward[]
// Soomla.Reward[]
struct  RewardU5BU5D_t3519  : public Array_t
{
};
struct RewardU5BU5D_t3519_StaticFields{
};
// Soomla.Store.VirtualCurrency[]
// Soomla.Store.VirtualCurrency[]
struct  VirtualCurrencyU5BU5D_t172  : public Array_t
{
};
// Soomla.Store.VirtualItem[]
// Soomla.Store.VirtualItem[]
struct  VirtualItemU5BU5D_t3564  : public Array_t
{
};
// Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>[]
// Soomla.SoomlaEntity`1<Soomla.Store.VirtualItem>[]
struct  SoomlaEntity_1U5BU5D_t4606  : public Array_t
{
};
// Soomla.Store.VirtualGood[]
// Soomla.Store.VirtualGood[]
struct  VirtualGoodU5BU5D_t173  : public Array_t
{
};
// Soomla.Store.PurchasableVirtualItem[]
// Soomla.Store.PurchasableVirtualItem[]
struct  PurchasableVirtualItemU5BU5D_t3605  : public Array_t
{
};
// Soomla.Store.VirtualCurrencyPack[]
// Soomla.Store.VirtualCurrencyPack[]
struct  VirtualCurrencyPackU5BU5D_t174  : public Array_t
{
};
struct VirtualCurrencyPackU5BU5D_t174_StaticFields{
};
// Soomla.Store.VirtualCategory[]
// Soomla.Store.VirtualCategory[]
struct  VirtualCategoryU5BU5D_t175  : public Array_t
{
};
// Soomla.Store.MarketItem[]
// Soomla.Store.MarketItem[]
struct  MarketItemU5BU5D_t3557  : public Array_t
{
};
// Soomla.Store.StoreInventory/LocalUpgrade[]
// Soomla.Store.StoreInventory/LocalUpgrade[]
struct  LocalUpgradeU5BU5D_t3570  : public Array_t
{
};
// Soomla.Store.UpgradeVG[]
// Soomla.Store.UpgradeVG[]
struct  UpgradeVGU5BU5D_t3584  : public Array_t
{
};
struct UpgradeVGU5BU5D_t3584_StaticFields{
};
