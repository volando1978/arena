﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct Dictionary_2_t542;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Achievement>
struct  ValueCollection_t3716  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.BasicApi.Achievement>::dictionary
	Dictionary_2_t542 * ___dictionary_0;
};
