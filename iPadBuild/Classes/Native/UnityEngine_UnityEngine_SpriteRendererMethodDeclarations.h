﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t744;
// UnityEngine.Sprite
struct Sprite_t766;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C" Sprite_t766 * SpriteRenderer_get_sprite_m4186 (SpriteRenderer_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C" void SpriteRenderer_set_sprite_m4164 (SpriteRenderer_t744 * __this, Sprite_t766 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
extern "C" Sprite_t766 * SpriteRenderer_GetSprite_INTERNAL_m6887 (SpriteRenderer_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
extern "C" void SpriteRenderer_SetSprite_INTERNAL_m6888 (SpriteRenderer_t744 * __this, Sprite_t766 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void SpriteRenderer_INTERNAL_get_color_m6889 (SpriteRenderer_t744 * __this, Color_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void SpriteRenderer_INTERNAL_set_color_m6890 (SpriteRenderer_t744 * __this, Color_t747 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C" Color_t747  SpriteRenderer_get_color_m4281 (SpriteRenderer_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C" void SpriteRenderer_set_color_m4213 (SpriteRenderer_t744 * __this, Color_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
