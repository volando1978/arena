﻿#pragma once
#include <stdint.h>
// JSONObject[]
struct JSONObjectU5BU5D_t168;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<JSONObject>
struct  List_1_t42  : public Object_t
{
	// T[] System.Collections.Generic.List`1<JSONObject>::_items
	JSONObjectU5BU5D_t168* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<JSONObject>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<JSONObject>::_version
	int32_t ____version_3;
};
struct List_1_t42_StaticFields{
	// T[] System.Collections.Generic.List`1<JSONObject>::EmptyArray
	JSONObjectU5BU5D_t168* ___EmptyArray_4;
};
