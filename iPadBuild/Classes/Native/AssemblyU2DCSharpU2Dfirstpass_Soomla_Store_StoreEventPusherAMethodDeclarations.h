﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEventPusherAndroid
struct StoreEventPusherAndroid_t64;

// System.Void Soomla.Store.StoreEventPusherAndroid::.ctor()
extern "C" void StoreEventPusherAndroid__ctor_m255 (StoreEventPusherAndroid_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
