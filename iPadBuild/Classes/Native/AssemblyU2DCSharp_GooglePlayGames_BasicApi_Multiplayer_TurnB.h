﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
struct  MatchStatus_t348 
{
	// System.Int32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus::value__
	int32_t ___value___1;
};
