﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// playerMovement
struct playerMovement_t830;
// enemyController
struct enemyController_t785;
// UnityEngine.Collider
struct Collider_t900;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void playerMovement::.ctor()
extern "C" void playerMovement__ctor_m3632 (playerMovement_t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerMovement::move()
extern "C" void playerMovement_move_m3633 (playerMovement_t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerMovement::rotateShipDirection(enemyController)
extern "C" void playerMovement_rotateShipDirection_m3634 (playerMovement_t830 * __this, enemyController_t785 * ____enemyController, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerMovement::OnTriggerEnter(UnityEngine.Collider)
extern "C" void playerMovement_OnTriggerEnter_m3635 (playerMovement_t830 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator playerMovement::moveCoin(UnityEngine.GameObject)
extern "C" Object_t * playerMovement_moveCoin_m3636 (playerMovement_t830 * __this, GameObject_t144 * ___coin, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single playerMovement::calculateAngle(UnityEngine.Vector2)
extern "C" float playerMovement_calculateAngle_m3637 (playerMovement_t830 * __this, Vector2_t739  ___target, MethodInfo* method) IL2CPP_METHOD_ATTR;
