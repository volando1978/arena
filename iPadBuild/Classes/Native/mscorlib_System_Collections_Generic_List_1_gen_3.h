﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t169;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.String>
struct  List_1_t43  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.String>::_items
	StringU5BU5D_t169* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.String>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.String>::_version
	int32_t ____version_3;
};
struct List_1_t43_StaticFields{
	// T[] System.Collections.Generic.List`1<System.String>::EmptyArray
	StringU5BU5D_t169* ___EmptyArray_4;
};
