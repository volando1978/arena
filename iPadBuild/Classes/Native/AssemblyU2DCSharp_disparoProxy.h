﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// enemyController
struct enemyController_t785;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// disparoProxy
struct  disparoProxy_t786  : public MonoBehaviour_t26
{
	// System.Int32 disparoProxy::shootTime
	int32_t ___shootTime_2;
	// System.Int32 disparoProxy::shotTimeNormal
	int32_t ___shotTimeNormal_3;
	// System.Single disparoProxy::shotForce
	float ___shotForce_4;
	// UnityEngine.GameObject disparoProxy::bulletThin
	GameObject_t144 * ___bulletThin_5;
	// UnityEngine.GameObject disparoProxy::enemyController
	GameObject_t144 * ___enemyController_6;
	// enemyController disparoProxy::_enemyController
	enemyController_t785 * ____enemyController_7;
};
