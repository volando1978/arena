﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Schedule
struct Schedule_t50;
// System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>
struct List_1_t49;
// JSONObject
struct JSONObject_t30;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// Soomla.Schedule/Recurrence
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Schedule_Recurrence.h"

// System.Void Soomla.Schedule::.ctor(System.Int32)
extern "C" void Schedule__ctor_m184 (Schedule_t50 * __this, int32_t ___activationLimit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Schedule::.ctor(System.DateTime,System.DateTime,Soomla.Schedule/Recurrence,System.Int32)
extern "C" void Schedule__ctor_m185 (Schedule_t50 * __this, DateTime_t48  ___startTime, DateTime_t48  ___endTime, int32_t ___recurrence, int32_t ___activationLimit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Schedule::.ctor(System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>,Soomla.Schedule/Recurrence,System.Int32)
extern "C" void Schedule__ctor_m186 (Schedule_t50 * __this, List_1_t49 * ___timeRanges, int32_t ___recurrence, int32_t ___activationLimit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Schedule::.ctor(JSONObject)
extern "C" void Schedule__ctor_m187 (Schedule_t50 * __this, JSONObject_t30 * ___jsonSched, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Schedule::.cctor()
extern "C" void Schedule__cctor_m188 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Schedule Soomla.Schedule::AnyTimeOnce()
extern "C" Schedule_t50 * Schedule_AnyTimeOnce_m189 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Schedule Soomla.Schedule::AnyTimeLimited(System.Int32)
extern "C" Schedule_t50 * Schedule_AnyTimeLimited_m190 (Object_t * __this /* static, unused */, int32_t ___activationLimit, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Schedule Soomla.Schedule::AnyTimeUnLimited()
extern "C" Schedule_t50 * Schedule_AnyTimeUnLimited_m191 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Schedule::toJSONObject()
extern "C" JSONObject_t30 * Schedule_toJSONObject_m192 (Schedule_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Schedule::Approve(System.Int32)
extern "C" bool Schedule_Approve_m193 (Schedule_t50 * __this, int32_t ___activationTimes, MethodInfo* method) IL2CPP_METHOD_ATTR;
