﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t1551;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1666;

// System.Void UnityEngine.WWWTranscoder::.cctor()
extern "C" void WWWTranscoder__cctor_m6948 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine.WWWTranscoder::Hex2Byte(System.Byte[],System.Int32)
extern "C" uint8_t WWWTranscoder_Hex2Byte_m6949 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___b, int32_t ___offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern "C" ByteU5BU5D_t350* WWWTranscoder_Byte2Hex_m6950 (Object_t * __this /* static, unused */, uint8_t ___b, ByteU5BU5D_t350* ___hexChars, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWWTranscoder::URLEncode(System.String,System.Text.Encoding)
extern "C" String_t* WWWTranscoder_URLEncode_m6951 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern "C" ByteU5BU5D_t350* WWWTranscoder_URLEncode_m6952 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___toEncode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern "C" String_t* WWWTranscoder_QPEncode_m6953 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern "C" ByteU5BU5D_t350* WWWTranscoder_Encode_m6954 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___input, uint8_t ___escapeChar, uint8_t ___space, ByteU5BU5D_t350* ___forbidden, bool ___uppercase, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C" bool WWWTranscoder_ByteArrayContains_m6955 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___array, uint8_t ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWWTranscoder::URLDecode(System.String,System.Text.Encoding)
extern "C" String_t* WWWTranscoder_URLDecode_m6956 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::Decode(System.Byte[],System.Byte,System.Byte)
extern "C" ByteU5BU5D_t350* WWWTranscoder_Decode_m6957 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___input, uint8_t ___escapeChar, uint8_t ___space, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern "C" bool WWWTranscoder_SevenBitClean_m6958 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C" bool WWWTranscoder_SevenBitClean_m6959 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
