﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDelaySignAttribute
struct AssemblyDelaySignAttribute_t1812;

// System.Void System.Reflection.AssemblyDelaySignAttribute::.ctor(System.Boolean)
extern "C" void AssemblyDelaySignAttribute__ctor_m7694 (AssemblyDelaySignAttribute_t1812 * __this, bool ___delaySign, MethodInfo* method) IL2CPP_METHOD_ATTR;
