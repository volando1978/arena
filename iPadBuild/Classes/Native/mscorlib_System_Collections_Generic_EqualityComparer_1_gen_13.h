﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct EqualityComparer_1_t2916;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>
struct  EqualityComparer_1_t2916  : public Object_t
{
};
struct EqualityComparer_1_t2916_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTimeOffset>::_default
	EqualityComparer_1_t2916 * ____default_0;
};
