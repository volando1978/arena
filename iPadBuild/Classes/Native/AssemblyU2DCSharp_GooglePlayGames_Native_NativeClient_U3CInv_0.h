﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Object>
struct Action_1_t3422;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>
struct  U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>::callback
	Action_1_t3422 * ___callback_0;
	// T GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>::data
	Object_t * ___data_1;
};
