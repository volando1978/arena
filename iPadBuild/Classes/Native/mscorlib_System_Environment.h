﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2808;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct  Environment_t2809  : public Object_t
{
};
struct Environment_t2809_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2808 * ___os_0;
};
