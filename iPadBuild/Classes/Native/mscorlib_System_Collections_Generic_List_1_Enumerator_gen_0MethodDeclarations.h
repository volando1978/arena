﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>
struct Enumerator_t205;
// System.Object
struct Object_t;
// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>
struct List_1_t49;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m16719(__this, ___l, method) (( void (*) (Enumerator_t205 *, List_1_t49 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16720(__this, method) (( Object_t * (*) (Enumerator_t205 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::Dispose()
#define Enumerator_Dispose_m16721(__this, method) (( void (*) (Enumerator_t205 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::VerifyState()
#define Enumerator_VerifyState_m16722(__this, method) (( void (*) (Enumerator_t205 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::MoveNext()
#define Enumerator_MoveNext_m923(__this, method) (( bool (*) (Enumerator_t205 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::get_Current()
#define Enumerator_get_Current_m921(__this, method) (( DateTimeRange_t47 * (*) (Enumerator_t205 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
