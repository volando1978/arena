﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>
struct Func_2_t957;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
struct ClaimMilestoneResponse_t690;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.IntPtr,System.Object>
#include "System_Core_System_Func_2_gen_41MethodDeclarations.h"
#define Func_2__ctor_m3959(__this, ___object, ___method, method) (( void (*) (Func_2_t957 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20368_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>::Invoke(T)
#define Func_2_Invoke_m20695(__this, ___arg1, method) (( ClaimMilestoneResponse_t690 * (*) (Func_2_t957 *, IntPtr_t, MethodInfo*))Func_2_Invoke_m20370_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20696(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t957 *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20372_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20697(__this, ___result, method) (( ClaimMilestoneResponse_t690 * (*) (Func_2_t957 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20374_gshared)(__this, ___result, method)
