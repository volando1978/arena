﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct HashSet_1_t583;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerable_1_t4386;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3758;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerator_1_t4387;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3757;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"

// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor()
// System.Collections.Generic.HashSet`1<System.Int32>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_2MethodDeclarations.h"
#define HashSet_1__ctor_m3820(__this, method) (( void (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1__ctor_m20229_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m20230(__this, ___collection, ___comparer, method) (( void (*) (HashSet_1_t583 *, Object_t*, Object_t*, MethodInfo*))HashSet_1__ctor_m20231_gshared)(__this, ___collection, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m20232(__this, ___info, ___context, method) (( void (*) (HashSet_1_t583 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1__ctor_m20233_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20234(__this, method) (( Object_t* (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20236(__this, method) (( bool (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20238(__this, ___array, ___index, method) (( void (*) (HashSet_1_t583 *, ParticipantStatusU5BU5D_t3757*, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20240(__this, ___item, method) (( void (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20242(__this, method) (( Object_t * (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::get_Count()
#define HashSet_1_get_Count_m20244(__this, method) (( int32_t (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_get_Count_m20245_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m20246(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t583 *, int32_t, Object_t*, MethodInfo*))HashSet_1_Init_m20247_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m20248(__this, ___size, method) (( void (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_InitArrays_m20249_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m20250(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t583 *, int32_t, int32_t, int32_t, MethodInfo*))HashSet_1_SlotsContainsAt_m20251_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m20252(__this, ___array, ___index, method) (( void (*) (HashSet_1_t583 *, ParticipantStatusU5BU5D_t3757*, int32_t, MethodInfo*))HashSet_1_CopyTo_m20253_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m20254(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t583 *, ParticipantStatusU5BU5D_t3757*, int32_t, int32_t, MethodInfo*))HashSet_1_CopyTo_m20255_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Resize()
#define HashSet_1_Resize_m20256(__this, method) (( void (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_Resize_m20257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m20258(__this, ___index, method) (( int32_t (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_GetLinkHashCode_m20259_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m20260(__this, ___item, method) (( int32_t (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_GetItemHashCode_m20261_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Add(T)
#define HashSet_1_Add_m3821(__this, ___item, method) (( bool (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_Add_m20262_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Clear()
#define HashSet_1_Clear_m20263(__this, method) (( void (*) (HashSet_1_t583 *, MethodInfo*))HashSet_1_Clear_m20264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Contains(T)
#define HashSet_1_Contains_m20265(__this, ___item, method) (( bool (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_Contains_m20266_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::Remove(T)
#define HashSet_1_Remove_m20267(__this, ___item, method) (( bool (*) (HashSet_1_t583 *, int32_t, MethodInfo*))HashSet_1_Remove_m20268_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m20269(__this, ___info, ___context, method) (( void (*) (HashSet_1_t583 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1_GetObjectData_m20270_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m20271(__this, ___sender, method) (( void (*) (HashSet_1_t583 *, Object_t *, MethodInfo*))HashSet_1_OnDeserialization_m20272_gshared)(__this, ___sender, method)
