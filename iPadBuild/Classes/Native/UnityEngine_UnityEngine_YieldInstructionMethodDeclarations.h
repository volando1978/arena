﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t1493;
struct YieldInstruction_t1493_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m7095 (YieldInstruction_t1493 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t1493_marshal(const YieldInstruction_t1493& unmarshaled, YieldInstruction_t1493_marshaled& marshaled);
void YieldInstruction_t1493_marshal_back(const YieldInstruction_t1493_marshaled& marshaled, YieldInstruction_t1493& unmarshaled);
void YieldInstruction_t1493_marshal_cleanup(YieldInstruction_t1493_marshaled& marshaled);
