﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.EventSystems.BaseInputModule>
struct Action_1_t3867;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t1152;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m21531(__this, ___object, ___method, method) (( void (*) (Action_1_t3867 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::Invoke(T)
#define Action_1_Invoke_m21532(__this, ___obj, method) (( void (*) (Action_1_t3867 *, BaseInputModule_t1152 *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.BaseInputModule>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m21533(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3867 *, BaseInputModule_t1152 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.EventSystems.BaseInputModule>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m21534(__this, ___result, method) (( void (*) (Action_1_t3867 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
