﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>
struct ReadOnlyCollection_1_t3637;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<GooglePlayGames.BasicApi.Achievement>
struct IList_1_t3636;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3635;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Achievement>
struct IEnumerator_1_t4349;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m18597(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3637 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m15428_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18598(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3637 *, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18599(__this, method) (( void (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18600(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18601(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18602(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18603(__this, ___index, method) (( Achievement_t333 * (*) (ReadOnlyCollection_1_t3637 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18604(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18605(__this, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18606(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3637 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18607(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m18608(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3637 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m18609(__this, method) (( void (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m18610(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18611(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3637 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m18612(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m18613(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3637 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18614(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18615(__this, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18616(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18617(__this, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18618(__this, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m18619(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3637 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m18620(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3637 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::Contains(T)
#define ReadOnlyCollection_1_Contains_m18621(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3637 *, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_Contains_m15452_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m18622(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3637 *, AchievementU5BU5D_t3635*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m15453_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m18623(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15454_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m18624(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3637 *, Achievement_t333 *, MethodInfo*))ReadOnlyCollection_1_IndexOf_m15455_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::get_Count()
#define ReadOnlyCollection_1_get_Count_m18625(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3637 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m15456_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m18626(__this, ___index, method) (( Achievement_t333 * (*) (ReadOnlyCollection_1_t3637 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m15457_gshared)(__this, ___index, method)
