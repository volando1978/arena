﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualItemNotFoundException
struct VirtualItemNotFoundException_t137;
// System.String
struct String_t;

// System.Void Soomla.Store.VirtualItemNotFoundException::.ctor()
extern "C" void VirtualItemNotFoundException__ctor_m669 (VirtualItemNotFoundException_t137 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualItemNotFoundException::.ctor(System.String,System.String)
extern "C" void VirtualItemNotFoundException__ctor_m670 (VirtualItemNotFoundException_t137 * __this, String_t* ___lookupBy, String_t* ___lookupVal, MethodInfo* method) IL2CPP_METHOD_ATTR;
