﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEvents/RunLaterDelegate
struct RunLaterDelegate_t87;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Soomla.Store.StoreEvents/RunLaterDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void RunLaterDelegate__ctor_m414 (RunLaterDelegate_t87 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/RunLaterDelegate::Invoke()
extern "C" void RunLaterDelegate_Invoke_m415 (RunLaterDelegate_t87 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RunLaterDelegate_t87(Il2CppObject* delegate);
// System.IAsyncResult Soomla.Store.StoreEvents/RunLaterDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * RunLaterDelegate_BeginInvoke_m416 (RunLaterDelegate_t87 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/RunLaterDelegate::EndInvoke(System.IAsyncResult)
extern "C" void RunLaterDelegate_EndInvoke_m417 (RunLaterDelegate_t87 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
