﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$72
struct U24ArrayTypeU2472_t2863;
struct U24ArrayTypeU2472_t2863_marshaled;

void U24ArrayTypeU2472_t2863_marshal(const U24ArrayTypeU2472_t2863& unmarshaled, U24ArrayTypeU2472_t2863_marshaled& marshaled);
void U24ArrayTypeU2472_t2863_marshal_back(const U24ArrayTypeU2472_t2863_marshaled& marshaled, U24ArrayTypeU2472_t2863& unmarshaled);
void U24ArrayTypeU2472_t2863_marshal_cleanup(U24ArrayTypeU2472_t2863_marshaled& marshaled);
