﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t3486;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t920;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16190_gshared (Enumerator_t3486 * __this, Dictionary_2_t920 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m16190(__this, ___dictionary, method) (( void (*) (Enumerator_t3486 *, Dictionary_2_t920 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16192(__this, method) (( Object_t * (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196(__this, method) (( Object_t * (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198(__this, method) (( Object_t * (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16199_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m16199(__this, method) (( bool (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3407  Enumerator_get_Current_m16200_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_get_Current_m16200(__this, method) (( KeyValuePair_2_t3407  (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m16202_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m16202(__this, method) (( Object_t * (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m16204_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m16204(__this, method) (( Object_t * (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m16206_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m16206(__this, method) (( void (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16208_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m16208(__this, method) (( void (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16210_gshared (Enumerator_t3486 * __this, MethodInfo* method);
#define Enumerator_Dispose_m16210(__this, method) (( void (*) (Enumerator_t3486 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
