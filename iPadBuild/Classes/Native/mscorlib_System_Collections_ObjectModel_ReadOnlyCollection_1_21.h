﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.Native.NativeEvent>
struct IList_1_t3729;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.NativeEvent>
struct  ReadOnlyCollection_1_t3730  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.NativeEvent>::list
	Object_t* ___list_0;
};
