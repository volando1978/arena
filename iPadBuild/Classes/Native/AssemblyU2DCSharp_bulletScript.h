﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.Sprite
struct Sprite_t766;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t744;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// bulletScript
struct  bulletScript_t767  : public MonoBehaviour_t26
{
	// System.Single bulletScript::speed
	float ___speed_2;
	// System.Single bulletScript::angle
	float ___angle_3;
	// System.Single bulletScript::step
	float ___step_4;
	// UnityEngine.Vector3 bulletScript::p
	Vector3_t758  ___p_5;
	// UnityEngine.GameObject bulletScript::enemyController
	GameObject_t144 * ___enemyController_6;
	// UnityEngine.GameObject bulletScript::paquete
	GameObject_t144 * ___paquete_7;
	// UnityEngine.GameObject bulletScript::coin
	GameObject_t144 * ___coin_8;
	// UnityEngine.GameObject bulletScript::movingText
	GameObject_t144 * ___movingText_9;
	// UnityEngine.GameObject bulletScript::movingTextKills
	GameObject_t144 * ___movingTextKills_10;
	// UnityEngine.Sprite bulletScript::thinBullet
	Sprite_t766 * ___thinBullet_11;
	// UnityEngine.Sprite bulletScript::fatBullet
	Sprite_t766 * ___fatBullet_12;
	// UnityEngine.Sprite bulletScript::triBullet
	Sprite_t766 * ___triBullet_13;
	// UnityEngine.Sprite bulletScript::circularBullet
	Sprite_t766 * ___circularBullet_14;
	// UnityEngine.SpriteRenderer bulletScript::_spriterenderer
	SpriteRenderer_t744 * ____spriterenderer_15;
	// UnityEngine.GameObject bulletScript::explosion
	GameObject_t144 * ___explosion_16;
	// UnityEngine.GameObject bulletScript::dust
	GameObject_t144 * ___dust_17;
	// UnityEngine.GameObject bulletScript::explosionRandom
	GameObject_t144 * ___explosionRandom_18;
};
