﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// newWeaponRoomSrc/<animSpin>c__Iterator11
struct U3CanimSpinU3Ec__Iterator11_t822;
// System.Object
struct Object_t;

// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::.ctor()
extern "C" void U3CanimSpinU3Ec__Iterator11__ctor_m3593 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean newWeaponRoomSrc/<animSpin>c__Iterator11::MoveNext()
extern "C" bool U3CanimSpinU3Ec__Iterator11_MoveNext_m3596 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Dispose()
extern "C" void U3CanimSpinU3Ec__Iterator11_Dispose_m3597 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Reset()
extern "C" void U3CanimSpinU3Ec__Iterator11_Reset_m3598 (U3CanimSpinU3Ec__Iterator11_t822 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
