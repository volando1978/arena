﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder
struct Builder_t678;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.Native.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t679;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::.ctor()
extern "C" void Builder__ctor_m2817 (Builder_t678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void Builder_CallDispose_m2818 (Builder_t678 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetDescription(System.String)
extern "C" Builder_t678 * Builder_SetDescription_m2819 (Builder_t678 * __this, String_t* ___description, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetPlayedTime(System.UInt64)
extern "C" Builder_t678 * Builder_SetPlayedTime_m2820 (Builder_t678 * __this, uint64_t ___playedTime, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::SetCoverImageFromPngData(System.Byte[])
extern "C" Builder_t678 * Builder_SetCoverImageFromPngData_m2821 (Builder_t678 * __this, ByteU5BU5D_t350* ___pngData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSnapshotMetadataChange/Builder::Build()
extern "C" NativeSnapshotMetadataChange_t679 * Builder_Build_m2822 (Builder_t678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
