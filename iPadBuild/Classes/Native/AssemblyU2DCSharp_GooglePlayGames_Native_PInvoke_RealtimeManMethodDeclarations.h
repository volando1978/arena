﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::.ctor(System.IntPtr)
extern "C" void RealTimeRoomResponse__ctor_m2953 (RealTimeRoomResponse_t695 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::ResponseStatus()
extern "C" int32_t RealTimeRoomResponse_ResponseStatus_m2954 (RealTimeRoomResponse_t695 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::RequestSucceeded()
extern "C" bool RealTimeRoomResponse_RequestSucceeded_m2955 (RealTimeRoomResponse_t695 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::Room()
extern "C" NativeRealTimeRoom_t574 * RealTimeRoomResponse_Room_m2956 (RealTimeRoomResponse_t695 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealTimeRoomResponse_CallDispose_m2957 (RealTimeRoomResponse_t695 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse::FromPointer(System.IntPtr)
extern "C" RealTimeRoomResponse_t695 * RealTimeRoomResponse_FromPointer_m2958 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
