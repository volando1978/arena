﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject/<BakeAsync>c__Iterator0
struct U3CBakeAsyncU3Ec__Iterator0_t35;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void JSONObject/<BakeAsync>c__Iterator0::.ctor()
extern "C" void U3CBakeAsyncU3Ec__Iterator0__ctor_m74 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m75 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m76 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m77 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator0::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C" Object_t* U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m78 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<BakeAsync>c__Iterator0::MoveNext()
extern "C" bool U3CBakeAsyncU3Ec__Iterator0_MoveNext_m79 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator0::Dispose()
extern "C" void U3CBakeAsyncU3Ec__Iterator0_Dispose_m80 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator0::Reset()
extern "C" void U3CBakeAsyncU3Ec__Iterator0_Reset_m81 (U3CBakeAsyncU3Ec__Iterator0_t35 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
