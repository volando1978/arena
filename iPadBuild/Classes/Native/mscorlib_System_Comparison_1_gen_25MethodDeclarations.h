﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<GooglePlayGames.Native.NativeEvent>
struct Comparison_1_t3734;
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<GooglePlayGames.Native.NativeEvent>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m19868(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3734 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m15541_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<GooglePlayGames.Native.NativeEvent>::Invoke(T,T)
#define Comparison_1_Invoke_m19869(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3734 *, NativeEvent_t674 *, NativeEvent_t674 *, MethodInfo*))Comparison_1_Invoke_m15542_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<GooglePlayGames.Native.NativeEvent>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m19870(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3734 *, NativeEvent_t674 *, NativeEvent_t674 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m15543_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<GooglePlayGames.Native.NativeEvent>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m19871(__this, ___result, method) (( int32_t (*) (Comparison_1_t3734 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m15544_gshared)(__this, ___result, method)
