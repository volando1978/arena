﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Globalization.Calendar>
struct InternalEnumerator_1_t4222;
// System.Object
struct Object_t;
// System.Globalization.Calendar
struct Calendar_t2456;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25915(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4222 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25916(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4222 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
#define InternalEnumerator_1_Dispose_m25917(__this, method) (( void (*) (InternalEnumerator_1_t4222 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25918(__this, method) (( bool (*) (InternalEnumerator_1_t4222 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
#define InternalEnumerator_1_get_Current_m25919(__this, method) (( Calendar_t2456 * (*) (InternalEnumerator_1_t4222 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
