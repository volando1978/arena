﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.String
struct String_t;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.Object
#include "mscorlib_System_Object.h"
// JSONObject/<BakeAsync>c__Iterator0
struct  U3CBakeAsyncU3Ec__Iterator0_t35  : public Object_t
{
	// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<BakeAsync>c__Iterator0::<$s_6>__0
	Object_t* ___U3CU24s_6U3E__0_0;
	// System.String JSONObject/<BakeAsync>c__Iterator0::<s>__1
	String_t* ___U3CsU3E__1_1;
	// System.Int32 JSONObject/<BakeAsync>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// System.Object JSONObject/<BakeAsync>c__Iterator0::$current
	Object_t * ___U24current_3;
	// JSONObject JSONObject/<BakeAsync>c__Iterator0::<>f__this
	JSONObject_t30 * ___U3CU3Ef__this_4;
};
