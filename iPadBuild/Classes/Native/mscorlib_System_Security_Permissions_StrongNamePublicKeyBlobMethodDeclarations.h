﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t2722;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Boolean System.Security.Permissions.StrongNamePublicKeyBlob::Equals(System.Object)
extern "C" bool StrongNamePublicKeyBlob_Equals_m13212 (StrongNamePublicKeyBlob_t2722 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.StrongNamePublicKeyBlob::GetHashCode()
extern "C" int32_t StrongNamePublicKeyBlob_GetHashCode_m13213 (StrongNamePublicKeyBlob_t2722 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.StrongNamePublicKeyBlob::ToString()
extern "C" String_t* StrongNamePublicKeyBlob_ToString_m13214 (StrongNamePublicKeyBlob_t2722 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
