﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// LaserControl
struct  LaserControl_t745  : public MonoBehaviour_t26
{
	// System.Boolean LaserControl::isShowingLaser
	bool ___isShowingLaser_2;
	// UnityEngine.Color LaserControl::yellow
	Color_t747  ___yellow_3;
	// System.Int32 LaserControl::timer
	int32_t ___timer_4;
};
