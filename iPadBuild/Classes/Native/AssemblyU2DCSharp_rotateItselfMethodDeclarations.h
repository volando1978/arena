﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// rotateItself
struct rotateItself_t833;

// System.Void rotateItself::.ctor()
extern "C" void rotateItself__ctor_m3644 (rotateItself_t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void rotateItself::Start()
extern "C" void rotateItself_Start_m3645 (rotateItself_t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void rotateItself::Update()
extern "C" void rotateItself_Update_m3646 (rotateItself_t833 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
