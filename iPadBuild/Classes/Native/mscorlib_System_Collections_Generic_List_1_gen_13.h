﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3635;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct  List_1_t841  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::_items
	AchievementU5BU5D_t3635* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::_version
	int32_t ____version_3;
};
struct List_1_t841_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::EmptyArray
	AchievementU5BU5D_t3635* ___EmptyArray_4;
};
