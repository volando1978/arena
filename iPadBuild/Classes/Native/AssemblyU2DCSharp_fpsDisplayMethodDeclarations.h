﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// fpsDisplay
struct fpsDisplay_t792;

// System.Void fpsDisplay::.ctor()
extern "C" void fpsDisplay__ctor_m3410 (fpsDisplay_t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void fpsDisplay::Start()
extern "C" void fpsDisplay_Start_m3411 (fpsDisplay_t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void fpsDisplay::OnGUI()
extern "C" void fpsDisplay_OnGUI_m3412 (fpsDisplay_t792 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
