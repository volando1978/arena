﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t913;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<GooglePlayGames.BasicApi.Events.IEvent>
struct  Comparison_1_t3646  : public MulticastDelegate_t22
{
};
