﻿#pragma once
#include <stdint.h>
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct Action_3_t554;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28
struct  U3CFromQuestUICallbackU3Ec__AnonStorey28_t555  : public Object_t
{
	// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone> GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28::callback
	Action_3_t554 * ___callback_0;
};
