﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.ASCIIEncoding
struct ASCIIEncoding_t2734;
// System.Char[]
struct CharU5BU5D_t41;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2745;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2736;
// System.Text.Decoder
struct Decoder_t2472;

// System.Void System.Text.ASCIIEncoding::.ctor()
extern "C" void ASCIIEncoding__ctor_m13275 (ASCIIEncoding_t2734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m13276 (ASCIIEncoding_t2734 * __this, CharU5BU5D_t41* ___chars, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.String)
extern "C" int32_t ASCIIEncoding_GetByteCount_m13277 (ASCIIEncoding_t2734 * __this, String_t* ___chars, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m13278 (ASCIIEncoding_t2734 * __this, CharU5BU5D_t41* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m13279 (ASCIIEncoding_t2734 * __this, CharU5BU5D_t41* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t2745 ** ___buffer, CharU5BU5D_t41** ___fallback_chars, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m13280 (ASCIIEncoding_t2734 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m13281 (ASCIIEncoding_t2734 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t2745 ** ___buffer, CharU5BU5D_t41** ___fallback_chars, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetCharCount(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetCharCount_m13282 (ASCIIEncoding_t2734 * __this, ByteU5BU5D_t350* ___bytes, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetChars_m13283 (ASCIIEncoding_t2734 * __this, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t41* ___chars, int32_t ___charIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Text.DecoderFallbackBuffer&)
extern "C" int32_t ASCIIEncoding_GetChars_m13284 (ASCIIEncoding_t2734 * __this, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t41* ___chars, int32_t ___charIndex, DecoderFallbackBuffer_t2736 ** ___buffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxByteCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxByteCount_m13285 (ASCIIEncoding_t2734 * __this, int32_t ___charCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxCharCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxCharCount_m13286 (ASCIIEncoding_t2734 * __this, int32_t ___byteCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.ASCIIEncoding::GetString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* ASCIIEncoding_GetString_m13287 (ASCIIEncoding_t2734 * __this, ByteU5BU5D_t350* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m13288 (ASCIIEncoding_t2734 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m13289 (ASCIIEncoding_t2734 * __this, uint16_t* ___chars, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Decoder System.Text.ASCIIEncoding::GetDecoder()
extern "C" Decoder_t2472 * ASCIIEncoding_GetDecoder_m13290 (ASCIIEncoding_t2734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
