﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SequenceReward
struct SequenceReward_t60;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// JSONObject
struct JSONObject_t30;
// Soomla.Reward
struct Reward_t55;

// System.Void Soomla.SequenceReward::.ctor(System.String,System.String,System.Collections.Generic.List`1<Soomla.Reward>)
extern "C" void SequenceReward__ctor_m245 (SequenceReward_t60 * __this, String_t* ___id, String_t* ___name, List_1_t56 * ___rewards, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SequenceReward::.ctor(JSONObject)
extern "C" void SequenceReward__ctor_m246 (SequenceReward_t60 * __this, JSONObject_t30 * ___jsonReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SequenceReward::.cctor()
extern "C" void SequenceReward__cctor_m247 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.SequenceReward::toJSONObject()
extern "C" JSONObject_t30 * SequenceReward_toJSONObject_m248 (SequenceReward_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Reward Soomla.SequenceReward::GetLastGivenReward()
extern "C" Reward_t55 * SequenceReward_GetLastGivenReward_m249 (SequenceReward_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.SequenceReward::HasMoreToGive()
extern "C" bool SequenceReward_HasMoreToGive_m250 (SequenceReward_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.SequenceReward::ForceNextRewardToGive(Soomla.Reward)
extern "C" bool SequenceReward_ForceNextRewardToGive_m251 (SequenceReward_t60 * __this, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.SequenceReward::giveInner()
extern "C" bool SequenceReward_giveInner_m252 (SequenceReward_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.SequenceReward::takeInner()
extern "C" bool SequenceReward_takeInner_m253 (SequenceReward_t60 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
