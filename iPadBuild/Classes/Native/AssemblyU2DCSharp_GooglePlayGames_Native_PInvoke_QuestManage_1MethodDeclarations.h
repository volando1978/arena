﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
struct ClaimMilestoneResponse_t690;
// GooglePlayGames.Native.NativeQuest
struct NativeQuest_t677;
// GooglePlayGames.Native.NativeQuestMilestone
struct NativeQuestMilestone_t676;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::.ctor(System.IntPtr)
extern "C" void ClaimMilestoneResponse__ctor_m2902 (ClaimMilestoneResponse_t690 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ResponseStatus()
extern "C" int32_t ClaimMilestoneResponse_ResponseStatus_m2903 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuest GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::Quest()
extern "C" NativeQuest_t677 * ClaimMilestoneResponse_Quest_m2904 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::ClaimedMilestone()
extern "C" NativeQuestMilestone_t676 * ClaimMilestoneResponse_ClaimedMilestone_m2905 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::RequestSucceeded()
extern "C" bool ClaimMilestoneResponse_RequestSucceeded_m2906 (ClaimMilestoneResponse_t690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ClaimMilestoneResponse_CallDispose_m2907 (ClaimMilestoneResponse_t690 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse::FromPointer(System.IntPtr)
extern "C" ClaimMilestoneResponse_t690 * ClaimMilestoneResponse_FromPointer_m2908 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
