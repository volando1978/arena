﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<System.IntPtr>
struct IList_1_t3809;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>
struct  ReadOnlyCollection_1_t3810  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::list
	Object_t* ___list_0;
};
