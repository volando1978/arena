﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.MultiArrayFixupRecord
struct MultiArrayFixupRecord_t2667;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2664;
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2657;

// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Int32[],System.Runtime.Serialization.ObjectRecord)
extern "C" void MultiArrayFixupRecord__ctor_m12843 (MultiArrayFixupRecord_t2667 * __this, ObjectRecord_t2664 * ___objectToBeFixed, Int32U5BU5D_t107* ___indices, ObjectRecord_t2664 * ___objectRequired, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern "C" void MultiArrayFixupRecord_FixupImpl_m12844 (MultiArrayFixupRecord_t2667 * __this, ObjectManager_t2657 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
