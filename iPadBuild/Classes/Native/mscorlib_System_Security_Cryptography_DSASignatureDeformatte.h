﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t2129;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// System.Security.Cryptography.DSASignatureDeformatter
struct  DSASignatureDeformatter_t2317  : public AsymmetricSignatureDeformatter_t2267
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureDeformatter::dsa
	DSA_t2129 * ___dsa_0;
};
