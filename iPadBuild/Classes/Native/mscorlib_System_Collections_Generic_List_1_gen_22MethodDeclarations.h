﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct List_1_t806;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Matrix4x4>
struct IEnumerable_1_t4400;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Matrix4x4>
struct IEnumerator_1_t4401;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.Matrix4x4>
struct ICollection_1_t4402;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Matrix4x4>
struct ReadOnlyCollection_1_t3834;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t3830;
// System.Predicate`1<UnityEngine.Matrix4x4>
struct Predicate_1_t3838;
// System.Action`1<UnityEngine.Matrix4x4>
struct Action_1_t3839;
// System.Comparison`1<UnityEngine.Matrix4x4>
struct Comparison_1_t3842;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::.ctor()
extern "C" void List_1__ctor_m4240_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1__ctor_m4240(__this, method) (( void (*) (List_1_t806 *, MethodInfo*))List_1__ctor_m4240_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m21030_gshared (List_1_t806 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m21030(__this, ___collection, method) (( void (*) (List_1_t806 *, Object_t*, MethodInfo*))List_1__ctor_m21030_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::.ctor(System.Int32)
extern "C" void List_1__ctor_m21031_gshared (List_1_t806 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m21031(__this, ___capacity, method) (( void (*) (List_1_t806 *, int32_t, MethodInfo*))List_1__ctor_m21031_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::.cctor()
extern "C" void List_1__cctor_m21032_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m21032(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m21032_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21033_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21033(__this, method) (( Object_t* (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21034_gshared (List_1_t806 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m21034(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t806 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m21034_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m21035_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21035(__this, method) (( Object_t * (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m21035_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m21036_gshared (List_1_t806 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m21036(__this, ___item, method) (( int32_t (*) (List_1_t806 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m21036_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m21037_gshared (List_1_t806 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m21037(__this, ___item, method) (( bool (*) (List_1_t806 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m21037_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m21038_gshared (List_1_t806 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m21038(__this, ___item, method) (( int32_t (*) (List_1_t806 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m21038_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m21039_gshared (List_1_t806 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m21039(__this, ___index, ___item, method) (( void (*) (List_1_t806 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m21039_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m21040_gshared (List_1_t806 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m21040(__this, ___item, method) (( void (*) (List_1_t806 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m21040_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21041_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21041(__this, method) (( bool (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21041_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m21042_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21042(__this, method) (( bool (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m21042_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m21043_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m21043(__this, method) (( Object_t * (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m21043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m21044_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m21044(__this, method) (( bool (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m21044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m21045_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m21045(__this, method) (( bool (*) (List_1_t806 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m21045_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m21046_gshared (List_1_t806 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m21046(__this, ___index, method) (( Object_t * (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m21046_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m21047_gshared (List_1_t806 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m21047(__this, ___index, ___value, method) (( void (*) (List_1_t806 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m21047_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Add(T)
extern "C" void List_1_Add_m21048_gshared (List_1_t806 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define List_1_Add_m21048(__this, ___item, method) (( void (*) (List_1_t806 *, Matrix4x4_t997 , MethodInfo*))List_1_Add_m21048_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m21049_gshared (List_1_t806 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m21049(__this, ___newCount, method) (( void (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m21049_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m21050_gshared (List_1_t806 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m21050(__this, ___collection, method) (( void (*) (List_1_t806 *, Object_t*, MethodInfo*))List_1_AddCollection_m21050_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m21051_gshared (List_1_t806 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m21051(__this, ___enumerable, method) (( void (*) (List_1_t806 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m21051_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m21052_gshared (List_1_t806 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m21052(__this, ___collection, method) (( void (*) (List_1_t806 *, Object_t*, MethodInfo*))List_1_AddRange_m21052_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3834 * List_1_AsReadOnly_m21053_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m21053(__this, method) (( ReadOnlyCollection_1_t3834 * (*) (List_1_t806 *, MethodInfo*))List_1_AsReadOnly_m21053_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Clear()
extern "C" void List_1_Clear_m21054_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_Clear_m21054(__this, method) (( void (*) (List_1_t806 *, MethodInfo*))List_1_Clear_m21054_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Contains(T)
extern "C" bool List_1_Contains_m21055_gshared (List_1_t806 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define List_1_Contains_m21055(__this, ___item, method) (( bool (*) (List_1_t806 *, Matrix4x4_t997 , MethodInfo*))List_1_Contains_m21055_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m21056_gshared (List_1_t806 * __this, Matrix4x4U5BU5D_t3830* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m21056(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t806 *, Matrix4x4U5BU5D_t3830*, int32_t, MethodInfo*))List_1_CopyTo_m21056_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Find(System.Predicate`1<T>)
extern "C" Matrix4x4_t997  List_1_Find_m21057_gshared (List_1_t806 * __this, Predicate_1_t3838 * ___match, MethodInfo* method);
#define List_1_Find_m21057(__this, ___match, method) (( Matrix4x4_t997  (*) (List_1_t806 *, Predicate_1_t3838 *, MethodInfo*))List_1_Find_m21057_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m21058_gshared (Object_t * __this /* static, unused */, Predicate_1_t3838 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m21058(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3838 *, MethodInfo*))List_1_CheckMatch_m21058_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m21059_gshared (List_1_t806 * __this, Predicate_1_t3838 * ___match, MethodInfo* method);
#define List_1_FindIndex_m21059(__this, ___match, method) (( int32_t (*) (List_1_t806 *, Predicate_1_t3838 *, MethodInfo*))List_1_FindIndex_m21059_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m21060_gshared (List_1_t806 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3838 * ___match, MethodInfo* method);
#define List_1_GetIndex_m21060(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t806 *, int32_t, int32_t, Predicate_1_t3838 *, MethodInfo*))List_1_GetIndex_m21060_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m21061_gshared (List_1_t806 * __this, Action_1_t3839 * ___action, MethodInfo* method);
#define List_1_ForEach_m21061(__this, ___action, method) (( void (*) (List_1_t806 *, Action_1_t3839 *, MethodInfo*))List_1_ForEach_m21061_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::GetEnumerator()
extern "C" Enumerator_t3832  List_1_GetEnumerator_m21062_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m21062(__this, method) (( Enumerator_t3832  (*) (List_1_t806 *, MethodInfo*))List_1_GetEnumerator_m21062_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m21063_gshared (List_1_t806 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define List_1_IndexOf_m21063(__this, ___item, method) (( int32_t (*) (List_1_t806 *, Matrix4x4_t997 , MethodInfo*))List_1_IndexOf_m21063_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m21064_gshared (List_1_t806 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m21064(__this, ___start, ___delta, method) (( void (*) (List_1_t806 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m21064_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m21065_gshared (List_1_t806 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m21065(__this, ___index, method) (( void (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_CheckIndex_m21065_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m21066_gshared (List_1_t806 * __this, int32_t ___index, Matrix4x4_t997  ___item, MethodInfo* method);
#define List_1_Insert_m21066(__this, ___index, ___item, method) (( void (*) (List_1_t806 *, int32_t, Matrix4x4_t997 , MethodInfo*))List_1_Insert_m21066_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m21067_gshared (List_1_t806 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m21067(__this, ___collection, method) (( void (*) (List_1_t806 *, Object_t*, MethodInfo*))List_1_CheckCollection_m21067_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Remove(T)
extern "C" bool List_1_Remove_m21068_gshared (List_1_t806 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define List_1_Remove_m21068(__this, ___item, method) (( bool (*) (List_1_t806 *, Matrix4x4_t997 , MethodInfo*))List_1_Remove_m21068_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m21069_gshared (List_1_t806 * __this, Predicate_1_t3838 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m21069(__this, ___match, method) (( int32_t (*) (List_1_t806 *, Predicate_1_t3838 *, MethodInfo*))List_1_RemoveAll_m21069_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m21070_gshared (List_1_t806 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m21070(__this, ___index, method) (( void (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_RemoveAt_m21070_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Reverse()
extern "C" void List_1_Reverse_m21071_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_Reverse_m21071(__this, method) (( void (*) (List_1_t806 *, MethodInfo*))List_1_Reverse_m21071_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Sort()
extern "C" void List_1_Sort_m21072_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_Sort_m21072(__this, method) (( void (*) (List_1_t806 *, MethodInfo*))List_1_Sort_m21072_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m21073_gshared (List_1_t806 * __this, Comparison_1_t3842 * ___comparison, MethodInfo* method);
#define List_1_Sort_m21073(__this, ___comparison, method) (( void (*) (List_1_t806 *, Comparison_1_t3842 *, MethodInfo*))List_1_Sort_m21073_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::ToArray()
extern "C" Matrix4x4U5BU5D_t3830* List_1_ToArray_m21074_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_ToArray_m21074(__this, method) (( Matrix4x4U5BU5D_t3830* (*) (List_1_t806 *, MethodInfo*))List_1_ToArray_m21074_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::TrimExcess()
extern "C" void List_1_TrimExcess_m21075_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_TrimExcess_m21075(__this, method) (( void (*) (List_1_t806 *, MethodInfo*))List_1_TrimExcess_m21075_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m21076_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_get_Capacity_m21076(__this, method) (( int32_t (*) (List_1_t806 *, MethodInfo*))List_1_get_Capacity_m21076_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m21077_gshared (List_1_t806 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m21077(__this, ___value, method) (( void (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_set_Capacity_m21077_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::get_Count()
extern "C" int32_t List_1_get_Count_m21078_gshared (List_1_t806 * __this, MethodInfo* method);
#define List_1_get_Count_m21078(__this, method) (( int32_t (*) (List_1_t806 *, MethodInfo*))List_1_get_Count_m21078_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::get_Item(System.Int32)
extern "C" Matrix4x4_t997  List_1_get_Item_m21079_gshared (List_1_t806 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m21079(__this, ___index, method) (( Matrix4x4_t997  (*) (List_1_t806 *, int32_t, MethodInfo*))List_1_get_Item_m21079_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m21080_gshared (List_1_t806 * __this, int32_t ___index, Matrix4x4_t997  ___value, MethodInfo* method);
#define List_1_set_Item_m21080(__this, ___index, ___value, method) (( void (*) (List_1_t806 *, int32_t, Matrix4x4_t997 , MethodInfo*))List_1_set_Item_m21080_gshared)(__this, ___index, ___value, method)
