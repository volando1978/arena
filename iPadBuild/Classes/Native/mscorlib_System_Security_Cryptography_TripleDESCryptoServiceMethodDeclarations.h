﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.TripleDESCryptoServiceProvider
struct TripleDESCryptoServiceProvider_t2720;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t2241;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::.ctor()
extern "C" void TripleDESCryptoServiceProvider__ctor_m13204 (TripleDESCryptoServiceProvider_t2720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateIV()
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m13205 (TripleDESCryptoServiceProvider_t2720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateKey()
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m13206 (TripleDESCryptoServiceProvider_t2720 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern "C" Object_t * TripleDESCryptoServiceProvider_CreateDecryptor_m13207 (TripleDESCryptoServiceProvider_t2720 * __this, ByteU5BU5D_t350* ___rgbKey, ByteU5BU5D_t350* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern "C" Object_t * TripleDESCryptoServiceProvider_CreateEncryptor_m13208 (TripleDESCryptoServiceProvider_t2720 * __this, ByteU5BU5D_t350* ___rgbKey, ByteU5BU5D_t350* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
