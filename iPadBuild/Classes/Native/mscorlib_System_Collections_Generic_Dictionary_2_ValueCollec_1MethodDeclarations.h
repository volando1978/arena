﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct ValueCollection_t921;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Dictionary_2_t575;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IEnumerator_1_t925;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
struct MultiplayerParticipantU5BU5D_t3739;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"
#define ValueCollection__ctor_m19987(__this, ___dictionary, method) (( void (*) (ValueCollection_t921 *, Dictionary_2_t575 *, MethodInfo*))ValueCollection__ctor_m16162_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19988(__this, ___item, method) (( void (*) (ValueCollection_t921 *, MultiplayerParticipant_t672 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19989(__this, method) (( void (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19990(__this, ___item, method) (( bool (*) (ValueCollection_t921 *, MultiplayerParticipant_t672 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19991(__this, ___item, method) (( bool (*) (ValueCollection_t921 *, MultiplayerParticipant_t672 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19992(__this, method) (( Object_t* (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m19993(__this, ___array, ___index, method) (( void (*) (ValueCollection_t921 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19994(__this, method) (( Object_t * (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19995(__this, method) (( bool (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19996(__this, method) (( bool (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m19997(__this, method) (( Object_t * (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m19998(__this, ___array, ___index, method) (( void (*) (ValueCollection_t921 *, MultiplayerParticipantU5BU5D_t3739*, int32_t, MethodInfo*))ValueCollection_CopyTo_m16184_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3828(__this, method) (( Enumerator_t928  (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_GetEnumerator_m16186_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Count()
#define ValueCollection_get_Count_m19999(__this, method) (( int32_t (*) (ValueCollection_t921 *, MethodInfo*))ValueCollection_get_Count_m16188_gshared)(__this, method)
