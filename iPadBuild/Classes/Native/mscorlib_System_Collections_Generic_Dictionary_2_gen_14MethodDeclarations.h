﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct KeyCollection_t3802;
// System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct ValueCollection_t3803;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3758;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>[]
struct KeyValuePair_2U5BU5D_t4389;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>>
struct IEnumerator_1_t4390;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// System.Collections.Generic.Dictionary`2/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_26MethodDeclarations.h"
#define Dictionary_2__ctor_m3927(__this, method) (( void (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2__ctor_m20443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20444(__this, ___comparer, method) (( void (*) (Dictionary_2_t671 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m20445_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20446(__this, ___capacity, method) (( void (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2__ctor_m20447_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20448(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t671 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m20449_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m20450(__this, method) (( Object_t * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m20451_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20452(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t671 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20453_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20454(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t671 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20455_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20456(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t671 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20457_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20458(__this, ___key, method) (( void (*) (Dictionary_2_t671 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20459_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20460(__this, method) (( bool (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20462(__this, method) (( Object_t * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20464(__this, method) (( bool (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20466(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t671 *, KeyValuePair_2_t3801 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20468(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t671 *, KeyValuePair_2_t3801 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20470(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t671 *, KeyValuePair_2U5BU5D_t4389*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20472(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t671 *, KeyValuePair_2_t3801 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20474(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t671 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20475_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20476(__this, method) (( Object_t * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20478(__this, method) (( Object_t* (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20480(__this, method) (( Object_t * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
#define Dictionary_2_get_Count_m20482(__this, method) (( int32_t (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_get_Count_m20483_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Item(TKey)
#define Dictionary_2_get_Item_m20484(__this, ___key, method) (( int32_t (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2_get_Item_m20485_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20486(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t671 *, int32_t, int32_t, MethodInfo*))Dictionary_2_set_Item_m20487_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20488(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t671 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m20489_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20490(__this, ___size, method) (( void (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m20491_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20492(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t671 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m20493_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20494(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3801  (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_make_pair_m20495_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20496(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_pick_key_m20497_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20498(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_pick_value_m20499_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20500(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t671 *, KeyValuePair_2U5BU5D_t4389*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m20501_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Resize()
#define Dictionary_2_Resize_m20502(__this, method) (( void (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_Resize_m20503_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Add(TKey,TValue)
#define Dictionary_2_Add_m20504(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t671 *, int32_t, int32_t, MethodInfo*))Dictionary_2_Add_m20505_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Clear()
#define Dictionary_2_Clear_m20506(__this, method) (( void (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_Clear_m20507_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20508(__this, ___key, method) (( bool (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2_ContainsKey_m20509_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20510(__this, ___value, method) (( bool (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2_ContainsValue_m20511_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20512(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t671 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m20513_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20514(__this, ___sender, method) (( void (*) (Dictionary_2_t671 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m20515_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::Remove(TKey)
#define Dictionary_2_Remove_m20516(__this, ___key, method) (( bool (*) (Dictionary_2_t671 *, int32_t, MethodInfo*))Dictionary_2_Remove_m20517_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20518(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t671 *, int32_t, int32_t*, MethodInfo*))Dictionary_2_TryGetValue_m20519_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Keys()
#define Dictionary_2_get_Keys_m20520(__this, method) (( KeyCollection_t3802 * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_get_Keys_m20521_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Values()
#define Dictionary_2_get_Values_m20522(__this, method) (( ValueCollection_t3803 * (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_get_Values_m20523_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20524(__this, ___key, method) (( int32_t (*) (Dictionary_2_t671 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m20525_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20526(__this, ___value, method) (( int32_t (*) (Dictionary_2_t671 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m20527_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20528(__this, ___pair, method) (( bool (*) (Dictionary_2_t671 *, KeyValuePair_2_t3801 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20529_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20530(__this, method) (( Enumerator_t3804  (*) (Dictionary_2_t671 *, MethodInfo*))Dictionary_2_GetEnumerator_m20531_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20532(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20533_gshared)(__this /* static, unused */, ___key, ___value, method)
