﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Reward>
struct Dictionary_2_t58;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Reward>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Reward>
struct  Enumerator_t3527 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Reward>::dictionary
	Dictionary_2_t58 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Reward>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Reward>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Reward>::current
	KeyValuePair_2_t3525  ___current_3;
};
