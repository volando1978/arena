﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
struct  LeaderboardTimeSpan_t511 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan::value__
	int32_t ___value___1;
};
