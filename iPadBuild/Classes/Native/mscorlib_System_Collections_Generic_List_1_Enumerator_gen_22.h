﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct List_1_t931;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct  Enumerator_t3694 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::l
	List_1_t931 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::current
	Object_t * ___current_3;
};
