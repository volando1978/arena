﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.IOS_ErrorCodes
struct IOS_ErrorCodes_t81;

// System.Void Soomla.Store.IOS_ErrorCodes::.cctor()
extern "C" void IOS_ErrorCodes__cctor_m304 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.IOS_ErrorCodes::CheckAndThrowException(System.Int32)
extern "C" void IOS_ErrorCodes_CheckAndThrowException_m305 (Object_t * __this /* static, unused */, int32_t ___error, MethodInfo* method) IL2CPP_METHOD_ATTR;
