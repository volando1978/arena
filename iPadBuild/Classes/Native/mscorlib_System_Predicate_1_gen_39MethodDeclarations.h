﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3965;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m22920_gshared (Predicate_1_t3965 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Predicate_1__ctor_m22920(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3965 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m22920_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m22921_gshared (Predicate_1_t3965 * __this, UIVertex_t1265  ___obj, MethodInfo* method);
#define Predicate_1_Invoke_m22921(__this, ___obj, method) (( bool (*) (Predicate_1_t3965 *, UIVertex_t1265 , MethodInfo*))Predicate_1_Invoke_m22921_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m22922_gshared (Predicate_1_t3965 * __this, UIVertex_t1265  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Predicate_1_BeginInvoke_m22922(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3965 *, UIVertex_t1265 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m22922_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m22923_gshared (Predicate_1_t3965 * __this, Object_t * ___result, MethodInfo* method);
#define Predicate_1_EndInvoke_m22923(__this, ___result, method) (( bool (*) (Predicate_1_t3965 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m22923_gshared)(__this, ___result, method)
