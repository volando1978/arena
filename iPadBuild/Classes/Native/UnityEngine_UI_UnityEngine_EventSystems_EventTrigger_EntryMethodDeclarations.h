﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t1159;

// System.Void UnityEngine.EventSystems.EventTrigger/Entry::.ctor()
extern "C" void Entry__ctor_m4583 (Entry_t1159 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
