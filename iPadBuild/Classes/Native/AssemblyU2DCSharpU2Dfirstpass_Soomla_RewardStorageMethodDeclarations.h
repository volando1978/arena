﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.RewardStorage
struct RewardStorage_t4;
// Soomla.Reward
struct Reward_t55;
// Soomla.SequenceReward
struct SequenceReward_t60;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Soomla.RewardStorage::.ctor()
extern "C" void RewardStorage__ctor_m207 (RewardStorage_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::.cctor()
extern "C" void RewardStorage__cctor_m208 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.RewardStorage Soomla.RewardStorage::get_instance()
extern "C" RewardStorage_t4 * RewardStorage_get_instance_m209 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::SetRewardStatus(Soomla.Reward,System.Boolean)
extern "C" void RewardStorage_SetRewardStatus_m210 (Object_t * __this /* static, unused */, Reward_t55 * ___reward, bool ___give, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::SetRewardStatus(Soomla.Reward,System.Boolean,System.Boolean)
extern "C" void RewardStorage_SetRewardStatus_m211 (Object_t * __this /* static, unused */, Reward_t55 * ___reward, bool ___give, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.RewardStorage::IsRewardGiven(Soomla.Reward)
extern "C" bool RewardStorage_IsRewardGiven_m212 (Object_t * __this /* static, unused */, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorage::GetTimesGiven(Soomla.Reward)
extern "C" int32_t RewardStorage_GetTimesGiven_m213 (Object_t * __this /* static, unused */, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Soomla.RewardStorage::GetLastGivenTime(Soomla.Reward)
extern "C" DateTime_t48  RewardStorage_GetLastGivenTime_m214 (Object_t * __this /* static, unused */, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorage::GetLastSeqIdxGiven(Soomla.SequenceReward)
extern "C" int32_t RewardStorage_GetLastSeqIdxGiven_m215 (Object_t * __this /* static, unused */, SequenceReward_t60 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::SetLastSeqIdxGiven(Soomla.SequenceReward,System.Int32)
extern "C" void RewardStorage_SetLastSeqIdxGiven_m216 (Object_t * __this /* static, unused */, SequenceReward_t60 * ___reward, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorage::_getLastSeqIdxGiven(Soomla.SequenceReward)
extern "C" int32_t RewardStorage__getLastSeqIdxGiven_m217 (RewardStorage_t4 * __this, SequenceReward_t60 * ___seqReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::_setLastSeqIdxGiven(Soomla.SequenceReward,System.Int32)
extern "C" void RewardStorage__setLastSeqIdxGiven_m218 (RewardStorage_t4 * __this, SequenceReward_t60 * ___seqReward, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RewardStorage::_setTimesGiven(Soomla.Reward,System.Boolean,System.Boolean)
extern "C" void RewardStorage__setTimesGiven_m219 (RewardStorage_t4 * __this, Reward_t55 * ___reward, bool ___up, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.RewardStorage::_getTimesGiven(Soomla.Reward)
extern "C" int32_t RewardStorage__getTimesGiven_m220 (RewardStorage_t4 * __this, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Soomla.RewardStorage::_getLastGivenTime(Soomla.Reward)
extern "C" DateTime_t48  RewardStorage__getLastGivenTime_m221 (RewardStorage_t4 * __this, Reward_t55 * ___reward, MethodInfo* method) IL2CPP_METHOD_ATTR;
