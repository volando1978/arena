﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreInfo
struct StoreInfo_t67;
// Soomla.Store.IStoreAssets
struct IStoreAssets_t176;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.String
struct String_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// System.Collections.Generic.List`1<Soomla.Store.VirtualItem>
struct List_1_t179;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.StoreInfo::.ctor()
extern "C" void StoreInfo__ctor_m516 (StoreInfo_t67 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::.cctor()
extern "C" void StoreInfo__cctor_m517 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.StoreInfo Soomla.Store.StoreInfo::get_instance()
extern "C" StoreInfo_t67 * StoreInfo_get_instance_m518 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::SetStoreAssets(Soomla.Store.IStoreAssets)
extern "C" void StoreInfo_SetStoreAssets_m519 (Object_t * __this /* static, unused */, Object_t * ___storeAssets, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualItem Soomla.Store.StoreInfo::GetItemByItemId(System.String)
extern "C" VirtualItem_t125 * StoreInfo_GetItemByItemId_m520 (Object_t * __this /* static, unused */, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.PurchasableVirtualItem Soomla.Store.StoreInfo::GetPurchasableItemWithProductId(System.String)
extern "C" PurchasableVirtualItem_t124 * StoreInfo_GetPurchasableItemWithProductId_m521 (Object_t * __this /* static, unused */, String_t* ___productId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualCategory Soomla.Store.StoreInfo::GetCategoryForVirtualGood(System.String)
extern "C" VirtualCategory_t126 * StoreInfo_GetCategoryForVirtualGood_m522 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.UpgradeVG Soomla.Store.StoreInfo::GetFirstUpgradeForVirtualGood(System.String)
extern "C" UpgradeVG_t133 * StoreInfo_GetFirstUpgradeForVirtualGood_m523 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.UpgradeVG Soomla.Store.StoreInfo::GetLastUpgradeForVirtualGood(System.String)
extern "C" UpgradeVG_t133 * StoreInfo_GetLastUpgradeForVirtualGood_m524 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG> Soomla.Store.StoreInfo::GetUpgradesForVirtualGood(System.String)
extern "C" List_1_t178 * StoreInfo_GetUpgradesForVirtualGood_m525 (Object_t * __this /* static, unused */, String_t* ___goodItemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::Save()
extern "C" void StoreInfo_Save_m526 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::Save(Soomla.Store.VirtualItem,System.Boolean)
extern "C" void StoreInfo_Save_m527 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___virtualItem, bool ___saveToDB, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::Save(System.Collections.Generic.List`1<Soomla.Store.VirtualItem>,System.Boolean)
extern "C" void StoreInfo_Save_m528 (Object_t * __this /* static, unused */, List_1_t179 * ___virtualItems, bool ___saveToDB, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::_setStoreAssets(Soomla.Store.IStoreAssets)
extern "C" void StoreInfo__setStoreAssets_m529 (StoreInfo_t67 * __this, Object_t * ___storeAssets, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::loadNativeFromDB()
extern "C" void StoreInfo_loadNativeFromDB_m530 (StoreInfo_t67 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.StoreInfo::IStoreAssetsToJSON(Soomla.Store.IStoreAssets)
extern "C" String_t* StoreInfo_IStoreAssetsToJSON_m531 (Object_t * __this /* static, unused */, Object_t * ___storeAssets, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::initializeFromDB()
extern "C" void StoreInfo_initializeFromDB_m532 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::fromJSONObject(JSONObject)
extern "C" void StoreInfo_fromJSONObject_m533 (Object_t * __this /* static, unused */, JSONObject_t30 * ___storeJSON, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::updateAggregatedLists()
extern "C" void StoreInfo_updateAggregatedLists_m534 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfo::replaceVirtualItem(Soomla.Store.VirtualItem)
extern "C" void StoreInfo_replaceVirtualItem_m535 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___virtualItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.StoreInfo::toJSONObject()
extern "C" JSONObject_t30 * StoreInfo_toJSONObject_m536 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.StoreInfo::keyMetaStoreInfo()
extern "C" String_t* StoreInfo_keyMetaStoreInfo_m537 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreInfo::<GetFirstUpgradeForVirtualGood>m__21(Soomla.Store.UpgradeVG)
extern "C" bool StoreInfo_U3CGetFirstUpgradeForVirtualGoodU3Em__21_m538 (Object_t * __this /* static, unused */, UpgradeVG_t133 * ___up, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreInfo::<GetLastUpgradeForVirtualGood>m__22(Soomla.Store.UpgradeVG)
extern "C" bool StoreInfo_U3CGetLastUpgradeForVirtualGoodU3Em__22_m539 (Object_t * __this /* static, unused */, UpgradeVG_t133 * ___up, MethodInfo* method) IL2CPP_METHOD_ATTR;
