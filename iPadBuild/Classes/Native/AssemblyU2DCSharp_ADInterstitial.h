﻿#pragma once
#include <stdint.h>
// UnityEngine.iOS.ADInterstitialAd
struct ADInterstitialAd_t331;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ADInterstitial
struct  ADInterstitial_t332  : public MonoBehaviour_t26
{
};
struct ADInterstitial_t332_StaticFields{
	// UnityEngine.iOS.ADInterstitialAd ADInterstitial::fullscreenAd
	ADInterstitialAd_t331 * ___fullscreenAd_2;
};
