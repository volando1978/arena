﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct MethodCallDictionary_t2614;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t2616;

// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern "C" void MethodCallDictionary__ctor_m12617 (MethodCallDictionary_t2614 * __this, Object_t * ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
extern "C" void MethodCallDictionary__cctor_m12618 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
