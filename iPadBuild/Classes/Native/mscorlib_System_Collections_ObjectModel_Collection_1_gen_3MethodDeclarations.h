﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t3853;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1284;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4406;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t3851;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m21374_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1__ctor_m21374(__this, method) (( void (*) (Collection_1_t3853 *, MethodInfo*))Collection_1__ctor_m21374_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21375_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21375(__this, method) (( bool (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21375_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21376_gshared (Collection_1_t3853 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m21376(__this, ___array, ___index, method) (( void (*) (Collection_1_t3853 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m21376_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m21377_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m21377(__this, method) (( Object_t * (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m21377_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m21378_gshared (Collection_1_t3853 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m21378(__this, ___value, method) (( int32_t (*) (Collection_1_t3853 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m21378_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m21379_gshared (Collection_1_t3853 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m21379(__this, ___value, method) (( bool (*) (Collection_1_t3853 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m21379_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m21380_gshared (Collection_1_t3853 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m21380(__this, ___value, method) (( int32_t (*) (Collection_1_t3853 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m21380_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m21381_gshared (Collection_1_t3853 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m21381(__this, ___index, ___value, method) (( void (*) (Collection_1_t3853 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m21381_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m21382_gshared (Collection_1_t3853 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m21382(__this, ___value, method) (( void (*) (Collection_1_t3853 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m21382_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m21383_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m21383(__this, method) (( bool (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m21383_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m21384_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m21384(__this, method) (( Object_t * (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m21384_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m21385_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m21385(__this, method) (( bool (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m21385_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m21386_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m21386(__this, method) (( bool (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m21386_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m21387_gshared (Collection_1_t3853 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m21387(__this, ___index, method) (( Object_t * (*) (Collection_1_t3853 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m21387_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m21388_gshared (Collection_1_t3853 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m21388(__this, ___index, ___value, method) (( void (*) (Collection_1_t3853 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m21388_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m21389_gshared (Collection_1_t3853 * __this, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_Add_m21389(__this, ___item, method) (( void (*) (Collection_1_t3853 *, Vector3_t758 , MethodInfo*))Collection_1_Add_m21389_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m21390_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_Clear_m21390(__this, method) (( void (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_Clear_m21390_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m21391_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m21391(__this, method) (( void (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_ClearItems_m21391_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m21392_gshared (Collection_1_t3853 * __this, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_Contains_m21392(__this, ___item, method) (( bool (*) (Collection_1_t3853 *, Vector3_t758 , MethodInfo*))Collection_1_Contains_m21392_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m21393_gshared (Collection_1_t3853 * __this, Vector3U5BU5D_t1284* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m21393(__this, ___array, ___index, method) (( void (*) (Collection_1_t3853 *, Vector3U5BU5D_t1284*, int32_t, MethodInfo*))Collection_1_CopyTo_m21393_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m21394_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m21394(__this, method) (( Object_t* (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_GetEnumerator_m21394_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m21395_gshared (Collection_1_t3853 * __this, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m21395(__this, ___item, method) (( int32_t (*) (Collection_1_t3853 *, Vector3_t758 , MethodInfo*))Collection_1_IndexOf_m21395_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m21396_gshared (Collection_1_t3853 * __this, int32_t ___index, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_Insert_m21396(__this, ___index, ___item, method) (( void (*) (Collection_1_t3853 *, int32_t, Vector3_t758 , MethodInfo*))Collection_1_Insert_m21396_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m21397_gshared (Collection_1_t3853 * __this, int32_t ___index, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m21397(__this, ___index, ___item, method) (( void (*) (Collection_1_t3853 *, int32_t, Vector3_t758 , MethodInfo*))Collection_1_InsertItem_m21397_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m21398_gshared (Collection_1_t3853 * __this, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_Remove_m21398(__this, ___item, method) (( bool (*) (Collection_1_t3853 *, Vector3_t758 , MethodInfo*))Collection_1_Remove_m21398_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m21399_gshared (Collection_1_t3853 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m21399(__this, ___index, method) (( void (*) (Collection_1_t3853 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m21399_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m21400_gshared (Collection_1_t3853 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m21400(__this, ___index, method) (( void (*) (Collection_1_t3853 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m21400_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m21401_gshared (Collection_1_t3853 * __this, MethodInfo* method);
#define Collection_1_get_Count_m21401(__this, method) (( int32_t (*) (Collection_1_t3853 *, MethodInfo*))Collection_1_get_Count_m21401_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t758  Collection_1_get_Item_m21402_gshared (Collection_1_t3853 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m21402(__this, ___index, method) (( Vector3_t758  (*) (Collection_1_t3853 *, int32_t, MethodInfo*))Collection_1_get_Item_m21402_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m21403_gshared (Collection_1_t3853 * __this, int32_t ___index, Vector3_t758  ___value, MethodInfo* method);
#define Collection_1_set_Item_m21403(__this, ___index, ___value, method) (( void (*) (Collection_1_t3853 *, int32_t, Vector3_t758 , MethodInfo*))Collection_1_set_Item_m21403_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m21404_gshared (Collection_1_t3853 * __this, int32_t ___index, Vector3_t758  ___item, MethodInfo* method);
#define Collection_1_SetItem_m21404(__this, ___index, ___item, method) (( void (*) (Collection_1_t3853 *, int32_t, Vector3_t758 , MethodInfo*))Collection_1_SetItem_m21404_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m21405_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m21405(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m21405_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t758  Collection_1_ConvertItem_m21406_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m21406(__this /* static, unused */, ___item, method) (( Vector3_t758  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m21406_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m21407_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m21407(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m21407_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m21408_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m21408(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m21408_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m21409_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m21409(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m21409_gshared)(__this /* static, unused */, ___list, method)
