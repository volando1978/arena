﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DESCryptoServiceProvider
struct DESCryptoServiceProvider_t2687;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t2241;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.DESCryptoServiceProvider::.ctor()
extern "C" void DESCryptoServiceProvider__ctor_m12935 (DESCryptoServiceProvider_t2687 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern "C" Object_t * DESCryptoServiceProvider_CreateDecryptor_m12936 (DESCryptoServiceProvider_t2687 * __this, ByteU5BU5D_t350* ___rgbKey, ByteU5BU5D_t350* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.DESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern "C" Object_t * DESCryptoServiceProvider_CreateEncryptor_m12937 (DESCryptoServiceProvider_t2687 * __this, ByteU5BU5D_t350* ___rgbKey, ByteU5BU5D_t350* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateIV()
extern "C" void DESCryptoServiceProvider_GenerateIV_m12938 (DESCryptoServiceProvider_t2687 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DESCryptoServiceProvider::GenerateKey()
extern "C" void DESCryptoServiceProvider_GenerateKey_m12939 (DESCryptoServiceProvider_t2687 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
