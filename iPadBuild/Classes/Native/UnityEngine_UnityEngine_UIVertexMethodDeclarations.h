﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UIVertex
struct UIVertex_t1265;

// System.Void UnityEngine.UIVertex::.cctor()
extern "C" void UIVertex__cctor_m7290 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
