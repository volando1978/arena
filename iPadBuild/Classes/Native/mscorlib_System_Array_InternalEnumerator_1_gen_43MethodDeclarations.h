﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>
struct InternalEnumerator_1_t4063;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1627;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m24219(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4063 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24220(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4063 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>::Dispose()
#define InternalEnumerator_1_Dispose_m24221(__this, method) (( void (*) (InternalEnumerator_1_t4063 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24222(__this, method) (( bool (*) (InternalEnumerator_1_t4063 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.Score>::get_Current()
#define InternalEnumerator_1_get_Current_m24223(__this, method) (( Score_t1627 * (*) (InternalEnumerator_1_t4063 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
