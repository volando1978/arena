﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2727;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<System.Security.Policy.StrongName>
struct  Action_1_t4243  : public MulticastDelegate_t22
{
};
