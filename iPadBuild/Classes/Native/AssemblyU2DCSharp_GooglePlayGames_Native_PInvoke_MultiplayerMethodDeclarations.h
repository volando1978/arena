﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.MultiplayerInvitation::.ctor(System.IntPtr)
extern "C" void MultiplayerInvitation__ctor_m2699 (MultiplayerInvitation_t601 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Inviter()
extern "C" MultiplayerParticipant_t672 * MultiplayerInvitation_Inviter_m2700 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Variant()
extern "C" uint32_t MultiplayerInvitation_Variant_m2701 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Type()
extern "C" int32_t MultiplayerInvitation_Type_m2702 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.MultiplayerInvitation::Id()
extern "C" String_t* MultiplayerInvitation_Id_m2703 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerInvitation::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerInvitation_CallDispose_m2704 (MultiplayerInvitation_t601 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::AutomatchingSlots()
extern "C" uint32_t MultiplayerInvitation_AutomatchingSlots_m2705 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.MultiplayerInvitation::ParticipantCount()
extern "C" uint32_t MultiplayerInvitation_ParticipantCount_m2706 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType GooglePlayGames.Native.PInvoke.MultiplayerInvitation::ToInvType(GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType)
extern "C" int32_t MultiplayerInvitation_ToInvType_m2707 (Object_t * __this /* static, unused */, int32_t ___invitationType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Invitation GooglePlayGames.Native.PInvoke.MultiplayerInvitation::AsInvitation()
extern "C" Invitation_t341 * MultiplayerInvitation_AsInvitation_m2708 (MultiplayerInvitation_t601 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.MultiplayerInvitation::FromPointer(System.IntPtr)
extern "C" MultiplayerInvitation_t601 * MultiplayerInvitation_FromPointer_m2709 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerInvitation::<Id>m__7B(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerInvitation_U3CIdU3Em__7B_m2710 (MultiplayerInvitation_t601 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
