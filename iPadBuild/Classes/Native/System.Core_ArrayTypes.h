﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct  LinkU5BU5D_t3578  : public Array_t
{
};
// System.Collections.Generic.HashSet`1/Link<System.String>[]
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct  LinkU5BU5D_t3576  : public Array_t
{
};
// System.Action[]
// System.Action[]
struct  ActionU5BU5D_t3700  : public Array_t
{
};
// System.Collections.Generic.HashSet`1/Link<System.Int32>[]
// System.Collections.Generic.HashSet`1/Link<System.Int32>[]
struct  LinkU5BU5D_t3759  : public Array_t
{
};
// System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>[]
// System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>[]
struct  LinkU5BU5D_t3755  : public Array_t
{
};
