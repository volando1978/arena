﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.OurUtils.Logger
struct Logger_t390;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.OurUtils.Logger::.ctor()
extern "C" void Logger__ctor_m1585 (Logger_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::.cctor()
extern "C" void Logger__cctor_m1586 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.OurUtils.Logger::get_DebugLogEnabled()
extern "C" bool Logger_get_DebugLogEnabled_m1587 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::set_DebugLogEnabled(System.Boolean)
extern "C" void Logger_set_DebugLogEnabled_m1588 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.OurUtils.Logger::get_WarningLogEnabled()
extern "C" bool Logger_get_WarningLogEnabled_m1589 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::set_WarningLogEnabled(System.Boolean)
extern "C" void Logger_set_WarningLogEnabled_m1590 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::d(System.String)
extern "C" void Logger_d_m1591 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::w(System.String)
extern "C" void Logger_w_m1592 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.OurUtils.Logger::e(System.String)
extern "C" void Logger_e_m1593 (Object_t * __this /* static, unused */, String_t* ___msg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.OurUtils.Logger::describe(System.Byte[])
extern "C" String_t* Logger_describe_m1594 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.OurUtils.Logger::ToLogMessage(System.String,System.String,System.String)
extern "C" String_t* Logger_ToLogMessage_m1595 (Object_t * __this /* static, unused */, String_t* ___prefix, String_t* ___logType, String_t* ___msg, MethodInfo* method) IL2CPP_METHOD_ATTR;
