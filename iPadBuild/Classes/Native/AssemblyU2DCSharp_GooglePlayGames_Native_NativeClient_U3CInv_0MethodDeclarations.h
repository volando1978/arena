﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720;

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>::.ctor()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19732_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19732(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19732_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Object>::<>m__F()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19733_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19733(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3720 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19733_gshared)(__this, method)
