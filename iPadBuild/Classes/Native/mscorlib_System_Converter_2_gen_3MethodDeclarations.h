﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Converter_2_t3405;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
// System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Converter_2_gen_4MethodDeclarations.h"
#define Converter_2__ctor_m15609(__this, ___object, ___method, method) (( void (*) (Converter_2_t3405 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15610_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(TInput)
#define Converter_2_Invoke_m15611(__this, ___input, method) (( KeyValuePair_2_t3407  (*) (Converter_2_t3405 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))Converter_2_Invoke_m15612_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m15613(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3405 *, UnityKeyValuePair_2_t3412 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15614_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m15615(__this, ___result, method) (( KeyValuePair_2_t3407  (*) (Converter_2_t3405 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15616_gshared)(__this, ___result, method)
