﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
struct InternalEnumerator_1_t4005;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23510_gshared (InternalEnumerator_1_t4005 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m23510(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4005 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m23510_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23511_gshared (InternalEnumerator_1_t4005 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23511(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4005 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23511_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23512_gshared (InternalEnumerator_1_t4005 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23512(__this, method) (( void (*) (InternalEnumerator_1_t4005 *, MethodInfo*))InternalEnumerator_1_Dispose_m23512_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23513_gshared (InternalEnumerator_1_t4005 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23513(__this, method) (( bool (*) (InternalEnumerator_1_t4005 *, MethodInfo*))InternalEnumerator_1_MoveNext_m23513_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t739  InternalEnumerator_1_get_Current_m23514_gshared (InternalEnumerator_1_t4005 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23514(__this, method) (( Vector2_t739  (*) (InternalEnumerator_1_t4005 *, MethodInfo*))InternalEnumerator_1_get_Current_m23514_gshared)(__this, method)
