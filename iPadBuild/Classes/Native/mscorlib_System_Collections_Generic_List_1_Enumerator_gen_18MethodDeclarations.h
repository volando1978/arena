﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>
struct Enumerator_t3562;
// System.Object
struct Object_t;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m17349(__this, ___l, method) (( void (*) (Enumerator_t3562 *, List_1_t177 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17350(__this, method) (( Object_t * (*) (Enumerator_t3562 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::Dispose()
#define Enumerator_Dispose_m17351(__this, method) (( void (*) (Enumerator_t3562 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::VerifyState()
#define Enumerator_VerifyState_m17352(__this, method) (( void (*) (Enumerator_t3562 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::MoveNext()
#define Enumerator_MoveNext_m17353(__this, method) (( bool (*) (Enumerator_t3562 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::get_Current()
#define Enumerator_get_Current_m17354(__this, method) (( MarketItem_t122 * (*) (Enumerator_t3562 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
