﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct  KeyCollection_t3802  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::dictionary
	Dictionary_2_t671 * ___dictionary_0;
};
