﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WWW
struct WWW_t383;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t1666;
// System.Byte[]
struct ByteU5BU5D_t350;
// UnityEngine.Texture2D
struct Texture2D_t384;
// UnityEngine.AudioClip
struct AudioClip_t753;
// UnityEngine.AssetBundle
struct AssetBundle_t1487;
// UnityEngine.WWWForm
struct WWWForm_t171;
// System.Collections.Hashtable
struct Hashtable_t1667;
// System.String[]
struct StringU5BU5D_t169;
// UnityEngine.ThreadPriority
#include "UnityEngine_UnityEngine_ThreadPriority.h"
// UnityEngine.Hash128
#include "UnityEngine_UnityEngine_Hash128.h"
// UnityEngine.AudioType
#include "UnityEngine_UnityEngine_AudioType.h"

// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C" void WWW__ctor_m3718 (WWW_t383 * __this, String_t* ___url, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C" void WWW__ctor_m6894 (WWW_t383 * __this, String_t* ___url, WWWForm_t171 * ___form, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[])
extern "C" void WWW__ctor_m6895 (WWW_t383 * __this, String_t* ___url, ByteU5BU5D_t350* ___postData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Hashtable)
extern "C" void WWW__ctor_m6896 (WWW_t383 * __this, String_t* ___url, ByteU5BU5D_t350* ___postData, Hashtable_t1667 * ___headers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void WWW__ctor_m6897 (WWW_t383 * __this, String_t* ___url, ByteU5BU5D_t350* ___postData, Dictionary_2_t165 * ___headers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.Hash128,System.UInt32)
extern "C" void WWW__ctor_m6898 (WWW_t383 * __this, String_t* ___url, Hash128_t1549  ___hash, uint32_t ___crc, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::Dispose()
extern "C" void WWW_Dispose_m6899 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::Finalize()
extern "C" void WWW_Finalize_m6900 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C" void WWW_DestroyWWW_m6901 (WWW_t383 * __this, bool ___cancel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C" void WWW_InitWWW_m6902 (WWW_t383 * __this, String_t* ___url, ByteU5BU5D_t350* ___postData, StringU5BU5D_t169* ___iHeaders, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWW::enforceWebSecurityRestrictions()
extern "C" bool WWW_enforceWebSecurityRestrictions_m6903 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::EscapeURL(System.String)
extern "C" String_t* WWW_EscapeURL_m6904 (Object_t * __this /* static, unused */, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::EscapeURL(System.String,System.Text.Encoding)
extern "C" String_t* WWW_EscapeURL_m6905 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::UnEscapeURL(System.String)
extern "C" String_t* WWW_UnEscapeURL_m6906 (Object_t * __this /* static, unused */, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::UnEscapeURL(System.String,System.Text.Encoding)
extern "C" String_t* WWW_UnEscapeURL_m6907 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t1666 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern "C" Dictionary_2_t165 * WWW_get_responseHeaders_m6908 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_responseHeadersString()
extern "C" String_t* WWW_get_responseHeadersString_m6909 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_text()
extern "C" String_t* WWW_get_text_m6910 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern "C" Encoding_t1666 * WWW_get_DefaultEncoding_m6911 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding UnityEngine.WWW::GetTextEncoder()
extern "C" Encoding_t1666 * WWW_GetTextEncoder_m6912 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_data()
extern "C" String_t* WWW_get_data_m6913 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C" ByteU5BU5D_t350* WWW_get_bytes_m6914 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.WWW::get_size()
extern "C" int32_t WWW_get_size_m6915 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_error()
extern "C" String_t* WWW_get_error_m6916 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
extern "C" Texture2D_t384 * WWW_GetTexture_m6917 (WWW_t383 * __this, bool ___markNonReadable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C" Texture2D_t384 * WWW_get_texture_m3721 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::get_textureNonReadable()
extern "C" Texture2D_t384 * WWW_get_textureNonReadable_m6918 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::get_audioClip()
extern "C" AudioClip_t753 * WWW_get_audioClip_m6919 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean)
extern "C" AudioClip_t753 * WWW_GetAudioClip_m6920 (WWW_t383 * __this, bool ___threeD, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean)
extern "C" AudioClip_t753 * WWW_GetAudioClip_m6921 (WWW_t383 * __this, bool ___threeD, bool ___stream, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean,UnityEngine.AudioType)
extern "C" AudioClip_t753 * WWW_GetAudioClip_m6922 (WWW_t383 * __this, bool ___threeD, bool ___stream, int32_t ___audioType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipCompressed()
extern "C" AudioClip_t753 * WWW_GetAudioClipCompressed_m6923 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipCompressed(System.Boolean)
extern "C" AudioClip_t753 * WWW_GetAudioClipCompressed_m6924 (WWW_t383 * __this, bool ___threeD, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipCompressed(System.Boolean,UnityEngine.AudioType)
extern "C" AudioClip_t753 * WWW_GetAudioClipCompressed_m6925 (WWW_t383 * __this, bool ___threeD, int32_t ___audioType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern "C" AudioClip_t753 * WWW_GetAudioClipInternal_m6926 (WWW_t383 * __this, bool ___threeD, bool ___stream, bool ___compressed, int32_t ___audioType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::LoadImageIntoTexture(UnityEngine.Texture2D)
extern "C" void WWW_LoadImageIntoTexture_m6927 (WWW_t383 * __this, Texture2D_t384 * ___tex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C" bool WWW_get_isDone_m3720 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::GetURL(System.String)
extern "C" String_t* WWW_GetURL_m6928 (Object_t * __this /* static, unused */, String_t* ___url, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.WWW::GetTextureFromURL(System.String)
extern "C" Texture2D_t384 * WWW_GetTextureFromURL_m6929 (Object_t * __this /* static, unused */, String_t* ___url, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WWW::get_progress()
extern "C" float WWW_get_progress_m6930 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WWW::get_uploadProgress()
extern "C" float WWW_get_uploadProgress_m6931 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.WWW::get_bytesDownloaded()
extern "C" int32_t WWW_get_bytesDownloaded_m6932 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip UnityEngine.WWW::get_oggVorbis()
extern "C" AudioClip_t753 * WWW_get_oggVorbis_m6933 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::LoadUnityWeb()
extern "C" void WWW_LoadUnityWeb_m6934 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_url()
extern "C" String_t* WWW_get_url_m3717 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.WWW::get_assetBundle()
extern "C" AssetBundle_t1487 * WWW_get_assetBundle_m6935 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ThreadPriority UnityEngine.WWW::get_threadPriority()
extern "C" int32_t WWW_get_threadPriority_m6936 (WWW_t383 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::set_threadPriority(UnityEngine.ThreadPriority)
extern "C" void WWW_set_threadPriority_m6937 (WWW_t383 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::INTERNAL_CALL_WWW(UnityEngine.WWW,System.String,UnityEngine.Hash128&,System.UInt32)
extern "C" void WWW_INTERNAL_CALL_WWW_m6938 (Object_t * __this /* static, unused */, WWW_t383 * ___self, String_t* ___url, Hash128_t1549 * ___hash, uint32_t ___crc, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32)
extern "C" WWW_t383 * WWW_LoadFromCacheOrDownload_m6939 (Object_t * __this /* static, unused */, String_t* ___url, int32_t ___version, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,System.Int32,System.UInt32)
extern "C" WWW_t383 * WWW_LoadFromCacheOrDownload_m6940 (Object_t * __this /* static, unused */, String_t* ___url, int32_t ___version, uint32_t ___crc, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128)
extern "C" WWW_t383 * WWW_LoadFromCacheOrDownload_m6941 (Object_t * __this /* static, unused */, String_t* ___url, Hash128_t1549  ___hash, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW UnityEngine.WWW::LoadFromCacheOrDownload(System.String,UnityEngine.Hash128,System.UInt32)
extern "C" WWW_t383 * WWW_LoadFromCacheOrDownload_m6942 (Object_t * __this /* static, unused */, String_t* ___url, Hash128_t1549  ___hash, uint32_t ___crc, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" StringU5BU5D_t169* WWW_FlattenedHeadersFrom_m6943 (Object_t * __this /* static, unused */, Dictionary_2_t165 * ___headers, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::ParseHTTPHeaderString(System.String)
extern "C" Dictionary_2_t165 * WWW_ParseHTTPHeaderString_m6944 (Object_t * __this /* static, unused */, String_t* ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
