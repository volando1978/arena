﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>
struct Enumerator_t3610;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct Dictionary_2_t111;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m18126(__this, ___dictionary, method) (( void (*) (Enumerator_t3610 *, Dictionary_2_t111 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18127(__this, method) (( Object_t * (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18128(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18129(__this, method) (( Object_t * (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18130(__this, method) (( Object_t * (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::MoveNext()
#define Enumerator_MoveNext_m18131(__this, method) (( bool (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::get_Current()
#define Enumerator_get_Current_m18132(__this, method) (( KeyValuePair_2_t3607  (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18133(__this, method) (( String_t* (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18134(__this, method) (( PurchasableVirtualItem_t124 * (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::VerifyState()
#define Enumerator_VerifyState_m18135(__this, method) (( void (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18136(__this, method) (( void (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>::Dispose()
#define Enumerator_Dispose_m18137(__this, method) (( void (*) (Enumerator_t3610 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
