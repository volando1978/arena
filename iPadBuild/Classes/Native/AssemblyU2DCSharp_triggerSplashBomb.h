﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// triggerSplashBomb
struct  triggerSplashBomb_t836  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject triggerSplashBomb::bombaSplash
	GameObject_t144 * ___bombaSplash_2;
	// UnityEngine.GameObject triggerSplashBomb::rayosbomba
	GameObject_t144 * ___rayosbomba_3;
	// UnityEngine.GameObject triggerSplashBomb::rayo
	GameObject_t144 * ___rayo_4;
	// UnityEngine.GameObject triggerSplashBomb::enemyController
	GameObject_t144 * ___enemyController_5;
	// System.Int32 triggerSplashBomb::timerRays
	int32_t ___timerRays_6;
};
