﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct  InvitationReceivedDelegate_t363  : public MulticastDelegate_t22
{
};
