﻿#pragma once
#include <stdint.h>
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t3830;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct  List_1_t806  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::_items
	Matrix4x4U5BU5D_t3830* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::_version
	int32_t ____version_3;
};
struct List_1_t806_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Matrix4x4>::EmptyArray
	Matrix4x4U5BU5D_t3830* ___EmptyArray_4;
};
