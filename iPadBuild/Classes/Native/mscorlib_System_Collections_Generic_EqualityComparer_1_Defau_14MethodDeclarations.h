﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t4251;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m26150_gshared (DefaultComparer_t4251 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26150(__this, method) (( void (*) (DefaultComparer_t4251 *, MethodInfo*))DefaultComparer__ctor_m26150_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26151_gshared (DefaultComparer_t4251 * __this, Guid_t2814  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m26151(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4251 *, Guid_t2814 , MethodInfo*))DefaultComparer_GetHashCode_m26151_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26152_gshared (DefaultComparer_t4251 * __this, Guid_t2814  ___x, Guid_t2814  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m26152(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4251 *, Guid_t2814 , Guid_t2814 , MethodInfo*))DefaultComparer_Equals_m26152_gshared)(__this, ___x, ___y, method)
