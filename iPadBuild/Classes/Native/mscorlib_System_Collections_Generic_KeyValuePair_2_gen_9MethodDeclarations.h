﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct KeyValuePair_2_t3607;
// System.String
struct String_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18092(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3607 *, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Key()
#define KeyValuePair_2_get_Key_m18093(__this, method) (( String_t* (*) (KeyValuePair_2_t3607 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18094(__this, ___value, method) (( void (*) (KeyValuePair_2_t3607 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Value()
#define KeyValuePair_2_get_Value_m18095(__this, method) (( PurchasableVirtualItem_t124 * (*) (KeyValuePair_2_t3607 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18096(__this, ___value, method) (( void (*) (KeyValuePair_2_t3607 *, PurchasableVirtualItem_t124 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>::ToString()
#define KeyValuePair_2_ToString_m18097(__this, method) (( String_t* (*) (KeyValuePair_2_t3607 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
