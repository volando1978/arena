﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Dictionary_2_t576;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct  KeyCollection_t3745  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::dictionary
	Dictionary_2_t576 * ___dictionary_0;
};
