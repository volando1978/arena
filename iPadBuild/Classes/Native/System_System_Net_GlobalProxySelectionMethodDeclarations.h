﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.GlobalProxySelection
struct GlobalProxySelection_t1995;
// System.Net.IWebProxy
struct IWebProxy_t1988;

// System.Net.IWebProxy System.Net.GlobalProxySelection::get_Select()
extern "C" Object_t * GlobalProxySelection_get_Select_m7967 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
