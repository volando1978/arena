﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3879;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3878;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" void ObjectPool_1__ctor_m21655_gshared (ObjectPool_1_t3879 * __this, UnityAction_1_t3878 * ___actionOnGet, UnityAction_1_t3878 * ___actionOnRelease, MethodInfo* method);
#define ObjectPool_1__ctor_m21655(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3879 *, UnityAction_1_t3878 *, UnityAction_1_t3878 *, MethodInfo*))ObjectPool_1__ctor_m21655_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" int32_t ObjectPool_1_get_countAll_m21657_gshared (ObjectPool_1_t3879 * __this, MethodInfo* method);
#define ObjectPool_1_get_countAll_m21657(__this, method) (( int32_t (*) (ObjectPool_1_t3879 *, MethodInfo*))ObjectPool_1_get_countAll_m21657_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" void ObjectPool_1_set_countAll_m21659_gshared (ObjectPool_1_t3879 * __this, int32_t ___value, MethodInfo* method);
#define ObjectPool_1_set_countAll_m21659(__this, ___value, method) (( void (*) (ObjectPool_1_t3879 *, int32_t, MethodInfo*))ObjectPool_1_set_countAll_m21659_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" int32_t ObjectPool_1_get_countActive_m21661_gshared (ObjectPool_1_t3879 * __this, MethodInfo* method);
#define ObjectPool_1_get_countActive_m21661(__this, method) (( int32_t (*) (ObjectPool_1_t3879 *, MethodInfo*))ObjectPool_1_get_countActive_m21661_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" int32_t ObjectPool_1_get_countInactive_m21663_gshared (ObjectPool_1_t3879 * __this, MethodInfo* method);
#define ObjectPool_1_get_countInactive_m21663(__this, method) (( int32_t (*) (ObjectPool_1_t3879 *, MethodInfo*))ObjectPool_1_get_countInactive_m21663_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" Object_t * ObjectPool_1_Get_m21665_gshared (ObjectPool_1_t3879 * __this, MethodInfo* method);
#define ObjectPool_1_Get_m21665(__this, method) (( Object_t * (*) (ObjectPool_1_t3879 *, MethodInfo*))ObjectPool_1_Get_m21665_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" void ObjectPool_1_Release_m21667_gshared (ObjectPool_1_t3879 * __this, Object_t * ___element, MethodInfo* method);
#define ObjectPool_1_Release_m21667(__this, ___element, method) (( void (*) (ObjectPool_1_t3879 *, Object_t *, MethodInfo*))ObjectPool_1_Release_m21667_gshared)(__this, ___element, method)
