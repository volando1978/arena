﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t2188_il2cpp_TypeInfo;
extern TypeInfo Locale_t2189_il2cpp_TypeInfo;
extern TypeInfo Sign_t2190_il2cpp_TypeInfo;
extern TypeInfo ModulusRing_t2192_il2cpp_TypeInfo;
extern TypeInfo Kernel_t2193_il2cpp_TypeInfo;
extern TypeInfo BigInteger_t2191_il2cpp_TypeInfo;
extern TypeInfo ConfidenceFactor_t2196_il2cpp_TypeInfo;
extern TypeInfo PrimalityTests_t2197_il2cpp_TypeInfo;
extern TypeInfo PrimeGeneratorBase_t2198_il2cpp_TypeInfo;
extern TypeInfo SequentialSearchPrimeGeneratorBase_t2199_il2cpp_TypeInfo;
extern TypeInfo ASN1_t2131_il2cpp_TypeInfo;
extern TypeInfo ASN1Convert_t2200_il2cpp_TypeInfo;
extern TypeInfo BitConverterLE_t2201_il2cpp_TypeInfo;
extern TypeInfo ContentInfo_t2202_il2cpp_TypeInfo;
extern TypeInfo EncryptedData_t2203_il2cpp_TypeInfo;
extern TypeInfo PKCS7_t2204_il2cpp_TypeInfo;
extern TypeInfo ARC4Managed_t2205_il2cpp_TypeInfo;
extern TypeInfo CryptoConvert_t2207_il2cpp_TypeInfo;
extern TypeInfo KeyBuilder_t2208_il2cpp_TypeInfo;
extern TypeInfo MD2_t2209_il2cpp_TypeInfo;
extern TypeInfo MD2Managed_t2211_il2cpp_TypeInfo;
extern TypeInfo PKCS1_t2212_il2cpp_TypeInfo;
extern TypeInfo PrivateKeyInfo_t2213_il2cpp_TypeInfo;
extern TypeInfo EncryptedPrivateKeyInfo_t2214_il2cpp_TypeInfo;
extern TypeInfo PKCS8_t2215_il2cpp_TypeInfo;
extern TypeInfo RC4_t2206_il2cpp_TypeInfo;
extern TypeInfo KeyGeneratedEventHandler_t2219_il2cpp_TypeInfo;
extern TypeInfo RSAManaged_t2146_il2cpp_TypeInfo;
extern TypeInfo SafeBag_t2220_il2cpp_TypeInfo;
extern TypeInfo DeriveBytes_t2221_il2cpp_TypeInfo;
extern TypeInfo PKCS12_t2152_il2cpp_TypeInfo;
extern TypeInfo X501_t2151_il2cpp_TypeInfo;
extern TypeInfo X509Certificate_t2024_il2cpp_TypeInfo;
extern TypeInfo X509CertificateEnumerator_t2158_il2cpp_TypeInfo;
extern TypeInfo X509CertificateCollection_t2153_il2cpp_TypeInfo;
extern TypeInfo X509Chain_t2222_il2cpp_TypeInfo;
extern TypeInfo X509ChainStatusFlags_t2223_il2cpp_TypeInfo;
extern TypeInfo X509CrlEntry_t2135_il2cpp_TypeInfo;
extern TypeInfo X509Crl_t2133_il2cpp_TypeInfo;
extern TypeInfo X509Extension_t2134_il2cpp_TypeInfo;
extern TypeInfo X509ExtensionCollection_t2156_il2cpp_TypeInfo;
extern TypeInfo X509Store_t2049_il2cpp_TypeInfo;
extern TypeInfo X509StoreManager_t2224_il2cpp_TypeInfo;
extern TypeInfo X509Stores_t2136_il2cpp_TypeInfo;
extern TypeInfo AuthorityKeyIdentifierExtension_t2157_il2cpp_TypeInfo;
extern TypeInfo BasicConstraintsExtension_t2225_il2cpp_TypeInfo;
extern TypeInfo ExtendedKeyUsageExtension_t2226_il2cpp_TypeInfo;
extern TypeInfo GeneralNames_t2227_il2cpp_TypeInfo;
extern TypeInfo KeyUsages_t2228_il2cpp_TypeInfo;
extern TypeInfo KeyUsageExtension_t2229_il2cpp_TypeInfo;
extern TypeInfo CertTypes_t2230_il2cpp_TypeInfo;
extern TypeInfo NetscapeCertTypeExtension_t2231_il2cpp_TypeInfo;
extern TypeInfo SubjectAltNameExtension_t2232_il2cpp_TypeInfo;
extern TypeInfo HMAC_t2233_il2cpp_TypeInfo;
extern TypeInfo MD5SHA1_t2235_il2cpp_TypeInfo;
extern TypeInfo AlertLevel_t2236_il2cpp_TypeInfo;
extern TypeInfo AlertDescription_t2237_il2cpp_TypeInfo;
extern TypeInfo Alert_t2238_il2cpp_TypeInfo;
extern TypeInfo CipherAlgorithmType_t2239_il2cpp_TypeInfo;
extern TypeInfo CipherSuite_t2242_il2cpp_TypeInfo;
extern TypeInfo CipherSuiteCollection_t2243_il2cpp_TypeInfo;
extern TypeInfo CipherSuiteFactory_t2244_il2cpp_TypeInfo;
extern TypeInfo ClientContext_t2246_il2cpp_TypeInfo;
extern TypeInfo ClientRecordProtocol_t2247_il2cpp_TypeInfo;
extern TypeInfo ClientSessionInfo_t2249_il2cpp_TypeInfo;
extern TypeInfo ClientSessionCache_t2250_il2cpp_TypeInfo;
extern TypeInfo ContentType_t2251_il2cpp_TypeInfo;
extern TypeInfo Context_t2240_il2cpp_TypeInfo;
extern TypeInfo ExchangeAlgorithmType_t2256_il2cpp_TypeInfo;
extern TypeInfo HandshakeState_t2257_il2cpp_TypeInfo;
extern TypeInfo HashAlgorithmType_t2258_il2cpp_TypeInfo;
extern TypeInfo HttpsClientStream_t2261_il2cpp_TypeInfo;
extern TypeInfo ReceiveRecordAsyncResult_t2263_il2cpp_TypeInfo;
extern TypeInfo SendRecordAsyncResult_t2265_il2cpp_TypeInfo;
extern TypeInfo RecordProtocol_t2248_il2cpp_TypeInfo;
extern TypeInfo RSASslSignatureDeformatter_t2266_il2cpp_TypeInfo;
extern TypeInfo RSASslSignatureFormatter_t2268_il2cpp_TypeInfo;
extern TypeInfo SecurityCompressionType_t2270_il2cpp_TypeInfo;
extern TypeInfo SecurityParameters_t2254_il2cpp_TypeInfo;
extern TypeInfo SecurityProtocolType_t2271_il2cpp_TypeInfo;
extern TypeInfo ServerContext_t2272_il2cpp_TypeInfo;
extern TypeInfo ValidationResult_t2273_il2cpp_TypeInfo;
extern TypeInfo SslClientStream_t2245_il2cpp_TypeInfo;
extern TypeInfo SslCipherSuite_t2277_il2cpp_TypeInfo;
extern TypeInfo SslHandshakeHash_t2278_il2cpp_TypeInfo;
extern TypeInfo InternalAsyncResult_t2279_il2cpp_TypeInfo;
extern TypeInfo SslStreamBase_t2276_il2cpp_TypeInfo;
extern TypeInfo TlsCipherSuite_t2280_il2cpp_TypeInfo;
extern TypeInfo TlsClientSettings_t2253_il2cpp_TypeInfo;
extern TypeInfo TlsException_t2281_il2cpp_TypeInfo;
extern TypeInfo TlsServerSettings_t2252_il2cpp_TypeInfo;
extern TypeInfo TlsStream_t2255_il2cpp_TypeInfo;
extern TypeInfo ClientCertificateType_t2283_il2cpp_TypeInfo;
extern TypeInfo HandshakeMessage_t2264_il2cpp_TypeInfo;
extern TypeInfo HandshakeType_t2284_il2cpp_TypeInfo;
extern TypeInfo TlsClientCertificate_t2285_il2cpp_TypeInfo;
extern TypeInfo TlsClientCertificateVerify_t2286_il2cpp_TypeInfo;
extern TypeInfo TlsClientFinished_t2287_il2cpp_TypeInfo;
extern TypeInfo TlsClientHello_t2288_il2cpp_TypeInfo;
extern TypeInfo TlsClientKeyExchange_t2289_il2cpp_TypeInfo;
extern TypeInfo TlsServerCertificate_t2290_il2cpp_TypeInfo;
extern TypeInfo TlsServerCertificateRequest_t2291_il2cpp_TypeInfo;
extern TypeInfo TlsServerFinished_t2292_il2cpp_TypeInfo;
extern TypeInfo TlsServerHello_t2293_il2cpp_TypeInfo;
extern TypeInfo TlsServerHelloDone_t2294_il2cpp_TypeInfo;
extern TypeInfo TlsServerKeyExchange_t2295_il2cpp_TypeInfo;
extern TypeInfo PrimalityTest_t2296_il2cpp_TypeInfo;
extern TypeInfo CertificateValidationCallback_t2274_il2cpp_TypeInfo;
extern TypeInfo CertificateValidationCallback2_t2275_il2cpp_TypeInfo;
extern TypeInfo CertificateSelectionCallback_t2259_il2cpp_TypeInfo;
extern TypeInfo PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU243132_t2297_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU24256_t2298_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2420_t2299_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2432_t2300_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2448_t2301_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2464_t2302_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2412_t2303_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU2416_t2304_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU244_t2305_il2cpp_TypeInfo;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo;
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_Mono_Security_Assembly_Types[122] = 
{
	&U3CModuleU3E_t2188_il2cpp_TypeInfo,
	&Locale_t2189_il2cpp_TypeInfo,
	&Sign_t2190_il2cpp_TypeInfo,
	&ModulusRing_t2192_il2cpp_TypeInfo,
	&Kernel_t2193_il2cpp_TypeInfo,
	&BigInteger_t2191_il2cpp_TypeInfo,
	&ConfidenceFactor_t2196_il2cpp_TypeInfo,
	&PrimalityTests_t2197_il2cpp_TypeInfo,
	&PrimeGeneratorBase_t2198_il2cpp_TypeInfo,
	&SequentialSearchPrimeGeneratorBase_t2199_il2cpp_TypeInfo,
	&ASN1_t2131_il2cpp_TypeInfo,
	&ASN1Convert_t2200_il2cpp_TypeInfo,
	&BitConverterLE_t2201_il2cpp_TypeInfo,
	&ContentInfo_t2202_il2cpp_TypeInfo,
	&EncryptedData_t2203_il2cpp_TypeInfo,
	&PKCS7_t2204_il2cpp_TypeInfo,
	&ARC4Managed_t2205_il2cpp_TypeInfo,
	&CryptoConvert_t2207_il2cpp_TypeInfo,
	&KeyBuilder_t2208_il2cpp_TypeInfo,
	&MD2_t2209_il2cpp_TypeInfo,
	&MD2Managed_t2211_il2cpp_TypeInfo,
	&PKCS1_t2212_il2cpp_TypeInfo,
	&PrivateKeyInfo_t2213_il2cpp_TypeInfo,
	&EncryptedPrivateKeyInfo_t2214_il2cpp_TypeInfo,
	&PKCS8_t2215_il2cpp_TypeInfo,
	&RC4_t2206_il2cpp_TypeInfo,
	&KeyGeneratedEventHandler_t2219_il2cpp_TypeInfo,
	&RSAManaged_t2146_il2cpp_TypeInfo,
	&SafeBag_t2220_il2cpp_TypeInfo,
	&DeriveBytes_t2221_il2cpp_TypeInfo,
	&PKCS12_t2152_il2cpp_TypeInfo,
	&X501_t2151_il2cpp_TypeInfo,
	&X509Certificate_t2024_il2cpp_TypeInfo,
	&X509CertificateEnumerator_t2158_il2cpp_TypeInfo,
	&X509CertificateCollection_t2153_il2cpp_TypeInfo,
	&X509Chain_t2222_il2cpp_TypeInfo,
	&X509ChainStatusFlags_t2223_il2cpp_TypeInfo,
	&X509CrlEntry_t2135_il2cpp_TypeInfo,
	&X509Crl_t2133_il2cpp_TypeInfo,
	&X509Extension_t2134_il2cpp_TypeInfo,
	&X509ExtensionCollection_t2156_il2cpp_TypeInfo,
	&X509Store_t2049_il2cpp_TypeInfo,
	&X509StoreManager_t2224_il2cpp_TypeInfo,
	&X509Stores_t2136_il2cpp_TypeInfo,
	&AuthorityKeyIdentifierExtension_t2157_il2cpp_TypeInfo,
	&BasicConstraintsExtension_t2225_il2cpp_TypeInfo,
	&ExtendedKeyUsageExtension_t2226_il2cpp_TypeInfo,
	&GeneralNames_t2227_il2cpp_TypeInfo,
	&KeyUsages_t2228_il2cpp_TypeInfo,
	&KeyUsageExtension_t2229_il2cpp_TypeInfo,
	&CertTypes_t2230_il2cpp_TypeInfo,
	&NetscapeCertTypeExtension_t2231_il2cpp_TypeInfo,
	&SubjectAltNameExtension_t2232_il2cpp_TypeInfo,
	&HMAC_t2233_il2cpp_TypeInfo,
	&MD5SHA1_t2235_il2cpp_TypeInfo,
	&AlertLevel_t2236_il2cpp_TypeInfo,
	&AlertDescription_t2237_il2cpp_TypeInfo,
	&Alert_t2238_il2cpp_TypeInfo,
	&CipherAlgorithmType_t2239_il2cpp_TypeInfo,
	&CipherSuite_t2242_il2cpp_TypeInfo,
	&CipherSuiteCollection_t2243_il2cpp_TypeInfo,
	&CipherSuiteFactory_t2244_il2cpp_TypeInfo,
	&ClientContext_t2246_il2cpp_TypeInfo,
	&ClientRecordProtocol_t2247_il2cpp_TypeInfo,
	&ClientSessionInfo_t2249_il2cpp_TypeInfo,
	&ClientSessionCache_t2250_il2cpp_TypeInfo,
	&ContentType_t2251_il2cpp_TypeInfo,
	&Context_t2240_il2cpp_TypeInfo,
	&ExchangeAlgorithmType_t2256_il2cpp_TypeInfo,
	&HandshakeState_t2257_il2cpp_TypeInfo,
	&HashAlgorithmType_t2258_il2cpp_TypeInfo,
	&HttpsClientStream_t2261_il2cpp_TypeInfo,
	&ReceiveRecordAsyncResult_t2263_il2cpp_TypeInfo,
	&SendRecordAsyncResult_t2265_il2cpp_TypeInfo,
	&RecordProtocol_t2248_il2cpp_TypeInfo,
	&RSASslSignatureDeformatter_t2266_il2cpp_TypeInfo,
	&RSASslSignatureFormatter_t2268_il2cpp_TypeInfo,
	&SecurityCompressionType_t2270_il2cpp_TypeInfo,
	&SecurityParameters_t2254_il2cpp_TypeInfo,
	&SecurityProtocolType_t2271_il2cpp_TypeInfo,
	&ServerContext_t2272_il2cpp_TypeInfo,
	&ValidationResult_t2273_il2cpp_TypeInfo,
	&SslClientStream_t2245_il2cpp_TypeInfo,
	&SslCipherSuite_t2277_il2cpp_TypeInfo,
	&SslHandshakeHash_t2278_il2cpp_TypeInfo,
	&InternalAsyncResult_t2279_il2cpp_TypeInfo,
	&SslStreamBase_t2276_il2cpp_TypeInfo,
	&TlsCipherSuite_t2280_il2cpp_TypeInfo,
	&TlsClientSettings_t2253_il2cpp_TypeInfo,
	&TlsException_t2281_il2cpp_TypeInfo,
	&TlsServerSettings_t2252_il2cpp_TypeInfo,
	&TlsStream_t2255_il2cpp_TypeInfo,
	&ClientCertificateType_t2283_il2cpp_TypeInfo,
	&HandshakeMessage_t2264_il2cpp_TypeInfo,
	&HandshakeType_t2284_il2cpp_TypeInfo,
	&TlsClientCertificate_t2285_il2cpp_TypeInfo,
	&TlsClientCertificateVerify_t2286_il2cpp_TypeInfo,
	&TlsClientFinished_t2287_il2cpp_TypeInfo,
	&TlsClientHello_t2288_il2cpp_TypeInfo,
	&TlsClientKeyExchange_t2289_il2cpp_TypeInfo,
	&TlsServerCertificate_t2290_il2cpp_TypeInfo,
	&TlsServerCertificateRequest_t2291_il2cpp_TypeInfo,
	&TlsServerFinished_t2292_il2cpp_TypeInfo,
	&TlsServerHello_t2293_il2cpp_TypeInfo,
	&TlsServerHelloDone_t2294_il2cpp_TypeInfo,
	&TlsServerKeyExchange_t2295_il2cpp_TypeInfo,
	&PrimalityTest_t2296_il2cpp_TypeInfo,
	&CertificateValidationCallback_t2274_il2cpp_TypeInfo,
	&CertificateValidationCallback2_t2275_il2cpp_TypeInfo,
	&CertificateSelectionCallback_t2259_il2cpp_TypeInfo,
	&PrivateKeySelectionCallback_t2260_il2cpp_TypeInfo,
	&U24ArrayTypeU243132_t2297_il2cpp_TypeInfo,
	&U24ArrayTypeU24256_t2298_il2cpp_TypeInfo,
	&U24ArrayTypeU2420_t2299_il2cpp_TypeInfo,
	&U24ArrayTypeU2432_t2300_il2cpp_TypeInfo,
	&U24ArrayTypeU2448_t2301_il2cpp_TypeInfo,
	&U24ArrayTypeU2464_t2302_il2cpp_TypeInfo,
	&U24ArrayTypeU2412_t2303_il2cpp_TypeInfo,
	&U24ArrayTypeU2416_t2304_il2cpp_TypeInfo,
	&U24ArrayTypeU244_t2305_il2cpp_TypeInfo,
	&U3CPrivateImplementationDetailsU3E_t2306_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_Mono_Security_dll_Image;
Il2CppAssembly g_Mono_Security_Assembly = 
{
	{ "Mono.Security", 0, 0, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x79\x15\x99\x77\xD2\xD0\x3A\x8E\x6B\xEA\x7A\x2E\x74\xE8\xD1\xAF\xCC\x93\xE8\x85\x19\x74\x95\x2B\xB4\x80\xA1\x2C\x91\x34\x47\x4D\x4\x6\x24\x47\xC3\x7E\xE\x68\xC0\x80\x53\x6F\xCF\x3C\x3F\xBE\x2F\xF9\xC9\x79\xCE\x99\x84\x75\xE5\x6\xE8\xCE\x82\xDD\x5B\xF\x35\xD\xC1\xE\x93\xBF\x2E\xEE\xCF\x87\x4B\x24\x77\xC\x50\x81\xDB\xEA\x74\x47\xFD\xDA\xFA\x27\x7B\x22\xDE\x47\xD6\xFF\xEA\x44\x96\x74\xA4\xF9\xFC\xCF\x84\xD1\x50\x69\x8\x93\x80\x28\x4D\xBD\xD3\x5F\x46\xCD\xFF\x12\xA1\xBD\x78\xE4\xEF\x0\x65\xD0\x16\xDF", { 0x07, 0x38, 0xEB, 0x9F, 0x13, 0x2E, 0xD7, 0x56 }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_Mono_Security_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_Mono_Security_Assembly_AttributeGenerators[39];
Il2CppImage g_Mono_Security_dll_Image = 
{
	 "Mono.Security.dll" ,
	&g_Mono_Security_Assembly,
	g_Mono_Security_Assembly_Types,
	121,
	NULL,
	39,
	NULL,
	g_Mono_Security_Assembly_AttributeGenerators,
};
