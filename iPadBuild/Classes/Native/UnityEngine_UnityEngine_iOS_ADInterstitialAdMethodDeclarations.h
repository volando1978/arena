﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.iOS.ADInterstitialAd
struct ADInterstitialAd_t331;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.iOS.ADInterstitialAd::.ctor()
extern "C" void ADInterstitialAd__ctor_m3688 (ADInterstitialAd_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::.cctor()
extern "C" void ADInterstitialAd__cctor_m7117 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.iOS.ADInterstitialAd::Native_CreateInterstitial(System.Boolean)
extern "C" IntPtr_t ADInterstitialAd_Native_CreateInterstitial_m7118 (Object_t * __this /* static, unused */, bool ___autoReload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_ShowInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_ShowInterstitial_m7119 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_ReloadInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_ReloadInterstitial_m7120 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.iOS.ADInterstitialAd::Native_InterstitialAdLoaded(System.IntPtr)
extern "C" bool ADInterstitialAd_Native_InterstitialAdLoaded_m7121 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::Native_DestroyInterstitial(System.IntPtr)
extern "C" void ADInterstitialAd_Native_DestroyInterstitial_m7122 (Object_t * __this /* static, unused */, IntPtr_t ___view, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::CtorImpl(System.Boolean)
extern "C" void ADInterstitialAd_CtorImpl_m7123 (ADInterstitialAd_t331 * __this, bool ___autoReload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::Finalize()
extern "C" void ADInterstitialAd_Finalize_m7124 (ADInterstitialAd_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::Show()
extern "C" void ADInterstitialAd_Show_m4234 (ADInterstitialAd_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::ReloadAd()
extern "C" void ADInterstitialAd_ReloadAd_m4207 (ADInterstitialAd_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.iOS.ADInterstitialAd::get_loaded()
extern "C" bool ADInterstitialAd_get_loaded_m4233 (ADInterstitialAd_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.ADInterstitialAd::FireInterstitialWasLoaded()
extern "C" void ADInterstitialAd_FireInterstitialWasLoaded_m7125 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
