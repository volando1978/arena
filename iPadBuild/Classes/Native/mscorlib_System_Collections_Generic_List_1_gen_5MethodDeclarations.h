﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// System.Object
struct Object_t;
// Soomla.Reward
struct Reward_t55;
// System.Collections.Generic.IEnumerable`1<Soomla.Reward>
struct IEnumerable_1_t4295;
// System.Collections.Generic.IEnumerator`1<Soomla.Reward>
struct IEnumerator_1_t4296;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Reward>
struct ICollection_1_t4297;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Reward>
struct ReadOnlyCollection_1_t3521;
// Soomla.Reward[]
struct RewardU5BU5D_t3519;
// System.Predicate`1<Soomla.Reward>
struct Predicate_1_t3522;
// System.Action`1<Soomla.Reward>
struct Action_1_t24;
// System.Comparison`1<Soomla.Reward>
struct Comparison_1_t3523;
// System.Collections.Generic.List`1/Enumerator<Soomla.Reward>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.List`1<Soomla.Reward>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m933(__this, method) (( void (*) (List_1_t56 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16816(__this, ___collection, method) (( void (*) (List_1_t56 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::.ctor(System.Int32)
#define List_1__ctor_m16817(__this, ___capacity, method) (( void (*) (List_1_t56 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::.cctor()
#define List_1__cctor_m16818(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16819(__this, method) (( Object_t* (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16820(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t56 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16821(__this, method) (( Object_t * (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16822(__this, ___item, method) (( int32_t (*) (List_1_t56 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16823(__this, ___item, method) (( bool (*) (List_1_t56 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16824(__this, ___item, method) (( int32_t (*) (List_1_t56 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16825(__this, ___index, ___item, method) (( void (*) (List_1_t56 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16826(__this, ___item, method) (( void (*) (List_1_t56 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16827(__this, method) (( bool (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16828(__this, method) (( bool (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16829(__this, method) (( Object_t * (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16830(__this, method) (( bool (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16831(__this, method) (( bool (*) (List_1_t56 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16832(__this, ___index, method) (( Object_t * (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16833(__this, ___index, ___value, method) (( void (*) (List_1_t56 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Add(T)
#define List_1_Add_m16834(__this, ___item, method) (( void (*) (List_1_t56 *, Reward_t55 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16835(__this, ___newCount, method) (( void (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16836(__this, ___collection, method) (( void (*) (List_1_t56 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16837(__this, ___enumerable, method) (( void (*) (List_1_t56 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16838(__this, ___collection, method) (( void (*) (List_1_t56 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Reward>::AsReadOnly()
#define List_1_AsReadOnly_m16839(__this, method) (( ReadOnlyCollection_1_t3521 * (*) (List_1_t56 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Clear()
#define List_1_Clear_m16840(__this, method) (( void (*) (List_1_t56 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::Contains(T)
#define List_1_Contains_m16841(__this, ___item, method) (( bool (*) (List_1_t56 *, Reward_t55 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16842(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t56 *, RewardU5BU5D_t3519*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Reward>::Find(System.Predicate`1<T>)
#define List_1_Find_m16843(__this, ___match, method) (( Reward_t55 * (*) (List_1_t56 *, Predicate_1_t3522 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16844(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3522 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m16845(__this, ___match, method) (( int32_t (*) (List_1_t56 *, Predicate_1_t3522 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16846(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t56 *, int32_t, int32_t, Predicate_1_t3522 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m16847(__this, ___action, method) (( void (*) (List_1_t56 *, Action_1_t24 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Reward>::GetEnumerator()
#define List_1_GetEnumerator_m934(__this, method) (( Enumerator_t206  (*) (List_1_t56 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::IndexOf(T)
#define List_1_IndexOf_m16848(__this, ___item, method) (( int32_t (*) (List_1_t56 *, Reward_t55 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16849(__this, ___start, ___delta, method) (( void (*) (List_1_t56 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16850(__this, ___index, method) (( void (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Insert(System.Int32,T)
#define List_1_Insert_m16851(__this, ___index, ___item, method) (( void (*) (List_1_t56 *, int32_t, Reward_t55 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16852(__this, ___collection, method) (( void (*) (List_1_t56 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Reward>::Remove(T)
#define List_1_Remove_m16853(__this, ___item, method) (( bool (*) (List_1_t56 *, Reward_t55 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16854(__this, ___match, method) (( int32_t (*) (List_1_t56 *, Predicate_1_t3522 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16855(__this, ___index, method) (( void (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Reverse()
#define List_1_Reverse_m16856(__this, method) (( void (*) (List_1_t56 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Sort()
#define List_1_Sort_m16857(__this, method) (( void (*) (List_1_t56 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16858(__this, ___comparison, method) (( void (*) (List_1_t56 *, Comparison_1_t3523 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Reward>::ToArray()
#define List_1_ToArray_m16859(__this, method) (( RewardU5BU5D_t3519* (*) (List_1_t56 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::TrimExcess()
#define List_1_TrimExcess_m16860(__this, method) (( void (*) (List_1_t56 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::get_Capacity()
#define List_1_get_Capacity_m16861(__this, method) (( int32_t (*) (List_1_t56 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16862(__this, ___value, method) (( void (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::get_Count()
#define List_1_get_Count_m16863(__this, method) (( int32_t (*) (List_1_t56 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Reward>::get_Item(System.Int32)
#define List_1_get_Item_m16864(__this, ___index, method) (( Reward_t55 * (*) (List_1_t56 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Reward>::set_Item(System.Int32,T)
#define List_1_set_Item_m16865(__this, ___index, ___value, method) (( void (*) (List_1_t56 *, int32_t, Reward_t55 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
