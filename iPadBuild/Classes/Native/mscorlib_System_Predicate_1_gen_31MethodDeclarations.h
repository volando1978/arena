﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t3856;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m21418_gshared (Predicate_1_t3856 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Predicate_1__ctor_m21418(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3856 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m21418_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m21419_gshared (Predicate_1_t3856 * __this, Vector3_t758  ___obj, MethodInfo* method);
#define Predicate_1_Invoke_m21419(__this, ___obj, method) (( bool (*) (Predicate_1_t3856 *, Vector3_t758 , MethodInfo*))Predicate_1_Invoke_m21419_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m21420_gshared (Predicate_1_t3856 * __this, Vector3_t758  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Predicate_1_BeginInvoke_m21420(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3856 *, Vector3_t758 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m21420_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m21421_gshared (Predicate_1_t3856 * __this, Object_t * ___result, MethodInfo* method);
#define Predicate_1_EndInvoke_m21421(__this, ___result, method) (( bool (*) (Predicate_1_t3856 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m21421_gshared)(__this, ___result, method)
