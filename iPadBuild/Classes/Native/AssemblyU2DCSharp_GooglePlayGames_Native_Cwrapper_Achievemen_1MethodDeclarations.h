﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
struct FetchCallback_t399;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchCallback__ctor_m1631 (FetchCallback_t399 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchCallback_Invoke_m1632 (FetchCallback_t399 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchCallback_t399(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchCallback_BeginInvoke_m1633 (FetchCallback_t399 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchCallback_EndInvoke_m1634 (FetchCallback_t399 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
