﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>
struct  Func_2_t699  : public MulticastDelegate_t22
{
};
