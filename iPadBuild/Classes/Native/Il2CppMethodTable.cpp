﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern "C" void UnityNameValuePair_1_get_Key_m15859_gshared ();
void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityNameValuePair_1_set_Key_m15860_gshared ();
void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityNameValuePair_1__ctor_m15857_gshared ();
void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityNameValuePair_1__ctor_m15858_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityKeyValuePair_2_get_Key_m15547_gshared ();
extern "C" void UnityKeyValuePair_2_set_Key_m15548_gshared ();
extern "C" void UnityKeyValuePair_2_get_Value_m15549_gshared ();
extern "C" void UnityKeyValuePair_2_set_Value_m15550_gshared ();
extern "C" void UnityKeyValuePair_2__ctor_m15545_gshared ();
extern "C" void UnityKeyValuePair_2__ctor_m15546_gshared ();
extern "C" void UnityDictionary_2_get_Item_m15266_gshared ();
void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_set_Item_m15267_gshared ();
extern "C" void UnityDictionary_2_get_Count_m15276_gshared ();
void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_get_Keys_m15279_gshared ();
extern "C" void UnityDictionary_2_get_Values_m15280_gshared ();
extern "C" void UnityDictionary_2_get_Items_m15282_gshared ();
extern "C" void UnityDictionary_2_get_SyncRoot_m15284_gshared ();
extern "C" void UnityDictionary_2_get_IsFixedSize_m15286_gshared ();
void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_get_IsReadOnly_m15287_gshared ();
extern "C" void UnityDictionary_2_get_IsSynchronized_m15289_gshared ();
extern "C" void UnityDictionary_2__ctor_m15264_gshared ();
extern "C" void UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared ();
extern "C" void UnityDictionary_2_Add_m15268_gshared ();
extern "C" void UnityDictionary_2_Add_m15269_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_TryGetValue_m15270_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_Remove_m15271_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_Remove_m15272_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_Clear_m15273_gshared ();
extern "C" void UnityDictionary_2_ContainsKey_m15274_gshared ();
extern "C" void UnityDictionary_2_Contains_m15275_gshared ();
extern "C" void UnityDictionary_2_CopyTo_m15277_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_GetEnumerator_m15278_gshared ();
extern "C" void UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared ();
extern "C" void UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared ();
extern "C" void UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared ();
extern "C" void UnityDictionaryEnumerator_System_Collections_IEnumerator_get_Current_m15777_gshared ();
extern "C" void UnityDictionaryEnumerator_get_Current_m15778_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityDictionaryEnumerator_get_Entry_m15779_gshared ();
extern "C" void UnityDictionaryEnumerator_get_Key_m15781_gshared ();
extern "C" void UnityDictionaryEnumerator_get_Value_m15782_gshared ();
extern "C" void UnityDictionaryEnumerator__ctor_m15775_gshared ();
extern "C" void UnityDictionaryEnumerator__ctor_m15776_gshared ();
extern "C" void UnityDictionaryEnumerator_Dispose_m15780_gshared ();
extern "C" void UnityDictionaryEnumerator_MoveNext_m15783_gshared ();
extern "C" void UnityDictionaryEnumerator_ValidateIndex_m15784_gshared ();
extern "C" void UnityDictionaryEnumerator_Reset_m15785_gshared ();
extern "C" void U3CU3Ec__AnonStorey4__ctor_m15599_gshared ();
extern "C" void U3CU3Ec__AnonStorey4_U3CU3Em__3_m15600_gshared ();
extern "C" void U3CRemoveU3Ec__AnonStorey5__ctor_m15771_gshared ();
extern "C" void U3CRemoveU3Ec__AnonStorey5_U3CU3Em__4_m15772_gshared ();
extern "C" void U3CContainsKeyU3Ec__AnonStorey6__ctor_m15773_gshared ();
extern "C" void U3CContainsKeyU3Ec__AnonStorey6_U3CU3Em__5_m15774_gshared ();
extern "C" void UnityDictionary_1__ctor_m15867_gshared ();
extern "C" void SoomlaEntity_1_get_ID_m16334_gshared ();
extern "C" void SoomlaEntity_1__ctor_m16331_gshared ();
extern "C" void SoomlaEntity_1__ctor_m16332_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void SoomlaEntity_1__ctor_m16333_gshared ();
extern "C" void SoomlaEntity_1_toJSONObject_m16335_gshared ();
extern "C" void SoomlaEntity_1_Equals_m16336_gshared ();
extern "C" void SoomlaEntity_1_Equals_m16338_gshared ();
extern "C" void SoomlaEntity_1_GetHashCode_m16339_gshared ();
extern "C" void SoomlaEntity_1_Clone_m16340_gshared ();
extern "C" void SoomlaEntity_1_op_Equality_m16341_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void SoomlaEntity_1_op_Inequality_m16343_gshared ();
extern "C" void SoomlaExtensions_AddOrUpdate_TisObject_t_m940_gshared ();
extern "C" void AsyncExec_runWithCallback_TisObject_t_TisObject_t_m26310_gshared ();
extern "C" void Misc_CheckNotNull_TisObject_t_m3706_gshared ();
extern "C" void Misc_CheckNotNull_TisObject_t_m26343_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void CallbackUtils_ToOnGameThread_TisObject_t_m26345_gshared ();
extern "C" void CallbackUtils_ToOnGameThread_TisObject_t_TisObject_t_m26347_gshared ();
extern "C" void CallbackUtils_ToOnGameThread_TisObject_t_TisObject_t_TisObject_t_m26349_gshared ();
extern "C" void CallbackUtils_U3CToOnGameThread_1U3Em__4_TisObject_t_m26344_gshared ();
extern "C" void CallbackUtils_U3CToOnGameThread_2U3Em__6_TisObject_t_TisObject_t_m26346_gshared ();
extern "C" void CallbackUtils_U3CToOnGameThread_3U3Em__8_TisObject_t_TisObject_t_TisObject_t_m26348_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey14_1__ctor_m19609_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey14_1_U3CU3Em__5_m19610_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey15_1__ctor_m19611_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey15_1_U3CU3Em__A_m19612_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19613_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19614_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19615_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19616_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19617_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19618_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19619_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19620_gshared ();
extern "C" void NativeClient_AsOnGameThreadCallback_TisObject_t_m26362_gshared ();
extern "C" void NativeClient_InvokeCallbackOnGameThread_TisObject_t_m26363_gshared ();
extern "C" void NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisObject_t_m26361_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19730_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19731_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19732_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19733_gshared ();
extern "C" void NativeRealtimeMultiplayerClient_WithDefault_TisObject_t_m26370_gshared ();
extern "C" void NativeSavedGameClient_ToOnGameThread_TisObject_t_TisObject_t_m26388_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20332_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20333_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20334_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20335_gshared ();
extern "C" void Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared ();
void* RuntimeInvoker_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Callbacks_ToIntPtr_TisObject_t_TisBaseReferenceHolder_t654_m26389_gshared ();
extern "C" void Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared ();
void* RuntimeInvoker_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Callbacks_IntPtrToCallback_TisObject_t_m26390_gshared ();
void* RuntimeInvoker_Object_t_IntPtr_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared ();
extern "C" void Callbacks_PerformInternalCallback_TisObject_t_m26393_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Callbacks_AsOnGameThreadCallback_TisObject_t_m26394_gshared ();
extern "C" void Callbacks_AsOnGameThreadCallback_TisObject_t_TisObject_t_m26395_gshared ();
extern "C" void U3CToIntPtrU3Ec__AnonStorey5C_1__ctor_m20386_gshared ();
extern "C" void U3CToIntPtrU3Ec__AnonStorey5C_1_U3CU3Em__73_m20387_gshared ();
void* RuntimeInvoker_Void_t260_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void U3CToIntPtrU3Ec__AnonStorey5D_2__ctor_m20414_gshared ();
extern "C" void U3CToIntPtrU3Ec__AnonStorey5D_2_U3CU3Em__74_m20415_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20416_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20417_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20418_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20419_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20420_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20421_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20422_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20423_gshared ();
extern "C" void PInvokeUtilities_OutParamsToArray_TisObject_t_m26416_gshared ();
extern "C" void PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared ();
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared ();
extern "C" void PInvokeUtilities_ArrayToSizeT_TisObject_t_m26417_gshared ();
void* RuntimeInvoker_UIntPtr_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void OutMethod_1__ctor_m20673_gshared ();
extern "C" void OutMethod_1_Invoke_m20674_gshared ();
void* RuntimeInvoker_UIntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void OutMethod_1_BeginInvoke_m20675_gshared ();
void* RuntimeInvoker_Object_t_Object_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void OutMethod_1_EndInvoke_m20676_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m20399_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20400_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1__ctor_m20398_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m20401_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20402_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_MoveNext_m20403_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_Dispose_m20404_gshared ();
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_Reset_m20405_gshared ();
extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m5747_gshared ();
extern "C" void ExecuteEvents_Execute_TisObject_t_m5734_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m5804_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m26493_gshared ();
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m26489_gshared ();
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m26518_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m5782_gshared ();
extern "C" void EventFunction_1__ctor_m21548_gshared ();
extern "C" void EventFunction_1_Invoke_m21550_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m21552_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_EndInvoke_m21554_gshared ();
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m5913_gshared ();
void* RuntimeInvoker_Boolean_t203_ObjectU26_t3349_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisObject_t_m6151_gshared ();
void* RuntimeInvoker_Void_t260_ObjectU26_t3349_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Count_m22570_gshared ();
extern "C" void IndexedSet_1_get_IsReadOnly_m22572_gshared ();
extern "C" void IndexedSet_1_get_Item_m22580_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_set_Item_m22582_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1__ctor_m22554_gshared ();
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556_gshared ();
extern "C" void IndexedSet_1_Add_m22558_gshared ();
extern "C" void IndexedSet_1_Remove_m22560_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m22562_gshared ();
extern "C" void IndexedSet_1_Clear_m22564_gshared ();
extern "C" void IndexedSet_1_Contains_m22566_gshared ();
extern "C" void IndexedSet_1_CopyTo_m22568_gshared ();
extern "C" void IndexedSet_1_IndexOf_m22574_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Insert_m22576_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m22578_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_RemoveAll_m22583_gshared ();
extern "C" void IndexedSet_1_Sort_m22584_gshared ();
extern "C" void ObjectPool_1_get_countAll_m21657_gshared ();
extern "C" void ObjectPool_1_set_countAll_m21659_gshared ();
extern "C" void ObjectPool_1_get_countActive_m21661_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m21663_gshared ();
extern "C" void ObjectPool_1__ctor_m21655_gshared ();
extern "C" void ObjectPool_1_Get_m21665_gshared ();
extern "C" void ObjectPool_1_Release_m21667_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisScriptableObject_t15_m846_gshared ();
extern "C" void Object_Instantiate_TisObject_t187_m4176_gshared ();
extern "C" void Object_FindObjectOfType_TisObject_t187_m962_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m4012_gshared ();
extern "C" void Component_GetComponentInChildren_TisObject_t_m4044_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m26612_gshared ();
void* RuntimeInvoker_Void_t260_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m6164_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m5732_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m4017_gshared ();
extern "C" void GameObject_GetComponentInChildren_TisObject_t_m4210_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m26492_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m26613_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m5864_gshared ();
extern "C" void GameObject_AddComponent_TisComponent_t230_m1035_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m26517_gshared ();
extern "C" void InvokableCall_1__ctor_m22161_gshared ();
extern "C" void InvokableCall_1__ctor_m22162_gshared ();
extern "C" void InvokableCall_1_Invoke_m22163_gshared ();
extern "C" void InvokableCall_1_Find_m22164_gshared ();
extern "C" void InvokableCall_2__ctor_m25276_gshared ();
extern "C" void InvokableCall_2_Invoke_m25277_gshared ();
extern "C" void InvokableCall_2_Find_m25278_gshared ();
extern "C" void InvokableCall_3__ctor_m25283_gshared ();
extern "C" void InvokableCall_3_Invoke_m25284_gshared ();
extern "C" void InvokableCall_3_Find_m25285_gshared ();
extern "C" void InvokableCall_4__ctor_m25290_gshared ();
extern "C" void InvokableCall_4_Invoke_m25291_gshared ();
extern "C" void InvokableCall_4_Find_m25292_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m25297_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m25298_gshared ();
extern "C" void UnityEvent_1__ctor_m22151_gshared ();
extern "C" void UnityEvent_1_AddListener_m22153_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m22155_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m22156_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m22157_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m22159_gshared ();
extern "C" void UnityEvent_1_Invoke_m22160_gshared ();
extern "C" void UnityEvent_2__ctor_m25513_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m25514_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m25515_gshared ();
extern "C" void UnityEvent_3__ctor_m25516_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m25517_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m25518_gshared ();
extern "C" void UnityEvent_4__ctor_m25519_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m25520_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m25521_gshared ();
extern "C" void UnityAction_1__ctor_m21684_gshared ();
extern "C" void UnityAction_1_Invoke_m21685_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m21686_gshared ();
extern "C" void UnityAction_1_EndInvoke_m21687_gshared ();
extern "C" void UnityAction_2__ctor_m25279_gshared ();
extern "C" void UnityAction_2_Invoke_m25280_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m25281_gshared ();
extern "C" void UnityAction_2_EndInvoke_m25282_gshared ();
extern "C" void UnityAction_3__ctor_m25286_gshared ();
extern "C" void UnityAction_3_Invoke_m25287_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m25288_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_3_EndInvoke_m25289_gshared ();
extern "C" void UnityAction_4__ctor_m25293_gshared ();
extern "C" void UnityAction_4_Invoke_m25294_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_BeginInvoke_m25295_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_EndInvoke_m25296_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609_gshared ();
extern "C" void HashSet_1_get_Count_m17617_gshared ();
extern "C" void HashSet_1__ctor_m17601_gshared ();
extern "C" void HashSet_1__ctor_m17603_gshared ();
extern "C" void HashSet_1__ctor_m17605_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613_gshared ();
extern "C" void HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615_gshared ();
extern "C" void HashSet_1_Init_m17619_gshared ();
extern "C" void HashSet_1_InitArrays_m17621_gshared ();
extern "C" void HashSet_1_SlotsContainsAt_m17623_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_CopyTo_m17625_gshared ();
extern "C" void HashSet_1_CopyTo_m17627_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_Resize_m17629_gshared ();
extern "C" void HashSet_1_GetLinkHashCode_m17631_gshared ();
void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_GetItemHashCode_m17633_gshared ();
extern "C" void HashSet_1_Add_m17634_gshared ();
extern "C" void HashSet_1_Clear_m17636_gshared ();
extern "C" void HashSet_1_Contains_m17638_gshared ();
extern "C" void HashSet_1_Remove_m17640_gshared ();
extern "C" void HashSet_1_GetObjectData_m17642_gshared ();
extern "C" void HashSet_1_OnDeserialization_m17644_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared ();
extern "C" void Enumerator_get_Current_m17653_gshared ();
extern "C" void Enumerator__ctor_m17650_gshared ();
extern "C" void Enumerator_MoveNext_m17652_gshared ();
extern "C" void Enumerator_Dispose_m17654_gshared ();
extern "C" void Enumerator_CheckState_m17655_gshared ();
extern "C" void PrimeHelper__cctor_m17656_gshared ();
extern "C" void PrimeHelper_TestPrime_m17657_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void PrimeHelper_CalcPrime_m17658_gshared ();
extern "C" void PrimeHelper_ToPrime_m17659_gshared ();
extern "C" void Enumerable_Cast_TisObject_t_m3761_gshared ();
extern "C" void Enumerable_CreateCastIterator_TisObject_t_m26368_gshared ();
extern "C" void Enumerable_Contains_TisObject_t_m26387_gshared ();
extern "C" void Enumerable_Count_TisObject_t_m1002_gshared ();
extern "C" void Enumerable_Except_TisObject_t_m3818_gshared ();
extern "C" void Enumerable_Except_TisObject_t_m26385_gshared ();
extern "C" void Enumerable_CreateExceptIterator_TisObject_t_m26386_gshared ();
extern "C" void Enumerable_First_TisObject_t_m26309_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerable_FirstOrDefault_TisObject_t_m997_gshared ();
extern "C" void Enumerable_LongCount_TisObject_t_m3977_gshared ();
void* RuntimeInvoker_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared ();
extern "C" void Enumerable_CreateSelectIterator_TisObject_t_TisObject_t_m26342_gshared ();
extern "C" void Enumerable_ToArray_TisObject_t_m3698_gshared ();
extern "C" void Enumerable_ToDictionary_TisObject_t_TisObject_t_TisObject_t_m26372_gshared ();
extern "C" void Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared ();
extern "C" void Enumerable_ToDictionary_TisObject_t_TisObject_t_m26371_gshared ();
extern "C" void Enumerable_ToList_TisObject_t_m3763_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m3805_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m26373_gshared ();
extern "C" void Function_1__cctor_m20115_gshared ();
extern "C" void Function_1_U3CIdentityU3Em__77_m20116_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19873_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19874_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19872_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19875_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19876_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19877_gshared ();
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19878_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20289_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m20290_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m20288_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m20291_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20292_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m20293_gshared ();
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m20294_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19204_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m19205_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m19203_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m19206_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19207_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m19208_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m19209_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20118_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20119_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20117_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20120_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20121_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20122_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20123_gshared ();
extern "C" void Action_2__ctor_m16118_gshared ();
extern "C" void Action_2_Invoke_m16120_gshared ();
extern "C" void Action_2_BeginInvoke_m16122_gshared ();
extern "C" void Action_2_EndInvoke_m16124_gshared ();
extern "C" void Action_3__ctor_m17301_gshared ();
extern "C" void Action_3_Invoke_m17303_gshared ();
extern "C" void Action_3_BeginInvoke_m17305_gshared ();
extern "C" void Action_3_EndInvoke_m17307_gshared ();
extern "C" void Action_4__ctor_m25522_gshared ();
extern "C" void Action_4_Invoke_m25523_gshared ();
extern "C" void Action_4_BeginInvoke_m25524_gshared ();
extern "C" void Action_4_EndInvoke_m25525_gshared ();
extern "C" void Func_2__ctor_m19196_gshared ();
extern "C" void Func_2_Invoke_m19198_gshared ();
extern "C" void Func_2_BeginInvoke_m19200_gshared ();
extern "C" void Func_2_EndInvoke_m19202_gshared ();
extern "C" void Func_3__ctor_m18532_gshared ();
extern "C" void Func_3_Invoke_m18534_gshared ();
extern "C" void Func_3_BeginInvoke_m18536_gshared ();
extern "C" void Func_3_EndInvoke_m18538_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m21670_gshared ();
extern "C" void Stack_1_get_Count_m21677_gshared ();
extern "C" void Stack_1__ctor_m21668_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m21671_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673_gshared ();
extern "C" void Stack_1_Peek_m21674_gshared ();
extern "C" void Stack_1_Pop_m21675_gshared ();
extern "C" void Stack_1_Push_m21676_gshared ();
extern "C" void Stack_1_GetEnumerator_m21678_gshared ();
void* RuntimeInvoker_Enumerator_t3880 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21680_gshared ();
extern "C" void Enumerator_get_Current_m21683_gshared ();
extern "C" void Enumerator__ctor_m21679_gshared ();
extern "C" void Enumerator_Dispose_m21681_gshared ();
extern "C" void Enumerator_MoveNext_m21682_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m26190_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m26182_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m26185_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m26183_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m26184_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m26187_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m26186_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m26181_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m26189_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_get_swapper_TisObject_t_m26196_gshared ();
extern "C" void Array_Sort_TisObject_t_m26803_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26804_gshared ();
extern "C" void Array_Sort_TisObject_t_m26805_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26806_gshared ();
extern "C" void Array_Sort_TisObject_t_m14419_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26807_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m26195_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m26194_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m26808_gshared ();
extern "C" void Array_Sort_TisObject_t_m26234_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_TisObject_t_m26197_gshared ();
extern "C" void Array_compare_TisObject_t_m26231_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_m26233_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m26232_gshared ();
extern "C" void Array_swap_TisObject_t_m26235_gshared ();
extern "C" void Array_Resize_TisObject_t_m26193_gshared ();
void* RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisObject_t_m26192_gshared ();
void* RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_TrueForAll_TisObject_t_m26809_gshared ();
extern "C" void Array_ForEach_TisObject_t_m26810_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m26811_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m26813_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m26814_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m26812_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindIndex_TisObject_t_m26816_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m26817_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m26815_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m26819_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m26820_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m26821_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m26818_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14421_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m26822_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m14418_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_LastIndexOf_TisObject_t_m26824_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m26823_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m26825_gshared ();
extern "C" void Array_FindAll_TisObject_t_m26826_gshared ();
extern "C" void Array_Exists_TisObject_t_m26827_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m26828_gshared ();
extern "C" void Array_Find_TisObject_t_m26829_gshared ();
extern "C" void Array_FindLast_TisObject_t_m26830_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15307_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15303_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15305_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15306_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m25841_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m25842_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m25843_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m25844_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m25839_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25840_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m25845_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m25846_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m25847_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m25848_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m25849_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m25850_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m25851_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m25852_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m25853_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m25854_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m25856_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m25857_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m25855_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m25858_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m25859_gshared ();
extern "C" void Comparer_1_get_Default_m15518_gshared ();
extern "C" void Comparer_1__ctor_m15515_gshared ();
extern "C" void Comparer_1__cctor_m15516_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15517_gshared ();
extern "C" void DefaultComparer__ctor_m15519_gshared ();
extern "C" void DefaultComparer_Compare_m15520_gshared ();
extern "C" void GenericComparer_1__ctor_m25895_gshared ();
extern "C" void GenericComparer_1_Compare_m25896_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared ();
extern "C" void Dictionary_2_get_Count_m16251_gshared ();
extern "C" void Dictionary_2_get_Item_m16253_gshared ();
extern "C" void Dictionary_2_set_Item_m16255_gshared ();
extern "C" void Dictionary_2_get_Keys_m16289_gshared ();
extern "C" void Dictionary_2_get_Values_m16291_gshared ();
extern "C" void Dictionary_2__ctor_m16211_gshared ();
extern "C" void Dictionary_2__ctor_m16213_gshared ();
extern "C" void Dictionary_2__ctor_m16215_gshared ();
extern "C" void Dictionary_2__ctor_m16217_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared ();
extern "C" void Dictionary_2_Init_m16257_gshared ();
extern "C" void Dictionary_2_InitArrays_m16259_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16261_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26249_gshared ();
extern "C" void Dictionary_2_make_pair_m16263_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16265_gshared ();
extern "C" void Dictionary_2_pick_value_m16267_gshared ();
extern "C" void Dictionary_2_CopyTo_m16269_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26250_gshared ();
extern "C" void Dictionary_2_Resize_m16271_gshared ();
extern "C" void Dictionary_2_Add_m16273_gshared ();
extern "C" void Dictionary_2_Clear_m16275_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16277_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16279_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16281_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16283_gshared ();
extern "C" void Dictionary_2_Remove_m16285_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16287_gshared ();
extern "C" void Dictionary_2_ToTKey_m16293_gshared ();
extern "C" void Dictionary_2_ToTValue_m16295_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16297_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16298_gshared ();
void* RuntimeInvoker_Enumerator_t3486 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16300_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Entry_m16326_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Key_m16327_gshared ();
extern "C" void ShimEnumerator_get_Value_m16328_gshared ();
extern "C" void ShimEnumerator_get_Current_m16329_gshared ();
extern "C" void ShimEnumerator__ctor_m16324_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16325_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared ();
extern "C" void Enumerator_get_Current_m16200_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16202_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16204_gshared ();
extern "C" void Enumerator__ctor_m16190_gshared ();
extern "C" void Enumerator_MoveNext_m16199_gshared ();
extern "C" void Enumerator_VerifyState_m16206_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16208_gshared ();
extern "C" void Enumerator_Dispose_m16210_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16150_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16152_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16154_gshared ();
extern "C" void KeyCollection_get_Count_m16160_gshared ();
extern "C" void KeyCollection__ctor_m16134_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16136_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16138_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16140_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16142_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16144_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16146_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16148_gshared ();
extern "C" void KeyCollection_CopyTo_m16156_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16158_gshared ();
void* RuntimeInvoker_Enumerator_t3485 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16302_gshared ();
extern "C" void Enumerator_get_Current_m16305_gshared ();
extern "C" void Enumerator__ctor_m16301_gshared ();
extern "C" void Enumerator_Dispose_m16303_gshared ();
extern "C" void Enumerator_MoveNext_m16304_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared ();
extern "C" void ValueCollection_get_Count_m16188_gshared ();
extern "C" void ValueCollection__ctor_m16162_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared ();
extern "C" void ValueCollection_CopyTo_m16184_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16186_gshared ();
void* RuntimeInvoker_Enumerator_t3489 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared ();
extern "C" void Enumerator_get_Current_m16314_gshared ();
extern "C" void Enumerator__ctor_m16310_gshared ();
extern "C" void Enumerator_Dispose_m16312_gshared ();
extern "C" void Enumerator_MoveNext_m16313_gshared ();
extern "C" void Transform_1__ctor_m16306_gshared ();
extern "C" void Transform_1_Invoke_m16307_gshared ();
extern "C" void Transform_1_BeginInvoke_m16308_gshared ();
extern "C" void Transform_1_EndInvoke_m16309_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15498_gshared ();
extern "C" void EqualityComparer_1__ctor_m15494_gshared ();
extern "C" void EqualityComparer_1__cctor_m15495_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15496_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15497_gshared ();
extern "C" void DefaultComparer__ctor_m15504_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15505_gshared ();
extern "C" void DefaultComparer_Equals_m15506_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25897_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25898_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25899_gshared ();
extern "C" void KeyValuePair_2_get_Key_m15309_gshared ();
extern "C" void KeyValuePair_2_set_Key_m15310_gshared ();
extern "C" void KeyValuePair_2_get_Value_m15311_gshared ();
extern "C" void KeyValuePair_2_set_Value_m15312_gshared ();
extern "C" void KeyValuePair_2__ctor_m15308_gshared ();
extern "C" void KeyValuePair_2_ToString_m15313_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15353_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15355_gshared ();
extern "C" void List_1_get_Capacity_m15413_gshared ();
extern "C" void List_1_set_Capacity_m15415_gshared ();
extern "C" void List_1_get_Count_m15417_gshared ();
extern "C" void List_1_get_Item_m15419_gshared ();
extern "C" void List_1_set_Item_m15421_gshared ();
extern "C" void List_1__ctor_m1042_gshared ();
extern "C" void List_1__ctor_m15321_gshared ();
extern "C" void List_1__ctor_m15323_gshared ();
extern "C" void List_1__cctor_m15325_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15329_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15333_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15335_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15337_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15339_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15341_gshared ();
extern "C" void List_1_Add_m15357_gshared ();
extern "C" void List_1_GrowIfNeeded_m15359_gshared ();
extern "C" void List_1_AddCollection_m15361_gshared ();
extern "C" void List_1_AddEnumerable_m15363_gshared ();
extern "C" void List_1_AddRange_m15365_gshared ();
extern "C" void List_1_AsReadOnly_m15367_gshared ();
extern "C" void List_1_Clear_m15369_gshared ();
extern "C" void List_1_Contains_m15371_gshared ();
extern "C" void List_1_ConvertAll_TisObject_t_m837_gshared ();
extern "C" void List_1_CopyTo_m15373_gshared ();
extern "C" void List_1_Find_m15375_gshared ();
extern "C" void List_1_CheckMatch_m15377_gshared ();
extern "C" void List_1_FindIndex_m15379_gshared ();
extern "C" void List_1_GetIndex_m15381_gshared ();
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ForEach_m15383_gshared ();
extern "C" void List_1_GetEnumerator_m15385_gshared ();
void* RuntimeInvoker_Enumerator_t3414 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15387_gshared ();
extern "C" void List_1_Shift_m15389_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckIndex_m15391_gshared ();
extern "C" void List_1_Insert_m15393_gshared ();
extern "C" void List_1_CheckCollection_m15395_gshared ();
extern "C" void List_1_Remove_m15397_gshared ();
extern "C" void List_1_RemoveAll_m15399_gshared ();
extern "C" void List_1_RemoveAt_m15401_gshared ();
extern "C" void List_1_Reverse_m15403_gshared ();
extern "C" void List_1_Sort_m15405_gshared ();
extern "C" void List_1_Sort_m15407_gshared ();
extern "C" void List_1_ToArray_m15409_gshared ();
extern "C" void List_1_TrimExcess_m15411_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared ();
extern "C" void Enumerator_get_Current_m15427_gshared ();
extern "C" void Enumerator__ctor_m15422_gshared ();
extern "C" void Enumerator_Dispose_m15424_gshared ();
extern "C" void Enumerator_VerifyState_m15425_gshared ();
extern "C" void Enumerator_MoveNext_m15426_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15459_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15467_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15468_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15469_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15470_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15471_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15472_gshared ();
extern "C" void Collection_1_get_Count_m15485_gshared ();
extern "C" void Collection_1_get_Item_m15486_gshared ();
extern "C" void Collection_1_set_Item_m15487_gshared ();
extern "C" void Collection_1__ctor_m15458_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15460_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15461_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15462_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15463_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15464_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15465_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15466_gshared ();
extern "C" void Collection_1_Add_m15473_gshared ();
extern "C" void Collection_1_Clear_m15474_gshared ();
extern "C" void Collection_1_ClearItems_m15475_gshared ();
extern "C" void Collection_1_Contains_m15476_gshared ();
extern "C" void Collection_1_CopyTo_m15477_gshared ();
extern "C" void Collection_1_GetEnumerator_m15478_gshared ();
extern "C" void Collection_1_IndexOf_m15479_gshared ();
extern "C" void Collection_1_Insert_m15480_gshared ();
extern "C" void Collection_1_InsertItem_m15481_gshared ();
extern "C" void Collection_1_Remove_m15482_gshared ();
extern "C" void Collection_1_RemoveAt_m15483_gshared ();
extern "C" void Collection_1_RemoveItem_m15484_gshared ();
extern "C" void Collection_1_SetItem_m15488_gshared ();
extern "C" void Collection_1_IsValidItem_m15489_gshared ();
extern "C" void Collection_1_ConvertItem_m15490_gshared ();
extern "C" void Collection_1_CheckWritable_m15491_gshared ();
extern "C" void Collection_1_IsSynchronized_m15492_gshared ();
extern "C" void Collection_1_IsFixedSize_m15493_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15456_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15457_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15428_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15452_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15453_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15454_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15455_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26864_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26865_gshared ();
extern "C" void Getter_2__ctor_m25955_gshared ();
extern "C" void Getter_2_Invoke_m25956_gshared ();
extern "C" void Getter_2_BeginInvoke_m25957_gshared ();
extern "C" void Getter_2_EndInvoke_m25958_gshared ();
extern "C" void StaticGetter_1__ctor_m25959_gshared ();
extern "C" void StaticGetter_1_Invoke_m25960_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m25961_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m25962_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m26490_gshared ();
extern "C" void Action_1__ctor_m15511_gshared ();
extern "C" void Action_1_Invoke_m15512_gshared ();
extern "C" void Action_1_BeginInvoke_m15513_gshared ();
extern "C" void Action_1_EndInvoke_m15514_gshared ();
extern "C" void Comparison_1__ctor_m15541_gshared ();
extern "C" void Comparison_1_Invoke_m15542_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15543_gshared ();
extern "C" void Comparison_1_EndInvoke_m15544_gshared ();
extern "C" void Converter_2__ctor_m15602_gshared ();
extern "C" void Converter_2_Invoke_m15604_gshared ();
extern "C" void Converter_2_BeginInvoke_m15606_gshared ();
extern "C" void Converter_2_EndInvoke_m15608_gshared ();
extern "C" void Predicate_1__ctor_m15507_gshared ();
extern "C" void Predicate_1_Invoke_m15508_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15509_gshared ();
extern "C" void Predicate_1_EndInvoke_m15510_gshared ();
extern "C" void Array_IndexOf_TisUInt16_t194_m881_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_3__ctor_m17250_gshared ();
extern "C" void Action_1__ctor_m17260_gshared ();
extern "C" void Dictionary_2__ctor_m17007_gshared ();
extern "C" void Dictionary_2__ctor_m17004_gshared ();
extern "C" void Func_2__ctor_m18520_gshared ();
extern "C" void Dictionary_2__ctor_m18861_gshared ();
extern "C" void Nullable_1__ctor_m3713_gshared ();
void* RuntimeInvoker_Void_t260_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m3714_gshared ();
extern "C" void Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_gshared ();
void* RuntimeInvoker_PlayGamesClientConfiguration_t366_PlayGamesClientConfiguration_t366 (MethodInfo* method, void* obj, void** args);
extern "C" void NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_3__ctor_m19345_gshared ();
extern "C" void NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743_gshared ();
extern "C" void Action_1__ctor_m18539_gshared ();
extern "C" void NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745_gshared ();
extern "C" void Action_2__ctor_m19626_gshared ();
extern "C" void Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared ();
extern "C" void CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared ();
extern "C" void CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared ();
extern "C" void HashSet_1__ctor_m20229_gshared ();
extern "C" void HashSet_1_Add_m20262_gshared ();
extern "C" void Action_4__ctor_m19898_gshared ();
extern "C" void Action_2__ctor_m18648_gshared ();
extern "C" void NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared ();
extern "C" void Nullable_1_get_Value_m3870_gshared ();
void* RuntimeInvoker_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874_gshared ();
extern "C" void Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877_gshared ();
extern "C" void Func_2__ctor_m20391_gshared ();
extern "C" void Func_2__ctor_m20368_gshared ();
extern "C" void OutMethod_1__ctor_m3916_gshared ();
extern "C" void PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_gshared ();
extern "C" void Enumerable_Select_TisIntPtr_t_TisObject_t_m3914_gshared ();
extern "C" void Dictionary_2__ctor_m20443_gshared ();
extern "C" void Nullable_1__ctor_m3928_gshared ();
void* RuntimeInvoker_Void_t260_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void OutMethod_1__ctor_m3930_gshared ();
extern "C" void PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_gshared ();
extern "C" void Action_1__ctor_m3963_gshared ();
extern "C" void PInvokeUtilities_ArrayToSizeT_TisByte_t237_m3971_gshared ();
extern "C" void Func_2__ctor_m20701_gshared ();
extern "C" void Enumerable_Select_TisObject_t_TisIntPtr_t_m3974_gshared ();
extern "C" void Enumerable_ToArray_TisIntPtr_t_m3975_gshared ();
extern "C" void UnityAction_1__ctor_m20906_gshared ();
extern "C" void UnityEvent_1_AddListener_m20914_gshared ();
extern "C" void List_1__ctor_m4240_gshared ();
extern "C" void List_1__ctor_m4257_gshared ();
extern "C" void Comparison_1__ctor_m5737_gshared ();
extern "C" void List_1_Sort_m5741_gshared ();
extern "C" void List_1__ctor_m5776_gshared ();
extern "C" void Dictionary_2__ctor_m22214_gshared ();
extern "C" void Dictionary_2_get_Values_m22293_gshared ();
extern "C" void ValueCollection_GetEnumerator_m22365_gshared ();
void* RuntimeInvoker_Enumerator_t3929 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m22371_gshared ();
extern "C" void Enumerator_MoveNext_m22370_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m22300_gshared ();
void* RuntimeInvoker_Enumerator_t3925 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m22339_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Value_m22311_gshared ();
extern "C" void KeyValuePair_2_get_Key_m22309_gshared ();
extern "C" void Enumerator_MoveNext_m22338_gshared ();
extern "C" void KeyValuePair_2_ToString_m22313_gshared ();
extern "C" void Comparison_1__ctor_m5836_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t990_m5831_gshared ();
extern "C" void UnityEvent_1__ctor_m5841_gshared ();
extern "C" void UnityEvent_1_Invoke_m5843_gshared ();
void* RuntimeInvoker_Void_t260_Color_t747 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m5844_gshared ();
extern "C" void TweenRunner_1__ctor_m5866_gshared ();
extern "C" void TweenRunner_1_Init_m5867_gshared ();
extern "C" void UnityAction_1__ctor_m5888_gshared ();
extern "C" void TweenRunner_1_StartTween_m5889_gshared ();
void* RuntimeInvoker_Void_t260_ColorTween_t1209 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_get_Capacity_m5891_gshared ();
extern "C" void List_1_set_Capacity_m5892_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t189_m5915_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisByte_t237_m5917_gshared ();
void* RuntimeInvoker_Boolean_t203_ByteU26_t2328_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t202_m5919_gshared ();
void* RuntimeInvoker_Boolean_t203_SingleU26_t316_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m5971_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisUInt16_t194_m5967_gshared ();
void* RuntimeInvoker_Boolean_t203_UInt16U26_t3017_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ToArray_m6018_gshared ();
extern "C" void UnityEvent_1__ctor_m6044_gshared ();
extern "C" void UnityEvent_1_Invoke_m6050_gshared ();
void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m6055_gshared ();
extern "C" void UnityAction_1__ctor_m6056_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m6057_gshared ();
extern "C" void UnityEvent_1_AddListener_m6058_gshared ();
extern "C" void UnityEvent_1_Invoke_m6062_gshared ();
void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t1272_m6078_gshared ();
void* RuntimeInvoker_Boolean_t203_NavigationU26_t4645_Navigation_t1272 (MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t1221_m6080_gshared ();
void* RuntimeInvoker_Boolean_t203_ColorBlockU26_t4646_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t1290_m6081_gshared ();
void* RuntimeInvoker_Boolean_t203_SpriteStateU26_t4647_SpriteState_t1290 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m20913_gshared ();
extern "C" void UnityEvent_1_Invoke_m20921_gshared ();
void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisInt32_t189_m6138_gshared ();
void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisVector2_t739_m6140_gshared ();
void* RuntimeInvoker_Void_t260_Vector2U26_t1725_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisSingle_t202_m6145_gshared ();
void* RuntimeInvoker_Void_t260_SingleU26_t316_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisByte_t237_m6147_gshared ();
void* RuntimeInvoker_Void_t260_ByteU26_t2328_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m24055_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m6304_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m6305_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m6312_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m6313_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m6314_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m6315_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m20917_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20918_gshared ();
extern "C" void List_1__ctor_m7513_gshared ();
extern "C" void List_1__ctor_m7514_gshared ();
extern "C" void List_1__ctor_m7515_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m7535_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m7536_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m25314_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m25528_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t189_m8861_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m14423_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14424_gshared ();
extern "C" void GenericComparer_1__ctor_m14425_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14426_gshared ();
extern "C" void GenericComparer_1__ctor_m14427_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14428_gshared ();
extern "C" void GenericComparer_1__ctor_m14429_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m14430_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3407_m26171_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3407_m26172_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3407_m26173_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3407_m26174_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3407_m26175_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3407_m26176_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3407_m26177_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3407_m26179_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3407_m26191_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t189_m26199_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t189_m26200_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t189_m26201_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t189_m26202_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t189_m26203_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t189_m26204_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t189_m26205_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t189_m26207_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t189_m26208_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t234_m26210_gshared ();
void* RuntimeInvoker_Double_t234_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t234_m26211_gshared ();
void* RuntimeInvoker_Void_t260_Double_t234 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t234_m26212_gshared ();
void* RuntimeInvoker_Boolean_t203_Double_t234 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t234_m26213_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t234_m26214_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t234_m26215_gshared ();
void* RuntimeInvoker_Int32_t189_Double_t234 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDouble_t234_m26216_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Double_t234 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDouble_t234_m26218_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t234_m26219_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t194_m26221_gshared ();
void* RuntimeInvoker_UInt16_t194_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t194_m26222_gshared ();
void* RuntimeInvoker_Void_t260_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t194_m26223_gshared ();
void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t194_m26224_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t194_m26225_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t194_m26226_gshared ();
void* RuntimeInvoker_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt16_t194_m26227_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt16_t194_m26229_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t194_m26230_gshared ();
extern "C" void List_1_ConvertAll_TisKeyValuePair_2_t3407_m26236_gshared ();
extern "C" void Array_Resize_TisKeyValuePair_2_t3407_m26238_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisKeyValuePair_2_t3407_m26237_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisKeyValuePair_2_t3407_m26239_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_KeyValuePair_2_t3407_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisKeyValuePair_2_t3407_m26241_gshared ();
extern "C" void Array_Sort_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26240_gshared ();
extern "C" void Array_get_swapper_TisKeyValuePair_2_t3407_m26242_gshared ();
extern "C" void Array_qsort_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26243_gshared ();
extern "C" void Array_compare_TisKeyValuePair_2_t3407_m26244_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26245_gshared ();
extern "C" void Array_Sort_TisKeyValuePair_2_t3407_m26247_gshared ();
extern "C" void Array_qsort_TisKeyValuePair_2_t3407_m26246_gshared ();
extern "C" void Array_swap_TisKeyValuePair_2_t3407_m26248_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t2128_m26252_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t2128_m26253_gshared ();
void* RuntimeInvoker_Void_t260_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t2128_m26254_gshared ();
void* RuntimeInvoker_Boolean_t203_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t2128_m26255_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t2128_m26256_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t2128_m26257_gshared ();
void* RuntimeInvoker_Int32_t189_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t2128_m26258_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t2128_m26260_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t2128_m26261_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26262_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3407_m26264_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26265_gshared ();
extern "C" void Array_IndexOf_TisUInt16_t194_m26266_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int16_t238_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3531_m26268_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3531_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3531_m26269_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3531_m26270_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3531_m26271_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3531_m26272_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3531_m26273_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3531_m26274_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3531_m26276_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3531_m26277_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26279_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26278_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26281_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26282_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26283_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3531_m26285_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3531_TisKeyValuePair_2_t3531_m26286_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t237_m26288_gshared ();
void* RuntimeInvoker_Byte_t237_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t237_m26289_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t237_m26290_gshared ();
void* RuntimeInvoker_Boolean_t203_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t237_m26291_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t237_m26292_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t237_m26293_gshared ();
void* RuntimeInvoker_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisByte_t237_m26294_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisByte_t237_m26296_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t237_m26297_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t3579_m26299_gshared ();
void* RuntimeInvoker_Link_t3579_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t3579_m26300_gshared ();
void* RuntimeInvoker_Void_t260_Link_t3579 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t3579_m26301_gshared ();
void* RuntimeInvoker_Boolean_t203_Link_t3579 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t3579_m26302_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t3579_m26303_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t3579_m26304_gshared ();
void* RuntimeInvoker_Int32_t189_Link_t3579 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t3579_m26305_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Link_t3579 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t3579_m26307_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3579_m26308_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3656_m26312_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3656_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3656_m26313_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3656_m26314_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3656_m26315_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3656_m26316_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3656_m26317_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3656_m26318_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3656_m26320_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3656_m26321_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t235_m26323_gshared ();
void* RuntimeInvoker_UInt32_t235_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t235_m26324_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t235_m26325_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t235_m26326_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t235_m26327_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t235_m26328_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t235_m26329_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t235_m26331_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t235_m26332_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26334_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26333_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt32_t235_m26336_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt32_t235_TisUInt32_t235_m26337_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26338_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3656_m26340_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3656_TisKeyValuePair_2_t3656_m26341_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m26351_gshared ();
void* RuntimeInvoker_IntPtr_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m26352_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m26353_gshared ();
void* RuntimeInvoker_Boolean_t203_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m26354_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m26355_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m26356_gshared ();
void* RuntimeInvoker_Int32_t189_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m26357_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m26359_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m26360_gshared ();
extern "C" void NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisByte_t237_m26364_gshared ();
extern "C" void NativeClient_InvokeCallbackOnGameThread_TisInt32_t189_m26366_gshared ();
extern "C" void NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisInt32_t189_m26365_gshared ();
extern "C" void CallbackUtils_U3CToOnGameThread_2U3Em__6_TisInt32_t189_TisObject_t_m26367_gshared ();
extern "C" void CallbackUtils_U3CToOnGameThread_3U3Em__8_TisInt32_t189_TisObject_t_TisObject_t_m26369_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisLink_t3760_m26375_gshared ();
void* RuntimeInvoker_Link_t3760_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t3760_m26376_gshared ();
void* RuntimeInvoker_Void_t260_Link_t3760 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t3760_m26377_gshared ();
void* RuntimeInvoker_Boolean_t203_Link_t3760 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t3760_m26378_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t3760_m26379_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t3760_m26380_gshared ();
void* RuntimeInvoker_Int32_t189_Link_t3760 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t3760_m26381_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Link_t3760 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t3760_m26383_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3760_m26384_gshared ();
extern "C" void Enumerable_CreateSelectIterator_TisIntPtr_t_TisObject_t_m26396_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3790_m26398_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3790_m26399_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3790_m26400_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3790_m26401_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3790_m26402_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3790_m26403_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3790_m26404_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3790_m26406_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3790_m26407_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26410_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26409_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26411_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26412_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3790_m26414_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisKeyValuePair_2_t3790_m26415_gshared ();
extern "C" void Enumerable_CreateSelectIterator_TisObject_t_TisIntPtr_t_m26418_gshared ();
extern "C" void Array_Resize_TisIntPtr_t_m26420_gshared ();
void* RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisIntPtr_t_m26419_gshared ();
void* RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisIntPtr_t_m26421_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_IntPtr_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisIntPtr_t_m26423_gshared ();
extern "C" void Array_Sort_TisIntPtr_t_TisIntPtr_t_m26422_gshared ();
extern "C" void Array_get_swapper_TisIntPtr_t_m26424_gshared ();
extern "C" void Array_qsort_TisIntPtr_t_TisIntPtr_t_m26425_gshared ();
extern "C" void Array_compare_TisIntPtr_t_m26426_gshared ();
void* RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisIntPtr_t_TisIntPtr_t_m26427_gshared ();
extern "C" void Array_Sort_TisIntPtr_t_m26429_gshared ();
extern "C" void Array_qsort_TisIntPtr_t_m26428_gshared ();
extern "C" void Array_swap_TisIntPtr_t_m26430_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisByte_t237_m26431_gshared ();
extern "C" void Array_InternalArray__get_Item_TisMatrix4x4_t997_m26433_gshared ();
void* RuntimeInvoker_Matrix4x4_t997_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMatrix4x4_t997_m26434_gshared ();
void* RuntimeInvoker_Void_t260_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMatrix4x4_t997_m26435_gshared ();
void* RuntimeInvoker_Boolean_t203_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMatrix4x4_t997_m26436_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMatrix4x4_t997_m26437_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMatrix4x4_t997_m26438_gshared ();
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMatrix4x4_t997_m26439_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMatrix4x4_t997_m26441_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMatrix4x4_t997_m26442_gshared ();
extern "C" void Array_Resize_TisMatrix4x4_t997_m26444_gshared ();
void* RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisMatrix4x4_t997_m26443_gshared ();
void* RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisMatrix4x4_t997_m26445_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Matrix4x4_t997_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisMatrix4x4_t997_m26447_gshared ();
extern "C" void Array_Sort_TisMatrix4x4_t997_TisMatrix4x4_t997_m26446_gshared ();
extern "C" void Array_get_swapper_TisMatrix4x4_t997_m26448_gshared ();
extern "C" void Array_qsort_TisMatrix4x4_t997_TisMatrix4x4_t997_m26449_gshared ();
extern "C" void Array_compare_TisMatrix4x4_t997_m26450_gshared ();
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisMatrix4x4_t997_TisMatrix4x4_t997_m26451_gshared ();
extern "C" void Array_Sort_TisMatrix4x4_t997_m26453_gshared ();
extern "C" void Array_qsort_TisMatrix4x4_t997_m26452_gshared ();
extern "C" void Array_swap_TisMatrix4x4_t997_m26454_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t758_m26456_gshared ();
void* RuntimeInvoker_Vector3_t758_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t758_m26457_gshared ();
void* RuntimeInvoker_Void_t260_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t758_m26458_gshared ();
void* RuntimeInvoker_Boolean_t203_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t758_m26459_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t758_m26460_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t758_m26461_gshared ();
void* RuntimeInvoker_Int32_t189_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector3_t758_m26462_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector3_t758_m26464_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t758_m26465_gshared ();
extern "C" void Array_Resize_TisVector3_t758_m26467_gshared ();
void* RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisVector3_t758_m26466_gshared ();
void* RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisVector3_t758_m26468_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Vector3_t758_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisVector3_t758_m26470_gshared ();
extern "C" void Array_Sort_TisVector3_t758_TisVector3_t758_m26469_gshared ();
extern "C" void Array_get_swapper_TisVector3_t758_m26471_gshared ();
extern "C" void Array_qsort_TisVector3_t758_TisVector3_t758_m26472_gshared ();
extern "C" void Array_compare_TisVector3_t758_m26473_gshared ();
void* RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisVector3_t758_TisVector3_t758_m26474_gshared ();
extern "C" void Array_Sort_TisVector3_t758_m26476_gshared ();
extern "C" void Array_qsort_TisVector3_t758_m26475_gshared ();
extern "C" void Array_swap_TisVector3_t758_m26477_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTouch_t901_m26479_gshared ();
void* RuntimeInvoker_Touch_t901_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTouch_t901_m26480_gshared ();
void* RuntimeInvoker_Void_t260_Touch_t901 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTouch_t901_m26481_gshared ();
void* RuntimeInvoker_Boolean_t203_Touch_t901 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTouch_t901_m26482_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTouch_t901_m26483_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTouch_t901_m26484_gshared ();
void* RuntimeInvoker_Int32_t189_Touch_t901 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTouch_t901_m26485_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Touch_t901 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTouch_t901_m26487_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTouch_t901_m26488_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t1187_m26495_gshared ();
void* RuntimeInvoker_RaycastResult_t1187_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t1187_m26496_gshared ();
void* RuntimeInvoker_Void_t260_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t1187_m26497_gshared ();
void* RuntimeInvoker_Boolean_t203_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t1187_m26498_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t1187_m26499_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t1187_m26500_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t1187_m26501_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t1187_m26503_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t1187_m26504_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t1187_m26506_gshared ();
void* RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisRaycastResult_t1187_m26505_gshared ();
void* RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisRaycastResult_t1187_m26507_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_RaycastResult_t1187_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisRaycastResult_t1187_m26509_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t1187_TisRaycastResult_t1187_m26508_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t1187_m26510_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t1187_TisRaycastResult_t1187_m26511_gshared ();
extern "C" void Array_compare_TisRaycastResult_t1187_m26512_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisRaycastResult_t1187_TisRaycastResult_t1187_m26513_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t1187_m26515_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t1187_m26514_gshared ();
extern "C" void Array_swap_TisRaycastResult_t1187_m26516_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t3921_m26520_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3921_m26521_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3921_m26522_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3921_m26523_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3921_m26524_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t3921_m26525_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t3921_m26526_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t3921_m26528_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3921_m26529_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26532_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26531_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26533_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26534_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26535_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3921_m26537_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3921_TisKeyValuePair_2_t3921_m26538_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t1378_m26540_gshared ();
void* RuntimeInvoker_RaycastHit2D_t1378_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t1378_m26541_gshared ();
void* RuntimeInvoker_Void_t260_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t1378_m26542_gshared ();
void* RuntimeInvoker_Boolean_t203_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t1378_m26543_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t1378_m26544_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t1378_m26545_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t1378_m26546_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t1378_m26548_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t1378_m26549_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t990_m26551_gshared ();
void* RuntimeInvoker_RaycastHit_t990_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t990_m26552_gshared ();
void* RuntimeInvoker_Void_t260_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t990_m26553_gshared ();
void* RuntimeInvoker_Boolean_t203_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t990_m26554_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t990_m26555_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t990_m26556_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t990_m26557_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t990_m26559_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t990_m26560_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t990_m26561_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t990_m26562_gshared ();
extern "C" void Array_swap_TisRaycastHit_t990_m26563_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t747_m26564_gshared ();
extern "C" void Array_Resize_TisUIVertex_t1265_m26566_gshared ();
void* RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUIVertex_t1265_m26565_gshared ();
void* RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUIVertex_t1265_m26567_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_UIVertex_t1265_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUIVertex_t1265_m26569_gshared ();
extern "C" void Array_Sort_TisUIVertex_t1265_TisUIVertex_t1265_m26568_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t1265_m26570_gshared ();
extern "C" void Array_qsort_TisUIVertex_t1265_TisUIVertex_t1265_m26571_gshared ();
extern "C" void Array_compare_TisUIVertex_t1265_m26572_gshared ();
void* RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUIVertex_t1265_TisUIVertex_t1265_m26573_gshared ();
extern "C" void Array_Sort_TisUIVertex_t1265_m26575_gshared ();
extern "C" void Array_qsort_TisUIVertex_t1265_m26574_gshared ();
extern "C" void Array_swap_TisUIVertex_t1265_m26576_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t739_m26578_gshared ();
void* RuntimeInvoker_Vector2_t739_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t739_m26579_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t739_m26580_gshared ();
void* RuntimeInvoker_Boolean_t203_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t739_m26581_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t739_m26582_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t739_m26583_gshared ();
void* RuntimeInvoker_Int32_t189_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector2_t739_m26584_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector2_t739_m26586_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t739_m26587_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t1398_m26589_gshared ();
void* RuntimeInvoker_UILineInfo_t1398_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t1398_m26590_gshared ();
void* RuntimeInvoker_Void_t260_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t1398_m26591_gshared ();
void* RuntimeInvoker_Boolean_t203_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t1398_m26592_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t1398_m26593_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t1398_m26594_gshared ();
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t1398_m26595_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t1398_m26597_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t1398_m26598_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t1400_m26600_gshared ();
void* RuntimeInvoker_UICharInfo_t1400_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t1400_m26601_gshared ();
void* RuntimeInvoker_Void_t260_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t1400_m26602_gshared ();
void* RuntimeInvoker_Boolean_t203_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t1400_m26603_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t1400_m26604_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t1400_m26605_gshared ();
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t1400_m26606_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t1400_m26608_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t1400_m26609_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t202_m26610_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t739_m26611_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t1617_m26615_gshared ();
void* RuntimeInvoker_GcAchievementData_t1617_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t1617_m26616_gshared ();
void* RuntimeInvoker_Void_t260_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1617_m26617_gshared ();
void* RuntimeInvoker_Boolean_t203_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1617_m26618_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1617_m26619_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t1617_m26620_gshared ();
void* RuntimeInvoker_Int32_t189_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t1617_m26621_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t1617_m26623_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1617_m26624_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t1618_m26626_gshared ();
void* RuntimeInvoker_GcScoreData_t1618_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t1618_m26627_gshared ();
void* RuntimeInvoker_Void_t260_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t1618_m26628_gshared ();
void* RuntimeInvoker_Boolean_t203_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1618_m26629_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t1618_m26630_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t1618_m26631_gshared ();
void* RuntimeInvoker_Int32_t189_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t1618_m26632_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t1618_m26634_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1618_m26635_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t202_m26637_gshared ();
void* RuntimeInvoker_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t202_m26638_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t202_m26639_gshared ();
void* RuntimeInvoker_Boolean_t203_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t202_m26640_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t202_m26641_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t202_m26642_gshared ();
void* RuntimeInvoker_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSingle_t202_m26643_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSingle_t202_m26645_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t202_m26646_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t1591_m26648_gshared ();
void* RuntimeInvoker_Keyframe_t1591_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t1591_m26649_gshared ();
void* RuntimeInvoker_Void_t260_Keyframe_t1591 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t1591_m26650_gshared ();
void* RuntimeInvoker_Boolean_t203_Keyframe_t1591 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1591_m26651_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t1591_m26652_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t1591_m26653_gshared ();
void* RuntimeInvoker_Int32_t189_Keyframe_t1591 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyframe_t1591_m26654_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Keyframe_t1591 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t1591_m26656_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1591_m26657_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t1400_m26659_gshared ();
void* RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUICharInfo_t1400_m26658_gshared ();
void* RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUICharInfo_t1400_m26660_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_UICharInfo_t1400_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUICharInfo_t1400_m26662_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t1400_TisUICharInfo_t1400_m26661_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t1400_m26663_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t1400_TisUICharInfo_t1400_m26664_gshared ();
extern "C" void Array_compare_TisUICharInfo_t1400_m26665_gshared ();
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUICharInfo_t1400_TisUICharInfo_t1400_m26666_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t1400_m26668_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t1400_m26667_gshared ();
extern "C" void Array_swap_TisUICharInfo_t1400_m26669_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t1398_m26671_gshared ();
void* RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUILineInfo_t1398_m26670_gshared ();
void* RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUILineInfo_t1398_m26672_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_UILineInfo_t1398_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUILineInfo_t1398_m26674_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t1398_TisUILineInfo_t1398_m26673_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t1398_m26675_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t1398_TisUILineInfo_t1398_m26676_gshared ();
extern "C" void Array_compare_TisUILineInfo_t1398_m26677_gshared ();
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUILineInfo_t1398_TisUILineInfo_t1398_m26678_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t1398_m26680_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t1398_m26679_gshared ();
extern "C" void Array_swap_TisUILineInfo_t1398_m26681_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t2550_m26683_gshared ();
void* RuntimeInvoker_ParameterModifier_t2550_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t2550_m26684_gshared ();
void* RuntimeInvoker_Void_t260_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t2550_m26685_gshared ();
void* RuntimeInvoker_Boolean_t203_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2550_m26686_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t2550_m26687_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t2550_m26688_gshared ();
void* RuntimeInvoker_Int32_t189_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t2550_m26689_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t2550_m26691_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2550_m26692_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t1629_m26694_gshared ();
void* RuntimeInvoker_HitInfo_t1629_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t1629_m26695_gshared ();
void* RuntimeInvoker_Void_t260_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t1629_m26696_gshared ();
void* RuntimeInvoker_Boolean_t203_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1629_m26697_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t1629_m26698_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t1629_m26699_gshared ();
void* RuntimeInvoker_Int32_t189_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisHitInfo_t1629_m26700_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t1629_m26702_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1629_m26703_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t189_m26704_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t4171_m26706_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t4171_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4171_m26707_gshared ();
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4171_m26708_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4171_m26709_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4171_m26710_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t4171_m26711_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t4171_m26712_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t4171_m26714_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4171_m26715_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26717_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26716_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t237_m26719_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t237_TisByte_t237_m26720_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26721_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4171_m26723_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4171_TisKeyValuePair_2_t4171_m26724_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t2034_m26726_gshared ();
void* RuntimeInvoker_X509ChainStatus_t2034_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t2034_m26727_gshared ();
void* RuntimeInvoker_Void_t260_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t2034_m26728_gshared ();
void* RuntimeInvoker_Boolean_t203_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t2034_m26729_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t2034_m26730_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t2034_m26731_gshared ();
void* RuntimeInvoker_Int32_t189_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t2034_m26732_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t2034_m26734_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t2034_m26735_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t189_m26736_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisMark_t2083_m26738_gshared ();
void* RuntimeInvoker_Mark_t2083_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t2083_m26739_gshared ();
void* RuntimeInvoker_Void_t260_Mark_t2083 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t2083_m26740_gshared ();
void* RuntimeInvoker_Boolean_t203_Mark_t2083 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t2083_m26741_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t2083_m26742_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t2083_m26743_gshared ();
void* RuntimeInvoker_Int32_t189_Mark_t2083 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMark_t2083_m26744_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Mark_t2083 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMark_t2083_m26746_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t2083_m26747_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t2118_m26749_gshared ();
void* RuntimeInvoker_UriScheme_t2118_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t2118_m26750_gshared ();
void* RuntimeInvoker_Void_t260_UriScheme_t2118 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t2118_m26751_gshared ();
void* RuntimeInvoker_Boolean_t203_UriScheme_t2118 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t2118_m26752_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t2118_m26753_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t2118_m26754_gshared ();
void* RuntimeInvoker_Int32_t189_UriScheme_t2118 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUriScheme_t2118_m26755_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_UriScheme_t2118 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t2118_m26757_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t2118_m26758_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t239_m26760_gshared ();
void* RuntimeInvoker_UInt64_t239_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t239_m26761_gshared ();
void* RuntimeInvoker_Void_t260_Int64_t233 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t239_m26762_gshared ();
void* RuntimeInvoker_Boolean_t203_Int64_t233 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t239_m26763_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t239_m26764_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t239_m26765_gshared ();
void* RuntimeInvoker_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt64_t239_m26766_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt64_t239_m26768_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t239_m26769_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t238_m26771_gshared ();
void* RuntimeInvoker_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t238_m26772_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t238_m26773_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t238_m26774_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t238_m26775_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t238_m26776_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t238_m26777_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t238_m26779_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t238_m26780_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t236_m26782_gshared ();
void* RuntimeInvoker_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t236_m26783_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t236_m26784_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t236_m26785_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t236_m26786_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t236_m26787_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t236_m26788_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t236_m26790_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t236_m26791_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t233_m26793_gshared ();
void* RuntimeInvoker_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t233_m26794_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t233_m26795_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t233_m26796_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t233_m26797_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t233_m26798_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt64_t233_m26799_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt64_t233_m26801_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t233_m26802_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t2360_m26832_gshared ();
void* RuntimeInvoker_TableRange_t2360_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t2360_m26833_gshared ();
void* RuntimeInvoker_Void_t260_TableRange_t2360 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t2360_m26834_gshared ();
void* RuntimeInvoker_Boolean_t203_TableRange_t2360 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t2360_m26835_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t2360_m26836_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t2360_m26837_gshared ();
void* RuntimeInvoker_Int32_t189_TableRange_t2360 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTableRange_t2360_m26838_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_TableRange_t2360 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTableRange_t2360_m26840_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2360_m26841_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2436_m26843_gshared ();
void* RuntimeInvoker_Slot_t2436_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2436_m26844_gshared ();
void* RuntimeInvoker_Void_t260_Slot_t2436 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2436_m26845_gshared ();
void* RuntimeInvoker_Boolean_t203_Slot_t2436 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2436_m26846_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2436_m26847_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2436_m26848_gshared ();
void* RuntimeInvoker_Int32_t189_Slot_t2436 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2436_m26849_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Slot_t2436 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2436_m26851_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2436_m26852_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t2443_m26854_gshared ();
void* RuntimeInvoker_Slot_t2443_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t2443_m26855_gshared ();
void* RuntimeInvoker_Void_t260_Slot_t2443 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t2443_m26856_gshared ();
void* RuntimeInvoker_Boolean_t203_Slot_t2443 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t2443_m26857_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t2443_m26858_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t2443_m26859_gshared ();
void* RuntimeInvoker_Int32_t189_Slot_t2443 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t2443_m26860_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Slot_t2443 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t2443_m26862_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2443_m26863_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t48_m26867_gshared ();
void* RuntimeInvoker_DateTime_t48_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t48_m26868_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t48_m26869_gshared ();
void* RuntimeInvoker_Boolean_t203_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t48_m26870_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t48_m26871_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t48_m26872_gshared ();
void* RuntimeInvoker_Int32_t189_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDateTime_t48_m26873_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDateTime_t48_m26875_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t48_m26876_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t240_m26878_gshared ();
void* RuntimeInvoker_Decimal_t240_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t240_m26879_gshared ();
void* RuntimeInvoker_Void_t260_Decimal_t240 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t240_m26880_gshared ();
void* RuntimeInvoker_Boolean_t203_Decimal_t240 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t240_m26881_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t240_m26882_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t240_m26883_gshared ();
void* RuntimeInvoker_Int32_t189_Decimal_t240 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDecimal_t240_m26884_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_Decimal_t240 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDecimal_t240_m26886_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t240_m26887_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t190_m26889_gshared ();
void* RuntimeInvoker_TimeSpan_t190_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t190_m26890_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t190_m26891_gshared ();
void* RuntimeInvoker_Boolean_t203_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t190_m26892_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t190_m26893_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t190_m26894_gshared ();
void* RuntimeInvoker_Int32_t189_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t190_m26895_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t190_m26897_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t190_m26898_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15298_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15299_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15300_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15301_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15302_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15521_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15523_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15524_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15525_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15526_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15527_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15528_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15529_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15530_gshared ();
void* RuntimeInvoker_Double_t234 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15532_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15534_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15536_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15538_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15540_gshared ();
void* RuntimeInvoker_UInt16_t194 (MethodInfo* method, void* obj, void** args);
extern "C" void Converter_2__ctor_m15610_gshared ();
extern "C" void Converter_2_Invoke_m15612_gshared ();
extern "C" void Converter_2_BeginInvoke_m15614_gshared ();
extern "C" void Converter_2_EndInvoke_m15616_gshared ();
extern "C" void List_1__ctor_m15617_gshared ();
extern "C" void List_1__ctor_m15618_gshared ();
extern "C" void List_1__ctor_m15619_gshared ();
extern "C" void List_1__cctor_m15620_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15621_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15622_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15623_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15624_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15625_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15626_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15627_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15628_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15629_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15630_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15631_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15632_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15633_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15634_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15635_gshared ();
extern "C" void List_1_Add_m15636_gshared ();
extern "C" void List_1_GrowIfNeeded_m15637_gshared ();
extern "C" void List_1_AddCollection_m15638_gshared ();
extern "C" void List_1_AddEnumerable_m15639_gshared ();
extern "C" void List_1_AddRange_m15640_gshared ();
extern "C" void List_1_AsReadOnly_m15641_gshared ();
extern "C" void List_1_Clear_m15642_gshared ();
extern "C" void List_1_Contains_m15643_gshared ();
extern "C" void List_1_CopyTo_m15644_gshared ();
extern "C" void List_1_Find_m15645_gshared ();
extern "C" void List_1_CheckMatch_m15646_gshared ();
extern "C" void List_1_FindIndex_m15647_gshared ();
extern "C" void List_1_GetIndex_m15648_gshared ();
extern "C" void List_1_ForEach_m15649_gshared ();
extern "C" void List_1_GetEnumerator_m15650_gshared ();
void* RuntimeInvoker_Enumerator_t3440 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15651_gshared ();
extern "C" void List_1_Shift_m15652_gshared ();
extern "C" void List_1_CheckIndex_m15653_gshared ();
extern "C" void List_1_Insert_m15654_gshared ();
extern "C" void List_1_CheckCollection_m15655_gshared ();
extern "C" void List_1_Remove_m15656_gshared ();
extern "C" void List_1_RemoveAll_m15657_gshared ();
extern "C" void List_1_RemoveAt_m15658_gshared ();
extern "C" void List_1_Reverse_m15659_gshared ();
extern "C" void List_1_Sort_m15660_gshared ();
extern "C" void List_1_Sort_m15661_gshared ();
extern "C" void List_1_ToArray_m15662_gshared ();
extern "C" void List_1_TrimExcess_m15663_gshared ();
extern "C" void List_1_get_Capacity_m15664_gshared ();
extern "C" void List_1_set_Capacity_m15665_gshared ();
extern "C" void List_1_get_Count_m15666_gshared ();
extern "C" void List_1_get_Item_m15667_gshared ();
extern "C" void List_1_set_Item_m15668_gshared ();
extern "C" void Enumerator__ctor_m15669_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15670_gshared ();
extern "C" void Enumerator_Dispose_m15671_gshared ();
extern "C" void Enumerator_VerifyState_m15672_gshared ();
extern "C" void Enumerator_MoveNext_m15673_gshared ();
extern "C" void Enumerator_get_Current_m15674_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15675_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15676_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15677_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15678_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15679_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15680_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15681_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15682_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15683_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15684_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15685_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15686_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15687_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15688_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15689_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15690_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15691_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15692_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15693_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15694_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15695_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15696_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15697_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15698_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15699_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15700_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15701_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15702_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15703_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15704_gshared ();
extern "C" void Collection_1__ctor_m15705_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15706_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15707_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15708_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15709_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15710_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15711_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15712_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15713_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15714_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15715_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15716_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15717_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15718_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15719_gshared ();
extern "C" void Collection_1_Add_m15720_gshared ();
extern "C" void Collection_1_Clear_m15721_gshared ();
extern "C" void Collection_1_ClearItems_m15722_gshared ();
extern "C" void Collection_1_Contains_m15723_gshared ();
extern "C" void Collection_1_CopyTo_m15724_gshared ();
extern "C" void Collection_1_GetEnumerator_m15725_gshared ();
extern "C" void Collection_1_IndexOf_m15726_gshared ();
extern "C" void Collection_1_Insert_m15727_gshared ();
extern "C" void Collection_1_InsertItem_m15728_gshared ();
extern "C" void Collection_1_Remove_m15729_gshared ();
extern "C" void Collection_1_RemoveAt_m15730_gshared ();
extern "C" void Collection_1_RemoveItem_m15731_gshared ();
extern "C" void Collection_1_get_Count_m15732_gshared ();
extern "C" void Collection_1_get_Item_m15733_gshared ();
extern "C" void Collection_1_set_Item_m15734_gshared ();
extern "C" void Collection_1_SetItem_m15735_gshared ();
extern "C" void Collection_1_IsValidItem_m15736_gshared ();
extern "C" void Collection_1_ConvertItem_m15737_gshared ();
extern "C" void Collection_1_CheckWritable_m15738_gshared ();
extern "C" void Collection_1_IsSynchronized_m15739_gshared ();
extern "C" void Collection_1_IsFixedSize_m15740_gshared ();
extern "C" void EqualityComparer_1__ctor_m15741_gshared ();
extern "C" void EqualityComparer_1__cctor_m15742_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15743_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15744_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15745_gshared ();
extern "C" void DefaultComparer__ctor_m15746_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15747_gshared ();
extern "C" void DefaultComparer_Equals_m15748_gshared ();
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void Converter_2__ctor_m15749_gshared ();
extern "C" void Converter_2_Invoke_m15750_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void Converter_2_BeginInvoke_m15751_gshared ();
void* RuntimeInvoker_Object_t_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Converter_2_EndInvoke_m15752_gshared ();
extern "C" void Predicate_1__ctor_m15753_gshared ();
extern "C" void Predicate_1_Invoke_m15754_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15755_gshared ();
extern "C" void Predicate_1_EndInvoke_m15756_gshared ();
extern "C" void Action_1__ctor_m15757_gshared ();
extern "C" void Action_1_Invoke_m15758_gshared ();
extern "C" void Action_1_BeginInvoke_m15759_gshared ();
extern "C" void Action_1_EndInvoke_m15760_gshared ();
extern "C" void Comparer_1__ctor_m15761_gshared ();
extern "C" void Comparer_1__cctor_m15762_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15763_gshared ();
extern "C" void Comparer_1_get_Default_m15764_gshared ();
extern "C" void DefaultComparer__ctor_m15765_gshared ();
extern "C" void DefaultComparer_Compare_m15766_gshared ();
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m15767_gshared ();
extern "C" void Comparison_1_Invoke_m15768_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15769_gshared ();
void* RuntimeInvoker_Object_t_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15770_gshared ();
extern "C" void Transform_1__ctor_m16126_gshared ();
extern "C" void Transform_1_Invoke_m16128_gshared ();
extern "C" void Transform_1_BeginInvoke_m16130_gshared ();
extern "C" void Transform_1_EndInvoke_m16132_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16315_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16316_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16317_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16318_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16319_gshared ();
extern "C" void Transform_1__ctor_m16320_gshared ();
extern "C" void Transform_1_Invoke_m16321_gshared ();
extern "C" void Transform_1_BeginInvoke_m16322_gshared ();
extern "C" void Transform_1_EndInvoke_m16323_gshared ();
extern "C" void EqualityComparer_1__ctor_m16620_gshared ();
extern "C" void EqualityComparer_1__cctor_m16621_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16624_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16625_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16626_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16627_gshared ();
void* RuntimeInvoker_Boolean_t203_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m16628_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16629_gshared ();
extern "C" void DefaultComparer_Equals_m16630_gshared ();
extern "C" void Dictionary_2__ctor_m17006_gshared ();
extern "C" void Dictionary_2__ctor_m17009_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m17011_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m17013_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17015_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17017_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17019_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17021_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17023_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17025_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17027_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17029_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17031_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17033_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17035_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17037_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17039_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17041_gshared ();
extern "C" void Dictionary_2_get_Count_m17043_gshared ();
extern "C" void Dictionary_2_get_Item_m17045_gshared ();
extern "C" void Dictionary_2_set_Item_m17047_gshared ();
extern "C" void Dictionary_2_Init_m17049_gshared ();
extern "C" void Dictionary_2_InitArrays_m17051_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m17053_gshared ();
extern "C" void Dictionary_2_make_pair_m17055_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3531_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m17057_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m17059_gshared ();
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m17061_gshared ();
extern "C" void Dictionary_2_Resize_m17063_gshared ();
extern "C" void Dictionary_2_Add_m17065_gshared ();
extern "C" void Dictionary_2_Clear_m17067_gshared ();
extern "C" void Dictionary_2_ContainsKey_m17069_gshared ();
extern "C" void Dictionary_2_ContainsValue_m17071_gshared ();
extern "C" void Dictionary_2_GetObjectData_m17073_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m17075_gshared ();
extern "C" void Dictionary_2_Remove_m17077_gshared ();
extern "C" void Dictionary_2_TryGetValue_m17079_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m17081_gshared ();
extern "C" void Dictionary_2_get_Values_m17083_gshared ();
extern "C" void Dictionary_2_ToTKey_m17085_gshared ();
extern "C" void Dictionary_2_ToTValue_m17087_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m17089_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m17091_gshared ();
void* RuntimeInvoker_Enumerator_t3535 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m17093_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17094_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17095_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17096_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17097_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17098_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m17099_gshared ();
extern "C" void KeyValuePair_2_get_Key_m17100_gshared ();
extern "C" void KeyValuePair_2_set_Key_m17101_gshared ();
extern "C" void KeyValuePair_2_get_Value_m17102_gshared ();
extern "C" void KeyValuePair_2_set_Value_m17103_gshared ();
extern "C" void KeyValuePair_2_ToString_m17104_gshared ();
extern "C" void KeyCollection__ctor_m17105_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17106_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17107_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17108_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17109_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17110_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17111_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17112_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17113_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17114_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m17115_gshared ();
extern "C" void KeyCollection_CopyTo_m17116_gshared ();
extern "C" void KeyCollection_GetEnumerator_m17117_gshared ();
void* RuntimeInvoker_Enumerator_t3534 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m17118_gshared ();
extern "C" void Enumerator__ctor_m17119_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17120_gshared ();
extern "C" void Enumerator_Dispose_m17121_gshared ();
extern "C" void Enumerator_MoveNext_m17122_gshared ();
extern "C" void Enumerator_get_Current_m17123_gshared ();
extern "C" void Enumerator__ctor_m17124_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared ();
extern "C" void Enumerator_MoveNext_m17129_gshared ();
extern "C" void Enumerator_get_Current_m17130_gshared ();
extern "C" void Enumerator_get_CurrentKey_m17131_gshared ();
extern "C" void Enumerator_get_CurrentValue_m17132_gshared ();
extern "C" void Enumerator_VerifyState_m17133_gshared ();
extern "C" void Enumerator_VerifyCurrent_m17134_gshared ();
extern "C" void Enumerator_Dispose_m17135_gshared ();
extern "C" void Transform_1__ctor_m17136_gshared ();
extern "C" void Transform_1_Invoke_m17137_gshared ();
extern "C" void Transform_1_BeginInvoke_m17138_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m17139_gshared ();
extern "C" void ValueCollection__ctor_m17140_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17141_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17142_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17143_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17144_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17145_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17146_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17147_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17148_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17149_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m17150_gshared ();
extern "C" void ValueCollection_CopyTo_m17151_gshared ();
extern "C" void ValueCollection_GetEnumerator_m17152_gshared ();
void* RuntimeInvoker_Enumerator_t3538 (MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m17153_gshared ();
extern "C" void Enumerator__ctor_m17154_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17155_gshared ();
extern "C" void Enumerator_Dispose_m17156_gshared ();
extern "C" void Enumerator_MoveNext_m17157_gshared ();
extern "C" void Enumerator_get_Current_m17158_gshared ();
extern "C" void Transform_1__ctor_m17159_gshared ();
extern "C" void Transform_1_Invoke_m17160_gshared ();
extern "C" void Transform_1_BeginInvoke_m17161_gshared ();
extern "C" void Transform_1_EndInvoke_m17162_gshared ();
extern "C" void Transform_1__ctor_m17163_gshared ();
extern "C" void Transform_1_Invoke_m17164_gshared ();
extern "C" void Transform_1_BeginInvoke_m17165_gshared ();
extern "C" void Transform_1_EndInvoke_m17166_gshared ();
extern "C" void Transform_1__ctor_m17167_gshared ();
extern "C" void Transform_1_Invoke_m17168_gshared ();
extern "C" void Transform_1_BeginInvoke_m17169_gshared ();
extern "C" void Transform_1_EndInvoke_m17170_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3531_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m17171_gshared ();
extern "C" void ShimEnumerator_MoveNext_m17172_gshared ();
extern "C" void ShimEnumerator_get_Entry_m17173_gshared ();
extern "C" void ShimEnumerator_get_Key_m17174_gshared ();
extern "C" void ShimEnumerator_get_Value_m17175_gshared ();
extern "C" void ShimEnumerator_get_Current_m17176_gshared ();
extern "C" void EqualityComparer_1__ctor_m17177_gshared ();
extern "C" void EqualityComparer_1__cctor_m17178_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17179_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17180_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17181_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m17182_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m17183_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m17184_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m17185_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17186_gshared ();
extern "C" void DefaultComparer_Equals_m17187_gshared ();
extern "C" void Action_3_Invoke_m17252_gshared ();
extern "C" void Action_3_BeginInvoke_m17254_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_3_EndInvoke_m17256_gshared ();
extern "C" void Action_1_Invoke_m17262_gshared ();
extern "C" void Action_1_BeginInvoke_m17264_gshared ();
void* RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m17266_gshared ();
extern "C" void InternalEnumerator_1__ctor_m17272_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17276_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17278_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17280_gshared ();
void* RuntimeInvoker_Byte_t237 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17645_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17646_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17647_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17648_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17649_gshared ();
void* RuntimeInvoker_Link_t3579 (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_Invoke_m18522_gshared ();
void* RuntimeInvoker_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m18524_gshared ();
extern "C" void Func_2_EndInvoke_m18526_gshared ();
extern "C" void Action_1_Invoke_m18541_gshared ();
extern "C" void Action_1_BeginInvoke_m18543_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m18545_gshared ();
extern "C" void Action_2_Invoke_m18650_gshared ();
extern "C" void Action_2_BeginInvoke_m18652_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_2_EndInvoke_m18654_gshared ();
extern "C" void Action_2__ctor_m18854_gshared ();
extern "C" void Action_2_Invoke_m18856_gshared ();
extern "C" void Action_2_BeginInvoke_m18858_gshared ();
void* RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_2_EndInvoke_m18860_gshared ();
extern "C" void Dictionary_2__ctor_m18863_gshared ();
extern "C" void Dictionary_2__ctor_m18865_gshared ();
extern "C" void Dictionary_2__ctor_m18867_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m18869_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m18871_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18873_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18875_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18877_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18893_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899_gshared ();
extern "C" void Dictionary_2_get_Count_m18901_gshared ();
extern "C" void Dictionary_2_get_Item_m18903_gshared ();
void* RuntimeInvoker_UInt32_t235_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m18905_gshared ();
extern "C" void Dictionary_2_Init_m18907_gshared ();
extern "C" void Dictionary_2_InitArrays_m18909_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m18911_gshared ();
extern "C" void Dictionary_2_make_pair_m18913_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3656_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m18915_gshared ();
extern "C" void Dictionary_2_pick_value_m18917_gshared ();
void* RuntimeInvoker_UInt32_t235_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m18919_gshared ();
extern "C" void Dictionary_2_Resize_m18921_gshared ();
extern "C" void Dictionary_2_Add_m18923_gshared ();
extern "C" void Dictionary_2_Clear_m18925_gshared ();
extern "C" void Dictionary_2_ContainsKey_m18927_gshared ();
extern "C" void Dictionary_2_ContainsValue_m18929_gshared ();
extern "C" void Dictionary_2_GetObjectData_m18931_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m18933_gshared ();
extern "C" void Dictionary_2_Remove_m18935_gshared ();
extern "C" void Dictionary_2_TryGetValue_m18937_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_UInt32U26_t318 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m18939_gshared ();
extern "C" void Dictionary_2_get_Values_m18941_gshared ();
extern "C" void Dictionary_2_ToTKey_m18943_gshared ();
extern "C" void Dictionary_2_ToTValue_m18945_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m18947_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m18949_gshared ();
void* RuntimeInvoker_Enumerator_t3661 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m18951_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18952_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18953_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18954_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18955_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18956_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m18957_gshared ();
extern "C" void KeyValuePair_2_get_Key_m18958_gshared ();
extern "C" void KeyValuePair_2_set_Key_m18959_gshared ();
extern "C" void KeyValuePair_2_get_Value_m18960_gshared ();
void* RuntimeInvoker_UInt32_t235 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m18961_gshared ();
extern "C" void KeyValuePair_2_ToString_m18962_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18963_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18964_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18965_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18966_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18967_gshared ();
extern "C" void KeyCollection__ctor_m18968_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18974_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978_gshared ();
extern "C" void KeyCollection_CopyTo_m18979_gshared ();
extern "C" void KeyCollection_GetEnumerator_m18980_gshared ();
void* RuntimeInvoker_Enumerator_t3660 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m18981_gshared ();
extern "C" void Enumerator__ctor_m18982_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18983_gshared ();
extern "C" void Enumerator_Dispose_m18984_gshared ();
extern "C" void Enumerator_MoveNext_m18985_gshared ();
extern "C" void Enumerator_get_Current_m18986_gshared ();
extern "C" void Enumerator__ctor_m18987_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18988_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991_gshared ();
extern "C" void Enumerator_MoveNext_m18992_gshared ();
extern "C" void Enumerator_get_Current_m18993_gshared ();
extern "C" void Enumerator_get_CurrentKey_m18994_gshared ();
extern "C" void Enumerator_get_CurrentValue_m18995_gshared ();
extern "C" void Enumerator_VerifyState_m18996_gshared ();
extern "C" void Enumerator_VerifyCurrent_m18997_gshared ();
extern "C" void Enumerator_Dispose_m18998_gshared ();
extern "C" void Transform_1__ctor_m18999_gshared ();
extern "C" void Transform_1_Invoke_m19000_gshared ();
extern "C" void Transform_1_BeginInvoke_m19001_gshared ();
extern "C" void Transform_1_EndInvoke_m19002_gshared ();
extern "C" void ValueCollection__ctor_m19003_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m19009_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013_gshared ();
extern "C" void ValueCollection_CopyTo_m19014_gshared ();
extern "C" void ValueCollection_GetEnumerator_m19015_gshared ();
void* RuntimeInvoker_Enumerator_t3664 (MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m19016_gshared ();
extern "C" void Enumerator__ctor_m19017_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m19018_gshared ();
extern "C" void Enumerator_Dispose_m19019_gshared ();
extern "C" void Enumerator_MoveNext_m19020_gshared ();
extern "C" void Enumerator_get_Current_m19021_gshared ();
extern "C" void Transform_1__ctor_m19022_gshared ();
extern "C" void Transform_1_Invoke_m19023_gshared ();
extern "C" void Transform_1_BeginInvoke_m19024_gshared ();
extern "C" void Transform_1_EndInvoke_m19025_gshared ();
extern "C" void Transform_1__ctor_m19026_gshared ();
extern "C" void Transform_1_Invoke_m19027_gshared ();
extern "C" void Transform_1_BeginInvoke_m19028_gshared ();
extern "C" void Transform_1_EndInvoke_m19029_gshared ();
extern "C" void Transform_1__ctor_m19030_gshared ();
extern "C" void Transform_1_Invoke_m19031_gshared ();
extern "C" void Transform_1_BeginInvoke_m19032_gshared ();
extern "C" void Transform_1_EndInvoke_m19033_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3656_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m19034_gshared ();
extern "C" void ShimEnumerator_MoveNext_m19035_gshared ();
extern "C" void ShimEnumerator_get_Entry_m19036_gshared ();
extern "C" void ShimEnumerator_get_Key_m19037_gshared ();
extern "C" void ShimEnumerator_get_Value_m19038_gshared ();
extern "C" void ShimEnumerator_get_Current_m19039_gshared ();
extern "C" void EqualityComparer_1__ctor_m19040_gshared ();
extern "C" void EqualityComparer_1__cctor_m19041_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19042_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19043_gshared ();
extern "C" void EqualityComparer_1_get_Default_m19044_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m19045_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m19046_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m19047_gshared ();
extern "C" void DefaultComparer__ctor_m19048_gshared ();
extern "C" void DefaultComparer_GetHashCode_m19049_gshared ();
extern "C" void DefaultComparer_Equals_m19050_gshared ();
extern "C" void Nullable_1_Equals_m19210_gshared ();
extern "C" void Nullable_1_Equals_m19211_gshared ();
void* RuntimeInvoker_Boolean_t203_Nullable_1_t377 (MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m19212_gshared ();
extern "C" void Nullable_1_ToString_m19213_gshared ();
extern "C" void Action_1__ctor_m19214_gshared ();
extern "C" void Action_1_Invoke_m19215_gshared ();
void* RuntimeInvoker_Void_t260_AdvertisingResult_t354 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_BeginInvoke_m19216_gshared ();
void* RuntimeInvoker_Object_t_AdvertisingResult_t354_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m19217_gshared ();
extern "C" void Action_1__ctor_m19218_gshared ();
extern "C" void Action_1_Invoke_m19219_gshared ();
void* RuntimeInvoker_Void_t260_ConnectionRequest_t355 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_BeginInvoke_m19220_gshared ();
void* RuntimeInvoker_Object_t_ConnectionRequest_t355_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m19221_gshared ();
extern "C" void Action_1__ctor_m19222_gshared ();
extern "C" void Action_1_Invoke_m19223_gshared ();
void* RuntimeInvoker_Void_t260_ConnectionResponse_t358 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_BeginInvoke_m19224_gshared ();
void* RuntimeInvoker_Object_t_ConnectionResponse_t358_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_1_EndInvoke_m19225_gshared ();
extern "C" void Nullable_1_get_HasValue_m19230_gshared ();
extern "C" void Nullable_1_get_Value_m19231_gshared ();
void* RuntimeInvoker_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_Equals_m19232_gshared ();
extern "C" void Nullable_1_Equals_m19233_gshared ();
void* RuntimeInvoker_Boolean_t203_Nullable_1_t873 (MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m19234_gshared ();
extern "C" void Nullable_1_ToString_m19235_gshared ();
extern "C" void Action_3_Invoke_m19347_gshared ();
extern "C" void Action_3_BeginInvoke_m19349_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_3_EndInvoke_m19351_gshared ();
extern "C" void InternalEnumerator_1__ctor_m19621_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19622_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m19623_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m19624_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m19625_gshared ();
void* RuntimeInvoker_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_2_Invoke_m19628_gshared ();
extern "C" void Action_2_BeginInvoke_m19630_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_2_EndInvoke_m19632_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19728_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19729_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19746_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19747_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19754_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19755_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19756_gshared ();
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19757_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m19758_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m19759_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m19760_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m19761_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19762_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19763_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19764_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19765_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19885_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19886_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19887_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19888_gshared ();
extern "C" void Action_4_Invoke_m19900_gshared ();
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Action_4_BeginInvoke_m19902_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Action_4_EndInvoke_m19904_gshared ();
extern "C" void HashSet_1__ctor_m20231_gshared ();
extern "C" void HashSet_1__ctor_m20233_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239_gshared ();
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared ();
extern "C" void HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243_gshared ();
extern "C" void HashSet_1_get_Count_m20245_gshared ();
extern "C" void HashSet_1_Init_m20247_gshared ();
extern "C" void HashSet_1_InitArrays_m20249_gshared ();
extern "C" void HashSet_1_SlotsContainsAt_m20251_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void HashSet_1_CopyTo_m20253_gshared ();
extern "C" void HashSet_1_CopyTo_m20255_gshared ();
extern "C" void HashSet_1_Resize_m20257_gshared ();
extern "C" void HashSet_1_GetLinkHashCode_m20259_gshared ();
extern "C" void HashSet_1_GetItemHashCode_m20261_gshared ();
extern "C" void HashSet_1_Clear_m20264_gshared ();
extern "C" void HashSet_1_Contains_m20266_gshared ();
extern "C" void HashSet_1_Remove_m20268_gshared ();
extern "C" void HashSet_1_GetObjectData_m20270_gshared ();
extern "C" void HashSet_1_OnDeserialization_m20272_gshared ();
extern "C" void InternalEnumerator_1__ctor_m20273_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20274_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20275_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20276_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20277_gshared ();
void* RuntimeInvoker_Link_t3760 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m20278_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared ();
extern "C" void Enumerator_MoveNext_m20280_gshared ();
extern "C" void Enumerator_get_Current_m20281_gshared ();
extern "C" void Enumerator_Dispose_m20282_gshared ();
extern "C" void Enumerator_CheckState_m20283_gshared ();
extern "C" void PrimeHelper__cctor_m20284_gshared ();
extern "C" void PrimeHelper_TestPrime_m20285_gshared ();
extern "C" void PrimeHelper_CalcPrime_m20286_gshared ();
extern "C" void PrimeHelper_ToPrime_m20287_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20313_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20314_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20315_gshared ();
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20316_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20342_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20343_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20344_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20345_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20358_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20359_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20360_gshared ();
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20361_gshared ();
extern "C" void Func_2_Invoke_m20370_gshared ();
extern "C" void Func_2_BeginInvoke_m20372_gshared ();
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_EndInvoke_m20374_gshared ();
extern "C" void Action_1_Invoke_m20383_gshared ();
extern "C" void Action_1_BeginInvoke_m20384_gshared ();
extern "C" void Action_1_EndInvoke_m20385_gshared ();
extern "C" void Func_2_Invoke_m20393_gshared ();
extern "C" void Func_2_BeginInvoke_m20395_gshared ();
extern "C" void Func_2_EndInvoke_m20397_gshared ();
extern "C" void Action_2__ctor_m20410_gshared ();
extern "C" void Action_2_Invoke_m20411_gshared ();
extern "C" void Action_2_BeginInvoke_m20412_gshared ();
extern "C" void Action_2_EndInvoke_m20413_gshared ();
extern "C" void OutMethod_1_Invoke_m20433_gshared ();
extern "C" void OutMethod_1_BeginInvoke_m20434_gshared ();
extern "C" void OutMethod_1_EndInvoke_m20435_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20436_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20437_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20438_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20439_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20440_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20441_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20442_gshared ();
extern "C" void Dictionary_2__ctor_m20445_gshared ();
extern "C" void Dictionary_2__ctor_m20447_gshared ();
extern "C" void Dictionary_2__ctor_m20449_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m20451_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m20453_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20455_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20457_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20459_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20475_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481_gshared ();
extern "C" void Dictionary_2_get_Count_m20483_gshared ();
extern "C" void Dictionary_2_get_Item_m20485_gshared ();
extern "C" void Dictionary_2_set_Item_m20487_gshared ();
extern "C" void Dictionary_2_Init_m20489_gshared ();
extern "C" void Dictionary_2_InitArrays_m20491_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m20493_gshared ();
extern "C" void Dictionary_2_make_pair_m20495_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m20497_gshared ();
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m20499_gshared ();
extern "C" void Dictionary_2_CopyTo_m20501_gshared ();
extern "C" void Dictionary_2_Resize_m20503_gshared ();
extern "C" void Dictionary_2_Add_m20505_gshared ();
extern "C" void Dictionary_2_Clear_m20507_gshared ();
extern "C" void Dictionary_2_ContainsKey_m20509_gshared ();
extern "C" void Dictionary_2_ContainsValue_m20511_gshared ();
extern "C" void Dictionary_2_GetObjectData_m20513_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m20515_gshared ();
extern "C" void Dictionary_2_Remove_m20517_gshared ();
extern "C" void Dictionary_2_TryGetValue_m20519_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m20521_gshared ();
extern "C" void Dictionary_2_get_Values_m20523_gshared ();
extern "C" void Dictionary_2_ToTKey_m20525_gshared ();
extern "C" void Dictionary_2_ToTValue_m20527_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m20529_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m20531_gshared ();
void* RuntimeInvoker_Enumerator_t3794 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m20533_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m20534_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20535_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m20536_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m20537_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m20538_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m20539_gshared ();
extern "C" void KeyValuePair_2_get_Key_m20540_gshared ();
extern "C" void KeyValuePair_2_set_Key_m20541_gshared ();
extern "C" void KeyValuePair_2_get_Value_m20542_gshared ();
extern "C" void KeyValuePair_2_set_Value_m20543_gshared ();
extern "C" void KeyValuePair_2_ToString_m20544_gshared ();
extern "C" void KeyCollection__ctor_m20545_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20546_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20547_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20548_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20549_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20550_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m20551_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20552_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20553_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20554_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m20555_gshared ();
extern "C" void KeyCollection_CopyTo_m20556_gshared ();
extern "C" void KeyCollection_GetEnumerator_m20557_gshared ();
void* RuntimeInvoker_Enumerator_t3793 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m20558_gshared ();
extern "C" void Enumerator__ctor_m20559_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20560_gshared ();
extern "C" void Enumerator_Dispose_m20561_gshared ();
extern "C" void Enumerator_MoveNext_m20562_gshared ();
extern "C" void Enumerator_get_Current_m20563_gshared ();
extern "C" void Enumerator__ctor_m20564_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20565_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568_gshared ();
extern "C" void Enumerator_MoveNext_m20569_gshared ();
extern "C" void Enumerator_get_Current_m20570_gshared ();
extern "C" void Enumerator_get_CurrentKey_m20571_gshared ();
extern "C" void Enumerator_get_CurrentValue_m20572_gshared ();
extern "C" void Enumerator_VerifyState_m20573_gshared ();
extern "C" void Enumerator_VerifyCurrent_m20574_gshared ();
extern "C" void Enumerator_Dispose_m20575_gshared ();
extern "C" void Transform_1__ctor_m20576_gshared ();
extern "C" void Transform_1_Invoke_m20577_gshared ();
extern "C" void Transform_1_BeginInvoke_m20578_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m20579_gshared ();
extern "C" void Transform_1__ctor_m20580_gshared ();
extern "C" void Transform_1_Invoke_m20581_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_BeginInvoke_m20582_gshared ();
extern "C" void Transform_1_EndInvoke_m20583_gshared ();
extern "C" void ValueCollection__ctor_m20584_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20585_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20586_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20587_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20588_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20589_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20590_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20591_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20592_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20593_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m20594_gshared ();
extern "C" void ValueCollection_CopyTo_m20595_gshared ();
extern "C" void ValueCollection_GetEnumerator_m20596_gshared ();
void* RuntimeInvoker_Enumerator_t3798 (MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m20597_gshared ();
extern "C" void Enumerator__ctor_m20598_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20599_gshared ();
extern "C" void Enumerator_Dispose_m20600_gshared ();
extern "C" void Enumerator_MoveNext_m20601_gshared ();
extern "C" void Enumerator_get_Current_m20602_gshared ();
extern "C" void Transform_1__ctor_m20603_gshared ();
extern "C" void Transform_1_Invoke_m20604_gshared ();
extern "C" void Transform_1_BeginInvoke_m20605_gshared ();
extern "C" void Transform_1_EndInvoke_m20606_gshared ();
extern "C" void Transform_1__ctor_m20607_gshared ();
extern "C" void Transform_1_Invoke_m20608_gshared ();
extern "C" void Transform_1_BeginInvoke_m20609_gshared ();
extern "C" void Transform_1_EndInvoke_m20610_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3790_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m20611_gshared ();
extern "C" void ShimEnumerator_MoveNext_m20612_gshared ();
extern "C" void ShimEnumerator_get_Entry_m20613_gshared ();
extern "C" void ShimEnumerator_get_Key_m20614_gshared ();
extern "C" void ShimEnumerator_get_Value_m20615_gshared ();
extern "C" void ShimEnumerator_get_Current_m20616_gshared ();
extern "C" void OutMethod_1_Invoke_m20667_gshared ();
extern "C" void OutMethod_1_BeginInvoke_m20668_gshared ();
extern "C" void OutMethod_1_EndInvoke_m20669_gshared ();
extern "C" void Func_2_Invoke_m20703_gshared ();
void* RuntimeInvoker_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m20705_gshared ();
extern "C" void Func_2_EndInvoke_m20707_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20720_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20721_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20722_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20723_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20724_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20725_gshared ();
extern "C" void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20726_gshared ();
extern "C" void List_1__ctor_m20727_gshared ();
extern "C" void List_1__ctor_m20728_gshared ();
extern "C" void List_1__ctor_m20729_gshared ();
extern "C" void List_1__cctor_m20730_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20731_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20732_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m20733_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m20734_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m20735_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m20736_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m20737_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m20738_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20739_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m20740_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m20741_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m20742_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m20743_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m20744_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m20745_gshared ();
extern "C" void List_1_Add_m20746_gshared ();
extern "C" void List_1_GrowIfNeeded_m20747_gshared ();
extern "C" void List_1_AddCollection_m20748_gshared ();
extern "C" void List_1_AddEnumerable_m20749_gshared ();
extern "C" void List_1_AddRange_m20750_gshared ();
extern "C" void List_1_AsReadOnly_m20751_gshared ();
extern "C" void List_1_Clear_m20752_gshared ();
extern "C" void List_1_Contains_m20753_gshared ();
extern "C" void List_1_CopyTo_m20754_gshared ();
extern "C" void List_1_Find_m20755_gshared ();
extern "C" void List_1_CheckMatch_m20756_gshared ();
extern "C" void List_1_FindIndex_m20757_gshared ();
extern "C" void List_1_GetIndex_m20758_gshared ();
extern "C" void List_1_ForEach_m20759_gshared ();
extern "C" void List_1_GetEnumerator_m20760_gshared ();
void* RuntimeInvoker_Enumerator_t3808 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m20761_gshared ();
extern "C" void List_1_Shift_m20762_gshared ();
extern "C" void List_1_CheckIndex_m20763_gshared ();
extern "C" void List_1_Insert_m20764_gshared ();
extern "C" void List_1_CheckCollection_m20765_gshared ();
extern "C" void List_1_Remove_m20766_gshared ();
extern "C" void List_1_RemoveAll_m20767_gshared ();
extern "C" void List_1_RemoveAt_m20768_gshared ();
extern "C" void List_1_Reverse_m20769_gshared ();
extern "C" void List_1_Sort_m20770_gshared ();
extern "C" void List_1_Sort_m20771_gshared ();
extern "C" void List_1_ToArray_m20772_gshared ();
extern "C" void List_1_TrimExcess_m20773_gshared ();
extern "C" void List_1_get_Capacity_m20774_gshared ();
extern "C" void List_1_set_Capacity_m20775_gshared ();
extern "C" void List_1_get_Count_m20776_gshared ();
extern "C" void List_1_get_Item_m20777_gshared ();
extern "C" void List_1_set_Item_m20778_gshared ();
extern "C" void Enumerator__ctor_m20779_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m20780_gshared ();
extern "C" void Enumerator_Dispose_m20781_gshared ();
extern "C" void Enumerator_VerifyState_m20782_gshared ();
extern "C" void Enumerator_MoveNext_m20783_gshared ();
extern "C" void Enumerator_get_Current_m20784_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m20785_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20786_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20787_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20788_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20789_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20790_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20791_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20792_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20793_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20794_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20795_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m20796_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m20797_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m20798_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20799_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m20800_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m20801_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20802_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20803_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20804_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20805_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20806_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m20807_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m20808_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m20809_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m20810_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m20811_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m20812_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m20813_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m20814_gshared ();
extern "C" void Collection_1__ctor_m20815_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20816_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m20817_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m20818_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m20819_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m20820_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m20821_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m20822_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m20823_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m20824_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m20825_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m20826_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m20827_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m20828_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m20829_gshared ();
extern "C" void Collection_1_Add_m20830_gshared ();
extern "C" void Collection_1_Clear_m20831_gshared ();
extern "C" void Collection_1_ClearItems_m20832_gshared ();
extern "C" void Collection_1_Contains_m20833_gshared ();
extern "C" void Collection_1_CopyTo_m20834_gshared ();
extern "C" void Collection_1_GetEnumerator_m20835_gshared ();
extern "C" void Collection_1_IndexOf_m20836_gshared ();
extern "C" void Collection_1_Insert_m20837_gshared ();
extern "C" void Collection_1_InsertItem_m20838_gshared ();
extern "C" void Collection_1_Remove_m20839_gshared ();
extern "C" void Collection_1_RemoveAt_m20840_gshared ();
extern "C" void Collection_1_RemoveItem_m20841_gshared ();
extern "C" void Collection_1_get_Count_m20842_gshared ();
extern "C" void Collection_1_get_Item_m20843_gshared ();
extern "C" void Collection_1_set_Item_m20844_gshared ();
extern "C" void Collection_1_SetItem_m20845_gshared ();
extern "C" void Collection_1_IsValidItem_m20846_gshared ();
extern "C" void Collection_1_ConvertItem_m20847_gshared ();
extern "C" void Collection_1_CheckWritable_m20848_gshared ();
extern "C" void Collection_1_IsSynchronized_m20849_gshared ();
extern "C" void Collection_1_IsFixedSize_m20850_gshared ();
extern "C" void EqualityComparer_1__ctor_m20851_gshared ();
extern "C" void EqualityComparer_1__cctor_m20852_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20853_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20854_gshared ();
extern "C" void EqualityComparer_1_get_Default_m20855_gshared ();
extern "C" void DefaultComparer__ctor_m20856_gshared ();
extern "C" void DefaultComparer_GetHashCode_m20857_gshared ();
extern "C" void DefaultComparer_Equals_m20858_gshared ();
void* RuntimeInvoker_Boolean_t203_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m20859_gshared ();
extern "C" void Predicate_1_Invoke_m20860_gshared ();
extern "C" void Predicate_1_BeginInvoke_m20861_gshared ();
extern "C" void Predicate_1_EndInvoke_m20862_gshared ();
extern "C" void Comparer_1__ctor_m20863_gshared ();
extern "C" void Comparer_1__cctor_m20864_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m20865_gshared ();
extern "C" void Comparer_1_get_Default_m20866_gshared ();
extern "C" void DefaultComparer__ctor_m20867_gshared ();
extern "C" void DefaultComparer_Compare_m20868_gshared ();
void* RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m20869_gshared ();
extern "C" void Comparison_1_Invoke_m20870_gshared ();
extern "C" void Comparison_1_BeginInvoke_m20871_gshared ();
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m20872_gshared ();
extern "C" void UnityAction_1_Invoke_m20908_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m20910_gshared ();
extern "C" void UnityAction_1_EndInvoke_m20912_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m20916_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m20920_gshared ();
extern "C" void InvokableCall_1__ctor_m20922_gshared ();
extern "C" void InvokableCall_1__ctor_m20923_gshared ();
extern "C" void InvokableCall_1_Invoke_m20924_gshared ();
extern "C" void InvokableCall_1_Find_m20925_gshared ();
extern "C" void List_1__ctor_m21030_gshared ();
extern "C" void List_1__ctor_m21031_gshared ();
extern "C" void List_1__cctor_m21032_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21033_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21034_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m21035_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m21036_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m21037_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m21038_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m21039_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m21040_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21041_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m21042_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m21043_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m21044_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m21045_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m21046_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m21047_gshared ();
extern "C" void List_1_Add_m21048_gshared ();
extern "C" void List_1_GrowIfNeeded_m21049_gshared ();
extern "C" void List_1_AddCollection_m21050_gshared ();
extern "C" void List_1_AddEnumerable_m21051_gshared ();
extern "C" void List_1_AddRange_m21052_gshared ();
extern "C" void List_1_AsReadOnly_m21053_gshared ();
extern "C" void List_1_Clear_m21054_gshared ();
extern "C" void List_1_Contains_m21055_gshared ();
extern "C" void List_1_CopyTo_m21056_gshared ();
extern "C" void List_1_Find_m21057_gshared ();
void* RuntimeInvoker_Matrix4x4_t997_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m21058_gshared ();
extern "C" void List_1_FindIndex_m21059_gshared ();
extern "C" void List_1_GetIndex_m21060_gshared ();
extern "C" void List_1_ForEach_m21061_gshared ();
extern "C" void List_1_GetEnumerator_m21062_gshared ();
void* RuntimeInvoker_Enumerator_t3832 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m21063_gshared ();
extern "C" void List_1_Shift_m21064_gshared ();
extern "C" void List_1_CheckIndex_m21065_gshared ();
extern "C" void List_1_Insert_m21066_gshared ();
extern "C" void List_1_CheckCollection_m21067_gshared ();
extern "C" void List_1_Remove_m21068_gshared ();
extern "C" void List_1_RemoveAll_m21069_gshared ();
extern "C" void List_1_RemoveAt_m21070_gshared ();
extern "C" void List_1_Reverse_m21071_gshared ();
extern "C" void List_1_Sort_m21072_gshared ();
extern "C" void List_1_Sort_m21073_gshared ();
extern "C" void List_1_ToArray_m21074_gshared ();
extern "C" void List_1_TrimExcess_m21075_gshared ();
extern "C" void List_1_get_Capacity_m21076_gshared ();
extern "C" void List_1_set_Capacity_m21077_gshared ();
extern "C" void List_1_get_Count_m21078_gshared ();
extern "C" void List_1_get_Item_m21079_gshared ();
extern "C" void List_1_set_Item_m21080_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21081_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21082_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21083_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21084_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21085_gshared ();
void* RuntimeInvoker_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m21086_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21087_gshared ();
extern "C" void Enumerator_Dispose_m21088_gshared ();
extern "C" void Enumerator_VerifyState_m21089_gshared ();
extern "C" void Enumerator_MoveNext_m21090_gshared ();
extern "C" void Enumerator_get_Current_m21091_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m21092_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21093_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21094_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21095_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21096_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21097_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21098_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21099_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21100_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21101_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21102_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m21103_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21104_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m21105_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21106_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21107_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21108_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21109_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21110_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21111_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21112_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21113_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m21114_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21115_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m21116_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m21117_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m21118_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m21119_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m21120_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m21121_gshared ();
extern "C" void Collection_1__ctor_m21122_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21123_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21124_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m21125_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m21126_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m21127_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m21128_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m21129_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m21130_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m21131_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m21132_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m21133_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m21134_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m21135_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m21136_gshared ();
extern "C" void Collection_1_Add_m21137_gshared ();
extern "C" void Collection_1_Clear_m21138_gshared ();
extern "C" void Collection_1_ClearItems_m21139_gshared ();
extern "C" void Collection_1_Contains_m21140_gshared ();
extern "C" void Collection_1_CopyTo_m21141_gshared ();
extern "C" void Collection_1_GetEnumerator_m21142_gshared ();
extern "C" void Collection_1_IndexOf_m21143_gshared ();
extern "C" void Collection_1_Insert_m21144_gshared ();
extern "C" void Collection_1_InsertItem_m21145_gshared ();
extern "C" void Collection_1_Remove_m21146_gshared ();
extern "C" void Collection_1_RemoveAt_m21147_gshared ();
extern "C" void Collection_1_RemoveItem_m21148_gshared ();
extern "C" void Collection_1_get_Count_m21149_gshared ();
extern "C" void Collection_1_get_Item_m21150_gshared ();
extern "C" void Collection_1_set_Item_m21151_gshared ();
extern "C" void Collection_1_SetItem_m21152_gshared ();
extern "C" void Collection_1_IsValidItem_m21153_gshared ();
extern "C" void Collection_1_ConvertItem_m21154_gshared ();
extern "C" void Collection_1_CheckWritable_m21155_gshared ();
extern "C" void Collection_1_IsSynchronized_m21156_gshared ();
extern "C" void Collection_1_IsFixedSize_m21157_gshared ();
extern "C" void EqualityComparer_1__ctor_m21158_gshared ();
extern "C" void EqualityComparer_1__cctor_m21159_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21160_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21161_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21162_gshared ();
extern "C" void DefaultComparer__ctor_m21163_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21164_gshared ();
extern "C" void DefaultComparer_Equals_m21165_gshared ();
void* RuntimeInvoker_Boolean_t203_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m21166_gshared ();
extern "C" void Predicate_1_Invoke_m21167_gshared ();
extern "C" void Predicate_1_BeginInvoke_m21168_gshared ();
void* RuntimeInvoker_Object_t_Matrix4x4_t997_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m21169_gshared ();
extern "C" void Action_1__ctor_m21170_gshared ();
extern "C" void Action_1_Invoke_m21171_gshared ();
extern "C" void Action_1_BeginInvoke_m21172_gshared ();
extern "C" void Action_1_EndInvoke_m21173_gshared ();
extern "C" void Comparer_1__ctor_m21174_gshared ();
extern "C" void Comparer_1__cctor_m21175_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m21176_gshared ();
extern "C" void Comparer_1_get_Default_m21177_gshared ();
extern "C" void DefaultComparer__ctor_m21178_gshared ();
extern "C" void DefaultComparer_Compare_m21179_gshared ();
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m21180_gshared ();
extern "C" void Comparison_1_Invoke_m21181_gshared ();
extern "C" void Comparison_1_BeginInvoke_m21182_gshared ();
void* RuntimeInvoker_Object_t_Matrix4x4_t997_Matrix4x4_t997_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m21183_gshared ();
extern "C" void List_1__ctor_m21282_gshared ();
extern "C" void List_1__ctor_m21283_gshared ();
extern "C" void List_1__cctor_m21284_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21285_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21286_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m21287_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m21288_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m21289_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m21290_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m21291_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m21292_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21293_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m21294_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m21295_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m21296_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m21297_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m21298_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m21299_gshared ();
extern "C" void List_1_Add_m21300_gshared ();
extern "C" void List_1_GrowIfNeeded_m21301_gshared ();
extern "C" void List_1_AddCollection_m21302_gshared ();
extern "C" void List_1_AddEnumerable_m21303_gshared ();
extern "C" void List_1_AddRange_m21304_gshared ();
extern "C" void List_1_AsReadOnly_m21305_gshared ();
extern "C" void List_1_Clear_m21306_gshared ();
extern "C" void List_1_Contains_m21307_gshared ();
extern "C" void List_1_CopyTo_m21308_gshared ();
extern "C" void List_1_Find_m21309_gshared ();
void* RuntimeInvoker_Vector3_t758_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m21310_gshared ();
extern "C" void List_1_FindIndex_m21311_gshared ();
extern "C" void List_1_GetIndex_m21312_gshared ();
extern "C" void List_1_ForEach_m21313_gshared ();
extern "C" void List_1_GetEnumerator_m21314_gshared ();
void* RuntimeInvoker_Enumerator_t3850 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m21315_gshared ();
extern "C" void List_1_Shift_m21316_gshared ();
extern "C" void List_1_CheckIndex_m21317_gshared ();
extern "C" void List_1_Insert_m21318_gshared ();
extern "C" void List_1_CheckCollection_m21319_gshared ();
extern "C" void List_1_Remove_m21320_gshared ();
extern "C" void List_1_RemoveAll_m21321_gshared ();
extern "C" void List_1_RemoveAt_m21322_gshared ();
extern "C" void List_1_Reverse_m21323_gshared ();
extern "C" void List_1_Sort_m21324_gshared ();
extern "C" void List_1_Sort_m21325_gshared ();
extern "C" void List_1_ToArray_m21326_gshared ();
extern "C" void List_1_TrimExcess_m21327_gshared ();
extern "C" void List_1_get_Capacity_m21328_gshared ();
extern "C" void List_1_set_Capacity_m21329_gshared ();
extern "C" void List_1_get_Count_m21330_gshared ();
extern "C" void List_1_get_Item_m21331_gshared ();
extern "C" void List_1_set_Item_m21332_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21333_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21334_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21335_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21336_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21337_gshared ();
void* RuntimeInvoker_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m21338_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21339_gshared ();
extern "C" void Enumerator_Dispose_m21340_gshared ();
extern "C" void Enumerator_VerifyState_m21341_gshared ();
extern "C" void Enumerator_MoveNext_m21342_gshared ();
extern "C" void Enumerator_get_Current_m21343_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m21344_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21345_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21346_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21347_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21348_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21349_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21350_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21351_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21352_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21353_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21354_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m21355_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21356_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m21357_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21358_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21359_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21360_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21361_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21362_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21363_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21364_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21365_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m21366_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21367_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m21368_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m21369_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m21370_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m21371_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m21372_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m21373_gshared ();
extern "C" void Collection_1__ctor_m21374_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21375_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21376_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m21377_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m21378_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m21379_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m21380_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m21381_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m21382_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m21383_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m21384_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m21385_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m21386_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m21387_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m21388_gshared ();
extern "C" void Collection_1_Add_m21389_gshared ();
extern "C" void Collection_1_Clear_m21390_gshared ();
extern "C" void Collection_1_ClearItems_m21391_gshared ();
extern "C" void Collection_1_Contains_m21392_gshared ();
extern "C" void Collection_1_CopyTo_m21393_gshared ();
extern "C" void Collection_1_GetEnumerator_m21394_gshared ();
extern "C" void Collection_1_IndexOf_m21395_gshared ();
extern "C" void Collection_1_Insert_m21396_gshared ();
extern "C" void Collection_1_InsertItem_m21397_gshared ();
extern "C" void Collection_1_Remove_m21398_gshared ();
extern "C" void Collection_1_RemoveAt_m21399_gshared ();
extern "C" void Collection_1_RemoveItem_m21400_gshared ();
extern "C" void Collection_1_get_Count_m21401_gshared ();
extern "C" void Collection_1_get_Item_m21402_gshared ();
extern "C" void Collection_1_set_Item_m21403_gshared ();
extern "C" void Collection_1_SetItem_m21404_gshared ();
extern "C" void Collection_1_IsValidItem_m21405_gshared ();
extern "C" void Collection_1_ConvertItem_m21406_gshared ();
extern "C" void Collection_1_CheckWritable_m21407_gshared ();
extern "C" void Collection_1_IsSynchronized_m21408_gshared ();
extern "C" void Collection_1_IsFixedSize_m21409_gshared ();
extern "C" void EqualityComparer_1__ctor_m21410_gshared ();
extern "C" void EqualityComparer_1__cctor_m21411_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21412_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21413_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21414_gshared ();
extern "C" void DefaultComparer__ctor_m21415_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21416_gshared ();
extern "C" void DefaultComparer_Equals_m21417_gshared ();
void* RuntimeInvoker_Boolean_t203_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m21418_gshared ();
extern "C" void Predicate_1_Invoke_m21419_gshared ();
extern "C" void Predicate_1_BeginInvoke_m21420_gshared ();
void* RuntimeInvoker_Object_t_Vector3_t758_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m21421_gshared ();
extern "C" void Action_1__ctor_m21422_gshared ();
extern "C" void Action_1_Invoke_m21423_gshared ();
extern "C" void Action_1_BeginInvoke_m21424_gshared ();
extern "C" void Action_1_EndInvoke_m21425_gshared ();
extern "C" void Comparer_1__ctor_m21426_gshared ();
extern "C" void Comparer_1__cctor_m21427_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m21428_gshared ();
extern "C" void Comparer_1_get_Default_m21429_gshared ();
extern "C" void DefaultComparer__ctor_m21430_gshared ();
extern "C" void DefaultComparer_Compare_m21431_gshared ();
void* RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m21432_gshared ();
extern "C" void Comparison_1_Invoke_m21433_gshared ();
extern "C" void Comparison_1_BeginInvoke_m21434_gshared ();
void* RuntimeInvoker_Object_t_Vector3_t758_Vector3_t758_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m21435_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21441_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21442_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21443_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21444_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21445_gshared ();
void* RuntimeInvoker_Touch_t901 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m21545_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m21546_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t1187_RaycastResult_t1187_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m21547_gshared ();
extern "C" void List_1__ctor_m21803_gshared ();
extern "C" void List_1__ctor_m21804_gshared ();
extern "C" void List_1__cctor_m21805_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21806_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21807_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m21808_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m21809_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m21810_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m21811_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m21812_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m21813_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21814_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m21815_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m21816_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m21817_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m21818_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m21819_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m21820_gshared ();
extern "C" void List_1_Add_m21821_gshared ();
extern "C" void List_1_GrowIfNeeded_m21822_gshared ();
extern "C" void List_1_AddCollection_m21823_gshared ();
extern "C" void List_1_AddEnumerable_m21824_gshared ();
extern "C" void List_1_AddRange_m21825_gshared ();
extern "C" void List_1_AsReadOnly_m21826_gshared ();
extern "C" void List_1_Clear_m21827_gshared ();
extern "C" void List_1_Contains_m21828_gshared ();
extern "C" void List_1_CopyTo_m21829_gshared ();
extern "C" void List_1_Find_m21830_gshared ();
void* RuntimeInvoker_RaycastResult_t1187_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m21831_gshared ();
extern "C" void List_1_FindIndex_m21832_gshared ();
extern "C" void List_1_GetIndex_m21833_gshared ();
extern "C" void List_1_ForEach_m21834_gshared ();
extern "C" void List_1_GetEnumerator_m21835_gshared ();
void* RuntimeInvoker_Enumerator_t3890 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m21836_gshared ();
extern "C" void List_1_Shift_m21837_gshared ();
extern "C" void List_1_CheckIndex_m21838_gshared ();
extern "C" void List_1_Insert_m21839_gshared ();
extern "C" void List_1_CheckCollection_m21840_gshared ();
extern "C" void List_1_Remove_m21841_gshared ();
extern "C" void List_1_RemoveAll_m21842_gshared ();
extern "C" void List_1_RemoveAt_m21843_gshared ();
extern "C" void List_1_Reverse_m21844_gshared ();
extern "C" void List_1_Sort_m21845_gshared ();
extern "C" void List_1_ToArray_m21846_gshared ();
extern "C" void List_1_TrimExcess_m21847_gshared ();
extern "C" void List_1_get_Capacity_m21848_gshared ();
extern "C" void List_1_set_Capacity_m21849_gshared ();
extern "C" void List_1_get_Count_m21850_gshared ();
extern "C" void List_1_get_Item_m21851_gshared ();
extern "C" void List_1_set_Item_m21852_gshared ();
extern "C" void InternalEnumerator_1__ctor_m21853_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21854_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m21855_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m21856_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m21857_gshared ();
void* RuntimeInvoker_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m21858_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m21859_gshared ();
extern "C" void Enumerator_Dispose_m21860_gshared ();
extern "C" void Enumerator_VerifyState_m21861_gshared ();
extern "C" void Enumerator_MoveNext_m21862_gshared ();
extern "C" void Enumerator_get_Current_m21863_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m21864_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21865_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21866_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21867_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21868_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21869_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21870_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21871_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21872_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21873_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21874_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m21875_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21876_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m21877_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21878_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21879_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21880_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21881_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21882_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21883_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21884_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21885_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m21886_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21887_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m21888_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m21889_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m21890_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m21891_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m21892_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m21893_gshared ();
extern "C" void Collection_1__ctor_m21894_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21895_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21896_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m21897_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m21898_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m21899_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m21900_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m21901_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m21902_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m21903_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m21904_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m21905_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m21906_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m21907_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m21908_gshared ();
extern "C" void Collection_1_Add_m21909_gshared ();
extern "C" void Collection_1_Clear_m21910_gshared ();
extern "C" void Collection_1_ClearItems_m21911_gshared ();
extern "C" void Collection_1_Contains_m21912_gshared ();
extern "C" void Collection_1_CopyTo_m21913_gshared ();
extern "C" void Collection_1_GetEnumerator_m21914_gshared ();
extern "C" void Collection_1_IndexOf_m21915_gshared ();
extern "C" void Collection_1_Insert_m21916_gshared ();
extern "C" void Collection_1_InsertItem_m21917_gshared ();
extern "C" void Collection_1_Remove_m21918_gshared ();
extern "C" void Collection_1_RemoveAt_m21919_gshared ();
extern "C" void Collection_1_RemoveItem_m21920_gshared ();
extern "C" void Collection_1_get_Count_m21921_gshared ();
extern "C" void Collection_1_get_Item_m21922_gshared ();
extern "C" void Collection_1_set_Item_m21923_gshared ();
extern "C" void Collection_1_SetItem_m21924_gshared ();
extern "C" void Collection_1_IsValidItem_m21925_gshared ();
extern "C" void Collection_1_ConvertItem_m21926_gshared ();
extern "C" void Collection_1_CheckWritable_m21927_gshared ();
extern "C" void Collection_1_IsSynchronized_m21928_gshared ();
extern "C" void Collection_1_IsFixedSize_m21929_gshared ();
extern "C" void EqualityComparer_1__ctor_m21930_gshared ();
extern "C" void EqualityComparer_1__cctor_m21931_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21932_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21933_gshared ();
extern "C" void EqualityComparer_1_get_Default_m21934_gshared ();
extern "C" void DefaultComparer__ctor_m21935_gshared ();
extern "C" void DefaultComparer_GetHashCode_m21936_gshared ();
extern "C" void DefaultComparer_Equals_m21937_gshared ();
void* RuntimeInvoker_Boolean_t203_RaycastResult_t1187_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m21938_gshared ();
extern "C" void Predicate_1_Invoke_m21939_gshared ();
extern "C" void Predicate_1_BeginInvoke_m21940_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t1187_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m21941_gshared ();
extern "C" void Action_1__ctor_m21942_gshared ();
extern "C" void Action_1_Invoke_m21943_gshared ();
extern "C" void Action_1_BeginInvoke_m21944_gshared ();
extern "C" void Action_1_EndInvoke_m21945_gshared ();
extern "C" void Comparer_1__ctor_m21946_gshared ();
extern "C" void Comparer_1__cctor_m21947_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m21948_gshared ();
extern "C" void Comparer_1_get_Default_m21949_gshared ();
extern "C" void DefaultComparer__ctor_m21950_gshared ();
extern "C" void DefaultComparer_Compare_m21951_gshared ();
extern "C" void Dictionary_2__ctor_m22216_gshared ();
extern "C" void Dictionary_2__ctor_m22218_gshared ();
extern "C" void Dictionary_2__ctor_m22220_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m22222_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m22224_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22226_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22228_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22230_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22232_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22234_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22236_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22238_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22240_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22242_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22244_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22246_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22248_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22250_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22252_gshared ();
extern "C" void Dictionary_2_get_Count_m22254_gshared ();
extern "C" void Dictionary_2_get_Item_m22256_gshared ();
extern "C" void Dictionary_2_set_Item_m22258_gshared ();
extern "C" void Dictionary_2_Init_m22260_gshared ();
extern "C" void Dictionary_2_InitArrays_m22262_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m22264_gshared ();
extern "C" void Dictionary_2_make_pair_m22266_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m22268_gshared ();
void* RuntimeInvoker_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m22270_gshared ();
void* RuntimeInvoker_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m22272_gshared ();
extern "C" void Dictionary_2_Resize_m22274_gshared ();
extern "C" void Dictionary_2_Add_m22276_gshared ();
extern "C" void Dictionary_2_Clear_m22278_gshared ();
extern "C" void Dictionary_2_ContainsKey_m22280_gshared ();
extern "C" void Dictionary_2_ContainsValue_m22282_gshared ();
extern "C" void Dictionary_2_GetObjectData_m22284_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m22286_gshared ();
extern "C" void Dictionary_2_Remove_m22288_gshared ();
extern "C" void Dictionary_2_TryGetValue_m22290_gshared ();
void* RuntimeInvoker_Boolean_t203_Int32_t189_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m22292_gshared ();
extern "C" void Dictionary_2_ToTKey_m22295_gshared ();
extern "C" void Dictionary_2_ToTValue_m22297_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m22299_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m22302_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m22303_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22304_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22305_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22306_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22307_gshared ();
extern "C" void KeyValuePair_2__ctor_m22308_gshared ();
extern "C" void KeyValuePair_2_set_Key_m22310_gshared ();
extern "C" void KeyValuePair_2_set_Value_m22312_gshared ();
extern "C" void KeyCollection__ctor_m22314_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22315_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22316_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22317_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22318_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22319_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m22320_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22321_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22322_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22323_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m22324_gshared ();
extern "C" void KeyCollection_CopyTo_m22325_gshared ();
extern "C" void KeyCollection_GetEnumerator_m22326_gshared ();
void* RuntimeInvoker_Enumerator_t3924 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m22327_gshared ();
extern "C" void Enumerator__ctor_m22328_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22329_gshared ();
extern "C" void Enumerator_Dispose_m22330_gshared ();
extern "C" void Enumerator_MoveNext_m22331_gshared ();
extern "C" void Enumerator_get_Current_m22332_gshared ();
extern "C" void Enumerator__ctor_m22333_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared ();
extern "C" void Enumerator_get_CurrentKey_m22340_gshared ();
extern "C" void Enumerator_get_CurrentValue_m22341_gshared ();
extern "C" void Enumerator_VerifyState_m22342_gshared ();
extern "C" void Enumerator_VerifyCurrent_m22343_gshared ();
extern "C" void Enumerator_Dispose_m22344_gshared ();
extern "C" void Transform_1__ctor_m22345_gshared ();
extern "C" void Transform_1_Invoke_m22346_gshared ();
extern "C" void Transform_1_BeginInvoke_m22347_gshared ();
extern "C" void Transform_1_EndInvoke_m22348_gshared ();
extern "C" void Transform_1__ctor_m22349_gshared ();
extern "C" void Transform_1_Invoke_m22350_gshared ();
extern "C" void Transform_1_BeginInvoke_m22351_gshared ();
extern "C" void Transform_1_EndInvoke_m22352_gshared ();
extern "C" void ValueCollection__ctor_m22353_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22354_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22355_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22356_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22357_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22358_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22359_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22360_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22361_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22362_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m22363_gshared ();
extern "C" void ValueCollection_CopyTo_m22364_gshared ();
extern "C" void ValueCollection_get_Count_m22366_gshared ();
extern "C" void Enumerator__ctor_m22367_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22368_gshared ();
extern "C" void Enumerator_Dispose_m22369_gshared ();
extern "C" void Transform_1__ctor_m22372_gshared ();
extern "C" void Transform_1_Invoke_m22373_gshared ();
extern "C" void Transform_1_BeginInvoke_m22374_gshared ();
extern "C" void Transform_1_EndInvoke_m22375_gshared ();
extern "C" void Transform_1__ctor_m22376_gshared ();
extern "C" void Transform_1_Invoke_m22377_gshared ();
extern "C" void Transform_1_BeginInvoke_m22378_gshared ();
extern "C" void Transform_1_EndInvoke_m22379_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t3921_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m22380_gshared ();
extern "C" void ShimEnumerator_MoveNext_m22381_gshared ();
extern "C" void ShimEnumerator_get_Entry_m22382_gshared ();
extern "C" void ShimEnumerator_get_Key_m22383_gshared ();
extern "C" void ShimEnumerator_get_Value_m22384_gshared ();
extern "C" void ShimEnumerator_get_Current_m22385_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22532_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22533_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22534_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22535_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22536_gshared ();
void* RuntimeInvoker_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m22537_gshared ();
void* RuntimeInvoker_Int32_t189_RaycastHit_t990_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m22538_gshared ();
void* RuntimeInvoker_Object_t_RaycastHit_t990_RaycastHit_t990_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m22539_gshared ();
extern "C" void InternalEnumerator_1__ctor_m22540_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22541_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m22542_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m22543_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m22544_gshared ();
void* RuntimeInvoker_RaycastHit_t990 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_RemoveListener_m22545_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m22546_gshared ();
extern "C" void UnityAction_1_Invoke_m22547_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m22548_gshared ();
void* RuntimeInvoker_Object_t_Color_t747_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m22549_gshared ();
extern "C" void InvokableCall_1__ctor_m22550_gshared ();
extern "C" void InvokableCall_1__ctor_m22551_gshared ();
extern "C" void InvokableCall_1_Invoke_m22552_gshared ();
extern "C" void InvokableCall_1_Find_m22553_gshared ();
extern "C" void List_1__ctor_m22949_gshared ();
extern "C" void List_1__cctor_m22950_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22951_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22952_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m22953_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m22954_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m22955_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m22956_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m22957_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m22958_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22959_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m22960_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m22961_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m22962_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m22963_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m22964_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m22965_gshared ();
extern "C" void List_1_Add_m22966_gshared ();
void* RuntimeInvoker_Void_t260_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GrowIfNeeded_m22967_gshared ();
extern "C" void List_1_AddCollection_m22968_gshared ();
extern "C" void List_1_AddEnumerable_m22969_gshared ();
extern "C" void List_1_AddRange_m22970_gshared ();
extern "C" void List_1_AsReadOnly_m22971_gshared ();
extern "C" void List_1_Clear_m22972_gshared ();
extern "C" void List_1_Contains_m22973_gshared ();
void* RuntimeInvoker_Boolean_t203_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CopyTo_m22974_gshared ();
extern "C" void List_1_Find_m22975_gshared ();
void* RuntimeInvoker_UIVertex_t1265_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m22976_gshared ();
extern "C" void List_1_FindIndex_m22977_gshared ();
extern "C" void List_1_GetIndex_m22978_gshared ();
extern "C" void List_1_ForEach_m22979_gshared ();
extern "C" void List_1_GetEnumerator_m22980_gshared ();
void* RuntimeInvoker_Enumerator_t3967 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m22981_gshared ();
void* RuntimeInvoker_Int32_t189_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_Shift_m22982_gshared ();
extern "C" void List_1_CheckIndex_m22983_gshared ();
extern "C" void List_1_Insert_m22984_gshared ();
void* RuntimeInvoker_Void_t260_Int32_t189_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckCollection_m22985_gshared ();
extern "C" void List_1_Remove_m22986_gshared ();
extern "C" void List_1_RemoveAll_m22987_gshared ();
extern "C" void List_1_RemoveAt_m22988_gshared ();
extern "C" void List_1_Reverse_m22989_gshared ();
extern "C" void List_1_Sort_m22990_gshared ();
extern "C" void List_1_Sort_m22991_gshared ();
extern "C" void List_1_TrimExcess_m22992_gshared ();
extern "C" void List_1_get_Count_m22993_gshared ();
extern "C" void List_1_get_Item_m22994_gshared ();
void* RuntimeInvoker_UIVertex_t1265_Int32_t189 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_set_Item_m22995_gshared ();
extern "C" void Enumerator__ctor_m22928_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m22929_gshared ();
extern "C" void Enumerator_Dispose_m22930_gshared ();
extern "C" void Enumerator_VerifyState_m22931_gshared ();
extern "C" void Enumerator_MoveNext_m22932_gshared ();
extern "C" void Enumerator_get_Current_m22933_gshared ();
void* RuntimeInvoker_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void ReadOnlyCollection_1__ctor_m22890_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22891_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22892_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22893_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22894_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22895_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22896_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22897_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22898_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22899_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22900_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m22901_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m22902_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m22903_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22904_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m22905_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m22906_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22907_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22908_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22909_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22910_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22911_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m22912_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m22913_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m22914_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m22915_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m22916_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m22917_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m22918_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m22919_gshared ();
extern "C" void Collection_1__ctor_m22999_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23000_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23001_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m23002_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m23003_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m23004_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m23005_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m23006_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m23007_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m23008_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m23009_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m23010_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m23011_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m23012_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m23013_gshared ();
extern "C" void Collection_1_Add_m23014_gshared ();
extern "C" void Collection_1_Clear_m23015_gshared ();
extern "C" void Collection_1_ClearItems_m23016_gshared ();
extern "C" void Collection_1_Contains_m23017_gshared ();
extern "C" void Collection_1_CopyTo_m23018_gshared ();
extern "C" void Collection_1_GetEnumerator_m23019_gshared ();
extern "C" void Collection_1_IndexOf_m23020_gshared ();
extern "C" void Collection_1_Insert_m23021_gshared ();
extern "C" void Collection_1_InsertItem_m23022_gshared ();
extern "C" void Collection_1_Remove_m23023_gshared ();
extern "C" void Collection_1_RemoveAt_m23024_gshared ();
extern "C" void Collection_1_RemoveItem_m23025_gshared ();
extern "C" void Collection_1_get_Count_m23026_gshared ();
extern "C" void Collection_1_get_Item_m23027_gshared ();
extern "C" void Collection_1_set_Item_m23028_gshared ();
extern "C" void Collection_1_SetItem_m23029_gshared ();
extern "C" void Collection_1_IsValidItem_m23030_gshared ();
extern "C" void Collection_1_ConvertItem_m23031_gshared ();
extern "C" void Collection_1_CheckWritable_m23032_gshared ();
extern "C" void Collection_1_IsSynchronized_m23033_gshared ();
extern "C" void Collection_1_IsFixedSize_m23034_gshared ();
extern "C" void EqualityComparer_1__ctor_m23035_gshared ();
extern "C" void EqualityComparer_1__cctor_m23036_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m23037_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m23038_gshared ();
extern "C" void EqualityComparer_1_get_Default_m23039_gshared ();
extern "C" void DefaultComparer__ctor_m23040_gshared ();
extern "C" void DefaultComparer_GetHashCode_m23041_gshared ();
extern "C" void DefaultComparer_Equals_m23042_gshared ();
void* RuntimeInvoker_Boolean_t203_UIVertex_t1265_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m22920_gshared ();
extern "C" void Predicate_1_Invoke_m22921_gshared ();
extern "C" void Predicate_1_BeginInvoke_m22922_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t1265_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m22923_gshared ();
extern "C" void Action_1__ctor_m22924_gshared ();
extern "C" void Action_1_Invoke_m22925_gshared ();
extern "C" void Action_1_BeginInvoke_m22926_gshared ();
extern "C" void Action_1_EndInvoke_m22927_gshared ();
extern "C" void Comparer_1__ctor_m23043_gshared ();
extern "C" void Comparer_1__cctor_m23044_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m23045_gshared ();
extern "C" void Comparer_1_get_Default_m23046_gshared ();
extern "C" void DefaultComparer__ctor_m23047_gshared ();
extern "C" void DefaultComparer_Compare_m23048_gshared ();
void* RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m22934_gshared ();
extern "C" void Comparison_1_Invoke_m22935_gshared ();
extern "C" void Comparison_1_BeginInvoke_m22936_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t1265_UIVertex_t1265_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m22937_gshared ();
extern "C" void TweenRunner_1_Start_m23049_gshared ();
void* RuntimeInvoker_Object_t_ColorTween_t1209 (MethodInfo* method, void* obj, void** args);
extern "C" void U3CStartU3Ec__Iterator0__ctor_m23050_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m23051_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m23052_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m23053_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m23054_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m23055_gshared ();
extern "C" void InternalEnumerator_1__ctor_m23510_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23511_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23512_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23513_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23514_gshared ();
void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m23520_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23521_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23522_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23523_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23524_gshared ();
void* RuntimeInvoker_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m23525_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23526_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m23527_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m23528_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m23529_gshared ();
void* RuntimeInvoker_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_GetDelegate_m23537_gshared ();
extern "C" void UnityAction_1_Invoke_m23538_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m23539_gshared ();
void* RuntimeInvoker_Object_t_Single_t202_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m23540_gshared ();
extern "C" void InvokableCall_1__ctor_m23541_gshared ();
extern "C" void InvokableCall_1__ctor_m23542_gshared ();
extern "C" void InvokableCall_1_Invoke_m23543_gshared ();
extern "C" void InvokableCall_1_Find_m23544_gshared ();
extern "C" void UnityEvent_1_AddListener_m23545_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m23546_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m23547_gshared ();
extern "C" void UnityAction_1__ctor_m23548_gshared ();
extern "C" void UnityAction_1_Invoke_m23549_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m23550_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t739_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m23551_gshared ();
extern "C" void InvokableCall_1__ctor_m23552_gshared ();
extern "C" void InvokableCall_1__ctor_m23553_gshared ();
extern "C" void InvokableCall_1_Invoke_m23554_gshared ();
extern "C" void InvokableCall_1_Find_m23555_gshared ();
extern "C" void Func_2_Invoke_m24057_gshared ();
void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m24059_gshared ();
extern "C" void Func_2_EndInvoke_m24061_gshared ();
extern "C" void InternalEnumerator_1__ctor_m24204_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24205_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24206_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24207_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24208_gshared ();
void* RuntimeInvoker_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24214_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24215_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24216_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24217_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24218_gshared ();
void* RuntimeInvoker_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24731_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24732_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24733_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24734_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24735_gshared ();
void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m24736_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24737_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m24738_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m24739_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m24740_gshared ();
void* RuntimeInvoker_Keyframe_t1591 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m24741_gshared ();
extern "C" void List_1__ctor_m24742_gshared ();
extern "C" void List_1__cctor_m24743_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24744_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24745_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m24746_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m24747_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m24748_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m24749_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m24750_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m24751_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24752_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m24753_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m24754_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m24755_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m24756_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m24757_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m24758_gshared ();
extern "C" void List_1_Add_m24759_gshared ();
extern "C" void List_1_GrowIfNeeded_m24760_gshared ();
extern "C" void List_1_AddCollection_m24761_gshared ();
extern "C" void List_1_AddEnumerable_m24762_gshared ();
extern "C" void List_1_AddRange_m24763_gshared ();
extern "C" void List_1_AsReadOnly_m24764_gshared ();
extern "C" void List_1_Clear_m24765_gshared ();
extern "C" void List_1_Contains_m24766_gshared ();
extern "C" void List_1_CopyTo_m24767_gshared ();
extern "C" void List_1_Find_m24768_gshared ();
void* RuntimeInvoker_UICharInfo_t1400_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m24769_gshared ();
extern "C" void List_1_FindIndex_m24770_gshared ();
extern "C" void List_1_GetIndex_m24771_gshared ();
extern "C" void List_1_ForEach_m24772_gshared ();
extern "C" void List_1_GetEnumerator_m24773_gshared ();
void* RuntimeInvoker_Enumerator_t4099 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m24774_gshared ();
extern "C" void List_1_Shift_m24775_gshared ();
extern "C" void List_1_CheckIndex_m24776_gshared ();
extern "C" void List_1_Insert_m24777_gshared ();
extern "C" void List_1_CheckCollection_m24778_gshared ();
extern "C" void List_1_Remove_m24779_gshared ();
extern "C" void List_1_RemoveAll_m24780_gshared ();
extern "C" void List_1_RemoveAt_m24781_gshared ();
extern "C" void List_1_Reverse_m24782_gshared ();
extern "C" void List_1_Sort_m24783_gshared ();
extern "C" void List_1_Sort_m24784_gshared ();
extern "C" void List_1_ToArray_m24785_gshared ();
extern "C" void List_1_TrimExcess_m24786_gshared ();
extern "C" void List_1_get_Capacity_m24787_gshared ();
extern "C" void List_1_set_Capacity_m24788_gshared ();
extern "C" void List_1_get_Count_m24789_gshared ();
extern "C" void List_1_get_Item_m24790_gshared ();
extern "C" void List_1_set_Item_m24791_gshared ();
extern "C" void Enumerator__ctor_m24792_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24793_gshared ();
extern "C" void Enumerator_Dispose_m24794_gshared ();
extern "C" void Enumerator_VerifyState_m24795_gshared ();
extern "C" void Enumerator_MoveNext_m24796_gshared ();
extern "C" void Enumerator_get_Current_m24797_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m24798_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24799_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24800_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24801_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24802_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24803_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24804_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24805_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24806_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24807_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24808_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m24809_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m24810_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m24811_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24812_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m24813_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m24814_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24815_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24816_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24817_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24818_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24819_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m24820_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m24821_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m24822_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m24823_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m24824_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m24825_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m24826_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m24827_gshared ();
extern "C" void Collection_1__ctor_m24828_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24829_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24830_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m24831_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m24832_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m24833_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m24834_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m24835_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m24836_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m24837_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m24838_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m24839_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m24840_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m24841_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m24842_gshared ();
extern "C" void Collection_1_Add_m24843_gshared ();
extern "C" void Collection_1_Clear_m24844_gshared ();
extern "C" void Collection_1_ClearItems_m24845_gshared ();
extern "C" void Collection_1_Contains_m24846_gshared ();
extern "C" void Collection_1_CopyTo_m24847_gshared ();
extern "C" void Collection_1_GetEnumerator_m24848_gshared ();
extern "C" void Collection_1_IndexOf_m24849_gshared ();
extern "C" void Collection_1_Insert_m24850_gshared ();
extern "C" void Collection_1_InsertItem_m24851_gshared ();
extern "C" void Collection_1_Remove_m24852_gshared ();
extern "C" void Collection_1_RemoveAt_m24853_gshared ();
extern "C" void Collection_1_RemoveItem_m24854_gshared ();
extern "C" void Collection_1_get_Count_m24855_gshared ();
extern "C" void Collection_1_get_Item_m24856_gshared ();
extern "C" void Collection_1_set_Item_m24857_gshared ();
extern "C" void Collection_1_SetItem_m24858_gshared ();
extern "C" void Collection_1_IsValidItem_m24859_gshared ();
extern "C" void Collection_1_ConvertItem_m24860_gshared ();
extern "C" void Collection_1_CheckWritable_m24861_gshared ();
extern "C" void Collection_1_IsSynchronized_m24862_gshared ();
extern "C" void Collection_1_IsFixedSize_m24863_gshared ();
extern "C" void EqualityComparer_1__ctor_m24864_gshared ();
extern "C" void EqualityComparer_1__cctor_m24865_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24866_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24867_gshared ();
extern "C" void EqualityComparer_1_get_Default_m24868_gshared ();
extern "C" void DefaultComparer__ctor_m24869_gshared ();
extern "C" void DefaultComparer_GetHashCode_m24870_gshared ();
extern "C" void DefaultComparer_Equals_m24871_gshared ();
void* RuntimeInvoker_Boolean_t203_UICharInfo_t1400_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m24872_gshared ();
extern "C" void Predicate_1_Invoke_m24873_gshared ();
extern "C" void Predicate_1_BeginInvoke_m24874_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t1400_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m24875_gshared ();
extern "C" void Action_1__ctor_m24876_gshared ();
extern "C" void Action_1_Invoke_m24877_gshared ();
extern "C" void Action_1_BeginInvoke_m24878_gshared ();
extern "C" void Action_1_EndInvoke_m24879_gshared ();
extern "C" void Comparer_1__ctor_m24880_gshared ();
extern "C" void Comparer_1__cctor_m24881_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m24882_gshared ();
extern "C" void Comparer_1_get_Default_m24883_gshared ();
extern "C" void DefaultComparer__ctor_m24884_gshared ();
extern "C" void DefaultComparer_Compare_m24885_gshared ();
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m24886_gshared ();
extern "C" void Comparison_1_Invoke_m24887_gshared ();
extern "C" void Comparison_1_BeginInvoke_m24888_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t1400_UICharInfo_t1400_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m24889_gshared ();
extern "C" void List_1__ctor_m24890_gshared ();
extern "C" void List_1__ctor_m24891_gshared ();
extern "C" void List_1__cctor_m24892_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24893_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24894_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m24895_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m24896_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m24897_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m24898_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m24899_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m24900_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24901_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m24902_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m24903_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m24904_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m24905_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m24906_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m24907_gshared ();
extern "C" void List_1_Add_m24908_gshared ();
extern "C" void List_1_GrowIfNeeded_m24909_gshared ();
extern "C" void List_1_AddCollection_m24910_gshared ();
extern "C" void List_1_AddEnumerable_m24911_gshared ();
extern "C" void List_1_AddRange_m24912_gshared ();
extern "C" void List_1_AsReadOnly_m24913_gshared ();
extern "C" void List_1_Clear_m24914_gshared ();
extern "C" void List_1_Contains_m24915_gshared ();
extern "C" void List_1_CopyTo_m24916_gshared ();
extern "C" void List_1_Find_m24917_gshared ();
void* RuntimeInvoker_UILineInfo_t1398_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m24918_gshared ();
extern "C" void List_1_FindIndex_m24919_gshared ();
extern "C" void List_1_GetIndex_m24920_gshared ();
extern "C" void List_1_ForEach_m24921_gshared ();
extern "C" void List_1_GetEnumerator_m24922_gshared ();
void* RuntimeInvoker_Enumerator_t4109 (MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m24923_gshared ();
extern "C" void List_1_Shift_m24924_gshared ();
extern "C" void List_1_CheckIndex_m24925_gshared ();
extern "C" void List_1_Insert_m24926_gshared ();
extern "C" void List_1_CheckCollection_m24927_gshared ();
extern "C" void List_1_Remove_m24928_gshared ();
extern "C" void List_1_RemoveAll_m24929_gshared ();
extern "C" void List_1_RemoveAt_m24930_gshared ();
extern "C" void List_1_Reverse_m24931_gshared ();
extern "C" void List_1_Sort_m24932_gshared ();
extern "C" void List_1_Sort_m24933_gshared ();
extern "C" void List_1_ToArray_m24934_gshared ();
extern "C" void List_1_TrimExcess_m24935_gshared ();
extern "C" void List_1_get_Capacity_m24936_gshared ();
extern "C" void List_1_set_Capacity_m24937_gshared ();
extern "C" void List_1_get_Count_m24938_gshared ();
extern "C" void List_1_get_Item_m24939_gshared ();
extern "C" void List_1_set_Item_m24940_gshared ();
extern "C" void Enumerator__ctor_m24941_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m24942_gshared ();
extern "C" void Enumerator_Dispose_m24943_gshared ();
extern "C" void Enumerator_VerifyState_m24944_gshared ();
extern "C" void Enumerator_MoveNext_m24945_gshared ();
extern "C" void Enumerator_get_Current_m24946_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m24947_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24948_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24949_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24950_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24951_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24952_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24953_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24954_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24955_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24956_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24957_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m24958_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m24959_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m24960_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24961_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m24962_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m24963_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24964_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24965_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24966_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24967_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24968_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m24969_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m24970_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m24971_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m24972_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m24973_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m24974_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m24975_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m24976_gshared ();
extern "C" void Collection_1__ctor_m24977_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24978_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24979_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m24980_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m24981_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m24982_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m24983_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m24984_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m24985_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m24986_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m24987_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m24988_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m24989_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m24990_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m24991_gshared ();
extern "C" void Collection_1_Add_m24992_gshared ();
extern "C" void Collection_1_Clear_m24993_gshared ();
extern "C" void Collection_1_ClearItems_m24994_gshared ();
extern "C" void Collection_1_Contains_m24995_gshared ();
extern "C" void Collection_1_CopyTo_m24996_gshared ();
extern "C" void Collection_1_GetEnumerator_m24997_gshared ();
extern "C" void Collection_1_IndexOf_m24998_gshared ();
extern "C" void Collection_1_Insert_m24999_gshared ();
extern "C" void Collection_1_InsertItem_m25000_gshared ();
extern "C" void Collection_1_Remove_m25001_gshared ();
extern "C" void Collection_1_RemoveAt_m25002_gshared ();
extern "C" void Collection_1_RemoveItem_m25003_gshared ();
extern "C" void Collection_1_get_Count_m25004_gshared ();
extern "C" void Collection_1_get_Item_m25005_gshared ();
extern "C" void Collection_1_set_Item_m25006_gshared ();
extern "C" void Collection_1_SetItem_m25007_gshared ();
extern "C" void Collection_1_IsValidItem_m25008_gshared ();
extern "C" void Collection_1_ConvertItem_m25009_gshared ();
extern "C" void Collection_1_CheckWritable_m25010_gshared ();
extern "C" void Collection_1_IsSynchronized_m25011_gshared ();
extern "C" void Collection_1_IsFixedSize_m25012_gshared ();
extern "C" void EqualityComparer_1__ctor_m25013_gshared ();
extern "C" void EqualityComparer_1__cctor_m25014_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25015_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25016_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25017_gshared ();
extern "C" void DefaultComparer__ctor_m25018_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25019_gshared ();
extern "C" void DefaultComparer_Equals_m25020_gshared ();
void* RuntimeInvoker_Boolean_t203_UILineInfo_t1398_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m25021_gshared ();
extern "C" void Predicate_1_Invoke_m25022_gshared ();
extern "C" void Predicate_1_BeginInvoke_m25023_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t1398_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m25024_gshared ();
extern "C" void Action_1__ctor_m25025_gshared ();
extern "C" void Action_1_Invoke_m25026_gshared ();
extern "C" void Action_1_BeginInvoke_m25027_gshared ();
extern "C" void Action_1_EndInvoke_m25028_gshared ();
extern "C" void Comparer_1__ctor_m25029_gshared ();
extern "C" void Comparer_1__cctor_m25030_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m25031_gshared ();
extern "C" void Comparer_1_get_Default_m25032_gshared ();
extern "C" void DefaultComparer__ctor_m25033_gshared ();
extern "C" void DefaultComparer_Compare_m25034_gshared ();
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m25035_gshared ();
extern "C" void Comparison_1_Invoke_m25036_gshared ();
extern "C" void Comparison_1_BeginInvoke_m25037_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t1398_UILineInfo_t1398_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m25038_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25165_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25166_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25167_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25168_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25169_gshared ();
void* RuntimeInvoker_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25170_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25172_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25173_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25174_gshared ();
void* RuntimeInvoker_HitInfo_t1629 (MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m25299_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m25300_gshared ();
extern "C" void InvokableCall_1__ctor_m25301_gshared ();
extern "C" void InvokableCall_1__ctor_m25302_gshared ();
extern "C" void InvokableCall_1_Invoke_m25303_gshared ();
extern "C" void InvokableCall_1_Find_m25304_gshared ();
extern "C" void UnityAction_1__ctor_m25305_gshared ();
extern "C" void UnityAction_1_Invoke_m25306_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m25307_gshared ();
extern "C" void UnityAction_1_EndInvoke_m25308_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m25316_gshared ();
extern "C" void Dictionary_2__ctor_m25527_gshared ();
extern "C" void Dictionary_2__ctor_m25530_gshared ();
extern "C" void Dictionary_2__ctor_m25532_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Keys_m25534_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m25536_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m25538_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m25540_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m25542_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25544_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25546_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25548_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25550_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25552_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25554_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25556_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m25558_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25560_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25562_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25564_gshared ();
extern "C" void Dictionary_2_get_Count_m25566_gshared ();
extern "C" void Dictionary_2_get_Item_m25568_gshared ();
extern "C" void Dictionary_2_set_Item_m25570_gshared ();
extern "C" void Dictionary_2_Init_m25572_gshared ();
extern "C" void Dictionary_2_InitArrays_m25574_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m25576_gshared ();
extern "C" void Dictionary_2_make_pair_m25578_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t4171_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m25580_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m25582_gshared ();
void* RuntimeInvoker_Byte_t237_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m25584_gshared ();
extern "C" void Dictionary_2_Resize_m25586_gshared ();
extern "C" void Dictionary_2_Add_m25588_gshared ();
extern "C" void Dictionary_2_Clear_m25590_gshared ();
extern "C" void Dictionary_2_ContainsKey_m25592_gshared ();
extern "C" void Dictionary_2_ContainsValue_m25594_gshared ();
extern "C" void Dictionary_2_GetObjectData_m25596_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m25598_gshared ();
extern "C" void Dictionary_2_Remove_m25600_gshared ();
extern "C" void Dictionary_2_TryGetValue_m25602_gshared ();
void* RuntimeInvoker_Boolean_t203_Object_t_ByteU26_t2328 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m25604_gshared ();
extern "C" void Dictionary_2_get_Values_m25606_gshared ();
extern "C" void Dictionary_2_ToTKey_m25608_gshared ();
extern "C" void Dictionary_2_ToTValue_m25610_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m25612_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m25614_gshared ();
void* RuntimeInvoker_Enumerator_t4175 (MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m25616_gshared ();
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25617_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25618_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25619_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25620_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25621_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m25622_gshared ();
extern "C" void KeyValuePair_2_get_Key_m25623_gshared ();
extern "C" void KeyValuePair_2_set_Key_m25624_gshared ();
extern "C" void KeyValuePair_2_get_Value_m25625_gshared ();
extern "C" void KeyValuePair_2_set_Value_m25626_gshared ();
extern "C" void KeyValuePair_2_ToString_m25627_gshared ();
extern "C" void KeyCollection__ctor_m25628_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25629_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25630_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25631_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25632_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25633_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m25634_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25635_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25636_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25637_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m25638_gshared ();
extern "C" void KeyCollection_CopyTo_m25639_gshared ();
extern "C" void KeyCollection_GetEnumerator_m25640_gshared ();
void* RuntimeInvoker_Enumerator_t4174 (MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m25641_gshared ();
extern "C" void Enumerator__ctor_m25642_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25643_gshared ();
extern "C" void Enumerator_Dispose_m25644_gshared ();
extern "C" void Enumerator_MoveNext_m25645_gshared ();
extern "C" void Enumerator_get_Current_m25646_gshared ();
extern "C" void Enumerator__ctor_m25647_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25648_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651_gshared ();
extern "C" void Enumerator_MoveNext_m25652_gshared ();
extern "C" void Enumerator_get_Current_m25653_gshared ();
extern "C" void Enumerator_get_CurrentKey_m25654_gshared ();
extern "C" void Enumerator_get_CurrentValue_m25655_gshared ();
extern "C" void Enumerator_VerifyState_m25656_gshared ();
extern "C" void Enumerator_VerifyCurrent_m25657_gshared ();
extern "C" void Enumerator_Dispose_m25658_gshared ();
extern "C" void Transform_1__ctor_m25659_gshared ();
extern "C" void Transform_1_Invoke_m25660_gshared ();
extern "C" void Transform_1_BeginInvoke_m25661_gshared ();
extern "C" void Transform_1_EndInvoke_m25662_gshared ();
extern "C" void ValueCollection__ctor_m25663_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25664_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25665_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25666_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25667_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25668_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m25669_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25670_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25671_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25672_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m25673_gshared ();
extern "C" void ValueCollection_CopyTo_m25674_gshared ();
extern "C" void ValueCollection_GetEnumerator_m25675_gshared ();
void* RuntimeInvoker_Enumerator_t4178 (MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m25676_gshared ();
extern "C" void Enumerator__ctor_m25677_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m25678_gshared ();
extern "C" void Enumerator_Dispose_m25679_gshared ();
extern "C" void Enumerator_MoveNext_m25680_gshared ();
extern "C" void Enumerator_get_Current_m25681_gshared ();
extern "C" void Transform_1__ctor_m25682_gshared ();
extern "C" void Transform_1_Invoke_m25683_gshared ();
extern "C" void Transform_1_BeginInvoke_m25684_gshared ();
extern "C" void Transform_1_EndInvoke_m25685_gshared ();
extern "C" void Transform_1__ctor_m25686_gshared ();
extern "C" void Transform_1_Invoke_m25687_gshared ();
extern "C" void Transform_1_BeginInvoke_m25688_gshared ();
extern "C" void Transform_1_EndInvoke_m25689_gshared ();
extern "C" void Transform_1__ctor_m25690_gshared ();
extern "C" void Transform_1_Invoke_m25691_gshared ();
extern "C" void Transform_1_BeginInvoke_m25692_gshared ();
extern "C" void Transform_1_EndInvoke_m25693_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t4171_Object_t (MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m25694_gshared ();
extern "C" void ShimEnumerator_MoveNext_m25695_gshared ();
extern "C" void ShimEnumerator_get_Entry_m25696_gshared ();
extern "C" void ShimEnumerator_get_Key_m25697_gshared ();
extern "C" void ShimEnumerator_get_Value_m25698_gshared ();
extern "C" void ShimEnumerator_get_Current_m25699_gshared ();
extern "C" void EqualityComparer_1__ctor_m25700_gshared ();
extern "C" void EqualityComparer_1__cctor_m25701_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25702_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25703_gshared ();
extern "C" void EqualityComparer_1_get_Default_m25704_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m25705_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m25706_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m25707_gshared ();
void* RuntimeInvoker_Boolean_t203_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m25708_gshared ();
extern "C" void DefaultComparer_GetHashCode_m25709_gshared ();
extern "C" void DefaultComparer_Equals_m25710_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25766_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25767_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25768_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25769_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25770_gshared ();
void* RuntimeInvoker_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m25781_gshared ();
extern "C" void Comparer_1__cctor_m25782_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m25783_gshared ();
extern "C" void Comparer_1_get_Default_m25784_gshared ();
extern "C" void GenericComparer_1__ctor_m25785_gshared ();
extern "C" void GenericComparer_1_Compare_m25786_gshared ();
extern "C" void DefaultComparer__ctor_m25787_gshared ();
extern "C" void DefaultComparer_Compare_m25788_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25789_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25790_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25791_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25792_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25793_gshared ();
void* RuntimeInvoker_Mark_t2083 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25794_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25795_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25796_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25797_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25798_gshared ();
void* RuntimeInvoker_UriScheme_t2118 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25819_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25820_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25821_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25822_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25823_gshared ();
void* RuntimeInvoker_UInt64_t239 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25824_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25825_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25826_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25827_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25828_gshared ();
void* RuntimeInvoker_Int16_t238 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25829_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25830_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25831_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25832_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25833_gshared ();
void* RuntimeInvoker_SByte_t236 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25834_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25835_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25836_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25837_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25838_gshared ();
void* RuntimeInvoker_Int64_t233 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25870_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25871_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25872_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25873_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25874_gshared ();
void* RuntimeInvoker_TableRange_t2360 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25900_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25901_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25902_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25903_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25904_gshared ();
void* RuntimeInvoker_Slot_t2436 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25905_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25906_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25907_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25908_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25909_gshared ();
void* RuntimeInvoker_Slot_t2443 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25978_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25979_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25980_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25981_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25982_gshared ();
extern "C" void InternalEnumerator_1__ctor_m25983_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25984_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25985_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25986_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25987_gshared ();
void* RuntimeInvoker_Decimal_t240 (MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m25988_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25989_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m25990_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m25991_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m25992_gshared ();
extern "C" void GenericComparer_1_Compare_m26102_gshared ();
void* RuntimeInvoker_Int32_t189_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m26103_gshared ();
extern "C" void Comparer_1__cctor_m26104_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m26105_gshared ();
extern "C" void Comparer_1_get_Default_m26106_gshared ();
extern "C" void DefaultComparer__ctor_m26107_gshared ();
extern "C" void DefaultComparer_Compare_m26108_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m26109_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m26110_gshared ();
void* RuntimeInvoker_Boolean_t203_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m26111_gshared ();
extern "C" void EqualityComparer_1__cctor_m26112_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26113_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26114_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26115_gshared ();
extern "C" void DefaultComparer__ctor_m26116_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26117_gshared ();
extern "C" void DefaultComparer_Equals_m26118_gshared ();
extern "C" void GenericComparer_1_Compare_m26119_gshared ();
void* RuntimeInvoker_Int32_t189_DateTimeOffset_t2793_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m26120_gshared ();
extern "C" void Comparer_1__cctor_m26121_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m26122_gshared ();
extern "C" void Comparer_1_get_Default_m26123_gshared ();
extern "C" void DefaultComparer__ctor_m26124_gshared ();
extern "C" void DefaultComparer_Compare_m26125_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m26126_gshared ();
void* RuntimeInvoker_Int32_t189_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m26127_gshared ();
void* RuntimeInvoker_Boolean_t203_DateTimeOffset_t2793_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m26128_gshared ();
extern "C" void EqualityComparer_1__cctor_m26129_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26130_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26131_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26132_gshared ();
extern "C" void DefaultComparer__ctor_m26133_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26134_gshared ();
extern "C" void DefaultComparer_Equals_m26135_gshared ();
extern "C" void GenericComparer_1_Compare_m26136_gshared ();
void* RuntimeInvoker_Int32_t189_Guid_t2814_Guid_t2814 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m26137_gshared ();
extern "C" void Comparer_1__cctor_m26138_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m26139_gshared ();
extern "C" void Comparer_1_get_Default_m26140_gshared ();
extern "C" void DefaultComparer__ctor_m26141_gshared ();
extern "C" void DefaultComparer_Compare_m26142_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m26143_gshared ();
void* RuntimeInvoker_Int32_t189_Guid_t2814 (MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m26144_gshared ();
void* RuntimeInvoker_Boolean_t203_Guid_t2814_Guid_t2814 (MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m26145_gshared ();
extern "C" void EqualityComparer_1__cctor_m26146_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26147_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26148_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26149_gshared ();
extern "C" void DefaultComparer__ctor_m26150_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26151_gshared ();
extern "C" void DefaultComparer_Equals_m26152_gshared ();
extern "C" void GenericComparer_1_Compare_m26153_gshared ();
void* RuntimeInvoker_Int32_t189_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m26154_gshared ();
extern "C" void Comparer_1__cctor_m26155_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m26156_gshared ();
extern "C" void Comparer_1_get_Default_m26157_gshared ();
extern "C" void DefaultComparer__ctor_m26158_gshared ();
extern "C" void DefaultComparer_Compare_m26159_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m26160_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m26161_gshared ();
void* RuntimeInvoker_Boolean_t203_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m26162_gshared ();
extern "C" void EqualityComparer_1__cctor_m26163_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26164_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26165_gshared ();
extern "C" void EqualityComparer_1_get_Default_m26166_gshared ();
extern "C" void DefaultComparer__ctor_m26167_gshared ();
extern "C" void DefaultComparer_GetHashCode_m26168_gshared ();
extern "C" void DefaultComparer_Equals_m26169_gshared ();
extern Il2CppGenericInst GenInst_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0;
extern Il2CppGenericInst GenInst_ObjectKvp_t6_0_0_0;
extern Il2CppGenericInst GenInst_ObjectKvp_t6_0_0_0_UnityKeyValuePair_2_t164_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t164_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t164_0_0_0_ObjectKvp_t6_0_0_0;
extern Il2CppGenericInst GenInst_SoomlaEditorScript_t14_0_0_0;
extern Il2CppGenericInst GenInst_Reward_t55_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t165_0_0_0;
extern Il2CppGenericInst GenInst_JSONObject_t30_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t30_0_0_0;
extern Il2CppGenericInst GenInst_Char_t193_0_0_0;
extern Il2CppGenericInst GenInst_DateTimeRange_t47_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Reward_t55_0_0_0;
extern Il2CppGenericInst GenInst_PurchasableVirtualItem_t124_0_0_0;
extern Il2CppGenericInst GenInst_PurchasableVirtualItem_t124_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_EquippableVG_t129_0_0_0;
extern Il2CppGenericInst GenInst_VirtualGood_t79_0_0_0_UpgradeVG_t133_0_0_0;
extern Il2CppGenericInst GenInst_VirtualCurrency_t77_0_0_0_Int32_t189_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_VirtualGood_t79_0_0_0_Int32_t189_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_StoreEvents_t90_0_0_0;
extern Il2CppGenericInst GenInst_VirtualItem_t125_0_0_0;
extern Il2CppGenericInst GenInst_PurchasableVirtualItem_t124_0_0_0_String_t_0_0_0_Dictionary_2_t165_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t177_0_0_0;
extern Il2CppGenericInst GenInst_MarketItem_t122_0_0_0;
extern Il2CppGenericInst GenInst_UpgradeVG_t133_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_LocalUpgrade_t101_0_0_0;
extern Il2CppGenericInst GenInst_VirtualCurrency_t77_0_0_0;
extern Il2CppGenericInst GenInst_VirtualGood_t79_0_0_0;
extern Il2CppGenericInst GenInst_LocalUpgrade_t101_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_VirtualItem_t125_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_PurchasableVirtualItem_t124_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_VirtualCategory_t126_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t178_0_0_0;
extern Il2CppGenericInst GenInst_VirtualCurrencyPack_t78_0_0_0;
extern Il2CppGenericInst GenInst_VirtualCategory_t126_0_0_0;
extern Il2CppGenericInst GenInst_UpgradeVG_t133_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_MonoBehaviour_t26_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityAds_t154_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t196_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_UnityNameValuePair_1_t247_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t248_gp_0_0_0_0_UnityKeyValuePair_2_t248_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_UnityDictionaryEnumerator_t251_gp_0_0_0_0_UnityDictionaryEnumerator_t251_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t271_0_0_0;
extern Il2CppGenericInst GenInst_U3CU3Ec__AnonStorey4_t252_gp_0_0_0_0_U3CU3Ec__AnonStorey4_t252_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CRemoveU3Ec__AnonStorey5_t253_gp_0_0_0_0_U3CRemoveU3Ec__AnonStorey5_t253_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CContainsKeyU3Ec__AnonStorey6_t254_gp_0_0_0_0_U3CContainsKeyU3Ec__AnonStorey6_t254_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t292_0_0_0;
extern Il2CppGenericInst GenInst_UnityDictionary_2_t249_gp_0_0_0_0_UnityDictionary_2_t249_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t286_0_0_0;
extern Il2CppGenericInst GenInst_UnityDictionary_2_t249_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UnityDictionary_2_t249_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t292_0_0_0_UnityDictionary_2_t249_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t292_0_0_0_UnityDictionary_2_t249_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t292_0_0_0_KeyValuePair_2_t286_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_UnityDictionary_1_t255_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4656_0_0_0;
extern Il2CppGenericInst GenInst_SoomlaEntity_1_t258_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_SoomlaExtensions_AddOrUpdate_m1144_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_AsyncExec_runWithCallback_m1160_gp_0_0_0_0_Action_1_t327_0_0_0_IEnumerator_t37_0_0_0;
extern Il2CppGenericInst GenInst_AsyncExec_runWithCallback_m1160_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_ShowResult_t160_0_0_0;
extern Il2CppGenericInst GenInst_Achievement_t333_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_UInt32_t235_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_ParticipantResult_t342_0_0_0;
extern Il2CppGenericInst GenInst_Participant_t340_0_0_0;
extern Il2CppGenericInst GenInst_Participant_t340_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_ByteU5BU5D_t350_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t361_0_0_0;
extern Il2CppGenericInst GenInst_InitializationStatus_t360_0_0_0;
extern Il2CppGenericInst GenInst_InvitationReceivedDelegate_t363_0_0_0;
extern Il2CppGenericInst GenInst_MatchDelegate_t364_0_0_0;
extern Il2CppGenericInst GenInst_TimeSpan_t190_0_0_0;
extern Il2CppGenericInst GenInst_IPlayGamesClient_t387_0_0_0;
extern Il2CppGenericInst GenInst_Action_t588_0_0_0;
extern Il2CppGenericInst GenInst_PlayGamesHelperObject_t392_0_0_0;
extern Il2CppGenericInst GenInst_PlayGamesClientConfiguration_t366_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerEvent_t518_0_0_0_String_t_0_0_0_NativeTurnBasedMatch_t680_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerEvent_t518_0_0_0_String_t_0_0_0_MultiplayerInvitation_t601_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Achievement_t333_0_0_0;
extern Il2CppGenericInst GenInst_NativeAchievement_t673_0_0_0;
extern Il2CppGenericInst GenInst_FetchAllResponse_t655_0_0_0;
extern Il2CppGenericInst GenInst_FetchSelfResponse_t684_0_0_0;
extern Il2CppGenericInst GenInst_FetchResponse_t653_0_0_0;
extern Il2CppGenericInst GenInst_UIStatus_t412_0_0_0;
extern Il2CppGenericInst GenInst_OnStateLoadedListener_t842_0_0_0;
extern Il2CppGenericInst GenInst_Invitation_t341_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_IEvent_t913_0_0_0;
extern Il2CppGenericInst GenInst_EventManager_t548_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t544_0_0_0;
extern Il2CppGenericInst GenInst_ResponseStatus_t335_0_0_0_List_1_t915_0_0_0;
extern Il2CppGenericInst GenInst_FetchAllResponse_t664_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t546_0_0_0;
extern Il2CppGenericInst GenInst_ResponseStatus_t335_0_0_0_IEvent_t913_0_0_0;
extern Il2CppGenericInst GenInst_FetchResponse_t662_0_0_0;
extern Il2CppGenericInst GenInst_IQuest_t861_0_0_0;
extern Il2CppGenericInst GenInst_QuestManager_t560_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t550_0_0_0;
extern Il2CppGenericInst GenInst_ResponseStatus_t335_0_0_0_IQuest_t861_0_0_0;
extern Il2CppGenericInst GenInst_FetchResponse_t688_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t552_0_0_0;
extern Il2CppGenericInst GenInst_ResponseStatus_t335_0_0_0_List_1_t918_0_0_0;
extern Il2CppGenericInst GenInst_FetchListResponse_t689_0_0_0;
extern Il2CppGenericInst GenInst_Action_3_t554_0_0_0;
extern Il2CppGenericInst GenInst_QuestUiResult_t372_0_0_0_IQuest_t861_0_0_0_IQuestMilestone_t863_0_0_0;
extern Il2CppGenericInst GenInst_QuestUIResponse_t692_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t556_0_0_0;
extern Il2CppGenericInst GenInst_QuestAcceptStatus_t370_0_0_0_IQuest_t861_0_0_0;
extern Il2CppGenericInst GenInst_AcceptResponse_t691_0_0_0;
extern Il2CppGenericInst GenInst_IQuestMilestone_t863_0_0_0;
extern Il2CppGenericInst GenInst_Action_3_t558_0_0_0;
extern Il2CppGenericInst GenInst_QuestClaimMilestoneStatus_t371_0_0_0_IQuest_t861_0_0_0_IQuestMilestone_t863_0_0_0;
extern Il2CppGenericInst GenInst_ClaimMilestoneResponse_t690_0_0_0;
extern Il2CppGenericInst GenInst_RealtimeManager_t564_0_0_0;
extern Il2CppGenericInst GenInst_State_t565_0_0_0;
extern Il2CppGenericInst GenInst_RealTimeMultiplayerListener_t573_0_0_0;
extern Il2CppGenericInst GenInst_RoomSession_t566_0_0_0;
extern Il2CppGenericInst GenInst_NativeRealTimeRoom_t574_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerParticipant_t672_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_MultiplayerParticipant_t672_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerParticipant_t672_0_0_0_Participant_t340_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Participant_t340_0_0_0;
extern Il2CppGenericInst GenInst_Participant_t340_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerParticipant_t672_0_0_0;
extern Il2CppGenericInst GenInst_ParticipantStatus_t513_0_0_0;
extern Il2CppGenericInst GenInst_WaitingRoomUIResponse_t697_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_ResponseStatus_t409_0_0_0;
extern Il2CppGenericInst GenInst_RealTimeRoomResponse_t695_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerInvitation_t601_0_0_0;
extern Il2CppGenericInst GenInst_NativeClient_t524_0_0_0;
extern Il2CppGenericInst GenInst_NativeRealTimeRoom_t574_0_0_0_MultiplayerParticipant_t672_0_0_0_ByteU5BU5D_t350_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_NativeRealTimeRoom_t574_0_0_0_MultiplayerParticipant_t672_0_0_0;
extern Il2CppGenericInst GenInst_PlayerSelectUIResponse_t686_0_0_0;
extern Il2CppGenericInst GenInst_RoomInboxUIResponse_t696_0_0_0;
extern Il2CppGenericInst GenInst_FetchInvitationsResponse_t698_0_0_0;
extern Il2CppGenericInst GenInst_SnapshotManager_t610_0_0_0;
extern Il2CppGenericInst GenInst_NativeSnapshotMetadata_t611_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t612_0_0_0;
extern Il2CppGenericInst GenInst_SavedGameRequestStatus_t374_0_0_0_ISavedGameMetadata_t618_0_0_0;
extern Il2CppGenericInst GenInst_CommitResponse_t703_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t614_0_0_0;
extern Il2CppGenericInst GenInst_ByteU5BU5D_t350_0_0_0_ByteU5BU5D_t350_0_0_0;
extern Il2CppGenericInst GenInst_ReadResponse_t704_0_0_0;
extern Il2CppGenericInst GenInst_ISavedGameMetadata_t618_0_0_0;
extern Il2CppGenericInst GenInst_ConflictCallback_t621_0_0_0;
extern Il2CppGenericInst GenInst_OpenResponse_t701_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t625_0_0_0;
extern Il2CppGenericInst GenInst_SavedGameRequestStatus_t374_0_0_0_ByteU5BU5D_t350_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t627_0_0_0;
extern Il2CppGenericInst GenInst_SelectUIStatus_t375_0_0_0_ISavedGameMetadata_t618_0_0_0;
extern Il2CppGenericInst GenInst_SnapshotSelectUIResponse_t705_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t630_0_0_0;
extern Il2CppGenericInst GenInst_SavedGameRequestStatus_t374_0_0_0_List_1_t931_0_0_0;
extern Il2CppGenericInst GenInst_FetchAllResponse_t702_0_0_0;
extern Il2CppGenericInst GenInst_TurnBasedMatchResponse_t707_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerStatus_t413_0_0_0;
extern Il2CppGenericInst GenInst_Boolean_t203_0_0_0_TurnBasedMatch_t353_0_0_0;
extern Il2CppGenericInst GenInst_MatchInboxUIResponse_t706_0_0_0;
extern Il2CppGenericInst GenInst_TurnBasedMatchesResponse_t708_0_0_0;
extern Il2CppGenericInst GenInst_TurnBasedMatch_t353_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerParticipant_t672_0_0_0_NativeTurnBasedMatch_t680_0_0_0;
extern Il2CppGenericInst GenInst_NativeTurnBasedMatch_t680_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeAchievement_t673_0_0_0;
extern Il2CppGenericInst GenInst_GameServices_t534_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t660_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t866_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t655_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t867_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t653_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t940_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_NativeEvent_t674_0_0_0;
extern Il2CppGenericInst GenInst_NativeEvent_t674_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t664_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t662_0_0_0;
extern Il2CppGenericInst GenInst_AuthFinishedCallback_t665_0_0_0;
extern Il2CppGenericInst GenInst_AuthStartedCallback_t666_0_0_0;
extern Il2CppGenericInst GenInst_Action_3_t871_0_0_0;
extern Il2CppGenericInst GenInst_Action_3_t872_0_0_0;
extern Il2CppGenericInst GenInst_ParticipantStatus_t513_0_0_0_ParticipantStatus_t346_0_0_0;
extern Il2CppGenericInst GenInst_DateTime_t48_0_0_0;
extern Il2CppGenericInst GenInst_Byte_t237_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_MultiplayerParticipant_t672_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchSelfResponse_t684_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeQuest_t677_0_0_0;
extern Il2CppGenericInst GenInst_NativeQuest_t677_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchResponse_t688_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchListResponse_t689_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_QuestUIResponse_t692_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_AcceptResponse_t691_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_ClaimMilestoneResponse_t690_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t881_0_0_0;
extern Il2CppGenericInst GenInst_Action_4_t882_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_MultiplayerInvitation_t601_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_PlayerSelectUIResponse_t686_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RoomInboxUIResponse_t696_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WaitingRoomUIResponse_t697_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchInvitationsResponse_t698_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t889_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t890_0_0_0;
extern Il2CppGenericInst GenInst_MultiplayerParticipant_t672_0_0_0_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RealTimeRoomResponse_t695_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_NativeSnapshotMetadata_t611_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FetchAllResponse_t702_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_SnapshotSelectUIResponse_t705_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t895_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_OpenResponse_t701_0_0_0;
extern Il2CppGenericInst GenInst_NativeSnapshotMetadataChange_t679_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_CommitResponse_t703_0_0_0;
extern Il2CppGenericInst GenInst_Action_1_t897_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_ReadResponse_t704_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_TurnBasedMatchesResponse_t708_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_MatchInboxUIResponse_t706_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_TurnBasedMatchResponse_t707_0_0_0;
extern Il2CppGenericInst GenInst_Toggle_t720_0_0_0;
extern Il2CppGenericInst GenInst_Renderer_t976_0_0_0;
extern Il2CppGenericInst GenInst_Animation_t982_0_0_0;
extern Il2CppGenericInst GenInst_IAchievementU5BU5D_t732_0_0_0;
extern Il2CppGenericInst GenInst_gameControl_t794_0_0_0;
extern Il2CppGenericInst GenInst_LineRenderer_t743_0_0_0;
extern Il2CppGenericInst GenInst_SpriteRenderer_t744_0_0_0;
extern Il2CppGenericInst GenInst_Animator_t749_0_0_0;
extern Il2CppGenericInst GenInst_enemyController_t785_0_0_0;
extern Il2CppGenericInst GenInst_AudioSource_t755_0_0_0;
extern Il2CppGenericInst GenInst_enemyLife_t788_0_0_0;
extern Il2CppGenericInst GenInst_Frame_t770_0_0_0;
extern Il2CppGenericInst GenInst_Camera_t978_0_0_0;
extern Il2CppGenericInst GenInst_GameObject_t144_0_0_0;
extern Il2CppGenericInst GenInst_cameraScript_t774_0_0_0;
extern Il2CppGenericInst GenInst_headScr_t781_0_0_0;
extern Il2CppGenericInst GenInst_Rigidbody_t994_0_0_0;
extern Il2CppGenericInst GenInst_RaycastScr_t751_0_0_0;
extern Il2CppGenericInst GenInst_LaserControl_t745_0_0_0;
extern Il2CppGenericInst GenInst_enemyMov_t790_0_0_0;
extern Il2CppGenericInst GenInst_HUD_t733_0_0_0;
extern Il2CppGenericInst GenInst_playerMovement_t830_0_0_0;
extern Il2CppGenericInst GenInst_disparo_t784_0_0_0;
extern Il2CppGenericInst GenInst_songsBatch_t834_0_0_0;
extern Il2CppGenericInst GenInst_naveProxyController_t820_0_0_0;
extern Il2CppGenericInst GenInst_LineScr_t748_0_0_0;
extern Il2CppGenericInst GenInst_Matrix4x4_t997_0_0_0;
extern Il2CppGenericInst GenInst_Transform_t809_0_0_0;
extern Il2CppGenericInst GenInst_Vector3_t758_0_0_0;
extern Il2CppGenericInst GenInst_movementProxy_t815_0_0_0;
extern Il2CppGenericInst GenInst_disparoProxy_t786_0_0_0;
extern Il2CppGenericInst GenInst_gizmosProxy_t804_0_0_0;
extern Il2CppGenericInst GenInst_randomColorScr_t832_0_0_0;
extern Il2CppGenericInst GenInst_collideAndDie_t779_0_0_0;
extern Il2CppGenericInst GenInst_paintBlue_t827_0_0_0;
extern Il2CppGenericInst GenInst_paqueteScr_t828_0_0_0;
extern Il2CppGenericInst GenInst_Coin_t727_0_0_0;
extern Il2CppGenericInst GenInst_triggerSplashBomb_t836_0_0_0;
extern Il2CppGenericInst GenInst_RayoScr_t752_0_0_0;
extern Il2CppGenericInst GenInst_UIStatus_t336_0_0_0;
extern Il2CppGenericInst GenInst_AdvertisingResult_t354_0_0_0;
extern Il2CppGenericInst GenInst_ConnectionRequest_t355_0_0_0;
extern Il2CppGenericInst GenInst_ConnectionResponse_t358_0_0_0;
extern Il2CppGenericInst GenInst_INearbyConnectionClient_t388_0_0_0;
extern Il2CppGenericInst GenInst_IUserProfileU5BU5D_t850_0_0_0;
extern Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t906_0_0_0;
extern Il2CppGenericInst GenInst_IScoreU5BU5D_t907_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey15_1_t1001_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey14_1_t1000_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey17_2_t1003_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey17_2_t1003_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey16_2_t1002_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey16_2_t1002_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey19_3_t1005_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey19_3_t1005_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey19_3_t1005_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey18_3_t1004_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey18_3_t1004_gp_1_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey18_3_t1004_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m4418_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m4419_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m4419_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_CallbackUtils_ToOnGameThread_m4420_gp_0_0_0_0_CallbackUtils_ToOnGameThread_m4420_gp_1_0_0_0_CallbackUtils_ToOnGameThread_m4420_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t1006_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t1007_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_NativeClient_AsOnGameThreadCallback_m4436_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_NativeClient_InvokeCallbackOnGameThread_m4437_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey4C_2_t1009_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey4C_2_t1009_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CToOnGameThreadU3Ec__AnonStorey4B_2_t1008_gp_0_0_0_0_U3CToOnGameThreadU3Ec__AnonStorey4B_2_t1008_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_NativeSavedGameClient_ToOnGameThread_m4444_gp_0_0_0_0_NativeSavedGameClient_ToOnGameThread_m4444_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey5C_1_t1011_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CToIntPtrU3Ec__AnonStorey5C_1_t1011_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_U3CToIntPtrU3Ec__AnonStorey5D_2_t1012_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CToIntPtrU3Ec__AnonStorey5D_2_t1012_gp_0_0_0_0_U3CToIntPtrU3Ec__AnonStorey5D_2_t1012_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t1014_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t1013_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t1016_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t1016_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t1015_gp_0_0_0_0_U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t1015_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m4451_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m4451_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m4452_gp_0_0_0_0_Callbacks_ToIntPtr_m4452_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Callbacks_ToIntPtr_m4452_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_ToIntPtr_m4452_gp_0_0_0_0_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_IntPtrToTempCallback_m4453_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_IntPtrToPermanentCallback_m4455_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Action_2_t1121_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_PerformInternalCallback_m4456_gp_0_0_0_0_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_AsOnGameThreadCallback_m4457_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Callbacks_AsOnGameThreadCallback_m4458_gp_0_0_0_0_Callbacks_AsOnGameThreadCallback_m4458_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CToEnumerableU3Ec__Iterator0_1_t1018_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_U3CToEnumerableU3Ec__Iterator0_1_t1018_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_PInvokeUtilities_OutParamsToArray_m4471_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerable_m4472_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_PInvokeUtilities_ToEnumerable_m4472_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_PInvokeUtilities_ToEnumerator_m4473_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_PInvokeUtilities_ToEnumerator_m4473_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_BaseInputModule_t1152_0_0_0;
extern Il2CppGenericInst GenInst_RaycastResult_t1187_0_0_0;
extern Il2CppGenericInst GenInst_IDeselectHandler_t1353_0_0_0;
extern Il2CppGenericInst GenInst_ISelectHandler_t1352_0_0_0;
extern Il2CppGenericInst GenInst_BaseEventData_t1153_0_0_0;
extern Il2CppGenericInst GenInst_IPointerEnterHandler_t1340_0_0_0;
extern Il2CppGenericInst GenInst_IPointerExitHandler_t1341_0_0_0;
extern Il2CppGenericInst GenInst_IPointerDownHandler_t1342_0_0_0;
extern Il2CppGenericInst GenInst_IPointerUpHandler_t1343_0_0_0;
extern Il2CppGenericInst GenInst_IPointerClickHandler_t1344_0_0_0;
extern Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t1345_0_0_0;
extern Il2CppGenericInst GenInst_IBeginDragHandler_t1346_0_0_0;
extern Il2CppGenericInst GenInst_IDragHandler_t1347_0_0_0;
extern Il2CppGenericInst GenInst_IEndDragHandler_t1348_0_0_0;
extern Il2CppGenericInst GenInst_IDropHandler_t1349_0_0_0;
extern Il2CppGenericInst GenInst_IScrollHandler_t1350_0_0_0;
extern Il2CppGenericInst GenInst_IUpdateSelectedHandler_t1351_0_0_0;
extern Il2CppGenericInst GenInst_IMoveHandler_t1354_0_0_0;
extern Il2CppGenericInst GenInst_ISubmitHandler_t1355_0_0_0;
extern Il2CppGenericInst GenInst_ICancelHandler_t1356_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t1358_0_0_0;
extern Il2CppGenericInst GenInst_IEventSystemHandler_t1428_0_0_0;
extern Il2CppGenericInst GenInst_PointerEventData_t1191_0_0_0;
extern Il2CppGenericInst GenInst_AxisEventData_t1188_0_0_0;
extern Il2CppGenericInst GenInst_BaseRaycaster_t1186_0_0_0;
extern Il2CppGenericInst GenInst_EventSystem_t1155_0_0_0;
extern Il2CppGenericInst GenInst_ButtonState_t1194_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_PointerEventData_t1191_0_0_0;
extern Il2CppGenericInst GenInst_RaycastHit_t990_0_0_0;
extern Il2CppGenericInst GenInst_Color_t747_0_0_0;
extern Il2CppGenericInst GenInst_ICanvasElement_t1360_0_0_0;
extern Il2CppGenericInst GenInst_Font_t1222_0_0_0_List_1_t1382_0_0_0;
extern Il2CppGenericInst GenInst_Text_t1263_0_0_0;
extern Il2CppGenericInst GenInst_Font_t1222_0_0_0;
extern Il2CppGenericInst GenInst_ColorTween_t1209_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t1267_0_0_0;
extern Il2CppGenericInst GenInst_UIVertex_t1265_0_0_0;
extern Il2CppGenericInst GenInst_RectTransform_t1227_0_0_0;
extern Il2CppGenericInst GenInst_Canvas_t1229_0_0_0;
extern Il2CppGenericInst GenInst_CanvasRenderer_t1228_0_0_0;
extern Il2CppGenericInst GenInst_Component_t230_0_0_0;
extern Il2CppGenericInst GenInst_Graphic_t1233_0_0_0;
extern Il2CppGenericInst GenInst_Canvas_t1229_0_0_0_IndexedSet_1_t1393_0_0_0;
extern Il2CppGenericInst GenInst_Sprite_t766_0_0_0;
extern Il2CppGenericInst GenInst_Type_t1240_0_0_0;
extern Il2CppGenericInst GenInst_FillMethod_t1241_0_0_0;
extern Il2CppGenericInst GenInst_Single_t202_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_SubmitEvent_t1254_0_0_0;
extern Il2CppGenericInst GenInst_OnChangeEvent_t1256_0_0_0;
extern Il2CppGenericInst GenInst_OnValidateInput_t1258_0_0_0;
extern Il2CppGenericInst GenInst_ContentType_t1250_0_0_0;
extern Il2CppGenericInst GenInst_LineType_t1253_0_0_0;
extern Il2CppGenericInst GenInst_InputType_t1251_0_0_0;
extern Il2CppGenericInst GenInst_TouchScreenKeyboardType_t1395_0_0_0;
extern Il2CppGenericInst GenInst_CharacterValidation_t1252_0_0_0;
extern Il2CppGenericInst GenInst_UILineInfo_t1398_0_0_0;
extern Il2CppGenericInst GenInst_UICharInfo_t1400_0_0_0;
extern Il2CppGenericInst GenInst_LayoutElement_t1320_0_0_0;
extern Il2CppGenericInst GenInst_Direction_t1274_0_0_0;
extern Il2CppGenericInst GenInst_Vector2_t739_0_0_0;
extern Il2CppGenericInst GenInst_CanvasGroup_t1387_0_0_0;
extern Il2CppGenericInst GenInst_Selectable_t1215_0_0_0;
extern Il2CppGenericInst GenInst_Navigation_t1272_0_0_0;
extern Il2CppGenericInst GenInst_Transition_t1286_0_0_0;
extern Il2CppGenericInst GenInst_ColorBlock_t1221_0_0_0;
extern Il2CppGenericInst GenInst_SpriteState_t1290_0_0_0;
extern Il2CppGenericInst GenInst_AnimationTriggers_t1210_0_0_0;
extern Il2CppGenericInst GenInst_Direction_t1292_0_0_0;
extern Il2CppGenericInst GenInst_Image_t1248_0_0_0;
extern Il2CppGenericInst GenInst_MatEntry_t1296_0_0_0;
extern Il2CppGenericInst GenInst_Toggle_t720_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_AspectMode_t1305_0_0_0;
extern Il2CppGenericInst GenInst_FitMode_t1311_0_0_0;
extern Il2CppGenericInst GenInst_Corner_t1313_0_0_0;
extern Il2CppGenericInst GenInst_Axis_t1314_0_0_0;
extern Il2CppGenericInst GenInst_Constraint_t1315_0_0_0;
extern Il2CppGenericInst GenInst_RectOffset_t1321_0_0_0;
extern Il2CppGenericInst GenInst_TextAnchor_t1410_0_0_0;
extern Il2CppGenericInst GenInst_ILayoutElement_t1367_0_0_0_Single_t202_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t1368_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t1366_0_0_0;
extern Il2CppGenericInst GenInst_Entry_t1159_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteEvents_Execute_m6204_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m6205_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m6207_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m6208_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m6209_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_TweenRunner_1_t1434_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_LayoutRebuilder_t1325_0_0_0;
extern Il2CppGenericInst GenInst_IndexedSet_1_t1444_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_IndexedSet_1_t1444_gp_0_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_ObjectPool_1_t1445_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GcLeaderboard_t1501_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_LayoutCache_t1520_0_0_0;
extern Il2CppGenericInst GenInst_GUILayoutEntry_t1523_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t724_0_0_0;
extern Il2CppGenericInst GenInst_Rigidbody2D_t1578_0_0_0;
extern Il2CppGenericInst GenInst_Type_t_0_0_0;
extern Il2CppGenericInst GenInst_GUILayer_t1510_0_0_0;
extern Il2CppGenericInst GenInst_PersistentCall_t1650_0_0_0;
extern Il2CppGenericInst GenInst_BaseInvokableCall_t1647_0_0_0;
extern Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7561_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m7562_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Component_GetComponents_m7563_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GameObject_GetComponents_m7566_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m7567_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m7568_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Event_t1269_0_0_0_TextEditOp_t1643_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_1_t1708_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_2_t1709_gp_0_0_0_0_InvokableCall_2_t1709_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_2_t1709_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_2_t1709_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_3_t1710_gp_0_0_0_0_InvokableCall_3_t1710_gp_1_0_0_0_InvokableCall_3_t1710_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_3_t1710_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_3_t1710_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_3_t1710_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_4_t1711_gp_0_0_0_0_InvokableCall_4_t1711_gp_1_0_0_0_InvokableCall_4_t1711_gp_2_0_0_0_InvokableCall_4_t1711_gp_3_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_4_t1711_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_4_t1711_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_4_t1711_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_InvokableCall_4_t1711_gp_3_0_0_0;
extern Il2CppGenericInst GenInst_CachedInvokableCall_1_t1699_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UnityEvent_1_t1712_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_UnityEvent_2_t1713_gp_0_0_0_0_UnityEvent_2_t1713_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_UnityEvent_3_t1714_gp_0_0_0_0_UnityEvent_3_t1714_gp_1_0_0_0_UnityEvent_3_t1714_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_UnityEvent_4_t1715_gp_0_0_0_0_UnityEvent_4_t1715_gp_1_0_0_0_UnityEvent_4_t1715_gp_2_0_0_0_UnityEvent_4_t1715_gp_3_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t1817_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_PrimeHelper_t1818_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_HashSet_1_t1815_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Function_1_t1819_gp_0_0_0_0_Function_1_t1819_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Function_1_t1819_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1820_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateExceptIteratorU3Ec__Iterator4_1_t1821_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t1822_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1823_gp_0_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Cast_m7730_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m7731_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Contains_m7732_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Count_m7733_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Except_m7734_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Except_m7735_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateExceptIterator_m7736_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_First_m7737_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_First_m7737_gp_0_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m7738_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m7738_gp_0_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_LongCount_m7739_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Select_m7740_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Select_m7740_gp_0_0_0_0_Enumerable_Select_m7740_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Select_m7740_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m7741_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m7741_gp_0_0_0_0_Enumerable_CreateSelectIterator_m7741_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m7741_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToArray_m7742_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7743_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7743_gp_0_0_0_0_Enumerable_ToDictionary_m7743_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7743_gp_0_0_0_0_Enumerable_ToDictionary_m7743_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7743_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7743_gp_1_0_0_0_Enumerable_ToDictionary_m7743_gp_2_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7744_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7744_gp_0_0_0_0_Enumerable_ToDictionary_m7744_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7744_gp_1_0_0_0_Enumerable_ToDictionary_m7744_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7745_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7745_gp_0_0_0_0_Enumerable_ToDictionary_m7745_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7745_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7745_gp_0_0_0_0_Enumerable_ToDictionary_m7745_gp_1_0_0_0_Enumerable_ToDictionary_m7745_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToDictionary_m7745_gp_1_0_0_0_Enumerable_ToDictionary_m7745_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_ToList_m7746_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Where_m7747_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_Where_m7747_gp_0_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7748_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m7748_gp_0_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2165_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Stack_1_t2164_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_StrongName_t2727_0_0_0;
extern Il2CppGenericInst GenInst_DateTimeOffset_t2793_0_0_0;
extern Il2CppGenericInst GenInst_Guid_t2814_0_0_0;
extern Il2CppGenericInst GenInst_Int64_t233_0_0_0;
extern Il2CppGenericInst GenInst_UInt32_t235_0_0_0;
extern Il2CppGenericInst GenInst_UInt64_t239_0_0_0;
extern Il2CppGenericInst GenInst_SByte_t236_0_0_0;
extern Il2CppGenericInst GenInst_Int16_t238_0_0_0;
extern Il2CppGenericInst GenInst_UInt16_t194_0_0_0;
extern Il2CppGenericInst GenInst_IEnumerable_1_t2928_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Double_t234_0_0_0;
extern Il2CppGenericInst GenInst_Decimal_t240_0_0_0;
extern Il2CppGenericInst GenInst_InternalEnumerator_1_t2929_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2931_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2930_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m14458_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14470_gp_0_0_0_0_Array_Sort_m14470_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14471_gp_0_0_0_0_Array_Sort_m14471_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14472_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14472_gp_0_0_0_0_Array_Sort_m14472_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14473_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14473_gp_0_0_0_0_Array_Sort_m14473_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14474_gp_0_0_0_0_Array_Sort_m14474_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14475_gp_0_0_0_0_Array_Sort_m14475_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14476_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14476_gp_0_0_0_0_Array_Sort_m14476_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14477_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14477_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14477_gp_0_0_0_0_Array_Sort_m14477_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14478_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Sort_m14479_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_qsort_m14480_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_qsort_m14480_gp_0_0_0_0_Array_qsort_m14480_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_compare_m14481_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_qsort_m14482_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Resize_m14485_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_TrueForAll_m14487_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_ForEach_m14488_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_ConvertAll_m14489_gp_0_0_0_0_Array_ConvertAll_m14489_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindLastIndex_m14490_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindLastIndex_m14491_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindLastIndex_m14492_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindIndex_m14493_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindIndex_m14494_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindIndex_m14495_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_BinarySearch_m14496_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_BinarySearch_m14497_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_BinarySearch_m14498_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_BinarySearch_m14499_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_IndexOf_m14500_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_IndexOf_m14501_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_IndexOf_m14502_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_LastIndexOf_m14503_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_LastIndexOf_m14504_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_LastIndexOf_m14505_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindAll_m14506_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Exists_m14507_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_AsReadOnly_m14508_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_Find_m14509_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Array_FindLast_m14510_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_IList_1_t2932_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ICollection_1_t2933_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Nullable_1_t2893_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_DefaultComparer_t2940_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Comparer_1_t2939_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GenericComparer_1_t2941_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ShimEnumerator_t2943_gp_0_0_0_0_ShimEnumerator_t2943_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2944_gp_0_0_0_0_Enumerator_t2944_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3233_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2946_gp_0_0_0_0_Enumerator_t2946_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2946_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_KeyCollection_t2945_gp_0_0_0_0_KeyCollection_t2945_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyCollection_t2945_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_KeyCollection_t2945_gp_0_0_0_0_KeyCollection_t2945_gp_1_0_0_0_KeyCollection_t2945_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_KeyCollection_t2945_gp_0_0_0_0_KeyCollection_t2945_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2948_gp_0_0_0_0_Enumerator_t2948_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2948_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_ValueCollection_t2947_gp_0_0_0_0_ValueCollection_t2947_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_ValueCollection_t2947_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_ValueCollection_t2947_gp_0_0_0_0_ValueCollection_t2947_gp_1_0_0_0_ValueCollection_t2947_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_ValueCollection_t2947_gp_1_0_0_0_ValueCollection_t2947_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0_Dictionary_2_t2942_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3269_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0_Dictionary_2_t2942_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m14653_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0_Dictionary_2_t2942_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m14658_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m14658_gp_0_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0_Dictionary_2_t2942_gp_1_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_DictionaryEntry_t2128_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_0_0_0_0_Dictionary_2_t2942_gp_1_0_0_0_KeyValuePair_2_t3269_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3269_0_0_0_KeyValuePair_2_t3269_0_0_0;
extern Il2CppGenericInst GenInst_Dictionary_2_t2942_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_DefaultComparer_t2951_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_EqualityComparer_1_t2950_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2952_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4657_0_0_0;
extern Il2CppGenericInst GenInst_IDictionary_2_t2954_gp_0_0_0_0_IDictionary_2_t2954_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t2956_gp_0_0_0_0_KeyValuePair_2_t2956_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_Enumerator_t2958_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t2957_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_List_1_t2957_gp_0_0_0_0_List_1_ConvertAll_m14785_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_List_1_ConvertAll_m14785_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Collection_1_t2959_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2960_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m14932_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m14932_gp_1_0_0_0;
extern Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m14933_gp_0_0_0_0;
extern Il2CppGenericInst GenInst_Version_t1997_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3407_0_0_0;
extern Il2CppGenericInst GenInst_ValueType_t329_0_0_0;
extern Il2CppGenericInst GenInst_IConvertible_t313_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_t314_0_0_0;
extern Il2CppGenericInst GenInst_IEnumerable_t38_0_0_0;
extern Il2CppGenericInst GenInst_ICloneable_t309_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3030_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3031_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3412_0_0_0;
extern Il2CppGenericInst GenInst_IReflect_t2936_0_0_0;
extern Il2CppGenericInst GenInst__Type_t2934_0_0_0;
extern Il2CppGenericInst GenInst_MemberInfo_t_0_0_0;
extern Il2CppGenericInst GenInst_ICustomAttributeProvider_t2890_0_0_0;
extern Il2CppGenericInst GenInst__MemberInfo_t2935_0_0_0;
extern Il2CppGenericInst GenInst_IFormattable_t312_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t2997_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t2998_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3038_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3039_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3018_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3019_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3026_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3027_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3412_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3412_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3407_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t164_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t164_0_0_0_KeyValuePair_2_t196_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3457_0_0_0_String_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3457_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3457_0_0_0_KeyValuePair_2_t3463_0_0_0;
extern Il2CppGenericInst GenInst_UnityKeyValuePair_2_t3457_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3463_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3407_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t30_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t198_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Reward_t55_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3525_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3531_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t189_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t189_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t189_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t189_0_0_0_KeyValuePair_2_t3531_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t189_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3545_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3009_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3010_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3043_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3044_0_0_0;
extern Il2CppGenericInst GenInst_SoomlaEntity_1_t127_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_LocalUpgrade_t101_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3572_0_0_0;
extern Il2CppGenericInst GenInst_Link_t3579_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_VirtualItem_t125_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3601_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_PurchasableVirtualItem_t124_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3607_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_VirtualCategory_t126_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3612_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t178_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3618_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t237_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t203_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Action_1_t3422_0_0_0_IEnumerator_t37_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Byte_t237_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_UInt32_t235_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3656_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3004_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3005_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_UInt32_t235_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_UInt32_t235_0_0_0_UInt32_t235_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_UInt32_t235_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_UInt32_t235_0_0_0_KeyValuePair_2_t3656_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_UInt32_t235_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3671_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_ParticipantResult_t342_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3677_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_IUserProfile_t983_0_0_0;
extern Il2CppGenericInst GenInst_IAchievementDescription_t1707_0_0_0;
extern Il2CppGenericInst GenInst_IAchievement_t856_0_0_0;
extern Il2CppGenericInst GenInst_IScore_t1022_0_0_0;
extern Il2CppGenericInst GenInst_ISerializable_t310_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Achievement_t333_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3714_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Byte_t237_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_MultiplayerParticipant_t672_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3741_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Participant_t340_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3744_0_0_0;
extern Il2CppGenericInst GenInst_Link_t3760_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_BaseReferenceHolder_t654_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_BaseReferenceHolder_t654_0_0_0;
extern Il2CppGenericInst GenInst_UIntPtr_t_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_BaseReferenceHolder_t654_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3790_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Int32_t189_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Int32_t189_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Int32_t189_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Int32_t189_0_0_0_KeyValuePair_2_t3790_0_0_0;
extern Il2CppGenericInst GenInst_ParticipantStatus_t513_0_0_0_ParticipantStatus_t346_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3801_0_0_0;
extern Il2CppGenericInst GenInst_Object_t187_0_0_0;
extern Il2CppGenericInst GenInst_Touch_t901_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3921_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Object_t_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Object_t_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_Object_t_0_0_0_KeyValuePair_2_t3921_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_PointerEventData_t1191_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t1373_0_0_0;
extern Il2CppGenericInst GenInst_RaycastHit2D_t1378_0_0_0;
extern Il2CppGenericInst GenInst_ICanvasElement_t1360_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_ICanvasElement_t1360_0_0_0_Int32_t189_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Font_t1222_0_0_0_List_1_t1382_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3959_0_0_0;
extern Il2CppGenericInst GenInst_Graphic_t1233_0_0_0_Int32_t189_0_0_0;
extern Il2CppGenericInst GenInst_Graphic_t1233_0_0_0_Int32_t189_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Canvas_t1229_0_0_0_IndexedSet_1_t1393_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3993_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3997_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4001_0_0_0;
extern Il2CppGenericInst GenInst_Enum_t218_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t202_0_0_0;
extern Il2CppGenericInst GenInst_AchievementDescription_t1626_0_0_0;
extern Il2CppGenericInst GenInst_UserProfile_t1624_0_0_0;
extern Il2CppGenericInst GenInst_GcAchievementData_t1617_0_0_0;
extern Il2CppGenericInst GenInst_Achievement_t1625_0_0_0;
extern Il2CppGenericInst GenInst_GcScoreData_t1618_0_0_0;
extern Il2CppGenericInst GenInst_Score_t1627_0_0_0;
extern Il2CppGenericInst GenInst_GUILayoutOption_t1527_0_0_0;
extern Il2CppGenericInst GenInst_Int32_t189_0_0_0_LayoutCache_t1520_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4067_0_0_0;
extern Il2CppGenericInst GenInst_GUIStyle_t724_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t724_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4079_0_0_0;
extern Il2CppGenericInst GenInst_Behaviour_t1416_0_0_0;
extern Il2CppGenericInst GenInst_Display_t1561_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3035_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3036_0_0_0;
extern Il2CppGenericInst GenInst_Keyframe_t1591_0_0_0;
extern Il2CppGenericInst GenInst_DisallowMultipleComponent_t1437_0_0_0;
extern Il2CppGenericInst GenInst_Attribute_t1546_0_0_0;
extern Il2CppGenericInst GenInst__Attribute_t1731_0_0_0;
extern Il2CppGenericInst GenInst_ExecuteInEditMode_t1438_0_0_0;
extern Il2CppGenericInst GenInst_RequireComponent_t1432_0_0_0;
extern Il2CppGenericInst GenInst_ParameterModifier_t2550_0_0_0;
extern Il2CppGenericInst GenInst_HitInfo_t1629_0_0_0;
extern Il2CppGenericInst GenInst_ParameterInfo_t1693_0_0_0;
extern Il2CppGenericInst GenInst__ParameterInfo_t2977_0_0_0;
extern Il2CppGenericInst GenInst_Event_t1269_0_0_0;
extern Il2CppGenericInst GenInst_Event_t1269_0_0_0_TextEditOp_t1643_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4136_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4171_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t237_0_0_0_Object_t_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t237_0_0_0_Byte_t237_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t237_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t237_0_0_0_KeyValuePair_2_t4171_0_0_0;
extern Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t203_0_0_0_DictionaryEntry_t2128_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4185_0_0_0;
extern Il2CppGenericInst GenInst_X509Certificate_t2026_0_0_0;
extern Il2CppGenericInst GenInst_IDeserializationCallback_t1842_0_0_0;
extern Il2CppGenericInst GenInst_X509ChainStatus_t2034_0_0_0;
extern Il2CppGenericInst GenInst_Capture_t2056_0_0_0;
extern Il2CppGenericInst GenInst_Group_t2059_0_0_0;
extern Il2CppGenericInst GenInst_Mark_t2083_0_0_0;
extern Il2CppGenericInst GenInst_UriScheme_t2118_0_0_0;
extern Il2CppGenericInst GenInst_BigInteger_t2191_0_0_0;
extern Il2CppGenericInst GenInst_KeySizes_t2311_0_0_0;
extern Il2CppGenericInst GenInst_ClientCertificateType_t2283_0_0_0;
extern Il2CppGenericInst GenInst_Delegate_t211_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3007_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3008_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3015_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3016_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3012_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3013_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3002_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3003_0_0_0;
extern Il2CppGenericInst GenInst_MethodInfo_t_0_0_0;
extern Il2CppGenericInst GenInst__MethodInfo_t2975_0_0_0;
extern Il2CppGenericInst GenInst_MethodBase_t1691_0_0_0;
extern Il2CppGenericInst GenInst__MethodBase_t2974_0_0_0;
extern Il2CppGenericInst GenInst_ConstructorInfo_t1698_0_0_0;
extern Il2CppGenericInst GenInst__ConstructorInfo_t2971_0_0_0;
extern Il2CppGenericInst GenInst_TableRange_t2360_0_0_0;
extern Il2CppGenericInst GenInst_TailoringInfo_t2363_0_0_0;
extern Il2CppGenericInst GenInst_Contraction_t2364_0_0_0;
extern Il2CppGenericInst GenInst_Level2Map_t2366_0_0_0;
extern Il2CppGenericInst GenInst_BigInteger_t2386_0_0_0;
extern Il2CppGenericInst GenInst_Slot_t2436_0_0_0;
extern Il2CppGenericInst GenInst_Slot_t2443_0_0_0;
extern Il2CppGenericInst GenInst_StackFrame_t1690_0_0_0;
extern Il2CppGenericInst GenInst_Calendar_t2456_0_0_0;
extern Il2CppGenericInst GenInst_ModuleBuilder_t2516_0_0_0;
extern Il2CppGenericInst GenInst__ModuleBuilder_t2966_0_0_0;
extern Il2CppGenericInst GenInst_Module_t2517_0_0_0;
extern Il2CppGenericInst GenInst__Module_t2976_0_0_0;
extern Il2CppGenericInst GenInst_ParameterBuilder_t2518_0_0_0;
extern Il2CppGenericInst GenInst__ParameterBuilder_t2967_0_0_0;
extern Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2514_0_0_0;
extern Il2CppGenericInst GenInst_MethodBuilder_t2513_0_0_0;
extern Il2CppGenericInst GenInst__MethodBuilder_t2965_0_0_0;
extern Il2CppGenericInst GenInst_ConstructorBuilder_t2509_0_0_0;
extern Il2CppGenericInst GenInst__ConstructorBuilder_t2962_0_0_0;
extern Il2CppGenericInst GenInst_FieldBuilder_t2512_0_0_0;
extern Il2CppGenericInst GenInst__FieldBuilder_t2964_0_0_0;
extern Il2CppGenericInst GenInst_FieldInfo_t_0_0_0;
extern Il2CppGenericInst GenInst__FieldInfo_t2973_0_0_0;
extern Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0;
extern Il2CppGenericInst GenInst__PropertyInfo_t2978_0_0_0;
extern Il2CppGenericInst GenInst_Header_t2611_0_0_0;
extern Il2CppGenericInst GenInst_ITrackingHandler_t2901_0_0_0;
extern Il2CppGenericInst GenInst_IContextAttribute_t2896_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3371_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3372_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3041_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3042_0_0_0;
extern Il2CppGenericInst GenInst_IComparable_1_t3390_0_0_0;
extern Il2CppGenericInst GenInst_IEquatable_1_t3391_0_0_0;
extern Il2CppGenericInst GenInst_TypeTag_t2649_0_0_0;
extern Il2CppGenericInst GenInst_MonoType_t_0_0_0;
extern Il2CppGenericInst GenInst_ScriptableObject_t15_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3531_0_0_0_KeyValuePair_2_t3531_0_0_0;
extern Il2CppGenericInst GenInst_UInt32_t235_0_0_0_UInt32_t235_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3656_0_0_0_KeyValuePair_2_t3656_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3790_0_0_0_KeyValuePair_2_t3790_0_0_0;
extern Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0;
extern Il2CppGenericInst GenInst_Matrix4x4_t997_0_0_0_Matrix4x4_t997_0_0_0;
extern Il2CppGenericInst GenInst_Vector3_t758_0_0_0_Vector3_t758_0_0_0;
extern Il2CppGenericInst GenInst_RaycastResult_t1187_0_0_0_RaycastResult_t1187_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t3921_0_0_0_KeyValuePair_2_t3921_0_0_0;
extern Il2CppGenericInst GenInst_UIVertex_t1265_0_0_0_UIVertex_t1265_0_0_0;
extern Il2CppGenericInst GenInst_UICharInfo_t1400_0_0_0_UICharInfo_t1400_0_0_0;
extern Il2CppGenericInst GenInst_UILineInfo_t1398_0_0_0_UILineInfo_t1398_0_0_0;
extern Il2CppGenericInst GenInst_Byte_t237_0_0_0_Byte_t237_0_0_0;
extern Il2CppGenericInst GenInst_KeyValuePair_2_t4171_0_0_0_KeyValuePair_2_t4171_0_0_0;
const methodPointerType g_Il2CppMethodPointers[3720] = 
{
	NULL/* 0*/,
	(methodPointerType)&UnityNameValuePair_1_get_Key_m15859_gshared/* 1*/,
	(methodPointerType)&UnityNameValuePair_1_set_Key_m15860_gshared/* 2*/,
	(methodPointerType)&UnityNameValuePair_1__ctor_m15857_gshared/* 3*/,
	(methodPointerType)&UnityNameValuePair_1__ctor_m15858_gshared/* 4*/,
	(methodPointerType)&UnityKeyValuePair_2_get_Key_m15547_gshared/* 5*/,
	(methodPointerType)&UnityKeyValuePair_2_set_Key_m15548_gshared/* 6*/,
	(methodPointerType)&UnityKeyValuePair_2_get_Value_m15549_gshared/* 7*/,
	(methodPointerType)&UnityKeyValuePair_2_set_Value_m15550_gshared/* 8*/,
	(methodPointerType)&UnityKeyValuePair_2__ctor_m15545_gshared/* 9*/,
	(methodPointerType)&UnityKeyValuePair_2__ctor_m15546_gshared/* 10*/,
	(methodPointerType)&UnityDictionary_2_get_Item_m15266_gshared/* 11*/,
	(methodPointerType)&UnityDictionary_2_set_Item_m15267_gshared/* 12*/,
	(methodPointerType)&UnityDictionary_2_get_Count_m15276_gshared/* 13*/,
	(methodPointerType)&UnityDictionary_2_get_Keys_m15279_gshared/* 14*/,
	(methodPointerType)&UnityDictionary_2_get_Values_m15280_gshared/* 15*/,
	(methodPointerType)&UnityDictionary_2_get_Items_m15282_gshared/* 16*/,
	(methodPointerType)&UnityDictionary_2_get_SyncRoot_m15284_gshared/* 17*/,
	(methodPointerType)&UnityDictionary_2_get_IsFixedSize_m15286_gshared/* 18*/,
	(methodPointerType)&UnityDictionary_2_get_IsReadOnly_m15287_gshared/* 19*/,
	(methodPointerType)&UnityDictionary_2_get_IsSynchronized_m15289_gshared/* 20*/,
	(methodPointerType)&UnityDictionary_2__ctor_m15264_gshared/* 21*/,
	(methodPointerType)&UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared/* 22*/,
	(methodPointerType)&UnityDictionary_2_Add_m15268_gshared/* 23*/,
	(methodPointerType)&UnityDictionary_2_Add_m15269_gshared/* 24*/,
	(methodPointerType)&UnityDictionary_2_TryGetValue_m15270_gshared/* 25*/,
	(methodPointerType)&UnityDictionary_2_Remove_m15271_gshared/* 26*/,
	(methodPointerType)&UnityDictionary_2_Remove_m15272_gshared/* 27*/,
	(methodPointerType)&UnityDictionary_2_Clear_m15273_gshared/* 28*/,
	(methodPointerType)&UnityDictionary_2_ContainsKey_m15274_gshared/* 29*/,
	(methodPointerType)&UnityDictionary_2_Contains_m15275_gshared/* 30*/,
	(methodPointerType)&UnityDictionary_2_CopyTo_m15277_gshared/* 31*/,
	(methodPointerType)&UnityDictionary_2_GetEnumerator_m15278_gshared/* 32*/,
	(methodPointerType)&UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared/* 33*/,
	(methodPointerType)&UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared/* 34*/,
	(methodPointerType)&UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared/* 35*/,
	(methodPointerType)&UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared/* 36*/,
	(methodPointerType)&UnityDictionaryEnumerator_System_Collections_IEnumerator_get_Current_m15777_gshared/* 37*/,
	(methodPointerType)&UnityDictionaryEnumerator_get_Current_m15778_gshared/* 38*/,
	(methodPointerType)&UnityDictionaryEnumerator_get_Entry_m15779_gshared/* 39*/,
	(methodPointerType)&UnityDictionaryEnumerator_get_Key_m15781_gshared/* 40*/,
	(methodPointerType)&UnityDictionaryEnumerator_get_Value_m15782_gshared/* 41*/,
	(methodPointerType)&UnityDictionaryEnumerator__ctor_m15775_gshared/* 42*/,
	(methodPointerType)&UnityDictionaryEnumerator__ctor_m15776_gshared/* 43*/,
	(methodPointerType)&UnityDictionaryEnumerator_Dispose_m15780_gshared/* 44*/,
	(methodPointerType)&UnityDictionaryEnumerator_MoveNext_m15783_gshared/* 45*/,
	(methodPointerType)&UnityDictionaryEnumerator_ValidateIndex_m15784_gshared/* 46*/,
	(methodPointerType)&UnityDictionaryEnumerator_Reset_m15785_gshared/* 47*/,
	(methodPointerType)&U3CU3Ec__AnonStorey4__ctor_m15599_gshared/* 48*/,
	(methodPointerType)&U3CU3Ec__AnonStorey4_U3CU3Em__3_m15600_gshared/* 49*/,
	(methodPointerType)&U3CRemoveU3Ec__AnonStorey5__ctor_m15771_gshared/* 50*/,
	(methodPointerType)&U3CRemoveU3Ec__AnonStorey5_U3CU3Em__4_m15772_gshared/* 51*/,
	(methodPointerType)&U3CContainsKeyU3Ec__AnonStorey6__ctor_m15773_gshared/* 52*/,
	(methodPointerType)&U3CContainsKeyU3Ec__AnonStorey6_U3CU3Em__5_m15774_gshared/* 53*/,
	(methodPointerType)&UnityDictionary_1__ctor_m15867_gshared/* 54*/,
	(methodPointerType)&SoomlaEntity_1_get_ID_m16334_gshared/* 55*/,
	(methodPointerType)&SoomlaEntity_1__ctor_m16331_gshared/* 56*/,
	(methodPointerType)&SoomlaEntity_1__ctor_m16332_gshared/* 57*/,
	(methodPointerType)&SoomlaEntity_1__ctor_m16333_gshared/* 58*/,
	(methodPointerType)&SoomlaEntity_1_toJSONObject_m16335_gshared/* 59*/,
	(methodPointerType)&SoomlaEntity_1_Equals_m16336_gshared/* 60*/,
	(methodPointerType)&SoomlaEntity_1_Equals_m16338_gshared/* 61*/,
	(methodPointerType)&SoomlaEntity_1_GetHashCode_m16339_gshared/* 62*/,
	(methodPointerType)&SoomlaEntity_1_Clone_m16340_gshared/* 63*/,
	(methodPointerType)&SoomlaEntity_1_op_Equality_m16341_gshared/* 64*/,
	(methodPointerType)&SoomlaEntity_1_op_Inequality_m16343_gshared/* 65*/,
	(methodPointerType)&SoomlaExtensions_AddOrUpdate_TisObject_t_m940_gshared/* 66*/,
	(methodPointerType)&AsyncExec_runWithCallback_TisObject_t_TisObject_t_m26310_gshared/* 67*/,
	(methodPointerType)&Misc_CheckNotNull_TisObject_t_m3706_gshared/* 68*/,
	(methodPointerType)&Misc_CheckNotNull_TisObject_t_m26343_gshared/* 69*/,
	(methodPointerType)&CallbackUtils_ToOnGameThread_TisObject_t_m26345_gshared/* 70*/,
	(methodPointerType)&CallbackUtils_ToOnGameThread_TisObject_t_TisObject_t_m26347_gshared/* 71*/,
	(methodPointerType)&CallbackUtils_ToOnGameThread_TisObject_t_TisObject_t_TisObject_t_m26349_gshared/* 72*/,
	(methodPointerType)&CallbackUtils_U3CToOnGameThread_1U3Em__4_TisObject_t_m26344_gshared/* 73*/,
	(methodPointerType)&CallbackUtils_U3CToOnGameThread_2U3Em__6_TisObject_t_TisObject_t_m26346_gshared/* 74*/,
	(methodPointerType)&CallbackUtils_U3CToOnGameThread_3U3Em__8_TisObject_t_TisObject_t_TisObject_t_m26348_gshared/* 75*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey14_1__ctor_m19609_gshared/* 76*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey14_1_U3CU3Em__5_m19610_gshared/* 77*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey15_1__ctor_m19611_gshared/* 78*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey15_1_U3CU3Em__A_m19612_gshared/* 79*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19613_gshared/* 80*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19614_gshared/* 81*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19615_gshared/* 82*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19616_gshared/* 83*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19617_gshared/* 84*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19618_gshared/* 85*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19619_gshared/* 86*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19620_gshared/* 87*/,
	(methodPointerType)&NativeClient_AsOnGameThreadCallback_TisObject_t_m26362_gshared/* 88*/,
	(methodPointerType)&NativeClient_InvokeCallbackOnGameThread_TisObject_t_m26363_gshared/* 89*/,
	(methodPointerType)&NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisObject_t_m26361_gshared/* 90*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19730_gshared/* 91*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19731_gshared/* 92*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19732_gshared/* 93*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19733_gshared/* 94*/,
	(methodPointerType)&NativeRealtimeMultiplayerClient_WithDefault_TisObject_t_m26370_gshared/* 95*/,
	(methodPointerType)&NativeSavedGameClient_ToOnGameThread_TisObject_t_TisObject_t_m26388_gshared/* 96*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20332_gshared/* 97*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20333_gshared/* 98*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20334_gshared/* 99*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20335_gshared/* 100*/,
	(methodPointerType)&Callbacks_ToIntPtr_TisBaseReferenceHolder_t654_m3895_gshared/* 101*/,
	(methodPointerType)&Callbacks_ToIntPtr_TisObject_t_TisBaseReferenceHolder_t654_m26389_gshared/* 102*/,
	(methodPointerType)&Callbacks_IntPtrToTempCallback_TisObject_t_m3904_gshared/* 103*/,
	(methodPointerType)&Callbacks_IntPtrToCallback_TisObject_t_m26390_gshared/* 104*/,
	(methodPointerType)&Callbacks_IntPtrToPermanentCallback_TisObject_t_m3906_gshared/* 105*/,
	(methodPointerType)&Callbacks_PerformInternalCallback_TisObject_t_m26393_gshared/* 106*/,
	(methodPointerType)&Callbacks_AsOnGameThreadCallback_TisObject_t_m26394_gshared/* 107*/,
	(methodPointerType)&Callbacks_AsOnGameThreadCallback_TisObject_t_TisObject_t_m26395_gshared/* 108*/,
	(methodPointerType)&U3CToIntPtrU3Ec__AnonStorey5C_1__ctor_m20386_gshared/* 109*/,
	(methodPointerType)&U3CToIntPtrU3Ec__AnonStorey5C_1_U3CU3Em__73_m20387_gshared/* 110*/,
	(methodPointerType)&U3CToIntPtrU3Ec__AnonStorey5D_2__ctor_m20414_gshared/* 111*/,
	(methodPointerType)&U3CToIntPtrU3Ec__AnonStorey5D_2_U3CU3Em__74_m20415_gshared/* 112*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20416_gshared/* 113*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20417_gshared/* 114*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20418_gshared/* 115*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20419_gshared/* 116*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20420_gshared/* 117*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20421_gshared/* 118*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20422_gshared/* 119*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20423_gshared/* 120*/,
	(methodPointerType)&PInvokeUtilities_OutParamsToArray_TisObject_t_m26416_gshared/* 121*/,
	(methodPointerType)&PInvokeUtilities_ToEnumerable_TisObject_t_m3933_gshared/* 122*/,
	(methodPointerType)&PInvokeUtilities_ToEnumerator_TisObject_t_m3887_gshared/* 123*/,
	(methodPointerType)&PInvokeUtilities_ArrayToSizeT_TisObject_t_m26417_gshared/* 124*/,
	(methodPointerType)&OutMethod_1__ctor_m20673_gshared/* 125*/,
	(methodPointerType)&OutMethod_1_Invoke_m20674_gshared/* 126*/,
	(methodPointerType)&OutMethod_1_BeginInvoke_m20675_gshared/* 127*/,
	(methodPointerType)&OutMethod_1_EndInvoke_m20676_gshared/* 128*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m20399_gshared/* 129*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20400_gshared/* 130*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1__ctor_m20398_gshared/* 131*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m20401_gshared/* 132*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20402_gshared/* 133*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_MoveNext_m20403_gshared/* 134*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_Dispose_m20404_gshared/* 135*/,
	(methodPointerType)&U3CToEnumerableU3Ec__Iterator0_1_Reset_m20405_gshared/* 136*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m5747_gshared/* 137*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m5734_gshared/* 138*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m5804_gshared/* 139*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m26493_gshared/* 140*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m26489_gshared/* 141*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m26518_gshared/* 142*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m5782_gshared/* 143*/,
	(methodPointerType)&EventFunction_1__ctor_m21548_gshared/* 144*/,
	(methodPointerType)&EventFunction_1_Invoke_m21550_gshared/* 145*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m21552_gshared/* 146*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m21554_gshared/* 147*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m5913_gshared/* 148*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m6151_gshared/* 149*/,
	(methodPointerType)&IndexedSet_1_get_Count_m22570_gshared/* 150*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m22572_gshared/* 151*/,
	(methodPointerType)&IndexedSet_1_get_Item_m22580_gshared/* 152*/,
	(methodPointerType)&IndexedSet_1_set_Item_m22582_gshared/* 153*/,
	(methodPointerType)&IndexedSet_1__ctor_m22554_gshared/* 154*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556_gshared/* 155*/,
	(methodPointerType)&IndexedSet_1_Add_m22558_gshared/* 156*/,
	(methodPointerType)&IndexedSet_1_Remove_m22560_gshared/* 157*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m22562_gshared/* 158*/,
	(methodPointerType)&IndexedSet_1_Clear_m22564_gshared/* 159*/,
	(methodPointerType)&IndexedSet_1_Contains_m22566_gshared/* 160*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m22568_gshared/* 161*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m22574_gshared/* 162*/,
	(methodPointerType)&IndexedSet_1_Insert_m22576_gshared/* 163*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m22578_gshared/* 164*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m22583_gshared/* 165*/,
	(methodPointerType)&IndexedSet_1_Sort_m22584_gshared/* 166*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m21657_gshared/* 167*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m21659_gshared/* 168*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m21661_gshared/* 169*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m21663_gshared/* 170*/,
	(methodPointerType)&ObjectPool_1__ctor_m21655_gshared/* 171*/,
	(methodPointerType)&ObjectPool_1_Get_m21665_gshared/* 172*/,
	(methodPointerType)&ObjectPool_1_Release_m21667_gshared/* 173*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisScriptableObject_t15_m846_gshared/* 174*/,
	(methodPointerType)&Object_Instantiate_TisObject_t187_m4176_gshared/* 175*/,
	(methodPointerType)&Object_FindObjectOfType_TisObject_t187_m962_gshared/* 176*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m4012_gshared/* 177*/,
	(methodPointerType)&Component_GetComponentInChildren_TisObject_t_m4044_gshared/* 178*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m26612_gshared/* 179*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m6164_gshared/* 180*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m5732_gshared/* 181*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m4017_gshared/* 182*/,
	(methodPointerType)&GameObject_GetComponentInChildren_TisObject_t_m4210_gshared/* 183*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m26492_gshared/* 184*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m26613_gshared/* 185*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m5864_gshared/* 186*/,
	(methodPointerType)&GameObject_AddComponent_TisComponent_t230_m1035_gshared/* 187*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m26517_gshared/* 188*/,
	(methodPointerType)&InvokableCall_1__ctor_m22161_gshared/* 189*/,
	(methodPointerType)&InvokableCall_1__ctor_m22162_gshared/* 190*/,
	(methodPointerType)&InvokableCall_1_Invoke_m22163_gshared/* 191*/,
	(methodPointerType)&InvokableCall_1_Find_m22164_gshared/* 192*/,
	(methodPointerType)&InvokableCall_2__ctor_m25276_gshared/* 193*/,
	(methodPointerType)&InvokableCall_2_Invoke_m25277_gshared/* 194*/,
	(methodPointerType)&InvokableCall_2_Find_m25278_gshared/* 195*/,
	(methodPointerType)&InvokableCall_3__ctor_m25283_gshared/* 196*/,
	(methodPointerType)&InvokableCall_3_Invoke_m25284_gshared/* 197*/,
	(methodPointerType)&InvokableCall_3_Find_m25285_gshared/* 198*/,
	(methodPointerType)&InvokableCall_4__ctor_m25290_gshared/* 199*/,
	(methodPointerType)&InvokableCall_4_Invoke_m25291_gshared/* 200*/,
	(methodPointerType)&InvokableCall_4_Find_m25292_gshared/* 201*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m25297_gshared/* 202*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m25298_gshared/* 203*/,
	(methodPointerType)&UnityEvent_1__ctor_m22151_gshared/* 204*/,
	(methodPointerType)&UnityEvent_1_AddListener_m22153_gshared/* 205*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m22155_gshared/* 206*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m22156_gshared/* 207*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m22157_gshared/* 208*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m22159_gshared/* 209*/,
	(methodPointerType)&UnityEvent_1_Invoke_m22160_gshared/* 210*/,
	(methodPointerType)&UnityEvent_2__ctor_m25513_gshared/* 211*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m25514_gshared/* 212*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m25515_gshared/* 213*/,
	(methodPointerType)&UnityEvent_3__ctor_m25516_gshared/* 214*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m25517_gshared/* 215*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m25518_gshared/* 216*/,
	(methodPointerType)&UnityEvent_4__ctor_m25519_gshared/* 217*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m25520_gshared/* 218*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m25521_gshared/* 219*/,
	(methodPointerType)&UnityAction_1__ctor_m21684_gshared/* 220*/,
	(methodPointerType)&UnityAction_1_Invoke_m21685_gshared/* 221*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m21686_gshared/* 222*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m21687_gshared/* 223*/,
	(methodPointerType)&UnityAction_2__ctor_m25279_gshared/* 224*/,
	(methodPointerType)&UnityAction_2_Invoke_m25280_gshared/* 225*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m25281_gshared/* 226*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m25282_gshared/* 227*/,
	(methodPointerType)&UnityAction_3__ctor_m25286_gshared/* 228*/,
	(methodPointerType)&UnityAction_3_Invoke_m25287_gshared/* 229*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m25288_gshared/* 230*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m25289_gshared/* 231*/,
	(methodPointerType)&UnityAction_4__ctor_m25293_gshared/* 232*/,
	(methodPointerType)&UnityAction_4_Invoke_m25294_gshared/* 233*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m25295_gshared/* 234*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m25296_gshared/* 235*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17609_gshared/* 236*/,
	(methodPointerType)&HashSet_1_get_Count_m17617_gshared/* 237*/,
	(methodPointerType)&HashSet_1__ctor_m17601_gshared/* 238*/,
	(methodPointerType)&HashSet_1__ctor_m17603_gshared/* 239*/,
	(methodPointerType)&HashSet_1__ctor_m17605_gshared/* 240*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17607_gshared/* 241*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m17611_gshared/* 242*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m17613_gshared/* 243*/,
	(methodPointerType)&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m17615_gshared/* 244*/,
	(methodPointerType)&HashSet_1_Init_m17619_gshared/* 245*/,
	(methodPointerType)&HashSet_1_InitArrays_m17621_gshared/* 246*/,
	(methodPointerType)&HashSet_1_SlotsContainsAt_m17623_gshared/* 247*/,
	(methodPointerType)&HashSet_1_CopyTo_m17625_gshared/* 248*/,
	(methodPointerType)&HashSet_1_CopyTo_m17627_gshared/* 249*/,
	(methodPointerType)&HashSet_1_Resize_m17629_gshared/* 250*/,
	(methodPointerType)&HashSet_1_GetLinkHashCode_m17631_gshared/* 251*/,
	(methodPointerType)&HashSet_1_GetItemHashCode_m17633_gshared/* 252*/,
	(methodPointerType)&HashSet_1_Add_m17634_gshared/* 253*/,
	(methodPointerType)&HashSet_1_Clear_m17636_gshared/* 254*/,
	(methodPointerType)&HashSet_1_Contains_m17638_gshared/* 255*/,
	(methodPointerType)&HashSet_1_Remove_m17640_gshared/* 256*/,
	(methodPointerType)&HashSet_1_GetObjectData_m17642_gshared/* 257*/,
	(methodPointerType)&HashSet_1_OnDeserialization_m17644_gshared/* 258*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17651_gshared/* 259*/,
	(methodPointerType)&Enumerator_get_Current_m17653_gshared/* 260*/,
	(methodPointerType)&Enumerator__ctor_m17650_gshared/* 261*/,
	(methodPointerType)&Enumerator_MoveNext_m17652_gshared/* 262*/,
	(methodPointerType)&Enumerator_Dispose_m17654_gshared/* 263*/,
	(methodPointerType)&Enumerator_CheckState_m17655_gshared/* 264*/,
	(methodPointerType)&PrimeHelper__cctor_m17656_gshared/* 265*/,
	(methodPointerType)&PrimeHelper_TestPrime_m17657_gshared/* 266*/,
	(methodPointerType)&PrimeHelper_CalcPrime_m17658_gshared/* 267*/,
	(methodPointerType)&PrimeHelper_ToPrime_m17659_gshared/* 268*/,
	(methodPointerType)&Enumerable_Cast_TisObject_t_m3761_gshared/* 269*/,
	(methodPointerType)&Enumerable_CreateCastIterator_TisObject_t_m26368_gshared/* 270*/,
	(methodPointerType)&Enumerable_Contains_TisObject_t_m26387_gshared/* 271*/,
	(methodPointerType)&Enumerable_Count_TisObject_t_m1002_gshared/* 272*/,
	(methodPointerType)&Enumerable_Except_TisObject_t_m3818_gshared/* 273*/,
	(methodPointerType)&Enumerable_Except_TisObject_t_m26385_gshared/* 274*/,
	(methodPointerType)&Enumerable_CreateExceptIterator_TisObject_t_m26386_gshared/* 275*/,
	(methodPointerType)&Enumerable_First_TisObject_t_m26309_gshared/* 276*/,
	(methodPointerType)&Enumerable_FirstOrDefault_TisObject_t_m997_gshared/* 277*/,
	(methodPointerType)&Enumerable_LongCount_TisObject_t_m3977_gshared/* 278*/,
	(methodPointerType)&Enumerable_Select_TisObject_t_TisObject_t_m3696_gshared/* 279*/,
	(methodPointerType)&Enumerable_CreateSelectIterator_TisObject_t_TisObject_t_m26342_gshared/* 280*/,
	(methodPointerType)&Enumerable_ToArray_TisObject_t_m3698_gshared/* 281*/,
	(methodPointerType)&Enumerable_ToDictionary_TisObject_t_TisObject_t_TisObject_t_m26372_gshared/* 282*/,
	(methodPointerType)&Enumerable_ToDictionary_TisObject_t_TisObject_t_m3801_gshared/* 283*/,
	(methodPointerType)&Enumerable_ToDictionary_TisObject_t_TisObject_t_m26371_gshared/* 284*/,
	(methodPointerType)&Enumerable_ToList_TisObject_t_m3763_gshared/* 285*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m3805_gshared/* 286*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m26373_gshared/* 287*/,
	(methodPointerType)&Function_1__cctor_m20115_gshared/* 288*/,
	(methodPointerType)&Function_1_U3CIdentityU3Em__77_m20116_gshared/* 289*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19873_gshared/* 290*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19874_gshared/* 291*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19872_gshared/* 292*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19875_gshared/* 293*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19876_gshared/* 294*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19877_gshared/* 295*/,
	(methodPointerType)&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19878_gshared/* 296*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20289_gshared/* 297*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m20290_gshared/* 298*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m20288_gshared/* 299*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m20291_gshared/* 300*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20292_gshared/* 301*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m20293_gshared/* 302*/,
	(methodPointerType)&U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m20294_gshared/* 303*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19204_gshared/* 304*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m19205_gshared/* 305*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m19203_gshared/* 306*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m19206_gshared/* 307*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19207_gshared/* 308*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m19208_gshared/* 309*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m19209_gshared/* 310*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20118_gshared/* 311*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20119_gshared/* 312*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20117_gshared/* 313*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20120_gshared/* 314*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20121_gshared/* 315*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20122_gshared/* 316*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20123_gshared/* 317*/,
	(methodPointerType)&Action_2__ctor_m16118_gshared/* 318*/,
	(methodPointerType)&Action_2_Invoke_m16120_gshared/* 319*/,
	(methodPointerType)&Action_2_BeginInvoke_m16122_gshared/* 320*/,
	(methodPointerType)&Action_2_EndInvoke_m16124_gshared/* 321*/,
	(methodPointerType)&Action_3__ctor_m17301_gshared/* 322*/,
	(methodPointerType)&Action_3_Invoke_m17303_gshared/* 323*/,
	(methodPointerType)&Action_3_BeginInvoke_m17305_gshared/* 324*/,
	(methodPointerType)&Action_3_EndInvoke_m17307_gshared/* 325*/,
	(methodPointerType)&Action_4__ctor_m25522_gshared/* 326*/,
	(methodPointerType)&Action_4_Invoke_m25523_gshared/* 327*/,
	(methodPointerType)&Action_4_BeginInvoke_m25524_gshared/* 328*/,
	(methodPointerType)&Action_4_EndInvoke_m25525_gshared/* 329*/,
	(methodPointerType)&Func_2__ctor_m19196_gshared/* 330*/,
	(methodPointerType)&Func_2_Invoke_m19198_gshared/* 331*/,
	(methodPointerType)&Func_2_BeginInvoke_m19200_gshared/* 332*/,
	(methodPointerType)&Func_2_EndInvoke_m19202_gshared/* 333*/,
	(methodPointerType)&Func_3__ctor_m18532_gshared/* 334*/,
	(methodPointerType)&Func_3_Invoke_m18534_gshared/* 335*/,
	(methodPointerType)&Func_3_BeginInvoke_m18536_gshared/* 336*/,
	(methodPointerType)&Func_3_EndInvoke_m18538_gshared/* 337*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669_gshared/* 338*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m21670_gshared/* 339*/,
	(methodPointerType)&Stack_1_get_Count_m21677_gshared/* 340*/,
	(methodPointerType)&Stack_1__ctor_m21668_gshared/* 341*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m21671_gshared/* 342*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672_gshared/* 343*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673_gshared/* 344*/,
	(methodPointerType)&Stack_1_Peek_m21674_gshared/* 345*/,
	(methodPointerType)&Stack_1_Pop_m21675_gshared/* 346*/,
	(methodPointerType)&Stack_1_Push_m21676_gshared/* 347*/,
	(methodPointerType)&Stack_1_GetEnumerator_m21678_gshared/* 348*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21680_gshared/* 349*/,
	(methodPointerType)&Enumerator_get_Current_m21683_gshared/* 350*/,
	(methodPointerType)&Enumerator__ctor_m21679_gshared/* 351*/,
	(methodPointerType)&Enumerator_Dispose_m21681_gshared/* 352*/,
	(methodPointerType)&Enumerator_MoveNext_m21682_gshared/* 353*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m26190_gshared/* 354*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m26182_gshared/* 355*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m26185_gshared/* 356*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m26183_gshared/* 357*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m26184_gshared/* 358*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m26187_gshared/* 359*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m26186_gshared/* 360*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m26181_gshared/* 361*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m26189_gshared/* 362*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m26196_gshared/* 363*/,
	(methodPointerType)&Array_Sort_TisObject_t_m26803_gshared/* 364*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m26804_gshared/* 365*/,
	(methodPointerType)&Array_Sort_TisObject_t_m26805_gshared/* 366*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m26806_gshared/* 367*/,
	(methodPointerType)&Array_Sort_TisObject_t_m14419_gshared/* 368*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m26807_gshared/* 369*/,
	(methodPointerType)&Array_Sort_TisObject_t_m26195_gshared/* 370*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m26194_gshared/* 371*/,
	(methodPointerType)&Array_Sort_TisObject_t_m26808_gshared/* 372*/,
	(methodPointerType)&Array_Sort_TisObject_t_m26234_gshared/* 373*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m26197_gshared/* 374*/,
	(methodPointerType)&Array_compare_TisObject_t_m26231_gshared/* 375*/,
	(methodPointerType)&Array_qsort_TisObject_t_m26233_gshared/* 376*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m26232_gshared/* 377*/,
	(methodPointerType)&Array_swap_TisObject_t_m26235_gshared/* 378*/,
	(methodPointerType)&Array_Resize_TisObject_t_m26193_gshared/* 379*/,
	(methodPointerType)&Array_Resize_TisObject_t_m26192_gshared/* 380*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m26809_gshared/* 381*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m26810_gshared/* 382*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m26811_gshared/* 383*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m26813_gshared/* 384*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m26814_gshared/* 385*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m26812_gshared/* 386*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m26816_gshared/* 387*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m26817_gshared/* 388*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m26815_gshared/* 389*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m26819_gshared/* 390*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m26820_gshared/* 391*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m26821_gshared/* 392*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m26818_gshared/* 393*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14421_gshared/* 394*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m26822_gshared/* 395*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m14418_gshared/* 396*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m26824_gshared/* 397*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m26823_gshared/* 398*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m26825_gshared/* 399*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m26826_gshared/* 400*/,
	(methodPointerType)&Array_Exists_TisObject_t_m26827_gshared/* 401*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m26828_gshared/* 402*/,
	(methodPointerType)&Array_Find_TisObject_t_m26829_gshared/* 403*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m26830_gshared/* 404*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared/* 405*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15307_gshared/* 406*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15303_gshared/* 407*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15305_gshared/* 408*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15306_gshared/* 409*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m25841_gshared/* 410*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m25842_gshared/* 411*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m25843_gshared/* 412*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m25844_gshared/* 413*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m25839_gshared/* 414*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m25840_gshared/* 415*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m25845_gshared/* 416*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m25846_gshared/* 417*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m25847_gshared/* 418*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m25848_gshared/* 419*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m25849_gshared/* 420*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m25850_gshared/* 421*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m25851_gshared/* 422*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m25852_gshared/* 423*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m25853_gshared/* 424*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m25854_gshared/* 425*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m25856_gshared/* 426*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m25857_gshared/* 427*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m25855_gshared/* 428*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m25858_gshared/* 429*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m25859_gshared/* 430*/,
	(methodPointerType)&Comparer_1_get_Default_m15518_gshared/* 431*/,
	(methodPointerType)&Comparer_1__ctor_m15515_gshared/* 432*/,
	(methodPointerType)&Comparer_1__cctor_m15516_gshared/* 433*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15517_gshared/* 434*/,
	(methodPointerType)&DefaultComparer__ctor_m15519_gshared/* 435*/,
	(methodPointerType)&DefaultComparer_Compare_m15520_gshared/* 436*/,
	(methodPointerType)&GenericComparer_1__ctor_m25895_gshared/* 437*/,
	(methodPointerType)&GenericComparer_1_Compare_m25896_gshared/* 438*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared/* 439*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared/* 440*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared/* 441*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared/* 442*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared/* 443*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared/* 444*/,
	(methodPointerType)&Dictionary_2_get_Count_m16251_gshared/* 445*/,
	(methodPointerType)&Dictionary_2_get_Item_m16253_gshared/* 446*/,
	(methodPointerType)&Dictionary_2_set_Item_m16255_gshared/* 447*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16289_gshared/* 448*/,
	(methodPointerType)&Dictionary_2_get_Values_m16291_gshared/* 449*/,
	(methodPointerType)&Dictionary_2__ctor_m16211_gshared/* 450*/,
	(methodPointerType)&Dictionary_2__ctor_m16213_gshared/* 451*/,
	(methodPointerType)&Dictionary_2__ctor_m16215_gshared/* 452*/,
	(methodPointerType)&Dictionary_2__ctor_m16217_gshared/* 453*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared/* 454*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared/* 455*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared/* 456*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared/* 457*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared/* 458*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared/* 459*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared/* 460*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared/* 461*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared/* 462*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared/* 463*/,
	(methodPointerType)&Dictionary_2_Init_m16257_gshared/* 464*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16259_gshared/* 465*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16261_gshared/* 466*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26249_gshared/* 467*/,
	(methodPointerType)&Dictionary_2_make_pair_m16263_gshared/* 468*/,
	(methodPointerType)&Dictionary_2_pick_key_m16265_gshared/* 469*/,
	(methodPointerType)&Dictionary_2_pick_value_m16267_gshared/* 470*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16269_gshared/* 471*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26250_gshared/* 472*/,
	(methodPointerType)&Dictionary_2_Resize_m16271_gshared/* 473*/,
	(methodPointerType)&Dictionary_2_Add_m16273_gshared/* 474*/,
	(methodPointerType)&Dictionary_2_Clear_m16275_gshared/* 475*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16277_gshared/* 476*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16279_gshared/* 477*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16281_gshared/* 478*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16283_gshared/* 479*/,
	(methodPointerType)&Dictionary_2_Remove_m16285_gshared/* 480*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16287_gshared/* 481*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16293_gshared/* 482*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16295_gshared/* 483*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16297_gshared/* 484*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16298_gshared/* 485*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16300_gshared/* 486*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16326_gshared/* 487*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16327_gshared/* 488*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16328_gshared/* 489*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16329_gshared/* 490*/,
	(methodPointerType)&ShimEnumerator__ctor_m16324_gshared/* 491*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16325_gshared/* 492*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared/* 493*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared/* 494*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared/* 495*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared/* 496*/,
	(methodPointerType)&Enumerator_get_Current_m16200_gshared/* 497*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16202_gshared/* 498*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16204_gshared/* 499*/,
	(methodPointerType)&Enumerator__ctor_m16190_gshared/* 500*/,
	(methodPointerType)&Enumerator_MoveNext_m16199_gshared/* 501*/,
	(methodPointerType)&Enumerator_VerifyState_m16206_gshared/* 502*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16208_gshared/* 503*/,
	(methodPointerType)&Enumerator_Dispose_m16210_gshared/* 504*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16150_gshared/* 505*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16152_gshared/* 506*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16154_gshared/* 507*/,
	(methodPointerType)&KeyCollection_get_Count_m16160_gshared/* 508*/,
	(methodPointerType)&KeyCollection__ctor_m16134_gshared/* 509*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16136_gshared/* 510*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16138_gshared/* 511*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16140_gshared/* 512*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16142_gshared/* 513*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16144_gshared/* 514*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16146_gshared/* 515*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16148_gshared/* 516*/,
	(methodPointerType)&KeyCollection_CopyTo_m16156_gshared/* 517*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16158_gshared/* 518*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16302_gshared/* 519*/,
	(methodPointerType)&Enumerator_get_Current_m16305_gshared/* 520*/,
	(methodPointerType)&Enumerator__ctor_m16301_gshared/* 521*/,
	(methodPointerType)&Enumerator_Dispose_m16303_gshared/* 522*/,
	(methodPointerType)&Enumerator_MoveNext_m16304_gshared/* 523*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared/* 524*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared/* 525*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared/* 526*/,
	(methodPointerType)&ValueCollection_get_Count_m16188_gshared/* 527*/,
	(methodPointerType)&ValueCollection__ctor_m16162_gshared/* 528*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared/* 529*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared/* 530*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared/* 531*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared/* 532*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared/* 533*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared/* 534*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared/* 535*/,
	(methodPointerType)&ValueCollection_CopyTo_m16184_gshared/* 536*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16186_gshared/* 537*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared/* 538*/,
	(methodPointerType)&Enumerator_get_Current_m16314_gshared/* 539*/,
	(methodPointerType)&Enumerator__ctor_m16310_gshared/* 540*/,
	(methodPointerType)&Enumerator_Dispose_m16312_gshared/* 541*/,
	(methodPointerType)&Enumerator_MoveNext_m16313_gshared/* 542*/,
	(methodPointerType)&Transform_1__ctor_m16306_gshared/* 543*/,
	(methodPointerType)&Transform_1_Invoke_m16307_gshared/* 544*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16308_gshared/* 545*/,
	(methodPointerType)&Transform_1_EndInvoke_m16309_gshared/* 546*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15498_gshared/* 547*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15494_gshared/* 548*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15495_gshared/* 549*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15496_gshared/* 550*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15497_gshared/* 551*/,
	(methodPointerType)&DefaultComparer__ctor_m15504_gshared/* 552*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15505_gshared/* 553*/,
	(methodPointerType)&DefaultComparer_Equals_m15506_gshared/* 554*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25897_gshared/* 555*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25898_gshared/* 556*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25899_gshared/* 557*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m15309_gshared/* 558*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m15310_gshared/* 559*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m15311_gshared/* 560*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m15312_gshared/* 561*/,
	(methodPointerType)&KeyValuePair_2__ctor_m15308_gshared/* 562*/,
	(methodPointerType)&KeyValuePair_2_ToString_m15313_gshared/* 563*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared/* 564*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared/* 565*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared/* 566*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared/* 567*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared/* 568*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15353_gshared/* 569*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15355_gshared/* 570*/,
	(methodPointerType)&List_1_get_Capacity_m15413_gshared/* 571*/,
	(methodPointerType)&List_1_set_Capacity_m15415_gshared/* 572*/,
	(methodPointerType)&List_1_get_Count_m15417_gshared/* 573*/,
	(methodPointerType)&List_1_get_Item_m15419_gshared/* 574*/,
	(methodPointerType)&List_1_set_Item_m15421_gshared/* 575*/,
	(methodPointerType)&List_1__ctor_m1042_gshared/* 576*/,
	(methodPointerType)&List_1__ctor_m15321_gshared/* 577*/,
	(methodPointerType)&List_1__ctor_m15323_gshared/* 578*/,
	(methodPointerType)&List_1__cctor_m15325_gshared/* 579*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared/* 580*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15329_gshared/* 581*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared/* 582*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15333_gshared/* 583*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15335_gshared/* 584*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15337_gshared/* 585*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15339_gshared/* 586*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15341_gshared/* 587*/,
	(methodPointerType)&List_1_Add_m15357_gshared/* 588*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15359_gshared/* 589*/,
	(methodPointerType)&List_1_AddCollection_m15361_gshared/* 590*/,
	(methodPointerType)&List_1_AddEnumerable_m15363_gshared/* 591*/,
	(methodPointerType)&List_1_AddRange_m15365_gshared/* 592*/,
	(methodPointerType)&List_1_AsReadOnly_m15367_gshared/* 593*/,
	(methodPointerType)&List_1_Clear_m15369_gshared/* 594*/,
	(methodPointerType)&List_1_Contains_m15371_gshared/* 595*/,
	(methodPointerType)&List_1_ConvertAll_TisObject_t_m837_gshared/* 596*/,
	(methodPointerType)&List_1_CopyTo_m15373_gshared/* 597*/,
	(methodPointerType)&List_1_Find_m15375_gshared/* 598*/,
	(methodPointerType)&List_1_CheckMatch_m15377_gshared/* 599*/,
	(methodPointerType)&List_1_FindIndex_m15379_gshared/* 600*/,
	(methodPointerType)&List_1_GetIndex_m15381_gshared/* 601*/,
	(methodPointerType)&List_1_ForEach_m15383_gshared/* 602*/,
	(methodPointerType)&List_1_GetEnumerator_m15385_gshared/* 603*/,
	(methodPointerType)&List_1_IndexOf_m15387_gshared/* 604*/,
	(methodPointerType)&List_1_Shift_m15389_gshared/* 605*/,
	(methodPointerType)&List_1_CheckIndex_m15391_gshared/* 606*/,
	(methodPointerType)&List_1_Insert_m15393_gshared/* 607*/,
	(methodPointerType)&List_1_CheckCollection_m15395_gshared/* 608*/,
	(methodPointerType)&List_1_Remove_m15397_gshared/* 609*/,
	(methodPointerType)&List_1_RemoveAll_m15399_gshared/* 610*/,
	(methodPointerType)&List_1_RemoveAt_m15401_gshared/* 611*/,
	(methodPointerType)&List_1_Reverse_m15403_gshared/* 612*/,
	(methodPointerType)&List_1_Sort_m15405_gshared/* 613*/,
	(methodPointerType)&List_1_Sort_m15407_gshared/* 614*/,
	(methodPointerType)&List_1_ToArray_m15409_gshared/* 615*/,
	(methodPointerType)&List_1_TrimExcess_m15411_gshared/* 616*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared/* 617*/,
	(methodPointerType)&Enumerator_get_Current_m15427_gshared/* 618*/,
	(methodPointerType)&Enumerator__ctor_m15422_gshared/* 619*/,
	(methodPointerType)&Enumerator_Dispose_m15424_gshared/* 620*/,
	(methodPointerType)&Enumerator_VerifyState_m15425_gshared/* 621*/,
	(methodPointerType)&Enumerator_MoveNext_m15426_gshared/* 622*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15459_gshared/* 623*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15467_gshared/* 624*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15468_gshared/* 625*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15469_gshared/* 626*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15470_gshared/* 627*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15471_gshared/* 628*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15472_gshared/* 629*/,
	(methodPointerType)&Collection_1_get_Count_m15485_gshared/* 630*/,
	(methodPointerType)&Collection_1_get_Item_m15486_gshared/* 631*/,
	(methodPointerType)&Collection_1_set_Item_m15487_gshared/* 632*/,
	(methodPointerType)&Collection_1__ctor_m15458_gshared/* 633*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15460_gshared/* 634*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15461_gshared/* 635*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15462_gshared/* 636*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15463_gshared/* 637*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15464_gshared/* 638*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15465_gshared/* 639*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15466_gshared/* 640*/,
	(methodPointerType)&Collection_1_Add_m15473_gshared/* 641*/,
	(methodPointerType)&Collection_1_Clear_m15474_gshared/* 642*/,
	(methodPointerType)&Collection_1_ClearItems_m15475_gshared/* 643*/,
	(methodPointerType)&Collection_1_Contains_m15476_gshared/* 644*/,
	(methodPointerType)&Collection_1_CopyTo_m15477_gshared/* 645*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15478_gshared/* 646*/,
	(methodPointerType)&Collection_1_IndexOf_m15479_gshared/* 647*/,
	(methodPointerType)&Collection_1_Insert_m15480_gshared/* 648*/,
	(methodPointerType)&Collection_1_InsertItem_m15481_gshared/* 649*/,
	(methodPointerType)&Collection_1_Remove_m15482_gshared/* 650*/,
	(methodPointerType)&Collection_1_RemoveAt_m15483_gshared/* 651*/,
	(methodPointerType)&Collection_1_RemoveItem_m15484_gshared/* 652*/,
	(methodPointerType)&Collection_1_SetItem_m15488_gshared/* 653*/,
	(methodPointerType)&Collection_1_IsValidItem_m15489_gshared/* 654*/,
	(methodPointerType)&Collection_1_ConvertItem_m15490_gshared/* 655*/,
	(methodPointerType)&Collection_1_CheckWritable_m15491_gshared/* 656*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15492_gshared/* 657*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15493_gshared/* 658*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared/* 659*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared/* 660*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared/* 661*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared/* 662*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared/* 663*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared/* 664*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared/* 665*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared/* 666*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared/* 667*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15456_gshared/* 668*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15457_gshared/* 669*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15428_gshared/* 670*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared/* 671*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared/* 672*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared/* 673*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared/* 674*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared/* 675*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared/* 676*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared/* 677*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared/* 678*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared/* 679*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared/* 680*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared/* 681*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared/* 682*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared/* 683*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared/* 684*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15452_gshared/* 685*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15453_gshared/* 686*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15454_gshared/* 687*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15455_gshared/* 688*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m26864_gshared/* 689*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m26865_gshared/* 690*/,
	(methodPointerType)&Getter_2__ctor_m25955_gshared/* 691*/,
	(methodPointerType)&Getter_2_Invoke_m25956_gshared/* 692*/,
	(methodPointerType)&Getter_2_BeginInvoke_m25957_gshared/* 693*/,
	(methodPointerType)&Getter_2_EndInvoke_m25958_gshared/* 694*/,
	(methodPointerType)&StaticGetter_1__ctor_m25959_gshared/* 695*/,
	(methodPointerType)&StaticGetter_1_Invoke_m25960_gshared/* 696*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m25961_gshared/* 697*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m25962_gshared/* 698*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m26490_gshared/* 699*/,
	(methodPointerType)&Action_1__ctor_m15511_gshared/* 700*/,
	(methodPointerType)&Action_1_Invoke_m15512_gshared/* 701*/,
	(methodPointerType)&Action_1_BeginInvoke_m15513_gshared/* 702*/,
	(methodPointerType)&Action_1_EndInvoke_m15514_gshared/* 703*/,
	(methodPointerType)&Comparison_1__ctor_m15541_gshared/* 704*/,
	(methodPointerType)&Comparison_1_Invoke_m15542_gshared/* 705*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15543_gshared/* 706*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15544_gshared/* 707*/,
	(methodPointerType)&Converter_2__ctor_m15602_gshared/* 708*/,
	(methodPointerType)&Converter_2_Invoke_m15604_gshared/* 709*/,
	(methodPointerType)&Converter_2_BeginInvoke_m15606_gshared/* 710*/,
	(methodPointerType)&Converter_2_EndInvoke_m15608_gshared/* 711*/,
	(methodPointerType)&Predicate_1__ctor_m15507_gshared/* 712*/,
	(methodPointerType)&Predicate_1_Invoke_m15508_gshared/* 713*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15509_gshared/* 714*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15510_gshared/* 715*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t194_m881_gshared/* 716*/,
	(methodPointerType)&Action_3__ctor_m17250_gshared/* 717*/,
	(methodPointerType)&Action_1__ctor_m17260_gshared/* 718*/,
	(methodPointerType)&Dictionary_2__ctor_m17007_gshared/* 719*/,
	(methodPointerType)&Dictionary_2__ctor_m17004_gshared/* 720*/,
	(methodPointerType)&Func_2__ctor_m18520_gshared/* 721*/,
	(methodPointerType)&Dictionary_2__ctor_m18861_gshared/* 722*/,
	(methodPointerType)&Nullable_1__ctor_m3713_gshared/* 723*/,
	(methodPointerType)&Nullable_1_get_HasValue_m3714_gshared/* 724*/,
	(methodPointerType)&Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_gshared/* 725*/,
	(methodPointerType)&NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741_gshared/* 726*/,
	(methodPointerType)&Action_3__ctor_m19345_gshared/* 727*/,
	(methodPointerType)&NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743_gshared/* 728*/,
	(methodPointerType)&Action_1__ctor_m18539_gshared/* 729*/,
	(methodPointerType)&NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745_gshared/* 730*/,
	(methodPointerType)&Action_2__ctor_m19626_gshared/* 731*/,
	(methodPointerType)&Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared/* 732*/,
	(methodPointerType)&CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared/* 733*/,
	(methodPointerType)&CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared/* 734*/,
	(methodPointerType)&HashSet_1__ctor_m20229_gshared/* 735*/,
	(methodPointerType)&HashSet_1_Add_m20262_gshared/* 736*/,
	(methodPointerType)&Action_4__ctor_m19898_gshared/* 737*/,
	(methodPointerType)&Action_2__ctor_m18648_gshared/* 738*/,
	(methodPointerType)&NativeSavedGameClient_ToOnGameThread_TisInt32_t189_TisObject_t_m3856_gshared/* 739*/,
	(methodPointerType)&Nullable_1_get_Value_m3870_gshared/* 740*/,
	(methodPointerType)&Callbacks_AsOnGameThreadCallback_TisByte_t237_TisObject_t_m3874_gshared/* 741*/,
	(methodPointerType)&Callbacks_AsOnGameThreadCallback_TisByte_t237_m3877_gshared/* 742*/,
	(methodPointerType)&Func_2__ctor_m20391_gshared/* 743*/,
	(methodPointerType)&Func_2__ctor_m20368_gshared/* 744*/,
	(methodPointerType)&OutMethod_1__ctor_m3916_gshared/* 745*/,
	(methodPointerType)&PInvokeUtilities_OutParamsToArray_TisIntPtr_t_m3912_gshared/* 746*/,
	(methodPointerType)&Enumerable_Select_TisIntPtr_t_TisObject_t_m3914_gshared/* 747*/,
	(methodPointerType)&Dictionary_2__ctor_m20443_gshared/* 748*/,
	(methodPointerType)&Nullable_1__ctor_m3928_gshared/* 749*/,
	(methodPointerType)&OutMethod_1__ctor_m3930_gshared/* 750*/,
	(methodPointerType)&PInvokeUtilities_OutParamsToArray_TisByte_t237_m3929_gshared/* 751*/,
	(methodPointerType)&Action_1__ctor_m3963_gshared/* 752*/,
	(methodPointerType)&PInvokeUtilities_ArrayToSizeT_TisByte_t237_m3971_gshared/* 753*/,
	(methodPointerType)&Func_2__ctor_m20701_gshared/* 754*/,
	(methodPointerType)&Enumerable_Select_TisObject_t_TisIntPtr_t_m3974_gshared/* 755*/,
	(methodPointerType)&Enumerable_ToArray_TisIntPtr_t_m3975_gshared/* 756*/,
	(methodPointerType)&UnityAction_1__ctor_m20906_gshared/* 757*/,
	(methodPointerType)&UnityEvent_1_AddListener_m20914_gshared/* 758*/,
	(methodPointerType)&List_1__ctor_m4240_gshared/* 759*/,
	(methodPointerType)&List_1__ctor_m4257_gshared/* 760*/,
	(methodPointerType)&Comparison_1__ctor_m5737_gshared/* 761*/,
	(methodPointerType)&List_1_Sort_m5741_gshared/* 762*/,
	(methodPointerType)&List_1__ctor_m5776_gshared/* 763*/,
	(methodPointerType)&Dictionary_2__ctor_m22214_gshared/* 764*/,
	(methodPointerType)&Dictionary_2_get_Values_m22293_gshared/* 765*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m22365_gshared/* 766*/,
	(methodPointerType)&Enumerator_get_Current_m22371_gshared/* 767*/,
	(methodPointerType)&Enumerator_MoveNext_m22370_gshared/* 768*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m22300_gshared/* 769*/,
	(methodPointerType)&Enumerator_get_Current_m22339_gshared/* 770*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m22311_gshared/* 771*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m22309_gshared/* 772*/,
	(methodPointerType)&Enumerator_MoveNext_m22338_gshared/* 773*/,
	(methodPointerType)&KeyValuePair_2_ToString_m22313_gshared/* 774*/,
	(methodPointerType)&Comparison_1__ctor_m5836_gshared/* 775*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t990_m5831_gshared/* 776*/,
	(methodPointerType)&UnityEvent_1__ctor_m5841_gshared/* 777*/,
	(methodPointerType)&UnityEvent_1_Invoke_m5843_gshared/* 778*/,
	(methodPointerType)&UnityEvent_1_AddListener_m5844_gshared/* 779*/,
	(methodPointerType)&TweenRunner_1__ctor_m5866_gshared/* 780*/,
	(methodPointerType)&TweenRunner_1_Init_m5867_gshared/* 781*/,
	(methodPointerType)&UnityAction_1__ctor_m5888_gshared/* 782*/,
	(methodPointerType)&TweenRunner_1_StartTween_m5889_gshared/* 783*/,
	(methodPointerType)&List_1_get_Capacity_m5891_gshared/* 784*/,
	(methodPointerType)&List_1_set_Capacity_m5892_gshared/* 785*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t189_m5915_gshared/* 786*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisByte_t237_m5917_gshared/* 787*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t202_m5919_gshared/* 788*/,
	(methodPointerType)&List_1__ctor_m5971_gshared/* 789*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisUInt16_t194_m5967_gshared/* 790*/,
	(methodPointerType)&List_1_ToArray_m6018_gshared/* 791*/,
	(methodPointerType)&UnityEvent_1__ctor_m6044_gshared/* 792*/,
	(methodPointerType)&UnityEvent_1_Invoke_m6050_gshared/* 793*/,
	(methodPointerType)&UnityEvent_1__ctor_m6055_gshared/* 794*/,
	(methodPointerType)&UnityAction_1__ctor_m6056_gshared/* 795*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m6057_gshared/* 796*/,
	(methodPointerType)&UnityEvent_1_AddListener_m6058_gshared/* 797*/,
	(methodPointerType)&UnityEvent_1_Invoke_m6062_gshared/* 798*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t1272_m6078_gshared/* 799*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t1221_m6080_gshared/* 800*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t1290_m6081_gshared/* 801*/,
	(methodPointerType)&UnityEvent_1__ctor_m20913_gshared/* 802*/,
	(methodPointerType)&UnityEvent_1_Invoke_m20921_gshared/* 803*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t189_m6138_gshared/* 804*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t739_m6140_gshared/* 805*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t202_m6145_gshared/* 806*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisByte_t237_m6147_gshared/* 807*/,
	(methodPointerType)&Func_2__ctor_m24055_gshared/* 808*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m6304_gshared/* 809*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m6305_gshared/* 810*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m6312_gshared/* 811*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m6313_gshared/* 812*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m6314_gshared/* 813*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m6315_gshared/* 814*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m20917_gshared/* 815*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20918_gshared/* 816*/,
	(methodPointerType)&List_1__ctor_m7513_gshared/* 817*/,
	(methodPointerType)&List_1__ctor_m7514_gshared/* 818*/,
	(methodPointerType)&List_1__ctor_m7515_gshared/* 819*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7535_gshared/* 820*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m7536_gshared/* 821*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m25314_gshared/* 822*/,
	(methodPointerType)&Dictionary_2__ctor_m25528_gshared/* 823*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t189_m8861_gshared/* 824*/,
	(methodPointerType)&GenericComparer_1__ctor_m14423_gshared/* 825*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14424_gshared/* 826*/,
	(methodPointerType)&GenericComparer_1__ctor_m14425_gshared/* 827*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14426_gshared/* 828*/,
	(methodPointerType)&GenericComparer_1__ctor_m14427_gshared/* 829*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14428_gshared/* 830*/,
	(methodPointerType)&GenericComparer_1__ctor_m14429_gshared/* 831*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m14430_gshared/* 832*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3407_m26171_gshared/* 833*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3407_m26172_gshared/* 834*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3407_m26173_gshared/* 835*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3407_m26174_gshared/* 836*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3407_m26175_gshared/* 837*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3407_m26176_gshared/* 838*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3407_m26177_gshared/* 839*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3407_m26179_gshared/* 840*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3407_m26191_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t189_m26199_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t189_m26200_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t189_m26201_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t189_m26202_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t189_m26203_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t189_m26204_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t189_m26205_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t189_m26207_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t189_m26208_gshared/* 850*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t234_m26210_gshared/* 851*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t234_m26211_gshared/* 852*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t234_m26212_gshared/* 853*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t234_m26213_gshared/* 854*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t234_m26214_gshared/* 855*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t234_m26215_gshared/* 856*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t234_m26216_gshared/* 857*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t234_m26218_gshared/* 858*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t234_m26219_gshared/* 859*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t194_m26221_gshared/* 860*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t194_m26222_gshared/* 861*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t194_m26223_gshared/* 862*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t194_m26224_gshared/* 863*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t194_m26225_gshared/* 864*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t194_m26226_gshared/* 865*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t194_m26227_gshared/* 866*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t194_m26229_gshared/* 867*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t194_m26230_gshared/* 868*/,
	(methodPointerType)&List_1_ConvertAll_TisKeyValuePair_2_t3407_m26236_gshared/* 869*/,
	(methodPointerType)&Array_Resize_TisKeyValuePair_2_t3407_m26238_gshared/* 870*/,
	(methodPointerType)&Array_Resize_TisKeyValuePair_2_t3407_m26237_gshared/* 871*/,
	(methodPointerType)&Array_IndexOf_TisKeyValuePair_2_t3407_m26239_gshared/* 872*/,
	(methodPointerType)&Array_Sort_TisKeyValuePair_2_t3407_m26241_gshared/* 873*/,
	(methodPointerType)&Array_Sort_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26240_gshared/* 874*/,
	(methodPointerType)&Array_get_swapper_TisKeyValuePair_2_t3407_m26242_gshared/* 875*/,
	(methodPointerType)&Array_qsort_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26243_gshared/* 876*/,
	(methodPointerType)&Array_compare_TisKeyValuePair_2_t3407_m26244_gshared/* 877*/,
	(methodPointerType)&Array_swap_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26245_gshared/* 878*/,
	(methodPointerType)&Array_Sort_TisKeyValuePair_2_t3407_m26247_gshared/* 879*/,
	(methodPointerType)&Array_qsort_TisKeyValuePair_2_t3407_m26246_gshared/* 880*/,
	(methodPointerType)&Array_swap_TisKeyValuePair_2_t3407_m26248_gshared/* 881*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t2128_m26252_gshared/* 882*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t2128_m26253_gshared/* 883*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t2128_m26254_gshared/* 884*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t2128_m26255_gshared/* 885*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t2128_m26256_gshared/* 886*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t2128_m26257_gshared/* 887*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t2128_m26258_gshared/* 888*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t2128_m26260_gshared/* 889*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t2128_m26261_gshared/* 890*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26262_gshared/* 891*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3407_m26264_gshared/* 892*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3407_TisKeyValuePair_2_t3407_m26265_gshared/* 893*/,
	(methodPointerType)&Array_IndexOf_TisUInt16_t194_m26266_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3531_m26268_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3531_m26269_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3531_m26270_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3531_m26271_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3531_m26272_gshared/* 899*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3531_m26273_gshared/* 900*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3531_m26274_gshared/* 901*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3531_m26276_gshared/* 902*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3531_m26277_gshared/* 903*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26279_gshared/* 904*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26278_gshared/* 905*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26281_gshared/* 906*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26282_gshared/* 907*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26283_gshared/* 908*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3531_m26285_gshared/* 909*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3531_TisKeyValuePair_2_t3531_m26286_gshared/* 910*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t237_m26288_gshared/* 911*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t237_m26289_gshared/* 912*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t237_m26290_gshared/* 913*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t237_m26291_gshared/* 914*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t237_m26292_gshared/* 915*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t237_m26293_gshared/* 916*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t237_m26294_gshared/* 917*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t237_m26296_gshared/* 918*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t237_m26297_gshared/* 919*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t3579_m26299_gshared/* 920*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t3579_m26300_gshared/* 921*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t3579_m26301_gshared/* 922*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t3579_m26302_gshared/* 923*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t3579_m26303_gshared/* 924*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t3579_m26304_gshared/* 925*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t3579_m26305_gshared/* 926*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t3579_m26307_gshared/* 927*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3579_m26308_gshared/* 928*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3656_m26312_gshared/* 929*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3656_m26313_gshared/* 930*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3656_m26314_gshared/* 931*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3656_m26315_gshared/* 932*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3656_m26316_gshared/* 933*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3656_m26317_gshared/* 934*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3656_m26318_gshared/* 935*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3656_m26320_gshared/* 936*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3656_m26321_gshared/* 937*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t235_m26323_gshared/* 938*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t235_m26324_gshared/* 939*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t235_m26325_gshared/* 940*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t235_m26326_gshared/* 941*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t235_m26327_gshared/* 942*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t235_m26328_gshared/* 943*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t235_m26329_gshared/* 944*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t235_m26331_gshared/* 945*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t235_m26332_gshared/* 946*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26334_gshared/* 947*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26333_gshared/* 948*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt32_t235_m26336_gshared/* 949*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt32_t235_TisUInt32_t235_m26337_gshared/* 950*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26338_gshared/* 951*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3656_m26340_gshared/* 952*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3656_TisKeyValuePair_2_t3656_m26341_gshared/* 953*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m26351_gshared/* 954*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m26352_gshared/* 955*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m26353_gshared/* 956*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m26354_gshared/* 957*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m26355_gshared/* 958*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m26356_gshared/* 959*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m26357_gshared/* 960*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m26359_gshared/* 961*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m26360_gshared/* 962*/,
	(methodPointerType)&NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisByte_t237_m26364_gshared/* 963*/,
	(methodPointerType)&NativeClient_InvokeCallbackOnGameThread_TisInt32_t189_m26366_gshared/* 964*/,
	(methodPointerType)&NativeClient_U3CAsOnGameThreadCallback_1U3Em__D_TisInt32_t189_m26365_gshared/* 965*/,
	(methodPointerType)&CallbackUtils_U3CToOnGameThread_2U3Em__6_TisInt32_t189_TisObject_t_m26367_gshared/* 966*/,
	(methodPointerType)&CallbackUtils_U3CToOnGameThread_3U3Em__8_TisInt32_t189_TisObject_t_TisObject_t_m26369_gshared/* 967*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t3760_m26375_gshared/* 968*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t3760_m26376_gshared/* 969*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t3760_m26377_gshared/* 970*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t3760_m26378_gshared/* 971*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t3760_m26379_gshared/* 972*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t3760_m26380_gshared/* 973*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t3760_m26381_gshared/* 974*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t3760_m26383_gshared/* 975*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t3760_m26384_gshared/* 976*/,
	(methodPointerType)&Enumerable_CreateSelectIterator_TisIntPtr_t_TisObject_t_m26396_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3790_m26398_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3790_m26399_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3790_m26400_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3790_m26401_gshared/* 981*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3790_m26402_gshared/* 982*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3790_m26403_gshared/* 983*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3790_m26404_gshared/* 984*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3790_m26406_gshared/* 985*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3790_m26407_gshared/* 986*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26410_gshared/* 987*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26409_gshared/* 988*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26411_gshared/* 989*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26412_gshared/* 990*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3790_m26414_gshared/* 991*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3790_TisKeyValuePair_2_t3790_m26415_gshared/* 992*/,
	(methodPointerType)&Enumerable_CreateSelectIterator_TisObject_t_TisIntPtr_t_m26418_gshared/* 993*/,
	(methodPointerType)&Array_Resize_TisIntPtr_t_m26420_gshared/* 994*/,
	(methodPointerType)&Array_Resize_TisIntPtr_t_m26419_gshared/* 995*/,
	(methodPointerType)&Array_IndexOf_TisIntPtr_t_m26421_gshared/* 996*/,
	(methodPointerType)&Array_Sort_TisIntPtr_t_m26423_gshared/* 997*/,
	(methodPointerType)&Array_Sort_TisIntPtr_t_TisIntPtr_t_m26422_gshared/* 998*/,
	(methodPointerType)&Array_get_swapper_TisIntPtr_t_m26424_gshared/* 999*/,
	(methodPointerType)&Array_qsort_TisIntPtr_t_TisIntPtr_t_m26425_gshared/* 1000*/,
	(methodPointerType)&Array_compare_TisIntPtr_t_m26426_gshared/* 1001*/,
	(methodPointerType)&Array_swap_TisIntPtr_t_TisIntPtr_t_m26427_gshared/* 1002*/,
	(methodPointerType)&Array_Sort_TisIntPtr_t_m26429_gshared/* 1003*/,
	(methodPointerType)&Array_qsort_TisIntPtr_t_m26428_gshared/* 1004*/,
	(methodPointerType)&Array_swap_TisIntPtr_t_m26430_gshared/* 1005*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisByte_t237_m26431_gshared/* 1006*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMatrix4x4_t997_m26433_gshared/* 1007*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMatrix4x4_t997_m26434_gshared/* 1008*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMatrix4x4_t997_m26435_gshared/* 1009*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMatrix4x4_t997_m26436_gshared/* 1010*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMatrix4x4_t997_m26437_gshared/* 1011*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMatrix4x4_t997_m26438_gshared/* 1012*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMatrix4x4_t997_m26439_gshared/* 1013*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMatrix4x4_t997_m26441_gshared/* 1014*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMatrix4x4_t997_m26442_gshared/* 1015*/,
	(methodPointerType)&Array_Resize_TisMatrix4x4_t997_m26444_gshared/* 1016*/,
	(methodPointerType)&Array_Resize_TisMatrix4x4_t997_m26443_gshared/* 1017*/,
	(methodPointerType)&Array_IndexOf_TisMatrix4x4_t997_m26445_gshared/* 1018*/,
	(methodPointerType)&Array_Sort_TisMatrix4x4_t997_m26447_gshared/* 1019*/,
	(methodPointerType)&Array_Sort_TisMatrix4x4_t997_TisMatrix4x4_t997_m26446_gshared/* 1020*/,
	(methodPointerType)&Array_get_swapper_TisMatrix4x4_t997_m26448_gshared/* 1021*/,
	(methodPointerType)&Array_qsort_TisMatrix4x4_t997_TisMatrix4x4_t997_m26449_gshared/* 1022*/,
	(methodPointerType)&Array_compare_TisMatrix4x4_t997_m26450_gshared/* 1023*/,
	(methodPointerType)&Array_swap_TisMatrix4x4_t997_TisMatrix4x4_t997_m26451_gshared/* 1024*/,
	(methodPointerType)&Array_Sort_TisMatrix4x4_t997_m26453_gshared/* 1025*/,
	(methodPointerType)&Array_qsort_TisMatrix4x4_t997_m26452_gshared/* 1026*/,
	(methodPointerType)&Array_swap_TisMatrix4x4_t997_m26454_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t758_m26456_gshared/* 1028*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t758_m26457_gshared/* 1029*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t758_m26458_gshared/* 1030*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t758_m26459_gshared/* 1031*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t758_m26460_gshared/* 1032*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t758_m26461_gshared/* 1033*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t758_m26462_gshared/* 1034*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t758_m26464_gshared/* 1035*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t758_m26465_gshared/* 1036*/,
	(methodPointerType)&Array_Resize_TisVector3_t758_m26467_gshared/* 1037*/,
	(methodPointerType)&Array_Resize_TisVector3_t758_m26466_gshared/* 1038*/,
	(methodPointerType)&Array_IndexOf_TisVector3_t758_m26468_gshared/* 1039*/,
	(methodPointerType)&Array_Sort_TisVector3_t758_m26470_gshared/* 1040*/,
	(methodPointerType)&Array_Sort_TisVector3_t758_TisVector3_t758_m26469_gshared/* 1041*/,
	(methodPointerType)&Array_get_swapper_TisVector3_t758_m26471_gshared/* 1042*/,
	(methodPointerType)&Array_qsort_TisVector3_t758_TisVector3_t758_m26472_gshared/* 1043*/,
	(methodPointerType)&Array_compare_TisVector3_t758_m26473_gshared/* 1044*/,
	(methodPointerType)&Array_swap_TisVector3_t758_TisVector3_t758_m26474_gshared/* 1045*/,
	(methodPointerType)&Array_Sort_TisVector3_t758_m26476_gshared/* 1046*/,
	(methodPointerType)&Array_qsort_TisVector3_t758_m26475_gshared/* 1047*/,
	(methodPointerType)&Array_swap_TisVector3_t758_m26477_gshared/* 1048*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTouch_t901_m26479_gshared/* 1049*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTouch_t901_m26480_gshared/* 1050*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTouch_t901_m26481_gshared/* 1051*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTouch_t901_m26482_gshared/* 1052*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTouch_t901_m26483_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTouch_t901_m26484_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTouch_t901_m26485_gshared/* 1055*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTouch_t901_m26487_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTouch_t901_m26488_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t1187_m26495_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t1187_m26496_gshared/* 1059*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t1187_m26497_gshared/* 1060*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t1187_m26498_gshared/* 1061*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t1187_m26499_gshared/* 1062*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t1187_m26500_gshared/* 1063*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t1187_m26501_gshared/* 1064*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t1187_m26503_gshared/* 1065*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t1187_m26504_gshared/* 1066*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t1187_m26506_gshared/* 1067*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t1187_m26505_gshared/* 1068*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t1187_m26507_gshared/* 1069*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t1187_m26509_gshared/* 1070*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t1187_TisRaycastResult_t1187_m26508_gshared/* 1071*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t1187_m26510_gshared/* 1072*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t1187_TisRaycastResult_t1187_m26511_gshared/* 1073*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t1187_m26512_gshared/* 1074*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t1187_TisRaycastResult_t1187_m26513_gshared/* 1075*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t1187_m26515_gshared/* 1076*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t1187_m26514_gshared/* 1077*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t1187_m26516_gshared/* 1078*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t3921_m26520_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3921_m26521_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3921_m26522_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3921_m26523_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3921_m26524_gshared/* 1083*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3921_m26525_gshared/* 1084*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t3921_m26526_gshared/* 1085*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t3921_m26528_gshared/* 1086*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3921_m26529_gshared/* 1087*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t189_m26532_gshared/* 1088*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26531_gshared/* 1089*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t189_TisInt32_t189_m26533_gshared/* 1090*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26534_gshared/* 1091*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26535_gshared/* 1092*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t3921_m26537_gshared/* 1093*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t3921_TisKeyValuePair_2_t3921_m26538_gshared/* 1094*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t1378_m26540_gshared/* 1095*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t1378_m26541_gshared/* 1096*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t1378_m26542_gshared/* 1097*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t1378_m26543_gshared/* 1098*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t1378_m26544_gshared/* 1099*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t1378_m26545_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t1378_m26546_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t1378_m26548_gshared/* 1102*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t1378_m26549_gshared/* 1103*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t990_m26551_gshared/* 1104*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t990_m26552_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t990_m26553_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t990_m26554_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t990_m26555_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t990_m26556_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t990_m26557_gshared/* 1110*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t990_m26559_gshared/* 1111*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t990_m26560_gshared/* 1112*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t990_m26561_gshared/* 1113*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t990_m26562_gshared/* 1114*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t990_m26563_gshared/* 1115*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t747_m26564_gshared/* 1116*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t1265_m26566_gshared/* 1117*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t1265_m26565_gshared/* 1118*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t1265_m26567_gshared/* 1119*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t1265_m26569_gshared/* 1120*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t1265_TisUIVertex_t1265_m26568_gshared/* 1121*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t1265_m26570_gshared/* 1122*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t1265_TisUIVertex_t1265_m26571_gshared/* 1123*/,
	(methodPointerType)&Array_compare_TisUIVertex_t1265_m26572_gshared/* 1124*/,
	(methodPointerType)&Array_swap_TisUIVertex_t1265_TisUIVertex_t1265_m26573_gshared/* 1125*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t1265_m26575_gshared/* 1126*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t1265_m26574_gshared/* 1127*/,
	(methodPointerType)&Array_swap_TisUIVertex_t1265_m26576_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t739_m26578_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t739_m26579_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t739_m26580_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t739_m26581_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t739_m26582_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t739_m26583_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t739_m26584_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t739_m26586_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t739_m26587_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t1398_m26589_gshared/* 1138*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t1398_m26590_gshared/* 1139*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t1398_m26591_gshared/* 1140*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t1398_m26592_gshared/* 1141*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t1398_m26593_gshared/* 1142*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t1398_m26594_gshared/* 1143*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t1398_m26595_gshared/* 1144*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t1398_m26597_gshared/* 1145*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t1398_m26598_gshared/* 1146*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t1400_m26600_gshared/* 1147*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t1400_m26601_gshared/* 1148*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t1400_m26602_gshared/* 1149*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t1400_m26603_gshared/* 1150*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t1400_m26604_gshared/* 1151*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t1400_m26605_gshared/* 1152*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t1400_m26606_gshared/* 1153*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t1400_m26608_gshared/* 1154*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t1400_m26609_gshared/* 1155*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t202_m26610_gshared/* 1156*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t739_m26611_gshared/* 1157*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t1617_m26615_gshared/* 1158*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t1617_m26616_gshared/* 1159*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t1617_m26617_gshared/* 1160*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t1617_m26618_gshared/* 1161*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t1617_m26619_gshared/* 1162*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t1617_m26620_gshared/* 1163*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t1617_m26621_gshared/* 1164*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t1617_m26623_gshared/* 1165*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t1617_m26624_gshared/* 1166*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t1618_m26626_gshared/* 1167*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t1618_m26627_gshared/* 1168*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t1618_m26628_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t1618_m26629_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t1618_m26630_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t1618_m26631_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t1618_m26632_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t1618_m26634_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t1618_m26635_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t202_m26637_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t202_m26638_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t202_m26639_gshared/* 1178*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t202_m26640_gshared/* 1179*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t202_m26641_gshared/* 1180*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t202_m26642_gshared/* 1181*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t202_m26643_gshared/* 1182*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t202_m26645_gshared/* 1183*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t202_m26646_gshared/* 1184*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t1591_m26648_gshared/* 1185*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t1591_m26649_gshared/* 1186*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t1591_m26650_gshared/* 1187*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t1591_m26651_gshared/* 1188*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t1591_m26652_gshared/* 1189*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t1591_m26653_gshared/* 1190*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t1591_m26654_gshared/* 1191*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t1591_m26656_gshared/* 1192*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t1591_m26657_gshared/* 1193*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t1400_m26659_gshared/* 1194*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t1400_m26658_gshared/* 1195*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t1400_m26660_gshared/* 1196*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t1400_m26662_gshared/* 1197*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t1400_TisUICharInfo_t1400_m26661_gshared/* 1198*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t1400_m26663_gshared/* 1199*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t1400_TisUICharInfo_t1400_m26664_gshared/* 1200*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t1400_m26665_gshared/* 1201*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t1400_TisUICharInfo_t1400_m26666_gshared/* 1202*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t1400_m26668_gshared/* 1203*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t1400_m26667_gshared/* 1204*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t1400_m26669_gshared/* 1205*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t1398_m26671_gshared/* 1206*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t1398_m26670_gshared/* 1207*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t1398_m26672_gshared/* 1208*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t1398_m26674_gshared/* 1209*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t1398_TisUILineInfo_t1398_m26673_gshared/* 1210*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t1398_m26675_gshared/* 1211*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t1398_TisUILineInfo_t1398_m26676_gshared/* 1212*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t1398_m26677_gshared/* 1213*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t1398_TisUILineInfo_t1398_m26678_gshared/* 1214*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t1398_m26680_gshared/* 1215*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t1398_m26679_gshared/* 1216*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t1398_m26681_gshared/* 1217*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t2550_m26683_gshared/* 1218*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t2550_m26684_gshared/* 1219*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t2550_m26685_gshared/* 1220*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t2550_m26686_gshared/* 1221*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t2550_m26687_gshared/* 1222*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t2550_m26688_gshared/* 1223*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t2550_m26689_gshared/* 1224*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t2550_m26691_gshared/* 1225*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t2550_m26692_gshared/* 1226*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t1629_m26694_gshared/* 1227*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t1629_m26695_gshared/* 1228*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t1629_m26696_gshared/* 1229*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t1629_m26697_gshared/* 1230*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t1629_m26698_gshared/* 1231*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t1629_m26699_gshared/* 1232*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t1629_m26700_gshared/* 1233*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t1629_m26702_gshared/* 1234*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t1629_m26703_gshared/* 1235*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t189_m26704_gshared/* 1236*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t4171_m26706_gshared/* 1237*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4171_m26707_gshared/* 1238*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4171_m26708_gshared/* 1239*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4171_m26709_gshared/* 1240*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4171_m26710_gshared/* 1241*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4171_m26711_gshared/* 1242*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t4171_m26712_gshared/* 1243*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t4171_m26714_gshared/* 1244*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4171_m26715_gshared/* 1245*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m26717_gshared/* 1246*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m26716_gshared/* 1247*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t237_m26719_gshared/* 1248*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t237_TisByte_t237_m26720_gshared/* 1249*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t2128_TisDictionaryEntry_t2128_m26721_gshared/* 1250*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t4171_m26723_gshared/* 1251*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t4171_TisKeyValuePair_2_t4171_m26724_gshared/* 1252*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t2034_m26726_gshared/* 1253*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t2034_m26727_gshared/* 1254*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t2034_m26728_gshared/* 1255*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t2034_m26729_gshared/* 1256*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t2034_m26730_gshared/* 1257*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t2034_m26731_gshared/* 1258*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t2034_m26732_gshared/* 1259*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t2034_m26734_gshared/* 1260*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t2034_m26735_gshared/* 1261*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t189_m26736_gshared/* 1262*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t2083_m26738_gshared/* 1263*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t2083_m26739_gshared/* 1264*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t2083_m26740_gshared/* 1265*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t2083_m26741_gshared/* 1266*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t2083_m26742_gshared/* 1267*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t2083_m26743_gshared/* 1268*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t2083_m26744_gshared/* 1269*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t2083_m26746_gshared/* 1270*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t2083_m26747_gshared/* 1271*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t2118_m26749_gshared/* 1272*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t2118_m26750_gshared/* 1273*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t2118_m26751_gshared/* 1274*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t2118_m26752_gshared/* 1275*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t2118_m26753_gshared/* 1276*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t2118_m26754_gshared/* 1277*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t2118_m26755_gshared/* 1278*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t2118_m26757_gshared/* 1279*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t2118_m26758_gshared/* 1280*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t239_m26760_gshared/* 1281*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t239_m26761_gshared/* 1282*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t239_m26762_gshared/* 1283*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t239_m26763_gshared/* 1284*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t239_m26764_gshared/* 1285*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t239_m26765_gshared/* 1286*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t239_m26766_gshared/* 1287*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t239_m26768_gshared/* 1288*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t239_m26769_gshared/* 1289*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t238_m26771_gshared/* 1290*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t238_m26772_gshared/* 1291*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t238_m26773_gshared/* 1292*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t238_m26774_gshared/* 1293*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t238_m26775_gshared/* 1294*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t238_m26776_gshared/* 1295*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t238_m26777_gshared/* 1296*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t238_m26779_gshared/* 1297*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t238_m26780_gshared/* 1298*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t236_m26782_gshared/* 1299*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t236_m26783_gshared/* 1300*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t236_m26784_gshared/* 1301*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t236_m26785_gshared/* 1302*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t236_m26786_gshared/* 1303*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t236_m26787_gshared/* 1304*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t236_m26788_gshared/* 1305*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t236_m26790_gshared/* 1306*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t236_m26791_gshared/* 1307*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t233_m26793_gshared/* 1308*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t233_m26794_gshared/* 1309*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t233_m26795_gshared/* 1310*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t233_m26796_gshared/* 1311*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t233_m26797_gshared/* 1312*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t233_m26798_gshared/* 1313*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t233_m26799_gshared/* 1314*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t233_m26801_gshared/* 1315*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t233_m26802_gshared/* 1316*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t2360_m26832_gshared/* 1317*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t2360_m26833_gshared/* 1318*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t2360_m26834_gshared/* 1319*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t2360_m26835_gshared/* 1320*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t2360_m26836_gshared/* 1321*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t2360_m26837_gshared/* 1322*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t2360_m26838_gshared/* 1323*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t2360_m26840_gshared/* 1324*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t2360_m26841_gshared/* 1325*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2436_m26843_gshared/* 1326*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2436_m26844_gshared/* 1327*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2436_m26845_gshared/* 1328*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2436_m26846_gshared/* 1329*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2436_m26847_gshared/* 1330*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2436_m26848_gshared/* 1331*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2436_m26849_gshared/* 1332*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2436_m26851_gshared/* 1333*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2436_m26852_gshared/* 1334*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t2443_m26854_gshared/* 1335*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t2443_m26855_gshared/* 1336*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t2443_m26856_gshared/* 1337*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t2443_m26857_gshared/* 1338*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t2443_m26858_gshared/* 1339*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t2443_m26859_gshared/* 1340*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t2443_m26860_gshared/* 1341*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t2443_m26862_gshared/* 1342*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t2443_m26863_gshared/* 1343*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t48_m26867_gshared/* 1344*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t48_m26868_gshared/* 1345*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t48_m26869_gshared/* 1346*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t48_m26870_gshared/* 1347*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t48_m26871_gshared/* 1348*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t48_m26872_gshared/* 1349*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t48_m26873_gshared/* 1350*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t48_m26875_gshared/* 1351*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t48_m26876_gshared/* 1352*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t240_m26878_gshared/* 1353*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t240_m26879_gshared/* 1354*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t240_m26880_gshared/* 1355*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t240_m26881_gshared/* 1356*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t240_m26882_gshared/* 1357*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t240_m26883_gshared/* 1358*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t240_m26884_gshared/* 1359*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t240_m26886_gshared/* 1360*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t240_m26887_gshared/* 1361*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t190_m26889_gshared/* 1362*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t190_m26890_gshared/* 1363*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t190_m26891_gshared/* 1364*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t190_m26892_gshared/* 1365*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t190_m26893_gshared/* 1366*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t190_m26894_gshared/* 1367*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t190_m26895_gshared/* 1368*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t190_m26897_gshared/* 1369*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t190_m26898_gshared/* 1370*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15298_gshared/* 1371*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15299_gshared/* 1372*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15300_gshared/* 1373*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15301_gshared/* 1374*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15302_gshared/* 1375*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15521_gshared/* 1376*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522_gshared/* 1377*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15523_gshared/* 1378*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15524_gshared/* 1379*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15525_gshared/* 1380*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15526_gshared/* 1381*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15527_gshared/* 1382*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15528_gshared/* 1383*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15529_gshared/* 1384*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15530_gshared/* 1385*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15532_gshared/* 1386*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15534_gshared/* 1387*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15536_gshared/* 1388*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15538_gshared/* 1389*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15540_gshared/* 1390*/,
	(methodPointerType)&Converter_2__ctor_m15610_gshared/* 1391*/,
	(methodPointerType)&Converter_2_Invoke_m15612_gshared/* 1392*/,
	(methodPointerType)&Converter_2_BeginInvoke_m15614_gshared/* 1393*/,
	(methodPointerType)&Converter_2_EndInvoke_m15616_gshared/* 1394*/,
	(methodPointerType)&List_1__ctor_m15617_gshared/* 1395*/,
	(methodPointerType)&List_1__ctor_m15618_gshared/* 1396*/,
	(methodPointerType)&List_1__ctor_m15619_gshared/* 1397*/,
	(methodPointerType)&List_1__cctor_m15620_gshared/* 1398*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15621_gshared/* 1399*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15622_gshared/* 1400*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15623_gshared/* 1401*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15624_gshared/* 1402*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15625_gshared/* 1403*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15626_gshared/* 1404*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15627_gshared/* 1405*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15628_gshared/* 1406*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15629_gshared/* 1407*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15630_gshared/* 1408*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15631_gshared/* 1409*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15632_gshared/* 1410*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15633_gshared/* 1411*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15634_gshared/* 1412*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15635_gshared/* 1413*/,
	(methodPointerType)&List_1_Add_m15636_gshared/* 1414*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15637_gshared/* 1415*/,
	(methodPointerType)&List_1_AddCollection_m15638_gshared/* 1416*/,
	(methodPointerType)&List_1_AddEnumerable_m15639_gshared/* 1417*/,
	(methodPointerType)&List_1_AddRange_m15640_gshared/* 1418*/,
	(methodPointerType)&List_1_AsReadOnly_m15641_gshared/* 1419*/,
	(methodPointerType)&List_1_Clear_m15642_gshared/* 1420*/,
	(methodPointerType)&List_1_Contains_m15643_gshared/* 1421*/,
	(methodPointerType)&List_1_CopyTo_m15644_gshared/* 1422*/,
	(methodPointerType)&List_1_Find_m15645_gshared/* 1423*/,
	(methodPointerType)&List_1_CheckMatch_m15646_gshared/* 1424*/,
	(methodPointerType)&List_1_FindIndex_m15647_gshared/* 1425*/,
	(methodPointerType)&List_1_GetIndex_m15648_gshared/* 1426*/,
	(methodPointerType)&List_1_ForEach_m15649_gshared/* 1427*/,
	(methodPointerType)&List_1_GetEnumerator_m15650_gshared/* 1428*/,
	(methodPointerType)&List_1_IndexOf_m15651_gshared/* 1429*/,
	(methodPointerType)&List_1_Shift_m15652_gshared/* 1430*/,
	(methodPointerType)&List_1_CheckIndex_m15653_gshared/* 1431*/,
	(methodPointerType)&List_1_Insert_m15654_gshared/* 1432*/,
	(methodPointerType)&List_1_CheckCollection_m15655_gshared/* 1433*/,
	(methodPointerType)&List_1_Remove_m15656_gshared/* 1434*/,
	(methodPointerType)&List_1_RemoveAll_m15657_gshared/* 1435*/,
	(methodPointerType)&List_1_RemoveAt_m15658_gshared/* 1436*/,
	(methodPointerType)&List_1_Reverse_m15659_gshared/* 1437*/,
	(methodPointerType)&List_1_Sort_m15660_gshared/* 1438*/,
	(methodPointerType)&List_1_Sort_m15661_gshared/* 1439*/,
	(methodPointerType)&List_1_ToArray_m15662_gshared/* 1440*/,
	(methodPointerType)&List_1_TrimExcess_m15663_gshared/* 1441*/,
	(methodPointerType)&List_1_get_Capacity_m15664_gshared/* 1442*/,
	(methodPointerType)&List_1_set_Capacity_m15665_gshared/* 1443*/,
	(methodPointerType)&List_1_get_Count_m15666_gshared/* 1444*/,
	(methodPointerType)&List_1_get_Item_m15667_gshared/* 1445*/,
	(methodPointerType)&List_1_set_Item_m15668_gshared/* 1446*/,
	(methodPointerType)&Enumerator__ctor_m15669_gshared/* 1447*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15670_gshared/* 1448*/,
	(methodPointerType)&Enumerator_Dispose_m15671_gshared/* 1449*/,
	(methodPointerType)&Enumerator_VerifyState_m15672_gshared/* 1450*/,
	(methodPointerType)&Enumerator_MoveNext_m15673_gshared/* 1451*/,
	(methodPointerType)&Enumerator_get_Current_m15674_gshared/* 1452*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15675_gshared/* 1453*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15676_gshared/* 1454*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15677_gshared/* 1455*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15678_gshared/* 1456*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15679_gshared/* 1457*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15680_gshared/* 1458*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15681_gshared/* 1459*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15682_gshared/* 1460*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15683_gshared/* 1461*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15684_gshared/* 1462*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15685_gshared/* 1463*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15686_gshared/* 1464*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15687_gshared/* 1465*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15688_gshared/* 1466*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15689_gshared/* 1467*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15690_gshared/* 1468*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15691_gshared/* 1469*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15692_gshared/* 1470*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15693_gshared/* 1471*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15694_gshared/* 1472*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15695_gshared/* 1473*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15696_gshared/* 1474*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15697_gshared/* 1475*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15698_gshared/* 1476*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15699_gshared/* 1477*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15700_gshared/* 1478*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15701_gshared/* 1479*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15702_gshared/* 1480*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15703_gshared/* 1481*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15704_gshared/* 1482*/,
	(methodPointerType)&Collection_1__ctor_m15705_gshared/* 1483*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15706_gshared/* 1484*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15707_gshared/* 1485*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15708_gshared/* 1486*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15709_gshared/* 1487*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15710_gshared/* 1488*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15711_gshared/* 1489*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15712_gshared/* 1490*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15713_gshared/* 1491*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15714_gshared/* 1492*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15715_gshared/* 1493*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15716_gshared/* 1494*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15717_gshared/* 1495*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15718_gshared/* 1496*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15719_gshared/* 1497*/,
	(methodPointerType)&Collection_1_Add_m15720_gshared/* 1498*/,
	(methodPointerType)&Collection_1_Clear_m15721_gshared/* 1499*/,
	(methodPointerType)&Collection_1_ClearItems_m15722_gshared/* 1500*/,
	(methodPointerType)&Collection_1_Contains_m15723_gshared/* 1501*/,
	(methodPointerType)&Collection_1_CopyTo_m15724_gshared/* 1502*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15725_gshared/* 1503*/,
	(methodPointerType)&Collection_1_IndexOf_m15726_gshared/* 1504*/,
	(methodPointerType)&Collection_1_Insert_m15727_gshared/* 1505*/,
	(methodPointerType)&Collection_1_InsertItem_m15728_gshared/* 1506*/,
	(methodPointerType)&Collection_1_Remove_m15729_gshared/* 1507*/,
	(methodPointerType)&Collection_1_RemoveAt_m15730_gshared/* 1508*/,
	(methodPointerType)&Collection_1_RemoveItem_m15731_gshared/* 1509*/,
	(methodPointerType)&Collection_1_get_Count_m15732_gshared/* 1510*/,
	(methodPointerType)&Collection_1_get_Item_m15733_gshared/* 1511*/,
	(methodPointerType)&Collection_1_set_Item_m15734_gshared/* 1512*/,
	(methodPointerType)&Collection_1_SetItem_m15735_gshared/* 1513*/,
	(methodPointerType)&Collection_1_IsValidItem_m15736_gshared/* 1514*/,
	(methodPointerType)&Collection_1_ConvertItem_m15737_gshared/* 1515*/,
	(methodPointerType)&Collection_1_CheckWritable_m15738_gshared/* 1516*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15739_gshared/* 1517*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15740_gshared/* 1518*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15741_gshared/* 1519*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15742_gshared/* 1520*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15743_gshared/* 1521*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15744_gshared/* 1522*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15745_gshared/* 1523*/,
	(methodPointerType)&DefaultComparer__ctor_m15746_gshared/* 1524*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15747_gshared/* 1525*/,
	(methodPointerType)&DefaultComparer_Equals_m15748_gshared/* 1526*/,
	(methodPointerType)&Converter_2__ctor_m15749_gshared/* 1527*/,
	(methodPointerType)&Converter_2_Invoke_m15750_gshared/* 1528*/,
	(methodPointerType)&Converter_2_BeginInvoke_m15751_gshared/* 1529*/,
	(methodPointerType)&Converter_2_EndInvoke_m15752_gshared/* 1530*/,
	(methodPointerType)&Predicate_1__ctor_m15753_gshared/* 1531*/,
	(methodPointerType)&Predicate_1_Invoke_m15754_gshared/* 1532*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15755_gshared/* 1533*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15756_gshared/* 1534*/,
	(methodPointerType)&Action_1__ctor_m15757_gshared/* 1535*/,
	(methodPointerType)&Action_1_Invoke_m15758_gshared/* 1536*/,
	(methodPointerType)&Action_1_BeginInvoke_m15759_gshared/* 1537*/,
	(methodPointerType)&Action_1_EndInvoke_m15760_gshared/* 1538*/,
	(methodPointerType)&Comparer_1__ctor_m15761_gshared/* 1539*/,
	(methodPointerType)&Comparer_1__cctor_m15762_gshared/* 1540*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15763_gshared/* 1541*/,
	(methodPointerType)&Comparer_1_get_Default_m15764_gshared/* 1542*/,
	(methodPointerType)&DefaultComparer__ctor_m15765_gshared/* 1543*/,
	(methodPointerType)&DefaultComparer_Compare_m15766_gshared/* 1544*/,
	(methodPointerType)&Comparison_1__ctor_m15767_gshared/* 1545*/,
	(methodPointerType)&Comparison_1_Invoke_m15768_gshared/* 1546*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15769_gshared/* 1547*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15770_gshared/* 1548*/,
	(methodPointerType)&Transform_1__ctor_m16126_gshared/* 1549*/,
	(methodPointerType)&Transform_1_Invoke_m16128_gshared/* 1550*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16130_gshared/* 1551*/,
	(methodPointerType)&Transform_1_EndInvoke_m16132_gshared/* 1552*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16315_gshared/* 1553*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16316_gshared/* 1554*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16317_gshared/* 1555*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16318_gshared/* 1556*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16319_gshared/* 1557*/,
	(methodPointerType)&Transform_1__ctor_m16320_gshared/* 1558*/,
	(methodPointerType)&Transform_1_Invoke_m16321_gshared/* 1559*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16322_gshared/* 1560*/,
	(methodPointerType)&Transform_1_EndInvoke_m16323_gshared/* 1561*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16620_gshared/* 1562*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16621_gshared/* 1563*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared/* 1564*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared/* 1565*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16624_gshared/* 1566*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16625_gshared/* 1567*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16626_gshared/* 1568*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16627_gshared/* 1569*/,
	(methodPointerType)&DefaultComparer__ctor_m16628_gshared/* 1570*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16629_gshared/* 1571*/,
	(methodPointerType)&DefaultComparer_Equals_m16630_gshared/* 1572*/,
	(methodPointerType)&Dictionary_2__ctor_m17006_gshared/* 1573*/,
	(methodPointerType)&Dictionary_2__ctor_m17009_gshared/* 1574*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m17011_gshared/* 1575*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m17013_gshared/* 1576*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m17015_gshared/* 1577*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m17017_gshared/* 1578*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m17019_gshared/* 1579*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17021_gshared/* 1580*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17023_gshared/* 1581*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17025_gshared/* 1582*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17027_gshared/* 1583*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17029_gshared/* 1584*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17031_gshared/* 1585*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17033_gshared/* 1586*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m17035_gshared/* 1587*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17037_gshared/* 1588*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17039_gshared/* 1589*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17041_gshared/* 1590*/,
	(methodPointerType)&Dictionary_2_get_Count_m17043_gshared/* 1591*/,
	(methodPointerType)&Dictionary_2_get_Item_m17045_gshared/* 1592*/,
	(methodPointerType)&Dictionary_2_set_Item_m17047_gshared/* 1593*/,
	(methodPointerType)&Dictionary_2_Init_m17049_gshared/* 1594*/,
	(methodPointerType)&Dictionary_2_InitArrays_m17051_gshared/* 1595*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m17053_gshared/* 1596*/,
	(methodPointerType)&Dictionary_2_make_pair_m17055_gshared/* 1597*/,
	(methodPointerType)&Dictionary_2_pick_key_m17057_gshared/* 1598*/,
	(methodPointerType)&Dictionary_2_pick_value_m17059_gshared/* 1599*/,
	(methodPointerType)&Dictionary_2_CopyTo_m17061_gshared/* 1600*/,
	(methodPointerType)&Dictionary_2_Resize_m17063_gshared/* 1601*/,
	(methodPointerType)&Dictionary_2_Add_m17065_gshared/* 1602*/,
	(methodPointerType)&Dictionary_2_Clear_m17067_gshared/* 1603*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m17069_gshared/* 1604*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m17071_gshared/* 1605*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m17073_gshared/* 1606*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m17075_gshared/* 1607*/,
	(methodPointerType)&Dictionary_2_Remove_m17077_gshared/* 1608*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m17079_gshared/* 1609*/,
	(methodPointerType)&Dictionary_2_get_Keys_m17081_gshared/* 1610*/,
	(methodPointerType)&Dictionary_2_get_Values_m17083_gshared/* 1611*/,
	(methodPointerType)&Dictionary_2_ToTKey_m17085_gshared/* 1612*/,
	(methodPointerType)&Dictionary_2_ToTValue_m17087_gshared/* 1613*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m17089_gshared/* 1614*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m17091_gshared/* 1615*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m17093_gshared/* 1616*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17094_gshared/* 1617*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17095_gshared/* 1618*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17096_gshared/* 1619*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17097_gshared/* 1620*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17098_gshared/* 1621*/,
	(methodPointerType)&KeyValuePair_2__ctor_m17099_gshared/* 1622*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m17100_gshared/* 1623*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m17101_gshared/* 1624*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m17102_gshared/* 1625*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m17103_gshared/* 1626*/,
	(methodPointerType)&KeyValuePair_2_ToString_m17104_gshared/* 1627*/,
	(methodPointerType)&KeyCollection__ctor_m17105_gshared/* 1628*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17106_gshared/* 1629*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17107_gshared/* 1630*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17108_gshared/* 1631*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17109_gshared/* 1632*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17110_gshared/* 1633*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m17111_gshared/* 1634*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17112_gshared/* 1635*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17113_gshared/* 1636*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17114_gshared/* 1637*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m17115_gshared/* 1638*/,
	(methodPointerType)&KeyCollection_CopyTo_m17116_gshared/* 1639*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m17117_gshared/* 1640*/,
	(methodPointerType)&KeyCollection_get_Count_m17118_gshared/* 1641*/,
	(methodPointerType)&Enumerator__ctor_m17119_gshared/* 1642*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17120_gshared/* 1643*/,
	(methodPointerType)&Enumerator_Dispose_m17121_gshared/* 1644*/,
	(methodPointerType)&Enumerator_MoveNext_m17122_gshared/* 1645*/,
	(methodPointerType)&Enumerator_get_Current_m17123_gshared/* 1646*/,
	(methodPointerType)&Enumerator__ctor_m17124_gshared/* 1647*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared/* 1648*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared/* 1649*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared/* 1650*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared/* 1651*/,
	(methodPointerType)&Enumerator_MoveNext_m17129_gshared/* 1652*/,
	(methodPointerType)&Enumerator_get_Current_m17130_gshared/* 1653*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m17131_gshared/* 1654*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m17132_gshared/* 1655*/,
	(methodPointerType)&Enumerator_VerifyState_m17133_gshared/* 1656*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m17134_gshared/* 1657*/,
	(methodPointerType)&Enumerator_Dispose_m17135_gshared/* 1658*/,
	(methodPointerType)&Transform_1__ctor_m17136_gshared/* 1659*/,
	(methodPointerType)&Transform_1_Invoke_m17137_gshared/* 1660*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17138_gshared/* 1661*/,
	(methodPointerType)&Transform_1_EndInvoke_m17139_gshared/* 1662*/,
	(methodPointerType)&ValueCollection__ctor_m17140_gshared/* 1663*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17141_gshared/* 1664*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17142_gshared/* 1665*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17143_gshared/* 1666*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17144_gshared/* 1667*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17145_gshared/* 1668*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m17146_gshared/* 1669*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17147_gshared/* 1670*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17148_gshared/* 1671*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17149_gshared/* 1672*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m17150_gshared/* 1673*/,
	(methodPointerType)&ValueCollection_CopyTo_m17151_gshared/* 1674*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m17152_gshared/* 1675*/,
	(methodPointerType)&ValueCollection_get_Count_m17153_gshared/* 1676*/,
	(methodPointerType)&Enumerator__ctor_m17154_gshared/* 1677*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17155_gshared/* 1678*/,
	(methodPointerType)&Enumerator_Dispose_m17156_gshared/* 1679*/,
	(methodPointerType)&Enumerator_MoveNext_m17157_gshared/* 1680*/,
	(methodPointerType)&Enumerator_get_Current_m17158_gshared/* 1681*/,
	(methodPointerType)&Transform_1__ctor_m17159_gshared/* 1682*/,
	(methodPointerType)&Transform_1_Invoke_m17160_gshared/* 1683*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17161_gshared/* 1684*/,
	(methodPointerType)&Transform_1_EndInvoke_m17162_gshared/* 1685*/,
	(methodPointerType)&Transform_1__ctor_m17163_gshared/* 1686*/,
	(methodPointerType)&Transform_1_Invoke_m17164_gshared/* 1687*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17165_gshared/* 1688*/,
	(methodPointerType)&Transform_1_EndInvoke_m17166_gshared/* 1689*/,
	(methodPointerType)&Transform_1__ctor_m17167_gshared/* 1690*/,
	(methodPointerType)&Transform_1_Invoke_m17168_gshared/* 1691*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17169_gshared/* 1692*/,
	(methodPointerType)&Transform_1_EndInvoke_m17170_gshared/* 1693*/,
	(methodPointerType)&ShimEnumerator__ctor_m17171_gshared/* 1694*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m17172_gshared/* 1695*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m17173_gshared/* 1696*/,
	(methodPointerType)&ShimEnumerator_get_Key_m17174_gshared/* 1697*/,
	(methodPointerType)&ShimEnumerator_get_Value_m17175_gshared/* 1698*/,
	(methodPointerType)&ShimEnumerator_get_Current_m17176_gshared/* 1699*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17177_gshared/* 1700*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17178_gshared/* 1701*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17179_gshared/* 1702*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17180_gshared/* 1703*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17181_gshared/* 1704*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m17182_gshared/* 1705*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m17183_gshared/* 1706*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m17184_gshared/* 1707*/,
	(methodPointerType)&DefaultComparer__ctor_m17185_gshared/* 1708*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17186_gshared/* 1709*/,
	(methodPointerType)&DefaultComparer_Equals_m17187_gshared/* 1710*/,
	(methodPointerType)&Action_3_Invoke_m17252_gshared/* 1711*/,
	(methodPointerType)&Action_3_BeginInvoke_m17254_gshared/* 1712*/,
	(methodPointerType)&Action_3_EndInvoke_m17256_gshared/* 1713*/,
	(methodPointerType)&Action_1_Invoke_m17262_gshared/* 1714*/,
	(methodPointerType)&Action_1_BeginInvoke_m17264_gshared/* 1715*/,
	(methodPointerType)&Action_1_EndInvoke_m17266_gshared/* 1716*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17272_gshared/* 1717*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared/* 1718*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17276_gshared/* 1719*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17278_gshared/* 1720*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17280_gshared/* 1721*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17645_gshared/* 1722*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17646_gshared/* 1723*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17647_gshared/* 1724*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17648_gshared/* 1725*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17649_gshared/* 1726*/,
	(methodPointerType)&Func_2_Invoke_m18522_gshared/* 1727*/,
	(methodPointerType)&Func_2_BeginInvoke_m18524_gshared/* 1728*/,
	(methodPointerType)&Func_2_EndInvoke_m18526_gshared/* 1729*/,
	(methodPointerType)&Action_1_Invoke_m18541_gshared/* 1730*/,
	(methodPointerType)&Action_1_BeginInvoke_m18543_gshared/* 1731*/,
	(methodPointerType)&Action_1_EndInvoke_m18545_gshared/* 1732*/,
	(methodPointerType)&Action_2_Invoke_m18650_gshared/* 1733*/,
	(methodPointerType)&Action_2_BeginInvoke_m18652_gshared/* 1734*/,
	(methodPointerType)&Action_2_EndInvoke_m18654_gshared/* 1735*/,
	(methodPointerType)&Action_2__ctor_m18854_gshared/* 1736*/,
	(methodPointerType)&Action_2_Invoke_m18856_gshared/* 1737*/,
	(methodPointerType)&Action_2_BeginInvoke_m18858_gshared/* 1738*/,
	(methodPointerType)&Action_2_EndInvoke_m18860_gshared/* 1739*/,
	(methodPointerType)&Dictionary_2__ctor_m18863_gshared/* 1740*/,
	(methodPointerType)&Dictionary_2__ctor_m18865_gshared/* 1741*/,
	(methodPointerType)&Dictionary_2__ctor_m18867_gshared/* 1742*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m18869_gshared/* 1743*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m18871_gshared/* 1744*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m18873_gshared/* 1745*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m18875_gshared/* 1746*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m18877_gshared/* 1747*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879_gshared/* 1748*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881_gshared/* 1749*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883_gshared/* 1750*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885_gshared/* 1751*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887_gshared/* 1752*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889_gshared/* 1753*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891_gshared/* 1754*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m18893_gshared/* 1755*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895_gshared/* 1756*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897_gshared/* 1757*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899_gshared/* 1758*/,
	(methodPointerType)&Dictionary_2_get_Count_m18901_gshared/* 1759*/,
	(methodPointerType)&Dictionary_2_get_Item_m18903_gshared/* 1760*/,
	(methodPointerType)&Dictionary_2_set_Item_m18905_gshared/* 1761*/,
	(methodPointerType)&Dictionary_2_Init_m18907_gshared/* 1762*/,
	(methodPointerType)&Dictionary_2_InitArrays_m18909_gshared/* 1763*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m18911_gshared/* 1764*/,
	(methodPointerType)&Dictionary_2_make_pair_m18913_gshared/* 1765*/,
	(methodPointerType)&Dictionary_2_pick_key_m18915_gshared/* 1766*/,
	(methodPointerType)&Dictionary_2_pick_value_m18917_gshared/* 1767*/,
	(methodPointerType)&Dictionary_2_CopyTo_m18919_gshared/* 1768*/,
	(methodPointerType)&Dictionary_2_Resize_m18921_gshared/* 1769*/,
	(methodPointerType)&Dictionary_2_Add_m18923_gshared/* 1770*/,
	(methodPointerType)&Dictionary_2_Clear_m18925_gshared/* 1771*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m18927_gshared/* 1772*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m18929_gshared/* 1773*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m18931_gshared/* 1774*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m18933_gshared/* 1775*/,
	(methodPointerType)&Dictionary_2_Remove_m18935_gshared/* 1776*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m18937_gshared/* 1777*/,
	(methodPointerType)&Dictionary_2_get_Keys_m18939_gshared/* 1778*/,
	(methodPointerType)&Dictionary_2_get_Values_m18941_gshared/* 1779*/,
	(methodPointerType)&Dictionary_2_ToTKey_m18943_gshared/* 1780*/,
	(methodPointerType)&Dictionary_2_ToTValue_m18945_gshared/* 1781*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m18947_gshared/* 1782*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m18949_gshared/* 1783*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m18951_gshared/* 1784*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18952_gshared/* 1785*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18953_gshared/* 1786*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18954_gshared/* 1787*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18955_gshared/* 1788*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18956_gshared/* 1789*/,
	(methodPointerType)&KeyValuePair_2__ctor_m18957_gshared/* 1790*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m18958_gshared/* 1791*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m18959_gshared/* 1792*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m18960_gshared/* 1793*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m18961_gshared/* 1794*/,
	(methodPointerType)&KeyValuePair_2_ToString_m18962_gshared/* 1795*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18963_gshared/* 1796*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18964_gshared/* 1797*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18965_gshared/* 1798*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18966_gshared/* 1799*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18967_gshared/* 1800*/,
	(methodPointerType)&KeyCollection__ctor_m18968_gshared/* 1801*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18969_gshared/* 1802*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18970_gshared/* 1803*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18971_gshared/* 1804*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18972_gshared/* 1805*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18973_gshared/* 1806*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m18974_gshared/* 1807*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18975_gshared/* 1808*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18976_gshared/* 1809*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18977_gshared/* 1810*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m18978_gshared/* 1811*/,
	(methodPointerType)&KeyCollection_CopyTo_m18979_gshared/* 1812*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m18980_gshared/* 1813*/,
	(methodPointerType)&KeyCollection_get_Count_m18981_gshared/* 1814*/,
	(methodPointerType)&Enumerator__ctor_m18982_gshared/* 1815*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18983_gshared/* 1816*/,
	(methodPointerType)&Enumerator_Dispose_m18984_gshared/* 1817*/,
	(methodPointerType)&Enumerator_MoveNext_m18985_gshared/* 1818*/,
	(methodPointerType)&Enumerator_get_Current_m18986_gshared/* 1819*/,
	(methodPointerType)&Enumerator__ctor_m18987_gshared/* 1820*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18988_gshared/* 1821*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989_gshared/* 1822*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990_gshared/* 1823*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991_gshared/* 1824*/,
	(methodPointerType)&Enumerator_MoveNext_m18992_gshared/* 1825*/,
	(methodPointerType)&Enumerator_get_Current_m18993_gshared/* 1826*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m18994_gshared/* 1827*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m18995_gshared/* 1828*/,
	(methodPointerType)&Enumerator_VerifyState_m18996_gshared/* 1829*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m18997_gshared/* 1830*/,
	(methodPointerType)&Enumerator_Dispose_m18998_gshared/* 1831*/,
	(methodPointerType)&Transform_1__ctor_m18999_gshared/* 1832*/,
	(methodPointerType)&Transform_1_Invoke_m19000_gshared/* 1833*/,
	(methodPointerType)&Transform_1_BeginInvoke_m19001_gshared/* 1834*/,
	(methodPointerType)&Transform_1_EndInvoke_m19002_gshared/* 1835*/,
	(methodPointerType)&ValueCollection__ctor_m19003_gshared/* 1836*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004_gshared/* 1837*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005_gshared/* 1838*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006_gshared/* 1839*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007_gshared/* 1840*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008_gshared/* 1841*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m19009_gshared/* 1842*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010_gshared/* 1843*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011_gshared/* 1844*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012_gshared/* 1845*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013_gshared/* 1846*/,
	(methodPointerType)&ValueCollection_CopyTo_m19014_gshared/* 1847*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m19015_gshared/* 1848*/,
	(methodPointerType)&ValueCollection_get_Count_m19016_gshared/* 1849*/,
	(methodPointerType)&Enumerator__ctor_m19017_gshared/* 1850*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m19018_gshared/* 1851*/,
	(methodPointerType)&Enumerator_Dispose_m19019_gshared/* 1852*/,
	(methodPointerType)&Enumerator_MoveNext_m19020_gshared/* 1853*/,
	(methodPointerType)&Enumerator_get_Current_m19021_gshared/* 1854*/,
	(methodPointerType)&Transform_1__ctor_m19022_gshared/* 1855*/,
	(methodPointerType)&Transform_1_Invoke_m19023_gshared/* 1856*/,
	(methodPointerType)&Transform_1_BeginInvoke_m19024_gshared/* 1857*/,
	(methodPointerType)&Transform_1_EndInvoke_m19025_gshared/* 1858*/,
	(methodPointerType)&Transform_1__ctor_m19026_gshared/* 1859*/,
	(methodPointerType)&Transform_1_Invoke_m19027_gshared/* 1860*/,
	(methodPointerType)&Transform_1_BeginInvoke_m19028_gshared/* 1861*/,
	(methodPointerType)&Transform_1_EndInvoke_m19029_gshared/* 1862*/,
	(methodPointerType)&Transform_1__ctor_m19030_gshared/* 1863*/,
	(methodPointerType)&Transform_1_Invoke_m19031_gshared/* 1864*/,
	(methodPointerType)&Transform_1_BeginInvoke_m19032_gshared/* 1865*/,
	(methodPointerType)&Transform_1_EndInvoke_m19033_gshared/* 1866*/,
	(methodPointerType)&ShimEnumerator__ctor_m19034_gshared/* 1867*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m19035_gshared/* 1868*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m19036_gshared/* 1869*/,
	(methodPointerType)&ShimEnumerator_get_Key_m19037_gshared/* 1870*/,
	(methodPointerType)&ShimEnumerator_get_Value_m19038_gshared/* 1871*/,
	(methodPointerType)&ShimEnumerator_get_Current_m19039_gshared/* 1872*/,
	(methodPointerType)&EqualityComparer_1__ctor_m19040_gshared/* 1873*/,
	(methodPointerType)&EqualityComparer_1__cctor_m19041_gshared/* 1874*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19042_gshared/* 1875*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19043_gshared/* 1876*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m19044_gshared/* 1877*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m19045_gshared/* 1878*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m19046_gshared/* 1879*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m19047_gshared/* 1880*/,
	(methodPointerType)&DefaultComparer__ctor_m19048_gshared/* 1881*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m19049_gshared/* 1882*/,
	(methodPointerType)&DefaultComparer_Equals_m19050_gshared/* 1883*/,
	(methodPointerType)&Nullable_1_Equals_m19210_gshared/* 1884*/,
	(methodPointerType)&Nullable_1_Equals_m19211_gshared/* 1885*/,
	(methodPointerType)&Nullable_1_GetHashCode_m19212_gshared/* 1886*/,
	(methodPointerType)&Nullable_1_ToString_m19213_gshared/* 1887*/,
	(methodPointerType)&Action_1__ctor_m19214_gshared/* 1888*/,
	(methodPointerType)&Action_1_Invoke_m19215_gshared/* 1889*/,
	(methodPointerType)&Action_1_BeginInvoke_m19216_gshared/* 1890*/,
	(methodPointerType)&Action_1_EndInvoke_m19217_gshared/* 1891*/,
	(methodPointerType)&Action_1__ctor_m19218_gshared/* 1892*/,
	(methodPointerType)&Action_1_Invoke_m19219_gshared/* 1893*/,
	(methodPointerType)&Action_1_BeginInvoke_m19220_gshared/* 1894*/,
	(methodPointerType)&Action_1_EndInvoke_m19221_gshared/* 1895*/,
	(methodPointerType)&Action_1__ctor_m19222_gshared/* 1896*/,
	(methodPointerType)&Action_1_Invoke_m19223_gshared/* 1897*/,
	(methodPointerType)&Action_1_BeginInvoke_m19224_gshared/* 1898*/,
	(methodPointerType)&Action_1_EndInvoke_m19225_gshared/* 1899*/,
	(methodPointerType)&Nullable_1_get_HasValue_m19230_gshared/* 1900*/,
	(methodPointerType)&Nullable_1_get_Value_m19231_gshared/* 1901*/,
	(methodPointerType)&Nullable_1_Equals_m19232_gshared/* 1902*/,
	(methodPointerType)&Nullable_1_Equals_m19233_gshared/* 1903*/,
	(methodPointerType)&Nullable_1_GetHashCode_m19234_gshared/* 1904*/,
	(methodPointerType)&Nullable_1_ToString_m19235_gshared/* 1905*/,
	(methodPointerType)&Action_3_Invoke_m19347_gshared/* 1906*/,
	(methodPointerType)&Action_3_BeginInvoke_m19349_gshared/* 1907*/,
	(methodPointerType)&Action_3_EndInvoke_m19351_gshared/* 1908*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m19621_gshared/* 1909*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19622_gshared/* 1910*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m19623_gshared/* 1911*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m19624_gshared/* 1912*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m19625_gshared/* 1913*/,
	(methodPointerType)&Action_2_Invoke_m19628_gshared/* 1914*/,
	(methodPointerType)&Action_2_BeginInvoke_m19630_gshared/* 1915*/,
	(methodPointerType)&Action_2_EndInvoke_m19632_gshared/* 1916*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19728_gshared/* 1917*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19729_gshared/* 1918*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19746_gshared/* 1919*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19747_gshared/* 1920*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19754_gshared/* 1921*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19755_gshared/* 1922*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19756_gshared/* 1923*/,
	(methodPointerType)&U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19757_gshared/* 1924*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m19758_gshared/* 1925*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m19759_gshared/* 1926*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m19760_gshared/* 1927*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m19761_gshared/* 1928*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19762_gshared/* 1929*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19763_gshared/* 1930*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19764_gshared/* 1931*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19765_gshared/* 1932*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey18_3__ctor_m19885_gshared/* 1933*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey18_3_U3CU3Em__9_m19886_gshared/* 1934*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey19_3__ctor_m19887_gshared/* 1935*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey19_3_U3CU3Em__C_m19888_gshared/* 1936*/,
	(methodPointerType)&Action_4_Invoke_m19900_gshared/* 1937*/,
	(methodPointerType)&Action_4_BeginInvoke_m19902_gshared/* 1938*/,
	(methodPointerType)&Action_4_EndInvoke_m19904_gshared/* 1939*/,
	(methodPointerType)&HashSet_1__ctor_m20231_gshared/* 1940*/,
	(methodPointerType)&HashSet_1__ctor_m20233_gshared/* 1941*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235_gshared/* 1942*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237_gshared/* 1943*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239_gshared/* 1944*/,
	(methodPointerType)&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared/* 1945*/,
	(methodPointerType)&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243_gshared/* 1946*/,
	(methodPointerType)&HashSet_1_get_Count_m20245_gshared/* 1947*/,
	(methodPointerType)&HashSet_1_Init_m20247_gshared/* 1948*/,
	(methodPointerType)&HashSet_1_InitArrays_m20249_gshared/* 1949*/,
	(methodPointerType)&HashSet_1_SlotsContainsAt_m20251_gshared/* 1950*/,
	(methodPointerType)&HashSet_1_CopyTo_m20253_gshared/* 1951*/,
	(methodPointerType)&HashSet_1_CopyTo_m20255_gshared/* 1952*/,
	(methodPointerType)&HashSet_1_Resize_m20257_gshared/* 1953*/,
	(methodPointerType)&HashSet_1_GetLinkHashCode_m20259_gshared/* 1954*/,
	(methodPointerType)&HashSet_1_GetItemHashCode_m20261_gshared/* 1955*/,
	(methodPointerType)&HashSet_1_Clear_m20264_gshared/* 1956*/,
	(methodPointerType)&HashSet_1_Contains_m20266_gshared/* 1957*/,
	(methodPointerType)&HashSet_1_Remove_m20268_gshared/* 1958*/,
	(methodPointerType)&HashSet_1_GetObjectData_m20270_gshared/* 1959*/,
	(methodPointerType)&HashSet_1_OnDeserialization_m20272_gshared/* 1960*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20273_gshared/* 1961*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20274_gshared/* 1962*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20275_gshared/* 1963*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20276_gshared/* 1964*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20277_gshared/* 1965*/,
	(methodPointerType)&Enumerator__ctor_m20278_gshared/* 1966*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared/* 1967*/,
	(methodPointerType)&Enumerator_MoveNext_m20280_gshared/* 1968*/,
	(methodPointerType)&Enumerator_get_Current_m20281_gshared/* 1969*/,
	(methodPointerType)&Enumerator_Dispose_m20282_gshared/* 1970*/,
	(methodPointerType)&Enumerator_CheckState_m20283_gshared/* 1971*/,
	(methodPointerType)&PrimeHelper__cctor_m20284_gshared/* 1972*/,
	(methodPointerType)&PrimeHelper_TestPrime_m20285_gshared/* 1973*/,
	(methodPointerType)&PrimeHelper_CalcPrime_m20286_gshared/* 1974*/,
	(methodPointerType)&PrimeHelper_ToPrime_m20287_gshared/* 1975*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4B_2__ctor_m20313_gshared/* 1976*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4B_2_U3CU3Em__4F_m20314_gshared/* 1977*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20315_gshared/* 1978*/,
	(methodPointerType)&U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20316_gshared/* 1979*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20342_gshared/* 1980*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20343_gshared/* 1981*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20344_gshared/* 1982*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20345_gshared/* 1983*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20358_gshared/* 1984*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20359_gshared/* 1985*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20360_gshared/* 1986*/,
	(methodPointerType)&U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20361_gshared/* 1987*/,
	(methodPointerType)&Func_2_Invoke_m20370_gshared/* 1988*/,
	(methodPointerType)&Func_2_BeginInvoke_m20372_gshared/* 1989*/,
	(methodPointerType)&Func_2_EndInvoke_m20374_gshared/* 1990*/,
	(methodPointerType)&Action_1_Invoke_m20383_gshared/* 1991*/,
	(methodPointerType)&Action_1_BeginInvoke_m20384_gshared/* 1992*/,
	(methodPointerType)&Action_1_EndInvoke_m20385_gshared/* 1993*/,
	(methodPointerType)&Func_2_Invoke_m20393_gshared/* 1994*/,
	(methodPointerType)&Func_2_BeginInvoke_m20395_gshared/* 1995*/,
	(methodPointerType)&Func_2_EndInvoke_m20397_gshared/* 1996*/,
	(methodPointerType)&Action_2__ctor_m20410_gshared/* 1997*/,
	(methodPointerType)&Action_2_Invoke_m20411_gshared/* 1998*/,
	(methodPointerType)&Action_2_BeginInvoke_m20412_gshared/* 1999*/,
	(methodPointerType)&Action_2_EndInvoke_m20413_gshared/* 2000*/,
	(methodPointerType)&OutMethod_1_Invoke_m20433_gshared/* 2001*/,
	(methodPointerType)&OutMethod_1_BeginInvoke_m20434_gshared/* 2002*/,
	(methodPointerType)&OutMethod_1_EndInvoke_m20435_gshared/* 2003*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20436_gshared/* 2004*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20437_gshared/* 2005*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20438_gshared/* 2006*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20439_gshared/* 2007*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20440_gshared/* 2008*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20441_gshared/* 2009*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20442_gshared/* 2010*/,
	(methodPointerType)&Dictionary_2__ctor_m20445_gshared/* 2011*/,
	(methodPointerType)&Dictionary_2__ctor_m20447_gshared/* 2012*/,
	(methodPointerType)&Dictionary_2__ctor_m20449_gshared/* 2013*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m20451_gshared/* 2014*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m20453_gshared/* 2015*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m20455_gshared/* 2016*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m20457_gshared/* 2017*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m20459_gshared/* 2018*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461_gshared/* 2019*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463_gshared/* 2020*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465_gshared/* 2021*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467_gshared/* 2022*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469_gshared/* 2023*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471_gshared/* 2024*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473_gshared/* 2025*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m20475_gshared/* 2026*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477_gshared/* 2027*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479_gshared/* 2028*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481_gshared/* 2029*/,
	(methodPointerType)&Dictionary_2_get_Count_m20483_gshared/* 2030*/,
	(methodPointerType)&Dictionary_2_get_Item_m20485_gshared/* 2031*/,
	(methodPointerType)&Dictionary_2_set_Item_m20487_gshared/* 2032*/,
	(methodPointerType)&Dictionary_2_Init_m20489_gshared/* 2033*/,
	(methodPointerType)&Dictionary_2_InitArrays_m20491_gshared/* 2034*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m20493_gshared/* 2035*/,
	(methodPointerType)&Dictionary_2_make_pair_m20495_gshared/* 2036*/,
	(methodPointerType)&Dictionary_2_pick_key_m20497_gshared/* 2037*/,
	(methodPointerType)&Dictionary_2_pick_value_m20499_gshared/* 2038*/,
	(methodPointerType)&Dictionary_2_CopyTo_m20501_gshared/* 2039*/,
	(methodPointerType)&Dictionary_2_Resize_m20503_gshared/* 2040*/,
	(methodPointerType)&Dictionary_2_Add_m20505_gshared/* 2041*/,
	(methodPointerType)&Dictionary_2_Clear_m20507_gshared/* 2042*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m20509_gshared/* 2043*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m20511_gshared/* 2044*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m20513_gshared/* 2045*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m20515_gshared/* 2046*/,
	(methodPointerType)&Dictionary_2_Remove_m20517_gshared/* 2047*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m20519_gshared/* 2048*/,
	(methodPointerType)&Dictionary_2_get_Keys_m20521_gshared/* 2049*/,
	(methodPointerType)&Dictionary_2_get_Values_m20523_gshared/* 2050*/,
	(methodPointerType)&Dictionary_2_ToTKey_m20525_gshared/* 2051*/,
	(methodPointerType)&Dictionary_2_ToTValue_m20527_gshared/* 2052*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m20529_gshared/* 2053*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m20531_gshared/* 2054*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m20533_gshared/* 2055*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m20534_gshared/* 2056*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20535_gshared/* 2057*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m20536_gshared/* 2058*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m20537_gshared/* 2059*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m20538_gshared/* 2060*/,
	(methodPointerType)&KeyValuePair_2__ctor_m20539_gshared/* 2061*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m20540_gshared/* 2062*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m20541_gshared/* 2063*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m20542_gshared/* 2064*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m20543_gshared/* 2065*/,
	(methodPointerType)&KeyValuePair_2_ToString_m20544_gshared/* 2066*/,
	(methodPointerType)&KeyCollection__ctor_m20545_gshared/* 2067*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20546_gshared/* 2068*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20547_gshared/* 2069*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20548_gshared/* 2070*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20549_gshared/* 2071*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20550_gshared/* 2072*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m20551_gshared/* 2073*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20552_gshared/* 2074*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20553_gshared/* 2075*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20554_gshared/* 2076*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m20555_gshared/* 2077*/,
	(methodPointerType)&KeyCollection_CopyTo_m20556_gshared/* 2078*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m20557_gshared/* 2079*/,
	(methodPointerType)&KeyCollection_get_Count_m20558_gshared/* 2080*/,
	(methodPointerType)&Enumerator__ctor_m20559_gshared/* 2081*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20560_gshared/* 2082*/,
	(methodPointerType)&Enumerator_Dispose_m20561_gshared/* 2083*/,
	(methodPointerType)&Enumerator_MoveNext_m20562_gshared/* 2084*/,
	(methodPointerType)&Enumerator_get_Current_m20563_gshared/* 2085*/,
	(methodPointerType)&Enumerator__ctor_m20564_gshared/* 2086*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20565_gshared/* 2087*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566_gshared/* 2088*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567_gshared/* 2089*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568_gshared/* 2090*/,
	(methodPointerType)&Enumerator_MoveNext_m20569_gshared/* 2091*/,
	(methodPointerType)&Enumerator_get_Current_m20570_gshared/* 2092*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m20571_gshared/* 2093*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m20572_gshared/* 2094*/,
	(methodPointerType)&Enumerator_VerifyState_m20573_gshared/* 2095*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m20574_gshared/* 2096*/,
	(methodPointerType)&Enumerator_Dispose_m20575_gshared/* 2097*/,
	(methodPointerType)&Transform_1__ctor_m20576_gshared/* 2098*/,
	(methodPointerType)&Transform_1_Invoke_m20577_gshared/* 2099*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20578_gshared/* 2100*/,
	(methodPointerType)&Transform_1_EndInvoke_m20579_gshared/* 2101*/,
	(methodPointerType)&Transform_1__ctor_m20580_gshared/* 2102*/,
	(methodPointerType)&Transform_1_Invoke_m20581_gshared/* 2103*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20582_gshared/* 2104*/,
	(methodPointerType)&Transform_1_EndInvoke_m20583_gshared/* 2105*/,
	(methodPointerType)&ValueCollection__ctor_m20584_gshared/* 2106*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20585_gshared/* 2107*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20586_gshared/* 2108*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20587_gshared/* 2109*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20588_gshared/* 2110*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20589_gshared/* 2111*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m20590_gshared/* 2112*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20591_gshared/* 2113*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20592_gshared/* 2114*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20593_gshared/* 2115*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m20594_gshared/* 2116*/,
	(methodPointerType)&ValueCollection_CopyTo_m20595_gshared/* 2117*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m20596_gshared/* 2118*/,
	(methodPointerType)&ValueCollection_get_Count_m20597_gshared/* 2119*/,
	(methodPointerType)&Enumerator__ctor_m20598_gshared/* 2120*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20599_gshared/* 2121*/,
	(methodPointerType)&Enumerator_Dispose_m20600_gshared/* 2122*/,
	(methodPointerType)&Enumerator_MoveNext_m20601_gshared/* 2123*/,
	(methodPointerType)&Enumerator_get_Current_m20602_gshared/* 2124*/,
	(methodPointerType)&Transform_1__ctor_m20603_gshared/* 2125*/,
	(methodPointerType)&Transform_1_Invoke_m20604_gshared/* 2126*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20605_gshared/* 2127*/,
	(methodPointerType)&Transform_1_EndInvoke_m20606_gshared/* 2128*/,
	(methodPointerType)&Transform_1__ctor_m20607_gshared/* 2129*/,
	(methodPointerType)&Transform_1_Invoke_m20608_gshared/* 2130*/,
	(methodPointerType)&Transform_1_BeginInvoke_m20609_gshared/* 2131*/,
	(methodPointerType)&Transform_1_EndInvoke_m20610_gshared/* 2132*/,
	(methodPointerType)&ShimEnumerator__ctor_m20611_gshared/* 2133*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m20612_gshared/* 2134*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m20613_gshared/* 2135*/,
	(methodPointerType)&ShimEnumerator_get_Key_m20614_gshared/* 2136*/,
	(methodPointerType)&ShimEnumerator_get_Value_m20615_gshared/* 2137*/,
	(methodPointerType)&ShimEnumerator_get_Current_m20616_gshared/* 2138*/,
	(methodPointerType)&OutMethod_1_Invoke_m20667_gshared/* 2139*/,
	(methodPointerType)&OutMethod_1_BeginInvoke_m20668_gshared/* 2140*/,
	(methodPointerType)&OutMethod_1_EndInvoke_m20669_gshared/* 2141*/,
	(methodPointerType)&Func_2_Invoke_m20703_gshared/* 2142*/,
	(methodPointerType)&Func_2_BeginInvoke_m20705_gshared/* 2143*/,
	(methodPointerType)&Func_2_EndInvoke_m20707_gshared/* 2144*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m20720_gshared/* 2145*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m20721_gshared/* 2146*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m20722_gshared/* 2147*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m20723_gshared/* 2148*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m20724_gshared/* 2149*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m20725_gshared/* 2150*/,
	(methodPointerType)&U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m20726_gshared/* 2151*/,
	(methodPointerType)&List_1__ctor_m20727_gshared/* 2152*/,
	(methodPointerType)&List_1__ctor_m20728_gshared/* 2153*/,
	(methodPointerType)&List_1__ctor_m20729_gshared/* 2154*/,
	(methodPointerType)&List_1__cctor_m20730_gshared/* 2155*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20731_gshared/* 2156*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m20732_gshared/* 2157*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m20733_gshared/* 2158*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m20734_gshared/* 2159*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m20735_gshared/* 2160*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m20736_gshared/* 2161*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m20737_gshared/* 2162*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m20738_gshared/* 2163*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20739_gshared/* 2164*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m20740_gshared/* 2165*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m20741_gshared/* 2166*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m20742_gshared/* 2167*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m20743_gshared/* 2168*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m20744_gshared/* 2169*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m20745_gshared/* 2170*/,
	(methodPointerType)&List_1_Add_m20746_gshared/* 2171*/,
	(methodPointerType)&List_1_GrowIfNeeded_m20747_gshared/* 2172*/,
	(methodPointerType)&List_1_AddCollection_m20748_gshared/* 2173*/,
	(methodPointerType)&List_1_AddEnumerable_m20749_gshared/* 2174*/,
	(methodPointerType)&List_1_AddRange_m20750_gshared/* 2175*/,
	(methodPointerType)&List_1_AsReadOnly_m20751_gshared/* 2176*/,
	(methodPointerType)&List_1_Clear_m20752_gshared/* 2177*/,
	(methodPointerType)&List_1_Contains_m20753_gshared/* 2178*/,
	(methodPointerType)&List_1_CopyTo_m20754_gshared/* 2179*/,
	(methodPointerType)&List_1_Find_m20755_gshared/* 2180*/,
	(methodPointerType)&List_1_CheckMatch_m20756_gshared/* 2181*/,
	(methodPointerType)&List_1_FindIndex_m20757_gshared/* 2182*/,
	(methodPointerType)&List_1_GetIndex_m20758_gshared/* 2183*/,
	(methodPointerType)&List_1_ForEach_m20759_gshared/* 2184*/,
	(methodPointerType)&List_1_GetEnumerator_m20760_gshared/* 2185*/,
	(methodPointerType)&List_1_IndexOf_m20761_gshared/* 2186*/,
	(methodPointerType)&List_1_Shift_m20762_gshared/* 2187*/,
	(methodPointerType)&List_1_CheckIndex_m20763_gshared/* 2188*/,
	(methodPointerType)&List_1_Insert_m20764_gshared/* 2189*/,
	(methodPointerType)&List_1_CheckCollection_m20765_gshared/* 2190*/,
	(methodPointerType)&List_1_Remove_m20766_gshared/* 2191*/,
	(methodPointerType)&List_1_RemoveAll_m20767_gshared/* 2192*/,
	(methodPointerType)&List_1_RemoveAt_m20768_gshared/* 2193*/,
	(methodPointerType)&List_1_Reverse_m20769_gshared/* 2194*/,
	(methodPointerType)&List_1_Sort_m20770_gshared/* 2195*/,
	(methodPointerType)&List_1_Sort_m20771_gshared/* 2196*/,
	(methodPointerType)&List_1_ToArray_m20772_gshared/* 2197*/,
	(methodPointerType)&List_1_TrimExcess_m20773_gshared/* 2198*/,
	(methodPointerType)&List_1_get_Capacity_m20774_gshared/* 2199*/,
	(methodPointerType)&List_1_set_Capacity_m20775_gshared/* 2200*/,
	(methodPointerType)&List_1_get_Count_m20776_gshared/* 2201*/,
	(methodPointerType)&List_1_get_Item_m20777_gshared/* 2202*/,
	(methodPointerType)&List_1_set_Item_m20778_gshared/* 2203*/,
	(methodPointerType)&Enumerator__ctor_m20779_gshared/* 2204*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m20780_gshared/* 2205*/,
	(methodPointerType)&Enumerator_Dispose_m20781_gshared/* 2206*/,
	(methodPointerType)&Enumerator_VerifyState_m20782_gshared/* 2207*/,
	(methodPointerType)&Enumerator_MoveNext_m20783_gshared/* 2208*/,
	(methodPointerType)&Enumerator_get_Current_m20784_gshared/* 2209*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m20785_gshared/* 2210*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20786_gshared/* 2211*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20787_gshared/* 2212*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20788_gshared/* 2213*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20789_gshared/* 2214*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20790_gshared/* 2215*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20791_gshared/* 2216*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20792_gshared/* 2217*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20793_gshared/* 2218*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20794_gshared/* 2219*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20795_gshared/* 2220*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m20796_gshared/* 2221*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m20797_gshared/* 2222*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m20798_gshared/* 2223*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20799_gshared/* 2224*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m20800_gshared/* 2225*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m20801_gshared/* 2226*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20802_gshared/* 2227*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20803_gshared/* 2228*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20804_gshared/* 2229*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20805_gshared/* 2230*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20806_gshared/* 2231*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m20807_gshared/* 2232*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m20808_gshared/* 2233*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m20809_gshared/* 2234*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m20810_gshared/* 2235*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m20811_gshared/* 2236*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m20812_gshared/* 2237*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m20813_gshared/* 2238*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m20814_gshared/* 2239*/,
	(methodPointerType)&Collection_1__ctor_m20815_gshared/* 2240*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20816_gshared/* 2241*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m20817_gshared/* 2242*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m20818_gshared/* 2243*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m20819_gshared/* 2244*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m20820_gshared/* 2245*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m20821_gshared/* 2246*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m20822_gshared/* 2247*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m20823_gshared/* 2248*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m20824_gshared/* 2249*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m20825_gshared/* 2250*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m20826_gshared/* 2251*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m20827_gshared/* 2252*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m20828_gshared/* 2253*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m20829_gshared/* 2254*/,
	(methodPointerType)&Collection_1_Add_m20830_gshared/* 2255*/,
	(methodPointerType)&Collection_1_Clear_m20831_gshared/* 2256*/,
	(methodPointerType)&Collection_1_ClearItems_m20832_gshared/* 2257*/,
	(methodPointerType)&Collection_1_Contains_m20833_gshared/* 2258*/,
	(methodPointerType)&Collection_1_CopyTo_m20834_gshared/* 2259*/,
	(methodPointerType)&Collection_1_GetEnumerator_m20835_gshared/* 2260*/,
	(methodPointerType)&Collection_1_IndexOf_m20836_gshared/* 2261*/,
	(methodPointerType)&Collection_1_Insert_m20837_gshared/* 2262*/,
	(methodPointerType)&Collection_1_InsertItem_m20838_gshared/* 2263*/,
	(methodPointerType)&Collection_1_Remove_m20839_gshared/* 2264*/,
	(methodPointerType)&Collection_1_RemoveAt_m20840_gshared/* 2265*/,
	(methodPointerType)&Collection_1_RemoveItem_m20841_gshared/* 2266*/,
	(methodPointerType)&Collection_1_get_Count_m20842_gshared/* 2267*/,
	(methodPointerType)&Collection_1_get_Item_m20843_gshared/* 2268*/,
	(methodPointerType)&Collection_1_set_Item_m20844_gshared/* 2269*/,
	(methodPointerType)&Collection_1_SetItem_m20845_gshared/* 2270*/,
	(methodPointerType)&Collection_1_IsValidItem_m20846_gshared/* 2271*/,
	(methodPointerType)&Collection_1_ConvertItem_m20847_gshared/* 2272*/,
	(methodPointerType)&Collection_1_CheckWritable_m20848_gshared/* 2273*/,
	(methodPointerType)&Collection_1_IsSynchronized_m20849_gshared/* 2274*/,
	(methodPointerType)&Collection_1_IsFixedSize_m20850_gshared/* 2275*/,
	(methodPointerType)&EqualityComparer_1__ctor_m20851_gshared/* 2276*/,
	(methodPointerType)&EqualityComparer_1__cctor_m20852_gshared/* 2277*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20853_gshared/* 2278*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20854_gshared/* 2279*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m20855_gshared/* 2280*/,
	(methodPointerType)&DefaultComparer__ctor_m20856_gshared/* 2281*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m20857_gshared/* 2282*/,
	(methodPointerType)&DefaultComparer_Equals_m20858_gshared/* 2283*/,
	(methodPointerType)&Predicate_1__ctor_m20859_gshared/* 2284*/,
	(methodPointerType)&Predicate_1_Invoke_m20860_gshared/* 2285*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m20861_gshared/* 2286*/,
	(methodPointerType)&Predicate_1_EndInvoke_m20862_gshared/* 2287*/,
	(methodPointerType)&Comparer_1__ctor_m20863_gshared/* 2288*/,
	(methodPointerType)&Comparer_1__cctor_m20864_gshared/* 2289*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m20865_gshared/* 2290*/,
	(methodPointerType)&Comparer_1_get_Default_m20866_gshared/* 2291*/,
	(methodPointerType)&DefaultComparer__ctor_m20867_gshared/* 2292*/,
	(methodPointerType)&DefaultComparer_Compare_m20868_gshared/* 2293*/,
	(methodPointerType)&Comparison_1__ctor_m20869_gshared/* 2294*/,
	(methodPointerType)&Comparison_1_Invoke_m20870_gshared/* 2295*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m20871_gshared/* 2296*/,
	(methodPointerType)&Comparison_1_EndInvoke_m20872_gshared/* 2297*/,
	(methodPointerType)&UnityAction_1_Invoke_m20908_gshared/* 2298*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m20910_gshared/* 2299*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m20912_gshared/* 2300*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m20916_gshared/* 2301*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m20920_gshared/* 2302*/,
	(methodPointerType)&InvokableCall_1__ctor_m20922_gshared/* 2303*/,
	(methodPointerType)&InvokableCall_1__ctor_m20923_gshared/* 2304*/,
	(methodPointerType)&InvokableCall_1_Invoke_m20924_gshared/* 2305*/,
	(methodPointerType)&InvokableCall_1_Find_m20925_gshared/* 2306*/,
	(methodPointerType)&List_1__ctor_m21030_gshared/* 2307*/,
	(methodPointerType)&List_1__ctor_m21031_gshared/* 2308*/,
	(methodPointerType)&List_1__cctor_m21032_gshared/* 2309*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21033_gshared/* 2310*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m21034_gshared/* 2311*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m21035_gshared/* 2312*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m21036_gshared/* 2313*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m21037_gshared/* 2314*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m21038_gshared/* 2315*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m21039_gshared/* 2316*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m21040_gshared/* 2317*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21041_gshared/* 2318*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m21042_gshared/* 2319*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m21043_gshared/* 2320*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m21044_gshared/* 2321*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m21045_gshared/* 2322*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m21046_gshared/* 2323*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m21047_gshared/* 2324*/,
	(methodPointerType)&List_1_Add_m21048_gshared/* 2325*/,
	(methodPointerType)&List_1_GrowIfNeeded_m21049_gshared/* 2326*/,
	(methodPointerType)&List_1_AddCollection_m21050_gshared/* 2327*/,
	(methodPointerType)&List_1_AddEnumerable_m21051_gshared/* 2328*/,
	(methodPointerType)&List_1_AddRange_m21052_gshared/* 2329*/,
	(methodPointerType)&List_1_AsReadOnly_m21053_gshared/* 2330*/,
	(methodPointerType)&List_1_Clear_m21054_gshared/* 2331*/,
	(methodPointerType)&List_1_Contains_m21055_gshared/* 2332*/,
	(methodPointerType)&List_1_CopyTo_m21056_gshared/* 2333*/,
	(methodPointerType)&List_1_Find_m21057_gshared/* 2334*/,
	(methodPointerType)&List_1_CheckMatch_m21058_gshared/* 2335*/,
	(methodPointerType)&List_1_FindIndex_m21059_gshared/* 2336*/,
	(methodPointerType)&List_1_GetIndex_m21060_gshared/* 2337*/,
	(methodPointerType)&List_1_ForEach_m21061_gshared/* 2338*/,
	(methodPointerType)&List_1_GetEnumerator_m21062_gshared/* 2339*/,
	(methodPointerType)&List_1_IndexOf_m21063_gshared/* 2340*/,
	(methodPointerType)&List_1_Shift_m21064_gshared/* 2341*/,
	(methodPointerType)&List_1_CheckIndex_m21065_gshared/* 2342*/,
	(methodPointerType)&List_1_Insert_m21066_gshared/* 2343*/,
	(methodPointerType)&List_1_CheckCollection_m21067_gshared/* 2344*/,
	(methodPointerType)&List_1_Remove_m21068_gshared/* 2345*/,
	(methodPointerType)&List_1_RemoveAll_m21069_gshared/* 2346*/,
	(methodPointerType)&List_1_RemoveAt_m21070_gshared/* 2347*/,
	(methodPointerType)&List_1_Reverse_m21071_gshared/* 2348*/,
	(methodPointerType)&List_1_Sort_m21072_gshared/* 2349*/,
	(methodPointerType)&List_1_Sort_m21073_gshared/* 2350*/,
	(methodPointerType)&List_1_ToArray_m21074_gshared/* 2351*/,
	(methodPointerType)&List_1_TrimExcess_m21075_gshared/* 2352*/,
	(methodPointerType)&List_1_get_Capacity_m21076_gshared/* 2353*/,
	(methodPointerType)&List_1_set_Capacity_m21077_gshared/* 2354*/,
	(methodPointerType)&List_1_get_Count_m21078_gshared/* 2355*/,
	(methodPointerType)&List_1_get_Item_m21079_gshared/* 2356*/,
	(methodPointerType)&List_1_set_Item_m21080_gshared/* 2357*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21081_gshared/* 2358*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21082_gshared/* 2359*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21083_gshared/* 2360*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21084_gshared/* 2361*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21085_gshared/* 2362*/,
	(methodPointerType)&Enumerator__ctor_m21086_gshared/* 2363*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21087_gshared/* 2364*/,
	(methodPointerType)&Enumerator_Dispose_m21088_gshared/* 2365*/,
	(methodPointerType)&Enumerator_VerifyState_m21089_gshared/* 2366*/,
	(methodPointerType)&Enumerator_MoveNext_m21090_gshared/* 2367*/,
	(methodPointerType)&Enumerator_get_Current_m21091_gshared/* 2368*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m21092_gshared/* 2369*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21093_gshared/* 2370*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21094_gshared/* 2371*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21095_gshared/* 2372*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21096_gshared/* 2373*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21097_gshared/* 2374*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21098_gshared/* 2375*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21099_gshared/* 2376*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21100_gshared/* 2377*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21101_gshared/* 2378*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21102_gshared/* 2379*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m21103_gshared/* 2380*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m21104_gshared/* 2381*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m21105_gshared/* 2382*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21106_gshared/* 2383*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m21107_gshared/* 2384*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m21108_gshared/* 2385*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21109_gshared/* 2386*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21110_gshared/* 2387*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21111_gshared/* 2388*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21112_gshared/* 2389*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21113_gshared/* 2390*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m21114_gshared/* 2391*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m21115_gshared/* 2392*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m21116_gshared/* 2393*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m21117_gshared/* 2394*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m21118_gshared/* 2395*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m21119_gshared/* 2396*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m21120_gshared/* 2397*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m21121_gshared/* 2398*/,
	(methodPointerType)&Collection_1__ctor_m21122_gshared/* 2399*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21123_gshared/* 2400*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m21124_gshared/* 2401*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m21125_gshared/* 2402*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m21126_gshared/* 2403*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m21127_gshared/* 2404*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m21128_gshared/* 2405*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m21129_gshared/* 2406*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m21130_gshared/* 2407*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m21131_gshared/* 2408*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m21132_gshared/* 2409*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m21133_gshared/* 2410*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m21134_gshared/* 2411*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m21135_gshared/* 2412*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m21136_gshared/* 2413*/,
	(methodPointerType)&Collection_1_Add_m21137_gshared/* 2414*/,
	(methodPointerType)&Collection_1_Clear_m21138_gshared/* 2415*/,
	(methodPointerType)&Collection_1_ClearItems_m21139_gshared/* 2416*/,
	(methodPointerType)&Collection_1_Contains_m21140_gshared/* 2417*/,
	(methodPointerType)&Collection_1_CopyTo_m21141_gshared/* 2418*/,
	(methodPointerType)&Collection_1_GetEnumerator_m21142_gshared/* 2419*/,
	(methodPointerType)&Collection_1_IndexOf_m21143_gshared/* 2420*/,
	(methodPointerType)&Collection_1_Insert_m21144_gshared/* 2421*/,
	(methodPointerType)&Collection_1_InsertItem_m21145_gshared/* 2422*/,
	(methodPointerType)&Collection_1_Remove_m21146_gshared/* 2423*/,
	(methodPointerType)&Collection_1_RemoveAt_m21147_gshared/* 2424*/,
	(methodPointerType)&Collection_1_RemoveItem_m21148_gshared/* 2425*/,
	(methodPointerType)&Collection_1_get_Count_m21149_gshared/* 2426*/,
	(methodPointerType)&Collection_1_get_Item_m21150_gshared/* 2427*/,
	(methodPointerType)&Collection_1_set_Item_m21151_gshared/* 2428*/,
	(methodPointerType)&Collection_1_SetItem_m21152_gshared/* 2429*/,
	(methodPointerType)&Collection_1_IsValidItem_m21153_gshared/* 2430*/,
	(methodPointerType)&Collection_1_ConvertItem_m21154_gshared/* 2431*/,
	(methodPointerType)&Collection_1_CheckWritable_m21155_gshared/* 2432*/,
	(methodPointerType)&Collection_1_IsSynchronized_m21156_gshared/* 2433*/,
	(methodPointerType)&Collection_1_IsFixedSize_m21157_gshared/* 2434*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21158_gshared/* 2435*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21159_gshared/* 2436*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21160_gshared/* 2437*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21161_gshared/* 2438*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21162_gshared/* 2439*/,
	(methodPointerType)&DefaultComparer__ctor_m21163_gshared/* 2440*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21164_gshared/* 2441*/,
	(methodPointerType)&DefaultComparer_Equals_m21165_gshared/* 2442*/,
	(methodPointerType)&Predicate_1__ctor_m21166_gshared/* 2443*/,
	(methodPointerType)&Predicate_1_Invoke_m21167_gshared/* 2444*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m21168_gshared/* 2445*/,
	(methodPointerType)&Predicate_1_EndInvoke_m21169_gshared/* 2446*/,
	(methodPointerType)&Action_1__ctor_m21170_gshared/* 2447*/,
	(methodPointerType)&Action_1_Invoke_m21171_gshared/* 2448*/,
	(methodPointerType)&Action_1_BeginInvoke_m21172_gshared/* 2449*/,
	(methodPointerType)&Action_1_EndInvoke_m21173_gshared/* 2450*/,
	(methodPointerType)&Comparer_1__ctor_m21174_gshared/* 2451*/,
	(methodPointerType)&Comparer_1__cctor_m21175_gshared/* 2452*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m21176_gshared/* 2453*/,
	(methodPointerType)&Comparer_1_get_Default_m21177_gshared/* 2454*/,
	(methodPointerType)&DefaultComparer__ctor_m21178_gshared/* 2455*/,
	(methodPointerType)&DefaultComparer_Compare_m21179_gshared/* 2456*/,
	(methodPointerType)&Comparison_1__ctor_m21180_gshared/* 2457*/,
	(methodPointerType)&Comparison_1_Invoke_m21181_gshared/* 2458*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m21182_gshared/* 2459*/,
	(methodPointerType)&Comparison_1_EndInvoke_m21183_gshared/* 2460*/,
	(methodPointerType)&List_1__ctor_m21282_gshared/* 2461*/,
	(methodPointerType)&List_1__ctor_m21283_gshared/* 2462*/,
	(methodPointerType)&List_1__cctor_m21284_gshared/* 2463*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21285_gshared/* 2464*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m21286_gshared/* 2465*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m21287_gshared/* 2466*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m21288_gshared/* 2467*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m21289_gshared/* 2468*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m21290_gshared/* 2469*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m21291_gshared/* 2470*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m21292_gshared/* 2471*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21293_gshared/* 2472*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m21294_gshared/* 2473*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m21295_gshared/* 2474*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m21296_gshared/* 2475*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m21297_gshared/* 2476*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m21298_gshared/* 2477*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m21299_gshared/* 2478*/,
	(methodPointerType)&List_1_Add_m21300_gshared/* 2479*/,
	(methodPointerType)&List_1_GrowIfNeeded_m21301_gshared/* 2480*/,
	(methodPointerType)&List_1_AddCollection_m21302_gshared/* 2481*/,
	(methodPointerType)&List_1_AddEnumerable_m21303_gshared/* 2482*/,
	(methodPointerType)&List_1_AddRange_m21304_gshared/* 2483*/,
	(methodPointerType)&List_1_AsReadOnly_m21305_gshared/* 2484*/,
	(methodPointerType)&List_1_Clear_m21306_gshared/* 2485*/,
	(methodPointerType)&List_1_Contains_m21307_gshared/* 2486*/,
	(methodPointerType)&List_1_CopyTo_m21308_gshared/* 2487*/,
	(methodPointerType)&List_1_Find_m21309_gshared/* 2488*/,
	(methodPointerType)&List_1_CheckMatch_m21310_gshared/* 2489*/,
	(methodPointerType)&List_1_FindIndex_m21311_gshared/* 2490*/,
	(methodPointerType)&List_1_GetIndex_m21312_gshared/* 2491*/,
	(methodPointerType)&List_1_ForEach_m21313_gshared/* 2492*/,
	(methodPointerType)&List_1_GetEnumerator_m21314_gshared/* 2493*/,
	(methodPointerType)&List_1_IndexOf_m21315_gshared/* 2494*/,
	(methodPointerType)&List_1_Shift_m21316_gshared/* 2495*/,
	(methodPointerType)&List_1_CheckIndex_m21317_gshared/* 2496*/,
	(methodPointerType)&List_1_Insert_m21318_gshared/* 2497*/,
	(methodPointerType)&List_1_CheckCollection_m21319_gshared/* 2498*/,
	(methodPointerType)&List_1_Remove_m21320_gshared/* 2499*/,
	(methodPointerType)&List_1_RemoveAll_m21321_gshared/* 2500*/,
	(methodPointerType)&List_1_RemoveAt_m21322_gshared/* 2501*/,
	(methodPointerType)&List_1_Reverse_m21323_gshared/* 2502*/,
	(methodPointerType)&List_1_Sort_m21324_gshared/* 2503*/,
	(methodPointerType)&List_1_Sort_m21325_gshared/* 2504*/,
	(methodPointerType)&List_1_ToArray_m21326_gshared/* 2505*/,
	(methodPointerType)&List_1_TrimExcess_m21327_gshared/* 2506*/,
	(methodPointerType)&List_1_get_Capacity_m21328_gshared/* 2507*/,
	(methodPointerType)&List_1_set_Capacity_m21329_gshared/* 2508*/,
	(methodPointerType)&List_1_get_Count_m21330_gshared/* 2509*/,
	(methodPointerType)&List_1_get_Item_m21331_gshared/* 2510*/,
	(methodPointerType)&List_1_set_Item_m21332_gshared/* 2511*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21333_gshared/* 2512*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21334_gshared/* 2513*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21335_gshared/* 2514*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21336_gshared/* 2515*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21337_gshared/* 2516*/,
	(methodPointerType)&Enumerator__ctor_m21338_gshared/* 2517*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21339_gshared/* 2518*/,
	(methodPointerType)&Enumerator_Dispose_m21340_gshared/* 2519*/,
	(methodPointerType)&Enumerator_VerifyState_m21341_gshared/* 2520*/,
	(methodPointerType)&Enumerator_MoveNext_m21342_gshared/* 2521*/,
	(methodPointerType)&Enumerator_get_Current_m21343_gshared/* 2522*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m21344_gshared/* 2523*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21345_gshared/* 2524*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21346_gshared/* 2525*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21347_gshared/* 2526*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21348_gshared/* 2527*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21349_gshared/* 2528*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21350_gshared/* 2529*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21351_gshared/* 2530*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21352_gshared/* 2531*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21353_gshared/* 2532*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21354_gshared/* 2533*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m21355_gshared/* 2534*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m21356_gshared/* 2535*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m21357_gshared/* 2536*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21358_gshared/* 2537*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m21359_gshared/* 2538*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m21360_gshared/* 2539*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21361_gshared/* 2540*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21362_gshared/* 2541*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21363_gshared/* 2542*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21364_gshared/* 2543*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21365_gshared/* 2544*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m21366_gshared/* 2545*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m21367_gshared/* 2546*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m21368_gshared/* 2547*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m21369_gshared/* 2548*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m21370_gshared/* 2549*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m21371_gshared/* 2550*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m21372_gshared/* 2551*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m21373_gshared/* 2552*/,
	(methodPointerType)&Collection_1__ctor_m21374_gshared/* 2553*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21375_gshared/* 2554*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m21376_gshared/* 2555*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m21377_gshared/* 2556*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m21378_gshared/* 2557*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m21379_gshared/* 2558*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m21380_gshared/* 2559*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m21381_gshared/* 2560*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m21382_gshared/* 2561*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m21383_gshared/* 2562*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m21384_gshared/* 2563*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m21385_gshared/* 2564*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m21386_gshared/* 2565*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m21387_gshared/* 2566*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m21388_gshared/* 2567*/,
	(methodPointerType)&Collection_1_Add_m21389_gshared/* 2568*/,
	(methodPointerType)&Collection_1_Clear_m21390_gshared/* 2569*/,
	(methodPointerType)&Collection_1_ClearItems_m21391_gshared/* 2570*/,
	(methodPointerType)&Collection_1_Contains_m21392_gshared/* 2571*/,
	(methodPointerType)&Collection_1_CopyTo_m21393_gshared/* 2572*/,
	(methodPointerType)&Collection_1_GetEnumerator_m21394_gshared/* 2573*/,
	(methodPointerType)&Collection_1_IndexOf_m21395_gshared/* 2574*/,
	(methodPointerType)&Collection_1_Insert_m21396_gshared/* 2575*/,
	(methodPointerType)&Collection_1_InsertItem_m21397_gshared/* 2576*/,
	(methodPointerType)&Collection_1_Remove_m21398_gshared/* 2577*/,
	(methodPointerType)&Collection_1_RemoveAt_m21399_gshared/* 2578*/,
	(methodPointerType)&Collection_1_RemoveItem_m21400_gshared/* 2579*/,
	(methodPointerType)&Collection_1_get_Count_m21401_gshared/* 2580*/,
	(methodPointerType)&Collection_1_get_Item_m21402_gshared/* 2581*/,
	(methodPointerType)&Collection_1_set_Item_m21403_gshared/* 2582*/,
	(methodPointerType)&Collection_1_SetItem_m21404_gshared/* 2583*/,
	(methodPointerType)&Collection_1_IsValidItem_m21405_gshared/* 2584*/,
	(methodPointerType)&Collection_1_ConvertItem_m21406_gshared/* 2585*/,
	(methodPointerType)&Collection_1_CheckWritable_m21407_gshared/* 2586*/,
	(methodPointerType)&Collection_1_IsSynchronized_m21408_gshared/* 2587*/,
	(methodPointerType)&Collection_1_IsFixedSize_m21409_gshared/* 2588*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21410_gshared/* 2589*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21411_gshared/* 2590*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21412_gshared/* 2591*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21413_gshared/* 2592*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21414_gshared/* 2593*/,
	(methodPointerType)&DefaultComparer__ctor_m21415_gshared/* 2594*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21416_gshared/* 2595*/,
	(methodPointerType)&DefaultComparer_Equals_m21417_gshared/* 2596*/,
	(methodPointerType)&Predicate_1__ctor_m21418_gshared/* 2597*/,
	(methodPointerType)&Predicate_1_Invoke_m21419_gshared/* 2598*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m21420_gshared/* 2599*/,
	(methodPointerType)&Predicate_1_EndInvoke_m21421_gshared/* 2600*/,
	(methodPointerType)&Action_1__ctor_m21422_gshared/* 2601*/,
	(methodPointerType)&Action_1_Invoke_m21423_gshared/* 2602*/,
	(methodPointerType)&Action_1_BeginInvoke_m21424_gshared/* 2603*/,
	(methodPointerType)&Action_1_EndInvoke_m21425_gshared/* 2604*/,
	(methodPointerType)&Comparer_1__ctor_m21426_gshared/* 2605*/,
	(methodPointerType)&Comparer_1__cctor_m21427_gshared/* 2606*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m21428_gshared/* 2607*/,
	(methodPointerType)&Comparer_1_get_Default_m21429_gshared/* 2608*/,
	(methodPointerType)&DefaultComparer__ctor_m21430_gshared/* 2609*/,
	(methodPointerType)&DefaultComparer_Compare_m21431_gshared/* 2610*/,
	(methodPointerType)&Comparison_1__ctor_m21432_gshared/* 2611*/,
	(methodPointerType)&Comparison_1_Invoke_m21433_gshared/* 2612*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m21434_gshared/* 2613*/,
	(methodPointerType)&Comparison_1_EndInvoke_m21435_gshared/* 2614*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21441_gshared/* 2615*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21442_gshared/* 2616*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21443_gshared/* 2617*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21444_gshared/* 2618*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21445_gshared/* 2619*/,
	(methodPointerType)&Comparison_1_Invoke_m21545_gshared/* 2620*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m21546_gshared/* 2621*/,
	(methodPointerType)&Comparison_1_EndInvoke_m21547_gshared/* 2622*/,
	(methodPointerType)&List_1__ctor_m21803_gshared/* 2623*/,
	(methodPointerType)&List_1__ctor_m21804_gshared/* 2624*/,
	(methodPointerType)&List_1__cctor_m21805_gshared/* 2625*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21806_gshared/* 2626*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m21807_gshared/* 2627*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m21808_gshared/* 2628*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m21809_gshared/* 2629*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m21810_gshared/* 2630*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m21811_gshared/* 2631*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m21812_gshared/* 2632*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m21813_gshared/* 2633*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21814_gshared/* 2634*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m21815_gshared/* 2635*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m21816_gshared/* 2636*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m21817_gshared/* 2637*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m21818_gshared/* 2638*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m21819_gshared/* 2639*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m21820_gshared/* 2640*/,
	(methodPointerType)&List_1_Add_m21821_gshared/* 2641*/,
	(methodPointerType)&List_1_GrowIfNeeded_m21822_gshared/* 2642*/,
	(methodPointerType)&List_1_AddCollection_m21823_gshared/* 2643*/,
	(methodPointerType)&List_1_AddEnumerable_m21824_gshared/* 2644*/,
	(methodPointerType)&List_1_AddRange_m21825_gshared/* 2645*/,
	(methodPointerType)&List_1_AsReadOnly_m21826_gshared/* 2646*/,
	(methodPointerType)&List_1_Clear_m21827_gshared/* 2647*/,
	(methodPointerType)&List_1_Contains_m21828_gshared/* 2648*/,
	(methodPointerType)&List_1_CopyTo_m21829_gshared/* 2649*/,
	(methodPointerType)&List_1_Find_m21830_gshared/* 2650*/,
	(methodPointerType)&List_1_CheckMatch_m21831_gshared/* 2651*/,
	(methodPointerType)&List_1_FindIndex_m21832_gshared/* 2652*/,
	(methodPointerType)&List_1_GetIndex_m21833_gshared/* 2653*/,
	(methodPointerType)&List_1_ForEach_m21834_gshared/* 2654*/,
	(methodPointerType)&List_1_GetEnumerator_m21835_gshared/* 2655*/,
	(methodPointerType)&List_1_IndexOf_m21836_gshared/* 2656*/,
	(methodPointerType)&List_1_Shift_m21837_gshared/* 2657*/,
	(methodPointerType)&List_1_CheckIndex_m21838_gshared/* 2658*/,
	(methodPointerType)&List_1_Insert_m21839_gshared/* 2659*/,
	(methodPointerType)&List_1_CheckCollection_m21840_gshared/* 2660*/,
	(methodPointerType)&List_1_Remove_m21841_gshared/* 2661*/,
	(methodPointerType)&List_1_RemoveAll_m21842_gshared/* 2662*/,
	(methodPointerType)&List_1_RemoveAt_m21843_gshared/* 2663*/,
	(methodPointerType)&List_1_Reverse_m21844_gshared/* 2664*/,
	(methodPointerType)&List_1_Sort_m21845_gshared/* 2665*/,
	(methodPointerType)&List_1_ToArray_m21846_gshared/* 2666*/,
	(methodPointerType)&List_1_TrimExcess_m21847_gshared/* 2667*/,
	(methodPointerType)&List_1_get_Capacity_m21848_gshared/* 2668*/,
	(methodPointerType)&List_1_set_Capacity_m21849_gshared/* 2669*/,
	(methodPointerType)&List_1_get_Count_m21850_gshared/* 2670*/,
	(methodPointerType)&List_1_get_Item_m21851_gshared/* 2671*/,
	(methodPointerType)&List_1_set_Item_m21852_gshared/* 2672*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m21853_gshared/* 2673*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21854_gshared/* 2674*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m21855_gshared/* 2675*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m21856_gshared/* 2676*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m21857_gshared/* 2677*/,
	(methodPointerType)&Enumerator__ctor_m21858_gshared/* 2678*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m21859_gshared/* 2679*/,
	(methodPointerType)&Enumerator_Dispose_m21860_gshared/* 2680*/,
	(methodPointerType)&Enumerator_VerifyState_m21861_gshared/* 2681*/,
	(methodPointerType)&Enumerator_MoveNext_m21862_gshared/* 2682*/,
	(methodPointerType)&Enumerator_get_Current_m21863_gshared/* 2683*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m21864_gshared/* 2684*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21865_gshared/* 2685*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21866_gshared/* 2686*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21867_gshared/* 2687*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21868_gshared/* 2688*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21869_gshared/* 2689*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21870_gshared/* 2690*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21871_gshared/* 2691*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21872_gshared/* 2692*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21873_gshared/* 2693*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21874_gshared/* 2694*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m21875_gshared/* 2695*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m21876_gshared/* 2696*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m21877_gshared/* 2697*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21878_gshared/* 2698*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m21879_gshared/* 2699*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m21880_gshared/* 2700*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21881_gshared/* 2701*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21882_gshared/* 2702*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21883_gshared/* 2703*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21884_gshared/* 2704*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21885_gshared/* 2705*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m21886_gshared/* 2706*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m21887_gshared/* 2707*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m21888_gshared/* 2708*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m21889_gshared/* 2709*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m21890_gshared/* 2710*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m21891_gshared/* 2711*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m21892_gshared/* 2712*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m21893_gshared/* 2713*/,
	(methodPointerType)&Collection_1__ctor_m21894_gshared/* 2714*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21895_gshared/* 2715*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m21896_gshared/* 2716*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m21897_gshared/* 2717*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m21898_gshared/* 2718*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m21899_gshared/* 2719*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m21900_gshared/* 2720*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m21901_gshared/* 2721*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m21902_gshared/* 2722*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m21903_gshared/* 2723*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m21904_gshared/* 2724*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m21905_gshared/* 2725*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m21906_gshared/* 2726*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m21907_gshared/* 2727*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m21908_gshared/* 2728*/,
	(methodPointerType)&Collection_1_Add_m21909_gshared/* 2729*/,
	(methodPointerType)&Collection_1_Clear_m21910_gshared/* 2730*/,
	(methodPointerType)&Collection_1_ClearItems_m21911_gshared/* 2731*/,
	(methodPointerType)&Collection_1_Contains_m21912_gshared/* 2732*/,
	(methodPointerType)&Collection_1_CopyTo_m21913_gshared/* 2733*/,
	(methodPointerType)&Collection_1_GetEnumerator_m21914_gshared/* 2734*/,
	(methodPointerType)&Collection_1_IndexOf_m21915_gshared/* 2735*/,
	(methodPointerType)&Collection_1_Insert_m21916_gshared/* 2736*/,
	(methodPointerType)&Collection_1_InsertItem_m21917_gshared/* 2737*/,
	(methodPointerType)&Collection_1_Remove_m21918_gshared/* 2738*/,
	(methodPointerType)&Collection_1_RemoveAt_m21919_gshared/* 2739*/,
	(methodPointerType)&Collection_1_RemoveItem_m21920_gshared/* 2740*/,
	(methodPointerType)&Collection_1_get_Count_m21921_gshared/* 2741*/,
	(methodPointerType)&Collection_1_get_Item_m21922_gshared/* 2742*/,
	(methodPointerType)&Collection_1_set_Item_m21923_gshared/* 2743*/,
	(methodPointerType)&Collection_1_SetItem_m21924_gshared/* 2744*/,
	(methodPointerType)&Collection_1_IsValidItem_m21925_gshared/* 2745*/,
	(methodPointerType)&Collection_1_ConvertItem_m21926_gshared/* 2746*/,
	(methodPointerType)&Collection_1_CheckWritable_m21927_gshared/* 2747*/,
	(methodPointerType)&Collection_1_IsSynchronized_m21928_gshared/* 2748*/,
	(methodPointerType)&Collection_1_IsFixedSize_m21929_gshared/* 2749*/,
	(methodPointerType)&EqualityComparer_1__ctor_m21930_gshared/* 2750*/,
	(methodPointerType)&EqualityComparer_1__cctor_m21931_gshared/* 2751*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m21932_gshared/* 2752*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m21933_gshared/* 2753*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m21934_gshared/* 2754*/,
	(methodPointerType)&DefaultComparer__ctor_m21935_gshared/* 2755*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m21936_gshared/* 2756*/,
	(methodPointerType)&DefaultComparer_Equals_m21937_gshared/* 2757*/,
	(methodPointerType)&Predicate_1__ctor_m21938_gshared/* 2758*/,
	(methodPointerType)&Predicate_1_Invoke_m21939_gshared/* 2759*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m21940_gshared/* 2760*/,
	(methodPointerType)&Predicate_1_EndInvoke_m21941_gshared/* 2761*/,
	(methodPointerType)&Action_1__ctor_m21942_gshared/* 2762*/,
	(methodPointerType)&Action_1_Invoke_m21943_gshared/* 2763*/,
	(methodPointerType)&Action_1_BeginInvoke_m21944_gshared/* 2764*/,
	(methodPointerType)&Action_1_EndInvoke_m21945_gshared/* 2765*/,
	(methodPointerType)&Comparer_1__ctor_m21946_gshared/* 2766*/,
	(methodPointerType)&Comparer_1__cctor_m21947_gshared/* 2767*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m21948_gshared/* 2768*/,
	(methodPointerType)&Comparer_1_get_Default_m21949_gshared/* 2769*/,
	(methodPointerType)&DefaultComparer__ctor_m21950_gshared/* 2770*/,
	(methodPointerType)&DefaultComparer_Compare_m21951_gshared/* 2771*/,
	(methodPointerType)&Dictionary_2__ctor_m22216_gshared/* 2772*/,
	(methodPointerType)&Dictionary_2__ctor_m22218_gshared/* 2773*/,
	(methodPointerType)&Dictionary_2__ctor_m22220_gshared/* 2774*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m22222_gshared/* 2775*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m22224_gshared/* 2776*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m22226_gshared/* 2777*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m22228_gshared/* 2778*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m22230_gshared/* 2779*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22232_gshared/* 2780*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22234_gshared/* 2781*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22236_gshared/* 2782*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22238_gshared/* 2783*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22240_gshared/* 2784*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22242_gshared/* 2785*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22244_gshared/* 2786*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m22246_gshared/* 2787*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22248_gshared/* 2788*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22250_gshared/* 2789*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22252_gshared/* 2790*/,
	(methodPointerType)&Dictionary_2_get_Count_m22254_gshared/* 2791*/,
	(methodPointerType)&Dictionary_2_get_Item_m22256_gshared/* 2792*/,
	(methodPointerType)&Dictionary_2_set_Item_m22258_gshared/* 2793*/,
	(methodPointerType)&Dictionary_2_Init_m22260_gshared/* 2794*/,
	(methodPointerType)&Dictionary_2_InitArrays_m22262_gshared/* 2795*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m22264_gshared/* 2796*/,
	(methodPointerType)&Dictionary_2_make_pair_m22266_gshared/* 2797*/,
	(methodPointerType)&Dictionary_2_pick_key_m22268_gshared/* 2798*/,
	(methodPointerType)&Dictionary_2_pick_value_m22270_gshared/* 2799*/,
	(methodPointerType)&Dictionary_2_CopyTo_m22272_gshared/* 2800*/,
	(methodPointerType)&Dictionary_2_Resize_m22274_gshared/* 2801*/,
	(methodPointerType)&Dictionary_2_Add_m22276_gshared/* 2802*/,
	(methodPointerType)&Dictionary_2_Clear_m22278_gshared/* 2803*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m22280_gshared/* 2804*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m22282_gshared/* 2805*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m22284_gshared/* 2806*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m22286_gshared/* 2807*/,
	(methodPointerType)&Dictionary_2_Remove_m22288_gshared/* 2808*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m22290_gshared/* 2809*/,
	(methodPointerType)&Dictionary_2_get_Keys_m22292_gshared/* 2810*/,
	(methodPointerType)&Dictionary_2_ToTKey_m22295_gshared/* 2811*/,
	(methodPointerType)&Dictionary_2_ToTValue_m22297_gshared/* 2812*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m22299_gshared/* 2813*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m22302_gshared/* 2814*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22303_gshared/* 2815*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22304_gshared/* 2816*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22305_gshared/* 2817*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22306_gshared/* 2818*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22307_gshared/* 2819*/,
	(methodPointerType)&KeyValuePair_2__ctor_m22308_gshared/* 2820*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m22310_gshared/* 2821*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m22312_gshared/* 2822*/,
	(methodPointerType)&KeyCollection__ctor_m22314_gshared/* 2823*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m22315_gshared/* 2824*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m22316_gshared/* 2825*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m22317_gshared/* 2826*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m22318_gshared/* 2827*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m22319_gshared/* 2828*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m22320_gshared/* 2829*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m22321_gshared/* 2830*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m22322_gshared/* 2831*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m22323_gshared/* 2832*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m22324_gshared/* 2833*/,
	(methodPointerType)&KeyCollection_CopyTo_m22325_gshared/* 2834*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m22326_gshared/* 2835*/,
	(methodPointerType)&KeyCollection_get_Count_m22327_gshared/* 2836*/,
	(methodPointerType)&Enumerator__ctor_m22328_gshared/* 2837*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22329_gshared/* 2838*/,
	(methodPointerType)&Enumerator_Dispose_m22330_gshared/* 2839*/,
	(methodPointerType)&Enumerator_MoveNext_m22331_gshared/* 2840*/,
	(methodPointerType)&Enumerator_get_Current_m22332_gshared/* 2841*/,
	(methodPointerType)&Enumerator__ctor_m22333_gshared/* 2842*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared/* 2843*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared/* 2844*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared/* 2845*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared/* 2846*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m22340_gshared/* 2847*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m22341_gshared/* 2848*/,
	(methodPointerType)&Enumerator_VerifyState_m22342_gshared/* 2849*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m22343_gshared/* 2850*/,
	(methodPointerType)&Enumerator_Dispose_m22344_gshared/* 2851*/,
	(methodPointerType)&Transform_1__ctor_m22345_gshared/* 2852*/,
	(methodPointerType)&Transform_1_Invoke_m22346_gshared/* 2853*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22347_gshared/* 2854*/,
	(methodPointerType)&Transform_1_EndInvoke_m22348_gshared/* 2855*/,
	(methodPointerType)&Transform_1__ctor_m22349_gshared/* 2856*/,
	(methodPointerType)&Transform_1_Invoke_m22350_gshared/* 2857*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22351_gshared/* 2858*/,
	(methodPointerType)&Transform_1_EndInvoke_m22352_gshared/* 2859*/,
	(methodPointerType)&ValueCollection__ctor_m22353_gshared/* 2860*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22354_gshared/* 2861*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22355_gshared/* 2862*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22356_gshared/* 2863*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22357_gshared/* 2864*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22358_gshared/* 2865*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m22359_gshared/* 2866*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22360_gshared/* 2867*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22361_gshared/* 2868*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22362_gshared/* 2869*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m22363_gshared/* 2870*/,
	(methodPointerType)&ValueCollection_CopyTo_m22364_gshared/* 2871*/,
	(methodPointerType)&ValueCollection_get_Count_m22366_gshared/* 2872*/,
	(methodPointerType)&Enumerator__ctor_m22367_gshared/* 2873*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22368_gshared/* 2874*/,
	(methodPointerType)&Enumerator_Dispose_m22369_gshared/* 2875*/,
	(methodPointerType)&Transform_1__ctor_m22372_gshared/* 2876*/,
	(methodPointerType)&Transform_1_Invoke_m22373_gshared/* 2877*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22374_gshared/* 2878*/,
	(methodPointerType)&Transform_1_EndInvoke_m22375_gshared/* 2879*/,
	(methodPointerType)&Transform_1__ctor_m22376_gshared/* 2880*/,
	(methodPointerType)&Transform_1_Invoke_m22377_gshared/* 2881*/,
	(methodPointerType)&Transform_1_BeginInvoke_m22378_gshared/* 2882*/,
	(methodPointerType)&Transform_1_EndInvoke_m22379_gshared/* 2883*/,
	(methodPointerType)&ShimEnumerator__ctor_m22380_gshared/* 2884*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m22381_gshared/* 2885*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m22382_gshared/* 2886*/,
	(methodPointerType)&ShimEnumerator_get_Key_m22383_gshared/* 2887*/,
	(methodPointerType)&ShimEnumerator_get_Value_m22384_gshared/* 2888*/,
	(methodPointerType)&ShimEnumerator_get_Current_m22385_gshared/* 2889*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22532_gshared/* 2890*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22533_gshared/* 2891*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22534_gshared/* 2892*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22535_gshared/* 2893*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22536_gshared/* 2894*/,
	(methodPointerType)&Comparison_1_Invoke_m22537_gshared/* 2895*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m22538_gshared/* 2896*/,
	(methodPointerType)&Comparison_1_EndInvoke_m22539_gshared/* 2897*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m22540_gshared/* 2898*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22541_gshared/* 2899*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m22542_gshared/* 2900*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m22543_gshared/* 2901*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m22544_gshared/* 2902*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m22545_gshared/* 2903*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m22546_gshared/* 2904*/,
	(methodPointerType)&UnityAction_1_Invoke_m22547_gshared/* 2905*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m22548_gshared/* 2906*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m22549_gshared/* 2907*/,
	(methodPointerType)&InvokableCall_1__ctor_m22550_gshared/* 2908*/,
	(methodPointerType)&InvokableCall_1__ctor_m22551_gshared/* 2909*/,
	(methodPointerType)&InvokableCall_1_Invoke_m22552_gshared/* 2910*/,
	(methodPointerType)&InvokableCall_1_Find_m22553_gshared/* 2911*/,
	(methodPointerType)&List_1__ctor_m22949_gshared/* 2912*/,
	(methodPointerType)&List_1__cctor_m22950_gshared/* 2913*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22951_gshared/* 2914*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m22952_gshared/* 2915*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m22953_gshared/* 2916*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m22954_gshared/* 2917*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m22955_gshared/* 2918*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m22956_gshared/* 2919*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m22957_gshared/* 2920*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m22958_gshared/* 2921*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22959_gshared/* 2922*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m22960_gshared/* 2923*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m22961_gshared/* 2924*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m22962_gshared/* 2925*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m22963_gshared/* 2926*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m22964_gshared/* 2927*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m22965_gshared/* 2928*/,
	(methodPointerType)&List_1_Add_m22966_gshared/* 2929*/,
	(methodPointerType)&List_1_GrowIfNeeded_m22967_gshared/* 2930*/,
	(methodPointerType)&List_1_AddCollection_m22968_gshared/* 2931*/,
	(methodPointerType)&List_1_AddEnumerable_m22969_gshared/* 2932*/,
	(methodPointerType)&List_1_AddRange_m22970_gshared/* 2933*/,
	(methodPointerType)&List_1_AsReadOnly_m22971_gshared/* 2934*/,
	(methodPointerType)&List_1_Clear_m22972_gshared/* 2935*/,
	(methodPointerType)&List_1_Contains_m22973_gshared/* 2936*/,
	(methodPointerType)&List_1_CopyTo_m22974_gshared/* 2937*/,
	(methodPointerType)&List_1_Find_m22975_gshared/* 2938*/,
	(methodPointerType)&List_1_CheckMatch_m22976_gshared/* 2939*/,
	(methodPointerType)&List_1_FindIndex_m22977_gshared/* 2940*/,
	(methodPointerType)&List_1_GetIndex_m22978_gshared/* 2941*/,
	(methodPointerType)&List_1_ForEach_m22979_gshared/* 2942*/,
	(methodPointerType)&List_1_GetEnumerator_m22980_gshared/* 2943*/,
	(methodPointerType)&List_1_IndexOf_m22981_gshared/* 2944*/,
	(methodPointerType)&List_1_Shift_m22982_gshared/* 2945*/,
	(methodPointerType)&List_1_CheckIndex_m22983_gshared/* 2946*/,
	(methodPointerType)&List_1_Insert_m22984_gshared/* 2947*/,
	(methodPointerType)&List_1_CheckCollection_m22985_gshared/* 2948*/,
	(methodPointerType)&List_1_Remove_m22986_gshared/* 2949*/,
	(methodPointerType)&List_1_RemoveAll_m22987_gshared/* 2950*/,
	(methodPointerType)&List_1_RemoveAt_m22988_gshared/* 2951*/,
	(methodPointerType)&List_1_Reverse_m22989_gshared/* 2952*/,
	(methodPointerType)&List_1_Sort_m22990_gshared/* 2953*/,
	(methodPointerType)&List_1_Sort_m22991_gshared/* 2954*/,
	(methodPointerType)&List_1_TrimExcess_m22992_gshared/* 2955*/,
	(methodPointerType)&List_1_get_Count_m22993_gshared/* 2956*/,
	(methodPointerType)&List_1_get_Item_m22994_gshared/* 2957*/,
	(methodPointerType)&List_1_set_Item_m22995_gshared/* 2958*/,
	(methodPointerType)&Enumerator__ctor_m22928_gshared/* 2959*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m22929_gshared/* 2960*/,
	(methodPointerType)&Enumerator_Dispose_m22930_gshared/* 2961*/,
	(methodPointerType)&Enumerator_VerifyState_m22931_gshared/* 2962*/,
	(methodPointerType)&Enumerator_MoveNext_m22932_gshared/* 2963*/,
	(methodPointerType)&Enumerator_get_Current_m22933_gshared/* 2964*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m22890_gshared/* 2965*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22891_gshared/* 2966*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22892_gshared/* 2967*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22893_gshared/* 2968*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22894_gshared/* 2969*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22895_gshared/* 2970*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22896_gshared/* 2971*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22897_gshared/* 2972*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22898_gshared/* 2973*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22899_gshared/* 2974*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22900_gshared/* 2975*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m22901_gshared/* 2976*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m22902_gshared/* 2977*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m22903_gshared/* 2978*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22904_gshared/* 2979*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m22905_gshared/* 2980*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m22906_gshared/* 2981*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22907_gshared/* 2982*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22908_gshared/* 2983*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22909_gshared/* 2984*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22910_gshared/* 2985*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22911_gshared/* 2986*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m22912_gshared/* 2987*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m22913_gshared/* 2988*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m22914_gshared/* 2989*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m22915_gshared/* 2990*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m22916_gshared/* 2991*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m22917_gshared/* 2992*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m22918_gshared/* 2993*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m22919_gshared/* 2994*/,
	(methodPointerType)&Collection_1__ctor_m22999_gshared/* 2995*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23000_gshared/* 2996*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m23001_gshared/* 2997*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m23002_gshared/* 2998*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m23003_gshared/* 2999*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m23004_gshared/* 3000*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m23005_gshared/* 3001*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m23006_gshared/* 3002*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m23007_gshared/* 3003*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m23008_gshared/* 3004*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m23009_gshared/* 3005*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m23010_gshared/* 3006*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m23011_gshared/* 3007*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m23012_gshared/* 3008*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m23013_gshared/* 3009*/,
	(methodPointerType)&Collection_1_Add_m23014_gshared/* 3010*/,
	(methodPointerType)&Collection_1_Clear_m23015_gshared/* 3011*/,
	(methodPointerType)&Collection_1_ClearItems_m23016_gshared/* 3012*/,
	(methodPointerType)&Collection_1_Contains_m23017_gshared/* 3013*/,
	(methodPointerType)&Collection_1_CopyTo_m23018_gshared/* 3014*/,
	(methodPointerType)&Collection_1_GetEnumerator_m23019_gshared/* 3015*/,
	(methodPointerType)&Collection_1_IndexOf_m23020_gshared/* 3016*/,
	(methodPointerType)&Collection_1_Insert_m23021_gshared/* 3017*/,
	(methodPointerType)&Collection_1_InsertItem_m23022_gshared/* 3018*/,
	(methodPointerType)&Collection_1_Remove_m23023_gshared/* 3019*/,
	(methodPointerType)&Collection_1_RemoveAt_m23024_gshared/* 3020*/,
	(methodPointerType)&Collection_1_RemoveItem_m23025_gshared/* 3021*/,
	(methodPointerType)&Collection_1_get_Count_m23026_gshared/* 3022*/,
	(methodPointerType)&Collection_1_get_Item_m23027_gshared/* 3023*/,
	(methodPointerType)&Collection_1_set_Item_m23028_gshared/* 3024*/,
	(methodPointerType)&Collection_1_SetItem_m23029_gshared/* 3025*/,
	(methodPointerType)&Collection_1_IsValidItem_m23030_gshared/* 3026*/,
	(methodPointerType)&Collection_1_ConvertItem_m23031_gshared/* 3027*/,
	(methodPointerType)&Collection_1_CheckWritable_m23032_gshared/* 3028*/,
	(methodPointerType)&Collection_1_IsSynchronized_m23033_gshared/* 3029*/,
	(methodPointerType)&Collection_1_IsFixedSize_m23034_gshared/* 3030*/,
	(methodPointerType)&EqualityComparer_1__ctor_m23035_gshared/* 3031*/,
	(methodPointerType)&EqualityComparer_1__cctor_m23036_gshared/* 3032*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m23037_gshared/* 3033*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m23038_gshared/* 3034*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m23039_gshared/* 3035*/,
	(methodPointerType)&DefaultComparer__ctor_m23040_gshared/* 3036*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m23041_gshared/* 3037*/,
	(methodPointerType)&DefaultComparer_Equals_m23042_gshared/* 3038*/,
	(methodPointerType)&Predicate_1__ctor_m22920_gshared/* 3039*/,
	(methodPointerType)&Predicate_1_Invoke_m22921_gshared/* 3040*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m22922_gshared/* 3041*/,
	(methodPointerType)&Predicate_1_EndInvoke_m22923_gshared/* 3042*/,
	(methodPointerType)&Action_1__ctor_m22924_gshared/* 3043*/,
	(methodPointerType)&Action_1_Invoke_m22925_gshared/* 3044*/,
	(methodPointerType)&Action_1_BeginInvoke_m22926_gshared/* 3045*/,
	(methodPointerType)&Action_1_EndInvoke_m22927_gshared/* 3046*/,
	(methodPointerType)&Comparer_1__ctor_m23043_gshared/* 3047*/,
	(methodPointerType)&Comparer_1__cctor_m23044_gshared/* 3048*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m23045_gshared/* 3049*/,
	(methodPointerType)&Comparer_1_get_Default_m23046_gshared/* 3050*/,
	(methodPointerType)&DefaultComparer__ctor_m23047_gshared/* 3051*/,
	(methodPointerType)&DefaultComparer_Compare_m23048_gshared/* 3052*/,
	(methodPointerType)&Comparison_1__ctor_m22934_gshared/* 3053*/,
	(methodPointerType)&Comparison_1_Invoke_m22935_gshared/* 3054*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m22936_gshared/* 3055*/,
	(methodPointerType)&Comparison_1_EndInvoke_m22937_gshared/* 3056*/,
	(methodPointerType)&TweenRunner_1_Start_m23049_gshared/* 3057*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m23050_gshared/* 3058*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m23051_gshared/* 3059*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m23052_gshared/* 3060*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m23053_gshared/* 3061*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m23054_gshared/* 3062*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m23055_gshared/* 3063*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23510_gshared/* 3064*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23511_gshared/* 3065*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23512_gshared/* 3066*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23513_gshared/* 3067*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23514_gshared/* 3068*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23520_gshared/* 3069*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23521_gshared/* 3070*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23522_gshared/* 3071*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23523_gshared/* 3072*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23524_gshared/* 3073*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m23525_gshared/* 3074*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23526_gshared/* 3075*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m23527_gshared/* 3076*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m23528_gshared/* 3077*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m23529_gshared/* 3078*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m23537_gshared/* 3079*/,
	(methodPointerType)&UnityAction_1_Invoke_m23538_gshared/* 3080*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m23539_gshared/* 3081*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m23540_gshared/* 3082*/,
	(methodPointerType)&InvokableCall_1__ctor_m23541_gshared/* 3083*/,
	(methodPointerType)&InvokableCall_1__ctor_m23542_gshared/* 3084*/,
	(methodPointerType)&InvokableCall_1_Invoke_m23543_gshared/* 3085*/,
	(methodPointerType)&InvokableCall_1_Find_m23544_gshared/* 3086*/,
	(methodPointerType)&UnityEvent_1_AddListener_m23545_gshared/* 3087*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m23546_gshared/* 3088*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m23547_gshared/* 3089*/,
	(methodPointerType)&UnityAction_1__ctor_m23548_gshared/* 3090*/,
	(methodPointerType)&UnityAction_1_Invoke_m23549_gshared/* 3091*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m23550_gshared/* 3092*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m23551_gshared/* 3093*/,
	(methodPointerType)&InvokableCall_1__ctor_m23552_gshared/* 3094*/,
	(methodPointerType)&InvokableCall_1__ctor_m23553_gshared/* 3095*/,
	(methodPointerType)&InvokableCall_1_Invoke_m23554_gshared/* 3096*/,
	(methodPointerType)&InvokableCall_1_Find_m23555_gshared/* 3097*/,
	(methodPointerType)&Func_2_Invoke_m24057_gshared/* 3098*/,
	(methodPointerType)&Func_2_BeginInvoke_m24059_gshared/* 3099*/,
	(methodPointerType)&Func_2_EndInvoke_m24061_gshared/* 3100*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24204_gshared/* 3101*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24205_gshared/* 3102*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24206_gshared/* 3103*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24207_gshared/* 3104*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24208_gshared/* 3105*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24214_gshared/* 3106*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24215_gshared/* 3107*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24216_gshared/* 3108*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24217_gshared/* 3109*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24218_gshared/* 3110*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24731_gshared/* 3111*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24732_gshared/* 3112*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24733_gshared/* 3113*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24734_gshared/* 3114*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24735_gshared/* 3115*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m24736_gshared/* 3116*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24737_gshared/* 3117*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m24738_gshared/* 3118*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m24739_gshared/* 3119*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m24740_gshared/* 3120*/,
	(methodPointerType)&List_1__ctor_m24741_gshared/* 3121*/,
	(methodPointerType)&List_1__ctor_m24742_gshared/* 3122*/,
	(methodPointerType)&List_1__cctor_m24743_gshared/* 3123*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24744_gshared/* 3124*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m24745_gshared/* 3125*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m24746_gshared/* 3126*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m24747_gshared/* 3127*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m24748_gshared/* 3128*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m24749_gshared/* 3129*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m24750_gshared/* 3130*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m24751_gshared/* 3131*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24752_gshared/* 3132*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m24753_gshared/* 3133*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m24754_gshared/* 3134*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m24755_gshared/* 3135*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m24756_gshared/* 3136*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m24757_gshared/* 3137*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m24758_gshared/* 3138*/,
	(methodPointerType)&List_1_Add_m24759_gshared/* 3139*/,
	(methodPointerType)&List_1_GrowIfNeeded_m24760_gshared/* 3140*/,
	(methodPointerType)&List_1_AddCollection_m24761_gshared/* 3141*/,
	(methodPointerType)&List_1_AddEnumerable_m24762_gshared/* 3142*/,
	(methodPointerType)&List_1_AddRange_m24763_gshared/* 3143*/,
	(methodPointerType)&List_1_AsReadOnly_m24764_gshared/* 3144*/,
	(methodPointerType)&List_1_Clear_m24765_gshared/* 3145*/,
	(methodPointerType)&List_1_Contains_m24766_gshared/* 3146*/,
	(methodPointerType)&List_1_CopyTo_m24767_gshared/* 3147*/,
	(methodPointerType)&List_1_Find_m24768_gshared/* 3148*/,
	(methodPointerType)&List_1_CheckMatch_m24769_gshared/* 3149*/,
	(methodPointerType)&List_1_FindIndex_m24770_gshared/* 3150*/,
	(methodPointerType)&List_1_GetIndex_m24771_gshared/* 3151*/,
	(methodPointerType)&List_1_ForEach_m24772_gshared/* 3152*/,
	(methodPointerType)&List_1_GetEnumerator_m24773_gshared/* 3153*/,
	(methodPointerType)&List_1_IndexOf_m24774_gshared/* 3154*/,
	(methodPointerType)&List_1_Shift_m24775_gshared/* 3155*/,
	(methodPointerType)&List_1_CheckIndex_m24776_gshared/* 3156*/,
	(methodPointerType)&List_1_Insert_m24777_gshared/* 3157*/,
	(methodPointerType)&List_1_CheckCollection_m24778_gshared/* 3158*/,
	(methodPointerType)&List_1_Remove_m24779_gshared/* 3159*/,
	(methodPointerType)&List_1_RemoveAll_m24780_gshared/* 3160*/,
	(methodPointerType)&List_1_RemoveAt_m24781_gshared/* 3161*/,
	(methodPointerType)&List_1_Reverse_m24782_gshared/* 3162*/,
	(methodPointerType)&List_1_Sort_m24783_gshared/* 3163*/,
	(methodPointerType)&List_1_Sort_m24784_gshared/* 3164*/,
	(methodPointerType)&List_1_ToArray_m24785_gshared/* 3165*/,
	(methodPointerType)&List_1_TrimExcess_m24786_gshared/* 3166*/,
	(methodPointerType)&List_1_get_Capacity_m24787_gshared/* 3167*/,
	(methodPointerType)&List_1_set_Capacity_m24788_gshared/* 3168*/,
	(methodPointerType)&List_1_get_Count_m24789_gshared/* 3169*/,
	(methodPointerType)&List_1_get_Item_m24790_gshared/* 3170*/,
	(methodPointerType)&List_1_set_Item_m24791_gshared/* 3171*/,
	(methodPointerType)&Enumerator__ctor_m24792_gshared/* 3172*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24793_gshared/* 3173*/,
	(methodPointerType)&Enumerator_Dispose_m24794_gshared/* 3174*/,
	(methodPointerType)&Enumerator_VerifyState_m24795_gshared/* 3175*/,
	(methodPointerType)&Enumerator_MoveNext_m24796_gshared/* 3176*/,
	(methodPointerType)&Enumerator_get_Current_m24797_gshared/* 3177*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m24798_gshared/* 3178*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24799_gshared/* 3179*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24800_gshared/* 3180*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24801_gshared/* 3181*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24802_gshared/* 3182*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24803_gshared/* 3183*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24804_gshared/* 3184*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24805_gshared/* 3185*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24806_gshared/* 3186*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24807_gshared/* 3187*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24808_gshared/* 3188*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m24809_gshared/* 3189*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m24810_gshared/* 3190*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m24811_gshared/* 3191*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24812_gshared/* 3192*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m24813_gshared/* 3193*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m24814_gshared/* 3194*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24815_gshared/* 3195*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24816_gshared/* 3196*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24817_gshared/* 3197*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24818_gshared/* 3198*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24819_gshared/* 3199*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m24820_gshared/* 3200*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m24821_gshared/* 3201*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m24822_gshared/* 3202*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m24823_gshared/* 3203*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m24824_gshared/* 3204*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m24825_gshared/* 3205*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m24826_gshared/* 3206*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m24827_gshared/* 3207*/,
	(methodPointerType)&Collection_1__ctor_m24828_gshared/* 3208*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24829_gshared/* 3209*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m24830_gshared/* 3210*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m24831_gshared/* 3211*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m24832_gshared/* 3212*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m24833_gshared/* 3213*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m24834_gshared/* 3214*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m24835_gshared/* 3215*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m24836_gshared/* 3216*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m24837_gshared/* 3217*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m24838_gshared/* 3218*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m24839_gshared/* 3219*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m24840_gshared/* 3220*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m24841_gshared/* 3221*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m24842_gshared/* 3222*/,
	(methodPointerType)&Collection_1_Add_m24843_gshared/* 3223*/,
	(methodPointerType)&Collection_1_Clear_m24844_gshared/* 3224*/,
	(methodPointerType)&Collection_1_ClearItems_m24845_gshared/* 3225*/,
	(methodPointerType)&Collection_1_Contains_m24846_gshared/* 3226*/,
	(methodPointerType)&Collection_1_CopyTo_m24847_gshared/* 3227*/,
	(methodPointerType)&Collection_1_GetEnumerator_m24848_gshared/* 3228*/,
	(methodPointerType)&Collection_1_IndexOf_m24849_gshared/* 3229*/,
	(methodPointerType)&Collection_1_Insert_m24850_gshared/* 3230*/,
	(methodPointerType)&Collection_1_InsertItem_m24851_gshared/* 3231*/,
	(methodPointerType)&Collection_1_Remove_m24852_gshared/* 3232*/,
	(methodPointerType)&Collection_1_RemoveAt_m24853_gshared/* 3233*/,
	(methodPointerType)&Collection_1_RemoveItem_m24854_gshared/* 3234*/,
	(methodPointerType)&Collection_1_get_Count_m24855_gshared/* 3235*/,
	(methodPointerType)&Collection_1_get_Item_m24856_gshared/* 3236*/,
	(methodPointerType)&Collection_1_set_Item_m24857_gshared/* 3237*/,
	(methodPointerType)&Collection_1_SetItem_m24858_gshared/* 3238*/,
	(methodPointerType)&Collection_1_IsValidItem_m24859_gshared/* 3239*/,
	(methodPointerType)&Collection_1_ConvertItem_m24860_gshared/* 3240*/,
	(methodPointerType)&Collection_1_CheckWritable_m24861_gshared/* 3241*/,
	(methodPointerType)&Collection_1_IsSynchronized_m24862_gshared/* 3242*/,
	(methodPointerType)&Collection_1_IsFixedSize_m24863_gshared/* 3243*/,
	(methodPointerType)&EqualityComparer_1__ctor_m24864_gshared/* 3244*/,
	(methodPointerType)&EqualityComparer_1__cctor_m24865_gshared/* 3245*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24866_gshared/* 3246*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24867_gshared/* 3247*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m24868_gshared/* 3248*/,
	(methodPointerType)&DefaultComparer__ctor_m24869_gshared/* 3249*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m24870_gshared/* 3250*/,
	(methodPointerType)&DefaultComparer_Equals_m24871_gshared/* 3251*/,
	(methodPointerType)&Predicate_1__ctor_m24872_gshared/* 3252*/,
	(methodPointerType)&Predicate_1_Invoke_m24873_gshared/* 3253*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m24874_gshared/* 3254*/,
	(methodPointerType)&Predicate_1_EndInvoke_m24875_gshared/* 3255*/,
	(methodPointerType)&Action_1__ctor_m24876_gshared/* 3256*/,
	(methodPointerType)&Action_1_Invoke_m24877_gshared/* 3257*/,
	(methodPointerType)&Action_1_BeginInvoke_m24878_gshared/* 3258*/,
	(methodPointerType)&Action_1_EndInvoke_m24879_gshared/* 3259*/,
	(methodPointerType)&Comparer_1__ctor_m24880_gshared/* 3260*/,
	(methodPointerType)&Comparer_1__cctor_m24881_gshared/* 3261*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m24882_gshared/* 3262*/,
	(methodPointerType)&Comparer_1_get_Default_m24883_gshared/* 3263*/,
	(methodPointerType)&DefaultComparer__ctor_m24884_gshared/* 3264*/,
	(methodPointerType)&DefaultComparer_Compare_m24885_gshared/* 3265*/,
	(methodPointerType)&Comparison_1__ctor_m24886_gshared/* 3266*/,
	(methodPointerType)&Comparison_1_Invoke_m24887_gshared/* 3267*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m24888_gshared/* 3268*/,
	(methodPointerType)&Comparison_1_EndInvoke_m24889_gshared/* 3269*/,
	(methodPointerType)&List_1__ctor_m24890_gshared/* 3270*/,
	(methodPointerType)&List_1__ctor_m24891_gshared/* 3271*/,
	(methodPointerType)&List_1__cctor_m24892_gshared/* 3272*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24893_gshared/* 3273*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m24894_gshared/* 3274*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m24895_gshared/* 3275*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m24896_gshared/* 3276*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m24897_gshared/* 3277*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m24898_gshared/* 3278*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m24899_gshared/* 3279*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m24900_gshared/* 3280*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24901_gshared/* 3281*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m24902_gshared/* 3282*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m24903_gshared/* 3283*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m24904_gshared/* 3284*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m24905_gshared/* 3285*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m24906_gshared/* 3286*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m24907_gshared/* 3287*/,
	(methodPointerType)&List_1_Add_m24908_gshared/* 3288*/,
	(methodPointerType)&List_1_GrowIfNeeded_m24909_gshared/* 3289*/,
	(methodPointerType)&List_1_AddCollection_m24910_gshared/* 3290*/,
	(methodPointerType)&List_1_AddEnumerable_m24911_gshared/* 3291*/,
	(methodPointerType)&List_1_AddRange_m24912_gshared/* 3292*/,
	(methodPointerType)&List_1_AsReadOnly_m24913_gshared/* 3293*/,
	(methodPointerType)&List_1_Clear_m24914_gshared/* 3294*/,
	(methodPointerType)&List_1_Contains_m24915_gshared/* 3295*/,
	(methodPointerType)&List_1_CopyTo_m24916_gshared/* 3296*/,
	(methodPointerType)&List_1_Find_m24917_gshared/* 3297*/,
	(methodPointerType)&List_1_CheckMatch_m24918_gshared/* 3298*/,
	(methodPointerType)&List_1_FindIndex_m24919_gshared/* 3299*/,
	(methodPointerType)&List_1_GetIndex_m24920_gshared/* 3300*/,
	(methodPointerType)&List_1_ForEach_m24921_gshared/* 3301*/,
	(methodPointerType)&List_1_GetEnumerator_m24922_gshared/* 3302*/,
	(methodPointerType)&List_1_IndexOf_m24923_gshared/* 3303*/,
	(methodPointerType)&List_1_Shift_m24924_gshared/* 3304*/,
	(methodPointerType)&List_1_CheckIndex_m24925_gshared/* 3305*/,
	(methodPointerType)&List_1_Insert_m24926_gshared/* 3306*/,
	(methodPointerType)&List_1_CheckCollection_m24927_gshared/* 3307*/,
	(methodPointerType)&List_1_Remove_m24928_gshared/* 3308*/,
	(methodPointerType)&List_1_RemoveAll_m24929_gshared/* 3309*/,
	(methodPointerType)&List_1_RemoveAt_m24930_gshared/* 3310*/,
	(methodPointerType)&List_1_Reverse_m24931_gshared/* 3311*/,
	(methodPointerType)&List_1_Sort_m24932_gshared/* 3312*/,
	(methodPointerType)&List_1_Sort_m24933_gshared/* 3313*/,
	(methodPointerType)&List_1_ToArray_m24934_gshared/* 3314*/,
	(methodPointerType)&List_1_TrimExcess_m24935_gshared/* 3315*/,
	(methodPointerType)&List_1_get_Capacity_m24936_gshared/* 3316*/,
	(methodPointerType)&List_1_set_Capacity_m24937_gshared/* 3317*/,
	(methodPointerType)&List_1_get_Count_m24938_gshared/* 3318*/,
	(methodPointerType)&List_1_get_Item_m24939_gshared/* 3319*/,
	(methodPointerType)&List_1_set_Item_m24940_gshared/* 3320*/,
	(methodPointerType)&Enumerator__ctor_m24941_gshared/* 3321*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m24942_gshared/* 3322*/,
	(methodPointerType)&Enumerator_Dispose_m24943_gshared/* 3323*/,
	(methodPointerType)&Enumerator_VerifyState_m24944_gshared/* 3324*/,
	(methodPointerType)&Enumerator_MoveNext_m24945_gshared/* 3325*/,
	(methodPointerType)&Enumerator_get_Current_m24946_gshared/* 3326*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m24947_gshared/* 3327*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24948_gshared/* 3328*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24949_gshared/* 3329*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24950_gshared/* 3330*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24951_gshared/* 3331*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24952_gshared/* 3332*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24953_gshared/* 3333*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24954_gshared/* 3334*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24955_gshared/* 3335*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24956_gshared/* 3336*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24957_gshared/* 3337*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m24958_gshared/* 3338*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m24959_gshared/* 3339*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m24960_gshared/* 3340*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24961_gshared/* 3341*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m24962_gshared/* 3342*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m24963_gshared/* 3343*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24964_gshared/* 3344*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24965_gshared/* 3345*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24966_gshared/* 3346*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24967_gshared/* 3347*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24968_gshared/* 3348*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m24969_gshared/* 3349*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m24970_gshared/* 3350*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m24971_gshared/* 3351*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m24972_gshared/* 3352*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m24973_gshared/* 3353*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m24974_gshared/* 3354*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m24975_gshared/* 3355*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m24976_gshared/* 3356*/,
	(methodPointerType)&Collection_1__ctor_m24977_gshared/* 3357*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24978_gshared/* 3358*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m24979_gshared/* 3359*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m24980_gshared/* 3360*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m24981_gshared/* 3361*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m24982_gshared/* 3362*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m24983_gshared/* 3363*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m24984_gshared/* 3364*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m24985_gshared/* 3365*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m24986_gshared/* 3366*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m24987_gshared/* 3367*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m24988_gshared/* 3368*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m24989_gshared/* 3369*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m24990_gshared/* 3370*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m24991_gshared/* 3371*/,
	(methodPointerType)&Collection_1_Add_m24992_gshared/* 3372*/,
	(methodPointerType)&Collection_1_Clear_m24993_gshared/* 3373*/,
	(methodPointerType)&Collection_1_ClearItems_m24994_gshared/* 3374*/,
	(methodPointerType)&Collection_1_Contains_m24995_gshared/* 3375*/,
	(methodPointerType)&Collection_1_CopyTo_m24996_gshared/* 3376*/,
	(methodPointerType)&Collection_1_GetEnumerator_m24997_gshared/* 3377*/,
	(methodPointerType)&Collection_1_IndexOf_m24998_gshared/* 3378*/,
	(methodPointerType)&Collection_1_Insert_m24999_gshared/* 3379*/,
	(methodPointerType)&Collection_1_InsertItem_m25000_gshared/* 3380*/,
	(methodPointerType)&Collection_1_Remove_m25001_gshared/* 3381*/,
	(methodPointerType)&Collection_1_RemoveAt_m25002_gshared/* 3382*/,
	(methodPointerType)&Collection_1_RemoveItem_m25003_gshared/* 3383*/,
	(methodPointerType)&Collection_1_get_Count_m25004_gshared/* 3384*/,
	(methodPointerType)&Collection_1_get_Item_m25005_gshared/* 3385*/,
	(methodPointerType)&Collection_1_set_Item_m25006_gshared/* 3386*/,
	(methodPointerType)&Collection_1_SetItem_m25007_gshared/* 3387*/,
	(methodPointerType)&Collection_1_IsValidItem_m25008_gshared/* 3388*/,
	(methodPointerType)&Collection_1_ConvertItem_m25009_gshared/* 3389*/,
	(methodPointerType)&Collection_1_CheckWritable_m25010_gshared/* 3390*/,
	(methodPointerType)&Collection_1_IsSynchronized_m25011_gshared/* 3391*/,
	(methodPointerType)&Collection_1_IsFixedSize_m25012_gshared/* 3392*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25013_gshared/* 3393*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25014_gshared/* 3394*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25015_gshared/* 3395*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25016_gshared/* 3396*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25017_gshared/* 3397*/,
	(methodPointerType)&DefaultComparer__ctor_m25018_gshared/* 3398*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25019_gshared/* 3399*/,
	(methodPointerType)&DefaultComparer_Equals_m25020_gshared/* 3400*/,
	(methodPointerType)&Predicate_1__ctor_m25021_gshared/* 3401*/,
	(methodPointerType)&Predicate_1_Invoke_m25022_gshared/* 3402*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m25023_gshared/* 3403*/,
	(methodPointerType)&Predicate_1_EndInvoke_m25024_gshared/* 3404*/,
	(methodPointerType)&Action_1__ctor_m25025_gshared/* 3405*/,
	(methodPointerType)&Action_1_Invoke_m25026_gshared/* 3406*/,
	(methodPointerType)&Action_1_BeginInvoke_m25027_gshared/* 3407*/,
	(methodPointerType)&Action_1_EndInvoke_m25028_gshared/* 3408*/,
	(methodPointerType)&Comparer_1__ctor_m25029_gshared/* 3409*/,
	(methodPointerType)&Comparer_1__cctor_m25030_gshared/* 3410*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m25031_gshared/* 3411*/,
	(methodPointerType)&Comparer_1_get_Default_m25032_gshared/* 3412*/,
	(methodPointerType)&DefaultComparer__ctor_m25033_gshared/* 3413*/,
	(methodPointerType)&DefaultComparer_Compare_m25034_gshared/* 3414*/,
	(methodPointerType)&Comparison_1__ctor_m25035_gshared/* 3415*/,
	(methodPointerType)&Comparison_1_Invoke_m25036_gshared/* 3416*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m25037_gshared/* 3417*/,
	(methodPointerType)&Comparison_1_EndInvoke_m25038_gshared/* 3418*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25165_gshared/* 3419*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25166_gshared/* 3420*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25167_gshared/* 3421*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25168_gshared/* 3422*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25169_gshared/* 3423*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25170_gshared/* 3424*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171_gshared/* 3425*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25172_gshared/* 3426*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25173_gshared/* 3427*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25174_gshared/* 3428*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m25299_gshared/* 3429*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m25300_gshared/* 3430*/,
	(methodPointerType)&InvokableCall_1__ctor_m25301_gshared/* 3431*/,
	(methodPointerType)&InvokableCall_1__ctor_m25302_gshared/* 3432*/,
	(methodPointerType)&InvokableCall_1_Invoke_m25303_gshared/* 3433*/,
	(methodPointerType)&InvokableCall_1_Find_m25304_gshared/* 3434*/,
	(methodPointerType)&UnityAction_1__ctor_m25305_gshared/* 3435*/,
	(methodPointerType)&UnityAction_1_Invoke_m25306_gshared/* 3436*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m25307_gshared/* 3437*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m25308_gshared/* 3438*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m25316_gshared/* 3439*/,
	(methodPointerType)&Dictionary_2__ctor_m25527_gshared/* 3440*/,
	(methodPointerType)&Dictionary_2__ctor_m25530_gshared/* 3441*/,
	(methodPointerType)&Dictionary_2__ctor_m25532_gshared/* 3442*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Keys_m25534_gshared/* 3443*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m25536_gshared/* 3444*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m25538_gshared/* 3445*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m25540_gshared/* 3446*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m25542_gshared/* 3447*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25544_gshared/* 3448*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25546_gshared/* 3449*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25548_gshared/* 3450*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25550_gshared/* 3451*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25552_gshared/* 3452*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25554_gshared/* 3453*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25556_gshared/* 3454*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m25558_gshared/* 3455*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25560_gshared/* 3456*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25562_gshared/* 3457*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25564_gshared/* 3458*/,
	(methodPointerType)&Dictionary_2_get_Count_m25566_gshared/* 3459*/,
	(methodPointerType)&Dictionary_2_get_Item_m25568_gshared/* 3460*/,
	(methodPointerType)&Dictionary_2_set_Item_m25570_gshared/* 3461*/,
	(methodPointerType)&Dictionary_2_Init_m25572_gshared/* 3462*/,
	(methodPointerType)&Dictionary_2_InitArrays_m25574_gshared/* 3463*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m25576_gshared/* 3464*/,
	(methodPointerType)&Dictionary_2_make_pair_m25578_gshared/* 3465*/,
	(methodPointerType)&Dictionary_2_pick_key_m25580_gshared/* 3466*/,
	(methodPointerType)&Dictionary_2_pick_value_m25582_gshared/* 3467*/,
	(methodPointerType)&Dictionary_2_CopyTo_m25584_gshared/* 3468*/,
	(methodPointerType)&Dictionary_2_Resize_m25586_gshared/* 3469*/,
	(methodPointerType)&Dictionary_2_Add_m25588_gshared/* 3470*/,
	(methodPointerType)&Dictionary_2_Clear_m25590_gshared/* 3471*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m25592_gshared/* 3472*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m25594_gshared/* 3473*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m25596_gshared/* 3474*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m25598_gshared/* 3475*/,
	(methodPointerType)&Dictionary_2_Remove_m25600_gshared/* 3476*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m25602_gshared/* 3477*/,
	(methodPointerType)&Dictionary_2_get_Keys_m25604_gshared/* 3478*/,
	(methodPointerType)&Dictionary_2_get_Values_m25606_gshared/* 3479*/,
	(methodPointerType)&Dictionary_2_ToTKey_m25608_gshared/* 3480*/,
	(methodPointerType)&Dictionary_2_ToTValue_m25610_gshared/* 3481*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m25612_gshared/* 3482*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m25614_gshared/* 3483*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m25616_gshared/* 3484*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25617_gshared/* 3485*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25618_gshared/* 3486*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25619_gshared/* 3487*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25620_gshared/* 3488*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25621_gshared/* 3489*/,
	(methodPointerType)&KeyValuePair_2__ctor_m25622_gshared/* 3490*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m25623_gshared/* 3491*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m25624_gshared/* 3492*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m25625_gshared/* 3493*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m25626_gshared/* 3494*/,
	(methodPointerType)&KeyValuePair_2_ToString_m25627_gshared/* 3495*/,
	(methodPointerType)&KeyCollection__ctor_m25628_gshared/* 3496*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m25629_gshared/* 3497*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m25630_gshared/* 3498*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m25631_gshared/* 3499*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m25632_gshared/* 3500*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m25633_gshared/* 3501*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m25634_gshared/* 3502*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m25635_gshared/* 3503*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m25636_gshared/* 3504*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25637_gshared/* 3505*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m25638_gshared/* 3506*/,
	(methodPointerType)&KeyCollection_CopyTo_m25639_gshared/* 3507*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m25640_gshared/* 3508*/,
	(methodPointerType)&KeyCollection_get_Count_m25641_gshared/* 3509*/,
	(methodPointerType)&Enumerator__ctor_m25642_gshared/* 3510*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25643_gshared/* 3511*/,
	(methodPointerType)&Enumerator_Dispose_m25644_gshared/* 3512*/,
	(methodPointerType)&Enumerator_MoveNext_m25645_gshared/* 3513*/,
	(methodPointerType)&Enumerator_get_Current_m25646_gshared/* 3514*/,
	(methodPointerType)&Enumerator__ctor_m25647_gshared/* 3515*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25648_gshared/* 3516*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649_gshared/* 3517*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650_gshared/* 3518*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651_gshared/* 3519*/,
	(methodPointerType)&Enumerator_MoveNext_m25652_gshared/* 3520*/,
	(methodPointerType)&Enumerator_get_Current_m25653_gshared/* 3521*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m25654_gshared/* 3522*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m25655_gshared/* 3523*/,
	(methodPointerType)&Enumerator_VerifyState_m25656_gshared/* 3524*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m25657_gshared/* 3525*/,
	(methodPointerType)&Enumerator_Dispose_m25658_gshared/* 3526*/,
	(methodPointerType)&Transform_1__ctor_m25659_gshared/* 3527*/,
	(methodPointerType)&Transform_1_Invoke_m25660_gshared/* 3528*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25661_gshared/* 3529*/,
	(methodPointerType)&Transform_1_EndInvoke_m25662_gshared/* 3530*/,
	(methodPointerType)&ValueCollection__ctor_m25663_gshared/* 3531*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m25664_gshared/* 3532*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m25665_gshared/* 3533*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m25666_gshared/* 3534*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m25667_gshared/* 3535*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m25668_gshared/* 3536*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m25669_gshared/* 3537*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m25670_gshared/* 3538*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m25671_gshared/* 3539*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m25672_gshared/* 3540*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m25673_gshared/* 3541*/,
	(methodPointerType)&ValueCollection_CopyTo_m25674_gshared/* 3542*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m25675_gshared/* 3543*/,
	(methodPointerType)&ValueCollection_get_Count_m25676_gshared/* 3544*/,
	(methodPointerType)&Enumerator__ctor_m25677_gshared/* 3545*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m25678_gshared/* 3546*/,
	(methodPointerType)&Enumerator_Dispose_m25679_gshared/* 3547*/,
	(methodPointerType)&Enumerator_MoveNext_m25680_gshared/* 3548*/,
	(methodPointerType)&Enumerator_get_Current_m25681_gshared/* 3549*/,
	(methodPointerType)&Transform_1__ctor_m25682_gshared/* 3550*/,
	(methodPointerType)&Transform_1_Invoke_m25683_gshared/* 3551*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25684_gshared/* 3552*/,
	(methodPointerType)&Transform_1_EndInvoke_m25685_gshared/* 3553*/,
	(methodPointerType)&Transform_1__ctor_m25686_gshared/* 3554*/,
	(methodPointerType)&Transform_1_Invoke_m25687_gshared/* 3555*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25688_gshared/* 3556*/,
	(methodPointerType)&Transform_1_EndInvoke_m25689_gshared/* 3557*/,
	(methodPointerType)&Transform_1__ctor_m25690_gshared/* 3558*/,
	(methodPointerType)&Transform_1_Invoke_m25691_gshared/* 3559*/,
	(methodPointerType)&Transform_1_BeginInvoke_m25692_gshared/* 3560*/,
	(methodPointerType)&Transform_1_EndInvoke_m25693_gshared/* 3561*/,
	(methodPointerType)&ShimEnumerator__ctor_m25694_gshared/* 3562*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m25695_gshared/* 3563*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m25696_gshared/* 3564*/,
	(methodPointerType)&ShimEnumerator_get_Key_m25697_gshared/* 3565*/,
	(methodPointerType)&ShimEnumerator_get_Value_m25698_gshared/* 3566*/,
	(methodPointerType)&ShimEnumerator_get_Current_m25699_gshared/* 3567*/,
	(methodPointerType)&EqualityComparer_1__ctor_m25700_gshared/* 3568*/,
	(methodPointerType)&EqualityComparer_1__cctor_m25701_gshared/* 3569*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25702_gshared/* 3570*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25703_gshared/* 3571*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m25704_gshared/* 3572*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m25705_gshared/* 3573*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m25706_gshared/* 3574*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m25707_gshared/* 3575*/,
	(methodPointerType)&DefaultComparer__ctor_m25708_gshared/* 3576*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m25709_gshared/* 3577*/,
	(methodPointerType)&DefaultComparer_Equals_m25710_gshared/* 3578*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25766_gshared/* 3579*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25767_gshared/* 3580*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25768_gshared/* 3581*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25769_gshared/* 3582*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25770_gshared/* 3583*/,
	(methodPointerType)&Comparer_1__ctor_m25781_gshared/* 3584*/,
	(methodPointerType)&Comparer_1__cctor_m25782_gshared/* 3585*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m25783_gshared/* 3586*/,
	(methodPointerType)&Comparer_1_get_Default_m25784_gshared/* 3587*/,
	(methodPointerType)&GenericComparer_1__ctor_m25785_gshared/* 3588*/,
	(methodPointerType)&GenericComparer_1_Compare_m25786_gshared/* 3589*/,
	(methodPointerType)&DefaultComparer__ctor_m25787_gshared/* 3590*/,
	(methodPointerType)&DefaultComparer_Compare_m25788_gshared/* 3591*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25789_gshared/* 3592*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25790_gshared/* 3593*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25791_gshared/* 3594*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25792_gshared/* 3595*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25793_gshared/* 3596*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25794_gshared/* 3597*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25795_gshared/* 3598*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25796_gshared/* 3599*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25797_gshared/* 3600*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25798_gshared/* 3601*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25819_gshared/* 3602*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25820_gshared/* 3603*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25821_gshared/* 3604*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25822_gshared/* 3605*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25823_gshared/* 3606*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25824_gshared/* 3607*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25825_gshared/* 3608*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25826_gshared/* 3609*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25827_gshared/* 3610*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25828_gshared/* 3611*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25829_gshared/* 3612*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25830_gshared/* 3613*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25831_gshared/* 3614*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25832_gshared/* 3615*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25833_gshared/* 3616*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25834_gshared/* 3617*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25835_gshared/* 3618*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25836_gshared/* 3619*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25837_gshared/* 3620*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25838_gshared/* 3621*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25870_gshared/* 3622*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25871_gshared/* 3623*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25872_gshared/* 3624*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25873_gshared/* 3625*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25874_gshared/* 3626*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25900_gshared/* 3627*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25901_gshared/* 3628*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25902_gshared/* 3629*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25903_gshared/* 3630*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25904_gshared/* 3631*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25905_gshared/* 3632*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25906_gshared/* 3633*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25907_gshared/* 3634*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25908_gshared/* 3635*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25909_gshared/* 3636*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25978_gshared/* 3637*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25979_gshared/* 3638*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25980_gshared/* 3639*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25981_gshared/* 3640*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25982_gshared/* 3641*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25983_gshared/* 3642*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25984_gshared/* 3643*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25985_gshared/* 3644*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25986_gshared/* 3645*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25987_gshared/* 3646*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m25988_gshared/* 3647*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25989_gshared/* 3648*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m25990_gshared/* 3649*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m25991_gshared/* 3650*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m25992_gshared/* 3651*/,
	(methodPointerType)&GenericComparer_1_Compare_m26102_gshared/* 3652*/,
	(methodPointerType)&Comparer_1__ctor_m26103_gshared/* 3653*/,
	(methodPointerType)&Comparer_1__cctor_m26104_gshared/* 3654*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m26105_gshared/* 3655*/,
	(methodPointerType)&Comparer_1_get_Default_m26106_gshared/* 3656*/,
	(methodPointerType)&DefaultComparer__ctor_m26107_gshared/* 3657*/,
	(methodPointerType)&DefaultComparer_Compare_m26108_gshared/* 3658*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m26109_gshared/* 3659*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m26110_gshared/* 3660*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26111_gshared/* 3661*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26112_gshared/* 3662*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26113_gshared/* 3663*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26114_gshared/* 3664*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26115_gshared/* 3665*/,
	(methodPointerType)&DefaultComparer__ctor_m26116_gshared/* 3666*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26117_gshared/* 3667*/,
	(methodPointerType)&DefaultComparer_Equals_m26118_gshared/* 3668*/,
	(methodPointerType)&GenericComparer_1_Compare_m26119_gshared/* 3669*/,
	(methodPointerType)&Comparer_1__ctor_m26120_gshared/* 3670*/,
	(methodPointerType)&Comparer_1__cctor_m26121_gshared/* 3671*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m26122_gshared/* 3672*/,
	(methodPointerType)&Comparer_1_get_Default_m26123_gshared/* 3673*/,
	(methodPointerType)&DefaultComparer__ctor_m26124_gshared/* 3674*/,
	(methodPointerType)&DefaultComparer_Compare_m26125_gshared/* 3675*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m26126_gshared/* 3676*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m26127_gshared/* 3677*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26128_gshared/* 3678*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26129_gshared/* 3679*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26130_gshared/* 3680*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26131_gshared/* 3681*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26132_gshared/* 3682*/,
	(methodPointerType)&DefaultComparer__ctor_m26133_gshared/* 3683*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26134_gshared/* 3684*/,
	(methodPointerType)&DefaultComparer_Equals_m26135_gshared/* 3685*/,
	(methodPointerType)&GenericComparer_1_Compare_m26136_gshared/* 3686*/,
	(methodPointerType)&Comparer_1__ctor_m26137_gshared/* 3687*/,
	(methodPointerType)&Comparer_1__cctor_m26138_gshared/* 3688*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m26139_gshared/* 3689*/,
	(methodPointerType)&Comparer_1_get_Default_m26140_gshared/* 3690*/,
	(methodPointerType)&DefaultComparer__ctor_m26141_gshared/* 3691*/,
	(methodPointerType)&DefaultComparer_Compare_m26142_gshared/* 3692*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m26143_gshared/* 3693*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m26144_gshared/* 3694*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26145_gshared/* 3695*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26146_gshared/* 3696*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26147_gshared/* 3697*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26148_gshared/* 3698*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26149_gshared/* 3699*/,
	(methodPointerType)&DefaultComparer__ctor_m26150_gshared/* 3700*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26151_gshared/* 3701*/,
	(methodPointerType)&DefaultComparer_Equals_m26152_gshared/* 3702*/,
	(methodPointerType)&GenericComparer_1_Compare_m26153_gshared/* 3703*/,
	(methodPointerType)&Comparer_1__ctor_m26154_gshared/* 3704*/,
	(methodPointerType)&Comparer_1__cctor_m26155_gshared/* 3705*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m26156_gshared/* 3706*/,
	(methodPointerType)&Comparer_1_get_Default_m26157_gshared/* 3707*/,
	(methodPointerType)&DefaultComparer__ctor_m26158_gshared/* 3708*/,
	(methodPointerType)&DefaultComparer_Compare_m26159_gshared/* 3709*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m26160_gshared/* 3710*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m26161_gshared/* 3711*/,
	(methodPointerType)&EqualityComparer_1__ctor_m26162_gshared/* 3712*/,
	(methodPointerType)&EqualityComparer_1__cctor_m26163_gshared/* 3713*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m26164_gshared/* 3714*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m26165_gshared/* 3715*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m26166_gshared/* 3716*/,
	(methodPointerType)&DefaultComparer__ctor_m26167_gshared/* 3717*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m26168_gshared/* 3718*/,
	(methodPointerType)&DefaultComparer_Equals_m26169_gshared/* 3719*/,
};
const InvokerMethod g_Il2CppInvokerPointers[498] = 
{
	NULL/* 0*/,
	RuntimeInvoker_Object_t/* 1*/,
	RuntimeInvoker_Void_t260_Object_t/* 2*/,
	RuntimeInvoker_Void_t260/* 3*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t/* 4*/,
	RuntimeInvoker_Object_t_Object_t/* 5*/,
	RuntimeInvoker_Int32_t189/* 6*/,
	RuntimeInvoker_Boolean_t203/* 7*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t3407/* 8*/,
	RuntimeInvoker_Boolean_t203_Object_t_ObjectU26_t3349/* 9*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407/* 10*/,
	RuntimeInvoker_Boolean_t203_Object_t/* 11*/,
	RuntimeInvoker_Void_t260_Object_t_Int32_t189/* 12*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Object_t/* 13*/,
	RuntimeInvoker_KeyValuePair_2_t3407/* 14*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* 15*/,
	RuntimeInvoker_Boolean_t203_Object_t_Object_t/* 16*/,
	RuntimeInvoker_Object_t_Object_t_Object_t/* 17*/,
	RuntimeInvoker_IntPtr_t_Object_t_Object_t/* 18*/,
	RuntimeInvoker_Object_t_IntPtr_t/* 19*/,
	RuntimeInvoker_Object_t_IntPtr_t_SByte_t236/* 20*/,
	RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_IntPtr_t_IntPtr_t/* 21*/,
	RuntimeInvoker_Void_t260_IntPtr_t/* 22*/,
	RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* 23*/,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t/* 24*/,
	RuntimeInvoker_UIntPtr_t_Object_t/* 25*/,
	RuntimeInvoker_UIntPtr_t_Object_t_IntPtr_t/* 26*/,
	RuntimeInvoker_Object_t_Object_t_IntPtr_t_Object_t_Object_t/* 27*/,
	RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t/* 28*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* 29*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* 30*/,
	RuntimeInvoker_Boolean_t203_ObjectU26_t3349_Object_t/* 31*/,
	RuntimeInvoker_Void_t260_ObjectU26_t3349_Object_t/* 32*/,
	RuntimeInvoker_Object_t_Int32_t189/* 33*/,
	RuntimeInvoker_Void_t260_Int32_t189_Object_t/* 34*/,
	RuntimeInvoker_Int32_t189_Object_t/* 35*/,
	RuntimeInvoker_Void_t260_Int32_t189/* 36*/,
	RuntimeInvoker_Void_t260_SByte_t236_Object_t/* 37*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 38*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t/* 39*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 40*/,
	RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* 41*/,
	RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Object_t/* 42*/,
	RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189/* 43*/,
	RuntimeInvoker_Int32_t189_Int32_t189/* 44*/,
	RuntimeInvoker_Boolean_t203_Int32_t189/* 45*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189/* 46*/,
	RuntimeInvoker_Int64_t233_Object_t/* 47*/,
	RuntimeInvoker_Enumerator_t3880/* 48*/,
	RuntimeInvoker_Void_t260_Int32_t189_ObjectU26_t3349/* 49*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189/* 50*/,
	RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t/* 51*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189_Object_t/* 52*/,
	RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t/* 53*/,
	RuntimeInvoker_Int32_t189_Object_t_Object_t_Object_t/* 54*/,
	RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189/* 55*/,
	RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189_Int32_t189/* 56*/,
	RuntimeInvoker_Int32_t189_Object_t_Object_t/* 57*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t/* 58*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t/* 59*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Object_t/* 60*/,
	RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189/* 61*/,
	RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189/* 62*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Object_t_Object_t/* 63*/,
	RuntimeInvoker_Enumerator_t3486/* 64*/,
	RuntimeInvoker_DictionaryEntry_t2128_Object_t_Object_t/* 65*/,
	RuntimeInvoker_DictionaryEntry_t2128/* 66*/,
	RuntimeInvoker_Enumerator_t3485/* 67*/,
	RuntimeInvoker_Enumerator_t3489/* 68*/,
	RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Object_t/* 69*/,
	RuntimeInvoker_Enumerator_t3414/* 70*/,
	RuntimeInvoker_Void_t260_Int32_t189_Int32_t189/* 71*/,
	RuntimeInvoker_Int32_t189_Object_t_Int16_t238/* 72*/,
	RuntimeInvoker_Void_t260_TimeSpan_t190/* 73*/,
	RuntimeInvoker_PlayGamesClientConfiguration_t366_PlayGamesClientConfiguration_t366/* 74*/,
	RuntimeInvoker_Void_t260_Object_t_SByte_t236/* 75*/,
	RuntimeInvoker_TimeSpan_t190/* 76*/,
	RuntimeInvoker_Void_t260_DateTime_t48/* 77*/,
	RuntimeInvoker_Enumerator_t3929/* 78*/,
	RuntimeInvoker_Enumerator_t3925/* 79*/,
	RuntimeInvoker_KeyValuePair_2_t3921/* 80*/,
	RuntimeInvoker_Void_t260_Color_t747/* 81*/,
	RuntimeInvoker_Void_t260_ColorTween_t1209/* 82*/,
	RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32_t189/* 83*/,
	RuntimeInvoker_Boolean_t203_ByteU26_t2328_SByte_t236/* 84*/,
	RuntimeInvoker_Boolean_t203_SingleU26_t316_Single_t202/* 85*/,
	RuntimeInvoker_Boolean_t203_UInt16U26_t3017_Int16_t238/* 86*/,
	RuntimeInvoker_Void_t260_Single_t202/* 87*/,
	RuntimeInvoker_Void_t260_Vector2_t739/* 88*/,
	RuntimeInvoker_Boolean_t203_NavigationU26_t4645_Navigation_t1272/* 89*/,
	RuntimeInvoker_Boolean_t203_ColorBlockU26_t4646_ColorBlock_t1221/* 90*/,
	RuntimeInvoker_Boolean_t203_SpriteStateU26_t4647_SpriteState_t1290/* 91*/,
	RuntimeInvoker_Void_t260_SByte_t236/* 92*/,
	RuntimeInvoker_Void_t260_Int32U26_t317_Int32_t189/* 93*/,
	RuntimeInvoker_Void_t260_Vector2U26_t1725_Vector2_t739/* 94*/,
	RuntimeInvoker_Void_t260_SingleU26_t316_Single_t202/* 95*/,
	RuntimeInvoker_Void_t260_ByteU26_t2328_SByte_t236/* 96*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Single_t202/* 97*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189/* 98*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236/* 99*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189/* 100*/,
	RuntimeInvoker_KeyValuePair_2_t3407_Int32_t189/* 101*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407/* 102*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3407/* 103*/,
	RuntimeInvoker_Double_t234_Int32_t189/* 104*/,
	RuntimeInvoker_Void_t260_Double_t234/* 105*/,
	RuntimeInvoker_Boolean_t203_Double_t234/* 106*/,
	RuntimeInvoker_Int32_t189_Double_t234/* 107*/,
	RuntimeInvoker_Void_t260_Int32_t189_Double_t234/* 108*/,
	RuntimeInvoker_UInt16_t194_Int32_t189/* 109*/,
	RuntimeInvoker_Void_t260_Int16_t238/* 110*/,
	RuntimeInvoker_Boolean_t203_Int16_t238/* 111*/,
	RuntimeInvoker_Int32_t189_Int16_t238/* 112*/,
	RuntimeInvoker_Void_t260_Int32_t189_Int16_t238/* 113*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189/* 114*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189_Int32_t189/* 115*/,
	RuntimeInvoker_Int32_t189_Object_t_KeyValuePair_2_t3407_Int32_t189_Int32_t189/* 116*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t/* 117*/,
	RuntimeInvoker_DictionaryEntry_t2128_Int32_t189/* 118*/,
	RuntimeInvoker_Void_t260_DictionaryEntry_t2128/* 119*/,
	RuntimeInvoker_Boolean_t203_DictionaryEntry_t2128/* 120*/,
	RuntimeInvoker_Int32_t189_DictionaryEntry_t2128/* 121*/,
	RuntimeInvoker_Void_t260_Int32_t189_DictionaryEntry_t2128/* 122*/,
	RuntimeInvoker_Int32_t189_Object_t_Int16_t238_Int32_t189_Int32_t189/* 123*/,
	RuntimeInvoker_KeyValuePair_2_t3531_Int32_t189/* 124*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t3531/* 125*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3531/* 126*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3531/* 127*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3531/* 128*/,
	RuntimeInvoker_Byte_t237_Int32_t189/* 129*/,
	RuntimeInvoker_Boolean_t203_SByte_t236/* 130*/,
	RuntimeInvoker_Int32_t189_SByte_t236/* 131*/,
	RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* 132*/,
	RuntimeInvoker_Link_t3579_Int32_t189/* 133*/,
	RuntimeInvoker_Void_t260_Link_t3579/* 134*/,
	RuntimeInvoker_Boolean_t203_Link_t3579/* 135*/,
	RuntimeInvoker_Int32_t189_Link_t3579/* 136*/,
	RuntimeInvoker_Void_t260_Int32_t189_Link_t3579/* 137*/,
	RuntimeInvoker_KeyValuePair_2_t3656_Int32_t189/* 138*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t3656/* 139*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3656/* 140*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3656/* 141*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3656/* 142*/,
	RuntimeInvoker_UInt32_t235_Int32_t189/* 143*/,
	RuntimeInvoker_IntPtr_t_Int32_t189/* 144*/,
	RuntimeInvoker_Boolean_t203_IntPtr_t/* 145*/,
	RuntimeInvoker_Int32_t189_IntPtr_t/* 146*/,
	RuntimeInvoker_Void_t260_Int32_t189_IntPtr_t/* 147*/,
	RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t/* 148*/,
	RuntimeInvoker_Link_t3760_Int32_t189/* 149*/,
	RuntimeInvoker_Void_t260_Link_t3760/* 150*/,
	RuntimeInvoker_Boolean_t203_Link_t3760/* 151*/,
	RuntimeInvoker_Int32_t189_Link_t3760/* 152*/,
	RuntimeInvoker_Void_t260_Int32_t189_Link_t3760/* 153*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189/* 154*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t3790/* 155*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3790/* 156*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3790/* 157*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3790/* 158*/,
	RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189/* 159*/,
	RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189_Int32_t189/* 160*/,
	RuntimeInvoker_Int32_t189_Object_t_IntPtr_t_Int32_t189_Int32_t189/* 161*/,
	RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t_Object_t/* 162*/,
	RuntimeInvoker_Matrix4x4_t997_Int32_t189/* 163*/,
	RuntimeInvoker_Void_t260_Matrix4x4_t997/* 164*/,
	RuntimeInvoker_Boolean_t203_Matrix4x4_t997/* 165*/,
	RuntimeInvoker_Int32_t189_Matrix4x4_t997/* 166*/,
	RuntimeInvoker_Void_t260_Int32_t189_Matrix4x4_t997/* 167*/,
	RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189/* 168*/,
	RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189_Int32_t189/* 169*/,
	RuntimeInvoker_Int32_t189_Object_t_Matrix4x4_t997_Int32_t189_Int32_t189/* 170*/,
	RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997_Object_t/* 171*/,
	RuntimeInvoker_Vector3_t758_Int32_t189/* 172*/,
	RuntimeInvoker_Void_t260_Vector3_t758/* 173*/,
	RuntimeInvoker_Boolean_t203_Vector3_t758/* 174*/,
	RuntimeInvoker_Int32_t189_Vector3_t758/* 175*/,
	RuntimeInvoker_Void_t260_Int32_t189_Vector3_t758/* 176*/,
	RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189/* 177*/,
	RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189_Int32_t189/* 178*/,
	RuntimeInvoker_Int32_t189_Object_t_Vector3_t758_Int32_t189_Int32_t189/* 179*/,
	RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758_Object_t/* 180*/,
	RuntimeInvoker_Touch_t901_Int32_t189/* 181*/,
	RuntimeInvoker_Void_t260_Touch_t901/* 182*/,
	RuntimeInvoker_Boolean_t203_Touch_t901/* 183*/,
	RuntimeInvoker_Int32_t189_Touch_t901/* 184*/,
	RuntimeInvoker_Void_t260_Int32_t189_Touch_t901/* 185*/,
	RuntimeInvoker_RaycastResult_t1187_Int32_t189/* 186*/,
	RuntimeInvoker_Void_t260_RaycastResult_t1187/* 187*/,
	RuntimeInvoker_Boolean_t203_RaycastResult_t1187/* 188*/,
	RuntimeInvoker_Int32_t189_RaycastResult_t1187/* 189*/,
	RuntimeInvoker_Void_t260_Int32_t189_RaycastResult_t1187/* 190*/,
	RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189/* 191*/,
	RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189_Int32_t189/* 192*/,
	RuntimeInvoker_Int32_t189_Object_t_RaycastResult_t1187_Int32_t189_Int32_t189/* 193*/,
	RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187_Object_t/* 194*/,
	RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189/* 195*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t3921/* 196*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3921/* 197*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3921/* 198*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3921/* 199*/,
	RuntimeInvoker_RaycastHit2D_t1378_Int32_t189/* 200*/,
	RuntimeInvoker_Void_t260_RaycastHit2D_t1378/* 201*/,
	RuntimeInvoker_Boolean_t203_RaycastHit2D_t1378/* 202*/,
	RuntimeInvoker_Int32_t189_RaycastHit2D_t1378/* 203*/,
	RuntimeInvoker_Void_t260_Int32_t189_RaycastHit2D_t1378/* 204*/,
	RuntimeInvoker_RaycastHit_t990_Int32_t189/* 205*/,
	RuntimeInvoker_Void_t260_RaycastHit_t990/* 206*/,
	RuntimeInvoker_Boolean_t203_RaycastHit_t990/* 207*/,
	RuntimeInvoker_Int32_t189_RaycastHit_t990/* 208*/,
	RuntimeInvoker_Void_t260_Int32_t189_RaycastHit_t990/* 209*/,
	RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189/* 210*/,
	RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189_Int32_t189/* 211*/,
	RuntimeInvoker_Int32_t189_Object_t_UIVertex_t1265_Int32_t189_Int32_t189/* 212*/,
	RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265_Object_t/* 213*/,
	RuntimeInvoker_Vector2_t739_Int32_t189/* 214*/,
	RuntimeInvoker_Boolean_t203_Vector2_t739/* 215*/,
	RuntimeInvoker_Int32_t189_Vector2_t739/* 216*/,
	RuntimeInvoker_Void_t260_Int32_t189_Vector2_t739/* 217*/,
	RuntimeInvoker_UILineInfo_t1398_Int32_t189/* 218*/,
	RuntimeInvoker_Void_t260_UILineInfo_t1398/* 219*/,
	RuntimeInvoker_Boolean_t203_UILineInfo_t1398/* 220*/,
	RuntimeInvoker_Int32_t189_UILineInfo_t1398/* 221*/,
	RuntimeInvoker_Void_t260_Int32_t189_UILineInfo_t1398/* 222*/,
	RuntimeInvoker_UICharInfo_t1400_Int32_t189/* 223*/,
	RuntimeInvoker_Void_t260_UICharInfo_t1400/* 224*/,
	RuntimeInvoker_Boolean_t203_UICharInfo_t1400/* 225*/,
	RuntimeInvoker_Int32_t189_UICharInfo_t1400/* 226*/,
	RuntimeInvoker_Void_t260_Int32_t189_UICharInfo_t1400/* 227*/,
	RuntimeInvoker_GcAchievementData_t1617_Int32_t189/* 228*/,
	RuntimeInvoker_Void_t260_GcAchievementData_t1617/* 229*/,
	RuntimeInvoker_Boolean_t203_GcAchievementData_t1617/* 230*/,
	RuntimeInvoker_Int32_t189_GcAchievementData_t1617/* 231*/,
	RuntimeInvoker_Void_t260_Int32_t189_GcAchievementData_t1617/* 232*/,
	RuntimeInvoker_GcScoreData_t1618_Int32_t189/* 233*/,
	RuntimeInvoker_Void_t260_GcScoreData_t1618/* 234*/,
	RuntimeInvoker_Boolean_t203_GcScoreData_t1618/* 235*/,
	RuntimeInvoker_Int32_t189_GcScoreData_t1618/* 236*/,
	RuntimeInvoker_Void_t260_Int32_t189_GcScoreData_t1618/* 237*/,
	RuntimeInvoker_Single_t202_Int32_t189/* 238*/,
	RuntimeInvoker_Boolean_t203_Single_t202/* 239*/,
	RuntimeInvoker_Int32_t189_Single_t202/* 240*/,
	RuntimeInvoker_Void_t260_Int32_t189_Single_t202/* 241*/,
	RuntimeInvoker_Keyframe_t1591_Int32_t189/* 242*/,
	RuntimeInvoker_Void_t260_Keyframe_t1591/* 243*/,
	RuntimeInvoker_Boolean_t203_Keyframe_t1591/* 244*/,
	RuntimeInvoker_Int32_t189_Keyframe_t1591/* 245*/,
	RuntimeInvoker_Void_t260_Int32_t189_Keyframe_t1591/* 246*/,
	RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189/* 247*/,
	RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189_Int32_t189/* 248*/,
	RuntimeInvoker_Int32_t189_Object_t_UICharInfo_t1400_Int32_t189_Int32_t189/* 249*/,
	RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400_Object_t/* 250*/,
	RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189/* 251*/,
	RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189_Int32_t189/* 252*/,
	RuntimeInvoker_Int32_t189_Object_t_UILineInfo_t1398_Int32_t189_Int32_t189/* 253*/,
	RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398_Object_t/* 254*/,
	RuntimeInvoker_ParameterModifier_t2550_Int32_t189/* 255*/,
	RuntimeInvoker_Void_t260_ParameterModifier_t2550/* 256*/,
	RuntimeInvoker_Boolean_t203_ParameterModifier_t2550/* 257*/,
	RuntimeInvoker_Int32_t189_ParameterModifier_t2550/* 258*/,
	RuntimeInvoker_Void_t260_Int32_t189_ParameterModifier_t2550/* 259*/,
	RuntimeInvoker_HitInfo_t1629_Int32_t189/* 260*/,
	RuntimeInvoker_Void_t260_HitInfo_t1629/* 261*/,
	RuntimeInvoker_Boolean_t203_HitInfo_t1629/* 262*/,
	RuntimeInvoker_Int32_t189_HitInfo_t1629/* 263*/,
	RuntimeInvoker_Void_t260_Int32_t189_HitInfo_t1629/* 264*/,
	RuntimeInvoker_KeyValuePair_2_t4171_Int32_t189/* 265*/,
	RuntimeInvoker_Void_t260_KeyValuePair_2_t4171/* 266*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t4171/* 267*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t4171/* 268*/,
	RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t4171/* 269*/,
	RuntimeInvoker_X509ChainStatus_t2034_Int32_t189/* 270*/,
	RuntimeInvoker_Void_t260_X509ChainStatus_t2034/* 271*/,
	RuntimeInvoker_Boolean_t203_X509ChainStatus_t2034/* 272*/,
	RuntimeInvoker_Int32_t189_X509ChainStatus_t2034/* 273*/,
	RuntimeInvoker_Void_t260_Int32_t189_X509ChainStatus_t2034/* 274*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Object_t/* 275*/,
	RuntimeInvoker_Mark_t2083_Int32_t189/* 276*/,
	RuntimeInvoker_Void_t260_Mark_t2083/* 277*/,
	RuntimeInvoker_Boolean_t203_Mark_t2083/* 278*/,
	RuntimeInvoker_Int32_t189_Mark_t2083/* 279*/,
	RuntimeInvoker_Void_t260_Int32_t189_Mark_t2083/* 280*/,
	RuntimeInvoker_UriScheme_t2118_Int32_t189/* 281*/,
	RuntimeInvoker_Void_t260_UriScheme_t2118/* 282*/,
	RuntimeInvoker_Boolean_t203_UriScheme_t2118/* 283*/,
	RuntimeInvoker_Int32_t189_UriScheme_t2118/* 284*/,
	RuntimeInvoker_Void_t260_Int32_t189_UriScheme_t2118/* 285*/,
	RuntimeInvoker_UInt64_t239_Int32_t189/* 286*/,
	RuntimeInvoker_Void_t260_Int64_t233/* 287*/,
	RuntimeInvoker_Boolean_t203_Int64_t233/* 288*/,
	RuntimeInvoker_Int32_t189_Int64_t233/* 289*/,
	RuntimeInvoker_Void_t260_Int32_t189_Int64_t233/* 290*/,
	RuntimeInvoker_Int16_t238_Int32_t189/* 291*/,
	RuntimeInvoker_SByte_t236_Int32_t189/* 292*/,
	RuntimeInvoker_Int64_t233_Int32_t189/* 293*/,
	RuntimeInvoker_TableRange_t2360_Int32_t189/* 294*/,
	RuntimeInvoker_Void_t260_TableRange_t2360/* 295*/,
	RuntimeInvoker_Boolean_t203_TableRange_t2360/* 296*/,
	RuntimeInvoker_Int32_t189_TableRange_t2360/* 297*/,
	RuntimeInvoker_Void_t260_Int32_t189_TableRange_t2360/* 298*/,
	RuntimeInvoker_Slot_t2436_Int32_t189/* 299*/,
	RuntimeInvoker_Void_t260_Slot_t2436/* 300*/,
	RuntimeInvoker_Boolean_t203_Slot_t2436/* 301*/,
	RuntimeInvoker_Int32_t189_Slot_t2436/* 302*/,
	RuntimeInvoker_Void_t260_Int32_t189_Slot_t2436/* 303*/,
	RuntimeInvoker_Slot_t2443_Int32_t189/* 304*/,
	RuntimeInvoker_Void_t260_Slot_t2443/* 305*/,
	RuntimeInvoker_Boolean_t203_Slot_t2443/* 306*/,
	RuntimeInvoker_Int32_t189_Slot_t2443/* 307*/,
	RuntimeInvoker_Void_t260_Int32_t189_Slot_t2443/* 308*/,
	RuntimeInvoker_DateTime_t48_Int32_t189/* 309*/,
	RuntimeInvoker_Boolean_t203_DateTime_t48/* 310*/,
	RuntimeInvoker_Int32_t189_DateTime_t48/* 311*/,
	RuntimeInvoker_Void_t260_Int32_t189_DateTime_t48/* 312*/,
	RuntimeInvoker_Decimal_t240_Int32_t189/* 313*/,
	RuntimeInvoker_Void_t260_Decimal_t240/* 314*/,
	RuntimeInvoker_Boolean_t203_Decimal_t240/* 315*/,
	RuntimeInvoker_Int32_t189_Decimal_t240/* 316*/,
	RuntimeInvoker_Void_t260_Int32_t189_Decimal_t240/* 317*/,
	RuntimeInvoker_TimeSpan_t190_Int32_t189/* 318*/,
	RuntimeInvoker_Boolean_t203_TimeSpan_t190/* 319*/,
	RuntimeInvoker_Int32_t189_TimeSpan_t190/* 320*/,
	RuntimeInvoker_Void_t260_Int32_t189_TimeSpan_t190/* 321*/,
	RuntimeInvoker_Double_t234/* 322*/,
	RuntimeInvoker_UInt16_t194/* 323*/,
	RuntimeInvoker_Enumerator_t3440/* 324*/,
	RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407_KeyValuePair_2_t3407/* 325*/,
	RuntimeInvoker_KeyValuePair_2_t3407_KeyValuePair_2_t3407/* 326*/,
	RuntimeInvoker_Object_t_KeyValuePair_2_t3407_Object_t_Object_t/* 327*/,
	RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407/* 328*/,
	RuntimeInvoker_Object_t_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t_Object_t/* 329*/,
	RuntimeInvoker_DictionaryEntry_t2128_Object_t/* 330*/,
	RuntimeInvoker_Boolean_t203_Int16_t238_Int16_t238/* 331*/,
	RuntimeInvoker_KeyValuePair_2_t3531_Object_t_Int32_t189/* 332*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t189/* 333*/,
	RuntimeInvoker_Int32_t189_Object_t_Int32_t189/* 334*/,
	RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317/* 335*/,
	RuntimeInvoker_Enumerator_t3535/* 336*/,
	RuntimeInvoker_DictionaryEntry_t2128_Object_t_Int32_t189/* 337*/,
	RuntimeInvoker_KeyValuePair_2_t3531/* 338*/,
	RuntimeInvoker_Enumerator_t3534/* 339*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t/* 340*/,
	RuntimeInvoker_Enumerator_t3538/* 341*/,
	RuntimeInvoker_KeyValuePair_2_t3531_Object_t/* 342*/,
	RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189/* 343*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Object_t_Object_t/* 344*/,
	RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t/* 345*/,
	RuntimeInvoker_Byte_t237/* 346*/,
	RuntimeInvoker_Link_t3579/* 347*/,
	RuntimeInvoker_Byte_t237_Object_t/* 348*/,
	RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t/* 349*/,
	RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t/* 350*/,
	RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t_Object_t/* 351*/,
	RuntimeInvoker_UInt32_t235_Object_t/* 352*/,
	RuntimeInvoker_KeyValuePair_2_t3656_Object_t_Int32_t189/* 353*/,
	RuntimeInvoker_UInt32_t235_Object_t_Int32_t189/* 354*/,
	RuntimeInvoker_Boolean_t203_Object_t_UInt32U26_t318/* 355*/,
	RuntimeInvoker_Enumerator_t3661/* 356*/,
	RuntimeInvoker_KeyValuePair_2_t3656/* 357*/,
	RuntimeInvoker_UInt32_t235/* 358*/,
	RuntimeInvoker_Enumerator_t3660/* 359*/,
	RuntimeInvoker_Enumerator_t3664/* 360*/,
	RuntimeInvoker_KeyValuePair_2_t3656_Object_t/* 361*/,
	RuntimeInvoker_Boolean_t203_Nullable_1_t377/* 362*/,
	RuntimeInvoker_Void_t260_AdvertisingResult_t354/* 363*/,
	RuntimeInvoker_Object_t_AdvertisingResult_t354_Object_t_Object_t/* 364*/,
	RuntimeInvoker_Void_t260_ConnectionRequest_t355/* 365*/,
	RuntimeInvoker_Object_t_ConnectionRequest_t355_Object_t_Object_t/* 366*/,
	RuntimeInvoker_Void_t260_ConnectionResponse_t358/* 367*/,
	RuntimeInvoker_Object_t_ConnectionResponse_t358_Object_t_Object_t/* 368*/,
	RuntimeInvoker_DateTime_t48/* 369*/,
	RuntimeInvoker_Boolean_t203_Nullable_1_t873/* 370*/,
	RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t/* 371*/,
	RuntimeInvoker_IntPtr_t/* 372*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t/* 373*/,
	RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_SByte_t236/* 374*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t236_Object_t_Object_t/* 375*/,
	RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Int32_t189/* 376*/,
	RuntimeInvoker_Link_t3760/* 377*/,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t_Object_t/* 378*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189_Int32_t189/* 379*/,
	RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189/* 380*/,
	RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317/* 381*/,
	RuntimeInvoker_Enumerator_t3794/* 382*/,
	RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Int32_t189/* 383*/,
	RuntimeInvoker_KeyValuePair_2_t3790/* 384*/,
	RuntimeInvoker_Enumerator_t3793/* 385*/,
	RuntimeInvoker_Object_t_Int32_t189_Int32_t189_Object_t_Object_t/* 386*/,
	RuntimeInvoker_Object_t_Int32_t189_Int32_t189/* 387*/,
	RuntimeInvoker_Enumerator_t3798/* 388*/,
	RuntimeInvoker_KeyValuePair_2_t3790_Object_t/* 389*/,
	RuntimeInvoker_IntPtr_t_Object_t/* 390*/,
	RuntimeInvoker_Enumerator_t3808/* 391*/,
	RuntimeInvoker_Boolean_t203_IntPtr_t_IntPtr_t/* 392*/,
	RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t/* 393*/,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t_Object_t_Object_t/* 394*/,
	RuntimeInvoker_Matrix4x4_t997_Object_t/* 395*/,
	RuntimeInvoker_Enumerator_t3832/* 396*/,
	RuntimeInvoker_Matrix4x4_t997/* 397*/,
	RuntimeInvoker_Boolean_t203_Matrix4x4_t997_Matrix4x4_t997/* 398*/,
	RuntimeInvoker_Object_t_Matrix4x4_t997_Object_t_Object_t/* 399*/,
	RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997/* 400*/,
	RuntimeInvoker_Object_t_Matrix4x4_t997_Matrix4x4_t997_Object_t_Object_t/* 401*/,
	RuntimeInvoker_Vector3_t758_Object_t/* 402*/,
	RuntimeInvoker_Enumerator_t3850/* 403*/,
	RuntimeInvoker_Vector3_t758/* 404*/,
	RuntimeInvoker_Boolean_t203_Vector3_t758_Vector3_t758/* 405*/,
	RuntimeInvoker_Object_t_Vector3_t758_Object_t_Object_t/* 406*/,
	RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758/* 407*/,
	RuntimeInvoker_Object_t_Vector3_t758_Vector3_t758_Object_t_Object_t/* 408*/,
	RuntimeInvoker_Touch_t901/* 409*/,
	RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187/* 410*/,
	RuntimeInvoker_Object_t_RaycastResult_t1187_RaycastResult_t1187_Object_t_Object_t/* 411*/,
	RuntimeInvoker_RaycastResult_t1187_Object_t/* 412*/,
	RuntimeInvoker_Enumerator_t3890/* 413*/,
	RuntimeInvoker_RaycastResult_t1187/* 414*/,
	RuntimeInvoker_Boolean_t203_RaycastResult_t1187_RaycastResult_t1187/* 415*/,
	RuntimeInvoker_Object_t_RaycastResult_t1187_Object_t_Object_t/* 416*/,
	RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189_Object_t/* 417*/,
	RuntimeInvoker_Int32_t189_Int32_t189_Object_t/* 418*/,
	RuntimeInvoker_Object_t_Int32_t189_Object_t/* 419*/,
	RuntimeInvoker_Boolean_t203_Int32_t189_ObjectU26_t3349/* 420*/,
	RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Object_t/* 421*/,
	RuntimeInvoker_Enumerator_t3924/* 422*/,
	RuntimeInvoker_KeyValuePair_2_t3921_Object_t/* 423*/,
	RuntimeInvoker_RaycastHit2D_t1378/* 424*/,
	RuntimeInvoker_Int32_t189_RaycastHit_t990_RaycastHit_t990/* 425*/,
	RuntimeInvoker_Object_t_RaycastHit_t990_RaycastHit_t990_Object_t_Object_t/* 426*/,
	RuntimeInvoker_RaycastHit_t990/* 427*/,
	RuntimeInvoker_Object_t_Color_t747_Object_t_Object_t/* 428*/,
	RuntimeInvoker_Void_t260_UIVertex_t1265/* 429*/,
	RuntimeInvoker_Boolean_t203_UIVertex_t1265/* 430*/,
	RuntimeInvoker_UIVertex_t1265_Object_t/* 431*/,
	RuntimeInvoker_Enumerator_t3967/* 432*/,
	RuntimeInvoker_Int32_t189_UIVertex_t1265/* 433*/,
	RuntimeInvoker_Void_t260_Int32_t189_UIVertex_t1265/* 434*/,
	RuntimeInvoker_UIVertex_t1265_Int32_t189/* 435*/,
	RuntimeInvoker_UIVertex_t1265/* 436*/,
	RuntimeInvoker_Boolean_t203_UIVertex_t1265_UIVertex_t1265/* 437*/,
	RuntimeInvoker_Object_t_UIVertex_t1265_Object_t_Object_t/* 438*/,
	RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265/* 439*/,
	RuntimeInvoker_Object_t_UIVertex_t1265_UIVertex_t1265_Object_t_Object_t/* 440*/,
	RuntimeInvoker_Object_t_ColorTween_t1209/* 441*/,
	RuntimeInvoker_Vector2_t739/* 442*/,
	RuntimeInvoker_UILineInfo_t1398/* 443*/,
	RuntimeInvoker_UICharInfo_t1400/* 444*/,
	RuntimeInvoker_Object_t_Single_t202_Object_t_Object_t/* 445*/,
	RuntimeInvoker_Object_t_Vector2_t739_Object_t_Object_t/* 446*/,
	RuntimeInvoker_Single_t202_Object_t/* 447*/,
	RuntimeInvoker_GcAchievementData_t1617/* 448*/,
	RuntimeInvoker_GcScoreData_t1618/* 449*/,
	RuntimeInvoker_Single_t202/* 450*/,
	RuntimeInvoker_Keyframe_t1591/* 451*/,
	RuntimeInvoker_UICharInfo_t1400_Object_t/* 452*/,
	RuntimeInvoker_Enumerator_t4099/* 453*/,
	RuntimeInvoker_Boolean_t203_UICharInfo_t1400_UICharInfo_t1400/* 454*/,
	RuntimeInvoker_Object_t_UICharInfo_t1400_Object_t_Object_t/* 455*/,
	RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400/* 456*/,
	RuntimeInvoker_Object_t_UICharInfo_t1400_UICharInfo_t1400_Object_t_Object_t/* 457*/,
	RuntimeInvoker_UILineInfo_t1398_Object_t/* 458*/,
	RuntimeInvoker_Enumerator_t4109/* 459*/,
	RuntimeInvoker_Boolean_t203_UILineInfo_t1398_UILineInfo_t1398/* 460*/,
	RuntimeInvoker_Object_t_UILineInfo_t1398_Object_t_Object_t/* 461*/,
	RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398/* 462*/,
	RuntimeInvoker_Object_t_UILineInfo_t1398_UILineInfo_t1398_Object_t_Object_t/* 463*/,
	RuntimeInvoker_ParameterModifier_t2550/* 464*/,
	RuntimeInvoker_HitInfo_t1629/* 465*/,
	RuntimeInvoker_KeyValuePair_2_t4171_Object_t_SByte_t236/* 466*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t236/* 467*/,
	RuntimeInvoker_Byte_t237_Object_t_SByte_t236/* 468*/,
	RuntimeInvoker_Boolean_t203_Object_t_ByteU26_t2328/* 469*/,
	RuntimeInvoker_Enumerator_t4175/* 470*/,
	RuntimeInvoker_DictionaryEntry_t2128_Object_t_SByte_t236/* 471*/,
	RuntimeInvoker_KeyValuePair_2_t4171/* 472*/,
	RuntimeInvoker_Enumerator_t4174/* 473*/,
	RuntimeInvoker_Enumerator_t4178/* 474*/,
	RuntimeInvoker_KeyValuePair_2_t4171_Object_t/* 475*/,
	RuntimeInvoker_Boolean_t203_SByte_t236_SByte_t236/* 476*/,
	RuntimeInvoker_X509ChainStatus_t2034/* 477*/,
	RuntimeInvoker_Mark_t2083/* 478*/,
	RuntimeInvoker_UriScheme_t2118/* 479*/,
	RuntimeInvoker_UInt64_t239/* 480*/,
	RuntimeInvoker_Int16_t238/* 481*/,
	RuntimeInvoker_SByte_t236/* 482*/,
	RuntimeInvoker_Int64_t233/* 483*/,
	RuntimeInvoker_TableRange_t2360/* 484*/,
	RuntimeInvoker_Slot_t2436/* 485*/,
	RuntimeInvoker_Slot_t2443/* 486*/,
	RuntimeInvoker_Decimal_t240/* 487*/,
	RuntimeInvoker_Int32_t189_DateTime_t48_DateTime_t48/* 488*/,
	RuntimeInvoker_Boolean_t203_DateTime_t48_DateTime_t48/* 489*/,
	RuntimeInvoker_Int32_t189_DateTimeOffset_t2793_DateTimeOffset_t2793/* 490*/,
	RuntimeInvoker_Int32_t189_DateTimeOffset_t2793/* 491*/,
	RuntimeInvoker_Boolean_t203_DateTimeOffset_t2793_DateTimeOffset_t2793/* 492*/,
	RuntimeInvoker_Int32_t189_Guid_t2814_Guid_t2814/* 493*/,
	RuntimeInvoker_Int32_t189_Guid_t2814/* 494*/,
	RuntimeInvoker_Boolean_t203_Guid_t2814_Guid_t2814/* 495*/,
	RuntimeInvoker_Int32_t189_TimeSpan_t190_TimeSpan_t190/* 496*/,
	RuntimeInvoker_Boolean_t203_TimeSpan_t190_TimeSpan_t190/* 497*/,
};
const Il2CppCodeRegistration g_CodeRegistration = 
{
	3720,
	g_Il2CppMethodPointers,
	498,
	g_Il2CppInvokerPointers,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
