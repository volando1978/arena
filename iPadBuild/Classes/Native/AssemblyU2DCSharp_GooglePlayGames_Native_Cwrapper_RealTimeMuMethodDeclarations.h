﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback
struct RealTimeRoomCallback_t458;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C" void RealTimeRoomCallback__ctor_m1955 (RealTimeRoomCallback_t458 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void RealTimeRoomCallback_Invoke_m1956 (RealTimeRoomCallback_t458 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RealTimeRoomCallback_t458(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * RealTimeRoomCallback_BeginInvoke_m1957 (RealTimeRoomCallback_t458 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::EndInvoke(System.IAsyncResult)
extern "C" void RealTimeRoomCallback_EndInvoke_m1958 (RealTimeRoomCallback_t458 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
