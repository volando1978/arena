﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>
struct InternalEnumerator_1_t3554;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCurrencyPack
struct VirtualCurrencyPack_t78;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17291(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3554 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17292(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3554 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>::Dispose()
#define InternalEnumerator_1_Dispose_m17293(__this, method) (( void (*) (InternalEnumerator_1_t3554 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17294(__this, method) (( bool (*) (InternalEnumerator_1_t3554 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Soomla.Store.VirtualCurrencyPack>::get_Current()
#define InternalEnumerator_1_get_Current_m17295(__this, method) (( VirtualCurrencyPack_t78 * (*) (InternalEnumerator_1_t3554 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
