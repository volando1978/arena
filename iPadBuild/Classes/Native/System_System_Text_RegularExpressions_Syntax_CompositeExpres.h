﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t2095;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// System.Text.RegularExpressions.Syntax.CompositeExpression
struct  CompositeExpression_t2097  : public Expression_t2096
{
	// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::expressions
	ExpressionCollection_t2095 * ___expressions_0;
};
