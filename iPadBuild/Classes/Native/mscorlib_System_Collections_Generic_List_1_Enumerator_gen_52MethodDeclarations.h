﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>
struct Enumerator_t4162;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1647;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t1653;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m25503(__this, ___l, method) (( void (*) (Enumerator_t4162 *, List_1_t1653 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25504(__this, method) (( Object_t * (*) (Enumerator_t4162 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::Dispose()
#define Enumerator_Dispose_m25505(__this, method) (( void (*) (Enumerator_t4162 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::VerifyState()
#define Enumerator_VerifyState_m25506(__this, method) (( void (*) (Enumerator_t4162 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::MoveNext()
#define Enumerator_MoveNext_m25507(__this, method) (( bool (*) (Enumerator_t4162 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::get_Current()
#define Enumerator_get_Current_m25508(__this, method) (( BaseInvokableCall_t1647 * (*) (Enumerator_t4162 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
