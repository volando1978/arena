﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities
struct  PInvokeUtilities_t682  : public Object_t
{
};
struct PInvokeUtilities_t682_StaticFields{
	// System.DateTime GooglePlayGames.Native.PInvoke.PInvokeUtilities::UnixEpoch
	DateTime_t48  ___UnixEpoch_0;
};
