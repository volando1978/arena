﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>
struct KeyCollection_t3608;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct Dictionary_2_t111;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String[]
struct StringU5BU5D_t169;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_44.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_2MethodDeclarations.h"
#define KeyCollection__ctor_m18098(__this, ___dictionary, method) (( void (*) (KeyCollection_t3608 *, Dictionary_2_t111 *, MethodInfo*))KeyCollection__ctor_m16134_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18099(__this, ___item, method) (( void (*) (KeyCollection_t3608 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16136_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18100(__this, method) (( void (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18101(__this, ___item, method) (( bool (*) (KeyCollection_t3608 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16140_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18102(__this, ___item, method) (( bool (*) (KeyCollection_t3608 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16142_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18103(__this, method) (( Object_t* (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16144_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m18104(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3608 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m16146_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18105(__this, method) (( Object_t * (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18106(__this, method) (( bool (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18107(__this, method) (( bool (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16152_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m18108(__this, method) (( Object_t * (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m18109(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3608 *, StringU5BU5D_t169*, int32_t, MethodInfo*))KeyCollection_CopyTo_m16156_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::GetEnumerator()
#define KeyCollection_GetEnumerator_m18110(__this, method) (( Enumerator_t4331  (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_GetEnumerator_m16158_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::get_Count()
#define KeyCollection_get_Count_m18111(__this, method) (( int32_t (*) (KeyCollection_t3608 *, MethodInfo*))KeyCollection_get_Count_m16160_gshared)(__this, method)
