﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Nullable`1<System.DateTime>
struct  Nullable_1_t873 
{
	// T System.Nullable`1<System.DateTime>::value
	DateTime_t48  ___value_0;
	// System.Boolean System.Nullable`1<System.DateTime>::has_value
	bool ___has_value_1;
};
