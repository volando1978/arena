﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct EqualityComparer_1_t4102;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>
struct  EqualityComparer_1_t4102  : public Object_t
{
};
struct EqualityComparer_1_t4102_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UICharInfo>::_default
	EqualityComparer_1_t4102 * ____default_0;
};
