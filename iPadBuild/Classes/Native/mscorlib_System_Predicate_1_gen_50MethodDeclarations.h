﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t4104;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m24872_gshared (Predicate_1_t4104 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Predicate_1__ctor_m24872(__this, ___object, ___method, method) (( void (*) (Predicate_1_t4104 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m24872_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m24873_gshared (Predicate_1_t4104 * __this, UICharInfo_t1400  ___obj, MethodInfo* method);
#define Predicate_1_Invoke_m24873(__this, ___obj, method) (( bool (*) (Predicate_1_t4104 *, UICharInfo_t1400 , MethodInfo*))Predicate_1_Invoke_m24873_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m24874_gshared (Predicate_1_t4104 * __this, UICharInfo_t1400  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Predicate_1_BeginInvoke_m24874(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t4104 *, UICharInfo_t1400 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m24874_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m24875_gshared (Predicate_1_t4104 * __this, Object_t * ___result, MethodInfo* method);
#define Predicate_1_EndInvoke_m24875(__this, ___result, method) (( bool (*) (Predicate_1_t4104 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m24875_gshared)(__this, ___result, method)
