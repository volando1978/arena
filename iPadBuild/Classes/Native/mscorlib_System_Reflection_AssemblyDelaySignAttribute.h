﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyDelaySignAttribute
struct  AssemblyDelaySignAttribute_t1812  : public Attribute_t1546
{
	// System.Boolean System.Reflection.AssemblyDelaySignAttribute::delay
	bool ___delay_0;
};
