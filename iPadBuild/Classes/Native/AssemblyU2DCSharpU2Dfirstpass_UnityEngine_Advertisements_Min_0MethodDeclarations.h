﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.MiniJSON.Json/Parser
struct Parser_t148;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t180;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t181;
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min.h"

// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::.ctor(System.String)
extern "C" void Parser__ctor_m707 (Parser_t148 * __this, String_t* ___jsonString, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern "C" bool Parser_IsWordBreak_m708 (Object_t * __this /* static, unused */, uint16_t ___c, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::Parse(System.String)
extern "C" Object_t * Parser_Parse_m709 (Object_t * __this /* static, unused */, String_t* ___jsonString, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::Dispose()
extern "C" void Parser_Dispose_m710 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseObject()
extern "C" Dictionary_2_t180 * Parser_ParseObject_m711 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseArray()
extern "C" List_1_t181 * Parser_ParseArray_m712 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseValue()
extern "C" Object_t * Parser_ParseValue_m713 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN)
extern "C" Object_t * Parser_ParseByToken_m714 (Parser_t148 * __this, int32_t ___token, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseString()
extern "C" String_t* Parser_ParseString_m715 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseNumber()
extern "C" Object_t * Parser_ParseNumber_m716 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::EatWhitespace()
extern "C" void Parser_EatWhitespace_m717 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_PeekChar()
extern "C" uint16_t Parser_get_PeekChar_m718 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextChar()
extern "C" uint16_t Parser_get_NextChar_m719 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextWord()
extern "C" String_t* Parser_get_NextWord_m720 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextToken()
extern "C" int32_t Parser_get_NextToken_m721 (Parser_t148 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
