﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t153;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t155;
// System.String
struct String_t;

// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C" void ShowOptions__ctor_m822 (ShowOptions_t153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.ShowOptions::get_pause()
extern "C" bool ShowOptions_get_pause_m823 (ShowOptions_t153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::set_pause(System.Boolean)
extern "C" void ShowOptions_set_pause_m824 (ShowOptions_t153 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern "C" Action_1_t155 * ShowOptions_get_resultCallback_m825 (ShowOptions_t153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C" void ShowOptions_set_resultCallback_m826 (ShowOptions_t153 * __this, Action_1_t155 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern "C" String_t* ShowOptions_get_gamerSid_m827 (ShowOptions_t153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern "C" void ShowOptions_set_gamerSid_m828 (ShowOptions_t153 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
