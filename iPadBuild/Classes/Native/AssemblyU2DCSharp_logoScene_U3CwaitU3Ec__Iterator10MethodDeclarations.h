﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// logoScene/<wait>c__Iterator10
struct U3CwaitU3Ec__Iterator10_t813;
// System.Object
struct Object_t;

// System.Void logoScene/<wait>c__Iterator10::.ctor()
extern "C" void U3CwaitU3Ec__Iterator10__ctor_m3555 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object logoScene/<wait>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object logoScene/<wait>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean logoScene/<wait>c__Iterator10::MoveNext()
extern "C" bool U3CwaitU3Ec__Iterator10_MoveNext_m3558 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void logoScene/<wait>c__Iterator10::Dispose()
extern "C" void U3CwaitU3Ec__Iterator10_Dispose_m3559 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void logoScene/<wait>c__Iterator10::Reset()
extern "C" void U3CwaitU3Ec__Iterator10_Reset_m3560 (U3CwaitU3Ec__Iterator10_t813 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
