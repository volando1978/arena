﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Boolean>
struct InternalEnumerator_1_t3550;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17271(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3550 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m17272_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17273(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3550 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
#define InternalEnumerator_1_Dispose_m17275(__this, method) (( void (*) (InternalEnumerator_1_t3550 *, MethodInfo*))InternalEnumerator_1_Dispose_m17276_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17277(__this, method) (( bool (*) (InternalEnumerator_1_t3550 *, MethodInfo*))InternalEnumerator_1_MoveNext_m17278_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
#define InternalEnumerator_1_get_Current_m17279(__this, method) (( bool (*) (InternalEnumerator_1_t3550 *, MethodInfo*))InternalEnumerator_1_get_Current_m17280_gshared)(__this, method)
