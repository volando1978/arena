﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t621;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43
struct  U3CToOnGameThreadU3Ec__AnonStorey43_t619  : public Object_t
{
	// GooglePlayGames.BasicApi.SavedGame.ConflictCallback GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43::conflictCallback
	ConflictCallback_t621 * ___conflictCallback_0;
};
