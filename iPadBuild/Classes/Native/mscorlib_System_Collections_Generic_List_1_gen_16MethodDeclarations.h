﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
struct List_1_t915;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t913;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.BasicApi.Events.IEvent>
struct IEnumerable_1_t914;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Events.IEvent>
struct IEnumerator_1_t4351;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.BasicApi.Events.IEvent>
struct ICollection_1_t4352;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Events.IEvent>
struct ReadOnlyCollection_1_t3642;
// GooglePlayGames.BasicApi.Events.IEvent[]
struct IEventU5BU5D_t3640;
// System.Predicate`1<GooglePlayGames.BasicApi.Events.IEvent>
struct Predicate_1_t3643;
// System.Action`1<GooglePlayGames.BasicApi.Events.IEvent>
struct Action_1_t3644;
// System.Comparison`1<GooglePlayGames.BasicApi.Events.IEvent>
struct Comparison_1_t3646;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3764(__this, method) (( void (*) (List_1_t915 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18703(__this, ___collection, method) (( void (*) (List_1_t915 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Int32)
#define List_1__ctor_m18704(__this, ___capacity, method) (( void (*) (List_1_t915 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::.cctor()
#define List_1__cctor_m18705(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18706(__this, method) (( Object_t* (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18707(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t915 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18708(__this, method) (( Object_t * (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18709(__this, ___item, method) (( int32_t (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18710(__this, ___item, method) (( bool (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18711(__this, ___item, method) (( int32_t (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18712(__this, ___index, ___item, method) (( void (*) (List_1_t915 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18713(__this, ___item, method) (( void (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18714(__this, method) (( bool (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18715(__this, method) (( bool (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18716(__this, method) (( Object_t * (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18717(__this, method) (( bool (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18718(__this, method) (( bool (*) (List_1_t915 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18719(__this, ___index, method) (( Object_t * (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18720(__this, ___index, ___value, method) (( void (*) (List_1_t915 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Add(T)
#define List_1_Add_m18721(__this, ___item, method) (( void (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18722(__this, ___newCount, method) (( void (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18723(__this, ___collection, method) (( void (*) (List_1_t915 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18724(__this, ___enumerable, method) (( void (*) (List_1_t915 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18725(__this, ___collection, method) (( void (*) (List_1_t915 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::AsReadOnly()
#define List_1_AsReadOnly_m18726(__this, method) (( ReadOnlyCollection_1_t3642 * (*) (List_1_t915 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Clear()
#define List_1_Clear_m18727(__this, method) (( void (*) (List_1_t915 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Contains(T)
#define List_1_Contains_m18728(__this, ___item, method) (( bool (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18729(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t915 *, IEventU5BU5D_t3640*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Find(System.Predicate`1<T>)
#define List_1_Find_m18730(__this, ___match, method) (( Object_t * (*) (List_1_t915 *, Predicate_1_t3643 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18731(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3643 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m18732(__this, ___match, method) (( int32_t (*) (List_1_t915 *, Predicate_1_t3643 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18733(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t915 *, int32_t, int32_t, Predicate_1_t3643 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m18734(__this, ___action, method) (( void (*) (List_1_t915 *, Action_1_t3644 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::GetEnumerator()
#define List_1_GetEnumerator_m18735(__this, method) (( Enumerator_t3645  (*) (List_1_t915 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::IndexOf(T)
#define List_1_IndexOf_m18736(__this, ___item, method) (( int32_t (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18737(__this, ___start, ___delta, method) (( void (*) (List_1_t915 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18738(__this, ___index, method) (( void (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Insert(System.Int32,T)
#define List_1_Insert_m18739(__this, ___index, ___item, method) (( void (*) (List_1_t915 *, int32_t, Object_t *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18740(__this, ___collection, method) (( void (*) (List_1_t915 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Remove(T)
#define List_1_Remove_m18741(__this, ___item, method) (( bool (*) (List_1_t915 *, Object_t *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18742(__this, ___match, method) (( int32_t (*) (List_1_t915 *, Predicate_1_t3643 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18743(__this, ___index, method) (( void (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Reverse()
#define List_1_Reverse_m18744(__this, method) (( void (*) (List_1_t915 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Sort()
#define List_1_Sort_m18745(__this, method) (( void (*) (List_1_t915 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18746(__this, ___comparison, method) (( void (*) (List_1_t915 *, Comparison_1_t3646 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::ToArray()
#define List_1_ToArray_m18747(__this, method) (( IEventU5BU5D_t3640* (*) (List_1_t915 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::TrimExcess()
#define List_1_TrimExcess_m18748(__this, method) (( void (*) (List_1_t915 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Capacity()
#define List_1_get_Capacity_m18749(__this, method) (( int32_t (*) (List_1_t915 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18750(__this, ___value, method) (( void (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Count()
#define List_1_get_Count_m18751(__this, method) (( int32_t (*) (List_1_t915 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::get_Item(System.Int32)
#define List_1_get_Item_m18752(__this, ___index, method) (( Object_t * (*) (List_1_t915 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>::set_Item(System.Int32,T)
#define List_1_set_Item_m18753(__this, ___index, ___value, method) (( void (*) (List_1_t915 *, int32_t, Object_t *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
