﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>
struct Enumerator_t3674;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t343;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12MethodDeclarations.h"
#define Enumerator__ctor_m19089(__this, ___dictionary, method) (( void (*) (Enumerator_t3674 *, Dictionary_2_t343 *, MethodInfo*))Enumerator__ctor_m18987_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19090(__this, method) (( Object_t * (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18988_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19091(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18989_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19092(__this, method) (( Object_t * (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18990_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19093(__this, method) (( Object_t * (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18991_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::MoveNext()
#define Enumerator_MoveNext_m19094(__this, method) (( bool (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_MoveNext_m18992_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_Current()
#define Enumerator_get_Current_m19095(__this, method) (( KeyValuePair_2_t3671  (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_get_Current_m18993_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m19096(__this, method) (( String_t* (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_get_CurrentKey_m18994_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19097(__this, method) (( uint32_t (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_get_CurrentValue_m18995_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::VerifyState()
#define Enumerator_VerifyState_m19098(__this, method) (( void (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_VerifyState_m18996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m19099(__this, method) (( void (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_VerifyCurrent_m18997_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>::Dispose()
#define Enumerator_Dispose_m19100(__this, method) (( void (*) (Enumerator_t3674 *, MethodInfo*))Enumerator_Dispose_m18998_gshared)(__this, method)
