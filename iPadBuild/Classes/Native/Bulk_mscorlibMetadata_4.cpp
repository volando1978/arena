﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t2980_il2cpp_TypeInfo;
static MethodInfo* _Thread_t2980_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType _Thread_t2980_0_0_0;
extern Il2CppType _Thread_t2980_1_0_0;
struct _Thread_t2980;
const Il2CppTypeDefinitionMetadata _Thread_t2980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo _Thread_t2980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Thread_t2980_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &_Thread_t2980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 496/* custom_attributes_cache */
	, &_Thread_t2980_0_0_0/* byval_arg */
	, &_Thread_t2980_1_0_0/* this_arg */
	, &_Thread_t2980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t2968_il2cpp_TypeInfo;
static MethodInfo* _TypeBuilder_t2968_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType _TypeBuilder_t2968_0_0_0;
extern Il2CppType _TypeBuilder_t2968_1_0_0;
struct _TypeBuilder_t2968;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t2968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo _TypeBuilder_t2968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _TypeBuilder_t2968_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &_TypeBuilder_t2968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 497/* custom_attributes_cache */
	, &_TypeBuilder_t2968_0_0_0/* byval_arg */
	, &_TypeBuilder_t2968_1_0_0/* this_arg */
	, &_TypeBuilder_t2968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t2583_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
extern Il2CppType IActivator_t2582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::get_ConstructionActivator()
MethodInfo ActivationServices_get_ConstructionActivator_m12500_MethodInfo = 
{
	"get_ConstructionActivator"/* name */
	, (methodPointerType)&ActivationServices_get_ConstructionActivator_m12500/* method */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2193/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo ActivationServices_t2583_ActivationServices_CreateProxyFromAttributes_m12501_ParameterInfos[] = 
{
	{"type", 0, 134221888, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134221889, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyFromAttributes(System.Type,System.Object[])
MethodInfo ActivationServices_CreateProxyFromAttributes_m12501_MethodInfo = 
{
	"CreateProxyFromAttributes"/* name */
	, (methodPointerType)&ActivationServices_CreateProxyFromAttributes_m12501/* method */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2583_ActivationServices_CreateProxyFromAttributes_m12501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo ActivationServices_t2583_ActivationServices_CreateConstructionCall_m12502_ParameterInfos[] = 
{
	{"type", 0, 134221890, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134221891, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134221892, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType ConstructionCall_t2606_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.ConstructionCall System.Runtime.Remoting.Activation.ActivationServices::CreateConstructionCall(System.Type,System.String,System.Object[])
MethodInfo ActivationServices_CreateConstructionCall_m12502_MethodInfo = 
{
	"CreateConstructionCall"/* name */
	, (methodPointerType)&ActivationServices_CreateConstructionCall_m12502/* method */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* declaring_type */
	, &ConstructionCall_t2606_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2583_ActivationServices_CreateConstructionCall_m12502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ActivationServices_t2583_ActivationServices_AllocateUninitializedClassInstance_m12503_ParameterInfos[] = 
{
	{"type", 0, 134221893, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)
MethodInfo ActivationServices_AllocateUninitializedClassInstance_m12503_MethodInfo = 
{
	"AllocateUninitializedClassInstance"/* name */
	, (methodPointerType)&ActivationServices_AllocateUninitializedClassInstance_m12503/* method */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ActivationServices_t2583_ActivationServices_AllocateUninitializedClassInstance_m12503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ActivationServices_t2583_MethodInfos[] =
{
	&ActivationServices_get_ConstructionActivator_m12500_MethodInfo,
	&ActivationServices_CreateProxyFromAttributes_m12501_MethodInfo,
	&ActivationServices_CreateConstructionCall_m12502_MethodInfo,
	&ActivationServices_AllocateUninitializedClassInstance_m12503_MethodInfo,
	NULL
};
extern Il2CppType IActivator_t2582_0_0_17;
FieldInfo ActivationServices_t2583_____constructionActivator_0_FieldInfo = 
{
	"_constructionActivator"/* name */
	, &IActivator_t2582_0_0_17/* type */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* parent */
	, offsetof(ActivationServices_t2583_StaticFields, ____constructionActivator_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ActivationServices_t2583_FieldInfos[] =
{
	&ActivationServices_t2583_____constructionActivator_0_FieldInfo,
	NULL
};
extern MethodInfo ActivationServices_get_ConstructionActivator_m12500_MethodInfo;
static PropertyInfo ActivationServices_t2583____ConstructionActivator_PropertyInfo = 
{
	&ActivationServices_t2583_il2cpp_TypeInfo/* parent */
	, "ConstructionActivator"/* name */
	, &ActivationServices_get_ConstructionActivator_m12500_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ActivationServices_t2583_PropertyInfos[] =
{
	&ActivationServices_t2583____ConstructionActivator_PropertyInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference ActivationServices_t2583_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ActivationServices_t2583_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ActivationServices_t2583_0_0_0;
extern Il2CppType ActivationServices_t2583_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct ActivationServices_t2583;
const Il2CppTypeDefinitionMetadata ActivationServices_t2583_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t2583_VTable/* vtableMethods */
	, ActivationServices_t2583_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ActivationServices_t2583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ActivationServices_t2583_MethodInfos/* methods */
	, ActivationServices_t2583_PropertyInfos/* properties */
	, ActivationServices_t2583_FieldInfos/* fields */
	, NULL/* events */
	, &ActivationServices_t2583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t2583_0_0_0/* byval_arg */
	, &ActivationServices_t2583_1_0_0/* this_arg */
	, &ActivationServices_t2583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t2583)/* instance_size */
	, sizeof (ActivationServices_t2583)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t2583_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t2584_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType IActivator_t2582_0_0_0;
extern Il2CppType IActivator_t2582_0_0_0;
static ParameterInfo AppDomainLevelActivator_t2584_AppDomainLevelActivator__ctor_m12504_ParameterInfos[] = 
{
	{"activationUrl", 0, 134221894, 0, &String_t_0_0_0},
	{"next", 1, 134221895, 0, &IActivator_t2582_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
MethodInfo AppDomainLevelActivator__ctor_m12504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainLevelActivator__ctor_m12504/* method */
	, &AppDomainLevelActivator_t2584_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, AppDomainLevelActivator_t2584_AppDomainLevelActivator__ctor_m12504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AppDomainLevelActivator_t2584_MethodInfos[] =
{
	&AppDomainLevelActivator__ctor_m12504_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo AppDomainLevelActivator_t2584_____activationUrl_0_FieldInfo = 
{
	"_activationUrl"/* name */
	, &String_t_0_0_1/* type */
	, &AppDomainLevelActivator_t2584_il2cpp_TypeInfo/* parent */
	, offsetof(AppDomainLevelActivator_t2584, ____activationUrl_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IActivator_t2582_0_0_1;
FieldInfo AppDomainLevelActivator_t2584_____next_1_FieldInfo = 
{
	"_next"/* name */
	, &IActivator_t2582_0_0_1/* type */
	, &AppDomainLevelActivator_t2584_il2cpp_TypeInfo/* parent */
	, offsetof(AppDomainLevelActivator_t2584, ____next_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AppDomainLevelActivator_t2584_FieldInfos[] =
{
	&AppDomainLevelActivator_t2584_____activationUrl_0_FieldInfo,
	&AppDomainLevelActivator_t2584_____next_1_FieldInfo,
	NULL
};
static Il2CppMethodReference AppDomainLevelActivator_t2584_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool AppDomainLevelActivator_t2584_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* AppDomainLevelActivator_t2584_InterfacesTypeInfos[] = 
{
	&IActivator_t2582_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t2584_InterfacesOffsets[] = 
{
	{ &IActivator_t2582_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType AppDomainLevelActivator_t2584_0_0_0;
extern Il2CppType AppDomainLevelActivator_t2584_1_0_0;
struct AppDomainLevelActivator_t2584;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t2584_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t2584_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t2584_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t2584_VTable/* vtableMethods */
	, AppDomainLevelActivator_t2584_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AppDomainLevelActivator_t2584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, AppDomainLevelActivator_t2584_MethodInfos/* methods */
	, NULL/* properties */
	, AppDomainLevelActivator_t2584_FieldInfos/* fields */
	, NULL/* events */
	, &AppDomainLevelActivator_t2584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t2584_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t2584_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t2584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t2584)/* instance_size */
	, sizeof (AppDomainLevelActivator_t2584)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t2585_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
MethodInfo ConstructionLevelActivator__ctor_m12505_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionLevelActivator__ctor_m12505/* method */
	, &ConstructionLevelActivator_t2585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ConstructionLevelActivator_t2585_MethodInfos[] =
{
	&ConstructionLevelActivator__ctor_m12505_MethodInfo,
	NULL
};
static Il2CppMethodReference ConstructionLevelActivator_t2585_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ConstructionLevelActivator_t2585_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionLevelActivator_t2585_InterfacesTypeInfos[] = 
{
	&IActivator_t2582_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t2585_InterfacesOffsets[] = 
{
	{ &IActivator_t2582_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ConstructionLevelActivator_t2585_0_0_0;
extern Il2CppType ConstructionLevelActivator_t2585_1_0_0;
struct ConstructionLevelActivator_t2585;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t2585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t2585_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t2585_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t2585_VTable/* vtableMethods */
	, ConstructionLevelActivator_t2585_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ConstructionLevelActivator_t2585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ConstructionLevelActivator_t2585_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ConstructionLevelActivator_t2585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t2585_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t2585_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t2585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t2585)/* instance_size */
	, sizeof (ConstructionLevelActivator_t2585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t2586_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
extern Il2CppType IActivator_t2582_0_0_0;
static ParameterInfo ContextLevelActivator_t2586_ContextLevelActivator__ctor_m12506_ParameterInfos[] = 
{
	{"next", 0, 134221896, 0, &IActivator_t2582_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
MethodInfo ContextLevelActivator__ctor_m12506_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextLevelActivator__ctor_m12506/* method */
	, &ContextLevelActivator_t2586_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ContextLevelActivator_t2586_ContextLevelActivator__ctor_m12506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ContextLevelActivator_t2586_MethodInfos[] =
{
	&ContextLevelActivator__ctor_m12506_MethodInfo,
	NULL
};
extern Il2CppType IActivator_t2582_0_0_1;
FieldInfo ContextLevelActivator_t2586____m_NextActivator_0_FieldInfo = 
{
	"m_NextActivator"/* name */
	, &IActivator_t2582_0_0_1/* type */
	, &ContextLevelActivator_t2586_il2cpp_TypeInfo/* parent */
	, offsetof(ContextLevelActivator_t2586, ___m_NextActivator_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ContextLevelActivator_t2586_FieldInfos[] =
{
	&ContextLevelActivator_t2586____m_NextActivator_0_FieldInfo,
	NULL
};
static Il2CppMethodReference ContextLevelActivator_t2586_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ContextLevelActivator_t2586_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextLevelActivator_t2586_InterfacesTypeInfos[] = 
{
	&IActivator_t2582_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t2586_InterfacesOffsets[] = 
{
	{ &IActivator_t2582_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ContextLevelActivator_t2586_0_0_0;
extern Il2CppType ContextLevelActivator_t2586_1_0_0;
struct ContextLevelActivator_t2586;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t2586_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t2586_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t2586_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t2586_VTable/* vtableMethods */
	, ContextLevelActivator_t2586_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ContextLevelActivator_t2586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ContextLevelActivator_t2586_MethodInfos/* methods */
	, NULL/* properties */
	, ContextLevelActivator_t2586_FieldInfos/* fields */
	, NULL/* events */
	, &ContextLevelActivator_t2586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t2586_0_0_0/* byval_arg */
	, &ContextLevelActivator_t2586_1_0_0/* this_arg */
	, &ContextLevelActivator_t2586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t2586)/* instance_size */
	, sizeof (ContextLevelActivator_t2586)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t2582_il2cpp_TypeInfo;
static MethodInfo* IActivator_t2582_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IActivator_t2582_1_0_0;
struct IActivator_t2582;
const Il2CppTypeDefinitionMetadata IActivator_t2582_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IActivator_t2582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IActivator_t2582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IActivator_t2582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 498/* custom_attributes_cache */
	, &IActivator_t2582_0_0_0/* byval_arg */
	, &IActivator_t2582_1_0_0/* this_arg */
	, &IActivator_t2582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t2884_il2cpp_TypeInfo;
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType()
MethodInfo IConstructionCallMessage_get_ActivationType_m14954_MethodInfo = 
{
	"get_ActivationType"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationTypeName()
MethodInfo IConstructionCallMessage_get_ActivationTypeName_m14955_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IActivator_t2582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator()
MethodInfo IConstructionCallMessage_get_Activator_m14956_MethodInfo = 
{
	"get_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IActivator_t2582_0_0_0;
static ParameterInfo IConstructionCallMessage_t2884_IConstructionCallMessage_set_Activator_m14957_ParameterInfos[] = 
{
	{"value", 0, 134221897, 0, &IActivator_t2582_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.IConstructionCallMessage::set_Activator(System.Runtime.Remoting.Activation.IActivator)
MethodInfo IConstructionCallMessage_set_Activator_m14957_MethodInfo = 
{
	"set_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IConstructionCallMessage_t2884_IConstructionCallMessage_set_Activator_m14957_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Activation.IConstructionCallMessage::get_CallSiteActivationAttributes()
MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m14958_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IList_t183_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties()
MethodInfo IConstructionCallMessage_get_ContextProperties_m14959_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* declaring_type */
	, &IList_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IConstructionCallMessage_t2884_MethodInfos[] =
{
	&IConstructionCallMessage_get_ActivationType_m14954_MethodInfo,
	&IConstructionCallMessage_get_ActivationTypeName_m14955_MethodInfo,
	&IConstructionCallMessage_get_Activator_m14956_MethodInfo,
	&IConstructionCallMessage_set_Activator_m14957_MethodInfo,
	&IConstructionCallMessage_get_CallSiteActivationAttributes_m14958_MethodInfo,
	&IConstructionCallMessage_get_ContextProperties_m14959_MethodInfo,
	NULL
};
extern MethodInfo IConstructionCallMessage_get_ActivationType_m14954_MethodInfo;
static PropertyInfo IConstructionCallMessage_t2884____ActivationType_PropertyInfo = 
{
	&IConstructionCallMessage_t2884_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &IConstructionCallMessage_get_ActivationType_m14954_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IConstructionCallMessage_get_ActivationTypeName_m14955_MethodInfo;
static PropertyInfo IConstructionCallMessage_t2884____ActivationTypeName_PropertyInfo = 
{
	&IConstructionCallMessage_t2884_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &IConstructionCallMessage_get_ActivationTypeName_m14955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IConstructionCallMessage_get_Activator_m14956_MethodInfo;
extern MethodInfo IConstructionCallMessage_set_Activator_m14957_MethodInfo;
static PropertyInfo IConstructionCallMessage_t2884____Activator_PropertyInfo = 
{
	&IConstructionCallMessage_t2884_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &IConstructionCallMessage_get_Activator_m14956_MethodInfo/* get */
	, &IConstructionCallMessage_set_Activator_m14957_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m14958_MethodInfo;
static PropertyInfo IConstructionCallMessage_t2884____CallSiteActivationAttributes_PropertyInfo = 
{
	&IConstructionCallMessage_t2884_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &IConstructionCallMessage_get_CallSiteActivationAttributes_m14958_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IConstructionCallMessage_get_ContextProperties_m14959_MethodInfo;
static PropertyInfo IConstructionCallMessage_t2884____ContextProperties_PropertyInfo = 
{
	&IConstructionCallMessage_t2884_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &IConstructionCallMessage_get_ContextProperties_m14959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IConstructionCallMessage_t2884_PropertyInfos[] =
{
	&IConstructionCallMessage_t2884____ActivationType_PropertyInfo,
	&IConstructionCallMessage_t2884____ActivationTypeName_PropertyInfo,
	&IConstructionCallMessage_t2884____Activator_PropertyInfo,
	&IConstructionCallMessage_t2884____CallSiteActivationAttributes_PropertyInfo,
	&IConstructionCallMessage_t2884____ContextProperties_PropertyInfo,
	NULL
};
extern Il2CppType IMessage_t2604_0_0_0;
extern Il2CppType IMethodCallMessage_t2888_0_0_0;
extern Il2CppType IMethodMessage_t2616_0_0_0;
static const Il2CppType* IConstructionCallMessage_t2884_InterfacesTypeInfos[] = 
{
	&IMessage_t2604_0_0_0,
	&IMethodCallMessage_t2888_0_0_0,
	&IMethodMessage_t2616_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_1_0_0;
struct IConstructionCallMessage_t2884;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t2884_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t2884_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IConstructionCallMessage_t2884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IConstructionCallMessage_t2884_MethodInfos/* methods */
	, IConstructionCallMessage_t2884_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IConstructionCallMessage_t2884_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 499/* custom_attributes_cache */
	, &IConstructionCallMessage_t2884_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t2884_1_0_0/* this_arg */
	, &IConstructionCallMessage_t2884_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t2587_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
static MethodInfo* RemoteActivator_t2587_MethodInfos[] =
{
	NULL
};
static Il2CppMethodReference RemoteActivator_t2587_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RemoteActivator_t2587_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemoteActivator_t2587_InterfacesTypeInfos[] = 
{
	&IActivator_t2582_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t2587_InterfacesOffsets[] = 
{
	{ &IActivator_t2582_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemoteActivator_t2587_0_0_0;
extern Il2CppType RemoteActivator_t2587_1_0_0;
extern Il2CppType MarshalByRefObject_t2011_0_0_0;
struct RemoteActivator_t2587;
const Il2CppTypeDefinitionMetadata RemoteActivator_t2587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t2587_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t2587_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t2011_0_0_0/* parent */
	, RemoteActivator_t2587_VTable/* vtableMethods */
	, RemoteActivator_t2587_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemoteActivator_t2587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, RemoteActivator_t2587_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &RemoteActivator_t2587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t2587_0_0_0/* byval_arg */
	, &RemoteActivator_t2587_1_0_0/* this_arg */
	, &RemoteActivator_t2587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t2587)/* instance_size */
	, sizeof (RemoteActivator_t2587)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t2588_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.UrlAttribute::get_UrlValue()
MethodInfo UrlAttribute_get_UrlValue_m12507_MethodInfo = 
{
	"get_UrlValue"/* name */
	, (methodPointerType)&UrlAttribute_get_UrlValue_m12507/* method */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UrlAttribute_t2588_UrlAttribute_Equals_m12508_ParameterInfos[] = 
{
	{"o", 0, 134221898, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::Equals(System.Object)
MethodInfo UrlAttribute_Equals_m12508_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&UrlAttribute_Equals_m12508/* method */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, UrlAttribute_t2588_UrlAttribute_Equals_m12508_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Activation.UrlAttribute::GetHashCode()
MethodInfo UrlAttribute_GetHashCode_m12509_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&UrlAttribute_GetHashCode_m12509/* method */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo UrlAttribute_t2588_UrlAttribute_GetPropertiesForNewContext_m12510_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221899, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo UrlAttribute_GetPropertiesForNewContext_m12510_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&UrlAttribute_GetPropertiesForNewContext_m12510/* method */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, UrlAttribute_t2588_UrlAttribute_GetPropertiesForNewContext_m12510_ParameterInfos/* parameters */
	, 501/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo UrlAttribute_t2588_UrlAttribute_IsContextOK_m12511_ParameterInfos[] = 
{
	{"ctx", 0, 134221900, 0, &Context_t2596_0_0_0},
	{"msg", 1, 134221901, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo UrlAttribute_IsContextOK_m12511_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&UrlAttribute_IsContextOK_m12511/* method */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, UrlAttribute_t2588_UrlAttribute_IsContextOK_m12511_ParameterInfos/* parameters */
	, 502/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UrlAttribute_t2588_MethodInfos[] =
{
	&UrlAttribute_get_UrlValue_m12507_MethodInfo,
	&UrlAttribute_Equals_m12508_MethodInfo,
	&UrlAttribute_GetHashCode_m12509_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12510_MethodInfo,
	&UrlAttribute_IsContextOK_m12511_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo UrlAttribute_t2588____url_1_FieldInfo = 
{
	"url"/* name */
	, &String_t_0_0_1/* type */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* parent */
	, offsetof(UrlAttribute_t2588, ___url_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* UrlAttribute_t2588_FieldInfos[] =
{
	&UrlAttribute_t2588____url_1_FieldInfo,
	NULL
};
extern MethodInfo UrlAttribute_get_UrlValue_m12507_MethodInfo;
static PropertyInfo UrlAttribute_t2588____UrlValue_PropertyInfo = 
{
	&UrlAttribute_t2588_il2cpp_TypeInfo/* parent */
	, "UrlValue"/* name */
	, &UrlAttribute_get_UrlValue_m12507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* UrlAttribute_t2588_PropertyInfos[] =
{
	&UrlAttribute_t2588____UrlValue_PropertyInfo,
	NULL
};
extern MethodInfo UrlAttribute_Equals_m12508_MethodInfo;
extern MethodInfo UrlAttribute_GetHashCode_m12509_MethodInfo;
extern MethodInfo UrlAttribute_GetPropertiesForNewContext_m12510_MethodInfo;
extern MethodInfo UrlAttribute_IsContextOK_m12511_MethodInfo;
extern MethodInfo ContextAttribute_get_Name_m12535_MethodInfo;
static Il2CppMethodReference UrlAttribute_t2588_VTable[] =
{
	&UrlAttribute_Equals_m12508_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&UrlAttribute_GetHashCode_m12509_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12510_MethodInfo,
	&UrlAttribute_IsContextOK_m12511_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m12510_MethodInfo,
	&UrlAttribute_IsContextOK_m12511_MethodInfo,
};
static bool UrlAttribute_t2588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IContextAttribute_t2896_0_0_0;
extern Il2CppType IContextProperty_t2886_0_0_0;
extern Il2CppType _Attribute_t1731_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t2588_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2896_0_0_0, 4},
	{ &IContextProperty_t2886_0_0_0, 6},
	{ &_Attribute_t1731_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType UrlAttribute_t2588_0_0_0;
extern Il2CppType UrlAttribute_t2588_1_0_0;
extern Il2CppType ContextAttribute_t2589_0_0_0;
struct UrlAttribute_t2588;
const Il2CppTypeDefinitionMetadata UrlAttribute_t2588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t2588_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t2589_0_0_0/* parent */
	, UrlAttribute_t2588_VTable/* vtableMethods */
	, UrlAttribute_t2588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo UrlAttribute_t2588_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, UrlAttribute_t2588_MethodInfos/* methods */
	, UrlAttribute_t2588_PropertyInfos/* properties */
	, UrlAttribute_t2588_FieldInfos/* fields */
	, NULL/* events */
	, &UrlAttribute_t2588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 500/* custom_attributes_cache */
	, &UrlAttribute_t2588_0_0_0/* byval_arg */
	, &UrlAttribute_t2588_1_0_0/* this_arg */
	, &UrlAttribute_t2588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t2588)/* instance_size */
	, sizeof (UrlAttribute_t2588)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t2590_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
MethodInfo ChannelInfo__ctor_m12512_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ChannelInfo__ctor_m12512/* method */
	, &ChannelInfo_t2590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
MethodInfo ChannelInfo_get_ChannelData_m12513_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&ChannelInfo_get_ChannelData_m12513/* method */
	, &ChannelInfo_t2590_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ChannelInfo_t2590_MethodInfos[] =
{
	&ChannelInfo__ctor_m12512_MethodInfo,
	&ChannelInfo_get_ChannelData_m12513_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo ChannelInfo_t2590____channelData_0_FieldInfo = 
{
	"channelData"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &ChannelInfo_t2590_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelInfo_t2590, ___channelData_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ChannelInfo_t2590_FieldInfos[] =
{
	&ChannelInfo_t2590____channelData_0_FieldInfo,
	NULL
};
extern MethodInfo ChannelInfo_get_ChannelData_m12513_MethodInfo;
static PropertyInfo ChannelInfo_t2590____ChannelData_PropertyInfo = 
{
	&ChannelInfo_t2590_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &ChannelInfo_get_ChannelData_m12513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ChannelInfo_t2590_PropertyInfos[] =
{
	&ChannelInfo_t2590____ChannelData_PropertyInfo,
	NULL
};
static Il2CppMethodReference ChannelInfo_t2590_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ChannelInfo_get_ChannelData_m12513_MethodInfo,
};
static bool ChannelInfo_t2590_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IChannelInfo_t2635_0_0_0;
static const Il2CppType* ChannelInfo_t2590_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t2635_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t2590_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t2635_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ChannelInfo_t2590_0_0_0;
extern Il2CppType ChannelInfo_t2590_1_0_0;
struct ChannelInfo_t2590;
const Il2CppTypeDefinitionMetadata ChannelInfo_t2590_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t2590_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t2590_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t2590_VTable/* vtableMethods */
	, ChannelInfo_t2590_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ChannelInfo_t2590_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ChannelInfo_t2590_MethodInfos/* methods */
	, ChannelInfo_t2590_PropertyInfos/* properties */
	, ChannelInfo_t2590_FieldInfos/* fields */
	, NULL/* events */
	, &ChannelInfo_t2590_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t2590_0_0_0/* byval_arg */
	, &ChannelInfo_t2590_1_0_0/* this_arg */
	, &ChannelInfo_t2590_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t2590)/* instance_size */
	, sizeof (ChannelInfo_t2590)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t2592_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
MethodInfo ChannelServices__cctor_m12514_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ChannelServices__cctor_m12514/* method */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IChannel_t2885_0_0_0;
extern Il2CppType IChannel_t2885_0_0_0;
static ParameterInfo ChannelServices_t2592_ChannelServices_RegisterChannel_m12515_ParameterInfos[] = 
{
	{"chnl", 0, 134221902, 0, &IChannel_t2885_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
MethodInfo ChannelServices_RegisterChannel_m12515_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m12515/* method */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ChannelServices_t2592_ChannelServices_RegisterChannel_m12515_ParameterInfos/* parameters */
	, 504/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IChannel_t2885_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ChannelServices_t2592_ChannelServices_RegisterChannel_m12516_ParameterInfos[] = 
{
	{"chnl", 0, 134221903, 0, &IChannel_t2885_0_0_0},
	{"ensureSecurity", 1, 134221904, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
MethodInfo ChannelServices_RegisterChannel_m12516_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m12516/* method */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, ChannelServices_t2592_ChannelServices_RegisterChannel_m12516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
MethodInfo ChannelServices_GetCurrentChannelInfo_m12517_MethodInfo = 
{
	"GetCurrentChannelInfo"/* name */
	, (methodPointerType)&ChannelServices_GetCurrentChannelInfo_m12517/* method */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ChannelServices_t2592_MethodInfos[] =
{
	&ChannelServices__cctor_m12514_MethodInfo,
	&ChannelServices_RegisterChannel_m12515_MethodInfo,
	&ChannelServices_RegisterChannel_m12516_MethodInfo,
	&ChannelServices_GetCurrentChannelInfo_m12517_MethodInfo,
	NULL
};
extern Il2CppType ArrayList_t737_0_0_17;
FieldInfo ChannelServices_t2592____registeredChannels_0_FieldInfo = 
{
	"registeredChannels"/* name */
	, &ArrayList_t737_0_0_17/* type */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelServices_t2592_StaticFields, ___registeredChannels_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_17;
FieldInfo ChannelServices_t2592____delayedClientChannels_1_FieldInfo = 
{
	"delayedClientChannels"/* name */
	, &ArrayList_t737_0_0_17/* type */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelServices_t2592_StaticFields, ___delayedClientChannels_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CrossContextChannel_t2591_0_0_17;
FieldInfo ChannelServices_t2592_____crossContextSink_2_FieldInfo = 
{
	"_crossContextSink"/* name */
	, &CrossContextChannel_t2591_0_0_17/* type */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelServices_t2592_StaticFields, ____crossContextSink_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_19;
FieldInfo ChannelServices_t2592____CrossContextUrl_3_FieldInfo = 
{
	"CrossContextUrl"/* name */
	, &String_t_0_0_19/* type */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelServices_t2592_StaticFields, ___CrossContextUrl_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IList_t183_0_0_17;
FieldInfo ChannelServices_t2592____oldStartModeTypes_4_FieldInfo = 
{
	"oldStartModeTypes"/* name */
	, &IList_t183_0_0_17/* type */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* parent */
	, offsetof(ChannelServices_t2592_StaticFields, ___oldStartModeTypes_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ChannelServices_t2592_FieldInfos[] =
{
	&ChannelServices_t2592____registeredChannels_0_FieldInfo,
	&ChannelServices_t2592____delayedClientChannels_1_FieldInfo,
	&ChannelServices_t2592_____crossContextSink_2_FieldInfo,
	&ChannelServices_t2592____CrossContextUrl_3_FieldInfo,
	&ChannelServices_t2592____oldStartModeTypes_4_FieldInfo,
	NULL
};
static Il2CppMethodReference ChannelServices_t2592_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ChannelServices_t2592_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ChannelServices_t2592_0_0_0;
extern Il2CppType ChannelServices_t2592_1_0_0;
struct ChannelServices_t2592;
const Il2CppTypeDefinitionMetadata ChannelServices_t2592_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t2592_VTable/* vtableMethods */
	, ChannelServices_t2592_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ChannelServices_t2592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ChannelServices_t2592_MethodInfos/* methods */
	, NULL/* properties */
	, ChannelServices_t2592_FieldInfos/* fields */
	, NULL/* events */
	, &ChannelServices_t2592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 503/* custom_attributes_cache */
	, &ChannelServices_t2592_0_0_0/* byval_arg */
	, &ChannelServices_t2592_1_0_0/* this_arg */
	, &ChannelServices_t2592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t2592)/* instance_size */
	, sizeof (ChannelServices_t2592)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t2592_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t2593_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo CrossAppDomainData_t2593_CrossAppDomainData__ctor_m12518_ParameterInfos[] = 
{
	{"domainId", 0, 134221905, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
MethodInfo CrossAppDomainData__ctor_m12518_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainData__ctor_m12518/* method */
	, &CrossAppDomainData_t2593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, CrossAppDomainData_t2593_CrossAppDomainData__ctor_m12518_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CrossAppDomainData_t2593_MethodInfos[] =
{
	&CrossAppDomainData__ctor_m12518_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_1;
FieldInfo CrossAppDomainData_t2593_____ContextID_0_FieldInfo = 
{
	"_ContextID"/* name */
	, &Object_t_0_0_1/* type */
	, &CrossAppDomainData_t2593_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainData_t2593, ____ContextID_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo CrossAppDomainData_t2593_____DomainID_1_FieldInfo = 
{
	"_DomainID"/* name */
	, &Int32_t189_0_0_1/* type */
	, &CrossAppDomainData_t2593_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainData_t2593, ____DomainID_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo CrossAppDomainData_t2593_____processGuid_2_FieldInfo = 
{
	"_processGuid"/* name */
	, &String_t_0_0_1/* type */
	, &CrossAppDomainData_t2593_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainData_t2593, ____processGuid_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CrossAppDomainData_t2593_FieldInfos[] =
{
	&CrossAppDomainData_t2593_____ContextID_0_FieldInfo,
	&CrossAppDomainData_t2593_____DomainID_1_FieldInfo,
	&CrossAppDomainData_t2593_____processGuid_2_FieldInfo,
	NULL
};
static Il2CppMethodReference CrossAppDomainData_t2593_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CrossAppDomainData_t2593_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType CrossAppDomainData_t2593_0_0_0;
extern Il2CppType CrossAppDomainData_t2593_1_0_0;
struct CrossAppDomainData_t2593;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t2593_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t2593_VTable/* vtableMethods */
	, CrossAppDomainData_t2593_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CrossAppDomainData_t2593_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainData_t2593_MethodInfos/* methods */
	, NULL/* properties */
	, CrossAppDomainData_t2593_FieldInfos/* fields */
	, NULL/* events */
	, &CrossAppDomainData_t2593_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t2593_0_0_0/* byval_arg */
	, &CrossAppDomainData_t2593_1_0_0/* this_arg */
	, &CrossAppDomainData_t2593_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t2593)/* instance_size */
	, sizeof (CrossAppDomainData_t2593)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t2594_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
MethodInfo CrossAppDomainChannel__ctor_m12519_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__ctor_m12519/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
MethodInfo CrossAppDomainChannel__cctor_m12520_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__cctor_m12520/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
MethodInfo CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12521_MethodInfo = 
{
	"RegisterCrossAppDomainChannel"/* name */
	, (methodPointerType)&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12521/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
MethodInfo CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo = 
{
	"get_ChannelName"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelName_m12522/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
MethodInfo CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelPriority_m12523/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
MethodInfo CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelData_m12524/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CrossAppDomainChannel_t2594_CrossAppDomainChannel_StartListening_m12525_ParameterInfos[] = 
{
	{"data", 0, 134221906, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
MethodInfo CrossAppDomainChannel_StartListening_m12525_MethodInfo = 
{
	"StartListening"/* name */
	, (methodPointerType)&CrossAppDomainChannel_StartListening_m12525/* method */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, CrossAppDomainChannel_t2594_CrossAppDomainChannel_StartListening_m12525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CrossAppDomainChannel_t2594_MethodInfos[] =
{
	&CrossAppDomainChannel__ctor_m12519_MethodInfo,
	&CrossAppDomainChannel__cctor_m12520_MethodInfo,
	&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m12521_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12525_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_17;
FieldInfo CrossAppDomainChannel_t2594____s_lock_0_FieldInfo = 
{
	"s_lock"/* name */
	, &Object_t_0_0_17/* type */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainChannel_t2594_StaticFields, ___s_lock_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CrossAppDomainChannel_t2594_FieldInfos[] =
{
	&CrossAppDomainChannel_t2594____s_lock_0_FieldInfo,
	NULL
};
extern MethodInfo CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo;
static PropertyInfo CrossAppDomainChannel_t2594____ChannelName_PropertyInfo = 
{
	&CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo;
static PropertyInfo CrossAppDomainChannel_t2594____ChannelPriority_PropertyInfo = 
{
	&CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo;
static PropertyInfo CrossAppDomainChannel_t2594____ChannelData_PropertyInfo = 
{
	&CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CrossAppDomainChannel_t2594_PropertyInfos[] =
{
	&CrossAppDomainChannel_t2594____ChannelName_PropertyInfo,
	&CrossAppDomainChannel_t2594____ChannelPriority_PropertyInfo,
	&CrossAppDomainChannel_t2594____ChannelData_PropertyInfo,
	NULL
};
extern MethodInfo CrossAppDomainChannel_StartListening_m12525_MethodInfo;
static Il2CppMethodReference CrossAppDomainChannel_t2594_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12525_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m12522_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m12523_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m12524_MethodInfo,
	&CrossAppDomainChannel_StartListening_m12525_MethodInfo,
};
static bool CrossAppDomainChannel_t2594_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IChannelReceiver_t2898_0_0_0;
extern Il2CppType IChannelSender_t2981_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t2594_InterfacesTypeInfos[] = 
{
	&IChannel_t2885_0_0_0,
	&IChannelReceiver_t2898_0_0_0,
	&IChannelSender_t2981_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t2594_InterfacesOffsets[] = 
{
	{ &IChannel_t2885_0_0_0, 4},
	{ &IChannelReceiver_t2898_0_0_0, 6},
	{ &IChannelSender_t2981_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType CrossAppDomainChannel_t2594_0_0_0;
extern Il2CppType CrossAppDomainChannel_t2594_1_0_0;
struct CrossAppDomainChannel_t2594;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t2594_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t2594_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t2594_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t2594_VTable/* vtableMethods */
	, CrossAppDomainChannel_t2594_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CrossAppDomainChannel_t2594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainChannel_t2594_MethodInfos/* methods */
	, CrossAppDomainChannel_t2594_PropertyInfos/* properties */
	, CrossAppDomainChannel_t2594_FieldInfos/* fields */
	, NULL/* events */
	, &CrossAppDomainChannel_t2594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t2594_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t2594_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t2594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t2594)/* instance_size */
	, sizeof (CrossAppDomainChannel_t2594)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t2594_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t2595_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.cctor()
MethodInfo CrossAppDomainSink__cctor_m12526_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainSink__cctor_m12526/* method */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainSink::get_TargetDomainId()
MethodInfo CrossAppDomainSink_get_TargetDomainId_m12527_MethodInfo = 
{
	"get_TargetDomainId"/* name */
	, (methodPointerType)&CrossAppDomainSink_get_TargetDomainId_m12527/* method */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CrossAppDomainSink_t2595_MethodInfos[] =
{
	&CrossAppDomainSink__cctor_m12526_MethodInfo,
	&CrossAppDomainSink_get_TargetDomainId_m12527_MethodInfo,
	NULL
};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo CrossAppDomainSink_t2595____s_sinks_0_FieldInfo = 
{
	"s_sinks"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainSink_t2595_StaticFields, ___s_sinks_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodInfo_t_0_0_17;
FieldInfo CrossAppDomainSink_t2595____processMessageMethod_1_FieldInfo = 
{
	"processMessageMethod"/* name */
	, &MethodInfo_t_0_0_17/* type */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainSink_t2595_StaticFields, ___processMessageMethod_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo CrossAppDomainSink_t2595_____domainID_2_FieldInfo = 
{
	"_domainID"/* name */
	, &Int32_t189_0_0_1/* type */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* parent */
	, offsetof(CrossAppDomainSink_t2595, ____domainID_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* CrossAppDomainSink_t2595_FieldInfos[] =
{
	&CrossAppDomainSink_t2595____s_sinks_0_FieldInfo,
	&CrossAppDomainSink_t2595____processMessageMethod_1_FieldInfo,
	&CrossAppDomainSink_t2595_____domainID_2_FieldInfo,
	NULL
};
extern MethodInfo CrossAppDomainSink_get_TargetDomainId_m12527_MethodInfo;
static PropertyInfo CrossAppDomainSink_t2595____TargetDomainId_PropertyInfo = 
{
	&CrossAppDomainSink_t2595_il2cpp_TypeInfo/* parent */
	, "TargetDomainId"/* name */
	, &CrossAppDomainSink_get_TargetDomainId_m12527_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* CrossAppDomainSink_t2595_PropertyInfos[] =
{
	&CrossAppDomainSink_t2595____TargetDomainId_PropertyInfo,
	NULL
};
static Il2CppMethodReference CrossAppDomainSink_t2595_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CrossAppDomainSink_t2595_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppType IMessageSink_t2187_0_0_0;
static const Il2CppType* CrossAppDomainSink_t2595_InterfacesTypeInfos[] = 
{
	&IMessageSink_t2187_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t2595_InterfacesOffsets[] = 
{
	{ &IMessageSink_t2187_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType CrossAppDomainSink_t2595_0_0_0;
extern Il2CppType CrossAppDomainSink_t2595_1_0_0;
struct CrossAppDomainSink_t2595;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t2595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t2595_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t2595_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t2595_VTable/* vtableMethods */
	, CrossAppDomainSink_t2595_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CrossAppDomainSink_t2595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainSink_t2595_MethodInfos/* methods */
	, CrossAppDomainSink_t2595_PropertyInfos/* properties */
	, CrossAppDomainSink_t2595_FieldInfos/* fields */
	, NULL/* events */
	, &CrossAppDomainSink_t2595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 505/* custom_attributes_cache */
	, &CrossAppDomainSink_t2595_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t2595_1_0_0/* this_arg */
	, &CrossAppDomainSink_t2595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t2595)/* instance_size */
	, sizeof (CrossAppDomainSink_t2595)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t2595_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t2885_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName()
MethodInfo IChannel_get_ChannelName_m14960_MethodInfo = 
{
	"get_ChannelName"/* name */
	, NULL/* method */
	, &IChannel_t2885_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority()
MethodInfo IChannel_get_ChannelPriority_m14961_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, NULL/* method */
	, &IChannel_t2885_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IChannel_t2885_MethodInfos[] =
{
	&IChannel_get_ChannelName_m14960_MethodInfo,
	&IChannel_get_ChannelPriority_m14961_MethodInfo,
	NULL
};
extern MethodInfo IChannel_get_ChannelName_m14960_MethodInfo;
static PropertyInfo IChannel_t2885____ChannelName_PropertyInfo = 
{
	&IChannel_t2885_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &IChannel_get_ChannelName_m14960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IChannel_get_ChannelPriority_m14961_MethodInfo;
static PropertyInfo IChannel_t2885____ChannelPriority_PropertyInfo = 
{
	&IChannel_t2885_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &IChannel_get_ChannelPriority_m14961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IChannel_t2885_PropertyInfos[] =
{
	&IChannel_t2885____ChannelName_PropertyInfo,
	&IChannel_t2885____ChannelPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IChannel_t2885_1_0_0;
struct IChannel_t2885;
const Il2CppTypeDefinitionMetadata IChannel_t2885_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IChannel_t2885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannel_t2885_MethodInfos/* methods */
	, IChannel_t2885_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IChannel_t2885_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 506/* custom_attributes_cache */
	, &IChannel_t2885_0_0_0/* byval_arg */
	, &IChannel_t2885_1_0_0/* this_arg */
	, &IChannel_t2885_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t2898_il2cpp_TypeInfo;
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.IChannelReceiver::get_ChannelData()
MethodInfo IChannelReceiver_get_ChannelData_m14962_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IChannelReceiver_t2898_IChannelReceiver_StartListening_m14963_ParameterInfos[] = 
{
	{"data", 0, 134221907, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StartListening(System.Object)
MethodInfo IChannelReceiver_StartListening_m14963_MethodInfo = 
{
	"StartListening"/* name */
	, NULL/* method */
	, &IChannelReceiver_t2898_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IChannelReceiver_t2898_IChannelReceiver_StartListening_m14963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IChannelReceiver_t2898_MethodInfos[] =
{
	&IChannelReceiver_get_ChannelData_m14962_MethodInfo,
	&IChannelReceiver_StartListening_m14963_MethodInfo,
	NULL
};
extern MethodInfo IChannelReceiver_get_ChannelData_m14962_MethodInfo;
static PropertyInfo IChannelReceiver_t2898____ChannelData_PropertyInfo = 
{
	&IChannelReceiver_t2898_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelReceiver_get_ChannelData_m14962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IChannelReceiver_t2898_PropertyInfos[] =
{
	&IChannelReceiver_t2898____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppType* IChannelReceiver_t2898_InterfacesTypeInfos[] = 
{
	&IChannel_t2885_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IChannelReceiver_t2898_1_0_0;
struct IChannelReceiver_t2898;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t2898_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t2898_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IChannelReceiver_t2898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelReceiver_t2898_MethodInfos/* methods */
	, IChannelReceiver_t2898_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IChannelReceiver_t2898_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 507/* custom_attributes_cache */
	, &IChannelReceiver_t2898_0_0_0/* byval_arg */
	, &IChannelReceiver_t2898_1_0_0/* this_arg */
	, &IChannelReceiver_t2898_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t2981_il2cpp_TypeInfo;
static MethodInfo* IChannelSender_t2981_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IChannelSender_t2981_InterfacesTypeInfos[] = 
{
	&IChannel_t2885_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IChannelSender_t2981_1_0_0;
struct IChannelSender_t2981;
const Il2CppTypeDefinitionMetadata IChannelSender_t2981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t2981_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IChannelSender_t2981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelSender_t2981_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IChannelSender_t2981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 508/* custom_attributes_cache */
	, &IChannelSender_t2981_0_0_0/* byval_arg */
	, &IChannelSender_t2981_1_0_0/* this_arg */
	, &IChannelSender_t2981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t2897_il2cpp_TypeInfo;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ISecurableChannel_t2897_ISecurableChannel_set_IsSecured_m14964_ParameterInfos[] = 
{
	{"value", 0, 134221908, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ISecurableChannel::set_IsSecured(System.Boolean)
MethodInfo ISecurableChannel_set_IsSecured_m14964_MethodInfo = 
{
	"set_IsSecured"/* name */
	, NULL/* method */
	, &ISecurableChannel_t2897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ISecurableChannel_t2897_ISecurableChannel_set_IsSecured_m14964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ISecurableChannel_t2897_MethodInfos[] =
{
	&ISecurableChannel_set_IsSecured_m14964_MethodInfo,
	NULL
};
extern MethodInfo ISecurableChannel_set_IsSecured_m14964_MethodInfo;
static PropertyInfo ISecurableChannel_t2897____IsSecured_PropertyInfo = 
{
	&ISecurableChannel_t2897_il2cpp_TypeInfo/* parent */
	, "IsSecured"/* name */
	, NULL/* get */
	, &ISecurableChannel_set_IsSecured_m14964_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ISecurableChannel_t2897_PropertyInfos[] =
{
	&ISecurableChannel_t2897____IsSecured_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ISecurableChannel_t2897_0_0_0;
extern Il2CppType ISecurableChannel_t2897_1_0_0;
struct ISecurableChannel_t2897;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t2897_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ISecurableChannel_t2897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ISecurableChannel_t2897_MethodInfos/* methods */
	, ISecurableChannel_t2897_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ISecurableChannel_t2897_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t2897_0_0_0/* byval_arg */
	, &ISecurableChannel_t2897_1_0_0/* this_arg */
	, &ISecurableChannel_t2897_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t2596_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::.cctor()
MethodInfo Context__cctor_m12528_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Context__cctor_m12528/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::Finalize()
MethodInfo Context_Finalize_m12529_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&Context_Finalize_m12529/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::get_DefaultContext()
MethodInfo Context_get_DefaultContext_m12530_MethodInfo = 
{
	"get_DefaultContext"/* name */
	, (methodPointerType)&Context_get_DefaultContext_m12530/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &Context_t2596_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_IsDefaultContext()
MethodInfo Context_get_IsDefaultContext_m12531_MethodInfo = 
{
	"get_IsDefaultContext"/* name */
	, (methodPointerType)&Context_get_IsDefaultContext_m12531/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Context_t2596_Context_GetProperty_m12532_ParameterInfos[] = 
{
	{"name", 0, 134221909, 0, &String_t_0_0_0},
};
extern Il2CppType IContextProperty_t2886_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String)
MethodInfo Context_GetProperty_m12532_MethodInfo = 
{
	"GetProperty"/* name */
	, (methodPointerType)&Context_GetProperty_m12532/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t2886_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Context_t2596_Context_GetProperty_m12532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.Context::ToString()
MethodInfo Context_ToString_m12533_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Context_ToString_m12533/* method */
	, &Context_t2596_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Context_t2596_MethodInfos[] =
{
	&Context__cctor_m12528_MethodInfo,
	&Context_Finalize_m12529_MethodInfo,
	&Context_get_DefaultContext_m12530_MethodInfo,
	&Context_get_IsDefaultContext_m12531_MethodInfo,
	&Context_GetProperty_m12532_MethodInfo,
	&Context_ToString_m12533_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo Context_t2596____context_id_0_FieldInfo = 
{
	"context_id"/* name */
	, &Int32_t189_0_0_1/* type */
	, &Context_t2596_il2cpp_TypeInfo/* parent */
	, offsetof(Context_t2596, ___context_id_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_1;
FieldInfo Context_t2596____context_properties_1_FieldInfo = 
{
	"context_properties"/* name */
	, &ArrayList_t737_0_0_1/* type */
	, &Context_t2596_il2cpp_TypeInfo/* parent */
	, offsetof(Context_t2596, ___context_properties_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo Context_t2596____namedSlots_2_FieldInfo = 
{
	"namedSlots"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &Context_t2596_il2cpp_TypeInfo/* parent */
	, offsetof(Context_t2596_StaticFields, ___namedSlots_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Context_t2596_FieldInfos[] =
{
	&Context_t2596____context_id_0_FieldInfo,
	&Context_t2596____context_properties_1_FieldInfo,
	&Context_t2596____namedSlots_2_FieldInfo,
	NULL
};
extern MethodInfo Context_get_DefaultContext_m12530_MethodInfo;
static PropertyInfo Context_t2596____DefaultContext_PropertyInfo = 
{
	&Context_t2596_il2cpp_TypeInfo/* parent */
	, "DefaultContext"/* name */
	, &Context_get_DefaultContext_m12530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Context_get_IsDefaultContext_m12531_MethodInfo;
static PropertyInfo Context_t2596____IsDefaultContext_PropertyInfo = 
{
	&Context_t2596_il2cpp_TypeInfo/* parent */
	, "IsDefaultContext"/* name */
	, &Context_get_IsDefaultContext_m12531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Context_t2596_PropertyInfos[] =
{
	&Context_t2596____DefaultContext_PropertyInfo,
	&Context_t2596____IsDefaultContext_PropertyInfo,
	NULL
};
extern MethodInfo Context_Finalize_m12529_MethodInfo;
extern MethodInfo Context_ToString_m12533_MethodInfo;
extern MethodInfo Context_GetProperty_m12532_MethodInfo;
static Il2CppMethodReference Context_t2596_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Context_Finalize_m12529_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Context_ToString_m12533_MethodInfo,
	&Context_GetProperty_m12532_MethodInfo,
};
static bool Context_t2596_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Context_t2596_1_0_0;
struct Context_t2596;
const Il2CppTypeDefinitionMetadata Context_t2596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t2596_VTable/* vtableMethods */
	, Context_t2596_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Context_t2596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, Context_t2596_MethodInfos/* methods */
	, Context_t2596_PropertyInfos/* properties */
	, Context_t2596_FieldInfos/* fields */
	, NULL/* events */
	, &Context_t2596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 509/* custom_attributes_cache */
	, &Context_t2596_0_0_0/* byval_arg */
	, &Context_t2596_1_0_0/* this_arg */
	, &Context_t2596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t2596)/* instance_size */
	, sizeof (Context_t2596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t2596_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t2589_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo ContextAttribute_t2589_ContextAttribute__ctor_m12534_ParameterInfos[] = 
{
	{"name", 0, 134221910, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::.ctor(System.String)
MethodInfo ContextAttribute__ctor_m12534_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextAttribute__ctor_m12534/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ContextAttribute_t2589_ContextAttribute__ctor_m12534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.ContextAttribute::get_Name()
MethodInfo ContextAttribute_get_Name_m12535_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ContextAttribute_get_Name_m12535/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ContextAttribute_t2589_ContextAttribute_Equals_m12536_ParameterInfos[] = 
{
	{"o", 0, 134221911, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::Equals(System.Object)
MethodInfo ContextAttribute_Equals_m12536_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&ContextAttribute_Equals_m12536/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, ContextAttribute_t2589_ContextAttribute_Equals_m12536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Contexts.ContextAttribute::GetHashCode()
MethodInfo ContextAttribute_GetHashCode_m12537_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&ContextAttribute_GetHashCode_m12537/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo ContextAttribute_t2589_ContextAttribute_GetPropertiesForNewContext_m12538_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221912, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo ContextAttribute_GetPropertiesForNewContext_m12538_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ContextAttribute_GetPropertiesForNewContext_m12538/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ContextAttribute_t2589_ContextAttribute_GetPropertiesForNewContext_m12538_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo ContextAttribute_t2589_ContextAttribute_IsContextOK_m12539_ParameterInfos[] = 
{
	{"ctx", 0, 134221913, 0, &Context_t2596_0_0_0},
	{"ctorMsg", 1, 134221914, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo ContextAttribute_IsContextOK_m12539_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ContextAttribute_IsContextOK_m12539/* method */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, ContextAttribute_t2589_ContextAttribute_IsContextOK_m12539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ContextAttribute_t2589_MethodInfos[] =
{
	&ContextAttribute__ctor_m12534_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&ContextAttribute_Equals_m12536_MethodInfo,
	&ContextAttribute_GetHashCode_m12537_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12538_MethodInfo,
	&ContextAttribute_IsContextOK_m12539_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_4;
FieldInfo ContextAttribute_t2589____AttributeName_0_FieldInfo = 
{
	"AttributeName"/* name */
	, &String_t_0_0_4/* type */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* parent */
	, offsetof(ContextAttribute_t2589, ___AttributeName_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ContextAttribute_t2589_FieldInfos[] =
{
	&ContextAttribute_t2589____AttributeName_0_FieldInfo,
	NULL
};
static PropertyInfo ContextAttribute_t2589____Name_PropertyInfo = 
{
	&ContextAttribute_t2589_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ContextAttribute_get_Name_m12535_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ContextAttribute_t2589_PropertyInfos[] =
{
	&ContextAttribute_t2589____Name_PropertyInfo,
	NULL
};
extern MethodInfo ContextAttribute_Equals_m12536_MethodInfo;
extern MethodInfo ContextAttribute_GetHashCode_m12537_MethodInfo;
extern MethodInfo ContextAttribute_GetPropertiesForNewContext_m12538_MethodInfo;
extern MethodInfo ContextAttribute_IsContextOK_m12539_MethodInfo;
static Il2CppMethodReference ContextAttribute_t2589_VTable[] =
{
	&ContextAttribute_Equals_m12536_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ContextAttribute_GetHashCode_m12537_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12538_MethodInfo,
	&ContextAttribute_IsContextOK_m12539_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m12538_MethodInfo,
	&ContextAttribute_IsContextOK_m12539_MethodInfo,
};
static bool ContextAttribute_t2589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextAttribute_t2589_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2896_0_0_0,
	&IContextProperty_t2886_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t2589_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
	{ &IContextAttribute_t2896_0_0_0, 4},
	{ &IContextProperty_t2886_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ContextAttribute_t2589_1_0_0;
extern Il2CppType Attribute_t1546_0_0_0;
struct ContextAttribute_t2589;
const Il2CppTypeDefinitionMetadata ContextAttribute_t2589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t2589_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t2589_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, ContextAttribute_t2589_VTable/* vtableMethods */
	, ContextAttribute_t2589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ContextAttribute_t2589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, ContextAttribute_t2589_MethodInfos/* methods */
	, ContextAttribute_t2589_PropertyInfos/* properties */
	, ContextAttribute_t2589_FieldInfos/* fields */
	, NULL/* events */
	, &ContextAttribute_t2589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 510/* custom_attributes_cache */
	, &ContextAttribute_t2589_0_0_0/* byval_arg */
	, &ContextAttribute_t2589_1_0_0/* this_arg */
	, &ContextAttribute_t2589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t2589)/* instance_size */
	, sizeof (ContextAttribute_t2589)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t2591_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
MethodInfo CrossContextChannel__ctor_m12540_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossContextChannel__ctor_m12540/* method */
	, &CrossContextChannel_t2591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CrossContextChannel_t2591_MethodInfos[] =
{
	&CrossContextChannel__ctor_m12540_MethodInfo,
	NULL
};
static Il2CppMethodReference CrossContextChannel_t2591_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CrossContextChannel_t2591_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CrossContextChannel_t2591_InterfacesTypeInfos[] = 
{
	&IMessageSink_t2187_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t2591_InterfacesOffsets[] = 
{
	{ &IMessageSink_t2187_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType CrossContextChannel_t2591_0_0_0;
extern Il2CppType CrossContextChannel_t2591_1_0_0;
struct CrossContextChannel_t2591;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t2591_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t2591_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t2591_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t2591_VTable/* vtableMethods */
	, CrossContextChannel_t2591_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CrossContextChannel_t2591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, CrossContextChannel_t2591_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CrossContextChannel_t2591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t2591_0_0_0/* byval_arg */
	, &CrossContextChannel_t2591_1_0_0/* this_arg */
	, &CrossContextChannel_t2591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t2591)/* instance_size */
	, sizeof (CrossContextChannel_t2591)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t2896_il2cpp_TypeInfo;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo IContextAttribute_t2896_IContextAttribute_GetPropertiesForNewContext_m14965_ParameterInfos[] = 
{
	{"msg", 0, 134221915, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.IContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo IContextAttribute_GetPropertiesForNewContext_m14965_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, NULL/* method */
	, &IContextAttribute_t2896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IContextAttribute_t2896_IContextAttribute_GetPropertiesForNewContext_m14965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo IContextAttribute_t2896_IContextAttribute_IsContextOK_m14966_ParameterInfos[] = 
{
	{"ctx", 0, 134221916, 0, &Context_t2596_0_0_0},
	{"msg", 1, 134221917, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo IContextAttribute_IsContextOK_m14966_MethodInfo = 
{
	"IsContextOK"/* name */
	, NULL/* method */
	, &IContextAttribute_t2896_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, IContextAttribute_t2896_IContextAttribute_IsContextOK_m14966_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IContextAttribute_t2896_MethodInfos[] =
{
	&IContextAttribute_GetPropertiesForNewContext_m14965_MethodInfo,
	&IContextAttribute_IsContextOK_m14966_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IContextAttribute_t2896_1_0_0;
struct IContextAttribute_t2896;
const Il2CppTypeDefinitionMetadata IContextAttribute_t2896_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IContextAttribute_t2896_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextAttribute_t2896_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IContextAttribute_t2896_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 511/* custom_attributes_cache */
	, &IContextAttribute_t2896_0_0_0/* byval_arg */
	, &IContextAttribute_t2896_1_0_0/* this_arg */
	, &IContextAttribute_t2896_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t2886_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name()
MethodInfo IContextProperty_get_Name_m14967_MethodInfo = 
{
	"get_Name"/* name */
	, NULL/* method */
	, &IContextProperty_t2886_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IContextProperty_t2886_MethodInfos[] =
{
	&IContextProperty_get_Name_m14967_MethodInfo,
	NULL
};
extern MethodInfo IContextProperty_get_Name_m14967_MethodInfo;
static PropertyInfo IContextProperty_t2886____Name_PropertyInfo = 
{
	&IContextProperty_t2886_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &IContextProperty_get_Name_m14967_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IContextProperty_t2886_PropertyInfos[] =
{
	&IContextProperty_t2886____Name_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IContextProperty_t2886_1_0_0;
struct IContextProperty_t2886;
const Il2CppTypeDefinitionMetadata IContextProperty_t2886_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IContextProperty_t2886_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextProperty_t2886_MethodInfos/* methods */
	, IContextProperty_t2886_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IContextProperty_t2886_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 512/* custom_attributes_cache */
	, &IContextProperty_t2886_0_0_0/* byval_arg */
	, &IContextProperty_t2886_1_0_0/* this_arg */
	, &IContextProperty_t2886_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t2982_il2cpp_TypeInfo;
static MethodInfo* IContributeClientContextSink_t2982_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IContributeClientContextSink_t2982_0_0_0;
extern Il2CppType IContributeClientContextSink_t2982_1_0_0;
struct IContributeClientContextSink_t2982;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t2982_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IContributeClientContextSink_t2982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeClientContextSink_t2982_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IContributeClientContextSink_t2982_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 513/* custom_attributes_cache */
	, &IContributeClientContextSink_t2982_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t2982_1_0_0/* this_arg */
	, &IContributeClientContextSink_t2982_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t2983_il2cpp_TypeInfo;
static MethodInfo* IContributeServerContextSink_t2983_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IContributeServerContextSink_t2983_0_0_0;
extern Il2CppType IContributeServerContextSink_t2983_1_0_0;
struct IContributeServerContextSink_t2983;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t2983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IContributeServerContextSink_t2983_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeServerContextSink_t2983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IContributeServerContextSink_t2983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 514/* custom_attributes_cache */
	, &IContributeServerContextSink_t2983_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t2983_1_0_0/* this_arg */
	, &IContributeServerContextSink_t2983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t2598_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor()
MethodInfo SynchronizationAttribute__ctor_m12541_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m12541/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo SynchronizationAttribute_t2598_SynchronizationAttribute__ctor_m12542_ParameterInfos[] = 
{
	{"flag", 0, 134221918, 0, &Int32_t189_0_0_0},
	{"reEntrant", 1, 134221919, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor(System.Int32,System.Boolean)
MethodInfo SynchronizationAttribute__ctor_m12542_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m12542/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189_SByte_t236/* invoker_method */
	, SynchronizationAttribute_t2598_SynchronizationAttribute__ctor_m12542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo SynchronizationAttribute_t2598_SynchronizationAttribute_set_Locked_m12543_ParameterInfos[] = 
{
	{"value", 0, 134221920, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::set_Locked(System.Boolean)
MethodInfo SynchronizationAttribute_set_Locked_m12543_MethodInfo = 
{
	"set_Locked"/* name */
	, (methodPointerType)&SynchronizationAttribute_set_Locked_m12543/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, SynchronizationAttribute_t2598_SynchronizationAttribute_set_Locked_m12543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ReleaseLock()
MethodInfo SynchronizationAttribute_ReleaseLock_m12544_MethodInfo = 
{
	"ReleaseLock"/* name */
	, (methodPointerType)&SynchronizationAttribute_ReleaseLock_m12544/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo SynchronizationAttribute_t2598_SynchronizationAttribute_GetPropertiesForNewContext_m12545_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221921, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m12545_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_GetPropertiesForNewContext_m12545/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, SynchronizationAttribute_t2598_SynchronizationAttribute_GetPropertiesForNewContext_m12545_ParameterInfos/* parameters */
	, 516/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo SynchronizationAttribute_t2598_SynchronizationAttribute_IsContextOK_m12546_ParameterInfos[] = 
{
	{"ctx", 0, 134221922, 0, &Context_t2596_0_0_0},
	{"msg", 1, 134221923, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.SynchronizationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo SynchronizationAttribute_IsContextOK_m12546_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&SynchronizationAttribute_IsContextOK_m12546/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, SynchronizationAttribute_t2598_SynchronizationAttribute_IsContextOK_m12546_ParameterInfos/* parameters */
	, 517/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ExitContext()
MethodInfo SynchronizationAttribute_ExitContext_m12547_MethodInfo = 
{
	"ExitContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_ExitContext_m12547/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::EnterContext()
MethodInfo SynchronizationAttribute_EnterContext_m12548_MethodInfo = 
{
	"EnterContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_EnterContext_m12548/* method */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SynchronizationAttribute_t2598_MethodInfos[] =
{
	&SynchronizationAttribute__ctor_m12541_MethodInfo,
	&SynchronizationAttribute__ctor_m12542_MethodInfo,
	&SynchronizationAttribute_set_Locked_m12543_MethodInfo,
	&SynchronizationAttribute_ReleaseLock_m12544_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12545_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12546_MethodInfo,
	&SynchronizationAttribute_ExitContext_m12547_MethodInfo,
	&SynchronizationAttribute_EnterContext_m12548_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo SynchronizationAttribute_t2598_____bReEntrant_1_FieldInfo = 
{
	"_bReEntrant"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, offsetof(SynchronizationAttribute_t2598, ____bReEntrant_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo SynchronizationAttribute_t2598_____flavor_2_FieldInfo = 
{
	"_flavor"/* name */
	, &Int32_t189_0_0_1/* type */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, offsetof(SynchronizationAttribute_t2598, ____flavor_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_129;
FieldInfo SynchronizationAttribute_t2598_____lockCount_3_FieldInfo = 
{
	"_lockCount"/* name */
	, &Int32_t189_0_0_129/* type */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, offsetof(SynchronizationAttribute_t2598, ____lockCount_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Mutex_t2597_0_0_129;
FieldInfo SynchronizationAttribute_t2598_____mutex_4_FieldInfo = 
{
	"_mutex"/* name */
	, &Mutex_t2597_0_0_129/* type */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, offsetof(SynchronizationAttribute_t2598, ____mutex_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Thread_t993_0_0_129;
FieldInfo SynchronizationAttribute_t2598_____ownerThread_5_FieldInfo = 
{
	"_ownerThread"/* name */
	, &Thread_t993_0_0_129/* type */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, offsetof(SynchronizationAttribute_t2598, ____ownerThread_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* SynchronizationAttribute_t2598_FieldInfos[] =
{
	&SynchronizationAttribute_t2598_____bReEntrant_1_FieldInfo,
	&SynchronizationAttribute_t2598_____flavor_2_FieldInfo,
	&SynchronizationAttribute_t2598_____lockCount_3_FieldInfo,
	&SynchronizationAttribute_t2598_____mutex_4_FieldInfo,
	&SynchronizationAttribute_t2598_____ownerThread_5_FieldInfo,
	NULL
};
extern MethodInfo SynchronizationAttribute_set_Locked_m12543_MethodInfo;
static PropertyInfo SynchronizationAttribute_t2598____Locked_PropertyInfo = 
{
	&SynchronizationAttribute_t2598_il2cpp_TypeInfo/* parent */
	, "Locked"/* name */
	, NULL/* get */
	, &SynchronizationAttribute_set_Locked_m12543_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* SynchronizationAttribute_t2598_PropertyInfos[] =
{
	&SynchronizationAttribute_t2598____Locked_PropertyInfo,
	NULL
};
extern MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m12545_MethodInfo;
extern MethodInfo SynchronizationAttribute_IsContextOK_m12546_MethodInfo;
static Il2CppMethodReference SynchronizationAttribute_t2598_VTable[] =
{
	&ContextAttribute_Equals_m12536_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&ContextAttribute_GetHashCode_m12537_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12545_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12546_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&ContextAttribute_get_Name_m12535_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m12545_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m12546_MethodInfo,
	&SynchronizationAttribute_set_Locked_m12543_MethodInfo,
};
static bool SynchronizationAttribute_t2598_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SynchronizationAttribute_t2598_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t2982_0_0_0,
	&IContributeServerContextSink_t2983_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t2598_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2896_0_0_0, 4},
	{ &IContextProperty_t2886_0_0_0, 6},
	{ &_Attribute_t1731_0_0_0, 4},
	{ &IContributeClientContextSink_t2982_0_0_0, 10},
	{ &IContributeServerContextSink_t2983_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType SynchronizationAttribute_t2598_0_0_0;
extern Il2CppType SynchronizationAttribute_t2598_1_0_0;
struct SynchronizationAttribute_t2598;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t2598_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t2598_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t2598_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t2589_0_0_0/* parent */
	, SynchronizationAttribute_t2598_VTable/* vtableMethods */
	, SynchronizationAttribute_t2598_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SynchronizationAttribute_t2598_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, SynchronizationAttribute_t2598_MethodInfos/* methods */
	, SynchronizationAttribute_t2598_PropertyInfos/* properties */
	, SynchronizationAttribute_t2598_FieldInfos/* fields */
	, NULL/* events */
	, &SynchronizationAttribute_t2598_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 515/* custom_attributes_cache */
	, &SynchronizationAttribute_t2598_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t2598_1_0_0/* this_arg */
	, &SynchronizationAttribute_t2598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t2598)/* instance_size */
	, sizeof (SynchronizationAttribute_t2598)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t2599_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static MethodInfo* ArgInfoType_t2599_MethodInfos[] =
{
	NULL
};
extern Il2CppType Byte_t237_0_0_1542;
FieldInfo ArgInfoType_t2599____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t237_0_0_1542/* type */
	, &ArgInfoType_t2599_il2cpp_TypeInfo/* parent */
	, offsetof(ArgInfoType_t2599, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArgInfoType_t2599_0_0_32854;
FieldInfo ArgInfoType_t2599____In_2_FieldInfo = 
{
	"In"/* name */
	, &ArgInfoType_t2599_0_0_32854/* type */
	, &ArgInfoType_t2599_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArgInfoType_t2599_0_0_32854;
FieldInfo ArgInfoType_t2599____Out_3_FieldInfo = 
{
	"Out"/* name */
	, &ArgInfoType_t2599_0_0_32854/* type */
	, &ArgInfoType_t2599_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ArgInfoType_t2599_FieldInfos[] =
{
	&ArgInfoType_t2599____value___1_FieldInfo,
	&ArgInfoType_t2599____In_2_FieldInfo,
	&ArgInfoType_t2599____Out_3_FieldInfo,
	NULL
};
static const uint8_t ArgInfoType_t2599____In_2_DefaultValueData = 0;
extern Il2CppType Byte_t237_0_0_0;
static Il2CppFieldDefaultValueEntry ArgInfoType_t2599____In_2_DefaultValue = 
{
	&ArgInfoType_t2599____In_2_FieldInfo/* field */
	, { (char*)&ArgInfoType_t2599____In_2_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t ArgInfoType_t2599____Out_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry ArgInfoType_t2599____Out_3_DefaultValue = 
{
	&ArgInfoType_t2599____Out_3_FieldInfo/* field */
	, { (char*)&ArgInfoType_t2599____Out_3_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ArgInfoType_t2599_FieldDefaultValues[] = 
{
	&ArgInfoType_t2599____In_2_DefaultValue,
	&ArgInfoType_t2599____Out_3_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference ArgInfoType_t2599_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ArgInfoType_t2599_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair ArgInfoType_t2599_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ArgInfoType_t2599_0_0_0;
extern Il2CppType ArgInfoType_t2599_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t237_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t2599_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t2599_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ArgInfoType_t2599_VTable/* vtableMethods */
	, ArgInfoType_t2599_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ArgInfoType_t2599_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfoType_t2599_MethodInfos/* methods */
	, NULL/* properties */
	, ArgInfoType_t2599_FieldInfos/* fields */
	, NULL/* events */
	, &Byte_t237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t2599_0_0_0/* byval_arg */
	, &ArgInfoType_t2599_1_0_0/* this_arg */
	, &ArgInfoType_t2599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ArgInfoType_t2599_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t2599)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t2599)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t2600_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
extern Il2CppType MethodBase_t1691_0_0_0;
extern Il2CppType MethodBase_t1691_0_0_0;
extern Il2CppType ArgInfoType_t2599_0_0_0;
static ParameterInfo ArgInfo_t2600_ArgInfo__ctor_m12549_ParameterInfos[] = 
{
	{"method", 0, 134221924, 0, &MethodBase_t1691_0_0_0},
	{"type", 1, 134221925, 0, &ArgInfoType_t2599_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Byte_t237 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ArgInfo::.ctor(System.Reflection.MethodBase,System.Runtime.Remoting.Messaging.ArgInfoType)
MethodInfo ArgInfo__ctor_m12549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgInfo__ctor_m12549/* method */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Byte_t237/* invoker_method */
	, ArgInfo_t2600_ArgInfo__ctor_m12549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo ArgInfo_t2600_ArgInfo_GetInOutArgs_m12550_ParameterInfos[] = 
{
	{"args", 0, 134221926, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ArgInfo::GetInOutArgs(System.Object[])
MethodInfo ArgInfo_GetInOutArgs_m12550_MethodInfo = 
{
	"GetInOutArgs"/* name */
	, (methodPointerType)&ArgInfo_GetInOutArgs_m12550/* method */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ArgInfo_t2600_ArgInfo_GetInOutArgs_m12550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ArgInfo_t2600_MethodInfos[] =
{
	&ArgInfo__ctor_m12549_MethodInfo,
	&ArgInfo_GetInOutArgs_m12550_MethodInfo,
	NULL
};
extern Il2CppType Int32U5BU5D_t107_0_0_1;
FieldInfo ArgInfo_t2600_____paramMap_0_FieldInfo = 
{
	"_paramMap"/* name */
	, &Int32U5BU5D_t107_0_0_1/* type */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* parent */
	, offsetof(ArgInfo_t2600, ____paramMap_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ArgInfo_t2600_____inoutArgCount_1_FieldInfo = 
{
	"_inoutArgCount"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* parent */
	, offsetof(ArgInfo_t2600, ____inoutArgCount_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodBase_t1691_0_0_1;
FieldInfo ArgInfo_t2600_____method_2_FieldInfo = 
{
	"_method"/* name */
	, &MethodBase_t1691_0_0_1/* type */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* parent */
	, offsetof(ArgInfo_t2600, ____method_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ArgInfo_t2600_FieldInfos[] =
{
	&ArgInfo_t2600_____paramMap_0_FieldInfo,
	&ArgInfo_t2600_____inoutArgCount_1_FieldInfo,
	&ArgInfo_t2600_____method_2_FieldInfo,
	NULL
};
static Il2CppMethodReference ArgInfo_t2600_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ArgInfo_t2600_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ArgInfo_t2600_0_0_0;
extern Il2CppType ArgInfo_t2600_1_0_0;
struct ArgInfo_t2600;
const Il2CppTypeDefinitionMetadata ArgInfo_t2600_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t2600_VTable/* vtableMethods */
	, ArgInfo_t2600_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ArgInfo_t2600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfo_t2600_MethodInfos/* methods */
	, NULL/* properties */
	, ArgInfo_t2600_FieldInfos/* fields */
	, NULL/* events */
	, &ArgInfo_t2600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t2600_0_0_0/* byval_arg */
	, &ArgInfo_t2600_1_0_0/* this_arg */
	, &ArgInfo_t2600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t2600)/* instance_size */
	, sizeof (ArgInfo_t2600)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t2605_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::.ctor()
MethodInfo AsyncResult__ctor_m12551_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsyncResult__ctor_m12551/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncState()
MethodInfo AsyncResult_get_AsyncState_m12552_MethodInfo = 
{
	"get_AsyncState"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncState_m12552/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WaitHandle_t2308_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Threading.WaitHandle System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncWaitHandle()
MethodInfo AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo = 
{
	"get_AsyncWaitHandle"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncWaitHandle_m12553/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &WaitHandle_t2308_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_CompletedSynchronously()
MethodInfo AsyncResult_get_CompletedSynchronously_m12554_MethodInfo = 
{
	"get_CompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_get_CompletedSynchronously_m12554/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_IsCompleted()
MethodInfo AsyncResult_get_IsCompleted_m12555_MethodInfo = 
{
	"get_IsCompleted"/* name */
	, (methodPointerType)&AsyncResult_get_IsCompleted_m12555/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_EndInvokeCalled()
MethodInfo AsyncResult_get_EndInvokeCalled_m12556_MethodInfo = 
{
	"get_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_get_EndInvokeCalled_m12556/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_set_EndInvokeCalled_m12557_ParameterInfos[] = 
{
	{"value", 0, 134221927, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_EndInvokeCalled(System.Boolean)
MethodInfo AsyncResult_set_EndInvokeCalled_m12557_MethodInfo = 
{
	"set_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_set_EndInvokeCalled_m12557/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, AsyncResult_t2605_AsyncResult_set_EndInvokeCalled_m12557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate()
MethodInfo AsyncResult_get_AsyncDelegate_m12558_MethodInfo = 
{
	"get_AsyncDelegate"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncDelegate_m12558/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessageSink_t2187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.AsyncResult::get_NextSink()
MethodInfo AsyncResult_get_NextSink_m12559_MethodInfo = 
{
	"get_NextSink"/* name */
	, (methodPointerType)&AsyncResult_get_NextSink_m12559/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t2187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessage_t2604_0_0_0;
extern Il2CppType IMessageSink_t2187_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_AsyncProcessMessage_m12560_ParameterInfos[] = 
{
	{"msg", 0, 134221928, 0, &IMessage_t2604_0_0_0},
	{"replySink", 1, 134221929, 0, &IMessageSink_t2187_0_0_0},
};
extern Il2CppType IMessageCtrl_t2603_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.AsyncResult::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
MethodInfo AsyncResult_AsyncProcessMessage_m12560_MethodInfo = 
{
	"AsyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_AsyncProcessMessage_m12560/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &IMessageCtrl_t2603_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, AsyncResult_t2605_AsyncResult_AsyncProcessMessage_m12560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessage_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::GetReplyMessage()
MethodInfo AsyncResult_GetReplyMessage_m12561_MethodInfo = 
{
	"GetReplyMessage"/* name */
	, (methodPointerType)&AsyncResult_GetReplyMessage_m12561/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessageCtrl_t2603_0_0_0;
extern Il2CppType IMessageCtrl_t2603_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_SetMessageCtrl_m12562_ParameterInfos[] = 
{
	{"mc", 0, 134221930, 0, &IMessageCtrl_t2603_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetMessageCtrl(System.Runtime.Remoting.Messaging.IMessageCtrl)
MethodInfo AsyncResult_SetMessageCtrl_m12562_MethodInfo = 
{
	"SetMessageCtrl"/* name */
	, (methodPointerType)&AsyncResult_SetMessageCtrl_m12562/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, AsyncResult_t2605_AsyncResult_SetMessageCtrl_m12562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_SetCompletedSynchronously_m12563_ParameterInfos[] = 
{
	{"completed", 0, 134221931, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetCompletedSynchronously(System.Boolean)
MethodInfo AsyncResult_SetCompletedSynchronously_m12563_MethodInfo = 
{
	"SetCompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_SetCompletedSynchronously_m12563/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, AsyncResult_t2605_AsyncResult_SetCompletedSynchronously_m12563_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessage_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::EndInvoke()
MethodInfo AsyncResult_EndInvoke_m12564_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AsyncResult_EndInvoke_m12564/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessage_t2604_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_SyncProcessMessage_m12565_ParameterInfos[] = 
{
	{"msg", 0, 134221932, 0, &IMessage_t2604_0_0_0},
};
extern Il2CppType IMessage_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
MethodInfo AsyncResult_SyncProcessMessage_m12565_MethodInfo = 
{
	"SyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_SyncProcessMessage_m12565/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsyncResult_t2605_AsyncResult_SyncProcessMessage_m12565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MonoMethodMessage_t2602_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.MonoMethodMessage System.Runtime.Remoting.Messaging.AsyncResult::get_CallMessage()
MethodInfo AsyncResult_get_CallMessage_m12566_MethodInfo = 
{
	"get_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_get_CallMessage_m12566/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodMessage_t2602_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MonoMethodMessage_t2602_0_0_0;
extern Il2CppType MonoMethodMessage_t2602_0_0_0;
static ParameterInfo AsyncResult_t2605_AsyncResult_set_CallMessage_m12567_ParameterInfos[] = 
{
	{"value", 0, 134221933, 0, &MonoMethodMessage_t2602_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_CallMessage(System.Runtime.Remoting.Messaging.MonoMethodMessage)
MethodInfo AsyncResult_set_CallMessage_m12567_MethodInfo = 
{
	"set_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_set_CallMessage_m12567/* method */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, AsyncResult_t2605_AsyncResult_set_CallMessage_m12567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AsyncResult_t2605_MethodInfos[] =
{
	&AsyncResult__ctor_m12551_MethodInfo,
	&AsyncResult_get_AsyncState_m12552_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m12554_MethodInfo,
	&AsyncResult_get_IsCompleted_m12555_MethodInfo,
	&AsyncResult_get_EndInvokeCalled_m12556_MethodInfo,
	&AsyncResult_set_EndInvokeCalled_m12557_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m12558_MethodInfo,
	&AsyncResult_get_NextSink_m12559_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m12560_MethodInfo,
	&AsyncResult_GetReplyMessage_m12561_MethodInfo,
	&AsyncResult_SetMessageCtrl_m12562_MethodInfo,
	&AsyncResult_SetCompletedSynchronously_m12563_MethodInfo,
	&AsyncResult_EndInvoke_m12564_MethodInfo,
	&AsyncResult_SyncProcessMessage_m12565_MethodInfo,
	&AsyncResult_get_CallMessage_m12566_MethodInfo,
	&AsyncResult_set_CallMessage_m12567_MethodInfo,
	NULL
};
extern Il2CppType Object_t_0_0_1;
FieldInfo AsyncResult_t2605____async_state_0_FieldInfo = 
{
	"async_state"/* name */
	, &Object_t_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___async_state_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType WaitHandle_t2308_0_0_1;
FieldInfo AsyncResult_t2605____handle_1_FieldInfo = 
{
	"handle"/* name */
	, &WaitHandle_t2308_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___handle_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo AsyncResult_t2605____async_delegate_2_FieldInfo = 
{
	"async_delegate"/* name */
	, &Object_t_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___async_delegate_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t_0_0_1;
FieldInfo AsyncResult_t2605____data_3_FieldInfo = 
{
	"data"/* name */
	, &IntPtr_t_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___data_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo AsyncResult_t2605____object_data_4_FieldInfo = 
{
	"object_data"/* name */
	, &Object_t_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___object_data_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo AsyncResult_t2605____sync_completed_5_FieldInfo = 
{
	"sync_completed"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___sync_completed_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo AsyncResult_t2605____completed_6_FieldInfo = 
{
	"completed"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___completed_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo AsyncResult_t2605____endinvoke_called_7_FieldInfo = 
{
	"endinvoke_called"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___endinvoke_called_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo AsyncResult_t2605____async_callback_8_FieldInfo = 
{
	"async_callback"/* name */
	, &Object_t_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___async_callback_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ExecutionContext_t2601_0_0_1;
FieldInfo AsyncResult_t2605____current_9_FieldInfo = 
{
	"current"/* name */
	, &ExecutionContext_t2601_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___current_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ExecutionContext_t2601_0_0_1;
FieldInfo AsyncResult_t2605____original_10_FieldInfo = 
{
	"original"/* name */
	, &ExecutionContext_t2601_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___original_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo AsyncResult_t2605____gchandle_11_FieldInfo = 
{
	"gchandle"/* name */
	, &Int32_t189_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___gchandle_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MonoMethodMessage_t2602_0_0_1;
FieldInfo AsyncResult_t2605____call_message_12_FieldInfo = 
{
	"call_message"/* name */
	, &MonoMethodMessage_t2602_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___call_message_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMessageCtrl_t2603_0_0_1;
FieldInfo AsyncResult_t2605____message_ctrl_13_FieldInfo = 
{
	"message_ctrl"/* name */
	, &IMessageCtrl_t2603_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___message_ctrl_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMessage_t2604_0_0_1;
FieldInfo AsyncResult_t2605____reply_message_14_FieldInfo = 
{
	"reply_message"/* name */
	, &IMessage_t2604_0_0_1/* type */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, offsetof(AsyncResult_t2605, ___reply_message_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* AsyncResult_t2605_FieldInfos[] =
{
	&AsyncResult_t2605____async_state_0_FieldInfo,
	&AsyncResult_t2605____handle_1_FieldInfo,
	&AsyncResult_t2605____async_delegate_2_FieldInfo,
	&AsyncResult_t2605____data_3_FieldInfo,
	&AsyncResult_t2605____object_data_4_FieldInfo,
	&AsyncResult_t2605____sync_completed_5_FieldInfo,
	&AsyncResult_t2605____completed_6_FieldInfo,
	&AsyncResult_t2605____endinvoke_called_7_FieldInfo,
	&AsyncResult_t2605____async_callback_8_FieldInfo,
	&AsyncResult_t2605____current_9_FieldInfo,
	&AsyncResult_t2605____original_10_FieldInfo,
	&AsyncResult_t2605____gchandle_11_FieldInfo,
	&AsyncResult_t2605____call_message_12_FieldInfo,
	&AsyncResult_t2605____message_ctrl_13_FieldInfo,
	&AsyncResult_t2605____reply_message_14_FieldInfo,
	NULL
};
extern MethodInfo AsyncResult_get_AsyncState_m12552_MethodInfo;
static PropertyInfo AsyncResult_t2605____AsyncState_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "AsyncState"/* name */
	, &AsyncResult_get_AsyncState_m12552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo;
static PropertyInfo AsyncResult_t2605____AsyncWaitHandle_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "AsyncWaitHandle"/* name */
	, &AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_CompletedSynchronously_m12554_MethodInfo;
static PropertyInfo AsyncResult_t2605____CompletedSynchronously_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "CompletedSynchronously"/* name */
	, &AsyncResult_get_CompletedSynchronously_m12554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_IsCompleted_m12555_MethodInfo;
static PropertyInfo AsyncResult_t2605____IsCompleted_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "IsCompleted"/* name */
	, &AsyncResult_get_IsCompleted_m12555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_EndInvokeCalled_m12556_MethodInfo;
extern MethodInfo AsyncResult_set_EndInvokeCalled_m12557_MethodInfo;
static PropertyInfo AsyncResult_t2605____EndInvokeCalled_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "EndInvokeCalled"/* name */
	, &AsyncResult_get_EndInvokeCalled_m12556_MethodInfo/* get */
	, &AsyncResult_set_EndInvokeCalled_m12557_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_AsyncDelegate_m12558_MethodInfo;
static PropertyInfo AsyncResult_t2605____AsyncDelegate_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "AsyncDelegate"/* name */
	, &AsyncResult_get_AsyncDelegate_m12558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_NextSink_m12559_MethodInfo;
static PropertyInfo AsyncResult_t2605____NextSink_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "NextSink"/* name */
	, &AsyncResult_get_NextSink_m12559_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo AsyncResult_get_CallMessage_m12566_MethodInfo;
extern MethodInfo AsyncResult_set_CallMessage_m12567_MethodInfo;
static PropertyInfo AsyncResult_t2605____CallMessage_PropertyInfo = 
{
	&AsyncResult_t2605_il2cpp_TypeInfo/* parent */
	, "CallMessage"/* name */
	, &AsyncResult_get_CallMessage_m12566_MethodInfo/* get */
	, &AsyncResult_set_CallMessage_m12567_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* AsyncResult_t2605_PropertyInfos[] =
{
	&AsyncResult_t2605____AsyncState_PropertyInfo,
	&AsyncResult_t2605____AsyncWaitHandle_PropertyInfo,
	&AsyncResult_t2605____CompletedSynchronously_PropertyInfo,
	&AsyncResult_t2605____IsCompleted_PropertyInfo,
	&AsyncResult_t2605____EndInvokeCalled_PropertyInfo,
	&AsyncResult_t2605____AsyncDelegate_PropertyInfo,
	&AsyncResult_t2605____NextSink_PropertyInfo,
	&AsyncResult_t2605____CallMessage_PropertyInfo,
	NULL
};
extern MethodInfo AsyncResult_AsyncProcessMessage_m12560_MethodInfo;
extern MethodInfo AsyncResult_GetReplyMessage_m12561_MethodInfo;
extern MethodInfo AsyncResult_SetMessageCtrl_m12562_MethodInfo;
extern MethodInfo AsyncResult_SyncProcessMessage_m12565_MethodInfo;
static Il2CppMethodReference AsyncResult_t2605_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&AsyncResult_get_AsyncState_m12552_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo,
	&AsyncResult_get_IsCompleted_m12555_MethodInfo,
	&AsyncResult_get_AsyncState_m12552_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m12553_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m12554_MethodInfo,
	&AsyncResult_get_IsCompleted_m12555_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m12558_MethodInfo,
	&AsyncResult_get_NextSink_m12559_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m12560_MethodInfo,
	&AsyncResult_GetReplyMessage_m12561_MethodInfo,
	&AsyncResult_SetMessageCtrl_m12562_MethodInfo,
	&AsyncResult_SyncProcessMessage_m12565_MethodInfo,
};
static bool AsyncResult_t2605_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IAsyncResult_t19_0_0_0;
static const Il2CppType* AsyncResult_t2605_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t19_0_0_0,
	&IMessageSink_t2187_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t2605_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t19_0_0_0, 4},
	{ &IMessageSink_t2187_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType AsyncResult_t2605_0_0_0;
extern Il2CppType AsyncResult_t2605_1_0_0;
struct AsyncResult_t2605;
const Il2CppTypeDefinitionMetadata AsyncResult_t2605_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t2605_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t2605_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t2605_VTable/* vtableMethods */
	, AsyncResult_t2605_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo AsyncResult_t2605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, AsyncResult_t2605_MethodInfos/* methods */
	, AsyncResult_t2605_PropertyInfos/* properties */
	, AsyncResult_t2605_FieldInfos/* fields */
	, NULL/* events */
	, &AsyncResult_t2605_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 518/* custom_attributes_cache */
	, &AsyncResult_t2605_0_0_0/* byval_arg */
	, &AsyncResult_t2605_1_0_0/* this_arg */
	, &AsyncResult_t2605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t2605)/* instance_size */
	, sizeof (AsyncResult_t2605)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t2606_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall__ctor_m12568_ParameterInfos[] = 
{
	{"type", 0, 134221934, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Type)
MethodInfo ConstructionCall__ctor_m12568_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m12568/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall__ctor_m12568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall__ctor_m12569_ParameterInfos[] = 
{
	{"info", 0, 134221935, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221936, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo ConstructionCall__ctor_m12569_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m12569/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall__ctor_m12569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitDictionary()
MethodInfo ConstructionCall_InitDictionary_m12570_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&ConstructionCall_InitDictionary_m12570/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall_set_IsContextOk_m12571_ParameterInfos[] = 
{
	{"value", 0, 134221937, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_IsContextOk(System.Boolean)
MethodInfo ConstructionCall_set_IsContextOk_m12571_MethodInfo = 
{
	"set_IsContextOk"/* name */
	, (methodPointerType)&ConstructionCall_set_IsContextOk_m12571/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall_set_IsContextOk_m12571_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationType()
MethodInfo ConstructionCall_get_ActivationType_m12572_MethodInfo = 
{
	"get_ActivationType"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationType_m12572/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationTypeName()
MethodInfo ConstructionCall_get_ActivationTypeName_m12573_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationTypeName_m12573/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IActivator_t2582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Messaging.ConstructionCall::get_Activator()
MethodInfo ConstructionCall_get_Activator_m12574_MethodInfo = 
{
	"get_Activator"/* name */
	, (methodPointerType)&ConstructionCall_get_Activator_m12574/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t2582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IActivator_t2582_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall_set_Activator_m12575_ParameterInfos[] = 
{
	{"value", 0, 134221938, 0, &IActivator_t2582_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator)
MethodInfo ConstructionCall_set_Activator_m12575_MethodInfo = 
{
	"set_Activator"/* name */
	, (methodPointerType)&ConstructionCall_set_Activator_m12575/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall_set_Activator_m12575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ConstructionCall::get_CallSiteActivationAttributes()
MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m12576_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_get_CallSiteActivationAttributes_m12576/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall_SetActivationAttributes_m12577_ParameterInfos[] = 
{
	{"attributes", 0, 134221939, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::SetActivationAttributes(System.Object[])
MethodInfo ConstructionCall_SetActivationAttributes_m12577_MethodInfo = 
{
	"SetActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_SetActivationAttributes_m12577/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall_SetActivationAttributes_m12577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IList_t183_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Messaging.ConstructionCall::get_ContextProperties()
MethodInfo ConstructionCall_get_ContextProperties_m12578_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, (methodPointerType)&ConstructionCall_get_ContextProperties_m12578/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &IList_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall_InitMethodProperty_m12579_ParameterInfos[] = 
{
	{"key", 0, 134221940, 0, &String_t_0_0_0},
	{"value", 1, 134221941, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitMethodProperty(System.String,System.Object)
MethodInfo ConstructionCall_InitMethodProperty_m12579_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&ConstructionCall_InitMethodProperty_m12579/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall_InitMethodProperty_m12579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo ConstructionCall_t2606_ConstructionCall_GetObjectData_m12580_ParameterInfos[] = 
{
	{"info", 0, 134221942, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221943, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo ConstructionCall_GetObjectData_m12580_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ConstructionCall_GetObjectData_m12580/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, ConstructionCall_t2606_ConstructionCall_GetObjectData_m12580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ConstructionCall::get_Properties()
MethodInfo ConstructionCall_get_Properties_m12581_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ConstructionCall_get_Properties_m12581/* method */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ConstructionCall_t2606_MethodInfos[] =
{
	&ConstructionCall__ctor_m12568_MethodInfo,
	&ConstructionCall__ctor_m12569_MethodInfo,
	&ConstructionCall_InitDictionary_m12570_MethodInfo,
	&ConstructionCall_set_IsContextOk_m12571_MethodInfo,
	&ConstructionCall_get_ActivationType_m12572_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m12573_MethodInfo,
	&ConstructionCall_get_Activator_m12574_MethodInfo,
	&ConstructionCall_set_Activator_m12575_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m12576_MethodInfo,
	&ConstructionCall_SetActivationAttributes_m12577_MethodInfo,
	&ConstructionCall_get_ContextProperties_m12578_MethodInfo,
	&ConstructionCall_InitMethodProperty_m12579_MethodInfo,
	&ConstructionCall_GetObjectData_m12580_MethodInfo,
	&ConstructionCall_get_Properties_m12581_MethodInfo,
	NULL
};
extern Il2CppType IActivator_t2582_0_0_1;
FieldInfo ConstructionCall_t2606_____activator_11_FieldInfo = 
{
	"_activator"/* name */
	, &IActivator_t2582_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____activator_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo ConstructionCall_t2606_____activationAttributes_12_FieldInfo = 
{
	"_activationAttributes"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____activationAttributes_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IList_t183_0_0_1;
FieldInfo ConstructionCall_t2606_____contextProperties_13_FieldInfo = 
{
	"_contextProperties"/* name */
	, &IList_t183_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____contextProperties_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Type_t_0_0_1;
FieldInfo ConstructionCall_t2606_____activationType_14_FieldInfo = 
{
	"_activationType"/* name */
	, &Type_t_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____activationType_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ConstructionCall_t2606_____activationTypeName_15_FieldInfo = 
{
	"_activationTypeName"/* name */
	, &String_t_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____activationTypeName_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo ConstructionCall_t2606_____isContextOk_16_FieldInfo = 
{
	"_isContextOk"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606, ____isContextOk_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo ConstructionCall_t2606____U3CU3Ef__switchU24map20_17_FieldInfo = 
{
	"<>f__switch$map20"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCall_t2606_StaticFields, ___U3CU3Ef__switchU24map20_17)/* offset */
	, 520/* custom_attributes_cache */

};
static FieldInfo* ConstructionCall_t2606_FieldInfos[] =
{
	&ConstructionCall_t2606_____activator_11_FieldInfo,
	&ConstructionCall_t2606_____activationAttributes_12_FieldInfo,
	&ConstructionCall_t2606_____contextProperties_13_FieldInfo,
	&ConstructionCall_t2606_____activationType_14_FieldInfo,
	&ConstructionCall_t2606_____activationTypeName_15_FieldInfo,
	&ConstructionCall_t2606_____isContextOk_16_FieldInfo,
	&ConstructionCall_t2606____U3CU3Ef__switchU24map20_17_FieldInfo,
	NULL
};
extern MethodInfo ConstructionCall_set_IsContextOk_m12571_MethodInfo;
static PropertyInfo ConstructionCall_t2606____IsContextOk_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "IsContextOk"/* name */
	, NULL/* get */
	, &ConstructionCall_set_IsContextOk_m12571_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_ActivationType_m12572_MethodInfo;
static PropertyInfo ConstructionCall_t2606____ActivationType_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &ConstructionCall_get_ActivationType_m12572_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_ActivationTypeName_m12573_MethodInfo;
static PropertyInfo ConstructionCall_t2606____ActivationTypeName_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &ConstructionCall_get_ActivationTypeName_m12573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_Activator_m12574_MethodInfo;
extern MethodInfo ConstructionCall_set_Activator_m12575_MethodInfo;
static PropertyInfo ConstructionCall_t2606____Activator_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &ConstructionCall_get_Activator_m12574_MethodInfo/* get */
	, &ConstructionCall_set_Activator_m12575_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m12576_MethodInfo;
static PropertyInfo ConstructionCall_t2606____CallSiteActivationAttributes_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &ConstructionCall_get_CallSiteActivationAttributes_m12576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_ContextProperties_m12578_MethodInfo;
static PropertyInfo ConstructionCall_t2606____ContextProperties_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &ConstructionCall_get_ContextProperties_m12578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ConstructionCall_get_Properties_m12581_MethodInfo;
static PropertyInfo ConstructionCall_t2606____Properties_PropertyInfo = 
{
	&ConstructionCall_t2606_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ConstructionCall_get_Properties_m12581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ConstructionCall_t2606_PropertyInfos[] =
{
	&ConstructionCall_t2606____IsContextOk_PropertyInfo,
	&ConstructionCall_t2606____ActivationType_PropertyInfo,
	&ConstructionCall_t2606____ActivationTypeName_PropertyInfo,
	&ConstructionCall_t2606____Activator_PropertyInfo,
	&ConstructionCall_t2606____CallSiteActivationAttributes_PropertyInfo,
	&ConstructionCall_t2606____ContextProperties_PropertyInfo,
	&ConstructionCall_t2606____Properties_PropertyInfo,
	NULL
};
extern MethodInfo ConstructionCall_GetObjectData_m12580_MethodInfo;
extern MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo;
extern MethodInfo MethodCall_get_Args_m12602_MethodInfo;
extern MethodInfo MethodCall_get_LogicalCallContext_m12603_MethodInfo;
extern MethodInfo MethodCall_get_MethodBase_m12604_MethodInfo;
extern MethodInfo MethodCall_get_MethodName_m12605_MethodInfo;
extern MethodInfo MethodCall_get_MethodSignature_m12606_MethodInfo;
extern MethodInfo MethodCall_get_TypeName_m12609_MethodInfo;
extern MethodInfo MethodCall_get_Uri_m12610_MethodInfo;
extern MethodInfo ConstructionCall_InitMethodProperty_m12579_MethodInfo;
extern MethodInfo ConstructionCall_InitDictionary_m12570_MethodInfo;
extern MethodInfo MethodCall_set_Uri_m12611_MethodInfo;
extern MethodInfo MethodCall_Init_m12612_MethodInfo;
static Il2CppMethodReference ConstructionCall_t2606_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ConstructionCall_GetObjectData_m12580_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo,
	&MethodCall_get_Args_m12602_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12603_MethodInfo,
	&MethodCall_get_MethodBase_m12604_MethodInfo,
	&MethodCall_get_MethodName_m12605_MethodInfo,
	&MethodCall_get_MethodSignature_m12606_MethodInfo,
	&MethodCall_get_TypeName_m12609_MethodInfo,
	&MethodCall_get_Uri_m12610_MethodInfo,
	&ConstructionCall_InitMethodProperty_m12579_MethodInfo,
	&ConstructionCall_GetObjectData_m12580_MethodInfo,
	&ConstructionCall_get_Properties_m12581_MethodInfo,
	&ConstructionCall_InitDictionary_m12570_MethodInfo,
	&MethodCall_set_Uri_m12611_MethodInfo,
	&MethodCall_Init_m12612_MethodInfo,
	&ConstructionCall_get_ActivationType_m12572_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m12573_MethodInfo,
	&ConstructionCall_get_Activator_m12574_MethodInfo,
	&ConstructionCall_set_Activator_m12575_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m12576_MethodInfo,
	&ConstructionCall_get_ContextProperties_m12578_MethodInfo,
};
static bool ConstructionCall_t2606_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionCall_t2606_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t2884_0_0_0,
	&IMessage_t2604_0_0_0,
	&IMethodCallMessage_t2888_0_0_0,
	&IMethodMessage_t2616_0_0_0,
};
extern Il2CppType ISerializable_t310_0_0_0;
extern Il2CppType IInternalMessage_t2899_0_0_0;
extern Il2CppType ISerializationRootObject_t2985_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t2606_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &IInternalMessage_t2899_0_0_0, 5},
	{ &IMessage_t2604_0_0_0, 6},
	{ &IMethodCallMessage_t2888_0_0_0, 6},
	{ &IMethodMessage_t2616_0_0_0, 6},
	{ &ISerializationRootObject_t2985_0_0_0, 13},
	{ &IConstructionCallMessage_t2884_0_0_0, 19},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ConstructionCall_t2606_0_0_0;
extern Il2CppType ConstructionCall_t2606_1_0_0;
extern Il2CppType MethodCall_t2607_0_0_0;
struct ConstructionCall_t2606;
const Il2CppTypeDefinitionMetadata ConstructionCall_t2606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t2606_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t2606_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t2607_0_0_0/* parent */
	, ConstructionCall_t2606_VTable/* vtableMethods */
	, ConstructionCall_t2606_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ConstructionCall_t2606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCall_t2606_MethodInfos/* methods */
	, ConstructionCall_t2606_PropertyInfos/* properties */
	, ConstructionCall_t2606_FieldInfos/* fields */
	, NULL/* events */
	, &ConstructionCall_t2606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 519/* custom_attributes_cache */
	, &ConstructionCall_t2606_0_0_0/* byval_arg */
	, &ConstructionCall_t2606_1_0_0/* this_arg */
	, &ConstructionCall_t2606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t2606)/* instance_size */
	, sizeof (ConstructionCall_t2606)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t2606_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t2608_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo ConstructionCallDictionary_t2608_ConstructionCallDictionary__ctor_m12582_ParameterInfos[] = 
{
	{"message", 0, 134221944, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.ctor(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo ConstructionCallDictionary__ctor_m12582_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__ctor_m12582/* method */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2608_ConstructionCallDictionary__ctor_m12582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.cctor()
MethodInfo ConstructionCallDictionary__cctor_m12583_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__cctor_m12583/* method */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ConstructionCallDictionary_t2608_ConstructionCallDictionary_GetMethodProperty_m12584_ParameterInfos[] = 
{
	{"key", 0, 134221945, 0, &String_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ConstructionCallDictionary::GetMethodProperty(System.String)
MethodInfo ConstructionCallDictionary_GetMethodProperty_m12584_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_GetMethodProperty_m12584/* method */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2608_ConstructionCallDictionary_GetMethodProperty_m12584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ConstructionCallDictionary_t2608_ConstructionCallDictionary_SetMethodProperty_m12585_ParameterInfos[] = 
{
	{"key", 0, 134221946, 0, &String_t_0_0_0},
	{"value", 1, 134221947, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::SetMethodProperty(System.String,System.Object)
MethodInfo ConstructionCallDictionary_SetMethodProperty_m12585_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_SetMethodProperty_m12585/* method */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t2608_ConstructionCallDictionary_SetMethodProperty_m12585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ConstructionCallDictionary_t2608_MethodInfos[] =
{
	&ConstructionCallDictionary__ctor_m12582_MethodInfo,
	&ConstructionCallDictionary__cctor_m12583_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m12584_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m12585_MethodInfo,
	NULL
};
extern Il2CppType StringU5BU5D_t169_0_0_22;
FieldInfo ConstructionCallDictionary_t2608____InternalKeys_6_FieldInfo = 
{
	"InternalKeys"/* name */
	, &StringU5BU5D_t169_0_0_22/* type */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCallDictionary_t2608_StaticFields, ___InternalKeys_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo ConstructionCallDictionary_t2608____U3CU3Ef__switchU24map23_7_FieldInfo = 
{
	"<>f__switch$map23"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCallDictionary_t2608_StaticFields, ___U3CU3Ef__switchU24map23_7)/* offset */
	, 521/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo ConstructionCallDictionary_t2608____U3CU3Ef__switchU24map24_8_FieldInfo = 
{
	"<>f__switch$map24"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* parent */
	, offsetof(ConstructionCallDictionary_t2608_StaticFields, ___U3CU3Ef__switchU24map24_8)/* offset */
	, 522/* custom_attributes_cache */

};
static FieldInfo* ConstructionCallDictionary_t2608_FieldInfos[] =
{
	&ConstructionCallDictionary_t2608____InternalKeys_6_FieldInfo,
	&ConstructionCallDictionary_t2608____U3CU3Ef__switchU24map23_7_FieldInfo,
	&ConstructionCallDictionary_t2608____U3CU3Ef__switchU24map24_8_FieldInfo,
	NULL
};
extern MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo;
extern MethodInfo MethodDictionary_get_Count_m12639_MethodInfo;
extern MethodInfo MethodDictionary_get_IsSynchronized_m12640_MethodInfo;
extern MethodInfo MethodDictionary_get_SyncRoot_m12641_MethodInfo;
extern MethodInfo MethodDictionary_CopyTo_m12642_MethodInfo;
extern MethodInfo MethodDictionary_get_Item_m12631_MethodInfo;
extern MethodInfo MethodDictionary_set_Item_m12632_MethodInfo;
extern MethodInfo MethodDictionary_get_Keys_m12635_MethodInfo;
extern MethodInfo MethodDictionary_Add_m12637_MethodInfo;
extern MethodInfo MethodDictionary_GetEnumerator_m12643_MethodInfo;
extern MethodInfo MethodDictionary_Remove_m12638_MethodInfo;
extern MethodInfo MethodDictionary_AllocInternalProperties_m12628_MethodInfo;
extern MethodInfo ConstructionCallDictionary_GetMethodProperty_m12584_MethodInfo;
extern MethodInfo ConstructionCallDictionary_SetMethodProperty_m12585_MethodInfo;
extern MethodInfo MethodDictionary_get_Values_m12636_MethodInfo;
static Il2CppMethodReference ConstructionCallDictionary_t2608_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo,
	&MethodDictionary_get_Count_m12639_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12640_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12641_MethodInfo,
	&MethodDictionary_CopyTo_m12642_MethodInfo,
	&MethodDictionary_get_Item_m12631_MethodInfo,
	&MethodDictionary_set_Item_m12632_MethodInfo,
	&MethodDictionary_get_Keys_m12635_MethodInfo,
	&MethodDictionary_Add_m12637_MethodInfo,
	&MethodDictionary_GetEnumerator_m12643_MethodInfo,
	&MethodDictionary_Remove_m12638_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12628_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m12584_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m12585_MethodInfo,
	&MethodDictionary_get_Values_m12636_MethodInfo,
};
static bool ConstructionCallDictionary_t2608_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerable_t38_0_0_0;
extern Il2CppType ICollection_t1789_0_0_0;
extern Il2CppType IDictionary_t182_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t2608_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICollection_t1789_0_0_0, 5},
	{ &IDictionary_t182_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ConstructionCallDictionary_t2608_0_0_0;
extern Il2CppType ConstructionCallDictionary_t2608_1_0_0;
extern Il2CppType MethodDictionary_t2609_0_0_0;
struct ConstructionCallDictionary_t2608;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t2608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t2608_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2609_0_0_0/* parent */
	, ConstructionCallDictionary_t2608_VTable/* vtableMethods */
	, ConstructionCallDictionary_t2608_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ConstructionCallDictionary_t2608_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCallDictionary_t2608_MethodInfos/* methods */
	, NULL/* properties */
	, ConstructionCallDictionary_t2608_FieldInfos/* fields */
	, NULL/* events */
	, &ConstructionCallDictionary_t2608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t2608_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t2608_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t2608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t2608)/* instance_size */
	, sizeof (ConstructionCallDictionary_t2608)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t2608_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t2610_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.ctor()
MethodInfo EnvoyTerminatorSink__ctor_m12586_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__ctor_m12586/* method */
	, &EnvoyTerminatorSink_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.cctor()
MethodInfo EnvoyTerminatorSink__cctor_m12587_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__cctor_m12587/* method */
	, &EnvoyTerminatorSink_t2610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* EnvoyTerminatorSink_t2610_MethodInfos[] =
{
	&EnvoyTerminatorSink__ctor_m12586_MethodInfo,
	&EnvoyTerminatorSink__cctor_m12587_MethodInfo,
	NULL
};
extern Il2CppType EnvoyTerminatorSink_t2610_0_0_22;
FieldInfo EnvoyTerminatorSink_t2610____Instance_0_FieldInfo = 
{
	"Instance"/* name */
	, &EnvoyTerminatorSink_t2610_0_0_22/* type */
	, &EnvoyTerminatorSink_t2610_il2cpp_TypeInfo/* parent */
	, offsetof(EnvoyTerminatorSink_t2610_StaticFields, ___Instance_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* EnvoyTerminatorSink_t2610_FieldInfos[] =
{
	&EnvoyTerminatorSink_t2610____Instance_0_FieldInfo,
	NULL
};
static Il2CppMethodReference EnvoyTerminatorSink_t2610_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool EnvoyTerminatorSink_t2610_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* EnvoyTerminatorSink_t2610_InterfacesTypeInfos[] = 
{
	&IMessageSink_t2187_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t2610_InterfacesOffsets[] = 
{
	{ &IMessageSink_t2187_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EnvoyTerminatorSink_t2610_0_0_0;
extern Il2CppType EnvoyTerminatorSink_t2610_1_0_0;
struct EnvoyTerminatorSink_t2610;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t2610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t2610_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t2610_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t2610_VTable/* vtableMethods */
	, EnvoyTerminatorSink_t2610_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo EnvoyTerminatorSink_t2610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, EnvoyTerminatorSink_t2610_MethodInfos/* methods */
	, NULL/* properties */
	, EnvoyTerminatorSink_t2610_FieldInfos/* fields */
	, NULL/* events */
	, &EnvoyTerminatorSink_t2610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t2610_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t2610_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t2610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t2610)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t2610)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t2610_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t2611_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Header_t2611_Header__ctor_m12588_ParameterInfos[] = 
{
	{"_Name", 0, 134221948, 0, &String_t_0_0_0},
	{"_Value", 1, 134221949, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object)
MethodInfo Header__ctor_m12588_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12588/* method */
	, &Header_t2611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, Header_t2611_Header__ctor_m12588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Header_t2611_Header__ctor_m12589_ParameterInfos[] = 
{
	{"_Name", 0, 134221950, 0, &String_t_0_0_0},
	{"_Value", 1, 134221951, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134221952, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean)
MethodInfo Header__ctor_m12589_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12589/* method */
	, &Header_t2611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236/* invoker_method */
	, Header_t2611_Header__ctor_m12589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Header_t2611_Header__ctor_m12590_ParameterInfos[] = 
{
	{"_Name", 0, 134221953, 0, &String_t_0_0_0},
	{"_Value", 1, 134221954, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134221955, 0, &Boolean_t203_0_0_0},
	{"_HeaderNamespace", 3, 134221956, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean,System.String)
MethodInfo Header__ctor_m12590_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m12590/* method */
	, &Header_t2611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Object_t/* invoker_method */
	, Header_t2611_Header__ctor_m12590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Header_t2611_MethodInfos[] =
{
	&Header__ctor_m12588_MethodInfo,
	&Header__ctor_m12589_MethodInfo,
	&Header__ctor_m12590_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_6;
FieldInfo Header_t2611____HeaderNamespace_0_FieldInfo = 
{
	"HeaderNamespace"/* name */
	, &String_t_0_0_6/* type */
	, &Header_t2611_il2cpp_TypeInfo/* parent */
	, offsetof(Header_t2611, ___HeaderNamespace_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo Header_t2611____MustUnderstand_1_FieldInfo = 
{
	"MustUnderstand"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &Header_t2611_il2cpp_TypeInfo/* parent */
	, offsetof(Header_t2611, ___MustUnderstand_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo Header_t2611____Name_2_FieldInfo = 
{
	"Name"/* name */
	, &String_t_0_0_6/* type */
	, &Header_t2611_il2cpp_TypeInfo/* parent */
	, offsetof(Header_t2611, ___Name_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_6;
FieldInfo Header_t2611____Value_3_FieldInfo = 
{
	"Value"/* name */
	, &Object_t_0_0_6/* type */
	, &Header_t2611_il2cpp_TypeInfo/* parent */
	, offsetof(Header_t2611, ___Value_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Header_t2611_FieldInfos[] =
{
	&Header_t2611____HeaderNamespace_0_FieldInfo,
	&Header_t2611____MustUnderstand_1_FieldInfo,
	&Header_t2611____Name_2_FieldInfo,
	&Header_t2611____Value_3_FieldInfo,
	NULL
};
static Il2CppMethodReference Header_t2611_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool Header_t2611_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Header_t2611_0_0_0;
extern Il2CppType Header_t2611_1_0_0;
struct Header_t2611;
const Il2CppTypeDefinitionMetadata Header_t2611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t2611_VTable/* vtableMethods */
	, Header_t2611_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Header_t2611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, Header_t2611_MethodInfos/* methods */
	, NULL/* properties */
	, Header_t2611_FieldInfos/* fields */
	, NULL/* events */
	, &Header_t2611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 523/* custom_attributes_cache */
	, &Header_t2611_0_0_0/* byval_arg */
	, &Header_t2611_1_0_0/* this_arg */
	, &Header_t2611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t2611)/* instance_size */
	, sizeof (Header_t2611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t2899_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
static ParameterInfo IInternalMessage_t2899_IInternalMessage_set_Uri_m14968_ParameterInfos[] = 
{
	{"value", 0, 134221957, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.IInternalMessage::set_Uri(System.String)
MethodInfo IInternalMessage_set_Uri_m14968_MethodInfo = 
{
	"set_Uri"/* name */
	, NULL/* method */
	, &IInternalMessage_t2899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IInternalMessage_t2899_IInternalMessage_set_Uri_m14968_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IInternalMessage_t2899_MethodInfos[] =
{
	&IInternalMessage_set_Uri_m14968_MethodInfo,
	NULL
};
extern MethodInfo IInternalMessage_set_Uri_m14968_MethodInfo;
static PropertyInfo IInternalMessage_t2899____Uri_PropertyInfo = 
{
	&IInternalMessage_t2899_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, NULL/* get */
	, &IInternalMessage_set_Uri_m14968_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IInternalMessage_t2899_PropertyInfos[] =
{
	&IInternalMessage_t2899____Uri_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IInternalMessage_t2899_1_0_0;
struct IInternalMessage_t2899;
const Il2CppTypeDefinitionMetadata IInternalMessage_t2899_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IInternalMessage_t2899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IInternalMessage_t2899_MethodInfos/* methods */
	, IInternalMessage_t2899_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IInternalMessage_t2899_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t2899_0_0_0/* byval_arg */
	, &IInternalMessage_t2899_1_0_0/* this_arg */
	, &IInternalMessage_t2899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t2604_il2cpp_TypeInfo;
static MethodInfo* IMessage_t2604_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMessage_t2604_1_0_0;
struct IMessage_t2604;
const Il2CppTypeDefinitionMetadata IMessage_t2604_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMessage_t2604_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessage_t2604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMessage_t2604_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 524/* custom_attributes_cache */
	, &IMessage_t2604_0_0_0/* byval_arg */
	, &IMessage_t2604_1_0_0/* this_arg */
	, &IMessage_t2604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t2603_il2cpp_TypeInfo;
static MethodInfo* IMessageCtrl_t2603_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMessageCtrl_t2603_1_0_0;
struct IMessageCtrl_t2603;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t2603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMessageCtrl_t2603_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageCtrl_t2603_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMessageCtrl_t2603_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 525/* custom_attributes_cache */
	, &IMessageCtrl_t2603_0_0_0/* byval_arg */
	, &IMessageCtrl_t2603_1_0_0/* this_arg */
	, &IMessageCtrl_t2603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t2187_il2cpp_TypeInfo;
static MethodInfo* IMessageSink_t2187_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMessageSink_t2187_1_0_0;
struct IMessageSink_t2187;
const Il2CppTypeDefinitionMetadata IMessageSink_t2187_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMessageSink_t2187_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageSink_t2187_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMessageSink_t2187_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 526/* custom_attributes_cache */
	, &IMessageSink_t2187_0_0_0/* byval_arg */
	, &IMessageSink_t2187_1_0_0/* this_arg */
	, &IMessageSink_t2187_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t2888_il2cpp_TypeInfo;
static MethodInfo* IMethodCallMessage_t2888_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IMethodCallMessage_t2888_InterfacesTypeInfos[] = 
{
	&IMessage_t2604_0_0_0,
	&IMethodMessage_t2616_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMethodCallMessage_t2888_1_0_0;
struct IMethodCallMessage_t2888;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t2888_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t2888_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMethodCallMessage_t2888_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodCallMessage_t2888_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMethodCallMessage_t2888_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 527/* custom_attributes_cache */
	, &IMethodCallMessage_t2888_0_0_0/* byval_arg */
	, &IMethodCallMessage_t2888_1_0_0/* this_arg */
	, &IMethodCallMessage_t2888_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t2616_il2cpp_TypeInfo;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args()
MethodInfo IMethodMessage_get_Args_m14969_MethodInfo = 
{
	"get_Args"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.IMethodMessage::get_LogicalCallContext()
MethodInfo IMethodMessage_get_LogicalCallContext_m14970_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2613_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase()
MethodInfo IMethodMessage_get_MethodBase_m14971_MethodInfo = 
{
	"get_MethodBase"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodName()
MethodInfo IMethodMessage_get_MethodName_m14972_MethodInfo = 
{
	"get_MethodName"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodSignature()
MethodInfo IMethodMessage_get_MethodSignature_m14973_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_TypeName()
MethodInfo IMethodMessage_get_TypeName_m14974_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri()
MethodInfo IMethodMessage_get_Uri_m14975_MethodInfo = 
{
	"get_Uri"/* name */
	, NULL/* method */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IMethodMessage_t2616_MethodInfos[] =
{
	&IMethodMessage_get_Args_m14969_MethodInfo,
	&IMethodMessage_get_LogicalCallContext_m14970_MethodInfo,
	&IMethodMessage_get_MethodBase_m14971_MethodInfo,
	&IMethodMessage_get_MethodName_m14972_MethodInfo,
	&IMethodMessage_get_MethodSignature_m14973_MethodInfo,
	&IMethodMessage_get_TypeName_m14974_MethodInfo,
	&IMethodMessage_get_Uri_m14975_MethodInfo,
	NULL
};
extern MethodInfo IMethodMessage_get_Args_m14969_MethodInfo;
static PropertyInfo IMethodMessage_t2616____Args_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &IMethodMessage_get_Args_m14969_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_LogicalCallContext_m14970_MethodInfo;
static PropertyInfo IMethodMessage_t2616____LogicalCallContext_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &IMethodMessage_get_LogicalCallContext_m14970_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_MethodBase_m14971_MethodInfo;
static PropertyInfo IMethodMessage_t2616____MethodBase_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &IMethodMessage_get_MethodBase_m14971_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_MethodName_m14972_MethodInfo;
static PropertyInfo IMethodMessage_t2616____MethodName_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &IMethodMessage_get_MethodName_m14972_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_MethodSignature_m14973_MethodInfo;
static PropertyInfo IMethodMessage_t2616____MethodSignature_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &IMethodMessage_get_MethodSignature_m14973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_TypeName_m14974_MethodInfo;
static PropertyInfo IMethodMessage_t2616____TypeName_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IMethodMessage_get_TypeName_m14974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodMessage_get_Uri_m14975_MethodInfo;
static PropertyInfo IMethodMessage_t2616____Uri_PropertyInfo = 
{
	&IMethodMessage_t2616_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &IMethodMessage_get_Uri_m14975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IMethodMessage_t2616_PropertyInfos[] =
{
	&IMethodMessage_t2616____Args_PropertyInfo,
	&IMethodMessage_t2616____LogicalCallContext_PropertyInfo,
	&IMethodMessage_t2616____MethodBase_PropertyInfo,
	&IMethodMessage_t2616____MethodName_PropertyInfo,
	&IMethodMessage_t2616____MethodSignature_PropertyInfo,
	&IMethodMessage_t2616____TypeName_PropertyInfo,
	&IMethodMessage_t2616____Uri_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodMessage_t2616_InterfacesTypeInfos[] = 
{
	&IMessage_t2604_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMethodMessage_t2616_1_0_0;
struct IMethodMessage_t2616;
const Il2CppTypeDefinitionMetadata IMethodMessage_t2616_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t2616_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMethodMessage_t2616_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodMessage_t2616_MethodInfos/* methods */
	, IMethodMessage_t2616_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMethodMessage_t2616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 528/* custom_attributes_cache */
	, &IMethodMessage_t2616_0_0_0/* byval_arg */
	, &IMethodMessage_t2616_1_0_0/* this_arg */
	, &IMethodMessage_t2616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t2887_il2cpp_TypeInfo;
extern Il2CppType Exception_t135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception()
MethodInfo IMethodReturnMessage_get_Exception_m14976_MethodInfo = 
{
	"get_Exception"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_OutArgs()
MethodInfo IMethodReturnMessage_get_OutArgs_m14977_MethodInfo = 
{
	"get_OutArgs"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2887_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_ReturnValue()
MethodInfo IMethodReturnMessage_get_ReturnValue_m14978_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t2887_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IMethodReturnMessage_t2887_MethodInfos[] =
{
	&IMethodReturnMessage_get_Exception_m14976_MethodInfo,
	&IMethodReturnMessage_get_OutArgs_m14977_MethodInfo,
	&IMethodReturnMessage_get_ReturnValue_m14978_MethodInfo,
	NULL
};
extern MethodInfo IMethodReturnMessage_get_Exception_m14976_MethodInfo;
static PropertyInfo IMethodReturnMessage_t2887____Exception_PropertyInfo = 
{
	&IMethodReturnMessage_t2887_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &IMethodReturnMessage_get_Exception_m14976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodReturnMessage_get_OutArgs_m14977_MethodInfo;
static PropertyInfo IMethodReturnMessage_t2887____OutArgs_PropertyInfo = 
{
	&IMethodReturnMessage_t2887_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &IMethodReturnMessage_get_OutArgs_m14977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo IMethodReturnMessage_get_ReturnValue_m14978_MethodInfo;
static PropertyInfo IMethodReturnMessage_t2887____ReturnValue_PropertyInfo = 
{
	&IMethodReturnMessage_t2887_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &IMethodReturnMessage_get_ReturnValue_m14978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IMethodReturnMessage_t2887_PropertyInfos[] =
{
	&IMethodReturnMessage_t2887____Exception_PropertyInfo,
	&IMethodReturnMessage_t2887____OutArgs_PropertyInfo,
	&IMethodReturnMessage_t2887____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodReturnMessage_t2887_InterfacesTypeInfos[] = 
{
	&IMessage_t2604_0_0_0,
	&IMethodMessage_t2616_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IMethodReturnMessage_t2887_0_0_0;
extern Il2CppType IMethodReturnMessage_t2887_1_0_0;
struct IMethodReturnMessage_t2887;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t2887_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t2887_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IMethodReturnMessage_t2887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodReturnMessage_t2887_MethodInfos/* methods */
	, IMethodReturnMessage_t2887_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IMethodReturnMessage_t2887_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 529/* custom_attributes_cache */
	, &IMethodReturnMessage_t2887_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t2887_1_0_0/* this_arg */
	, &IMethodReturnMessage_t2887_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t2984_il2cpp_TypeInfo;
static MethodInfo* IRemotingFormatter_t2984_MethodInfos[] =
{
	NULL
};
extern Il2CppType IFormatter_t2986_0_0_0;
static const Il2CppType* IRemotingFormatter_t2984_InterfacesTypeInfos[] = 
{
	&IFormatter_t2986_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IRemotingFormatter_t2984_0_0_0;
extern Il2CppType IRemotingFormatter_t2984_1_0_0;
struct IRemotingFormatter_t2984;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t2984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t2984_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IRemotingFormatter_t2984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IRemotingFormatter_t2984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IRemotingFormatter_t2984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 530/* custom_attributes_cache */
	, &IRemotingFormatter_t2984_0_0_0/* byval_arg */
	, &IRemotingFormatter_t2984_1_0_0/* this_arg */
	, &IRemotingFormatter_t2984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t2985_il2cpp_TypeInfo;
static MethodInfo* ISerializationRootObject_t2985_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ISerializationRootObject_t2985_1_0_0;
struct ISerializationRootObject_t2985;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t2985_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ISerializationRootObject_t2985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ISerializationRootObject_t2985_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ISerializationRootObject_t2985_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t2985_0_0_0/* byval_arg */
	, &ISerializationRootObject_t2985_1_0_0/* this_arg */
	, &ISerializationRootObject_t2985_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t2613_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor()
MethodInfo LogicalCallContext__ctor_m12591_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m12591/* method */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo LogicalCallContext_t2613_LogicalCallContext__ctor_m12592_ParameterInfos[] = 
{
	{"info", 0, 134221958, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221959, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LogicalCallContext__ctor_m12592_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m12592/* method */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, LogicalCallContext_t2613_LogicalCallContext__ctor_m12592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo LogicalCallContext_t2613_LogicalCallContext_GetObjectData_m12593_ParameterInfos[] = 
{
	{"info", 0, 134221960, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221961, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LogicalCallContext_GetObjectData_m12593_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LogicalCallContext_GetObjectData_m12593/* method */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, LogicalCallContext_t2613_LogicalCallContext_GetObjectData_m12593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LogicalCallContext_t2613_LogicalCallContext_SetData_m12594_ParameterInfos[] = 
{
	{"name", 0, 134221962, 0, &String_t_0_0_0},
	{"data", 1, 134221963, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::SetData(System.String,System.Object)
MethodInfo LogicalCallContext_SetData_m12594_MethodInfo = 
{
	"SetData"/* name */
	, (methodPointerType)&LogicalCallContext_SetData_m12594/* method */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, LogicalCallContext_t2613_LogicalCallContext_SetData_m12594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* LogicalCallContext_t2613_MethodInfos[] =
{
	&LogicalCallContext__ctor_m12591_MethodInfo,
	&LogicalCallContext__ctor_m12592_MethodInfo,
	&LogicalCallContext_GetObjectData_m12593_MethodInfo,
	&LogicalCallContext_SetData_m12594_MethodInfo,
	NULL
};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo LogicalCallContext_t2613_____data_0_FieldInfo = 
{
	"_data"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* parent */
	, offsetof(LogicalCallContext_t2613, ____data_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType CallContextRemotingData_t2612_0_0_1;
FieldInfo LogicalCallContext_t2613_____remotingData_1_FieldInfo = 
{
	"_remotingData"/* name */
	, &CallContextRemotingData_t2612_0_0_1/* type */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* parent */
	, offsetof(LogicalCallContext_t2613, ____remotingData_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* LogicalCallContext_t2613_FieldInfos[] =
{
	&LogicalCallContext_t2613_____data_0_FieldInfo,
	&LogicalCallContext_t2613_____remotingData_1_FieldInfo,
	NULL
};
extern MethodInfo LogicalCallContext_GetObjectData_m12593_MethodInfo;
static Il2CppMethodReference LogicalCallContext_t2613_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&LogicalCallContext_GetObjectData_m12593_MethodInfo,
};
static bool LogicalCallContext_t2613_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICloneable_t309_0_0_0;
static const Il2CppType* LogicalCallContext_t2613_InterfacesTypeInfos[] = 
{
	&ICloneable_t309_0_0_0,
	&ISerializable_t310_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t2613_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern Il2CppType LogicalCallContext_t2613_1_0_0;
struct LogicalCallContext_t2613;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t2613_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t2613_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t2613_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t2613_VTable/* vtableMethods */
	, LogicalCallContext_t2613_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo LogicalCallContext_t2613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, LogicalCallContext_t2613_MethodInfos/* methods */
	, NULL/* properties */
	, LogicalCallContext_t2613_FieldInfos/* fields */
	, NULL/* events */
	, &LogicalCallContext_t2613_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 531/* custom_attributes_cache */
	, &LogicalCallContext_t2613_0_0_0/* byval_arg */
	, &LogicalCallContext_t2613_1_0_0/* this_arg */
	, &LogicalCallContext_t2613_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t2613)/* instance_size */
	, sizeof (LogicalCallContext_t2613)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t2612_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
MethodInfo CallContextRemotingData__ctor_m12595_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CallContextRemotingData__ctor_m12595/* method */
	, &CallContextRemotingData_t2612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CallContextRemotingData_t2612_MethodInfos[] =
{
	&CallContextRemotingData__ctor_m12595_MethodInfo,
	NULL
};
static Il2CppMethodReference CallContextRemotingData_t2612_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool CallContextRemotingData_t2612_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CallContextRemotingData_t2612_InterfacesTypeInfos[] = 
{
	&ICloneable_t309_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t2612_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType CallContextRemotingData_t2612_0_0_0;
extern Il2CppType CallContextRemotingData_t2612_1_0_0;
struct CallContextRemotingData_t2612;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t2612_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t2612_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t2612_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t2612_VTable/* vtableMethods */
	, CallContextRemotingData_t2612_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo CallContextRemotingData_t2612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, CallContextRemotingData_t2612_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CallContextRemotingData_t2612_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t2612_0_0_0/* byval_arg */
	, &CallContextRemotingData_t2612_1_0_0/* this_arg */
	, &CallContextRemotingData_t2612_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t2612)/* instance_size */
	, sizeof (CallContextRemotingData_t2612)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t2607_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
extern Il2CppType HeaderU5BU5D_t2850_0_0_0;
extern Il2CppType HeaderU5BU5D_t2850_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall__ctor_m12596_ParameterInfos[] = 
{
	{"h1", 0, 134221964, 0, &HeaderU5BU5D_t2850_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Remoting.Messaging.Header[])
MethodInfo MethodCall__ctor_m12596_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12596/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall__ctor_m12596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall__ctor_m12597_ParameterInfos[] = 
{
	{"info", 0, 134221965, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221966, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo MethodCall__ctor_m12597_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12597/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, MethodCall_t2607_MethodCall__ctor_m12597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor()
MethodInfo MethodCall__ctor_m12598_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m12598/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_ParameterInfos[] = 
{
	{"value", 0, 134221967, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_InitMethodProperty_m12600_ParameterInfos[] = 
{
	{"key", 0, 134221968, 0, &String_t_0_0_0},
	{"value", 1, 134221969, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitMethodProperty(System.String,System.Object)
MethodInfo MethodCall_InitMethodProperty_m12600_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&MethodCall_InitMethodProperty_m12600/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall_InitMethodProperty_m12600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_GetObjectData_m12601_ParameterInfos[] = 
{
	{"info", 0, 134221970, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134221971, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo MethodCall_GetObjectData_m12601_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MethodCall_GetObjectData_m12601/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, MethodCall_t2607_MethodCall_GetObjectData_m12601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MethodCall::get_Args()
MethodInfo MethodCall_get_Args_m12602_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MethodCall_get_Args_m12602/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodCall::get_LogicalCallContext()
MethodInfo MethodCall_get_LogicalCallContext_m12603_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MethodCall_get_LogicalCallContext_m12603/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2613_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodCall::get_MethodBase()
MethodInfo MethodCall_get_MethodBase_m12604_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MethodCall_get_MethodBase_m12604/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_MethodName()
MethodInfo MethodCall_get_MethodName_m12605_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MethodCall_get_MethodName_m12605/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodCall::get_MethodSignature()
MethodInfo MethodCall_get_MethodSignature_m12606_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MethodCall_get_MethodSignature_m12606/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCall::get_Properties()
MethodInfo MethodCall_get_Properties_m12607_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&MethodCall_get_Properties_m12607/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitDictionary()
MethodInfo MethodCall_InitDictionary_m12608_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&MethodCall_InitDictionary_m12608/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_TypeName()
MethodInfo MethodCall_get_TypeName_m12609_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MethodCall_get_TypeName_m12609/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_Uri()
MethodInfo MethodCall_get_Uri_m12610_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MethodCall_get_Uri_m12610/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_set_Uri_m12611_ParameterInfos[] = 
{
	{"value", 0, 134221972, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::set_Uri(System.String)
MethodInfo MethodCall_set_Uri_m12611_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MethodCall_set_Uri_m12611/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall_set_Uri_m12611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::Init()
MethodInfo MethodCall_Init_m12612_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&MethodCall_Init_m12612/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::ResolveMethod()
MethodInfo MethodCall_ResolveMethod_m12613_MethodInfo = 
{
	"ResolveMethod"/* name */
	, (methodPointerType)&MethodCall_ResolveMethod_m12613/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_CastTo_m12614_ParameterInfos[] = 
{
	{"clientType", 0, 134221973, 0, &String_t_0_0_0},
	{"serverType", 1, 134221974, 0, &Type_t_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.MethodCall::CastTo(System.String,System.Type)
MethodInfo MethodCall_CastTo_m12614_MethodInfo = 
{
	"CastTo"/* name */
	, (methodPointerType)&MethodCall_CastTo_m12614/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall_CastTo_m12614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MethodCall_t2607_MethodCall_GetTypeNameFromAssemblyQualifiedName_m12615_ParameterInfos[] = 
{
	{"aqname", 0, 134221975, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::GetTypeNameFromAssemblyQualifiedName(System.String)
MethodInfo MethodCall_GetTypeNameFromAssemblyQualifiedName_m12615_MethodInfo = 
{
	"GetTypeNameFromAssemblyQualifiedName"/* name */
	, (methodPointerType)&MethodCall_GetTypeNameFromAssemblyQualifiedName_m12615/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodCall_t2607_MethodCall_GetTypeNameFromAssemblyQualifiedName_m12615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type[] System.Runtime.Remoting.Messaging.MethodCall::get_GenericArguments()
MethodInfo MethodCall_get_GenericArguments_m12616_MethodInfo = 
{
	"get_GenericArguments"/* name */
	, (methodPointerType)&MethodCall_get_GenericArguments_m12616/* method */
	, &MethodCall_t2607_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t1671_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MethodCall_t2607_MethodInfos[] =
{
	&MethodCall__ctor_m12596_MethodInfo,
	&MethodCall__ctor_m12597_MethodInfo,
	&MethodCall__ctor_m12598_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo,
	&MethodCall_InitMethodProperty_m12600_MethodInfo,
	&MethodCall_GetObjectData_m12601_MethodInfo,
	&MethodCall_get_Args_m12602_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12603_MethodInfo,
	&MethodCall_get_MethodBase_m12604_MethodInfo,
	&MethodCall_get_MethodName_m12605_MethodInfo,
	&MethodCall_get_MethodSignature_m12606_MethodInfo,
	&MethodCall_get_Properties_m12607_MethodInfo,
	&MethodCall_InitDictionary_m12608_MethodInfo,
	&MethodCall_get_TypeName_m12609_MethodInfo,
	&MethodCall_get_Uri_m12610_MethodInfo,
	&MethodCall_set_Uri_m12611_MethodInfo,
	&MethodCall_Init_m12612_MethodInfo,
	&MethodCall_ResolveMethod_m12613_MethodInfo,
	&MethodCall_CastTo_m12614_MethodInfo,
	&MethodCall_GetTypeNameFromAssemblyQualifiedName_m12615_MethodInfo,
	&MethodCall_get_GenericArguments_m12616_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo MethodCall_t2607_____uri_0_FieldInfo = 
{
	"_uri"/* name */
	, &String_t_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____uri_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo MethodCall_t2607_____typeName_1_FieldInfo = 
{
	"_typeName"/* name */
	, &String_t_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____typeName_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo MethodCall_t2607_____methodName_2_FieldInfo = 
{
	"_methodName"/* name */
	, &String_t_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____methodName_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo MethodCall_t2607_____args_3_FieldInfo = 
{
	"_args"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____args_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_1;
FieldInfo MethodCall_t2607_____methodSignature_4_FieldInfo = 
{
	"_methodSignature"/* name */
	, &TypeU5BU5D_t1671_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____methodSignature_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodBase_t1691_0_0_1;
FieldInfo MethodCall_t2607_____methodBase_5_FieldInfo = 
{
	"_methodBase"/* name */
	, &MethodBase_t1691_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____methodBase_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType LogicalCallContext_t2613_0_0_1;
FieldInfo MethodCall_t2607_____callContext_6_FieldInfo = 
{
	"_callContext"/* name */
	, &LogicalCallContext_t2613_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____callContext_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_1;
FieldInfo MethodCall_t2607_____genericArguments_7_FieldInfo = 
{
	"_genericArguments"/* name */
	, &TypeU5BU5D_t1671_0_0_1/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ____genericArguments_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IDictionary_t182_0_0_4;
FieldInfo MethodCall_t2607____ExternalProperties_8_FieldInfo = 
{
	"ExternalProperties"/* name */
	, &IDictionary_t182_0_0_4/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ___ExternalProperties_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IDictionary_t182_0_0_4;
FieldInfo MethodCall_t2607____InternalProperties_9_FieldInfo = 
{
	"InternalProperties"/* name */
	, &IDictionary_t182_0_0_4/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607, ___InternalProperties_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo MethodCall_t2607____U3CU3Ef__switchU24map1F_10_FieldInfo = 
{
	"<>f__switch$map1F"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCall_t2607_StaticFields, ___U3CU3Ef__switchU24map1F_10)/* offset */
	, 533/* custom_attributes_cache */

};
static FieldInfo* MethodCall_t2607_FieldInfos[] =
{
	&MethodCall_t2607_____uri_0_FieldInfo,
	&MethodCall_t2607_____typeName_1_FieldInfo,
	&MethodCall_t2607_____methodName_2_FieldInfo,
	&MethodCall_t2607_____args_3_FieldInfo,
	&MethodCall_t2607_____methodSignature_4_FieldInfo,
	&MethodCall_t2607_____methodBase_5_FieldInfo,
	&MethodCall_t2607_____callContext_6_FieldInfo,
	&MethodCall_t2607_____genericArguments_7_FieldInfo,
	&MethodCall_t2607____ExternalProperties_8_FieldInfo,
	&MethodCall_t2607____InternalProperties_9_FieldInfo,
	&MethodCall_t2607____U3CU3Ef__switchU24map1F_10_FieldInfo,
	NULL
};
static PropertyInfo MethodCall_t2607____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____Args_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MethodCall_get_Args_m12602_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____LogicalCallContext_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MethodCall_get_LogicalCallContext_m12603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____MethodBase_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MethodCall_get_MethodBase_m12604_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____MethodName_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MethodCall_get_MethodName_m12605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____MethodSignature_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MethodCall_get_MethodSignature_m12606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MethodCall_get_Properties_m12607_MethodInfo;
static PropertyInfo MethodCall_t2607____Properties_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &MethodCall_get_Properties_m12607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____TypeName_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MethodCall_get_TypeName_m12609_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodCall_t2607____Uri_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MethodCall_get_Uri_m12610_MethodInfo/* get */
	, &MethodCall_set_Uri_m12611_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MethodCall_get_GenericArguments_m12616_MethodInfo;
static PropertyInfo MethodCall_t2607____GenericArguments_PropertyInfo = 
{
	&MethodCall_t2607_il2cpp_TypeInfo/* parent */
	, "GenericArguments"/* name */
	, &MethodCall_get_GenericArguments_m12616_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* MethodCall_t2607_PropertyInfos[] =
{
	&MethodCall_t2607____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&MethodCall_t2607____Args_PropertyInfo,
	&MethodCall_t2607____LogicalCallContext_PropertyInfo,
	&MethodCall_t2607____MethodBase_PropertyInfo,
	&MethodCall_t2607____MethodName_PropertyInfo,
	&MethodCall_t2607____MethodSignature_PropertyInfo,
	&MethodCall_t2607____Properties_PropertyInfo,
	&MethodCall_t2607____TypeName_PropertyInfo,
	&MethodCall_t2607____Uri_PropertyInfo,
	&MethodCall_t2607____GenericArguments_PropertyInfo,
	NULL
};
extern MethodInfo MethodCall_GetObjectData_m12601_MethodInfo;
extern MethodInfo MethodCall_InitMethodProperty_m12600_MethodInfo;
extern MethodInfo MethodCall_InitDictionary_m12608_MethodInfo;
static Il2CppMethodReference MethodCall_t2607_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MethodCall_GetObjectData_m12601_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12599_MethodInfo,
	&MethodCall_get_Args_m12602_MethodInfo,
	&MethodCall_get_LogicalCallContext_m12603_MethodInfo,
	&MethodCall_get_MethodBase_m12604_MethodInfo,
	&MethodCall_get_MethodName_m12605_MethodInfo,
	&MethodCall_get_MethodSignature_m12606_MethodInfo,
	&MethodCall_get_TypeName_m12609_MethodInfo,
	&MethodCall_get_Uri_m12610_MethodInfo,
	&MethodCall_InitMethodProperty_m12600_MethodInfo,
	&MethodCall_GetObjectData_m12601_MethodInfo,
	&MethodCall_get_Properties_m12607_MethodInfo,
	&MethodCall_InitDictionary_m12608_MethodInfo,
	&MethodCall_set_Uri_m12611_MethodInfo,
	&MethodCall_Init_m12612_MethodInfo,
};
static bool MethodCall_t2607_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodCall_t2607_InterfacesTypeInfos[] = 
{
	&ISerializable_t310_0_0_0,
	&IInternalMessage_t2899_0_0_0,
	&IMessage_t2604_0_0_0,
	&IMethodCallMessage_t2888_0_0_0,
	&IMethodMessage_t2616_0_0_0,
	&ISerializationRootObject_t2985_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t2607_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &IInternalMessage_t2899_0_0_0, 5},
	{ &IMessage_t2604_0_0_0, 6},
	{ &IMethodCallMessage_t2888_0_0_0, 6},
	{ &IMethodMessage_t2616_0_0_0, 6},
	{ &ISerializationRootObject_t2985_0_0_0, 13},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MethodCall_t2607_1_0_0;
struct MethodCall_t2607;
const Il2CppTypeDefinitionMetadata MethodCall_t2607_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t2607_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t2607_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t2607_VTable/* vtableMethods */
	, MethodCall_t2607_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MethodCall_t2607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCall_t2607_MethodInfos/* methods */
	, MethodCall_t2607_PropertyInfos/* properties */
	, MethodCall_t2607_FieldInfos/* fields */
	, NULL/* events */
	, &MethodCall_t2607_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 532/* custom_attributes_cache */
	, &MethodCall_t2607_0_0_0/* byval_arg */
	, &MethodCall_t2607_1_0_0/* this_arg */
	, &MethodCall_t2607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t2607)/* instance_size */
	, sizeof (MethodCall_t2607)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t2607_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t2614_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
extern Il2CppType IMethodMessage_t2616_0_0_0;
static ParameterInfo MethodCallDictionary_t2614_MethodCallDictionary__ctor_m12617_ParameterInfos[] = 
{
	{"message", 0, 134221976, 0, &IMethodMessage_t2616_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
MethodInfo MethodCallDictionary__ctor_m12617_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCallDictionary__ctor_m12617/* method */
	, &MethodCallDictionary_t2614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodCallDictionary_t2614_MethodCallDictionary__ctor_m12617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
MethodInfo MethodCallDictionary__cctor_m12618_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodCallDictionary__cctor_m12618/* method */
	, &MethodCallDictionary_t2614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MethodCallDictionary_t2614_MethodInfos[] =
{
	&MethodCallDictionary__ctor_m12617_MethodInfo,
	&MethodCallDictionary__cctor_m12618_MethodInfo,
	NULL
};
extern Il2CppType StringU5BU5D_t169_0_0_22;
FieldInfo MethodCallDictionary_t2614____InternalKeys_6_FieldInfo = 
{
	"InternalKeys"/* name */
	, &StringU5BU5D_t169_0_0_22/* type */
	, &MethodCallDictionary_t2614_il2cpp_TypeInfo/* parent */
	, offsetof(MethodCallDictionary_t2614_StaticFields, ___InternalKeys_6)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MethodCallDictionary_t2614_FieldInfos[] =
{
	&MethodCallDictionary_t2614____InternalKeys_6_FieldInfo,
	NULL
};
extern MethodInfo MethodDictionary_GetMethodProperty_m12633_MethodInfo;
extern MethodInfo MethodDictionary_SetMethodProperty_m12634_MethodInfo;
static Il2CppMethodReference MethodCallDictionary_t2614_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo,
	&MethodDictionary_get_Count_m12639_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12640_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12641_MethodInfo,
	&MethodDictionary_CopyTo_m12642_MethodInfo,
	&MethodDictionary_get_Item_m12631_MethodInfo,
	&MethodDictionary_set_Item_m12632_MethodInfo,
	&MethodDictionary_get_Keys_m12635_MethodInfo,
	&MethodDictionary_Add_m12637_MethodInfo,
	&MethodDictionary_GetEnumerator_m12643_MethodInfo,
	&MethodDictionary_Remove_m12638_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12628_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12633_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12634_MethodInfo,
	&MethodDictionary_get_Values_m12636_MethodInfo,
};
static bool MethodCallDictionary_t2614_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t2614_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICollection_t1789_0_0_0, 5},
	{ &IDictionary_t182_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MethodCallDictionary_t2614_0_0_0;
extern Il2CppType MethodCallDictionary_t2614_1_0_0;
struct MethodCallDictionary_t2614;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t2614_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t2614_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2609_0_0_0/* parent */
	, MethodCallDictionary_t2614_VTable/* vtableMethods */
	, MethodCallDictionary_t2614_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MethodCallDictionary_t2614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCallDictionary_t2614_MethodInfos/* methods */
	, NULL/* properties */
	, MethodCallDictionary_t2614_FieldInfos/* fields */
	, NULL/* events */
	, &MethodCallDictionary_t2614_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t2614_0_0_0/* byval_arg */
	, &MethodCallDictionary_t2614_1_0_0/* this_arg */
	, &MethodCallDictionary_t2614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t2614)/* instance_size */
	, sizeof (MethodCallDictionary_t2614)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t2614_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t2615_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
extern Il2CppType MethodDictionary_t2609_0_0_0;
static ParameterInfo DictionaryEnumerator_t2615_DictionaryEnumerator__ctor_m12619_ParameterInfos[] = 
{
	{"methodDictionary", 0, 134221991, 0, &MethodDictionary_t2609_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::.ctor(System.Runtime.Remoting.Messaging.MethodDictionary)
MethodInfo DictionaryEnumerator__ctor_m12619_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DictionaryEnumerator__ctor_m12619/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, DictionaryEnumerator_t2615_DictionaryEnumerator__ctor_m12619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Current()
MethodInfo DictionaryEnumerator_get_Current_m12620_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Current_m12620/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::MoveNext()
MethodInfo DictionaryEnumerator_MoveNext_m12621_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&DictionaryEnumerator_MoveNext_m12621/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType DictionaryEntry_t2128_0_0_0;
extern void* RuntimeInvoker_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args);
// System.Collections.DictionaryEntry System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Entry()
MethodInfo DictionaryEnumerator_get_Entry_m12622_MethodInfo = 
{
	"get_Entry"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Entry_m12622/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &DictionaryEntry_t2128_0_0_0/* return_type */
	, RuntimeInvoker_DictionaryEntry_t2128/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Key()
MethodInfo DictionaryEnumerator_get_Key_m12623_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Key_m12623/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Value()
MethodInfo DictionaryEnumerator_get_Value_m12624_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Value_m12624/* method */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DictionaryEnumerator_t2615_MethodInfos[] =
{
	&DictionaryEnumerator__ctor_m12619_MethodInfo,
	&DictionaryEnumerator_get_Current_m12620_MethodInfo,
	&DictionaryEnumerator_MoveNext_m12621_MethodInfo,
	&DictionaryEnumerator_get_Entry_m12622_MethodInfo,
	&DictionaryEnumerator_get_Key_m12623_MethodInfo,
	&DictionaryEnumerator_get_Value_m12624_MethodInfo,
	NULL
};
extern Il2CppType MethodDictionary_t2609_0_0_1;
FieldInfo DictionaryEnumerator_t2615_____methodDictionary_0_FieldInfo = 
{
	"_methodDictionary"/* name */
	, &MethodDictionary_t2609_0_0_1/* type */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, offsetof(DictionaryEnumerator_t2615, ____methodDictionary_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IDictionaryEnumerator_t1968_0_0_1;
FieldInfo DictionaryEnumerator_t2615_____hashtableEnum_1_FieldInfo = 
{
	"_hashtableEnum"/* name */
	, &IDictionaryEnumerator_t1968_0_0_1/* type */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, offsetof(DictionaryEnumerator_t2615, ____hashtableEnum_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo DictionaryEnumerator_t2615_____posMethod_2_FieldInfo = 
{
	"_posMethod"/* name */
	, &Int32_t189_0_0_1/* type */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, offsetof(DictionaryEnumerator_t2615, ____posMethod_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* DictionaryEnumerator_t2615_FieldInfos[] =
{
	&DictionaryEnumerator_t2615_____methodDictionary_0_FieldInfo,
	&DictionaryEnumerator_t2615_____hashtableEnum_1_FieldInfo,
	&DictionaryEnumerator_t2615_____posMethod_2_FieldInfo,
	NULL
};
extern MethodInfo DictionaryEnumerator_get_Current_m12620_MethodInfo;
static PropertyInfo DictionaryEnumerator_t2615____Current_PropertyInfo = 
{
	&DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &DictionaryEnumerator_get_Current_m12620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo DictionaryEnumerator_get_Entry_m12622_MethodInfo;
static PropertyInfo DictionaryEnumerator_t2615____Entry_PropertyInfo = 
{
	&DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, "Entry"/* name */
	, &DictionaryEnumerator_get_Entry_m12622_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo DictionaryEnumerator_get_Key_m12623_MethodInfo;
static PropertyInfo DictionaryEnumerator_t2615____Key_PropertyInfo = 
{
	&DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &DictionaryEnumerator_get_Key_m12623_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo DictionaryEnumerator_get_Value_m12624_MethodInfo;
static PropertyInfo DictionaryEnumerator_t2615____Value_PropertyInfo = 
{
	&DictionaryEnumerator_t2615_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DictionaryEnumerator_get_Value_m12624_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* DictionaryEnumerator_t2615_PropertyInfos[] =
{
	&DictionaryEnumerator_t2615____Current_PropertyInfo,
	&DictionaryEnumerator_t2615____Entry_PropertyInfo,
	&DictionaryEnumerator_t2615____Key_PropertyInfo,
	&DictionaryEnumerator_t2615____Value_PropertyInfo,
	NULL
};
extern MethodInfo DictionaryEnumerator_MoveNext_m12621_MethodInfo;
static Il2CppMethodReference DictionaryEnumerator_t2615_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&DictionaryEnumerator_get_Current_m12620_MethodInfo,
	&DictionaryEnumerator_MoveNext_m12621_MethodInfo,
	&DictionaryEnumerator_get_Entry_m12622_MethodInfo,
	&DictionaryEnumerator_get_Key_m12623_MethodInfo,
	&DictionaryEnumerator_get_Value_m12624_MethodInfo,
};
static bool DictionaryEnumerator_t2615_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern Il2CppType IDictionaryEnumerator_t1968_0_0_0;
static const Il2CppType* DictionaryEnumerator_t2615_InterfacesTypeInfos[] = 
{
	&IEnumerator_t37_0_0_0,
	&IDictionaryEnumerator_t1968_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t2615_InterfacesOffsets[] = 
{
	{ &IEnumerator_t37_0_0_0, 4},
	{ &IDictionaryEnumerator_t1968_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DictionaryEnumerator_t2615_0_0_0;
extern Il2CppType DictionaryEnumerator_t2615_1_0_0;
extern TypeInfo MethodDictionary_t2609_il2cpp_TypeInfo;
struct DictionaryEnumerator_t2615;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t2615_DefinitionMetadata = 
{
	&MethodDictionary_t2609_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t2615_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t2615_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t2615_VTable/* vtableMethods */
	, DictionaryEnumerator_t2615_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo DictionaryEnumerator_t2615_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, DictionaryEnumerator_t2615_MethodInfos/* methods */
	, DictionaryEnumerator_t2615_PropertyInfos/* properties */
	, DictionaryEnumerator_t2615_FieldInfos/* fields */
	, NULL/* events */
	, &DictionaryEnumerator_t2615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t2615_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t2615_1_0_0/* this_arg */
	, &DictionaryEnumerator_t2615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t2615)/* instance_size */
	, sizeof (DictionaryEnumerator_t2615)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern Il2CppType IMethodMessage_t2616_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary__ctor_m12625_ParameterInfos[] = 
{
	{"message", 0, 134221977, 0, &IMethodMessage_t2616_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
MethodInfo MethodDictionary__ctor_m12625_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodDictionary__ctor_m12625/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary__ctor_m12625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::System.Collections.IEnumerable.GetEnumerator()
MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StringU5BU5D_t169_0_0_0;
extern Il2CppType StringU5BU5D_t169_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_set_MethodKeys_m12627_ParameterInfos[] = 
{
	{"value", 0, 134221978, 0, &StringU5BU5D_t169_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_MethodKeys(System.String[])
MethodInfo MethodDictionary_set_MethodKeys_m12627_MethodInfo = 
{
	"set_MethodKeys"/* name */
	, (methodPointerType)&MethodDictionary_set_MethodKeys_m12627/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_set_MethodKeys_m12627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::AllocInternalProperties()
MethodInfo MethodDictionary_AllocInternalProperties_m12628_MethodInfo = 
{
	"AllocInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_AllocInternalProperties_m12628/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::GetInternalProperties()
MethodInfo MethodDictionary_GetInternalProperties_m12629_MethodInfo = 
{
	"GetInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_GetInternalProperties_m12629/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_IsOverridenKey_m12630_ParameterInfos[] = 
{
	{"key", 0, 134221979, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::IsOverridenKey(System.String)
MethodInfo MethodDictionary_IsOverridenKey_m12630_MethodInfo = 
{
	"IsOverridenKey"/* name */
	, (methodPointerType)&MethodDictionary_IsOverridenKey_m12630/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_IsOverridenKey_m12630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_get_Item_m12631_ParameterInfos[] = 
{
	{"key", 0, 134221980, 0, &Object_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_Item(System.Object)
MethodInfo MethodDictionary_get_Item_m12631_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&MethodDictionary_get_Item_m12631/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_get_Item_m12631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_set_Item_m12632_ParameterInfos[] = 
{
	{"key", 0, 134221981, 0, &Object_t_0_0_0},
	{"value", 1, 134221982, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_Item(System.Object,System.Object)
MethodInfo MethodDictionary_set_Item_m12632_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&MethodDictionary_set_Item_m12632/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_set_Item_m12632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_GetMethodProperty_m12633_ParameterInfos[] = 
{
	{"key", 0, 134221983, 0, &String_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::GetMethodProperty(System.String)
MethodInfo MethodDictionary_GetMethodProperty_m12633_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_GetMethodProperty_m12633/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_GetMethodProperty_m12633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_SetMethodProperty_m12634_ParameterInfos[] = 
{
	{"key", 0, 134221984, 0, &String_t_0_0_0},
	{"value", 1, 134221985, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::SetMethodProperty(System.String,System.Object)
MethodInfo MethodDictionary_SetMethodProperty_m12634_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_SetMethodProperty_m12634/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_SetMethodProperty_m12634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICollection_t1789_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.ICollection System.Runtime.Remoting.Messaging.MethodDictionary::get_Keys()
MethodInfo MethodDictionary_get_Keys_m12635_MethodInfo = 
{
	"get_Keys"/* name */
	, (methodPointerType)&MethodDictionary_get_Keys_m12635/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_t1789_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ICollection_t1789_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.ICollection System.Runtime.Remoting.Messaging.MethodDictionary::get_Values()
MethodInfo MethodDictionary_get_Values_m12636_MethodInfo = 
{
	"get_Values"/* name */
	, (methodPointerType)&MethodDictionary_get_Values_m12636/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_t1789_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_Add_m12637_ParameterInfos[] = 
{
	{"key", 0, 134221986, 0, &Object_t_0_0_0},
	{"value", 1, 134221987, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Add(System.Object,System.Object)
MethodInfo MethodDictionary_Add_m12637_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&MethodDictionary_Add_m12637/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_Add_m12637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_Remove_m12638_ParameterInfos[] = 
{
	{"key", 0, 134221988, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Remove(System.Object)
MethodInfo MethodDictionary_Remove_m12638_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&MethodDictionary_Remove_m12638/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_Remove_m12638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MethodDictionary::get_Count()
MethodInfo MethodDictionary_get_Count_m12639_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&MethodDictionary_get_Count_m12639/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::get_IsSynchronized()
MethodInfo MethodDictionary_get_IsSynchronized_m12640_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&MethodDictionary_get_IsSynchronized_m12640/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_SyncRoot()
MethodInfo MethodDictionary_get_SyncRoot_m12641_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&MethodDictionary_get_SyncRoot_m12641/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo MethodDictionary_t2609_MethodDictionary_CopyTo_m12642_ParameterInfos[] = 
{
	{"array", 0, 134221989, 0, &Array_t_0_0_0},
	{"index", 1, 134221990, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::CopyTo(System.Array,System.Int32)
MethodInfo MethodDictionary_CopyTo_m12642_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&MethodDictionary_CopyTo_m12642/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189/* invoker_method */
	, MethodDictionary_t2609_MethodDictionary_CopyTo_m12642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionaryEnumerator_t1968_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::GetEnumerator()
MethodInfo MethodDictionary_GetEnumerator_m12643_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_GetEnumerator_m12643/* method */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* declaring_type */
	, &IDictionaryEnumerator_t1968_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MethodDictionary_t2609_MethodInfos[] =
{
	&MethodDictionary__ctor_m12625_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo,
	&MethodDictionary_set_MethodKeys_m12627_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12628_MethodInfo,
	&MethodDictionary_GetInternalProperties_m12629_MethodInfo,
	&MethodDictionary_IsOverridenKey_m12630_MethodInfo,
	&MethodDictionary_get_Item_m12631_MethodInfo,
	&MethodDictionary_set_Item_m12632_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12633_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12634_MethodInfo,
	&MethodDictionary_get_Keys_m12635_MethodInfo,
	&MethodDictionary_get_Values_m12636_MethodInfo,
	&MethodDictionary_Add_m12637_MethodInfo,
	&MethodDictionary_Remove_m12638_MethodInfo,
	&MethodDictionary_get_Count_m12639_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12640_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12641_MethodInfo,
	&MethodDictionary_CopyTo_m12642_MethodInfo,
	&MethodDictionary_GetEnumerator_m12643_MethodInfo,
	NULL
};
extern Il2CppType IDictionary_t182_0_0_1;
FieldInfo MethodDictionary_t2609_____internalProperties_0_FieldInfo = 
{
	"_internalProperties"/* name */
	, &IDictionary_t182_0_0_1/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609, ____internalProperties_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMethodMessage_t2616_0_0_4;
FieldInfo MethodDictionary_t2609_____message_1_FieldInfo = 
{
	"_message"/* name */
	, &IMethodMessage_t2616_0_0_4/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609, ____message_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo MethodDictionary_t2609_____methodKeys_2_FieldInfo = 
{
	"_methodKeys"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609, ____methodKeys_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo MethodDictionary_t2609_____ownProperties_3_FieldInfo = 
{
	"_ownProperties"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609, ____ownProperties_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo MethodDictionary_t2609____U3CU3Ef__switchU24map21_4_FieldInfo = 
{
	"<>f__switch$map21"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609_StaticFields, ___U3CU3Ef__switchU24map21_4)/* offset */
	, 535/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo MethodDictionary_t2609____U3CU3Ef__switchU24map22_5_FieldInfo = 
{
	"<>f__switch$map22"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, offsetof(MethodDictionary_t2609_StaticFields, ___U3CU3Ef__switchU24map22_5)/* offset */
	, 536/* custom_attributes_cache */

};
static FieldInfo* MethodDictionary_t2609_FieldInfos[] =
{
	&MethodDictionary_t2609_____internalProperties_0_FieldInfo,
	&MethodDictionary_t2609_____message_1_FieldInfo,
	&MethodDictionary_t2609_____methodKeys_2_FieldInfo,
	&MethodDictionary_t2609_____ownProperties_3_FieldInfo,
	&MethodDictionary_t2609____U3CU3Ef__switchU24map21_4_FieldInfo,
	&MethodDictionary_t2609____U3CU3Ef__switchU24map22_5_FieldInfo,
	NULL
};
extern MethodInfo MethodDictionary_set_MethodKeys_m12627_MethodInfo;
static PropertyInfo MethodDictionary_t2609____MethodKeys_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "MethodKeys"/* name */
	, NULL/* get */
	, &MethodDictionary_set_MethodKeys_m12627_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____Item_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &MethodDictionary_get_Item_m12631_MethodInfo/* get */
	, &MethodDictionary_set_Item_m12632_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____Keys_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "Keys"/* name */
	, &MethodDictionary_get_Keys_m12635_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____Values_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &MethodDictionary_get_Values_m12636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____Count_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &MethodDictionary_get_Count_m12639_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____IsSynchronized_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &MethodDictionary_get_IsSynchronized_m12640_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo MethodDictionary_t2609____SyncRoot_PropertyInfo = 
{
	&MethodDictionary_t2609_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &MethodDictionary_get_SyncRoot_m12641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* MethodDictionary_t2609_PropertyInfos[] =
{
	&MethodDictionary_t2609____MethodKeys_PropertyInfo,
	&MethodDictionary_t2609____Item_PropertyInfo,
	&MethodDictionary_t2609____Keys_PropertyInfo,
	&MethodDictionary_t2609____Values_PropertyInfo,
	&MethodDictionary_t2609____Count_PropertyInfo,
	&MethodDictionary_t2609____IsSynchronized_PropertyInfo,
	&MethodDictionary_t2609____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* MethodDictionary_t2609_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t2615_0_0_0,
};
static Il2CppMethodReference MethodDictionary_t2609_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo,
	&MethodDictionary_get_Count_m12639_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12640_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12641_MethodInfo,
	&MethodDictionary_CopyTo_m12642_MethodInfo,
	&MethodDictionary_get_Item_m12631_MethodInfo,
	&MethodDictionary_set_Item_m12632_MethodInfo,
	&MethodDictionary_get_Keys_m12635_MethodInfo,
	&MethodDictionary_Add_m12637_MethodInfo,
	&MethodDictionary_GetEnumerator_m12643_MethodInfo,
	&MethodDictionary_Remove_m12638_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12628_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12633_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12634_MethodInfo,
	&MethodDictionary_get_Values_m12636_MethodInfo,
};
static bool MethodDictionary_t2609_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodDictionary_t2609_InterfacesTypeInfos[] = 
{
	&IEnumerable_t38_0_0_0,
	&ICollection_t1789_0_0_0,
	&IDictionary_t182_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t2609_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICollection_t1789_0_0_0, 5},
	{ &IDictionary_t182_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MethodDictionary_t2609_1_0_0;
struct MethodDictionary_t2609;
const Il2CppTypeDefinitionMetadata MethodDictionary_t2609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t2609_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t2609_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t2609_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t2609_VTable/* vtableMethods */
	, MethodDictionary_t2609_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MethodDictionary_t2609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodDictionary_t2609_MethodInfos/* methods */
	, MethodDictionary_t2609_PropertyInfos/* properties */
	, MethodDictionary_t2609_FieldInfos/* fields */
	, NULL/* events */
	, &MethodDictionary_t2609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 534/* custom_attributes_cache */
	, &MethodDictionary_t2609_0_0_0/* byval_arg */
	, &MethodDictionary_t2609_1_0_0/* this_arg */
	, &MethodDictionary_t2609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t2609)/* instance_size */
	, sizeof (MethodDictionary_t2609)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t2609_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t2617_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
extern Il2CppType IMethodReturnMessage_t2887_0_0_0;
static ParameterInfo MethodReturnDictionary_t2617_MethodReturnDictionary__ctor_m12644_ParameterInfos[] = 
{
	{"message", 0, 134221992, 0, &IMethodReturnMessage_t2887_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
MethodInfo MethodReturnDictionary__ctor_m12644_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__ctor_m12644/* method */
	, &MethodReturnDictionary_t2617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MethodReturnDictionary_t2617_MethodReturnDictionary__ctor_m12644_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
MethodInfo MethodReturnDictionary__cctor_m12645_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__cctor_m12645/* method */
	, &MethodReturnDictionary_t2617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MethodReturnDictionary_t2617_MethodInfos[] =
{
	&MethodReturnDictionary__ctor_m12644_MethodInfo,
	&MethodReturnDictionary__cctor_m12645_MethodInfo,
	NULL
};
extern Il2CppType StringU5BU5D_t169_0_0_22;
FieldInfo MethodReturnDictionary_t2617____InternalReturnKeys_6_FieldInfo = 
{
	"InternalReturnKeys"/* name */
	, &StringU5BU5D_t169_0_0_22/* type */
	, &MethodReturnDictionary_t2617_il2cpp_TypeInfo/* parent */
	, offsetof(MethodReturnDictionary_t2617_StaticFields, ___InternalReturnKeys_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_22;
FieldInfo MethodReturnDictionary_t2617____InternalExceptionKeys_7_FieldInfo = 
{
	"InternalExceptionKeys"/* name */
	, &StringU5BU5D_t169_0_0_22/* type */
	, &MethodReturnDictionary_t2617_il2cpp_TypeInfo/* parent */
	, offsetof(MethodReturnDictionary_t2617_StaticFields, ___InternalExceptionKeys_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MethodReturnDictionary_t2617_FieldInfos[] =
{
	&MethodReturnDictionary_t2617____InternalReturnKeys_6_FieldInfo,
	&MethodReturnDictionary_t2617____InternalExceptionKeys_7_FieldInfo,
	NULL
};
static Il2CppMethodReference MethodReturnDictionary_t2617_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m12626_MethodInfo,
	&MethodDictionary_get_Count_m12639_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m12640_MethodInfo,
	&MethodDictionary_get_SyncRoot_m12641_MethodInfo,
	&MethodDictionary_CopyTo_m12642_MethodInfo,
	&MethodDictionary_get_Item_m12631_MethodInfo,
	&MethodDictionary_set_Item_m12632_MethodInfo,
	&MethodDictionary_get_Keys_m12635_MethodInfo,
	&MethodDictionary_Add_m12637_MethodInfo,
	&MethodDictionary_GetEnumerator_m12643_MethodInfo,
	&MethodDictionary_Remove_m12638_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m12628_MethodInfo,
	&MethodDictionary_GetMethodProperty_m12633_MethodInfo,
	&MethodDictionary_SetMethodProperty_m12634_MethodInfo,
	&MethodDictionary_get_Values_m12636_MethodInfo,
};
static bool MethodReturnDictionary_t2617_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t2617_InterfacesOffsets[] = 
{
	{ &IEnumerable_t38_0_0_0, 4},
	{ &ICollection_t1789_0_0_0, 5},
	{ &IDictionary_t182_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MethodReturnDictionary_t2617_0_0_0;
extern Il2CppType MethodReturnDictionary_t2617_1_0_0;
struct MethodReturnDictionary_t2617;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t2617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t2617_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t2609_0_0_0/* parent */
	, MethodReturnDictionary_t2617_VTable/* vtableMethods */
	, MethodReturnDictionary_t2617_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MethodReturnDictionary_t2617_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodReturnDictionary_t2617_MethodInfos/* methods */
	, NULL/* properties */
	, MethodReturnDictionary_t2617_FieldInfos/* fields */
	, NULL/* events */
	, &MethodReturnDictionary_t2617_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t2617_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t2617_1_0_0/* this_arg */
	, &MethodReturnDictionary_t2617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t2617)/* instance_size */
	, sizeof (MethodReturnDictionary_t2617)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t2617_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t2602_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Args()
MethodInfo MonoMethodMessage_get_Args_m12646_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Args_m12646/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::get_LogicalCallContext()
MethodInfo MonoMethodMessage_get_LogicalCallContext_m12647_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MonoMethodMessage_get_LogicalCallContext_m12647/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2613_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodBase()
MethodInfo MonoMethodMessage_get_MethodBase_m12648_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodBase_m12648/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodName()
MethodInfo MonoMethodMessage_get_MethodName_m12649_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodName_m12649/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodSignature()
MethodInfo MonoMethodMessage_get_MethodSignature_m12650_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodSignature_m12650/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_TypeName()
MethodInfo MonoMethodMessage_get_TypeName_m12651_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_TypeName_m12651/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Uri()
MethodInfo MonoMethodMessage_get_Uri_m12652_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Uri_m12652/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo MonoMethodMessage_t2602_MonoMethodMessage_set_Uri_m12653_ParameterInfos[] = 
{
	{"value", 0, 134221993, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_Uri(System.String)
MethodInfo MonoMethodMessage_set_Uri_m12653_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_set_Uri_m12653/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MonoMethodMessage_t2602_MonoMethodMessage_set_Uri_m12653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Exception_t135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Exception()
MethodInfo MonoMethodMessage_get_Exception_m12654_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Exception_m12654/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgCount()
MethodInfo MonoMethodMessage_get_OutArgCount_m12655_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgCount_m12655/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgs()
MethodInfo MonoMethodMessage_get_OutArgs_m12656_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgs_m12656/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ReturnValue()
MethodInfo MonoMethodMessage_get_ReturnValue_m12657_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&MonoMethodMessage_get_ReturnValue_m12657/* method */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MonoMethodMessage_t2602_MethodInfos[] =
{
	&MonoMethodMessage_get_Args_m12646_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m12647_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m12648_MethodInfo,
	&MonoMethodMessage_get_MethodName_m12649_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m12650_MethodInfo,
	&MonoMethodMessage_get_TypeName_m12651_MethodInfo,
	&MonoMethodMessage_get_Uri_m12652_MethodInfo,
	&MonoMethodMessage_set_Uri_m12653_MethodInfo,
	&MonoMethodMessage_get_Exception_m12654_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m12655_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m12656_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m12657_MethodInfo,
	NULL
};
extern Il2CppType MonoMethod_t_0_0_1;
FieldInfo MonoMethodMessage_t2602____method_0_FieldInfo = 
{
	"method"/* name */
	, &MonoMethod_t_0_0_1/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___method_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo MonoMethodMessage_t2602____args_1_FieldInfo = 
{
	"args"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___args_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo MonoMethodMessage_t2602____arg_types_2_FieldInfo = 
{
	"arg_types"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___arg_types_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType LogicalCallContext_t2613_0_0_6;
FieldInfo MonoMethodMessage_t2602____ctx_3_FieldInfo = 
{
	"ctx"/* name */
	, &LogicalCallContext_t2613_0_0_6/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___ctx_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_6;
FieldInfo MonoMethodMessage_t2602____rval_4_FieldInfo = 
{
	"rval"/* name */
	, &Object_t_0_0_6/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___rval_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Exception_t135_0_0_6;
FieldInfo MonoMethodMessage_t2602____exc_5_FieldInfo = 
{
	"exc"/* name */
	, &Exception_t135_0_0_6/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___exc_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo MonoMethodMessage_t2602____uri_6_FieldInfo = 
{
	"uri"/* name */
	, &String_t_0_0_1/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___uri_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_1;
FieldInfo MonoMethodMessage_t2602____methodSignature_7_FieldInfo = 
{
	"methodSignature"/* name */
	, &TypeU5BU5D_t1671_0_0_1/* type */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, offsetof(MonoMethodMessage_t2602, ___methodSignature_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MonoMethodMessage_t2602_FieldInfos[] =
{
	&MonoMethodMessage_t2602____method_0_FieldInfo,
	&MonoMethodMessage_t2602____args_1_FieldInfo,
	&MonoMethodMessage_t2602____arg_types_2_FieldInfo,
	&MonoMethodMessage_t2602____ctx_3_FieldInfo,
	&MonoMethodMessage_t2602____rval_4_FieldInfo,
	&MonoMethodMessage_t2602____exc_5_FieldInfo,
	&MonoMethodMessage_t2602____uri_6_FieldInfo,
	&MonoMethodMessage_t2602____methodSignature_7_FieldInfo,
	NULL
};
extern MethodInfo MonoMethodMessage_get_Args_m12646_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____Args_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MonoMethodMessage_get_Args_m12646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_LogicalCallContext_m12647_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____LogicalCallContext_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MonoMethodMessage_get_LogicalCallContext_m12647_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_MethodBase_m12648_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____MethodBase_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MonoMethodMessage_get_MethodBase_m12648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_MethodName_m12649_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____MethodName_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MonoMethodMessage_get_MethodName_m12649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_MethodSignature_m12650_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____MethodSignature_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MonoMethodMessage_get_MethodSignature_m12650_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_TypeName_m12651_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____TypeName_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MonoMethodMessage_get_TypeName_m12651_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_Uri_m12652_MethodInfo;
extern MethodInfo MonoMethodMessage_set_Uri_m12653_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____Uri_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MonoMethodMessage_get_Uri_m12652_MethodInfo/* get */
	, &MonoMethodMessage_set_Uri_m12653_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_Exception_m12654_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____Exception_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &MonoMethodMessage_get_Exception_m12654_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_OutArgCount_m12655_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____OutArgCount_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &MonoMethodMessage_get_OutArgCount_m12655_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_OutArgs_m12656_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____OutArgs_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &MonoMethodMessage_get_OutArgs_m12656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo MonoMethodMessage_get_ReturnValue_m12657_MethodInfo;
static PropertyInfo MonoMethodMessage_t2602____ReturnValue_PropertyInfo = 
{
	&MonoMethodMessage_t2602_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &MonoMethodMessage_get_ReturnValue_m12657_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* MonoMethodMessage_t2602_PropertyInfos[] =
{
	&MonoMethodMessage_t2602____Args_PropertyInfo,
	&MonoMethodMessage_t2602____LogicalCallContext_PropertyInfo,
	&MonoMethodMessage_t2602____MethodBase_PropertyInfo,
	&MonoMethodMessage_t2602____MethodName_PropertyInfo,
	&MonoMethodMessage_t2602____MethodSignature_PropertyInfo,
	&MonoMethodMessage_t2602____TypeName_PropertyInfo,
	&MonoMethodMessage_t2602____Uri_PropertyInfo,
	&MonoMethodMessage_t2602____Exception_PropertyInfo,
	&MonoMethodMessage_t2602____OutArgCount_PropertyInfo,
	&MonoMethodMessage_t2602____OutArgs_PropertyInfo,
	&MonoMethodMessage_t2602____ReturnValue_PropertyInfo,
	NULL
};
static Il2CppMethodReference MonoMethodMessage_t2602_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MonoMethodMessage_set_Uri_m12653_MethodInfo,
	&MonoMethodMessage_get_Args_m12646_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m12647_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m12648_MethodInfo,
	&MonoMethodMessage_get_MethodName_m12649_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m12650_MethodInfo,
	&MonoMethodMessage_get_TypeName_m12651_MethodInfo,
	&MonoMethodMessage_get_Uri_m12652_MethodInfo,
	&MonoMethodMessage_get_Exception_m12654_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m12656_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m12657_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m12655_MethodInfo,
};
static bool MonoMethodMessage_t2602_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethodMessage_t2602_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2899_0_0_0,
	&IMessage_t2604_0_0_0,
	&IMethodCallMessage_t2888_0_0_0,
	&IMethodMessage_t2616_0_0_0,
	&IMethodReturnMessage_t2887_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t2602_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2899_0_0_0, 4},
	{ &IMessage_t2604_0_0_0, 5},
	{ &IMethodCallMessage_t2888_0_0_0, 5},
	{ &IMethodMessage_t2616_0_0_0, 5},
	{ &IMethodReturnMessage_t2887_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MonoMethodMessage_t2602_1_0_0;
struct MonoMethodMessage_t2602;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t2602_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t2602_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t2602_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t2602_VTable/* vtableMethods */
	, MonoMethodMessage_t2602_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MonoMethodMessage_t2602_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MonoMethodMessage_t2602_MethodInfos/* methods */
	, MonoMethodMessage_t2602_PropertyInfos/* properties */
	, MonoMethodMessage_t2602_FieldInfos/* fields */
	, NULL/* events */
	, &MonoMethodMessage_t2602_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t2602_0_0_0/* byval_arg */
	, &MonoMethodMessage_t2602_1_0_0/* this_arg */
	, &MonoMethodMessage_t2602_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t2602)/* instance_size */
	, sizeof (MonoMethodMessage_t2602)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 11/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t2618_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::.ctor()
MethodInfo RemotingSurrogate__ctor_m12658_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogate__ctor_m12658/* method */
	, &RemotingSurrogate_t2618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
static ParameterInfo RemotingSurrogate_t2618_RemotingSurrogate_SetObjectData_m12659_ParameterInfos[] = 
{
	{"obj", 0, 134221994, 0, &Object_t_0_0_0},
	{"si", 1, 134221995, 0, &SerializationInfo_t1673_0_0_0},
	{"sc", 2, 134221996, 0, &StreamingContext_t1674_0_0_0},
	{"selector", 3, 134221997, 0, &ISurrogateSelector_t2620_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
MethodInfo RemotingSurrogate_SetObjectData_m12659_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&RemotingSurrogate_SetObjectData_m12659/* method */
	, &RemotingSurrogate_t2618_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t/* invoker_method */
	, RemotingSurrogate_t2618_RemotingSurrogate_SetObjectData_m12659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingSurrogate_t2618_MethodInfos[] =
{
	&RemotingSurrogate__ctor_m12658_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12659_MethodInfo,
	NULL
};
extern MethodInfo RemotingSurrogate_SetObjectData_m12659_MethodInfo;
static Il2CppMethodReference RemotingSurrogate_t2618_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12659_MethodInfo,
	&RemotingSurrogate_SetObjectData_m12659_MethodInfo,
};
static bool RemotingSurrogate_t2618_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ISerializationSurrogate_t2671_0_0_0;
static const Il2CppType* RemotingSurrogate_t2618_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2671_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t2618_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2671_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingSurrogate_t2618_0_0_0;
extern Il2CppType RemotingSurrogate_t2618_1_0_0;
struct RemotingSurrogate_t2618;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t2618_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t2618_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t2618_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t2618_VTable/* vtableMethods */
	, RemotingSurrogate_t2618_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingSurrogate_t2618_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogate_t2618_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &RemotingSurrogate_t2618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t2618_0_0_0/* byval_arg */
	, &RemotingSurrogate_t2618_1_0_0/* this_arg */
	, &RemotingSurrogate_t2618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t2618)/* instance_size */
	, sizeof (RemotingSurrogate_t2618)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t2619_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::.ctor()
MethodInfo ObjRefSurrogate__ctor_m12660_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRefSurrogate__ctor_m12660/* method */
	, &ObjRefSurrogate_t2619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
static ParameterInfo ObjRefSurrogate_t2619_ObjRefSurrogate_SetObjectData_m12661_ParameterInfos[] = 
{
	{"obj", 0, 134221998, 0, &Object_t_0_0_0},
	{"si", 1, 134221999, 0, &SerializationInfo_t1673_0_0_0},
	{"sc", 2, 134222000, 0, &StreamingContext_t1674_0_0_0},
	{"selector", 3, 134222001, 0, &ISurrogateSelector_t2620_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ObjRefSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
MethodInfo ObjRefSurrogate_SetObjectData_m12661_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&ObjRefSurrogate_SetObjectData_m12661/* method */
	, &ObjRefSurrogate_t2619_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t/* invoker_method */
	, ObjRefSurrogate_t2619_ObjRefSurrogate_SetObjectData_m12661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObjRefSurrogate_t2619_MethodInfos[] =
{
	&ObjRefSurrogate__ctor_m12660_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12661_MethodInfo,
	NULL
};
extern MethodInfo ObjRefSurrogate_SetObjectData_m12661_MethodInfo;
static Il2CppMethodReference ObjRefSurrogate_t2619_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12661_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m12661_MethodInfo,
};
static bool ObjRefSurrogate_t2619_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ObjRefSurrogate_t2619_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t2671_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t2619_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t2671_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ObjRefSurrogate_t2619_0_0_0;
extern Il2CppType ObjRefSurrogate_t2619_1_0_0;
struct ObjRefSurrogate_t2619;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t2619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t2619_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t2619_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t2619_VTable/* vtableMethods */
	, ObjRefSurrogate_t2619_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ObjRefSurrogate_t2619_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ObjRefSurrogate_t2619_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ObjRefSurrogate_t2619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t2619_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t2619_1_0_0/* this_arg */
	, &ObjRefSurrogate_t2619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t2619)/* instance_size */
	, sizeof (ObjRefSurrogate_t2619)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t2621_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.ctor()
MethodInfo RemotingSurrogateSelector__ctor_m12662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__ctor_m12662/* method */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.cctor()
MethodInfo RemotingSurrogateSelector__cctor_m12663_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__cctor_m12663/* method */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_1_0_2;
extern Il2CppType ISurrogateSelector_t2620_1_0_0;
static ParameterInfo RemotingSurrogateSelector_t2621_RemotingSurrogateSelector_GetSurrogate_m12664_ParameterInfos[] = 
{
	{"type", 0, 134222002, 0, &Type_t_0_0_0},
	{"context", 1, 134222003, 0, &StreamingContext_t1674_0_0_0},
	{"ssout", 2, 134222004, 0, &ISurrogateSelector_t2620_1_0_2},
};
extern Il2CppType ISerializationSurrogate_t2671_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1674_ISurrogateSelectorU26_t3361 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
MethodInfo RemotingSurrogateSelector_GetSurrogate_m12664_MethodInfo = 
{
	"GetSurrogate"/* name */
	, (methodPointerType)&RemotingSurrogateSelector_GetSurrogate_m12664/* method */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t2671_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1674_ISurrogateSelectorU26_t3361/* invoker_method */
	, RemotingSurrogateSelector_t2621_RemotingSurrogateSelector_GetSurrogate_m12664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingSurrogateSelector_t2621_MethodInfos[] =
{
	&RemotingSurrogateSelector__ctor_m12662_MethodInfo,
	&RemotingSurrogateSelector__cctor_m12663_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12664_MethodInfo,
	NULL
};
extern Il2CppType Type_t_0_0_17;
FieldInfo RemotingSurrogateSelector_t2621____s_cachedTypeObjRef_0_FieldInfo = 
{
	"s_cachedTypeObjRef"/* name */
	, &Type_t_0_0_17/* type */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingSurrogateSelector_t2621_StaticFields, ___s_cachedTypeObjRef_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjRefSurrogate_t2619_0_0_17;
FieldInfo RemotingSurrogateSelector_t2621_____objRefSurrogate_1_FieldInfo = 
{
	"_objRefSurrogate"/* name */
	, &ObjRefSurrogate_t2619_0_0_17/* type */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingSurrogateSelector_t2621_StaticFields, ____objRefSurrogate_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType RemotingSurrogate_t2618_0_0_17;
FieldInfo RemotingSurrogateSelector_t2621_____objRemotingSurrogate_2_FieldInfo = 
{
	"_objRemotingSurrogate"/* name */
	, &RemotingSurrogate_t2618_0_0_17/* type */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingSurrogateSelector_t2621_StaticFields, ____objRemotingSurrogate_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_1;
FieldInfo RemotingSurrogateSelector_t2621_____next_3_FieldInfo = 
{
	"_next"/* name */
	, &ISurrogateSelector_t2620_0_0_1/* type */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingSurrogateSelector_t2621, ____next_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RemotingSurrogateSelector_t2621_FieldInfos[] =
{
	&RemotingSurrogateSelector_t2621____s_cachedTypeObjRef_0_FieldInfo,
	&RemotingSurrogateSelector_t2621_____objRefSurrogate_1_FieldInfo,
	&RemotingSurrogateSelector_t2621_____objRemotingSurrogate_2_FieldInfo,
	&RemotingSurrogateSelector_t2621_____next_3_FieldInfo,
	NULL
};
extern MethodInfo RemotingSurrogateSelector_GetSurrogate_m12664_MethodInfo;
static Il2CppMethodReference RemotingSurrogateSelector_t2621_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12664_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m12664_MethodInfo,
};
static bool RemotingSurrogateSelector_t2621_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemotingSurrogateSelector_t2621_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t2620_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t2621_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t2620_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingSurrogateSelector_t2621_0_0_0;
extern Il2CppType RemotingSurrogateSelector_t2621_1_0_0;
struct RemotingSurrogateSelector_t2621;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t2621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t2621_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t2621_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t2621_VTable/* vtableMethods */
	, RemotingSurrogateSelector_t2621_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingSurrogateSelector_t2621_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogateSelector_t2621_MethodInfos/* methods */
	, NULL/* properties */
	, RemotingSurrogateSelector_t2621_FieldInfos/* fields */
	, NULL/* events */
	, &RemotingSurrogateSelector_t2621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 537/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t2621_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t2621_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t2621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t2621)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t2621)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t2621_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t2622_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern Il2CppType IMethodCallMessage_t2888_0_0_0;
static ParameterInfo ReturnMessage_t2622_ReturnMessage__ctor_m12665_ParameterInfos[] = 
{
	{"ret", 0, 134222005, 0, &Object_t_0_0_0},
	{"outArgs", 1, 134222006, 0, &ObjectU5BU5D_t208_0_0_0},
	{"outArgsCount", 2, 134222007, 0, &Int32_t189_0_0_0},
	{"callCtx", 3, 134222008, 0, &LogicalCallContext_t2613_0_0_0},
	{"mcm", 4, 134222009, 0, &IMethodCallMessage_t2888_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Object,System.Object[],System.Int32,System.Runtime.Remoting.Messaging.LogicalCallContext,System.Runtime.Remoting.Messaging.IMethodCallMessage)
MethodInfo ReturnMessage__ctor_m12665_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m12665/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t2622_ReturnMessage__ctor_m12665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Exception_t135_0_0_0;
extern Il2CppType Exception_t135_0_0_0;
extern Il2CppType IMethodCallMessage_t2888_0_0_0;
static ParameterInfo ReturnMessage_t2622_ReturnMessage__ctor_m12666_ParameterInfos[] = 
{
	{"e", 0, 134222010, 0, &Exception_t135_0_0_0},
	{"mcm", 1, 134222011, 0, &IMethodCallMessage_t2888_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Exception,System.Runtime.Remoting.Messaging.IMethodCallMessage)
MethodInfo ReturnMessage__ctor_m12666_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m12666/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t2622_ReturnMessage__ctor_m12666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ReturnMessage_t2622_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_ParameterInfos[] = 
{
	{"value", 0, 134222012, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ReturnMessage_t2622_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_Args()
MethodInfo ReturnMessage_get_Args_m12668_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&ReturnMessage_get_Args_m12668/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LogicalCallContext_t2613_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.ReturnMessage::get_LogicalCallContext()
MethodInfo ReturnMessage_get_LogicalCallContext_m12669_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&ReturnMessage_get_LogicalCallContext_m12669/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t2613_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodBase()
MethodInfo ReturnMessage_get_MethodBase_m12670_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodBase_m12670/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodName()
MethodInfo ReturnMessage_get_MethodName_m12671_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodName_m12671/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodSignature()
MethodInfo ReturnMessage_get_MethodSignature_m12672_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodSignature_m12672/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IDictionary_t182_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ReturnMessage::get_Properties()
MethodInfo ReturnMessage_get_Properties_m12673_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ReturnMessage_get_Properties_m12673/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t182_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_TypeName()
MethodInfo ReturnMessage_get_TypeName_m12674_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&ReturnMessage_get_TypeName_m12674/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_Uri()
MethodInfo ReturnMessage_get_Uri_m12675_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&ReturnMessage_get_Uri_m12675/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ReturnMessage_t2622_ReturnMessage_set_Uri_m12676_ParameterInfos[] = 
{
	{"value", 0, 134222013, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::set_Uri(System.String)
MethodInfo ReturnMessage_set_Uri_m12676_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_set_Uri_m12676/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ReturnMessage_t2622_ReturnMessage_set_Uri_m12676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Exception_t135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.ReturnMessage::get_Exception()
MethodInfo ReturnMessage_get_Exception_m12677_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&ReturnMessage_get_Exception_m12677/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_OutArgs()
MethodInfo ReturnMessage_get_OutArgs_m12678_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&ReturnMessage_get_OutArgs_m12678/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_ReturnValue()
MethodInfo ReturnMessage_get_ReturnValue_m12679_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&ReturnMessage_get_ReturnValue_m12679/* method */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ReturnMessage_t2622_MethodInfos[] =
{
	&ReturnMessage__ctor_m12665_MethodInfo,
	&ReturnMessage__ctor_m12666_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_MethodInfo,
	&ReturnMessage_get_Args_m12668_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m12669_MethodInfo,
	&ReturnMessage_get_MethodBase_m12670_MethodInfo,
	&ReturnMessage_get_MethodName_m12671_MethodInfo,
	&ReturnMessage_get_MethodSignature_m12672_MethodInfo,
	&ReturnMessage_get_Properties_m12673_MethodInfo,
	&ReturnMessage_get_TypeName_m12674_MethodInfo,
	&ReturnMessage_get_Uri_m12675_MethodInfo,
	&ReturnMessage_set_Uri_m12676_MethodInfo,
	&ReturnMessage_get_Exception_m12677_MethodInfo,
	&ReturnMessage_get_OutArgs_m12678_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12679_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo ReturnMessage_t2622_____outArgs_0_FieldInfo = 
{
	"_outArgs"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____outArgs_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjectU5BU5D_t208_0_0_1;
FieldInfo ReturnMessage_t2622_____args_1_FieldInfo = 
{
	"_args"/* name */
	, &ObjectU5BU5D_t208_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____args_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ReturnMessage_t2622_____outArgsCount_2_FieldInfo = 
{
	"_outArgsCount"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____outArgsCount_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType LogicalCallContext_t2613_0_0_1;
FieldInfo ReturnMessage_t2622_____callCtx_3_FieldInfo = 
{
	"_callCtx"/* name */
	, &LogicalCallContext_t2613_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____callCtx_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo ReturnMessage_t2622_____returnValue_4_FieldInfo = 
{
	"_returnValue"/* name */
	, &Object_t_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____returnValue_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ReturnMessage_t2622_____uri_5_FieldInfo = 
{
	"_uri"/* name */
	, &String_t_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____uri_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Exception_t135_0_0_1;
FieldInfo ReturnMessage_t2622_____exception_6_FieldInfo = 
{
	"_exception"/* name */
	, &Exception_t135_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____exception_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodBase_t1691_0_0_1;
FieldInfo ReturnMessage_t2622_____methodBase_7_FieldInfo = 
{
	"_methodBase"/* name */
	, &MethodBase_t1691_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____methodBase_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ReturnMessage_t2622_____methodName_8_FieldInfo = 
{
	"_methodName"/* name */
	, &String_t_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____methodName_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_1;
FieldInfo ReturnMessage_t2622_____methodSignature_9_FieldInfo = 
{
	"_methodSignature"/* name */
	, &TypeU5BU5D_t1671_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____methodSignature_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ReturnMessage_t2622_____typeName_10_FieldInfo = 
{
	"_typeName"/* name */
	, &String_t_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____typeName_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodReturnDictionary_t2617_0_0_1;
FieldInfo ReturnMessage_t2622_____properties_11_FieldInfo = 
{
	"_properties"/* name */
	, &MethodReturnDictionary_t2617_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____properties_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArgInfo_t2600_0_0_1;
FieldInfo ReturnMessage_t2622_____inArgInfo_12_FieldInfo = 
{
	"_inArgInfo"/* name */
	, &ArgInfo_t2600_0_0_1/* type */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnMessage_t2622, ____inArgInfo_12)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ReturnMessage_t2622_FieldInfos[] =
{
	&ReturnMessage_t2622_____outArgs_0_FieldInfo,
	&ReturnMessage_t2622_____args_1_FieldInfo,
	&ReturnMessage_t2622_____outArgsCount_2_FieldInfo,
	&ReturnMessage_t2622_____callCtx_3_FieldInfo,
	&ReturnMessage_t2622_____returnValue_4_FieldInfo,
	&ReturnMessage_t2622_____uri_5_FieldInfo,
	&ReturnMessage_t2622_____exception_6_FieldInfo,
	&ReturnMessage_t2622_____methodBase_7_FieldInfo,
	&ReturnMessage_t2622_____methodName_8_FieldInfo,
	&ReturnMessage_t2622_____methodSignature_9_FieldInfo,
	&ReturnMessage_t2622_____typeName_10_FieldInfo,
	&ReturnMessage_t2622_____properties_11_FieldInfo,
	&ReturnMessage_t2622_____inArgInfo_12_FieldInfo,
	NULL
};
extern MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_MethodInfo;
static PropertyInfo ReturnMessage_t2622____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_Args_m12668_MethodInfo;
static PropertyInfo ReturnMessage_t2622____Args_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &ReturnMessage_get_Args_m12668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_LogicalCallContext_m12669_MethodInfo;
static PropertyInfo ReturnMessage_t2622____LogicalCallContext_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &ReturnMessage_get_LogicalCallContext_m12669_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_MethodBase_m12670_MethodInfo;
static PropertyInfo ReturnMessage_t2622____MethodBase_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &ReturnMessage_get_MethodBase_m12670_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_MethodName_m12671_MethodInfo;
static PropertyInfo ReturnMessage_t2622____MethodName_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &ReturnMessage_get_MethodName_m12671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_MethodSignature_m12672_MethodInfo;
static PropertyInfo ReturnMessage_t2622____MethodSignature_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &ReturnMessage_get_MethodSignature_m12672_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_Properties_m12673_MethodInfo;
static PropertyInfo ReturnMessage_t2622____Properties_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ReturnMessage_get_Properties_m12673_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_TypeName_m12674_MethodInfo;
static PropertyInfo ReturnMessage_t2622____TypeName_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &ReturnMessage_get_TypeName_m12674_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_Uri_m12675_MethodInfo;
extern MethodInfo ReturnMessage_set_Uri_m12676_MethodInfo;
static PropertyInfo ReturnMessage_t2622____Uri_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &ReturnMessage_get_Uri_m12675_MethodInfo/* get */
	, &ReturnMessage_set_Uri_m12676_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_Exception_m12677_MethodInfo;
static PropertyInfo ReturnMessage_t2622____Exception_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &ReturnMessage_get_Exception_m12677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_OutArgs_m12678_MethodInfo;
static PropertyInfo ReturnMessage_t2622____OutArgs_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &ReturnMessage_get_OutArgs_m12678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ReturnMessage_get_ReturnValue_m12679_MethodInfo;
static PropertyInfo ReturnMessage_t2622____ReturnValue_PropertyInfo = 
{
	&ReturnMessage_t2622_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &ReturnMessage_get_ReturnValue_m12679_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ReturnMessage_t2622_PropertyInfos[] =
{
	&ReturnMessage_t2622____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&ReturnMessage_t2622____Args_PropertyInfo,
	&ReturnMessage_t2622____LogicalCallContext_PropertyInfo,
	&ReturnMessage_t2622____MethodBase_PropertyInfo,
	&ReturnMessage_t2622____MethodName_PropertyInfo,
	&ReturnMessage_t2622____MethodSignature_PropertyInfo,
	&ReturnMessage_t2622____Properties_PropertyInfo,
	&ReturnMessage_t2622____TypeName_PropertyInfo,
	&ReturnMessage_t2622____Uri_PropertyInfo,
	&ReturnMessage_t2622____Exception_PropertyInfo,
	&ReturnMessage_t2622____OutArgs_PropertyInfo,
	&ReturnMessage_t2622____ReturnValue_PropertyInfo,
	NULL
};
static Il2CppMethodReference ReturnMessage_t2622_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m12667_MethodInfo,
	&ReturnMessage_get_Args_m12668_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m12669_MethodInfo,
	&ReturnMessage_get_MethodBase_m12670_MethodInfo,
	&ReturnMessage_get_MethodName_m12671_MethodInfo,
	&ReturnMessage_get_MethodSignature_m12672_MethodInfo,
	&ReturnMessage_get_TypeName_m12674_MethodInfo,
	&ReturnMessage_get_Uri_m12675_MethodInfo,
	&ReturnMessage_get_Exception_m12677_MethodInfo,
	&ReturnMessage_get_OutArgs_m12678_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12679_MethodInfo,
	&ReturnMessage_get_Properties_m12673_MethodInfo,
	&ReturnMessage_set_Uri_m12676_MethodInfo,
	&ReturnMessage_get_ReturnValue_m12679_MethodInfo,
};
static bool ReturnMessage_t2622_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ReturnMessage_t2622_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2899_0_0_0,
	&IMessage_t2604_0_0_0,
	&IMethodMessage_t2616_0_0_0,
	&IMethodReturnMessage_t2887_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t2622_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2899_0_0_0, 4},
	{ &IMessage_t2604_0_0_0, 5},
	{ &IMethodMessage_t2616_0_0_0, 5},
	{ &IMethodReturnMessage_t2887_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ReturnMessage_t2622_0_0_0;
extern Il2CppType ReturnMessage_t2622_1_0_0;
struct ReturnMessage_t2622;
const Il2CppTypeDefinitionMetadata ReturnMessage_t2622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t2622_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t2622_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t2622_VTable/* vtableMethods */
	, ReturnMessage_t2622_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ReturnMessage_t2622_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ReturnMessage_t2622_MethodInfos/* methods */
	, ReturnMessage_t2622_PropertyInfos/* properties */
	, ReturnMessage_t2622_FieldInfos/* fields */
	, NULL/* events */
	, &ReturnMessage_t2622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 538/* custom_attributes_cache */
	, &ReturnMessage_t2622_0_0_0/* byval_arg */
	, &ReturnMessage_t2622_1_0_0/* this_arg */
	, &ReturnMessage_t2622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t2622)/* instance_size */
	, sizeof (ReturnMessage_t2622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t2623_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ProxyAttribute_t2623_ProxyAttribute_CreateInstance_m12680_ParameterInfos[] = 
{
	{"serverType", 0, 134222014, 0, &Type_t_0_0_0},
};
extern Il2CppType MarshalByRefObject_t2011_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.ProxyAttribute::CreateInstance(System.Type)
MethodInfo ProxyAttribute_CreateInstance_m12680_MethodInfo = 
{
	"CreateInstance"/* name */
	, (methodPointerType)&ProxyAttribute_CreateInstance_m12680/* method */
	, &ProxyAttribute_t2623_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t2011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2623_ProxyAttribute_CreateInstance_m12680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Context_t2596_0_0_0;
static ParameterInfo ProxyAttribute_t2623_ProxyAttribute_CreateProxy_m12681_ParameterInfos[] = 
{
	{"objRef", 0, 134222015, 0, &ObjRef_t2632_0_0_0},
	{"serverType", 1, 134222016, 0, &Type_t_0_0_0},
	{"serverObject", 2, 134222017, 0, &Object_t_0_0_0},
	{"serverContext", 3, 134222018, 0, &Context_t2596_0_0_0},
};
extern Il2CppType RealProxy_t2624_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.ProxyAttribute::CreateProxy(System.Runtime.Remoting.ObjRef,System.Type,System.Object,System.Runtime.Remoting.Contexts.Context)
MethodInfo ProxyAttribute_CreateProxy_m12681_MethodInfo = 
{
	"CreateProxy"/* name */
	, (methodPointerType)&ProxyAttribute_CreateProxy_m12681/* method */
	, &ProxyAttribute_t2623_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2624_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2623_ProxyAttribute_CreateProxy_m12681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo ProxyAttribute_t2623_ProxyAttribute_GetPropertiesForNewContext_m12682_ParameterInfos[] = 
{
	{"msg", 0, 134222019, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.ProxyAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo ProxyAttribute_GetPropertiesForNewContext_m12682_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ProxyAttribute_GetPropertiesForNewContext_m12682/* method */
	, &ProxyAttribute_t2623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ProxyAttribute_t2623_ProxyAttribute_GetPropertiesForNewContext_m12682_ParameterInfos/* parameters */
	, 540/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType IConstructionCallMessage_t2884_0_0_0;
static ParameterInfo ProxyAttribute_t2623_ProxyAttribute_IsContextOK_m12683_ParameterInfos[] = 
{
	{"ctx", 0, 134222020, 0, &Context_t2596_0_0_0},
	{"msg", 1, 134222021, 0, &IConstructionCallMessage_t2884_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Proxies.ProxyAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
MethodInfo ProxyAttribute_IsContextOK_m12683_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ProxyAttribute_IsContextOK_m12683/* method */
	, &ProxyAttribute_t2623_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t2623_ProxyAttribute_IsContextOK_m12683_ParameterInfos/* parameters */
	, 541/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ProxyAttribute_t2623_MethodInfos[] =
{
	&ProxyAttribute_CreateInstance_m12680_MethodInfo,
	&ProxyAttribute_CreateProxy_m12681_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m12682_MethodInfo,
	&ProxyAttribute_IsContextOK_m12683_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m7644_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m7549_MethodInfo;
extern MethodInfo ProxyAttribute_GetPropertiesForNewContext_m12682_MethodInfo;
extern MethodInfo ProxyAttribute_IsContextOK_m12683_MethodInfo;
extern MethodInfo ProxyAttribute_CreateInstance_m12680_MethodInfo;
extern MethodInfo ProxyAttribute_CreateProxy_m12681_MethodInfo;
static Il2CppMethodReference ProxyAttribute_t2623_VTable[] =
{
	&Attribute_Equals_m7644_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Attribute_GetHashCode_m7549_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m12682_MethodInfo,
	&ProxyAttribute_IsContextOK_m12683_MethodInfo,
	&ProxyAttribute_CreateInstance_m12680_MethodInfo,
	&ProxyAttribute_CreateProxy_m12681_MethodInfo,
};
static bool ProxyAttribute_t2623_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ProxyAttribute_t2623_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2896_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t2623_InterfacesOffsets[] = 
{
	{ &_Attribute_t1731_0_0_0, 4},
	{ &IContextAttribute_t2896_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ProxyAttribute_t2623_0_0_0;
extern Il2CppType ProxyAttribute_t2623_1_0_0;
struct ProxyAttribute_t2623;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t2623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t2623_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t2623_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t1546_0_0_0/* parent */
	, ProxyAttribute_t2623_VTable/* vtableMethods */
	, ProxyAttribute_t2623_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ProxyAttribute_t2623_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, ProxyAttribute_t2623_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ProxyAttribute_t2623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 539/* custom_attributes_cache */
	, &ProxyAttribute_t2623_0_0_0/* byval_arg */
	, &ProxyAttribute_t2623_1_0_0/* this_arg */
	, &ProxyAttribute_t2623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t2623)/* instance_size */
	, sizeof (ProxyAttribute_t2623)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t2625_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static MethodInfo* TransparentProxy_t2625_MethodInfos[] =
{
	NULL
};
extern Il2CppType RealProxy_t2624_0_0_6;
FieldInfo TransparentProxy_t2625_____rp_0_FieldInfo = 
{
	"_rp"/* name */
	, &RealProxy_t2624_0_0_6/* type */
	, &TransparentProxy_t2625_il2cpp_TypeInfo/* parent */
	, offsetof(TransparentProxy_t2625, ____rp_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TransparentProxy_t2625_FieldInfos[] =
{
	&TransparentProxy_t2625_____rp_0_FieldInfo,
	NULL
};
static Il2CppMethodReference TransparentProxy_t2625_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TransparentProxy_t2625_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TransparentProxy_t2625_0_0_0;
extern Il2CppType TransparentProxy_t2625_1_0_0;
struct TransparentProxy_t2625;
const Il2CppTypeDefinitionMetadata TransparentProxy_t2625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t2625_VTable/* vtableMethods */
	, TransparentProxy_t2625_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TransparentProxy_t2625_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, TransparentProxy_t2625_MethodInfos/* methods */
	, NULL/* properties */
	, TransparentProxy_t2625_FieldInfos/* fields */
	, NULL/* events */
	, &TransparentProxy_t2625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t2625_0_0_0/* byval_arg */
	, &TransparentProxy_t2625_1_0_0/* this_arg */
	, &TransparentProxy_t2625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t2625)/* instance_size */
	, sizeof (TransparentProxy_t2625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t2624_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy__ctor_m12684_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222022, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type)
MethodInfo RealProxy__ctor_m12684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12684/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, RealProxy_t2624_RealProxy__ctor_m12684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType ClientIdentity_t2634_0_0_0;
extern Il2CppType ClientIdentity_t2634_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy__ctor_m12685_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222023, 0, &Type_t_0_0_0},
	{"identity", 1, 134222024, 0, &ClientIdentity_t2634_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
MethodInfo RealProxy__ctor_m12685_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12685/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, RealProxy_t2624_RealProxy__ctor_m12685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy__ctor_m12686_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222025, 0, &Type_t_0_0_0},
	{"stub", 1, 134222026, 0, &IntPtr_t_0_0_0},
	{"stubData", 2, 134222027, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.IntPtr,System.Object)
MethodInfo RealProxy__ctor_m12686_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m12686/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t_Object_t/* invoker_method */
	, RealProxy_t2624_RealProxy__ctor_m12686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy_InternalGetProxyType_m12687_ParameterInfos[] = 
{
	{"transparentProxy", 0, 134222028, 0, &Object_t_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)
MethodInfo RealProxy_InternalGetProxyType_m12687_MethodInfo = 
{
	"InternalGetProxyType"/* name */
	, (methodPointerType)&RealProxy_InternalGetProxyType_m12687/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2624_RealProxy_InternalGetProxyType_m12687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::GetProxiedType()
MethodInfo RealProxy_GetProxiedType_m12688_MethodInfo = 
{
	"GetProxiedType"/* name */
	, (methodPointerType)&RealProxy_GetProxiedType_m12688/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy_InternalGetTransparentProxy_m12689_ParameterInfos[] = 
{
	{"className", 0, 134222029, 0, &String_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)
MethodInfo RealProxy_InternalGetTransparentProxy_m12689_MethodInfo = 
{
	"InternalGetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_InternalGetTransparentProxy_m12689/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t2624_RealProxy_InternalGetTransparentProxy_m12689_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 4096/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy()
MethodInfo RealProxy_GetTransparentProxy_m12690_MethodInfo = 
{
	"GetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_GetTransparentProxy_m12690/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo RealProxy_t2624_RealProxy_SetTargetDomain_m12691_ParameterInfos[] = 
{
	{"domainId", 0, 134222030, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetTargetDomain(System.Int32)
MethodInfo RealProxy_SetTargetDomain_m12691_MethodInfo = 
{
	"SetTargetDomain"/* name */
	, (methodPointerType)&RealProxy_SetTargetDomain_m12691/* method */
	, &RealProxy_t2624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, RealProxy_t2624_RealProxy_SetTargetDomain_m12691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RealProxy_t2624_MethodInfos[] =
{
	&RealProxy__ctor_m12684_MethodInfo,
	&RealProxy__ctor_m12685_MethodInfo,
	&RealProxy__ctor_m12686_MethodInfo,
	&RealProxy_InternalGetProxyType_m12687_MethodInfo,
	&RealProxy_GetProxiedType_m12688_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12689_MethodInfo,
	&RealProxy_GetTransparentProxy_m12690_MethodInfo,
	&RealProxy_SetTargetDomain_m12691_MethodInfo,
	NULL
};
extern Il2CppType Type_t_0_0_1;
FieldInfo RealProxy_t2624____class_to_proxy_0_FieldInfo = 
{
	"class_to_proxy"/* name */
	, &Type_t_0_0_1/* type */
	, &RealProxy_t2624_il2cpp_TypeInfo/* parent */
	, offsetof(RealProxy_t2624, ___class_to_proxy_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo RealProxy_t2624_____targetDomainId_1_FieldInfo = 
{
	"_targetDomainId"/* name */
	, &Int32_t189_0_0_1/* type */
	, &RealProxy_t2624_il2cpp_TypeInfo/* parent */
	, offsetof(RealProxy_t2624, ____targetDomainId_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_3;
FieldInfo RealProxy_t2624_____targetUri_2_FieldInfo = 
{
	"_targetUri"/* name */
	, &String_t_0_0_3/* type */
	, &RealProxy_t2624_il2cpp_TypeInfo/* parent */
	, offsetof(RealProxy_t2624, ____targetUri_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Identity_t2626_0_0_3;
FieldInfo RealProxy_t2624_____objectIdentity_3_FieldInfo = 
{
	"_objectIdentity"/* name */
	, &Identity_t2626_0_0_3/* type */
	, &RealProxy_t2624_il2cpp_TypeInfo/* parent */
	, offsetof(RealProxy_t2624, ____objectIdentity_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo RealProxy_t2624_____objTP_4_FieldInfo = 
{
	"_objTP"/* name */
	, &Object_t_0_0_1/* type */
	, &RealProxy_t2624_il2cpp_TypeInfo/* parent */
	, offsetof(RealProxy_t2624, ____objTP_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RealProxy_t2624_FieldInfos[] =
{
	&RealProxy_t2624____class_to_proxy_0_FieldInfo,
	&RealProxy_t2624_____targetDomainId_1_FieldInfo,
	&RealProxy_t2624_____targetUri_2_FieldInfo,
	&RealProxy_t2624_____objectIdentity_3_FieldInfo,
	&RealProxy_t2624_____objTP_4_FieldInfo,
	NULL
};
extern MethodInfo RealProxy_InternalGetTransparentProxy_m12689_MethodInfo;
extern MethodInfo RealProxy_GetTransparentProxy_m12690_MethodInfo;
static Il2CppMethodReference RealProxy_t2624_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12689_MethodInfo,
	&RealProxy_GetTransparentProxy_m12690_MethodInfo,
};
static bool RealProxy_t2624_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RealProxy_t2624_0_0_0;
extern Il2CppType RealProxy_t2624_1_0_0;
struct RealProxy_t2624;
const Il2CppTypeDefinitionMetadata RealProxy_t2624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t2624_VTable/* vtableMethods */
	, RealProxy_t2624_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RealProxy_t2624_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RealProxy_t2624_MethodInfos/* methods */
	, NULL/* properties */
	, RealProxy_t2624_FieldInfos/* fields */
	, NULL/* events */
	, &RealProxy_t2624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 542/* custom_attributes_cache */
	, &RealProxy_t2624_0_0_0/* byval_arg */
	, &RealProxy_t2624_1_0_0/* this_arg */
	, &RealProxy_t2624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t2624)/* instance_size */
	, sizeof (RealProxy_t2624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t2627_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
extern Il2CppType ClientIdentity_t2634_0_0_0;
static ParameterInfo RemotingProxy_t2627_RemotingProxy__ctor_m12692_ParameterInfos[] = 
{
	{"type", 0, 134222031, 0, &Type_t_0_0_0},
	{"identity", 1, 134222032, 0, &ClientIdentity_t2634_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
MethodInfo RemotingProxy__ctor_m12692_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m12692/* method */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2627_RemotingProxy__ctor_m12692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo RemotingProxy_t2627_RemotingProxy__ctor_m12693_ParameterInfos[] = 
{
	{"type", 0, 134222033, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134222034, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134222035, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
MethodInfo RemotingProxy__ctor_m12693_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m12693/* method */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t2627_RemotingProxy__ctor_m12693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
MethodInfo RemotingProxy__cctor_m12694_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingProxy__cctor_m12694/* method */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
MethodInfo RemotingProxy_get_TypeName_m12695_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&RemotingProxy_get_TypeName_m12695/* method */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
MethodInfo RemotingProxy_Finalize_m12696_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&RemotingProxy_Finalize_m12696/* method */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingProxy_t2627_MethodInfos[] =
{
	&RemotingProxy__ctor_m12692_MethodInfo,
	&RemotingProxy__ctor_m12693_MethodInfo,
	&RemotingProxy__cctor_m12694_MethodInfo,
	&RemotingProxy_get_TypeName_m12695_MethodInfo,
	&RemotingProxy_Finalize_m12696_MethodInfo,
	NULL
};
extern Il2CppType MethodInfo_t_0_0_17;
FieldInfo RemotingProxy_t2627_____cache_GetTypeMethod_5_FieldInfo = 
{
	"_cache_GetTypeMethod"/* name */
	, &MethodInfo_t_0_0_17/* type */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingProxy_t2627_StaticFields, ____cache_GetTypeMethod_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodInfo_t_0_0_17;
FieldInfo RemotingProxy_t2627_____cache_GetHashCodeMethod_6_FieldInfo = 
{
	"_cache_GetHashCodeMethod"/* name */
	, &MethodInfo_t_0_0_17/* type */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingProxy_t2627_StaticFields, ____cache_GetHashCodeMethod_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMessageSink_t2187_0_0_1;
FieldInfo RemotingProxy_t2627_____sink_7_FieldInfo = 
{
	"_sink"/* name */
	, &IMessageSink_t2187_0_0_1/* type */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingProxy_t2627, ____sink_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo RemotingProxy_t2627_____hasEnvoySink_8_FieldInfo = 
{
	"_hasEnvoySink"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingProxy_t2627, ____hasEnvoySink_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ConstructionCall_t2606_0_0_1;
FieldInfo RemotingProxy_t2627_____ctorCall_9_FieldInfo = 
{
	"_ctorCall"/* name */
	, &ConstructionCall_t2606_0_0_1/* type */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingProxy_t2627, ____ctorCall_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RemotingProxy_t2627_FieldInfos[] =
{
	&RemotingProxy_t2627_____cache_GetTypeMethod_5_FieldInfo,
	&RemotingProxy_t2627_____cache_GetHashCodeMethod_6_FieldInfo,
	&RemotingProxy_t2627_____sink_7_FieldInfo,
	&RemotingProxy_t2627_____hasEnvoySink_8_FieldInfo,
	&RemotingProxy_t2627_____ctorCall_9_FieldInfo,
	NULL
};
extern MethodInfo RemotingProxy_get_TypeName_m12695_MethodInfo;
static PropertyInfo RemotingProxy_t2627____TypeName_PropertyInfo = 
{
	&RemotingProxy_t2627_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &RemotingProxy_get_TypeName_m12695_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* RemotingProxy_t2627_PropertyInfos[] =
{
	&RemotingProxy_t2627____TypeName_PropertyInfo,
	NULL
};
extern MethodInfo RemotingProxy_Finalize_m12696_MethodInfo;
static Il2CppMethodReference RemotingProxy_t2627_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&RemotingProxy_Finalize_m12696_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m12689_MethodInfo,
	&RealProxy_GetTransparentProxy_m12690_MethodInfo,
	&RemotingProxy_get_TypeName_m12695_MethodInfo,
};
static bool RemotingProxy_t2627_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IRemotingTypeInfo_t2636_0_0_0;
static const Il2CppType* RemotingProxy_t2627_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2636_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t2627_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2636_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingProxy_t2627_0_0_0;
extern Il2CppType RemotingProxy_t2627_1_0_0;
struct RemotingProxy_t2627;
const Il2CppTypeDefinitionMetadata RemotingProxy_t2627_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t2627_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t2627_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t2624_0_0_0/* parent */
	, RemotingProxy_t2627_VTable/* vtableMethods */
	, RemotingProxy_t2627_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingProxy_t2627_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RemotingProxy_t2627_MethodInfos/* methods */
	, RemotingProxy_t2627_PropertyInfos/* properties */
	, RemotingProxy_t2627_FieldInfos/* fields */
	, NULL/* events */
	, &RemotingProxy_t2627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t2627_0_0_0/* byval_arg */
	, &RemotingProxy_t2627_1_0_0/* this_arg */
	, &RemotingProxy_t2627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t2627)/* instance_size */
	, sizeof (RemotingProxy_t2627)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t2627_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t2901_il2cpp_TypeInfo;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType ObjRef_t2632_0_0_0;
static ParameterInfo ITrackingHandler_t2901_ITrackingHandler_UnmarshaledObject_m14979_ParameterInfos[] = 
{
	{"obj", 0, 134222036, 0, &Object_t_0_0_0},
	{"or", 1, 134222037, 0, &ObjRef_t2632_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::UnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
MethodInfo ITrackingHandler_UnmarshaledObject_m14979_MethodInfo = 
{
	"UnmarshaledObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t2901_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ITrackingHandler_t2901_ITrackingHandler_UnmarshaledObject_m14979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ITrackingHandler_t2901_MethodInfos[] =
{
	&ITrackingHandler_UnmarshaledObject_m14979_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ITrackingHandler_t2901_0_0_0;
extern Il2CppType ITrackingHandler_t2901_1_0_0;
struct ITrackingHandler_t2901;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t2901_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ITrackingHandler_t2901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, ITrackingHandler_t2901_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ITrackingHandler_t2901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 543/* custom_attributes_cache */
	, &ITrackingHandler_t2901_0_0_0/* byval_arg */
	, &ITrackingHandler_t2901_1_0_0/* this_arg */
	, &ITrackingHandler_t2901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t2628_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
MethodInfo TrackingServices__cctor_m12697_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TrackingServices__cctor_m12697/* method */
	, &TrackingServices_t2628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType ObjRef_t2632_0_0_0;
static ParameterInfo TrackingServices_t2628_TrackingServices_NotifyUnmarshaledObject_m12698_ParameterInfos[] = 
{
	{"obj", 0, 134222038, 0, &Object_t_0_0_0},
	{"or", 1, 134222039, 0, &ObjRef_t2632_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
MethodInfo TrackingServices_NotifyUnmarshaledObject_m12698_MethodInfo = 
{
	"NotifyUnmarshaledObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyUnmarshaledObject_m12698/* method */
	, &TrackingServices_t2628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, TrackingServices_t2628_TrackingServices_NotifyUnmarshaledObject_m12698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TrackingServices_t2628_MethodInfos[] =
{
	&TrackingServices__cctor_m12697_MethodInfo,
	&TrackingServices_NotifyUnmarshaledObject_m12698_MethodInfo,
	NULL
};
extern Il2CppType ArrayList_t737_0_0_17;
FieldInfo TrackingServices_t2628_____handlers_0_FieldInfo = 
{
	"_handlers"/* name */
	, &ArrayList_t737_0_0_17/* type */
	, &TrackingServices_t2628_il2cpp_TypeInfo/* parent */
	, offsetof(TrackingServices_t2628_StaticFields, ____handlers_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TrackingServices_t2628_FieldInfos[] =
{
	&TrackingServices_t2628_____handlers_0_FieldInfo,
	NULL
};
static Il2CppMethodReference TrackingServices_t2628_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TrackingServices_t2628_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TrackingServices_t2628_0_0_0;
extern Il2CppType TrackingServices_t2628_1_0_0;
struct TrackingServices_t2628;
const Il2CppTypeDefinitionMetadata TrackingServices_t2628_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t2628_VTable/* vtableMethods */
	, TrackingServices_t2628_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TrackingServices_t2628_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, TrackingServices_t2628_MethodInfos/* methods */
	, NULL/* properties */
	, TrackingServices_t2628_FieldInfos/* fields */
	, NULL/* events */
	, &TrackingServices_t2628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 544/* custom_attributes_cache */
	, &TrackingServices_t2628_0_0_0/* byval_arg */
	, &TrackingServices_t2628_1_0_0/* this_arg */
	, &TrackingServices_t2628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t2628)/* instance_size */
	, sizeof (TrackingServices_t2628)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t2628_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::get_ApplicationUrl()
MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m12699_MethodInfo = 
{
	"get_ApplicationUrl"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ApplicationUrl_m12699/* method */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IContextAttributeU5BU5D_t2889_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedClientTypeEntry::get_ContextAttributes()
MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m12700_MethodInfo = 
{
	"get_ContextAttributes"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ContextAttributes_m12700/* method */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttributeU5BU5D_t2889_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ActivatedClientTypeEntry::get_ObjectType()
MethodInfo ActivatedClientTypeEntry_get_ObjectType_m12701_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ObjectType_m12701/* method */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::ToString()
MethodInfo ActivatedClientTypeEntry_ToString_m12702_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_ToString_m12702/* method */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ActivatedClientTypeEntry_t2629_MethodInfos[] =
{
	&ActivatedClientTypeEntry_get_ApplicationUrl_m12699_MethodInfo,
	&ActivatedClientTypeEntry_get_ContextAttributes_m12700_MethodInfo,
	&ActivatedClientTypeEntry_get_ObjectType_m12701_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m12702_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo ActivatedClientTypeEntry_t2629____applicationUrl_2_FieldInfo = 
{
	"applicationUrl"/* name */
	, &String_t_0_0_1/* type */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* parent */
	, offsetof(ActivatedClientTypeEntry_t2629, ___applicationUrl_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Type_t_0_0_1;
FieldInfo ActivatedClientTypeEntry_t2629____obj_type_3_FieldInfo = 
{
	"obj_type"/* name */
	, &Type_t_0_0_1/* type */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* parent */
	, offsetof(ActivatedClientTypeEntry_t2629, ___obj_type_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ActivatedClientTypeEntry_t2629_FieldInfos[] =
{
	&ActivatedClientTypeEntry_t2629____applicationUrl_2_FieldInfo,
	&ActivatedClientTypeEntry_t2629____obj_type_3_FieldInfo,
	NULL
};
extern MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m12699_MethodInfo;
static PropertyInfo ActivatedClientTypeEntry_t2629____ApplicationUrl_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* parent */
	, "ApplicationUrl"/* name */
	, &ActivatedClientTypeEntry_get_ApplicationUrl_m12699_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m12700_MethodInfo;
static PropertyInfo ActivatedClientTypeEntry_t2629____ContextAttributes_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* parent */
	, "ContextAttributes"/* name */
	, &ActivatedClientTypeEntry_get_ContextAttributes_m12700_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ActivatedClientTypeEntry_get_ObjectType_m12701_MethodInfo;
static PropertyInfo ActivatedClientTypeEntry_t2629____ObjectType_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ActivatedClientTypeEntry_get_ObjectType_m12701_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ActivatedClientTypeEntry_t2629_PropertyInfos[] =
{
	&ActivatedClientTypeEntry_t2629____ApplicationUrl_PropertyInfo,
	&ActivatedClientTypeEntry_t2629____ContextAttributes_PropertyInfo,
	&ActivatedClientTypeEntry_t2629____ObjectType_PropertyInfo,
	NULL
};
extern MethodInfo ActivatedClientTypeEntry_ToString_m12702_MethodInfo;
static Il2CppMethodReference ActivatedClientTypeEntry_t2629_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m12702_MethodInfo,
};
static bool ActivatedClientTypeEntry_t2629_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ActivatedClientTypeEntry_t2629_0_0_0;
extern Il2CppType ActivatedClientTypeEntry_t2629_1_0_0;
extern Il2CppType TypeEntry_t2630_0_0_0;
struct ActivatedClientTypeEntry_t2629;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t2629_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t2630_0_0_0/* parent */
	, ActivatedClientTypeEntry_t2629_VTable/* vtableMethods */
	, ActivatedClientTypeEntry_t2629_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ActivatedClientTypeEntry_t2629_MethodInfos/* methods */
	, ActivatedClientTypeEntry_t2629_PropertyInfos/* properties */
	, ActivatedClientTypeEntry_t2629_FieldInfos/* fields */
	, NULL/* events */
	, &ActivatedClientTypeEntry_t2629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 545/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t2629_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t2629_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t2629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t2629)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t2629)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t2631_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
extern Il2CppType IMessageSink_t2187_0_0_0;
static ParameterInfo EnvoyInfo_t2631_EnvoyInfo__ctor_m12703_ParameterInfos[] = 
{
	{"sinks", 0, 134222040, 0, &IMessageSink_t2187_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.EnvoyInfo::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
MethodInfo EnvoyInfo__ctor_m12703_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyInfo__ctor_m12703/* method */
	, &EnvoyInfo_t2631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, EnvoyInfo_t2631_EnvoyInfo__ctor_m12703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessageSink_t2187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.EnvoyInfo::get_EnvoySinks()
MethodInfo EnvoyInfo_get_EnvoySinks_m12704_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, (methodPointerType)&EnvoyInfo_get_EnvoySinks_m12704/* method */
	, &EnvoyInfo_t2631_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t2187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* EnvoyInfo_t2631_MethodInfos[] =
{
	&EnvoyInfo__ctor_m12703_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m12704_MethodInfo,
	NULL
};
extern Il2CppType IMessageSink_t2187_0_0_1;
FieldInfo EnvoyInfo_t2631____envoySinks_0_FieldInfo = 
{
	"envoySinks"/* name */
	, &IMessageSink_t2187_0_0_1/* type */
	, &EnvoyInfo_t2631_il2cpp_TypeInfo/* parent */
	, offsetof(EnvoyInfo_t2631, ___envoySinks_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* EnvoyInfo_t2631_FieldInfos[] =
{
	&EnvoyInfo_t2631____envoySinks_0_FieldInfo,
	NULL
};
extern MethodInfo EnvoyInfo_get_EnvoySinks_m12704_MethodInfo;
static PropertyInfo EnvoyInfo_t2631____EnvoySinks_PropertyInfo = 
{
	&EnvoyInfo_t2631_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &EnvoyInfo_get_EnvoySinks_m12704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* EnvoyInfo_t2631_PropertyInfos[] =
{
	&EnvoyInfo_t2631____EnvoySinks_PropertyInfo,
	NULL
};
static Il2CppMethodReference EnvoyInfo_t2631_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m12704_MethodInfo,
};
static bool EnvoyInfo_t2631_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IEnvoyInfo_t2637_0_0_0;
static const Il2CppType* EnvoyInfo_t2631_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t2637_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t2631_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t2637_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EnvoyInfo_t2631_0_0_0;
extern Il2CppType EnvoyInfo_t2631_1_0_0;
struct EnvoyInfo_t2631;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t2631_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t2631_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t2631_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t2631_VTable/* vtableMethods */
	, EnvoyInfo_t2631_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo EnvoyInfo_t2631_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, EnvoyInfo_t2631_MethodInfos/* methods */
	, EnvoyInfo_t2631_PropertyInfos/* properties */
	, EnvoyInfo_t2631_FieldInfos/* fields */
	, NULL/* events */
	, &EnvoyInfo_t2631_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t2631_0_0_0/* byval_arg */
	, &EnvoyInfo_t2631_1_0_0/* this_arg */
	, &EnvoyInfo_t2631_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t2631)/* instance_size */
	, sizeof (EnvoyInfo_t2631)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t2635_il2cpp_TypeInfo;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.IChannelInfo::get_ChannelData()
MethodInfo IChannelInfo_get_ChannelData_m14980_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelInfo_t2635_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IChannelInfo_t2635_MethodInfos[] =
{
	&IChannelInfo_get_ChannelData_m14980_MethodInfo,
	NULL
};
extern MethodInfo IChannelInfo_get_ChannelData_m14980_MethodInfo;
static PropertyInfo IChannelInfo_t2635____ChannelData_PropertyInfo = 
{
	&IChannelInfo_t2635_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelInfo_get_ChannelData_m14980_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IChannelInfo_t2635_PropertyInfos[] =
{
	&IChannelInfo_t2635____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IChannelInfo_t2635_1_0_0;
struct IChannelInfo_t2635;
const Il2CppTypeDefinitionMetadata IChannelInfo_t2635_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IChannelInfo_t2635_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IChannelInfo_t2635_MethodInfos/* methods */
	, IChannelInfo_t2635_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IChannelInfo_t2635_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 546/* custom_attributes_cache */
	, &IChannelInfo_t2635_0_0_0/* byval_arg */
	, &IChannelInfo_t2635_1_0_0/* this_arg */
	, &IChannelInfo_t2635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t2637_il2cpp_TypeInfo;
extern Il2CppType IMessageSink_t2187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.IEnvoyInfo::get_EnvoySinks()
MethodInfo IEnvoyInfo_get_EnvoySinks_m14981_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, NULL/* method */
	, &IEnvoyInfo_t2637_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t2187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IEnvoyInfo_t2637_MethodInfos[] =
{
	&IEnvoyInfo_get_EnvoySinks_m14981_MethodInfo,
	NULL
};
extern MethodInfo IEnvoyInfo_get_EnvoySinks_m14981_MethodInfo;
static PropertyInfo IEnvoyInfo_t2637____EnvoySinks_PropertyInfo = 
{
	&IEnvoyInfo_t2637_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &IEnvoyInfo_get_EnvoySinks_m14981_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IEnvoyInfo_t2637_PropertyInfos[] =
{
	&IEnvoyInfo_t2637____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnvoyInfo_t2637_1_0_0;
struct IEnvoyInfo_t2637;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t2637_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IEnvoyInfo_t2637_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IEnvoyInfo_t2637_MethodInfos/* methods */
	, IEnvoyInfo_t2637_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IEnvoyInfo_t2637_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 547/* custom_attributes_cache */
	, &IEnvoyInfo_t2637_0_0_0/* byval_arg */
	, &IEnvoyInfo_t2637_1_0_0/* this_arg */
	, &IEnvoyInfo_t2637_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t2636_il2cpp_TypeInfo;
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.IRemotingTypeInfo::get_TypeName()
MethodInfo IRemotingTypeInfo_get_TypeName_m14982_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IRemotingTypeInfo_t2636_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IRemotingTypeInfo_t2636_MethodInfos[] =
{
	&IRemotingTypeInfo_get_TypeName_m14982_MethodInfo,
	NULL
};
extern MethodInfo IRemotingTypeInfo_get_TypeName_m14982_MethodInfo;
static PropertyInfo IRemotingTypeInfo_t2636____TypeName_PropertyInfo = 
{
	&IRemotingTypeInfo_t2636_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IRemotingTypeInfo_get_TypeName_m14982_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* IRemotingTypeInfo_t2636_PropertyInfos[] =
{
	&IRemotingTypeInfo_t2636____TypeName_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IRemotingTypeInfo_t2636_1_0_0;
struct IRemotingTypeInfo_t2636;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t2636_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IRemotingTypeInfo_t2636_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IRemotingTypeInfo_t2636_MethodInfos/* methods */
	, IRemotingTypeInfo_t2636_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IRemotingTypeInfo_t2636_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 548/* custom_attributes_cache */
	, &IRemotingTypeInfo_t2636_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t2636_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t2636_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t2626_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
static ParameterInfo Identity_t2626_Identity__ctor_m12705_ParameterInfos[] = 
{
	{"objectUri", 0, 134222041, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::.ctor(System.String)
MethodInfo Identity__ctor_m12705_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Identity__ctor_m12705/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Identity_t2626_Identity__ctor_m12705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo Identity_t2626_Identity_CreateObjRef_m14983_ParameterInfos[] = 
{
	{"requestedType", 0, 134222042, 0, &Type_t_0_0_0},
};
extern Il2CppType ObjRef_t2632_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Identity::CreateObjRef(System.Type)
MethodInfo Identity_CreateObjRef_m14983_MethodInfo = 
{
	"CreateObjRef"/* name */
	, NULL/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Identity_t2626_Identity_CreateObjRef_m14983_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessageSink_t2187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Identity::get_ChannelSink()
MethodInfo Identity_get_ChannelSink_m12706_MethodInfo = 
{
	"get_ChannelSink"/* name */
	, (methodPointerType)&Identity_get_ChannelSink_m12706/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t2187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMessageSink_t2187_0_0_0;
static ParameterInfo Identity_t2626_Identity_set_ChannelSink_m12707_ParameterInfos[] = 
{
	{"value", 0, 134222043, 0, &IMessageSink_t2187_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_ChannelSink(System.Runtime.Remoting.Messaging.IMessageSink)
MethodInfo Identity_set_ChannelSink_m12707_MethodInfo = 
{
	"set_ChannelSink"/* name */
	, (methodPointerType)&Identity_set_ChannelSink_m12707/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, Identity_t2626_Identity_set_ChannelSink_m12707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Identity::get_ObjectUri()
MethodInfo Identity_get_ObjectUri_m12708_MethodInfo = 
{
	"get_ObjectUri"/* name */
	, (methodPointerType)&Identity_get_ObjectUri_m12708/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Identity::get_Disposed()
MethodInfo Identity_get_Disposed_m12709_MethodInfo = 
{
	"get_Disposed"/* name */
	, (methodPointerType)&Identity_get_Disposed_m12709/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo Identity_t2626_Identity_set_Disposed_m12710_ParameterInfos[] = 
{
	{"value", 0, 134222044, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_Disposed(System.Boolean)
MethodInfo Identity_set_Disposed_m12710_MethodInfo = 
{
	"set_Disposed"/* name */
	, (methodPointerType)&Identity_set_Disposed_m12710/* method */
	, &Identity_t2626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, Identity_t2626_Identity_set_Disposed_m12710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Identity_t2626_MethodInfos[] =
{
	&Identity__ctor_m12705_MethodInfo,
	&Identity_CreateObjRef_m14983_MethodInfo,
	&Identity_get_ChannelSink_m12706_MethodInfo,
	&Identity_set_ChannelSink_m12707_MethodInfo,
	&Identity_get_ObjectUri_m12708_MethodInfo,
	&Identity_get_Disposed_m12709_MethodInfo,
	&Identity_set_Disposed_m12710_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_4;
FieldInfo Identity_t2626_____objectUri_0_FieldInfo = 
{
	"_objectUri"/* name */
	, &String_t_0_0_4/* type */
	, &Identity_t2626_il2cpp_TypeInfo/* parent */
	, offsetof(Identity_t2626, ____objectUri_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMessageSink_t2187_0_0_4;
FieldInfo Identity_t2626_____channelSink_1_FieldInfo = 
{
	"_channelSink"/* name */
	, &IMessageSink_t2187_0_0_4/* type */
	, &Identity_t2626_il2cpp_TypeInfo/* parent */
	, offsetof(Identity_t2626, ____channelSink_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IMessageSink_t2187_0_0_4;
FieldInfo Identity_t2626_____envoySink_2_FieldInfo = 
{
	"_envoySink"/* name */
	, &IMessageSink_t2187_0_0_4/* type */
	, &Identity_t2626_il2cpp_TypeInfo/* parent */
	, offsetof(Identity_t2626, ____envoySink_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjRef_t2632_0_0_4;
FieldInfo Identity_t2626_____objRef_3_FieldInfo = 
{
	"_objRef"/* name */
	, &ObjRef_t2632_0_0_4/* type */
	, &Identity_t2626_il2cpp_TypeInfo/* parent */
	, offsetof(Identity_t2626, ____objRef_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo Identity_t2626_____disposed_4_FieldInfo = 
{
	"_disposed"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &Identity_t2626_il2cpp_TypeInfo/* parent */
	, offsetof(Identity_t2626, ____disposed_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* Identity_t2626_FieldInfos[] =
{
	&Identity_t2626_____objectUri_0_FieldInfo,
	&Identity_t2626_____channelSink_1_FieldInfo,
	&Identity_t2626_____envoySink_2_FieldInfo,
	&Identity_t2626_____objRef_3_FieldInfo,
	&Identity_t2626_____disposed_4_FieldInfo,
	NULL
};
extern MethodInfo Identity_get_ChannelSink_m12706_MethodInfo;
extern MethodInfo Identity_set_ChannelSink_m12707_MethodInfo;
static PropertyInfo Identity_t2626____ChannelSink_PropertyInfo = 
{
	&Identity_t2626_il2cpp_TypeInfo/* parent */
	, "ChannelSink"/* name */
	, &Identity_get_ChannelSink_m12706_MethodInfo/* get */
	, &Identity_set_ChannelSink_m12707_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Identity_get_ObjectUri_m12708_MethodInfo;
static PropertyInfo Identity_t2626____ObjectUri_PropertyInfo = 
{
	&Identity_t2626_il2cpp_TypeInfo/* parent */
	, "ObjectUri"/* name */
	, &Identity_get_ObjectUri_m12708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo Identity_get_Disposed_m12709_MethodInfo;
extern MethodInfo Identity_set_Disposed_m12710_MethodInfo;
static PropertyInfo Identity_t2626____Disposed_PropertyInfo = 
{
	&Identity_t2626_il2cpp_TypeInfo/* parent */
	, "Disposed"/* name */
	, &Identity_get_Disposed_m12709_MethodInfo/* get */
	, &Identity_set_Disposed_m12710_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* Identity_t2626_PropertyInfos[] =
{
	&Identity_t2626____ChannelSink_PropertyInfo,
	&Identity_t2626____ObjectUri_PropertyInfo,
	&Identity_t2626____Disposed_PropertyInfo,
	NULL
};
static Il2CppMethodReference Identity_t2626_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	NULL,
};
static bool Identity_t2626_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Identity_t2626_0_0_0;
extern Il2CppType Identity_t2626_1_0_0;
struct Identity_t2626;
const Il2CppTypeDefinitionMetadata Identity_t2626_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t2626_VTable/* vtableMethods */
	, Identity_t2626_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo Identity_t2626_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, Identity_t2626_MethodInfos/* methods */
	, Identity_t2626_PropertyInfos/* properties */
	, Identity_t2626_FieldInfos/* fields */
	, NULL/* events */
	, &Identity_t2626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t2626_0_0_0/* byval_arg */
	, &Identity_t2626_1_0_0/* this_arg */
	, &Identity_t2626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t2626)/* instance_size */
	, sizeof (Identity_t2626)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t2634_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType ObjRef_t2632_0_0_0;
static ParameterInfo ClientIdentity_t2634_ClientIdentity__ctor_m12711_ParameterInfos[] = 
{
	{"objectUri", 0, 134222045, 0, &String_t_0_0_0},
	{"objRef", 1, 134222046, 0, &ObjRef_t2632_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::.ctor(System.String,System.Runtime.Remoting.ObjRef)
MethodInfo ClientIdentity__ctor_m12711_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClientIdentity__ctor_m12711/* method */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2634_ClientIdentity__ctor_m12711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MarshalByRefObject_t2011_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientIdentity::get_ClientProxy()
MethodInfo ClientIdentity_get_ClientProxy_m12712_MethodInfo = 
{
	"get_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_get_ClientProxy_m12712/* method */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t2011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType MarshalByRefObject_t2011_0_0_0;
static ParameterInfo ClientIdentity_t2634_ClientIdentity_set_ClientProxy_m12713_ParameterInfos[] = 
{
	{"value", 0, 134222047, 0, &MarshalByRefObject_t2011_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::set_ClientProxy(System.MarshalByRefObject)
MethodInfo ClientIdentity_set_ClientProxy_m12713_MethodInfo = 
{
	"set_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_set_ClientProxy_m12713/* method */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ClientIdentity_t2634_ClientIdentity_set_ClientProxy_m12713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ClientIdentity_t2634_ClientIdentity_CreateObjRef_m12714_ParameterInfos[] = 
{
	{"requestedType", 0, 134222048, 0, &Type_t_0_0_0},
};
extern Il2CppType ObjRef_t2632_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ClientIdentity::CreateObjRef(System.Type)
MethodInfo ClientIdentity_CreateObjRef_m12714_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ClientIdentity_CreateObjRef_m12714/* method */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t2634_ClientIdentity_CreateObjRef_m12714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ClientIdentity::get_TargetUri()
MethodInfo ClientIdentity_get_TargetUri_m12715_MethodInfo = 
{
	"get_TargetUri"/* name */
	, (methodPointerType)&ClientIdentity_get_TargetUri_m12715/* method */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ClientIdentity_t2634_MethodInfos[] =
{
	&ClientIdentity__ctor_m12711_MethodInfo,
	&ClientIdentity_get_ClientProxy_m12712_MethodInfo,
	&ClientIdentity_set_ClientProxy_m12713_MethodInfo,
	&ClientIdentity_CreateObjRef_m12714_MethodInfo,
	&ClientIdentity_get_TargetUri_m12715_MethodInfo,
	NULL
};
extern Il2CppType WeakReference_t2633_0_0_1;
FieldInfo ClientIdentity_t2634_____proxyReference_5_FieldInfo = 
{
	"_proxyReference"/* name */
	, &WeakReference_t2633_0_0_1/* type */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* parent */
	, offsetof(ClientIdentity_t2634, ____proxyReference_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ClientIdentity_t2634_FieldInfos[] =
{
	&ClientIdentity_t2634_____proxyReference_5_FieldInfo,
	NULL
};
extern MethodInfo ClientIdentity_get_ClientProxy_m12712_MethodInfo;
extern MethodInfo ClientIdentity_set_ClientProxy_m12713_MethodInfo;
static PropertyInfo ClientIdentity_t2634____ClientProxy_PropertyInfo = 
{
	&ClientIdentity_t2634_il2cpp_TypeInfo/* parent */
	, "ClientProxy"/* name */
	, &ClientIdentity_get_ClientProxy_m12712_MethodInfo/* get */
	, &ClientIdentity_set_ClientProxy_m12713_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ClientIdentity_get_TargetUri_m12715_MethodInfo;
static PropertyInfo ClientIdentity_t2634____TargetUri_PropertyInfo = 
{
	&ClientIdentity_t2634_il2cpp_TypeInfo/* parent */
	, "TargetUri"/* name */
	, &ClientIdentity_get_TargetUri_m12715_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ClientIdentity_t2634_PropertyInfos[] =
{
	&ClientIdentity_t2634____ClientProxy_PropertyInfo,
	&ClientIdentity_t2634____TargetUri_PropertyInfo,
	NULL
};
extern MethodInfo ClientIdentity_CreateObjRef_m12714_MethodInfo;
static Il2CppMethodReference ClientIdentity_t2634_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ClientIdentity_CreateObjRef_m12714_MethodInfo,
};
static bool ClientIdentity_t2634_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ClientIdentity_t2634_1_0_0;
struct ClientIdentity_t2634;
const Il2CppTypeDefinitionMetadata ClientIdentity_t2634_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2626_0_0_0/* parent */
	, ClientIdentity_t2634_VTable/* vtableMethods */
	, ClientIdentity_t2634_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ClientIdentity_t2634_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientIdentity_t2634_MethodInfos/* methods */
	, ClientIdentity_t2634_PropertyInfos/* properties */
	, ClientIdentity_t2634_FieldInfos/* fields */
	, NULL/* events */
	, &ClientIdentity_t2634_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t2634_0_0_0/* byval_arg */
	, &ClientIdentity_t2634_1_0_0/* this_arg */
	, &ClientIdentity_t2634_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t2634)/* instance_size */
	, sizeof (ClientIdentity_t2634)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t2632_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor()
MethodInfo ObjRef__ctor_m12716_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m12716/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef__ctor_m12717_ParameterInfos[] = 
{
	{"info", 0, 134222049, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134222050, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo ObjRef__ctor_m12717_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m12717/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, ObjRef_t2632_ObjRef__ctor_m12717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.cctor()
MethodInfo ObjRef__cctor_m12718_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjRef__cctor_m12718/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.ObjRef::get_IsReferenceToWellKnow()
MethodInfo ObjRef_get_IsReferenceToWellKnow_m12719_MethodInfo = 
{
	"get_IsReferenceToWellKnow"/* name */
	, (methodPointerType)&ObjRef_get_IsReferenceToWellKnow_m12719/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IChannelInfo_t2635_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IChannelInfo System.Runtime.Remoting.ObjRef::get_ChannelInfo()
MethodInfo ObjRef_get_ChannelInfo_m12720_MethodInfo = 
{
	"get_ChannelInfo"/* name */
	, (methodPointerType)&ObjRef_get_ChannelInfo_m12720/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &IChannelInfo_t2635_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 551/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnvoyInfo_t2637_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IEnvoyInfo System.Runtime.Remoting.ObjRef::get_EnvoyInfo()
MethodInfo ObjRef_get_EnvoyInfo_m12721_MethodInfo = 
{
	"get_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_get_EnvoyInfo_m12721/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &IEnvoyInfo_t2637_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnvoyInfo_t2637_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef_set_EnvoyInfo_m12722_ParameterInfos[] = 
{
	{"value", 0, 134222051, 0, &IEnvoyInfo_t2637_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_EnvoyInfo(System.Runtime.Remoting.IEnvoyInfo)
MethodInfo ObjRef_set_EnvoyInfo_m12722_MethodInfo = 
{
	"set_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_set_EnvoyInfo_m12722/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ObjRef_t2632_ObjRef_set_EnvoyInfo_m12722_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IRemotingTypeInfo_t2636_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IRemotingTypeInfo System.Runtime.Remoting.ObjRef::get_TypeInfo()
MethodInfo ObjRef_get_TypeInfo_m12723_MethodInfo = 
{
	"get_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_get_TypeInfo_m12723/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &IRemotingTypeInfo_t2636_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IRemotingTypeInfo_t2636_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef_set_TypeInfo_m12724_ParameterInfos[] = 
{
	{"value", 0, 134222052, 0, &IRemotingTypeInfo_t2636_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_TypeInfo(System.Runtime.Remoting.IRemotingTypeInfo)
MethodInfo ObjRef_set_TypeInfo_m12724_MethodInfo = 
{
	"set_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_set_TypeInfo_m12724/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ObjRef_t2632_ObjRef_set_TypeInfo_m12724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ObjRef::get_URI()
MethodInfo ObjRef_get_URI_m12725_MethodInfo = 
{
	"get_URI"/* name */
	, (methodPointerType)&ObjRef_get_URI_m12725/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef_set_URI_m12726_ParameterInfos[] = 
{
	{"value", 0, 134222053, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_URI(System.String)
MethodInfo ObjRef_set_URI_m12726_MethodInfo = 
{
	"set_URI"/* name */
	, (methodPointerType)&ObjRef_set_URI_m12726/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ObjRef_t2632_ObjRef_set_URI_m12726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef_GetObjectData_m12727_ParameterInfos[] = 
{
	{"info", 0, 134222054, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134222055, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo ObjRef_GetObjectData_m12727_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjRef_GetObjectData_m12727/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, ObjRef_t2632_ObjRef_GetObjectData_m12727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo ObjRef_t2632_ObjRef_GetRealObject_m12728_ParameterInfos[] = 
{
	{"context", 0, 134222056, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.ObjRef::GetRealObject(System.Runtime.Serialization.StreamingContext)
MethodInfo ObjRef_GetRealObject_m12728_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&ObjRef_GetRealObject_m12728/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1674/* invoker_method */
	, ObjRef_t2632_ObjRef_GetRealObject_m12728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::UpdateChannelInfo()
MethodInfo ObjRef_UpdateChannelInfo_m12729_MethodInfo = 
{
	"UpdateChannelInfo"/* name */
	, (methodPointerType)&ObjRef_UpdateChannelInfo_m12729/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ObjRef::get_ServerType()
MethodInfo ObjRef_get_ServerType_m12730_MethodInfo = 
{
	"get_ServerType"/* name */
	, (methodPointerType)&ObjRef_get_ServerType_m12730/* method */
	, &ObjRef_t2632_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObjRef_t2632_MethodInfos[] =
{
	&ObjRef__ctor_m12716_MethodInfo,
	&ObjRef__ctor_m12717_MethodInfo,
	&ObjRef__cctor_m12718_MethodInfo,
	&ObjRef_get_IsReferenceToWellKnow_m12719_MethodInfo,
	&ObjRef_get_ChannelInfo_m12720_MethodInfo,
	&ObjRef_get_EnvoyInfo_m12721_MethodInfo,
	&ObjRef_set_EnvoyInfo_m12722_MethodInfo,
	&ObjRef_get_TypeInfo_m12723_MethodInfo,
	&ObjRef_set_TypeInfo_m12724_MethodInfo,
	&ObjRef_get_URI_m12725_MethodInfo,
	&ObjRef_set_URI_m12726_MethodInfo,
	&ObjRef_GetObjectData_m12727_MethodInfo,
	&ObjRef_GetRealObject_m12728_MethodInfo,
	&ObjRef_UpdateChannelInfo_m12729_MethodInfo,
	&ObjRef_get_ServerType_m12730_MethodInfo,
	NULL
};
extern Il2CppType IChannelInfo_t2635_0_0_1;
FieldInfo ObjRef_t2632____channel_info_0_FieldInfo = 
{
	"channel_info"/* name */
	, &IChannelInfo_t2635_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ___channel_info_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo ObjRef_t2632____uri_1_FieldInfo = 
{
	"uri"/* name */
	, &String_t_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ___uri_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IRemotingTypeInfo_t2636_0_0_1;
FieldInfo ObjRef_t2632____typeInfo_2_FieldInfo = 
{
	"typeInfo"/* name */
	, &IRemotingTypeInfo_t2636_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ___typeInfo_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType IEnvoyInfo_t2637_0_0_1;
FieldInfo ObjRef_t2632____envoyInfo_3_FieldInfo = 
{
	"envoyInfo"/* name */
	, &IEnvoyInfo_t2637_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ___envoyInfo_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ObjRef_t2632____flags_4_FieldInfo = 
{
	"flags"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ___flags_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Type_t_0_0_1;
FieldInfo ObjRef_t2632_____serverType_5_FieldInfo = 
{
	"_serverType"/* name */
	, &Type_t_0_0_1/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632, ____serverType_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_17;
FieldInfo ObjRef_t2632____MarshalledObjectRef_6_FieldInfo = 
{
	"MarshalledObjectRef"/* name */
	, &Int32_t189_0_0_17/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632_StaticFields, ___MarshalledObjectRef_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_17;
FieldInfo ObjRef_t2632____WellKnowObjectRef_7_FieldInfo = 
{
	"WellKnowObjectRef"/* name */
	, &Int32_t189_0_0_17/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632_StaticFields, ___WellKnowObjectRef_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t75_0_0_17;
FieldInfo ObjRef_t2632____U3CU3Ef__switchU24map26_8_FieldInfo = 
{
	"<>f__switch$map26"/* name */
	, &Dictionary_2_t75_0_0_17/* type */
	, &ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, offsetof(ObjRef_t2632_StaticFields, ___U3CU3Ef__switchU24map26_8)/* offset */
	, 550/* custom_attributes_cache */

};
static FieldInfo* ObjRef_t2632_FieldInfos[] =
{
	&ObjRef_t2632____channel_info_0_FieldInfo,
	&ObjRef_t2632____uri_1_FieldInfo,
	&ObjRef_t2632____typeInfo_2_FieldInfo,
	&ObjRef_t2632____envoyInfo_3_FieldInfo,
	&ObjRef_t2632____flags_4_FieldInfo,
	&ObjRef_t2632_____serverType_5_FieldInfo,
	&ObjRef_t2632____MarshalledObjectRef_6_FieldInfo,
	&ObjRef_t2632____WellKnowObjectRef_7_FieldInfo,
	&ObjRef_t2632____U3CU3Ef__switchU24map26_8_FieldInfo,
	NULL
};
extern MethodInfo ObjRef_get_IsReferenceToWellKnow_m12719_MethodInfo;
static PropertyInfo ObjRef_t2632____IsReferenceToWellKnow_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "IsReferenceToWellKnow"/* name */
	, &ObjRef_get_IsReferenceToWellKnow_m12719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjRef_get_ChannelInfo_m12720_MethodInfo;
static PropertyInfo ObjRef_t2632____ChannelInfo_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "ChannelInfo"/* name */
	, &ObjRef_get_ChannelInfo_m12720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjRef_get_EnvoyInfo_m12721_MethodInfo;
extern MethodInfo ObjRef_set_EnvoyInfo_m12722_MethodInfo;
static PropertyInfo ObjRef_t2632____EnvoyInfo_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "EnvoyInfo"/* name */
	, &ObjRef_get_EnvoyInfo_m12721_MethodInfo/* get */
	, &ObjRef_set_EnvoyInfo_m12722_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjRef_get_TypeInfo_m12723_MethodInfo;
extern MethodInfo ObjRef_set_TypeInfo_m12724_MethodInfo;
static PropertyInfo ObjRef_t2632____TypeInfo_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "TypeInfo"/* name */
	, &ObjRef_get_TypeInfo_m12723_MethodInfo/* get */
	, &ObjRef_set_TypeInfo_m12724_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjRef_get_URI_m12725_MethodInfo;
extern MethodInfo ObjRef_set_URI_m12726_MethodInfo;
static PropertyInfo ObjRef_t2632____URI_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "URI"/* name */
	, &ObjRef_get_URI_m12725_MethodInfo/* get */
	, &ObjRef_set_URI_m12726_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo ObjRef_get_ServerType_m12730_MethodInfo;
static PropertyInfo ObjRef_t2632____ServerType_PropertyInfo = 
{
	&ObjRef_t2632_il2cpp_TypeInfo/* parent */
	, "ServerType"/* name */
	, &ObjRef_get_ServerType_m12730_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ObjRef_t2632_PropertyInfos[] =
{
	&ObjRef_t2632____IsReferenceToWellKnow_PropertyInfo,
	&ObjRef_t2632____ChannelInfo_PropertyInfo,
	&ObjRef_t2632____EnvoyInfo_PropertyInfo,
	&ObjRef_t2632____TypeInfo_PropertyInfo,
	&ObjRef_t2632____URI_PropertyInfo,
	&ObjRef_t2632____ServerType_PropertyInfo,
	NULL
};
extern MethodInfo ObjRef_GetObjectData_m12727_MethodInfo;
extern MethodInfo ObjRef_GetRealObject_m12728_MethodInfo;
static Il2CppMethodReference ObjRef_t2632_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ObjRef_GetObjectData_m12727_MethodInfo,
	&ObjRef_GetRealObject_m12728_MethodInfo,
	&ObjRef_get_ChannelInfo_m12720_MethodInfo,
	&ObjRef_get_EnvoyInfo_m12721_MethodInfo,
	&ObjRef_set_EnvoyInfo_m12722_MethodInfo,
	&ObjRef_get_TypeInfo_m12723_MethodInfo,
	&ObjRef_set_TypeInfo_m12724_MethodInfo,
	&ObjRef_get_URI_m12725_MethodInfo,
	&ObjRef_set_URI_m12726_MethodInfo,
	&ObjRef_GetObjectData_m12727_MethodInfo,
	&ObjRef_GetRealObject_m12728_MethodInfo,
};
static bool ObjRef_t2632_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IObjectReference_t2906_0_0_0;
static const Il2CppType* ObjRef_t2632_InterfacesTypeInfos[] = 
{
	&ISerializable_t310_0_0_0,
	&IObjectReference_t2906_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t2632_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &IObjectReference_t2906_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ObjRef_t2632_1_0_0;
struct ObjRef_t2632;
const Il2CppTypeDefinitionMetadata ObjRef_t2632_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t2632_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t2632_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t2632_VTable/* vtableMethods */
	, ObjRef_t2632_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ObjRef_t2632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ObjRef_t2632_MethodInfos/* methods */
	, ObjRef_t2632_PropertyInfos/* properties */
	, ObjRef_t2632_FieldInfos/* fields */
	, NULL/* events */
	, &ObjRef_t2632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 549/* custom_attributes_cache */
	, &ObjRef_t2632_0_0_0/* byval_arg */
	, &ObjRef_t2632_1_0_0/* this_arg */
	, &ObjRef_t2632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t2632)/* instance_size */
	, sizeof (ObjRef_t2632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t2632_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t2638_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingConfiguration::.cctor()
MethodInfo RemotingConfiguration__cctor_m12731_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingConfiguration__cctor_m12731/* method */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationName()
MethodInfo RemotingConfiguration_get_ApplicationName_m12732_MethodInfo = 
{
	"get_ApplicationName"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ApplicationName_m12732/* method */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ProcessId()
MethodInfo RemotingConfiguration_get_ProcessId_m12733_MethodInfo = 
{
	"get_ProcessId"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ProcessId_m12733/* method */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo RemotingConfiguration_t2638_RemotingConfiguration_IsRemotelyActivatedClientType_m12734_ParameterInfos[] = 
{
	{"svrType", 0, 134222057, 0, &Type_t_0_0_0},
};
extern Il2CppType ActivatedClientTypeEntry_t2629_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.Type)
MethodInfo RemotingConfiguration_IsRemotelyActivatedClientType_m12734_MethodInfo = 
{
	"IsRemotelyActivatedClientType"/* name */
	, (methodPointerType)&RemotingConfiguration_IsRemotelyActivatedClientType_m12734/* method */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* declaring_type */
	, &ActivatedClientTypeEntry_t2629_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingConfiguration_t2638_RemotingConfiguration_IsRemotelyActivatedClientType_m12734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingConfiguration_t2638_MethodInfos[] =
{
	&RemotingConfiguration__cctor_m12731_MethodInfo,
	&RemotingConfiguration_get_ApplicationName_m12732_MethodInfo,
	&RemotingConfiguration_get_ProcessId_m12733_MethodInfo,
	&RemotingConfiguration_IsRemotelyActivatedClientType_m12734_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_17;
FieldInfo RemotingConfiguration_t2638____applicationID_0_FieldInfo = 
{
	"applicationID"/* name */
	, &String_t_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___applicationID_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo RemotingConfiguration_t2638____applicationName_1_FieldInfo = 
{
	"applicationName"/* name */
	, &String_t_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___applicationName_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo RemotingConfiguration_t2638____processGuid_2_FieldInfo = 
{
	"processGuid"/* name */
	, &String_t_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___processGuid_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo RemotingConfiguration_t2638____defaultConfigRead_3_FieldInfo = 
{
	"defaultConfigRead"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___defaultConfigRead_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_17;
FieldInfo RemotingConfiguration_t2638____defaultDelayedConfigRead_4_FieldInfo = 
{
	"defaultDelayedConfigRead"/* name */
	, &Boolean_t203_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___defaultDelayedConfigRead_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____wellKnownClientEntries_5_FieldInfo = 
{
	"wellKnownClientEntries"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___wellKnownClientEntries_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____activatedClientEntries_6_FieldInfo = 
{
	"activatedClientEntries"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___activatedClientEntries_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____wellKnownServiceEntries_7_FieldInfo = 
{
	"wellKnownServiceEntries"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___wellKnownServiceEntries_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____activatedServiceEntries_8_FieldInfo = 
{
	"activatedServiceEntries"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___activatedServiceEntries_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____channelTemplates_9_FieldInfo = 
{
	"channelTemplates"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___channelTemplates_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____clientProviderTemplates_10_FieldInfo = 
{
	"clientProviderTemplates"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___clientProviderTemplates_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingConfiguration_t2638____serverProviderTemplates_11_FieldInfo = 
{
	"serverProviderTemplates"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingConfiguration_t2638_StaticFields, ___serverProviderTemplates_11)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RemotingConfiguration_t2638_FieldInfos[] =
{
	&RemotingConfiguration_t2638____applicationID_0_FieldInfo,
	&RemotingConfiguration_t2638____applicationName_1_FieldInfo,
	&RemotingConfiguration_t2638____processGuid_2_FieldInfo,
	&RemotingConfiguration_t2638____defaultConfigRead_3_FieldInfo,
	&RemotingConfiguration_t2638____defaultDelayedConfigRead_4_FieldInfo,
	&RemotingConfiguration_t2638____wellKnownClientEntries_5_FieldInfo,
	&RemotingConfiguration_t2638____activatedClientEntries_6_FieldInfo,
	&RemotingConfiguration_t2638____wellKnownServiceEntries_7_FieldInfo,
	&RemotingConfiguration_t2638____activatedServiceEntries_8_FieldInfo,
	&RemotingConfiguration_t2638____channelTemplates_9_FieldInfo,
	&RemotingConfiguration_t2638____clientProviderTemplates_10_FieldInfo,
	&RemotingConfiguration_t2638____serverProviderTemplates_11_FieldInfo,
	NULL
};
extern MethodInfo RemotingConfiguration_get_ApplicationName_m12732_MethodInfo;
static PropertyInfo RemotingConfiguration_t2638____ApplicationName_PropertyInfo = 
{
	&RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, "ApplicationName"/* name */
	, &RemotingConfiguration_get_ApplicationName_m12732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo RemotingConfiguration_get_ProcessId_m12733_MethodInfo;
static PropertyInfo RemotingConfiguration_t2638____ProcessId_PropertyInfo = 
{
	&RemotingConfiguration_t2638_il2cpp_TypeInfo/* parent */
	, "ProcessId"/* name */
	, &RemotingConfiguration_get_ProcessId_m12733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* RemotingConfiguration_t2638_PropertyInfos[] =
{
	&RemotingConfiguration_t2638____ApplicationName_PropertyInfo,
	&RemotingConfiguration_t2638____ProcessId_PropertyInfo,
	NULL
};
static Il2CppMethodReference RemotingConfiguration_t2638_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RemotingConfiguration_t2638_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingConfiguration_t2638_0_0_0;
extern Il2CppType RemotingConfiguration_t2638_1_0_0;
struct RemotingConfiguration_t2638;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t2638_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t2638_VTable/* vtableMethods */
	, RemotingConfiguration_t2638_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingConfiguration_t2638_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingConfiguration_t2638_MethodInfos/* methods */
	, RemotingConfiguration_t2638_PropertyInfos/* properties */
	, RemotingConfiguration_t2638_FieldInfos/* fields */
	, NULL/* events */
	, &RemotingConfiguration_t2638_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 552/* custom_attributes_cache */
	, &RemotingConfiguration_t2638_0_0_0/* byval_arg */
	, &RemotingConfiguration_t2638_1_0_0/* this_arg */
	, &RemotingConfiguration_t2638_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t2638)/* instance_size */
	, sizeof (RemotingConfiguration_t2638)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t2638_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t2639_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor()
MethodInfo RemotingException__ctor_m12735_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12735/* method */
	, &RemotingException_t2639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RemotingException_t2639_RemotingException__ctor_m12736_ParameterInfos[] = 
{
	{"message", 0, 134222058, 0, &String_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.String)
MethodInfo RemotingException__ctor_m12736_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12736/* method */
	, &RemotingException_t2639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, RemotingException_t2639_RemotingException__ctor_m12736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo RemotingException_t2639_RemotingException__ctor_m12737_ParameterInfos[] = 
{
	{"info", 0, 134222059, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 1, 134222060, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo RemotingException__ctor_m12737_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m12737/* method */
	, &RemotingException_t2639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, RemotingException_t2639_RemotingException__ctor_m12737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingException_t2639_MethodInfos[] =
{
	&RemotingException__ctor_m12735_MethodInfo,
	&RemotingException__ctor_m12736_MethodInfo,
	&RemotingException__ctor_m12737_MethodInfo,
	NULL
};
extern MethodInfo Exception_ToString_m1311_MethodInfo;
extern MethodInfo Exception_GetObjectData_m1312_MethodInfo;
extern MethodInfo Exception_get_InnerException_m1313_MethodInfo;
extern MethodInfo Exception_get_Message_m1314_MethodInfo;
extern MethodInfo Exception_get_Source_m1315_MethodInfo;
extern MethodInfo Exception_get_StackTrace_m1316_MethodInfo;
extern MethodInfo Exception_GetType_m1317_MethodInfo;
static Il2CppMethodReference RemotingException_t2639_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Exception_ToString_m1311_MethodInfo,
	&Exception_GetObjectData_m1312_MethodInfo,
	&Exception_get_InnerException_m1313_MethodInfo,
	&Exception_get_Message_m1314_MethodInfo,
	&Exception_get_Source_m1315_MethodInfo,
	&Exception_get_StackTrace_m1316_MethodInfo,
	&Exception_GetObjectData_m1312_MethodInfo,
	&Exception_GetType_m1317_MethodInfo,
};
static bool RemotingException_t2639_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType _Exception_t324_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t2639_InterfacesOffsets[] = 
{
	{ &ISerializable_t310_0_0_0, 4},
	{ &_Exception_t324_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingException_t2639_0_0_0;
extern Il2CppType RemotingException_t2639_1_0_0;
extern Il2CppType SystemException_t2160_0_0_0;
struct RemotingException_t2639;
const Il2CppTypeDefinitionMetadata RemotingException_t2639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t2639_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2160_0_0_0/* parent */
	, RemotingException_t2639_VTable/* vtableMethods */
	, RemotingException_t2639_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingException_t2639_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingException_t2639_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &RemotingException_t2639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 553/* custom_attributes_cache */
	, &RemotingException_t2639_0_0_0/* byval_arg */
	, &RemotingException_t2639_1_0_0/* this_arg */
	, &RemotingException_t2639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t2639)/* instance_size */
	, sizeof (RemotingException_t2639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t2641_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::.cctor()
MethodInfo RemotingServices__cctor_m12738_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingServices__cctor_m12738/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType MethodBase_t1691_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetVirtualMethod_m12739_ParameterInfos[] = 
{
	{"type", 0, 134222061, 0, &Type_t_0_0_0},
	{"method", 1, 134222062, 0, &MethodBase_t1691_0_0_0},
};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)
MethodInfo RemotingServices_GetVirtualMethod_m12739_MethodInfo = 
{
	"GetVirtualMethod"/* name */
	, (methodPointerType)&RemotingServices_GetVirtualMethod_m12739/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetVirtualMethod_m12739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_IsTransparentProxy_m12740_ParameterInfos[] = 
{
	{"proxy", 0, 134222063, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)
MethodInfo RemotingServices_IsTransparentProxy_m12740_MethodInfo = 
{
	"IsTransparentProxy"/* name */
	, (methodPointerType)&RemotingServices_IsTransparentProxy_m12740/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_IsTransparentProxy_m12740_ParameterInfos/* parameters */
	, 555/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetServerTypeForUri_m12741_ParameterInfos[] = 
{
	{"URI", 0, 134222064, 0, &String_t_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.RemotingServices::GetServerTypeForUri(System.String)
MethodInfo RemotingServices_GetServerTypeForUri_m12741_MethodInfo = 
{
	"GetServerTypeForUri"/* name */
	, (methodPointerType)&RemotingServices_GetServerTypeForUri_m12741/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetServerTypeForUri_m12741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_Unmarshal_m12742_ParameterInfos[] = 
{
	{"objectRef", 0, 134222065, 0, &ObjRef_t2632_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef)
MethodInfo RemotingServices_Unmarshal_m12742_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m12742/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_Unmarshal_m12742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_Unmarshal_m12743_ParameterInfos[] = 
{
	{"objectRef", 0, 134222066, 0, &ObjRef_t2632_0_0_0},
	{"fRefine", 1, 134222067, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef,System.Boolean)
MethodInfo RemotingServices_Unmarshal_m12743_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m12743/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236/* invoker_method */
	, RemotingServices_t2641_RemotingServices_Unmarshal_m12743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetRealProxy_m12744_ParameterInfos[] = 
{
	{"proxy", 0, 134222068, 0, &Object_t_0_0_0},
};
extern Il2CppType RealProxy_t2624_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.RemotingServices::GetRealProxy(System.Object)
MethodInfo RemotingServices_GetRealProxy_m12744_MethodInfo = 
{
	"GetRealProxy"/* name */
	, (methodPointerType)&RemotingServices_GetRealProxy_m12744/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t2624_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetRealProxy_m12744_ParameterInfos/* parameters */
	, 556/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IMethodMessage_t2616_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetMethodBaseFromMethodMessage_m12745_ParameterInfos[] = 
{
	{"msg", 0, 134222069, 0, &IMethodMessage_t2616_0_0_0},
};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
MethodInfo RemotingServices_GetMethodBaseFromMethodMessage_m12745_MethodInfo = 
{
	"GetMethodBaseFromMethodMessage"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromMethodMessage_m12745/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetMethodBaseFromMethodMessage_m12745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetMethodBaseFromName_m12746_ParameterInfos[] = 
{
	{"type", 0, 134222070, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222071, 0, &String_t_0_0_0},
	{"signature", 2, 134222072, 0, &TypeU5BU5D_t1671_0_0_0},
};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromName(System.Type,System.String,System.Type[])
MethodInfo RemotingServices_GetMethodBaseFromName_m12746_MethodInfo = 
{
	"GetMethodBaseFromName"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromName_m12746/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetMethodBaseFromName_m12746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType TypeU5BU5D_t1671_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_FindInterfaceMethod_m12747_ParameterInfos[] = 
{
	{"type", 0, 134222073, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222074, 0, &String_t_0_0_0},
	{"signature", 2, 134222075, 0, &TypeU5BU5D_t1671_0_0_0},
};
extern Il2CppType MethodBase_t1691_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::FindInterfaceMethod(System.Type,System.String,System.Type[])
MethodInfo RemotingServices_FindInterfaceMethod_m12747_MethodInfo = 
{
	"FindInterfaceMethod"/* name */
	, (methodPointerType)&RemotingServices_FindInterfaceMethod_m12747/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_FindInterfaceMethod_m12747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ActivatedClientTypeEntry_t2629_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_CreateClientProxy_m12748_ParameterInfos[] = 
{
	{"entry", 0, 134222076, 0, &ActivatedClientTypeEntry_t2629_0_0_0},
	{"activationAttributes", 1, 134222077, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.ActivatedClientTypeEntry,System.Object[])
MethodInfo RemotingServices_CreateClientProxy_m12748_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m12748/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_CreateClientProxy_m12748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_CreateClientProxyForContextBound_m12749_ParameterInfos[] = 
{
	{"type", 0, 134222078, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134222079, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxyForContextBound(System.Type,System.Object[])
MethodInfo RemotingServices_CreateClientProxyForContextBound_m12749_MethodInfo = 
{
	"CreateClientProxyForContextBound"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxyForContextBound_m12749/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_CreateClientProxyForContextBound_m12749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetIdentityForUri_m12750_ParameterInfos[] = 
{
	{"uri", 0, 134222080, 0, &String_t_0_0_0},
};
extern Il2CppType Identity_t2626_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetIdentityForUri(System.String)
MethodInfo RemotingServices_GetIdentityForUri_m12750_MethodInfo = 
{
	"GetIdentityForUri"/* name */
	, (methodPointerType)&RemotingServices_GetIdentityForUri_m12750/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Identity_t2626_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetIdentityForUri_m12750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_RemoveAppNameFromUri_m12751_ParameterInfos[] = 
{
	{"uri", 0, 134222081, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::RemoveAppNameFromUri(System.String)
MethodInfo RemotingServices_RemoveAppNameFromUri_m12751_MethodInfo = 
{
	"RemoveAppNameFromUri"/* name */
	, (methodPointerType)&RemotingServices_RemoveAppNameFromUri_m12751/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_RemoveAppNameFromUri_m12751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType Object_t_1_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetOrCreateClientIdentity_m12752_ParameterInfos[] = 
{
	{"objRef", 0, 134222082, 0, &ObjRef_t2632_0_0_0},
	{"proxyType", 1, 134222083, 0, &Type_t_0_0_0},
	{"clientProxy", 2, 134222084, 0, &Object_t_1_0_2},
};
extern Il2CppType ClientIdentity_t2634_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ClientIdentity System.Runtime.Remoting.RemotingServices::GetOrCreateClientIdentity(System.Runtime.Remoting.ObjRef,System.Type,System.Object&)
MethodInfo RemotingServices_GetOrCreateClientIdentity_m12752_MethodInfo = 
{
	"GetOrCreateClientIdentity"/* name */
	, (methodPointerType)&RemotingServices_GetOrCreateClientIdentity_m12752/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &ClientIdentity_t2634_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3349/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetOrCreateClientIdentity_m12752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType WellKnownObjectMode_t2646_0_0_0;
extern Il2CppType WellKnownObjectMode_t2646_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_CreateWellKnownServerIdentity_m12753_ParameterInfos[] = 
{
	{"objectType", 0, 134222085, 0, &Type_t_0_0_0},
	{"objectUri", 1, 134222086, 0, &String_t_0_0_0},
	{"mode", 2, 134222087, 0, &WellKnownObjectMode_t2646_0_0_0},
};
extern Il2CppType ServerIdentity_t2352_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.RemotingServices::CreateWellKnownServerIdentity(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
MethodInfo RemotingServices_CreateWellKnownServerIdentity_m12753_MethodInfo = 
{
	"CreateWellKnownServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_CreateWellKnownServerIdentity_m12753/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &ServerIdentity_t2352_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189/* invoker_method */
	, RemotingServices_t2641_RemotingServices_CreateWellKnownServerIdentity_m12753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ServerIdentity_t2352_0_0_0;
extern Il2CppType ServerIdentity_t2352_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_RegisterServerIdentity_m12754_ParameterInfos[] = 
{
	{"identity", 0, 134222088, 0, &ServerIdentity_t2352_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterServerIdentity(System.Runtime.Remoting.ServerIdentity)
MethodInfo RemotingServices_RegisterServerIdentity_m12754_MethodInfo = 
{
	"RegisterServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_RegisterServerIdentity_m12754/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_RegisterServerIdentity_m12754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetProxyForRemoteObject_m12755_ParameterInfos[] = 
{
	{"objref", 0, 134222089, 0, &ObjRef_t2632_0_0_0},
	{"classToProxy", 1, 134222090, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetProxyForRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
MethodInfo RemotingServices_GetProxyForRemoteObject_m12755_MethodInfo = 
{
	"GetProxyForRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetProxyForRemoteObject_m12755/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetProxyForRemoteObject_m12755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjRef_t2632_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetRemoteObject_m12756_ParameterInfos[] = 
{
	{"objRef", 0, 134222091, 0, &ObjRef_t2632_0_0_0},
	{"proxyType", 1, 134222092, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
MethodInfo RemotingServices_GetRemoteObject_m12756_MethodInfo = 
{
	"GetRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetRemoteObject_m12756/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetRemoteObject_m12756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterInternalChannels()
MethodInfo RemotingServices_RegisterInternalChannels_m12757_MethodInfo = 
{
	"RegisterInternalChannels"/* name */
	, (methodPointerType)&RemotingServices_RegisterInternalChannels_m12757/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Identity_t2626_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_DisposeIdentity_m12758_ParameterInfos[] = 
{
	{"ident", 0, 134222093, 0, &Identity_t2626_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::DisposeIdentity(System.Runtime.Remoting.Identity)
MethodInfo RemotingServices_DisposeIdentity_m12758_MethodInfo = 
{
	"DisposeIdentity"/* name */
	, (methodPointerType)&RemotingServices_DisposeIdentity_m12758/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_DisposeIdentity_m12758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo RemotingServices_t2641_RemotingServices_GetNormalizedUri_m12759_ParameterInfos[] = 
{
	{"uri", 0, 134222094, 0, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::GetNormalizedUri(System.String)
MethodInfo RemotingServices_GetNormalizedUri_m12759_MethodInfo = 
{
	"GetNormalizedUri"/* name */
	, (methodPointerType)&RemotingServices_GetNormalizedUri_m12759/* method */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2641_RemotingServices_GetNormalizedUri_m12759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingServices_t2641_MethodInfos[] =
{
	&RemotingServices__cctor_m12738_MethodInfo,
	&RemotingServices_GetVirtualMethod_m12739_MethodInfo,
	&RemotingServices_IsTransparentProxy_m12740_MethodInfo,
	&RemotingServices_GetServerTypeForUri_m12741_MethodInfo,
	&RemotingServices_Unmarshal_m12742_MethodInfo,
	&RemotingServices_Unmarshal_m12743_MethodInfo,
	&RemotingServices_GetRealProxy_m12744_MethodInfo,
	&RemotingServices_GetMethodBaseFromMethodMessage_m12745_MethodInfo,
	&RemotingServices_GetMethodBaseFromName_m12746_MethodInfo,
	&RemotingServices_FindInterfaceMethod_m12747_MethodInfo,
	&RemotingServices_CreateClientProxy_m12748_MethodInfo,
	&RemotingServices_CreateClientProxyForContextBound_m12749_MethodInfo,
	&RemotingServices_GetIdentityForUri_m12750_MethodInfo,
	&RemotingServices_RemoveAppNameFromUri_m12751_MethodInfo,
	&RemotingServices_GetOrCreateClientIdentity_m12752_MethodInfo,
	&RemotingServices_CreateWellKnownServerIdentity_m12753_MethodInfo,
	&RemotingServices_RegisterServerIdentity_m12754_MethodInfo,
	&RemotingServices_GetProxyForRemoteObject_m12755_MethodInfo,
	&RemotingServices_GetRemoteObject_m12756_MethodInfo,
	&RemotingServices_RegisterInternalChannels_m12757_MethodInfo,
	&RemotingServices_DisposeIdentity_m12758_MethodInfo,
	&RemotingServices_GetNormalizedUri_m12759_MethodInfo,
	NULL
};
extern Il2CppType Hashtable_t1667_0_0_17;
FieldInfo RemotingServices_t2641____uri_hash_0_FieldInfo = 
{
	"uri_hash"/* name */
	, &Hashtable_t1667_0_0_17/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___uri_hash_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryFormatter_t2640_0_0_17;
FieldInfo RemotingServices_t2641_____serializationFormatter_1_FieldInfo = 
{
	"_serializationFormatter"/* name */
	, &BinaryFormatter_t2640_0_0_17/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ____serializationFormatter_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryFormatter_t2640_0_0_17;
FieldInfo RemotingServices_t2641_____deserializationFormatter_2_FieldInfo = 
{
	"_deserializationFormatter"/* name */
	, &BinaryFormatter_t2640_0_0_17/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ____deserializationFormatter_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_19;
FieldInfo RemotingServices_t2641____app_id_3_FieldInfo = 
{
	"app_id"/* name */
	, &String_t_0_0_19/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___app_id_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_17;
FieldInfo RemotingServices_t2641____next_id_4_FieldInfo = 
{
	"next_id"/* name */
	, &Int32_t189_0_0_17/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___next_id_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BindingFlags_t2530_0_0_49;
FieldInfo RemotingServices_t2641____methodBindings_5_FieldInfo = 
{
	"methodBindings"/* name */
	, &BindingFlags_t2530_0_0_49/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___methodBindings_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodInfo_t_0_0_49;
FieldInfo RemotingServices_t2641____FieldSetterMethod_6_FieldInfo = 
{
	"FieldSetterMethod"/* name */
	, &MethodInfo_t_0_0_49/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___FieldSetterMethod_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodInfo_t_0_0_49;
FieldInfo RemotingServices_t2641____FieldGetterMethod_7_FieldInfo = 
{
	"FieldGetterMethod"/* name */
	, &MethodInfo_t_0_0_49/* type */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* parent */
	, offsetof(RemotingServices_t2641_StaticFields, ___FieldGetterMethod_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* RemotingServices_t2641_FieldInfos[] =
{
	&RemotingServices_t2641____uri_hash_0_FieldInfo,
	&RemotingServices_t2641_____serializationFormatter_1_FieldInfo,
	&RemotingServices_t2641_____deserializationFormatter_2_FieldInfo,
	&RemotingServices_t2641____app_id_3_FieldInfo,
	&RemotingServices_t2641____next_id_4_FieldInfo,
	&RemotingServices_t2641____methodBindings_5_FieldInfo,
	&RemotingServices_t2641____FieldSetterMethod_6_FieldInfo,
	&RemotingServices_t2641____FieldGetterMethod_7_FieldInfo,
	NULL
};
static Il2CppMethodReference RemotingServices_t2641_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RemotingServices_t2641_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType RemotingServices_t2641_0_0_0;
extern Il2CppType RemotingServices_t2641_1_0_0;
struct RemotingServices_t2641;
const Il2CppTypeDefinitionMetadata RemotingServices_t2641_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t2641_VTable/* vtableMethods */
	, RemotingServices_t2641_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingServices_t2641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingServices_t2641_MethodInfos/* methods */
	, NULL/* properties */
	, RemotingServices_t2641_FieldInfos/* fields */
	, NULL/* events */
	, &RemotingServices_t2641_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 554/* custom_attributes_cache */
	, &RemotingServices_t2641_0_0_0/* byval_arg */
	, &RemotingServices_t2641_1_0_0/* this_arg */
	, &RemotingServices_t2641_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t2641)/* instance_size */
	, sizeof (RemotingServices_t2641)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t2641_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t2352_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ServerIdentity_t2352_ServerIdentity__ctor_m12760_ParameterInfos[] = 
{
	{"objectUri", 0, 134222095, 0, &String_t_0_0_0},
	{"context", 1, 134222096, 0, &Context_t2596_0_0_0},
	{"objectType", 2, 134222097, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
MethodInfo ServerIdentity__ctor_m12760_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ServerIdentity__ctor_m12760/* method */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t2352_ServerIdentity__ctor_m12760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ServerIdentity::get_ObjectType()
MethodInfo ServerIdentity_get_ObjectType_m12761_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ServerIdentity_get_ObjectType_m12761/* method */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ServerIdentity_t2352_ServerIdentity_CreateObjRef_m12762_ParameterInfos[] = 
{
	{"requestedType", 0, 134222098, 0, &Type_t_0_0_0},
};
extern Il2CppType ObjRef_t2632_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ServerIdentity::CreateObjRef(System.Type)
MethodInfo ServerIdentity_CreateObjRef_m12762_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ServerIdentity_CreateObjRef_m12762/* method */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t2632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t2352_ServerIdentity_CreateObjRef_m12762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ServerIdentity_t2352_MethodInfos[] =
{
	&ServerIdentity__ctor_m12760_MethodInfo,
	&ServerIdentity_get_ObjectType_m12761_MethodInfo,
	&ServerIdentity_CreateObjRef_m12762_MethodInfo,
	NULL
};
extern Il2CppType Type_t_0_0_4;
FieldInfo ServerIdentity_t2352_____objectType_5_FieldInfo = 
{
	"_objectType"/* name */
	, &Type_t_0_0_4/* type */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* parent */
	, offsetof(ServerIdentity_t2352, ____objectType_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MarshalByRefObject_t2011_0_0_4;
FieldInfo ServerIdentity_t2352_____serverObject_6_FieldInfo = 
{
	"_serverObject"/* name */
	, &MarshalByRefObject_t2011_0_0_4/* type */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* parent */
	, offsetof(ServerIdentity_t2352, ____serverObject_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Context_t2596_0_0_4;
FieldInfo ServerIdentity_t2352_____context_7_FieldInfo = 
{
	"_context"/* name */
	, &Context_t2596_0_0_4/* type */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* parent */
	, offsetof(ServerIdentity_t2352, ____context_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ServerIdentity_t2352_FieldInfos[] =
{
	&ServerIdentity_t2352_____objectType_5_FieldInfo,
	&ServerIdentity_t2352_____serverObject_6_FieldInfo,
	&ServerIdentity_t2352_____context_7_FieldInfo,
	NULL
};
extern MethodInfo ServerIdentity_get_ObjectType_m12761_MethodInfo;
static PropertyInfo ServerIdentity_t2352____ObjectType_PropertyInfo = 
{
	&ServerIdentity_t2352_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ServerIdentity_get_ObjectType_m12761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ServerIdentity_t2352_PropertyInfos[] =
{
	&ServerIdentity_t2352____ObjectType_PropertyInfo,
	NULL
};
extern MethodInfo ServerIdentity_CreateObjRef_m12762_MethodInfo;
static Il2CppMethodReference ServerIdentity_t2352_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ServerIdentity_CreateObjRef_m12762_MethodInfo,
};
static bool ServerIdentity_t2352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ServerIdentity_t2352_1_0_0;
struct ServerIdentity_t2352;
const Il2CppTypeDefinitionMetadata ServerIdentity_t2352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t2626_0_0_0/* parent */
	, ServerIdentity_t2352_VTable/* vtableMethods */
	, ServerIdentity_t2352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ServerIdentity_t2352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ServerIdentity_t2352_MethodInfos/* methods */
	, ServerIdentity_t2352_PropertyInfos/* properties */
	, ServerIdentity_t2352_FieldInfos/* fields */
	, NULL/* events */
	, &ServerIdentity_t2352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t2352_0_0_0/* byval_arg */
	, &ServerIdentity_t2352_1_0_0/* this_arg */
	, &ServerIdentity_t2352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t2352)/* instance_size */
	, sizeof (ServerIdentity_t2352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t2642_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
extern Il2CppType MarshalByRefObject_t2011_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
MethodInfo ClientActivatedIdentity_GetServerObject_m12763_MethodInfo = 
{
	"GetServerObject"/* name */
	, (methodPointerType)&ClientActivatedIdentity_GetServerObject_m12763/* method */
	, &ClientActivatedIdentity_t2642_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t2011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ClientActivatedIdentity_t2642_MethodInfos[] =
{
	&ClientActivatedIdentity_GetServerObject_m12763_MethodInfo,
	NULL
};
static Il2CppMethodReference ClientActivatedIdentity_t2642_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ServerIdentity_CreateObjRef_m12762_MethodInfo,
};
static bool ClientActivatedIdentity_t2642_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ClientActivatedIdentity_t2642_0_0_0;
extern Il2CppType ClientActivatedIdentity_t2642_1_0_0;
struct ClientActivatedIdentity_t2642;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t2642_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2352_0_0_0/* parent */
	, ClientActivatedIdentity_t2642_VTable/* vtableMethods */
	, ClientActivatedIdentity_t2642_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ClientActivatedIdentity_t2642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientActivatedIdentity_t2642_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ClientActivatedIdentity_t2642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t2642_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t2642_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t2642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t2642)/* instance_size */
	, sizeof (ClientActivatedIdentity_t2642)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t2643_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo SingletonIdentity_t2643_SingletonIdentity__ctor_m12764_ParameterInfos[] = 
{
	{"objectUri", 0, 134222099, 0, &String_t_0_0_0},
	{"context", 1, 134222100, 0, &Context_t2596_0_0_0},
	{"objectType", 2, 134222101, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
MethodInfo SingletonIdentity__ctor_m12764_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingletonIdentity__ctor_m12764/* method */
	, &SingletonIdentity_t2643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* invoker_method */
	, SingletonIdentity_t2643_SingletonIdentity__ctor_m12764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SingletonIdentity_t2643_MethodInfos[] =
{
	&SingletonIdentity__ctor_m12764_MethodInfo,
	NULL
};
static Il2CppMethodReference SingletonIdentity_t2643_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ServerIdentity_CreateObjRef_m12762_MethodInfo,
};
static bool SingletonIdentity_t2643_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType SingletonIdentity_t2643_0_0_0;
extern Il2CppType SingletonIdentity_t2643_1_0_0;
struct SingletonIdentity_t2643;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t2643_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2352_0_0_0/* parent */
	, SingletonIdentity_t2643_VTable/* vtableMethods */
	, SingletonIdentity_t2643_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SingletonIdentity_t2643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingletonIdentity_t2643_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SingletonIdentity_t2643_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t2643_0_0_0/* byval_arg */
	, &SingletonIdentity_t2643_1_0_0/* this_arg */
	, &SingletonIdentity_t2643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t2643)/* instance_size */
	, sizeof (SingletonIdentity_t2643)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t2644_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType Context_t2596_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo SingleCallIdentity_t2644_SingleCallIdentity__ctor_m12765_ParameterInfos[] = 
{
	{"objectUri", 0, 134222102, 0, &String_t_0_0_0},
	{"context", 1, 134222103, 0, &Context_t2596_0_0_0},
	{"objectType", 2, 134222104, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
MethodInfo SingleCallIdentity__ctor_m12765_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingleCallIdentity__ctor_m12765/* method */
	, &SingleCallIdentity_t2644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t/* invoker_method */
	, SingleCallIdentity_t2644_SingleCallIdentity__ctor_m12765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SingleCallIdentity_t2644_MethodInfos[] =
{
	&SingleCallIdentity__ctor_m12765_MethodInfo,
	NULL
};
static Il2CppMethodReference SingleCallIdentity_t2644_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&ServerIdentity_CreateObjRef_m12762_MethodInfo,
};
static bool SingleCallIdentity_t2644_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType SingleCallIdentity_t2644_0_0_0;
extern Il2CppType SingleCallIdentity_t2644_1_0_0;
struct SingleCallIdentity_t2644;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t2644_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t2352_0_0_0/* parent */
	, SingleCallIdentity_t2644_VTable/* vtableMethods */
	, SingleCallIdentity_t2644_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SingleCallIdentity_t2644_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingleCallIdentity_t2644_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SingleCallIdentity_t2644_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t2644_0_0_0/* byval_arg */
	, &SingleCallIdentity_t2644_1_0_0/* this_arg */
	, &SingleCallIdentity_t2644_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t2644)/* instance_size */
	, sizeof (SingleCallIdentity_t2644)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t2630_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
MethodInfo TypeEntry_get_AssemblyName_m12766_MethodInfo = 
{
	"get_AssemblyName"/* name */
	, (methodPointerType)&TypeEntry_get_AssemblyName_m12766/* method */
	, &TypeEntry_t2630_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
MethodInfo TypeEntry_get_TypeName_m12767_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeEntry_get_TypeName_m12767/* method */
	, &TypeEntry_t2630_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeEntry_t2630_MethodInfos[] =
{
	&TypeEntry_get_AssemblyName_m12766_MethodInfo,
	&TypeEntry_get_TypeName_m12767_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo TypeEntry_t2630____assembly_name_0_FieldInfo = 
{
	"assembly_name"/* name */
	, &String_t_0_0_1/* type */
	, &TypeEntry_t2630_il2cpp_TypeInfo/* parent */
	, offsetof(TypeEntry_t2630, ___assembly_name_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TypeEntry_t2630____type_name_1_FieldInfo = 
{
	"type_name"/* name */
	, &String_t_0_0_1/* type */
	, &TypeEntry_t2630_il2cpp_TypeInfo/* parent */
	, offsetof(TypeEntry_t2630, ___type_name_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeEntry_t2630_FieldInfos[] =
{
	&TypeEntry_t2630____assembly_name_0_FieldInfo,
	&TypeEntry_t2630____type_name_1_FieldInfo,
	NULL
};
extern MethodInfo TypeEntry_get_AssemblyName_m12766_MethodInfo;
static PropertyInfo TypeEntry_t2630____AssemblyName_PropertyInfo = 
{
	&TypeEntry_t2630_il2cpp_TypeInfo/* parent */
	, "AssemblyName"/* name */
	, &TypeEntry_get_AssemblyName_m12766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo TypeEntry_get_TypeName_m12767_MethodInfo;
static PropertyInfo TypeEntry_t2630____TypeName_PropertyInfo = 
{
	&TypeEntry_t2630_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeEntry_get_TypeName_m12767_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* TypeEntry_t2630_PropertyInfos[] =
{
	&TypeEntry_t2630____AssemblyName_PropertyInfo,
	&TypeEntry_t2630____TypeName_PropertyInfo,
	NULL
};
static Il2CppMethodReference TypeEntry_t2630_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TypeEntry_t2630_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TypeEntry_t2630_1_0_0;
struct TypeEntry_t2630;
const Il2CppTypeDefinitionMetadata TypeEntry_t2630_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t2630_VTable/* vtableMethods */
	, TypeEntry_t2630_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeEntry_t2630_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeEntry_t2630_MethodInfos/* methods */
	, TypeEntry_t2630_PropertyInfos/* properties */
	, TypeEntry_t2630_FieldInfos/* fields */
	, NULL/* events */
	, &TypeEntry_t2630_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 557/* custom_attributes_cache */
	, &TypeEntry_t2630_0_0_0/* byval_arg */
	, &TypeEntry_t2630_1_0_0/* this_arg */
	, &TypeEntry_t2630_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t2630)/* instance_size */
	, sizeof (TypeEntry_t2630)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t2645_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
static ParameterInfo TypeInfo_t2645_TypeInfo__ctor_m12768_ParameterInfos[] = 
{
	{"type", 0, 134222105, 0, &Type_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.TypeInfo::.ctor(System.Type)
MethodInfo TypeInfo__ctor_m12768_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInfo__ctor_m12768/* method */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, TypeInfo_t2645_TypeInfo__ctor_m12768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeInfo::get_TypeName()
MethodInfo TypeInfo_get_TypeName_m12769_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeInfo_get_TypeName_m12769/* method */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeInfo_t2645_MethodInfos[] =
{
	&TypeInfo__ctor_m12768_MethodInfo,
	&TypeInfo_get_TypeName_m12769_MethodInfo,
	NULL
};
extern Il2CppType String_t_0_0_1;
FieldInfo TypeInfo_t2645____serverType_0_FieldInfo = 
{
	"serverType"/* name */
	, &String_t_0_0_1/* type */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInfo_t2645, ___serverType_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo TypeInfo_t2645____serverHierarchy_1_FieldInfo = 
{
	"serverHierarchy"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInfo_t2645, ___serverHierarchy_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_1;
FieldInfo TypeInfo_t2645____interfacesImplemented_2_FieldInfo = 
{
	"interfacesImplemented"/* name */
	, &StringU5BU5D_t169_0_0_1/* type */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInfo_t2645, ___interfacesImplemented_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeInfo_t2645_FieldInfos[] =
{
	&TypeInfo_t2645____serverType_0_FieldInfo,
	&TypeInfo_t2645____serverHierarchy_1_FieldInfo,
	&TypeInfo_t2645____interfacesImplemented_2_FieldInfo,
	NULL
};
extern MethodInfo TypeInfo_get_TypeName_m12769_MethodInfo;
static PropertyInfo TypeInfo_t2645____TypeName_PropertyInfo = 
{
	&TypeInfo_t2645_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeInfo_get_TypeName_m12769_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* TypeInfo_t2645_PropertyInfos[] =
{
	&TypeInfo_t2645____TypeName_PropertyInfo,
	NULL
};
static Il2CppMethodReference TypeInfo_t2645_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&TypeInfo_get_TypeName_m12769_MethodInfo,
};
static bool TypeInfo_t2645_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* TypeInfo_t2645_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t2636_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t2645_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t2636_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TypeInfo_t2645_0_0_0;
extern Il2CppType TypeInfo_t2645_1_0_0;
struct TypeInfo_t2645;
const Il2CppTypeDefinitionMetadata TypeInfo_t2645_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t2645_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t2645_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t2645_VTable/* vtableMethods */
	, TypeInfo_t2645_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeInfo_t2645_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeInfo_t2645_MethodInfos/* methods */
	, TypeInfo_t2645_PropertyInfos/* properties */
	, TypeInfo_t2645_FieldInfos/* fields */
	, NULL/* events */
	, &TypeInfo_t2645_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t2645_0_0_0/* byval_arg */
	, &TypeInfo_t2645_1_0_0/* this_arg */
	, &TypeInfo_t2645_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t2645)/* instance_size */
	, sizeof (TypeInfo_t2645)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t2646_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static MethodInfo* WellKnownObjectMode_t2646_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo WellKnownObjectMode_t2646____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &WellKnownObjectMode_t2646_il2cpp_TypeInfo/* parent */
	, offsetof(WellKnownObjectMode_t2646, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType WellKnownObjectMode_t2646_0_0_32854;
FieldInfo WellKnownObjectMode_t2646____Singleton_2_FieldInfo = 
{
	"Singleton"/* name */
	, &WellKnownObjectMode_t2646_0_0_32854/* type */
	, &WellKnownObjectMode_t2646_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType WellKnownObjectMode_t2646_0_0_32854;
FieldInfo WellKnownObjectMode_t2646____SingleCall_3_FieldInfo = 
{
	"SingleCall"/* name */
	, &WellKnownObjectMode_t2646_0_0_32854/* type */
	, &WellKnownObjectMode_t2646_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* WellKnownObjectMode_t2646_FieldInfos[] =
{
	&WellKnownObjectMode_t2646____value___1_FieldInfo,
	&WellKnownObjectMode_t2646____Singleton_2_FieldInfo,
	&WellKnownObjectMode_t2646____SingleCall_3_FieldInfo,
	NULL
};
static const int32_t WellKnownObjectMode_t2646____Singleton_2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry WellKnownObjectMode_t2646____Singleton_2_DefaultValue = 
{
	&WellKnownObjectMode_t2646____Singleton_2_FieldInfo/* field */
	, { (char*)&WellKnownObjectMode_t2646____Singleton_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t WellKnownObjectMode_t2646____SingleCall_3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry WellKnownObjectMode_t2646____SingleCall_3_DefaultValue = 
{
	&WellKnownObjectMode_t2646____SingleCall_3_FieldInfo/* field */
	, { (char*)&WellKnownObjectMode_t2646____SingleCall_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* WellKnownObjectMode_t2646_FieldDefaultValues[] = 
{
	&WellKnownObjectMode_t2646____Singleton_2_DefaultValue,
	&WellKnownObjectMode_t2646____SingleCall_3_DefaultValue,
	NULL
};
static Il2CppMethodReference WellKnownObjectMode_t2646_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool WellKnownObjectMode_t2646_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t2646_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType WellKnownObjectMode_t2646_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t2646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t2646_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, WellKnownObjectMode_t2646_VTable/* vtableMethods */
	, WellKnownObjectMode_t2646_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo WellKnownObjectMode_t2646_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, WellKnownObjectMode_t2646_MethodInfos/* methods */
	, NULL/* properties */
	, WellKnownObjectMode_t2646_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 558/* custom_attributes_cache */
	, &WellKnownObjectMode_t2646_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t2646_1_0_0/* this_arg */
	, &WellKnownObjectMode_t2646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, WellKnownObjectMode_t2646_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t2646)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t2646)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t2647_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::.cctor()
MethodInfo BinaryCommon__cctor_m12770_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&BinaryCommon__cctor_m12770/* method */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo BinaryCommon_t2647_BinaryCommon_IsPrimitive_m12771_ParameterInfos[] = 
{
	{"type", 0, 134222106, 0, &Type_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryCommon::IsPrimitive(System.Type)
MethodInfo BinaryCommon_IsPrimitive_m12771_MethodInfo = 
{
	"IsPrimitive"/* name */
	, (methodPointerType)&BinaryCommon_IsPrimitive_m12771/* method */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, BinaryCommon_t2647_BinaryCommon_IsPrimitive_m12771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo BinaryCommon_t2647_BinaryCommon_GetTypeFromCode_m12772_ParameterInfos[] = 
{
	{"code", 0, 134222107, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryCommon::GetTypeFromCode(System.Int32)
MethodInfo BinaryCommon_GetTypeFromCode_m12772_MethodInfo = 
{
	"GetTypeFromCode"/* name */
	, (methodPointerType)&BinaryCommon_GetTypeFromCode_m12772/* method */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t189/* invoker_method */
	, BinaryCommon_t2647_BinaryCommon_GetTypeFromCode_m12772_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo BinaryCommon_t2647_BinaryCommon_SwapBytes_m12773_ParameterInfos[] = 
{
	{"byteArray", 0, 134222108, 0, &ByteU5BU5D_t350_0_0_0},
	{"size", 1, 134222109, 0, &Int32_t189_0_0_0},
	{"dataSize", 2, 134222110, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::SwapBytes(System.Byte[],System.Int32,System.Int32)
MethodInfo BinaryCommon_SwapBytes_m12773_MethodInfo = 
{
	"SwapBytes"/* name */
	, (methodPointerType)&BinaryCommon_SwapBytes_m12773/* method */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189/* invoker_method */
	, BinaryCommon_t2647_BinaryCommon_SwapBytes_m12773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BinaryCommon_t2647_MethodInfos[] =
{
	&BinaryCommon__cctor_m12770_MethodInfo,
	&BinaryCommon_IsPrimitive_m12771_MethodInfo,
	&BinaryCommon_GetTypeFromCode_m12772_MethodInfo,
	&BinaryCommon_SwapBytes_m12773_MethodInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t350_0_0_22;
FieldInfo BinaryCommon_t2647____BinaryHeader_0_FieldInfo = 
{
	"BinaryHeader"/* name */
	, &ByteU5BU5D_t350_0_0_22/* type */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryCommon_t2647_StaticFields, ___BinaryHeader_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_17;
FieldInfo BinaryCommon_t2647_____typeCodesToType_1_FieldInfo = 
{
	"_typeCodesToType"/* name */
	, &TypeU5BU5D_t1671_0_0_17/* type */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryCommon_t2647_StaticFields, ____typeCodesToType_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_17;
FieldInfo BinaryCommon_t2647_____typeCodeMap_2_FieldInfo = 
{
	"_typeCodeMap"/* name */
	, &ByteU5BU5D_t350_0_0_17/* type */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryCommon_t2647_StaticFields, ____typeCodeMap_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo BinaryCommon_t2647____UseReflectionSerialization_3_FieldInfo = 
{
	"UseReflectionSerialization"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryCommon_t2647_StaticFields, ___UseReflectionSerialization_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* BinaryCommon_t2647_FieldInfos[] =
{
	&BinaryCommon_t2647____BinaryHeader_0_FieldInfo,
	&BinaryCommon_t2647_____typeCodesToType_1_FieldInfo,
	&BinaryCommon_t2647_____typeCodeMap_2_FieldInfo,
	&BinaryCommon_t2647____UseReflectionSerialization_3_FieldInfo,
	NULL
};
static Il2CppMethodReference BinaryCommon_t2647_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool BinaryCommon_t2647_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType BinaryCommon_t2647_0_0_0;
extern Il2CppType BinaryCommon_t2647_1_0_0;
struct BinaryCommon_t2647;
const Il2CppTypeDefinitionMetadata BinaryCommon_t2647_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t2647_VTable/* vtableMethods */
	, BinaryCommon_t2647_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BinaryCommon_t2647_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryCommon_t2647_MethodInfos/* methods */
	, NULL/* properties */
	, BinaryCommon_t2647_FieldInfos/* fields */
	, NULL/* events */
	, &BinaryCommon_t2647_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t2647_0_0_0/* byval_arg */
	, &BinaryCommon_t2647_1_0_0/* this_arg */
	, &BinaryCommon_t2647_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t2647)/* instance_size */
	, sizeof (BinaryCommon_t2647)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t2647_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t2648_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
static MethodInfo* BinaryElement_t2648_MethodInfos[] =
{
	NULL
};
extern Il2CppType Byte_t237_0_0_1542;
FieldInfo BinaryElement_t2648____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t237_0_0_1542/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryElement_t2648, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____Header_2_FieldInfo = 
{
	"Header"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____RefTypeObject_3_FieldInfo = 
{
	"RefTypeObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____UntypedRuntimeObject_4_FieldInfo = 
{
	"UntypedRuntimeObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____UntypedExternalObject_5_FieldInfo = 
{
	"UntypedExternalObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____RuntimeObject_6_FieldInfo = 
{
	"RuntimeObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ExternalObject_7_FieldInfo = 
{
	"ExternalObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____String_8_FieldInfo = 
{
	"String"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____GenericArray_9_FieldInfo = 
{
	"GenericArray"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____BoxedPrimitiveTypeValue_10_FieldInfo = 
{
	"BoxedPrimitiveTypeValue"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ObjectReference_11_FieldInfo = 
{
	"ObjectReference"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____NullValue_12_FieldInfo = 
{
	"NullValue"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____End_13_FieldInfo = 
{
	"End"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____Assembly_14_FieldInfo = 
{
	"Assembly"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ArrayFiller8b_15_FieldInfo = 
{
	"ArrayFiller8b"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ArrayFiller32b_16_FieldInfo = 
{
	"ArrayFiller32b"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ArrayOfPrimitiveType_17_FieldInfo = 
{
	"ArrayOfPrimitiveType"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ArrayOfObject_18_FieldInfo = 
{
	"ArrayOfObject"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____ArrayOfString_19_FieldInfo = 
{
	"ArrayOfString"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____Method_20_FieldInfo = 
{
	"Method"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648_____Unknown4_21_FieldInfo = 
{
	"_Unknown4"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648_____Unknown5_22_FieldInfo = 
{
	"_Unknown5"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____MethodCall_23_FieldInfo = 
{
	"MethodCall"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType BinaryElement_t2648_0_0_32854;
FieldInfo BinaryElement_t2648____MethodResponse_24_FieldInfo = 
{
	"MethodResponse"/* name */
	, &BinaryElement_t2648_0_0_32854/* type */
	, &BinaryElement_t2648_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* BinaryElement_t2648_FieldInfos[] =
{
	&BinaryElement_t2648____value___1_FieldInfo,
	&BinaryElement_t2648____Header_2_FieldInfo,
	&BinaryElement_t2648____RefTypeObject_3_FieldInfo,
	&BinaryElement_t2648____UntypedRuntimeObject_4_FieldInfo,
	&BinaryElement_t2648____UntypedExternalObject_5_FieldInfo,
	&BinaryElement_t2648____RuntimeObject_6_FieldInfo,
	&BinaryElement_t2648____ExternalObject_7_FieldInfo,
	&BinaryElement_t2648____String_8_FieldInfo,
	&BinaryElement_t2648____GenericArray_9_FieldInfo,
	&BinaryElement_t2648____BoxedPrimitiveTypeValue_10_FieldInfo,
	&BinaryElement_t2648____ObjectReference_11_FieldInfo,
	&BinaryElement_t2648____NullValue_12_FieldInfo,
	&BinaryElement_t2648____End_13_FieldInfo,
	&BinaryElement_t2648____Assembly_14_FieldInfo,
	&BinaryElement_t2648____ArrayFiller8b_15_FieldInfo,
	&BinaryElement_t2648____ArrayFiller32b_16_FieldInfo,
	&BinaryElement_t2648____ArrayOfPrimitiveType_17_FieldInfo,
	&BinaryElement_t2648____ArrayOfObject_18_FieldInfo,
	&BinaryElement_t2648____ArrayOfString_19_FieldInfo,
	&BinaryElement_t2648____Method_20_FieldInfo,
	&BinaryElement_t2648_____Unknown4_21_FieldInfo,
	&BinaryElement_t2648_____Unknown5_22_FieldInfo,
	&BinaryElement_t2648____MethodCall_23_FieldInfo,
	&BinaryElement_t2648____MethodResponse_24_FieldInfo,
	NULL
};
static const uint8_t BinaryElement_t2648____Header_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____Header_2_DefaultValue = 
{
	&BinaryElement_t2648____Header_2_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____Header_2_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____RefTypeObject_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____RefTypeObject_3_DefaultValue = 
{
	&BinaryElement_t2648____RefTypeObject_3_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____RefTypeObject_3_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____UntypedRuntimeObject_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____UntypedRuntimeObject_4_DefaultValue = 
{
	&BinaryElement_t2648____UntypedRuntimeObject_4_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____UntypedRuntimeObject_4_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____UntypedExternalObject_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____UntypedExternalObject_5_DefaultValue = 
{
	&BinaryElement_t2648____UntypedExternalObject_5_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____UntypedExternalObject_5_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____RuntimeObject_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____RuntimeObject_6_DefaultValue = 
{
	&BinaryElement_t2648____RuntimeObject_6_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____RuntimeObject_6_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ExternalObject_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ExternalObject_7_DefaultValue = 
{
	&BinaryElement_t2648____ExternalObject_7_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ExternalObject_7_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____String_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____String_8_DefaultValue = 
{
	&BinaryElement_t2648____String_8_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____String_8_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____GenericArray_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____GenericArray_9_DefaultValue = 
{
	&BinaryElement_t2648____GenericArray_9_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____GenericArray_9_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____BoxedPrimitiveTypeValue_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____BoxedPrimitiveTypeValue_10_DefaultValue = 
{
	&BinaryElement_t2648____BoxedPrimitiveTypeValue_10_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____BoxedPrimitiveTypeValue_10_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ObjectReference_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ObjectReference_11_DefaultValue = 
{
	&BinaryElement_t2648____ObjectReference_11_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ObjectReference_11_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____NullValue_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____NullValue_12_DefaultValue = 
{
	&BinaryElement_t2648____NullValue_12_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____NullValue_12_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____End_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____End_13_DefaultValue = 
{
	&BinaryElement_t2648____End_13_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____End_13_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____Assembly_14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____Assembly_14_DefaultValue = 
{
	&BinaryElement_t2648____Assembly_14_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____Assembly_14_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ArrayFiller8b_15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ArrayFiller8b_15_DefaultValue = 
{
	&BinaryElement_t2648____ArrayFiller8b_15_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ArrayFiller8b_15_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ArrayFiller32b_16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ArrayFiller32b_16_DefaultValue = 
{
	&BinaryElement_t2648____ArrayFiller32b_16_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ArrayFiller32b_16_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ArrayOfPrimitiveType_17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ArrayOfPrimitiveType_17_DefaultValue = 
{
	&BinaryElement_t2648____ArrayOfPrimitiveType_17_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ArrayOfPrimitiveType_17_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ArrayOfObject_18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ArrayOfObject_18_DefaultValue = 
{
	&BinaryElement_t2648____ArrayOfObject_18_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ArrayOfObject_18_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____ArrayOfString_19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____ArrayOfString_19_DefaultValue = 
{
	&BinaryElement_t2648____ArrayOfString_19_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____ArrayOfString_19_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____Method_20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____Method_20_DefaultValue = 
{
	&BinaryElement_t2648____Method_20_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____Method_20_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648_____Unknown4_21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648_____Unknown4_21_DefaultValue = 
{
	&BinaryElement_t2648_____Unknown4_21_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648_____Unknown4_21_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648_____Unknown5_22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648_____Unknown5_22_DefaultValue = 
{
	&BinaryElement_t2648_____Unknown5_22_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648_____Unknown5_22_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____MethodCall_23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____MethodCall_23_DefaultValue = 
{
	&BinaryElement_t2648____MethodCall_23_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____MethodCall_23_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t BinaryElement_t2648____MethodResponse_24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry BinaryElement_t2648____MethodResponse_24_DefaultValue = 
{
	&BinaryElement_t2648____MethodResponse_24_FieldInfo/* field */
	, { (char*)&BinaryElement_t2648____MethodResponse_24_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* BinaryElement_t2648_FieldDefaultValues[] = 
{
	&BinaryElement_t2648____Header_2_DefaultValue,
	&BinaryElement_t2648____RefTypeObject_3_DefaultValue,
	&BinaryElement_t2648____UntypedRuntimeObject_4_DefaultValue,
	&BinaryElement_t2648____UntypedExternalObject_5_DefaultValue,
	&BinaryElement_t2648____RuntimeObject_6_DefaultValue,
	&BinaryElement_t2648____ExternalObject_7_DefaultValue,
	&BinaryElement_t2648____String_8_DefaultValue,
	&BinaryElement_t2648____GenericArray_9_DefaultValue,
	&BinaryElement_t2648____BoxedPrimitiveTypeValue_10_DefaultValue,
	&BinaryElement_t2648____ObjectReference_11_DefaultValue,
	&BinaryElement_t2648____NullValue_12_DefaultValue,
	&BinaryElement_t2648____End_13_DefaultValue,
	&BinaryElement_t2648____Assembly_14_DefaultValue,
	&BinaryElement_t2648____ArrayFiller8b_15_DefaultValue,
	&BinaryElement_t2648____ArrayFiller32b_16_DefaultValue,
	&BinaryElement_t2648____ArrayOfPrimitiveType_17_DefaultValue,
	&BinaryElement_t2648____ArrayOfObject_18_DefaultValue,
	&BinaryElement_t2648____ArrayOfString_19_DefaultValue,
	&BinaryElement_t2648____Method_20_DefaultValue,
	&BinaryElement_t2648_____Unknown4_21_DefaultValue,
	&BinaryElement_t2648_____Unknown5_22_DefaultValue,
	&BinaryElement_t2648____MethodCall_23_DefaultValue,
	&BinaryElement_t2648____MethodResponse_24_DefaultValue,
	NULL
};
static Il2CppMethodReference BinaryElement_t2648_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool BinaryElement_t2648_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BinaryElement_t2648_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryElement_t2648_1_0_0;
const Il2CppTypeDefinitionMetadata BinaryElement_t2648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t2648_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, BinaryElement_t2648_VTable/* vtableMethods */
	, BinaryElement_t2648_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BinaryElement_t2648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryElement_t2648_MethodInfos/* methods */
	, NULL/* properties */
	, BinaryElement_t2648_FieldInfos/* fields */
	, NULL/* events */
	, &Byte_t237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t2648_0_0_0/* byval_arg */
	, &BinaryElement_t2648_1_0_0/* this_arg */
	, &BinaryElement_t2648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, BinaryElement_t2648_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t2648)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t2648)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t2649_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static MethodInfo* TypeTag_t2649_MethodInfos[] =
{
	NULL
};
extern Il2CppType Byte_t237_0_0_1542;
FieldInfo TypeTag_t2649____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t237_0_0_1542/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, offsetof(TypeTag_t2649, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____PrimitiveType_2_FieldInfo = 
{
	"PrimitiveType"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____String_3_FieldInfo = 
{
	"String"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____ObjectType_4_FieldInfo = 
{
	"ObjectType"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____RuntimeType_5_FieldInfo = 
{
	"RuntimeType"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____GenericType_6_FieldInfo = 
{
	"GenericType"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____ArrayOfObject_7_FieldInfo = 
{
	"ArrayOfObject"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____ArrayOfString_8_FieldInfo = 
{
	"ArrayOfString"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeTag_t2649_0_0_32854;
FieldInfo TypeTag_t2649____ArrayOfPrimitiveType_9_FieldInfo = 
{
	"ArrayOfPrimitiveType"/* name */
	, &TypeTag_t2649_0_0_32854/* type */
	, &TypeTag_t2649_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeTag_t2649_FieldInfos[] =
{
	&TypeTag_t2649____value___1_FieldInfo,
	&TypeTag_t2649____PrimitiveType_2_FieldInfo,
	&TypeTag_t2649____String_3_FieldInfo,
	&TypeTag_t2649____ObjectType_4_FieldInfo,
	&TypeTag_t2649____RuntimeType_5_FieldInfo,
	&TypeTag_t2649____GenericType_6_FieldInfo,
	&TypeTag_t2649____ArrayOfObject_7_FieldInfo,
	&TypeTag_t2649____ArrayOfString_8_FieldInfo,
	&TypeTag_t2649____ArrayOfPrimitiveType_9_FieldInfo,
	NULL
};
static const uint8_t TypeTag_t2649____PrimitiveType_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____PrimitiveType_2_DefaultValue = 
{
	&TypeTag_t2649____PrimitiveType_2_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____PrimitiveType_2_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____String_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____String_3_DefaultValue = 
{
	&TypeTag_t2649____String_3_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____String_3_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____ObjectType_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____ObjectType_4_DefaultValue = 
{
	&TypeTag_t2649____ObjectType_4_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____ObjectType_4_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____RuntimeType_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____RuntimeType_5_DefaultValue = 
{
	&TypeTag_t2649____RuntimeType_5_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____RuntimeType_5_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____GenericType_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____GenericType_6_DefaultValue = 
{
	&TypeTag_t2649____GenericType_6_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____GenericType_6_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____ArrayOfObject_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____ArrayOfObject_7_DefaultValue = 
{
	&TypeTag_t2649____ArrayOfObject_7_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____ArrayOfObject_7_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____ArrayOfString_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____ArrayOfString_8_DefaultValue = 
{
	&TypeTag_t2649____ArrayOfString_8_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____ArrayOfString_8_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t TypeTag_t2649____ArrayOfPrimitiveType_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TypeTag_t2649____ArrayOfPrimitiveType_9_DefaultValue = 
{
	&TypeTag_t2649____ArrayOfPrimitiveType_9_FieldInfo/* field */
	, { (char*)&TypeTag_t2649____ArrayOfPrimitiveType_9_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TypeTag_t2649_FieldDefaultValues[] = 
{
	&TypeTag_t2649____PrimitiveType_2_DefaultValue,
	&TypeTag_t2649____String_3_DefaultValue,
	&TypeTag_t2649____ObjectType_4_DefaultValue,
	&TypeTag_t2649____RuntimeType_5_DefaultValue,
	&TypeTag_t2649____GenericType_6_DefaultValue,
	&TypeTag_t2649____ArrayOfObject_7_DefaultValue,
	&TypeTag_t2649____ArrayOfString_8_DefaultValue,
	&TypeTag_t2649____ArrayOfPrimitiveType_9_DefaultValue,
	NULL
};
static Il2CppMethodReference TypeTag_t2649_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TypeTag_t2649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeTag_t2649_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TypeTag_t2649_0_0_0;
extern Il2CppType TypeTag_t2649_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t2649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t2649_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TypeTag_t2649_VTable/* vtableMethods */
	, TypeTag_t2649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeTag_t2649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, TypeTag_t2649_MethodInfos/* methods */
	, NULL/* properties */
	, TypeTag_t2649_FieldInfos/* fields */
	, NULL/* events */
	, &Byte_t237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t2649_0_0_0/* byval_arg */
	, &TypeTag_t2649_1_0_0/* this_arg */
	, &TypeTag_t2649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TypeTag_t2649_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t2649)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t2649)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t2650_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static MethodInfo* MethodFlags_t2650_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo MethodFlags_t2650____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, offsetof(MethodFlags_t2650, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____NoArguments_2_FieldInfo = 
{
	"NoArguments"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____PrimitiveArguments_3_FieldInfo = 
{
	"PrimitiveArguments"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____ArgumentsInSimpleArray_4_FieldInfo = 
{
	"ArgumentsInSimpleArray"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____ArgumentsInMultiArray_5_FieldInfo = 
{
	"ArgumentsInMultiArray"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____ExcludeLogicalCallContext_6_FieldInfo = 
{
	"ExcludeLogicalCallContext"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____IncludesLogicalCallContext_7_FieldInfo = 
{
	"IncludesLogicalCallContext"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____IncludesSignature_8_FieldInfo = 
{
	"IncludesSignature"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____FormatMask_9_FieldInfo = 
{
	"FormatMask"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____GenericArguments_10_FieldInfo = 
{
	"GenericArguments"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MethodFlags_t2650_0_0_32854;
FieldInfo MethodFlags_t2650____NeedsInfoArrayMask_11_FieldInfo = 
{
	"NeedsInfoArrayMask"/* name */
	, &MethodFlags_t2650_0_0_32854/* type */
	, &MethodFlags_t2650_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* MethodFlags_t2650_FieldInfos[] =
{
	&MethodFlags_t2650____value___1_FieldInfo,
	&MethodFlags_t2650____NoArguments_2_FieldInfo,
	&MethodFlags_t2650____PrimitiveArguments_3_FieldInfo,
	&MethodFlags_t2650____ArgumentsInSimpleArray_4_FieldInfo,
	&MethodFlags_t2650____ArgumentsInMultiArray_5_FieldInfo,
	&MethodFlags_t2650____ExcludeLogicalCallContext_6_FieldInfo,
	&MethodFlags_t2650____IncludesLogicalCallContext_7_FieldInfo,
	&MethodFlags_t2650____IncludesSignature_8_FieldInfo,
	&MethodFlags_t2650____FormatMask_9_FieldInfo,
	&MethodFlags_t2650____GenericArguments_10_FieldInfo,
	&MethodFlags_t2650____NeedsInfoArrayMask_11_FieldInfo,
	NULL
};
static const int32_t MethodFlags_t2650____NoArguments_2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____NoArguments_2_DefaultValue = 
{
	&MethodFlags_t2650____NoArguments_2_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____NoArguments_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____PrimitiveArguments_3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____PrimitiveArguments_3_DefaultValue = 
{
	&MethodFlags_t2650____PrimitiveArguments_3_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____PrimitiveArguments_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____ArgumentsInSimpleArray_4_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____ArgumentsInSimpleArray_4_DefaultValue = 
{
	&MethodFlags_t2650____ArgumentsInSimpleArray_4_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____ArgumentsInSimpleArray_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____ArgumentsInMultiArray_5_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____ArgumentsInMultiArray_5_DefaultValue = 
{
	&MethodFlags_t2650____ArgumentsInMultiArray_5_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____ArgumentsInMultiArray_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____ExcludeLogicalCallContext_6_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____ExcludeLogicalCallContext_6_DefaultValue = 
{
	&MethodFlags_t2650____ExcludeLogicalCallContext_6_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____ExcludeLogicalCallContext_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____IncludesLogicalCallContext_7_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____IncludesLogicalCallContext_7_DefaultValue = 
{
	&MethodFlags_t2650____IncludesLogicalCallContext_7_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____IncludesLogicalCallContext_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____IncludesSignature_8_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____IncludesSignature_8_DefaultValue = 
{
	&MethodFlags_t2650____IncludesSignature_8_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____IncludesSignature_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____FormatMask_9_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____FormatMask_9_DefaultValue = 
{
	&MethodFlags_t2650____FormatMask_9_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____FormatMask_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____GenericArguments_10_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____GenericArguments_10_DefaultValue = 
{
	&MethodFlags_t2650____GenericArguments_10_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____GenericArguments_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t MethodFlags_t2650____NeedsInfoArrayMask_11_DefaultValueData = 32972;
static Il2CppFieldDefaultValueEntry MethodFlags_t2650____NeedsInfoArrayMask_11_DefaultValue = 
{
	&MethodFlags_t2650____NeedsInfoArrayMask_11_FieldInfo/* field */
	, { (char*)&MethodFlags_t2650____NeedsInfoArrayMask_11_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* MethodFlags_t2650_FieldDefaultValues[] = 
{
	&MethodFlags_t2650____NoArguments_2_DefaultValue,
	&MethodFlags_t2650____PrimitiveArguments_3_DefaultValue,
	&MethodFlags_t2650____ArgumentsInSimpleArray_4_DefaultValue,
	&MethodFlags_t2650____ArgumentsInMultiArray_5_DefaultValue,
	&MethodFlags_t2650____ExcludeLogicalCallContext_6_DefaultValue,
	&MethodFlags_t2650____IncludesLogicalCallContext_7_DefaultValue,
	&MethodFlags_t2650____IncludesSignature_8_DefaultValue,
	&MethodFlags_t2650____FormatMask_9_DefaultValue,
	&MethodFlags_t2650____GenericArguments_10_DefaultValue,
	&MethodFlags_t2650____NeedsInfoArrayMask_11_DefaultValue,
	NULL
};
static Il2CppMethodReference MethodFlags_t2650_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool MethodFlags_t2650_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodFlags_t2650_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MethodFlags_t2650_0_0_0;
extern Il2CppType MethodFlags_t2650_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t2650_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t2650_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, MethodFlags_t2650_VTable/* vtableMethods */
	, MethodFlags_t2650_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MethodFlags_t2650_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MethodFlags_t2650_MethodInfos/* methods */
	, NULL/* properties */
	, MethodFlags_t2650_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t2650_0_0_0/* byval_arg */
	, &MethodFlags_t2650_1_0_0/* this_arg */
	, &MethodFlags_t2650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, MethodFlags_t2650_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t2650)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t2650)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t2651_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static MethodInfo* ReturnTypeTag_t2651_MethodInfos[] =
{
	NULL
};
extern Il2CppType Byte_t237_0_0_1542;
FieldInfo ReturnTypeTag_t2651____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t237_0_0_1542/* type */
	, &ReturnTypeTag_t2651_il2cpp_TypeInfo/* parent */
	, offsetof(ReturnTypeTag_t2651, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ReturnTypeTag_t2651_0_0_32854;
FieldInfo ReturnTypeTag_t2651____Null_2_FieldInfo = 
{
	"Null"/* name */
	, &ReturnTypeTag_t2651_0_0_32854/* type */
	, &ReturnTypeTag_t2651_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ReturnTypeTag_t2651_0_0_32854;
FieldInfo ReturnTypeTag_t2651____PrimitiveType_3_FieldInfo = 
{
	"PrimitiveType"/* name */
	, &ReturnTypeTag_t2651_0_0_32854/* type */
	, &ReturnTypeTag_t2651_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ReturnTypeTag_t2651_0_0_32854;
FieldInfo ReturnTypeTag_t2651____ObjectType_4_FieldInfo = 
{
	"ObjectType"/* name */
	, &ReturnTypeTag_t2651_0_0_32854/* type */
	, &ReturnTypeTag_t2651_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ReturnTypeTag_t2651_0_0_32854;
FieldInfo ReturnTypeTag_t2651____Exception_5_FieldInfo = 
{
	"Exception"/* name */
	, &ReturnTypeTag_t2651_0_0_32854/* type */
	, &ReturnTypeTag_t2651_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ReturnTypeTag_t2651_FieldInfos[] =
{
	&ReturnTypeTag_t2651____value___1_FieldInfo,
	&ReturnTypeTag_t2651____Null_2_FieldInfo,
	&ReturnTypeTag_t2651____PrimitiveType_3_FieldInfo,
	&ReturnTypeTag_t2651____ObjectType_4_FieldInfo,
	&ReturnTypeTag_t2651____Exception_5_FieldInfo,
	NULL
};
static const uint8_t ReturnTypeTag_t2651____Null_2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry ReturnTypeTag_t2651____Null_2_DefaultValue = 
{
	&ReturnTypeTag_t2651____Null_2_FieldInfo/* field */
	, { (char*)&ReturnTypeTag_t2651____Null_2_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t ReturnTypeTag_t2651____PrimitiveType_3_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry ReturnTypeTag_t2651____PrimitiveType_3_DefaultValue = 
{
	&ReturnTypeTag_t2651____PrimitiveType_3_FieldInfo/* field */
	, { (char*)&ReturnTypeTag_t2651____PrimitiveType_3_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t ReturnTypeTag_t2651____ObjectType_4_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry ReturnTypeTag_t2651____ObjectType_4_DefaultValue = 
{
	&ReturnTypeTag_t2651____ObjectType_4_FieldInfo/* field */
	, { (char*)&ReturnTypeTag_t2651____ObjectType_4_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static const uint8_t ReturnTypeTag_t2651____Exception_5_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry ReturnTypeTag_t2651____Exception_5_DefaultValue = 
{
	&ReturnTypeTag_t2651____Exception_5_FieldInfo/* field */
	, { (char*)&ReturnTypeTag_t2651____Exception_5_DefaultValueData, &Byte_t237_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* ReturnTypeTag_t2651_FieldDefaultValues[] = 
{
	&ReturnTypeTag_t2651____Null_2_DefaultValue,
	&ReturnTypeTag_t2651____PrimitiveType_3_DefaultValue,
	&ReturnTypeTag_t2651____ObjectType_4_DefaultValue,
	&ReturnTypeTag_t2651____Exception_5_DefaultValue,
	NULL
};
static Il2CppMethodReference ReturnTypeTag_t2651_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool ReturnTypeTag_t2651_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t2651_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ReturnTypeTag_t2651_0_0_0;
extern Il2CppType ReturnTypeTag_t2651_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t2651_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t2651_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, ReturnTypeTag_t2651_VTable/* vtableMethods */
	, ReturnTypeTag_t2651_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ReturnTypeTag_t2651_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ReturnTypeTag_t2651_MethodInfos/* methods */
	, NULL/* properties */
	, ReturnTypeTag_t2651_FieldInfos/* fields */
	, NULL/* events */
	, &Byte_t237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t2651_0_0_0/* byval_arg */
	, &ReturnTypeTag_t2651_1_0_0/* this_arg */
	, &ReturnTypeTag_t2651_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, ReturnTypeTag_t2651_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t2651)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t2651)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t2640_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
MethodInfo BinaryFormatter__ctor_m12774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m12774/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo BinaryFormatter_t2640_BinaryFormatter__ctor_m12775_ParameterInfos[] = 
{
	{"selector", 0, 134222111, 0, &ISurrogateSelector_t2620_0_0_0},
	{"context", 1, 134222112, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
MethodInfo BinaryFormatter__ctor_m12775_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m12775/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674/* invoker_method */
	, BinaryFormatter_t2640_BinaryFormatter__ctor_m12775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_DefaultSurrogateSelector()
MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m12776_MethodInfo = 
{
	"get_DefaultSurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_DefaultSurrogateSelector_m12776/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t2620_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 561/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FormatterAssemblyStyle_t2659_0_0_0;
extern Il2CppType FormatterAssemblyStyle_t2659_0_0_0;
static ParameterInfo BinaryFormatter_t2640_BinaryFormatter_set_AssemblyFormat_m12777_ParameterInfos[] = 
{
	{"value", 0, 134222113, 0, &FormatterAssemblyStyle_t2659_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_AssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
MethodInfo BinaryFormatter_set_AssemblyFormat_m12777_MethodInfo = 
{
	"set_AssemblyFormat"/* name */
	, (methodPointerType)&BinaryFormatter_set_AssemblyFormat_m12777/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, BinaryFormatter_t2640_BinaryFormatter_set_AssemblyFormat_m12777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationBinder_t2652_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Binder()
MethodInfo BinaryFormatter_get_Binder_m12778_MethodInfo = 
{
	"get_Binder"/* name */
	, (methodPointerType)&BinaryFormatter_get_Binder_m12778/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &SerializationBinder_t2652_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StreamingContext_t1674_0_0_0;
extern void* RuntimeInvoker_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Context()
MethodInfo BinaryFormatter_get_Context_m12779_MethodInfo = 
{
	"get_Context"/* name */
	, (methodPointerType)&BinaryFormatter_get_Context_m12779/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &StreamingContext_t1674_0_0_0/* return_type */
	, RuntimeInvoker_StreamingContext_t1674/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_SurrogateSelector()
MethodInfo BinaryFormatter_get_SurrogateSelector_m12780_MethodInfo = 
{
	"get_SurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_SurrogateSelector_m12780/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t2620_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TypeFilterLevel_t2661_0_0_0;
extern void* RuntimeInvoker_TypeFilterLevel_t2661 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_FilterLevel()
MethodInfo BinaryFormatter_get_FilterLevel_m12781_MethodInfo = 
{
	"get_FilterLevel"/* name */
	, (methodPointerType)&BinaryFormatter_get_FilterLevel_m12781/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &TypeFilterLevel_t2661_0_0_0/* return_type */
	, RuntimeInvoker_TypeFilterLevel_t2661/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Stream_t1685_0_0_0;
extern Il2CppType Stream_t1685_0_0_0;
static ParameterInfo BinaryFormatter_t2640_BinaryFormatter_Deserialize_m12782_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222114, 0, &Stream_t1685_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
MethodInfo BinaryFormatter_Deserialize_m12782_MethodInfo = 
{
	"Deserialize"/* name */
	, (methodPointerType)&BinaryFormatter_Deserialize_m12782/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t2640_BinaryFormatter_Deserialize_m12782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Stream_t1685_0_0_0;
extern Il2CppType HeaderHandler_t2851_0_0_0;
extern Il2CppType HeaderHandler_t2851_0_0_0;
static ParameterInfo BinaryFormatter_t2640_BinaryFormatter_NoCheckDeserialize_m12783_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222115, 0, &Stream_t1685_0_0_0},
	{"handler", 1, 134222116, 0, &HeaderHandler_t2851_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
MethodInfo BinaryFormatter_NoCheckDeserialize_m12783_MethodInfo = 
{
	"NoCheckDeserialize"/* name */
	, (methodPointerType)&BinaryFormatter_NoCheckDeserialize_m12783/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t2640_BinaryFormatter_NoCheckDeserialize_m12783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_1_0_2;
extern Il2CppType Boolean_t203_1_0_0;
static ParameterInfo BinaryFormatter_t2640_BinaryFormatter_ReadBinaryHeader_m12784_ParameterInfos[] = 
{
	{"reader", 0, 134222117, 0, &BinaryReader_t2473_0_0_0},
	{"hasHeaders", 1, 134222118, 0, &Boolean_t203_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::ReadBinaryHeader(System.IO.BinaryReader,System.Boolean&)
MethodInfo BinaryFormatter_ReadBinaryHeader_m12784_MethodInfo = 
{
	"ReadBinaryHeader"/* name */
	, (methodPointerType)&BinaryFormatter_ReadBinaryHeader_m12784/* method */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_BooleanU26_t315/* invoker_method */
	, BinaryFormatter_t2640_BinaryFormatter_ReadBinaryHeader_m12784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BinaryFormatter_t2640_MethodInfos[] =
{
	&BinaryFormatter__ctor_m12774_MethodInfo,
	&BinaryFormatter__ctor_m12775_MethodInfo,
	&BinaryFormatter_get_DefaultSurrogateSelector_m12776_MethodInfo,
	&BinaryFormatter_set_AssemblyFormat_m12777_MethodInfo,
	&BinaryFormatter_get_Binder_m12778_MethodInfo,
	&BinaryFormatter_get_Context_m12779_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m12780_MethodInfo,
	&BinaryFormatter_get_FilterLevel_m12781_MethodInfo,
	&BinaryFormatter_Deserialize_m12782_MethodInfo,
	&BinaryFormatter_NoCheckDeserialize_m12783_MethodInfo,
	&BinaryFormatter_ReadBinaryHeader_m12784_MethodInfo,
	NULL
};
extern Il2CppType FormatterAssemblyStyle_t2659_0_0_1;
FieldInfo BinaryFormatter_t2640____assembly_format_0_FieldInfo = 
{
	"assembly_format"/* name */
	, &FormatterAssemblyStyle_t2659_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___assembly_format_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SerializationBinder_t2652_0_0_1;
FieldInfo BinaryFormatter_t2640____binder_1_FieldInfo = 
{
	"binder"/* name */
	, &SerializationBinder_t2652_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___binder_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StreamingContext_t1674_0_0_1;
FieldInfo BinaryFormatter_t2640____context_2_FieldInfo = 
{
	"context"/* name */
	, &StreamingContext_t1674_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___context_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_1;
FieldInfo BinaryFormatter_t2640____surrogate_selector_3_FieldInfo = 
{
	"surrogate_selector"/* name */
	, &ISurrogateSelector_t2620_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___surrogate_selector_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterTypeStyle_t2660_0_0_1;
FieldInfo BinaryFormatter_t2640____type_format_4_FieldInfo = 
{
	"type_format"/* name */
	, &FormatterTypeStyle_t2660_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___type_format_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeFilterLevel_t2661_0_0_1;
FieldInfo BinaryFormatter_t2640____filter_level_5_FieldInfo = 
{
	"filter_level"/* name */
	, &TypeFilterLevel_t2661_0_0_1/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640, ___filter_level_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ISurrogateSelector_t2620_0_0_17;
FieldInfo BinaryFormatter_t2640____U3CDefaultSurrogateSelectorU3Ek__BackingField_6_FieldInfo = 
{
	"<DefaultSurrogateSelector>k__BackingField"/* name */
	, &ISurrogateSelector_t2620_0_0_17/* type */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, offsetof(BinaryFormatter_t2640_StaticFields, ___U3CDefaultSurrogateSelectorU3Ek__BackingField_6)/* offset */
	, 560/* custom_attributes_cache */

};
static FieldInfo* BinaryFormatter_t2640_FieldInfos[] =
{
	&BinaryFormatter_t2640____assembly_format_0_FieldInfo,
	&BinaryFormatter_t2640____binder_1_FieldInfo,
	&BinaryFormatter_t2640____context_2_FieldInfo,
	&BinaryFormatter_t2640____surrogate_selector_3_FieldInfo,
	&BinaryFormatter_t2640____type_format_4_FieldInfo,
	&BinaryFormatter_t2640____filter_level_5_FieldInfo,
	&BinaryFormatter_t2640____U3CDefaultSurrogateSelectorU3Ek__BackingField_6_FieldInfo,
	NULL
};
extern MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m12776_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____DefaultSurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "DefaultSurrogateSelector"/* name */
	, &BinaryFormatter_get_DefaultSurrogateSelector_m12776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo BinaryFormatter_set_AssemblyFormat_m12777_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____AssemblyFormat_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "AssemblyFormat"/* name */
	, NULL/* get */
	, &BinaryFormatter_set_AssemblyFormat_m12777_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo BinaryFormatter_get_Binder_m12778_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____Binder_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "Binder"/* name */
	, &BinaryFormatter_get_Binder_m12778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo BinaryFormatter_get_Context_m12779_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____Context_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "Context"/* name */
	, &BinaryFormatter_get_Context_m12779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo BinaryFormatter_get_SurrogateSelector_m12780_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____SurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "SurrogateSelector"/* name */
	, &BinaryFormatter_get_SurrogateSelector_m12780_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo BinaryFormatter_get_FilterLevel_m12781_MethodInfo;
static PropertyInfo BinaryFormatter_t2640____FilterLevel_PropertyInfo = 
{
	&BinaryFormatter_t2640_il2cpp_TypeInfo/* parent */
	, "FilterLevel"/* name */
	, &BinaryFormatter_get_FilterLevel_m12781_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* BinaryFormatter_t2640_PropertyInfos[] =
{
	&BinaryFormatter_t2640____DefaultSurrogateSelector_PropertyInfo,
	&BinaryFormatter_t2640____AssemblyFormat_PropertyInfo,
	&BinaryFormatter_t2640____Binder_PropertyInfo,
	&BinaryFormatter_t2640____Context_PropertyInfo,
	&BinaryFormatter_t2640____SurrogateSelector_PropertyInfo,
	&BinaryFormatter_t2640____FilterLevel_PropertyInfo,
	NULL
};
extern MethodInfo BinaryFormatter_Deserialize_m12782_MethodInfo;
static Il2CppMethodReference BinaryFormatter_t2640_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&BinaryFormatter_get_Binder_m12778_MethodInfo,
	&BinaryFormatter_get_Context_m12779_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m12780_MethodInfo,
	&BinaryFormatter_Deserialize_m12782_MethodInfo,
};
static bool BinaryFormatter_t2640_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* BinaryFormatter_t2640_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t2984_0_0_0,
	&IFormatter_t2986_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t2640_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t2984_0_0_0, 4},
	{ &IFormatter_t2986_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType BinaryFormatter_t2640_0_0_0;
extern Il2CppType BinaryFormatter_t2640_1_0_0;
struct BinaryFormatter_t2640;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t2640_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t2640_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t2640_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t2640_VTable/* vtableMethods */
	, BinaryFormatter_t2640_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo BinaryFormatter_t2640_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryFormatter_t2640_MethodInfos/* methods */
	, BinaryFormatter_t2640_PropertyInfos/* properties */
	, BinaryFormatter_t2640_FieldInfos/* fields */
	, NULL/* events */
	, &BinaryFormatter_t2640_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 559/* custom_attributes_cache */
	, &BinaryFormatter_t2640_0_0_0/* byval_arg */
	, &BinaryFormatter_t2640_1_0_0/* this_arg */
	, &BinaryFormatter_t2640_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t2640)/* instance_size */
	, sizeof (BinaryFormatter_t2640)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t2640_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t2653_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType HeaderHandler_t2851_0_0_0;
extern Il2CppType BinaryFormatter_t2640_0_0_0;
static ParameterInfo MessageFormatter_t2653_MessageFormatter_ReadMethodCall_m12785_ParameterInfos[] = 
{
	{"elem", 0, 134222119, 0, &BinaryElement_t2648_0_0_0},
	{"reader", 1, 134222120, 0, &BinaryReader_t2473_0_0_0},
	{"hasHeaders", 2, 134222121, 0, &Boolean_t203_0_0_0},
	{"headerHandler", 3, 134222122, 0, &HeaderHandler_t2851_0_0_0},
	{"formatter", 4, 134222123, 0, &BinaryFormatter_t2640_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
MethodInfo MessageFormatter_ReadMethodCall_m12785_MethodInfo = 
{
	"ReadMethodCall"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodCall_m12785/* method */
	, &MessageFormatter_t2653_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2653_MessageFormatter_ReadMethodCall_m12785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType HeaderHandler_t2851_0_0_0;
extern Il2CppType IMethodCallMessage_t2888_0_0_0;
extern Il2CppType BinaryFormatter_t2640_0_0_0;
static ParameterInfo MessageFormatter_t2653_MessageFormatter_ReadMethodResponse_m12786_ParameterInfos[] = 
{
	{"elem", 0, 134222124, 0, &BinaryElement_t2648_0_0_0},
	{"reader", 1, 134222125, 0, &BinaryReader_t2473_0_0_0},
	{"hasHeaders", 2, 134222126, 0, &Boolean_t203_0_0_0},
	{"headerHandler", 3, 134222127, 0, &HeaderHandler_t2851_0_0_0},
	{"methodCallMessage", 4, 134222128, 0, &IMethodCallMessage_t2888_0_0_0},
	{"formatter", 5, 134222129, 0, &BinaryFormatter_t2640_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
MethodInfo MessageFormatter_ReadMethodResponse_m12786_MethodInfo = 
{
	"ReadMethodResponse"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodResponse_m12786/* method */
	, &MessageFormatter_t2653_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t2653_MessageFormatter_ReadMethodResponse_m12786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MessageFormatter_t2653_MethodInfos[] =
{
	&MessageFormatter_ReadMethodCall_m12785_MethodInfo,
	&MessageFormatter_ReadMethodResponse_m12786_MethodInfo,
	NULL
};
static Il2CppMethodReference MessageFormatter_t2653_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool MessageFormatter_t2653_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType MessageFormatter_t2653_0_0_0;
extern Il2CppType MessageFormatter_t2653_1_0_0;
struct MessageFormatter_t2653;
const Il2CppTypeDefinitionMetadata MessageFormatter_t2653_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t2653_VTable/* vtableMethods */
	, MessageFormatter_t2653_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MessageFormatter_t2653_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MessageFormatter_t2653_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MessageFormatter_t2653_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t2653_0_0_0/* byval_arg */
	, &MessageFormatter_t2653_1_0_0/* this_arg */
	, &MessageFormatter_t2653_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t2653)/* instance_size */
	, sizeof (MessageFormatter_t2653)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t2655_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata::.ctor()
MethodInfo TypeMetadata__ctor_m12787_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeMetadata__ctor_m12787/* method */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeMetadata_t2655_MethodInfos[] =
{
	&TypeMetadata__ctor_m12787_MethodInfo,
	NULL
};
extern Il2CppType Type_t_0_0_6;
FieldInfo TypeMetadata_t2655____Type_0_FieldInfo = 
{
	"Type"/* name */
	, &Type_t_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___Type_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeU5BU5D_t1671_0_0_6;
FieldInfo TypeMetadata_t2655____MemberTypes_1_FieldInfo = 
{
	"MemberTypes"/* name */
	, &TypeU5BU5D_t1671_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___MemberTypes_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StringU5BU5D_t169_0_0_6;
FieldInfo TypeMetadata_t2655____MemberNames_2_FieldInfo = 
{
	"MemberNames"/* name */
	, &StringU5BU5D_t169_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___MemberNames_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType MemberInfoU5BU5D_t2654_0_0_6;
FieldInfo TypeMetadata_t2655____MemberInfos_3_FieldInfo = 
{
	"MemberInfos"/* name */
	, &MemberInfoU5BU5D_t2654_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___MemberInfos_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo TypeMetadata_t2655____FieldCount_4_FieldInfo = 
{
	"FieldCount"/* name */
	, &Int32_t189_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___FieldCount_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo TypeMetadata_t2655____NeedsSerializationInfo_5_FieldInfo = 
{
	"NeedsSerializationInfo"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* parent */
	, offsetof(TypeMetadata_t2655, ___NeedsSerializationInfo_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeMetadata_t2655_FieldInfos[] =
{
	&TypeMetadata_t2655____Type_0_FieldInfo,
	&TypeMetadata_t2655____MemberTypes_1_FieldInfo,
	&TypeMetadata_t2655____MemberNames_2_FieldInfo,
	&TypeMetadata_t2655____MemberInfos_3_FieldInfo,
	&TypeMetadata_t2655____FieldCount_4_FieldInfo,
	&TypeMetadata_t2655____NeedsSerializationInfo_5_FieldInfo,
	NULL
};
static Il2CppMethodReference TypeMetadata_t2655_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool TypeMetadata_t2655_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TypeMetadata_t2655_0_0_0;
extern Il2CppType TypeMetadata_t2655_1_0_0;
extern TypeInfo ObjectReader_t2658_il2cpp_TypeInfo;
extern Il2CppType ObjectReader_t2658_0_0_0;
struct TypeMetadata_t2655;
const Il2CppTypeDefinitionMetadata TypeMetadata_t2655_DefinitionMetadata = 
{
	&ObjectReader_t2658_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t2655_VTable/* vtableMethods */
	, TypeMetadata_t2655_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeMetadata_t2655_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, TypeMetadata_t2655_MethodInfos/* methods */
	, NULL/* properties */
	, TypeMetadata_t2655_FieldInfos/* fields */
	, NULL/* events */
	, &TypeMetadata_t2655_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t2655_0_0_0/* byval_arg */
	, &TypeMetadata_t2655_1_0_0/* this_arg */
	, &TypeMetadata_t2655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t2655)/* instance_size */
	, sizeof (TypeMetadata_t2655)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t2656_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ArrayNullFiller_t2656_ArrayNullFiller__ctor_m12788_ParameterInfos[] = 
{
	{"count", 0, 134222224, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
MethodInfo ArrayNullFiller__ctor_m12788_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArrayNullFiller__ctor_m12788/* method */
	, &ArrayNullFiller_t2656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int32_t189/* invoker_method */
	, ArrayNullFiller_t2656_ArrayNullFiller__ctor_m12788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ArrayNullFiller_t2656_MethodInfos[] =
{
	&ArrayNullFiller__ctor_m12788_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo ArrayNullFiller_t2656____NullCount_0_FieldInfo = 
{
	"NullCount"/* name */
	, &Int32_t189_0_0_6/* type */
	, &ArrayNullFiller_t2656_il2cpp_TypeInfo/* parent */
	, offsetof(ArrayNullFiller_t2656, ___NullCount_0)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ArrayNullFiller_t2656_FieldInfos[] =
{
	&ArrayNullFiller_t2656____NullCount_0_FieldInfo,
	NULL
};
static Il2CppMethodReference ArrayNullFiller_t2656_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ArrayNullFiller_t2656_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ArrayNullFiller_t2656_0_0_0;
extern Il2CppType ArrayNullFiller_t2656_1_0_0;
struct ArrayNullFiller_t2656;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t2656_DefinitionMetadata = 
{
	&ObjectReader_t2658_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t2656_VTable/* vtableMethods */
	, ArrayNullFiller_t2656_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ArrayNullFiller_t2656_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, ArrayNullFiller_t2656_MethodInfos/* methods */
	, NULL/* properties */
	, ArrayNullFiller_t2656_FieldInfos/* fields */
	, NULL/* events */
	, &ArrayNullFiller_t2656_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t2656_0_0_0/* byval_arg */
	, &ArrayNullFiller_t2656_1_0_0/* this_arg */
	, &ArrayNullFiller_t2656_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t2656)/* instance_size */
	, sizeof (ArrayNullFiller_t2656)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern Il2CppType BinaryFormatter_t2640_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader__ctor_m12789_ParameterInfos[] = 
{
	{"formatter", 0, 134222130, 0, &BinaryFormatter_t2640_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::.ctor(System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
MethodInfo ObjectReader__ctor_m12789_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectReader__ctor_m12789/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader__ctor_m12789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType HeaderU5BU5D_t2850_1_0_2;
extern Il2CppType HeaderU5BU5D_t2850_1_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadObjectGraph_m12790_ParameterInfos[] = 
{
	{"reader", 0, 134222131, 0, &BinaryReader_t2473_0_0_0},
	{"readHeaders", 1, 134222132, 0, &Boolean_t203_0_0_0},
	{"result", 2, 134222133, 0, &Object_t_1_0_2},
	{"headers", 3, 134222134, 0, &HeaderU5BU5D_t2850_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
MethodInfo ObjectReader_ReadObjectGraph_m12790_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m12790/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadObjectGraph_m12790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType HeaderU5BU5D_t2850_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadObjectGraph_m12791_ParameterInfos[] = 
{
	{"elem", 0, 134222135, 0, &BinaryElement_t2648_0_0_0},
	{"reader", 1, 134222136, 0, &BinaryReader_t2473_0_0_0},
	{"readHeaders", 2, 134222137, 0, &Boolean_t203_0_0_0},
	{"result", 3, 134222138, 0, &Object_t_1_0_2},
	{"headers", 4, 134222139, 0, &HeaderU5BU5D_t2850_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Byte_t237_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
MethodInfo ObjectReader_ReadObjectGraph_m12791_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m12791/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Byte_t237_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadObjectGraph_m12791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadNextObject_m12792_ParameterInfos[] = 
{
	{"element", 0, 134222140, 0, &BinaryElement_t2648_0_0_0},
	{"reader", 1, 134222141, 0, &BinaryReader_t2473_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader)
MethodInfo ObjectReader_ReadNextObject_m12792_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m12792/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Byte_t237_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadNextObject_m12792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadNextObject_m12793_ParameterInfos[] = 
{
	{"reader", 0, 134222142, 0, &BinaryReader_t2473_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.IO.BinaryReader)
MethodInfo ObjectReader_ReadNextObject_m12793_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m12793/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadNextObject_m12793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::get_CurrentObject()
MethodInfo ObjectReader_get_CurrentObject_m12794_MethodInfo = 
{
	"get_CurrentObject"/* name */
	, (methodPointerType)&ObjectReader_get_CurrentObject_m12794/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryElement_t2648_0_0_0;
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Int64_t233_1_0_0;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType SerializationInfo_t1673_1_0_2;
extern Il2CppType SerializationInfo_t1673_1_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadObject_m12795_ParameterInfos[] = 
{
	{"element", 0, 134222143, 0, &BinaryElement_t2648_0_0_0},
	{"reader", 1, 134222144, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 2, 134222145, 0, &Int64_t233_1_0_2},
	{"value", 3, 134222146, 0, &Object_t_1_0_2},
	{"info", 4, 134222147, 0, &SerializationInfo_t1673_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Byte_t237_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
MethodInfo ObjectReader_ReadObject_m12795_MethodInfo = 
{
	"ReadObject"/* name */
	, (methodPointerType)&ObjectReader_ReadObject_m12795/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Byte_t237_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadObject_m12795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadAssembly_m12796_ParameterInfos[] = 
{
	{"reader", 0, 134222148, 0, &BinaryReader_t2473_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadAssembly(System.IO.BinaryReader)
MethodInfo ObjectReader_ReadAssembly_m12796_MethodInfo = 
{
	"ReadAssembly"/* name */
	, (methodPointerType)&ObjectReader_ReadAssembly_m12796/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadAssembly_m12796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType SerializationInfo_t1673_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadObjectInstance_m12797_ParameterInfos[] = 
{
	{"reader", 0, 134222149, 0, &BinaryReader_t2473_0_0_0},
	{"isRuntimeObject", 1, 134222150, 0, &Boolean_t203_0_0_0},
	{"hasTypeInfo", 2, 134222151, 0, &Boolean_t203_0_0_0},
	{"objectId", 3, 134222152, 0, &Int64_t233_1_0_2},
	{"value", 4, 134222153, 0, &Object_t_1_0_2},
	{"info", 5, 134222154, 0, &SerializationInfo_t1673_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectInstance(System.IO.BinaryReader,System.Boolean,System.Boolean,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
MethodInfo ObjectReader_ReadObjectInstance_m12797_MethodInfo = 
{
	"ReadObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectInstance_m12797/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadObjectInstance_m12797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType SerializationInfo_t1673_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadRefTypeObjectInstance_m12798_ParameterInfos[] = 
{
	{"reader", 0, 134222155, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222156, 0, &Int64_t233_1_0_2},
	{"value", 2, 134222157, 0, &Object_t_1_0_2},
	{"info", 3, 134222158, 0, &SerializationInfo_t1673_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadRefTypeObjectInstance(System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
MethodInfo ObjectReader_ReadRefTypeObjectInstance_m12798_MethodInfo = 
{
	"ReadRefTypeObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadRefTypeObjectInstance_m12798/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadRefTypeObjectInstance_m12798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType TypeMetadata_t2655_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Object_t_1_0_2;
extern Il2CppType SerializationInfo_t1673_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadObjectContent_m12799_ParameterInfos[] = 
{
	{"reader", 0, 134222159, 0, &BinaryReader_t2473_0_0_0},
	{"metadata", 1, 134222160, 0, &TypeMetadata_t2655_0_0_0},
	{"objectId", 2, 134222161, 0, &Int64_t233_0_0_0},
	{"objectInstance", 3, 134222162, 0, &Object_t_1_0_2},
	{"info", 4, 134222163, 0, &SerializationInfo_t1673_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectContent(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata,System.Int64,System.Object&,System.Runtime.Serialization.SerializationInfo&)
MethodInfo ObjectReader_ReadObjectContent_m12799_MethodInfo = 
{
	"ReadObjectContent"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectContent_m12799/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_ObjectU26_t3349_SerializationInfoU26_t3363/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadObjectContent_m12799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType MemberInfo_t_0_0_0;
extern Il2CppType MemberInfo_t_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_RegisterObject_m12800_ParameterInfos[] = 
{
	{"objectId", 0, 134222164, 0, &Int64_t233_0_0_0},
	{"objectInstance", 1, 134222165, 0, &Object_t_0_0_0},
	{"info", 2, 134222166, 0, &SerializationInfo_t1673_0_0_0},
	{"parentObjectId", 3, 134222167, 0, &Int64_t233_0_0_0},
	{"parentObjectMemeber", 4, 134222168, 0, &MemberInfo_t_0_0_0},
	{"indices", 5, 134222169, 0, &Int32U5BU5D_t107_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RegisterObject(System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.Int64,System.Reflection.MemberInfo,System.Int32[])
MethodInfo ObjectReader_RegisterObject_m12800_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&ObjectReader_RegisterObject_m12800/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t_Int64_t233_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_RegisterObject_m12800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadStringIntance_m12801_ParameterInfos[] = 
{
	{"reader", 0, 134222170, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222171, 0, &Int64_t233_1_0_2},
	{"value", 2, 134222172, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadStringIntance(System.IO.BinaryReader,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadStringIntance_m12801_MethodInfo = 
{
	"ReadStringIntance"/* name */
	, (methodPointerType)&ObjectReader_ReadStringIntance_m12801/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadStringIntance_m12801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadGenericArray_m12802_ParameterInfos[] = 
{
	{"reader", 0, 134222173, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222174, 0, &Int64_t233_1_0_2},
	{"val", 2, 134222175, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadGenericArray(System.IO.BinaryReader,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadGenericArray_m12802_MethodInfo = 
{
	"ReadGenericArray"/* name */
	, (methodPointerType)&ObjectReader_ReadGenericArray_m12802/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadGenericArray_m12802_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadBoxedPrimitiveTypeValue_m12803_ParameterInfos[] = 
{
	{"reader", 0, 134222176, 0, &BinaryReader_t2473_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadBoxedPrimitiveTypeValue(System.IO.BinaryReader)
MethodInfo ObjectReader_ReadBoxedPrimitiveTypeValue_m12803_MethodInfo = 
{
	"ReadBoxedPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadBoxedPrimitiveTypeValue_m12803/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadBoxedPrimitiveTypeValue_m12803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadArrayOfPrimitiveType_m12804_ParameterInfos[] = 
{
	{"reader", 0, 134222177, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222178, 0, &Int64_t233_1_0_2},
	{"val", 2, 134222179, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfPrimitiveType(System.IO.BinaryReader,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadArrayOfPrimitiveType_m12804_MethodInfo = 
{
	"ReadArrayOfPrimitiveType"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfPrimitiveType_m12804/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadArrayOfPrimitiveType_m12804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t189_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_BlockRead_m12805_ParameterInfos[] = 
{
	{"reader", 0, 134222180, 0, &BinaryReader_t2473_0_0_0},
	{"array", 1, 134222181, 0, &Array_t_0_0_0},
	{"dataSize", 2, 134222182, 0, &Int32_t189_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::BlockRead(System.IO.BinaryReader,System.Array,System.Int32)
MethodInfo ObjectReader_BlockRead_m12805_MethodInfo = 
{
	"BlockRead"/* name */
	, (methodPointerType)&ObjectReader_BlockRead_m12805/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189/* invoker_method */
	, ObjectReader_t2658_ObjectReader_BlockRead_m12805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadArrayOfObject_m12806_ParameterInfos[] = 
{
	{"reader", 0, 134222183, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222184, 0, &Int64_t233_1_0_2},
	{"array", 2, 134222185, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfObject(System.IO.BinaryReader,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadArrayOfObject_m12806_MethodInfo = 
{
	"ReadArrayOfObject"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfObject_m12806/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadArrayOfObject_m12806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadArrayOfString_m12807_ParameterInfos[] = 
{
	{"reader", 0, 134222186, 0, &BinaryReader_t2473_0_0_0},
	{"objectId", 1, 134222187, 0, &Int64_t233_1_0_2},
	{"array", 2, 134222188, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfString(System.IO.BinaryReader,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadArrayOfString_m12807_MethodInfo = 
{
	"ReadArrayOfString"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfString_m12807/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadArrayOfString_m12807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Int64_t233_1_0_2;
extern Il2CppType Object_t_1_0_2;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadSimpleArray_m12808_ParameterInfos[] = 
{
	{"reader", 0, 134222189, 0, &BinaryReader_t2473_0_0_0},
	{"elementType", 1, 134222190, 0, &Type_t_0_0_0},
	{"objectId", 2, 134222191, 0, &Int64_t233_1_0_2},
	{"val", 3, 134222192, 0, &Object_t_1_0_2},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadSimpleArray(System.IO.BinaryReader,System.Type,System.Int64&,System.Object&)
MethodInfo ObjectReader_ReadSimpleArray_m12808_MethodInfo = 
{
	"ReadSimpleArray"/* name */
	, (methodPointerType)&ObjectReader_ReadSimpleArray_m12808/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int64U26_t3001_ObjectU26_t3349/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadSimpleArray_m12808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadTypeMetadata_m12809_ParameterInfos[] = 
{
	{"reader", 0, 134222193, 0, &BinaryReader_t2473_0_0_0},
	{"isRuntimeObject", 1, 134222194, 0, &Boolean_t203_0_0_0},
	{"hasTypeInfo", 2, 134222195, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType TypeMetadata_t2655_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadTypeMetadata(System.IO.BinaryReader,System.Boolean,System.Boolean)
MethodInfo ObjectReader_ReadTypeMetadata_m12809_MethodInfo = 
{
	"ReadTypeMetadata"/* name */
	, (methodPointerType)&ObjectReader_ReadTypeMetadata_m12809/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &TypeMetadata_t2655_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadTypeMetadata_m12809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType MemberInfo_t_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadValue_m12810_ParameterInfos[] = 
{
	{"reader", 0, 134222196, 0, &BinaryReader_t2473_0_0_0},
	{"parentObject", 1, 134222197, 0, &Object_t_0_0_0},
	{"parentObjectId", 2, 134222198, 0, &Int64_t233_0_0_0},
	{"info", 3, 134222199, 0, &SerializationInfo_t1673_0_0_0},
	{"valueType", 4, 134222200, 0, &Type_t_0_0_0},
	{"fieldName", 5, 134222201, 0, &String_t_0_0_0},
	{"memberInfo", 6, 134222202, 0, &MemberInfo_t_0_0_0},
	{"indices", 7, 134222203, 0, &Int32U5BU5D_t107_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadValue(System.IO.BinaryReader,System.Object,System.Int64,System.Runtime.Serialization.SerializationInfo,System.Type,System.String,System.Reflection.MemberInfo,System.Int32[])
MethodInfo ObjectReader_ReadValue_m12810_MethodInfo = 
{
	"ReadValue"/* name */
	, (methodPointerType)&ObjectReader_ReadValue_m12810/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadValue_m12810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType MemberInfo_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_SetObjectValue_m12811_ParameterInfos[] = 
{
	{"parentObject", 0, 134222204, 0, &Object_t_0_0_0},
	{"fieldName", 1, 134222205, 0, &String_t_0_0_0},
	{"memberInfo", 2, 134222206, 0, &MemberInfo_t_0_0_0},
	{"info", 3, 134222207, 0, &SerializationInfo_t1673_0_0_0},
	{"value", 4, 134222208, 0, &Object_t_0_0_0},
	{"valueType", 5, 134222209, 0, &Type_t_0_0_0},
	{"indices", 6, 134222210, 0, &Int32U5BU5D_t107_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::SetObjectValue(System.Object,System.String,System.Reflection.MemberInfo,System.Runtime.Serialization.SerializationInfo,System.Object,System.Type,System.Int32[])
MethodInfo ObjectReader_SetObjectValue_m12811_MethodInfo = 
{
	"SetObjectValue"/* name */
	, (methodPointerType)&ObjectReader_SetObjectValue_m12811/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_SetObjectValue_m12811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType MemberInfo_t_0_0_0;
extern Il2CppType Int32U5BU5D_t107_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_RecordFixup_m12812_ParameterInfos[] = 
{
	{"parentObjectId", 0, 134222211, 0, &Int64_t233_0_0_0},
	{"childObjectId", 1, 134222212, 0, &Int64_t233_0_0_0},
	{"parentObject", 2, 134222213, 0, &Object_t_0_0_0},
	{"info", 3, 134222214, 0, &SerializationInfo_t1673_0_0_0},
	{"fieldName", 4, 134222215, 0, &String_t_0_0_0},
	{"memberInfo", 5, 134222216, 0, &MemberInfo_t_0_0_0},
	{"indices", 6, 134222217, 0, &Int32U5BU5D_t107_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Int64_t233_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RecordFixup(System.Int64,System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.String,System.Reflection.MemberInfo,System.Int32[])
MethodInfo ObjectReader_RecordFixup_m12812_MethodInfo = 
{
	"RecordFixup"/* name */
	, (methodPointerType)&ObjectReader_RecordFixup_m12812/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Int64_t233_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_RecordFixup_m12812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int64_t233_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_GetDeserializationType_m12813_ParameterInfos[] = 
{
	{"assemblyId", 0, 134222218, 0, &Int64_t233_0_0_0},
	{"className", 1, 134222219, 0, &String_t_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::GetDeserializationType(System.Int64,System.String)
MethodInfo ObjectReader_GetDeserializationType_m12813_MethodInfo = 
{
	"GetDeserializationType"/* name */
	, (methodPointerType)&ObjectReader_GetDeserializationType_m12813/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t233_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_GetDeserializationType_m12813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType TypeTag_t2649_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadType_m12814_ParameterInfos[] = 
{
	{"reader", 0, 134222220, 0, &BinaryReader_t2473_0_0_0},
	{"code", 1, 134222221, 0, &TypeTag_t2649_0_0_0},
};
extern Il2CppType Type_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Byte_t237 (MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadType(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.TypeTag)
MethodInfo ObjectReader_ReadType_m12814_MethodInfo = 
{
	"ReadType"/* name */
	, (methodPointerType)&ObjectReader_ReadType_m12814/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Byte_t237/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadType_m12814_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BinaryReader_t2473_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo ObjectReader_t2658_ObjectReader_ReadPrimitiveTypeValue_m12815_ParameterInfos[] = 
{
	{"reader", 0, 134222222, 0, &BinaryReader_t2473_0_0_0},
	{"type", 1, 134222223, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadPrimitiveTypeValue(System.IO.BinaryReader,System.Type)
MethodInfo ObjectReader_ReadPrimitiveTypeValue_m12815_MethodInfo = 
{
	"ReadPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadPrimitiveTypeValue_m12815/* method */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t2658_ObjectReader_ReadPrimitiveTypeValue_m12815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObjectReader_t2658_MethodInfos[] =
{
	&ObjectReader__ctor_m12789_MethodInfo,
	&ObjectReader_ReadObjectGraph_m12790_MethodInfo,
	&ObjectReader_ReadObjectGraph_m12791_MethodInfo,
	&ObjectReader_ReadNextObject_m12792_MethodInfo,
	&ObjectReader_ReadNextObject_m12793_MethodInfo,
	&ObjectReader_get_CurrentObject_m12794_MethodInfo,
	&ObjectReader_ReadObject_m12795_MethodInfo,
	&ObjectReader_ReadAssembly_m12796_MethodInfo,
	&ObjectReader_ReadObjectInstance_m12797_MethodInfo,
	&ObjectReader_ReadRefTypeObjectInstance_m12798_MethodInfo,
	&ObjectReader_ReadObjectContent_m12799_MethodInfo,
	&ObjectReader_RegisterObject_m12800_MethodInfo,
	&ObjectReader_ReadStringIntance_m12801_MethodInfo,
	&ObjectReader_ReadGenericArray_m12802_MethodInfo,
	&ObjectReader_ReadBoxedPrimitiveTypeValue_m12803_MethodInfo,
	&ObjectReader_ReadArrayOfPrimitiveType_m12804_MethodInfo,
	&ObjectReader_BlockRead_m12805_MethodInfo,
	&ObjectReader_ReadArrayOfObject_m12806_MethodInfo,
	&ObjectReader_ReadArrayOfString_m12807_MethodInfo,
	&ObjectReader_ReadSimpleArray_m12808_MethodInfo,
	&ObjectReader_ReadTypeMetadata_m12809_MethodInfo,
	&ObjectReader_ReadValue_m12810_MethodInfo,
	&ObjectReader_SetObjectValue_m12811_MethodInfo,
	&ObjectReader_RecordFixup_m12812_MethodInfo,
	&ObjectReader_GetDeserializationType_m12813_MethodInfo,
	&ObjectReader_ReadType_m12814_MethodInfo,
	&ObjectReader_ReadPrimitiveTypeValue_m12815_MethodInfo,
	NULL
};
extern Il2CppType ISurrogateSelector_t2620_0_0_1;
FieldInfo ObjectReader_t2658_____surrogateSelector_0_FieldInfo = 
{
	"_surrogateSelector"/* name */
	, &ISurrogateSelector_t2620_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____surrogateSelector_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType StreamingContext_t1674_0_0_1;
FieldInfo ObjectReader_t2658_____context_1_FieldInfo = 
{
	"_context"/* name */
	, &StreamingContext_t1674_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____context_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SerializationBinder_t2652_0_0_1;
FieldInfo ObjectReader_t2658_____binder_2_FieldInfo = 
{
	"_binder"/* name */
	, &SerializationBinder_t2652_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____binder_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeFilterLevel_t2661_0_0_1;
FieldInfo ObjectReader_t2658_____filterLevel_3_FieldInfo = 
{
	"_filterLevel"/* name */
	, &TypeFilterLevel_t2661_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____filterLevel_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ObjectManager_t2657_0_0_1;
FieldInfo ObjectReader_t2658_____manager_4_FieldInfo = 
{
	"_manager"/* name */
	, &ObjectManager_t2657_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____manager_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo ObjectReader_t2658_____registeredAssemblies_5_FieldInfo = 
{
	"_registeredAssemblies"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____registeredAssemblies_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Hashtable_t1667_0_0_1;
FieldInfo ObjectReader_t2658_____typeMetadataCache_6_FieldInfo = 
{
	"_typeMetadataCache"/* name */
	, &Hashtable_t1667_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____typeMetadataCache_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo ObjectReader_t2658_____lastObject_7_FieldInfo = 
{
	"_lastObject"/* name */
	, &Object_t_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____lastObject_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int64_t233_0_0_1;
FieldInfo ObjectReader_t2658_____lastObjectID_8_FieldInfo = 
{
	"_lastObjectID"/* name */
	, &Int64_t233_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____lastObjectID_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int64_t233_0_0_1;
FieldInfo ObjectReader_t2658_____rootObjectID_9_FieldInfo = 
{
	"_rootObjectID"/* name */
	, &Int64_t233_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ____rootObjectID_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ByteU5BU5D_t350_0_0_1;
FieldInfo ObjectReader_t2658____arrayBuffer_10_FieldInfo = 
{
	"arrayBuffer"/* name */
	, &ByteU5BU5D_t350_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ___arrayBuffer_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo ObjectReader_t2658____ArrayBufferLength_11_FieldInfo = 
{
	"ArrayBufferLength"/* name */
	, &Int32_t189_0_0_1/* type */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectReader_t2658, ___ArrayBufferLength_11)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* ObjectReader_t2658_FieldInfos[] =
{
	&ObjectReader_t2658_____surrogateSelector_0_FieldInfo,
	&ObjectReader_t2658_____context_1_FieldInfo,
	&ObjectReader_t2658_____binder_2_FieldInfo,
	&ObjectReader_t2658_____filterLevel_3_FieldInfo,
	&ObjectReader_t2658_____manager_4_FieldInfo,
	&ObjectReader_t2658_____registeredAssemblies_5_FieldInfo,
	&ObjectReader_t2658_____typeMetadataCache_6_FieldInfo,
	&ObjectReader_t2658_____lastObject_7_FieldInfo,
	&ObjectReader_t2658_____lastObjectID_8_FieldInfo,
	&ObjectReader_t2658_____rootObjectID_9_FieldInfo,
	&ObjectReader_t2658____arrayBuffer_10_FieldInfo,
	&ObjectReader_t2658____ArrayBufferLength_11_FieldInfo,
	NULL
};
extern MethodInfo ObjectReader_get_CurrentObject_m12794_MethodInfo;
static PropertyInfo ObjectReader_t2658____CurrentObject_PropertyInfo = 
{
	&ObjectReader_t2658_il2cpp_TypeInfo/* parent */
	, "CurrentObject"/* name */
	, &ObjectReader_get_CurrentObject_m12794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* ObjectReader_t2658_PropertyInfos[] =
{
	&ObjectReader_t2658____CurrentObject_PropertyInfo,
	NULL
};
static const Il2CppType* ObjectReader_t2658_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t2655_0_0_0,
	&ArrayNullFiller_t2656_0_0_0,
};
static Il2CppMethodReference ObjectReader_t2658_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool ObjectReader_t2658_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ObjectReader_t2658_1_0_0;
struct ObjectReader_t2658;
const Il2CppTypeDefinitionMetadata ObjectReader_t2658_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t2658_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t2658_VTable/* vtableMethods */
	, ObjectReader_t2658_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ObjectReader_t2658_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ObjectReader_t2658_MethodInfos/* methods */
	, ObjectReader_t2658_PropertyInfos/* properties */
	, ObjectReader_t2658_FieldInfos/* fields */
	, NULL/* events */
	, &ObjectReader_t2658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t2658_0_0_0/* byval_arg */
	, &ObjectReader_t2658_1_0_0/* this_arg */
	, &ObjectReader_t2658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t2658)/* instance_size */
	, sizeof (ObjectReader_t2658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t2659_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static MethodInfo* FormatterAssemblyStyle_t2659_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo FormatterAssemblyStyle_t2659____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &FormatterAssemblyStyle_t2659_il2cpp_TypeInfo/* parent */
	, offsetof(FormatterAssemblyStyle_t2659, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterAssemblyStyle_t2659_0_0_32854;
FieldInfo FormatterAssemblyStyle_t2659____Simple_2_FieldInfo = 
{
	"Simple"/* name */
	, &FormatterAssemblyStyle_t2659_0_0_32854/* type */
	, &FormatterAssemblyStyle_t2659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterAssemblyStyle_t2659_0_0_32854;
FieldInfo FormatterAssemblyStyle_t2659____Full_3_FieldInfo = 
{
	"Full"/* name */
	, &FormatterAssemblyStyle_t2659_0_0_32854/* type */
	, &FormatterAssemblyStyle_t2659_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* FormatterAssemblyStyle_t2659_FieldInfos[] =
{
	&FormatterAssemblyStyle_t2659____value___1_FieldInfo,
	&FormatterAssemblyStyle_t2659____Simple_2_FieldInfo,
	&FormatterAssemblyStyle_t2659____Full_3_FieldInfo,
	NULL
};
static const int32_t FormatterAssemblyStyle_t2659____Simple_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry FormatterAssemblyStyle_t2659____Simple_2_DefaultValue = 
{
	&FormatterAssemblyStyle_t2659____Simple_2_FieldInfo/* field */
	, { (char*)&FormatterAssemblyStyle_t2659____Simple_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t FormatterAssemblyStyle_t2659____Full_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry FormatterAssemblyStyle_t2659____Full_3_DefaultValue = 
{
	&FormatterAssemblyStyle_t2659____Full_3_FieldInfo/* field */
	, { (char*)&FormatterAssemblyStyle_t2659____Full_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* FormatterAssemblyStyle_t2659_FieldDefaultValues[] = 
{
	&FormatterAssemblyStyle_t2659____Simple_2_DefaultValue,
	&FormatterAssemblyStyle_t2659____Full_3_DefaultValue,
	NULL
};
static Il2CppMethodReference FormatterAssemblyStyle_t2659_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool FormatterAssemblyStyle_t2659_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t2659_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType FormatterAssemblyStyle_t2659_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t2659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t2659_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, FormatterAssemblyStyle_t2659_VTable/* vtableMethods */
	, FormatterAssemblyStyle_t2659_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FormatterAssemblyStyle_t2659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterAssemblyStyle_t2659_MethodInfos/* methods */
	, NULL/* properties */
	, FormatterAssemblyStyle_t2659_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 562/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t2659_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t2659_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t2659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, FormatterAssemblyStyle_t2659_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t2659)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t2659)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t2660_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static MethodInfo* FormatterTypeStyle_t2660_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo FormatterTypeStyle_t2660____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &FormatterTypeStyle_t2660_il2cpp_TypeInfo/* parent */
	, offsetof(FormatterTypeStyle_t2660, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterTypeStyle_t2660_0_0_32854;
FieldInfo FormatterTypeStyle_t2660____TypesWhenNeeded_2_FieldInfo = 
{
	"TypesWhenNeeded"/* name */
	, &FormatterTypeStyle_t2660_0_0_32854/* type */
	, &FormatterTypeStyle_t2660_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterTypeStyle_t2660_0_0_32854;
FieldInfo FormatterTypeStyle_t2660____TypesAlways_3_FieldInfo = 
{
	"TypesAlways"/* name */
	, &FormatterTypeStyle_t2660_0_0_32854/* type */
	, &FormatterTypeStyle_t2660_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType FormatterTypeStyle_t2660_0_0_32854;
FieldInfo FormatterTypeStyle_t2660____XsdString_4_FieldInfo = 
{
	"XsdString"/* name */
	, &FormatterTypeStyle_t2660_0_0_32854/* type */
	, &FormatterTypeStyle_t2660_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* FormatterTypeStyle_t2660_FieldInfos[] =
{
	&FormatterTypeStyle_t2660____value___1_FieldInfo,
	&FormatterTypeStyle_t2660____TypesWhenNeeded_2_FieldInfo,
	&FormatterTypeStyle_t2660____TypesAlways_3_FieldInfo,
	&FormatterTypeStyle_t2660____XsdString_4_FieldInfo,
	NULL
};
static const int32_t FormatterTypeStyle_t2660____TypesWhenNeeded_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry FormatterTypeStyle_t2660____TypesWhenNeeded_2_DefaultValue = 
{
	&FormatterTypeStyle_t2660____TypesWhenNeeded_2_FieldInfo/* field */
	, { (char*)&FormatterTypeStyle_t2660____TypesWhenNeeded_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t FormatterTypeStyle_t2660____TypesAlways_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry FormatterTypeStyle_t2660____TypesAlways_3_DefaultValue = 
{
	&FormatterTypeStyle_t2660____TypesAlways_3_FieldInfo/* field */
	, { (char*)&FormatterTypeStyle_t2660____TypesAlways_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t FormatterTypeStyle_t2660____XsdString_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry FormatterTypeStyle_t2660____XsdString_4_DefaultValue = 
{
	&FormatterTypeStyle_t2660____XsdString_4_FieldInfo/* field */
	, { (char*)&FormatterTypeStyle_t2660____XsdString_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* FormatterTypeStyle_t2660_FieldDefaultValues[] = 
{
	&FormatterTypeStyle_t2660____TypesWhenNeeded_2_DefaultValue,
	&FormatterTypeStyle_t2660____TypesAlways_3_DefaultValue,
	&FormatterTypeStyle_t2660____XsdString_4_DefaultValue,
	NULL
};
static Il2CppMethodReference FormatterTypeStyle_t2660_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool FormatterTypeStyle_t2660_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t2660_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType FormatterTypeStyle_t2660_0_0_0;
extern Il2CppType FormatterTypeStyle_t2660_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t2660_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t2660_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, FormatterTypeStyle_t2660_VTable/* vtableMethods */
	, FormatterTypeStyle_t2660_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FormatterTypeStyle_t2660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterTypeStyle_t2660_MethodInfos/* methods */
	, NULL/* properties */
	, FormatterTypeStyle_t2660_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 563/* custom_attributes_cache */
	, &FormatterTypeStyle_t2660_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t2660_1_0_0/* this_arg */
	, &FormatterTypeStyle_t2660_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, FormatterTypeStyle_t2660_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t2660)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t2660)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t2661_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static MethodInfo* TypeFilterLevel_t2661_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo TypeFilterLevel_t2661____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &TypeFilterLevel_t2661_il2cpp_TypeInfo/* parent */
	, offsetof(TypeFilterLevel_t2661, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeFilterLevel_t2661_0_0_32854;
FieldInfo TypeFilterLevel_t2661____Low_2_FieldInfo = 
{
	"Low"/* name */
	, &TypeFilterLevel_t2661_0_0_32854/* type */
	, &TypeFilterLevel_t2661_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType TypeFilterLevel_t2661_0_0_32854;
FieldInfo TypeFilterLevel_t2661____Full_3_FieldInfo = 
{
	"Full"/* name */
	, &TypeFilterLevel_t2661_0_0_32854/* type */
	, &TypeFilterLevel_t2661_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* TypeFilterLevel_t2661_FieldInfos[] =
{
	&TypeFilterLevel_t2661____value___1_FieldInfo,
	&TypeFilterLevel_t2661____Low_2_FieldInfo,
	&TypeFilterLevel_t2661____Full_3_FieldInfo,
	NULL
};
static const int32_t TypeFilterLevel_t2661____Low_2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TypeFilterLevel_t2661____Low_2_DefaultValue = 
{
	&TypeFilterLevel_t2661____Low_2_FieldInfo/* field */
	, { (char*)&TypeFilterLevel_t2661____Low_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t TypeFilterLevel_t2661____Full_3_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TypeFilterLevel_t2661____Full_3_DefaultValue = 
{
	&TypeFilterLevel_t2661____Full_3_FieldInfo/* field */
	, { (char*)&TypeFilterLevel_t2661____Full_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TypeFilterLevel_t2661_FieldDefaultValues[] = 
{
	&TypeFilterLevel_t2661____Low_2_DefaultValue,
	&TypeFilterLevel_t2661____Full_3_DefaultValue,
	NULL
};
static Il2CppMethodReference TypeFilterLevel_t2661_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool TypeFilterLevel_t2661_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t2661_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType TypeFilterLevel_t2661_0_0_0;
extern Il2CppType TypeFilterLevel_t2661_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t2661_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t2661_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, TypeFilterLevel_t2661_VTable/* vtableMethods */
	, TypeFilterLevel_t2661_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo TypeFilterLevel_t2661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, TypeFilterLevel_t2661_MethodInfos/* methods */
	, NULL/* properties */
	, TypeFilterLevel_t2661_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 564/* custom_attributes_cache */
	, &TypeFilterLevel_t2661_0_0_0/* byval_arg */
	, &TypeFilterLevel_t2661_1_0_0/* this_arg */
	, &TypeFilterLevel_t2661_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TypeFilterLevel_t2661_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t2661)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t2661)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t2662_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
MethodInfo FormatterConverter__ctor_m12816_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatterConverter__ctor_m12816/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_Convert_m12817_ParameterInfos[] = 
{
	{"value", 0, 134222225, 0, &Object_t_0_0_0},
	{"type", 1, 134222226, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
MethodInfo FormatterConverter_Convert_m12817_MethodInfo = 
{
	"Convert"/* name */
	, (methodPointerType)&FormatterConverter_Convert_m12817/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_Convert_m12817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_ToBoolean_m12818_ParameterInfos[] = 
{
	{"value", 0, 134222227, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
MethodInfo FormatterConverter_ToBoolean_m12818_MethodInfo = 
{
	"ToBoolean"/* name */
	, (methodPointerType)&FormatterConverter_ToBoolean_m12818/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_ToBoolean_m12818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_ToInt16_m12819_ParameterInfos[] = 
{
	{"value", 0, 134222228, 0, &Object_t_0_0_0},
};
extern Il2CppType Int16_t238_0_0_0;
extern void* RuntimeInvoker_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
MethodInfo FormatterConverter_ToInt16_m12819_MethodInfo = 
{
	"ToInt16"/* name */
	, (methodPointerType)&FormatterConverter_ToInt16_m12819/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t238_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t238_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_ToInt16_m12819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_ToInt32_m12820_ParameterInfos[] = 
{
	{"value", 0, 134222229, 0, &Object_t_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
MethodInfo FormatterConverter_ToInt32_m12820_MethodInfo = 
{
	"ToInt32"/* name */
	, (methodPointerType)&FormatterConverter_ToInt32_m12820/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_ToInt32_m12820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_ToInt64_m12821_ParameterInfos[] = 
{
	{"value", 0, 134222230, 0, &Object_t_0_0_0},
};
extern Il2CppType Int64_t233_0_0_0;
extern void* RuntimeInvoker_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
MethodInfo FormatterConverter_ToInt64_m12821_MethodInfo = 
{
	"ToInt64"/* name */
	, (methodPointerType)&FormatterConverter_ToInt64_m12821/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t233_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t233_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_ToInt64_m12821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo FormatterConverter_t2662_FormatterConverter_ToString_m12822_ParameterInfos[] = 
{
	{"value", 0, 134222231, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
MethodInfo FormatterConverter_ToString_m12822_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&FormatterConverter_ToString_m12822/* method */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t2662_FormatterConverter_ToString_m12822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FormatterConverter_t2662_MethodInfos[] =
{
	&FormatterConverter__ctor_m12816_MethodInfo,
	&FormatterConverter_Convert_m12817_MethodInfo,
	&FormatterConverter_ToBoolean_m12818_MethodInfo,
	&FormatterConverter_ToInt16_m12819_MethodInfo,
	&FormatterConverter_ToInt32_m12820_MethodInfo,
	&FormatterConverter_ToInt64_m12821_MethodInfo,
	&FormatterConverter_ToString_m12822_MethodInfo,
	NULL
};
extern MethodInfo FormatterConverter_Convert_m12817_MethodInfo;
extern MethodInfo FormatterConverter_ToBoolean_m12818_MethodInfo;
extern MethodInfo FormatterConverter_ToInt16_m12819_MethodInfo;
extern MethodInfo FormatterConverter_ToInt32_m12820_MethodInfo;
extern MethodInfo FormatterConverter_ToInt64_m12821_MethodInfo;
extern MethodInfo FormatterConverter_ToString_m12822_MethodInfo;
static Il2CppMethodReference FormatterConverter_t2662_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&FormatterConverter_Convert_m12817_MethodInfo,
	&FormatterConverter_ToBoolean_m12818_MethodInfo,
	&FormatterConverter_ToInt16_m12819_MethodInfo,
	&FormatterConverter_ToInt32_m12820_MethodInfo,
	&FormatterConverter_ToInt64_m12821_MethodInfo,
	&FormatterConverter_ToString_m12822_MethodInfo,
};
static bool FormatterConverter_t2662_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormatterConverter_t2679_0_0_0;
static const Il2CppType* FormatterConverter_t2662_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t2679_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t2662_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t2679_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType FormatterConverter_t2662_0_0_0;
extern Il2CppType FormatterConverter_t2662_1_0_0;
struct FormatterConverter_t2662;
const Il2CppTypeDefinitionMetadata FormatterConverter_t2662_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t2662_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t2662_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t2662_VTable/* vtableMethods */
	, FormatterConverter_t2662_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FormatterConverter_t2662_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterConverter_t2662_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &FormatterConverter_t2662_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 565/* custom_attributes_cache */
	, &FormatterConverter_t2662_0_0_0/* byval_arg */
	, &FormatterConverter_t2662_1_0_0/* this_arg */
	, &FormatterConverter_t2662_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t2662)/* instance_size */
	, sizeof (FormatterConverter_t2662)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t2663_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
extern Il2CppType Type_t_0_0_0;
static ParameterInfo FormatterServices_t2663_FormatterServices_GetUninitializedObject_m12823_ParameterInfos[] = 
{
	{"type", 0, 134222232, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
MethodInfo FormatterServices_GetUninitializedObject_m12823_MethodInfo = 
{
	"GetUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetUninitializedObject_m12823/* method */
	, &FormatterServices_t2663_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2663_FormatterServices_GetUninitializedObject_m12823_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
static ParameterInfo FormatterServices_t2663_FormatterServices_GetSafeUninitializedObject_m12824_ParameterInfos[] = 
{
	{"type", 0, 134222233, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
MethodInfo FormatterServices_GetSafeUninitializedObject_m12824_MethodInfo = 
{
	"GetSafeUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetSafeUninitializedObject_m12824/* method */
	, &FormatterServices_t2663_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t2663_FormatterServices_GetSafeUninitializedObject_m12824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FormatterServices_t2663_MethodInfos[] =
{
	&FormatterServices_GetUninitializedObject_m12823_MethodInfo,
	&FormatterServices_GetSafeUninitializedObject_m12824_MethodInfo,
	NULL
};
static Il2CppMethodReference FormatterServices_t2663_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool FormatterServices_t2663_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType FormatterServices_t2663_0_0_0;
extern Il2CppType FormatterServices_t2663_1_0_0;
struct FormatterServices_t2663;
const Il2CppTypeDefinitionMetadata FormatterServices_t2663_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t2663_VTable/* vtableMethods */
	, FormatterServices_t2663_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo FormatterServices_t2663_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterServices_t2663_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &FormatterServices_t2663_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 566/* custom_attributes_cache */
	, &FormatterServices_t2663_0_0_0/* byval_arg */
	, &FormatterServices_t2663_1_0_0/* this_arg */
	, &FormatterServices_t2663_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t2663)/* instance_size */
	, sizeof (FormatterServices_t2663)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t1842_il2cpp_TypeInfo;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IDeserializationCallback_t1842_IDeserializationCallback_OnDeserialization_m14984_ParameterInfos[] = 
{
	{"sender", 0, 134222234, 0, &Object_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object)
MethodInfo IDeserializationCallback_OnDeserialization_m14984_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &IDeserializationCallback_t1842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, IDeserializationCallback_t1842_IDeserializationCallback_OnDeserialization_m14984_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IDeserializationCallback_t1842_MethodInfos[] =
{
	&IDeserializationCallback_OnDeserialization_m14984_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IDeserializationCallback_t1842_0_0_0;
extern Il2CppType IDeserializationCallback_t1842_1_0_0;
struct IDeserializationCallback_t1842;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t1842_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IDeserializationCallback_t1842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IDeserializationCallback_t1842_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IDeserializationCallback_t1842_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 567/* custom_attributes_cache */
	, &IDeserializationCallback_t1842_0_0_0/* byval_arg */
	, &IDeserializationCallback_t1842_1_0_0/* this_arg */
	, &IDeserializationCallback_t1842_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t2986_il2cpp_TypeInfo;
static MethodInfo* IFormatter_t2986_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IFormatter_t2986_1_0_0;
struct IFormatter_t2986;
const Il2CppTypeDefinitionMetadata IFormatter_t2986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IFormatter_t2986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatter_t2986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IFormatter_t2986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 568/* custom_attributes_cache */
	, &IFormatter_t2986_0_0_0/* byval_arg */
	, &IFormatter_t2986_1_0_0/* this_arg */
	, &IFormatter_t2986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t2679_il2cpp_TypeInfo;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_Convert_m14985_ParameterInfos[] = 
{
	{"value", 0, 134222235, 0, &Object_t_0_0_0},
	{"type", 1, 134222236, 0, &Type_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IFormatterConverter::Convert(System.Object,System.Type)
MethodInfo IFormatterConverter_Convert_m14985_MethodInfo = 
{
	"Convert"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_Convert_m14985_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_ToBoolean_m14986_ParameterInfos[] = 
{
	{"value", 0, 134222237, 0, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.IFormatterConverter::ToBoolean(System.Object)
MethodInfo IFormatterConverter_ToBoolean_m14986_MethodInfo = 
{
	"ToBoolean"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_ToBoolean_m14986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_ToInt16_m14987_ParameterInfos[] = 
{
	{"value", 0, 134222238, 0, &Object_t_0_0_0},
};
extern Il2CppType Int16_t238_0_0_0;
extern void* RuntimeInvoker_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.IFormatterConverter::ToInt16(System.Object)
MethodInfo IFormatterConverter_ToInt16_m14987_MethodInfo = 
{
	"ToInt16"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t238_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t238_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_ToInt16_m14987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_ToInt32_m14988_ParameterInfos[] = 
{
	{"value", 0, 134222239, 0, &Object_t_0_0_0},
};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.IFormatterConverter::ToInt32(System.Object)
MethodInfo IFormatterConverter_ToInt32_m14988_MethodInfo = 
{
	"ToInt32"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_ToInt32_m14988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_ToInt64_m14989_ParameterInfos[] = 
{
	{"value", 0, 134222240, 0, &Object_t_0_0_0},
};
extern Il2CppType Int64_t233_0_0_0;
extern void* RuntimeInvoker_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.IFormatterConverter::ToInt64(System.Object)
MethodInfo IFormatterConverter_ToInt64_m14989_MethodInfo = 
{
	"ToInt64"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t233_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t233_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_ToInt64_m14989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IFormatterConverter_t2679_IFormatterConverter_ToString_m14990_ParameterInfos[] = 
{
	{"value", 0, 134222241, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.IFormatterConverter::ToString(System.Object)
MethodInfo IFormatterConverter_ToString_m14990_MethodInfo = 
{
	"ToString"/* name */
	, NULL/* method */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t2679_IFormatterConverter_ToString_m14990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IFormatterConverter_t2679_MethodInfos[] =
{
	&IFormatterConverter_Convert_m14985_MethodInfo,
	&IFormatterConverter_ToBoolean_m14986_MethodInfo,
	&IFormatterConverter_ToInt16_m14987_MethodInfo,
	&IFormatterConverter_ToInt32_m14988_MethodInfo,
	&IFormatterConverter_ToInt64_m14989_MethodInfo,
	&IFormatterConverter_ToString_m14990_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IFormatterConverter_t2679_1_0_0;
struct IFormatterConverter_t2679;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t2679_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IFormatterConverter_t2679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatterConverter_t2679_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IFormatterConverter_t2679_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 569/* custom_attributes_cache */
	, &IFormatterConverter_t2679_0_0_0/* byval_arg */
	, &IFormatterConverter_t2679_1_0_0/* this_arg */
	, &IFormatterConverter_t2679_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t2906_il2cpp_TypeInfo;
extern Il2CppType StreamingContext_t1674_0_0_0;
static ParameterInfo IObjectReference_t2906_IObjectReference_GetRealObject_m14991_ParameterInfos[] = 
{
	{"context", 0, 134222242, 0, &StreamingContext_t1674_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IObjectReference::GetRealObject(System.Runtime.Serialization.StreamingContext)
MethodInfo IObjectReference_GetRealObject_m14991_MethodInfo = 
{
	"GetRealObject"/* name */
	, NULL/* method */
	, &IObjectReference_t2906_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1674/* invoker_method */
	, IObjectReference_t2906_IObjectReference_GetRealObject_m14991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IObjectReference_t2906_MethodInfos[] =
{
	&IObjectReference_GetRealObject_m14991_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IObjectReference_t2906_1_0_0;
struct IObjectReference_t2906;
const Il2CppTypeDefinitionMetadata IObjectReference_t2906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo IObjectReference_t2906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IObjectReference_t2906_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &IObjectReference_t2906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 570/* custom_attributes_cache */
	, &IObjectReference_t2906_0_0_0/* byval_arg */
	, &IObjectReference_t2906_1_0_0/* this_arg */
	, &IObjectReference_t2906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializationSurrogate
extern TypeInfo ISerializationSurrogate_t2671_il2cpp_TypeInfo;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType SerializationInfo_t1673_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_0_0_0;
static ParameterInfo ISerializationSurrogate_t2671_ISerializationSurrogate_SetObjectData_m14992_ParameterInfos[] = 
{
	{"obj", 0, 134222243, 0, &Object_t_0_0_0},
	{"info", 1, 134222244, 0, &SerializationInfo_t1673_0_0_0},
	{"context", 2, 134222245, 0, &StreamingContext_t1674_0_0_0},
	{"selector", 3, 134222246, 0, &ISurrogateSelector_t2620_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.ISerializationSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
MethodInfo ISerializationSurrogate_SetObjectData_m14992_MethodInfo = 
{
	"SetObjectData"/* name */
	, NULL/* method */
	, &ISerializationSurrogate_t2671_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t/* invoker_method */
	, ISerializationSurrogate_t2671_ISerializationSurrogate_SetObjectData_m14992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ISerializationSurrogate_t2671_MethodInfos[] =
{
	&ISerializationSurrogate_SetObjectData_m14992_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ISerializationSurrogate_t2671_1_0_0;
struct ISerializationSurrogate_t2671;
const Il2CppTypeDefinitionMetadata ISerializationSurrogate_t2671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ISerializationSurrogate_t2671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationSurrogate"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ISerializationSurrogate_t2671_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ISerializationSurrogate_t2671_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 571/* custom_attributes_cache */
	, &ISerializationSurrogate_t2671_0_0_0/* byval_arg */
	, &ISerializationSurrogate_t2671_1_0_0/* this_arg */
	, &ISerializationSurrogate_t2671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISurrogateSelector
extern TypeInfo ISurrogateSelector_t2620_il2cpp_TypeInfo;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType StreamingContext_t1674_0_0_0;
extern Il2CppType ISurrogateSelector_t2620_1_0_2;
static ParameterInfo ISurrogateSelector_t2620_ISurrogateSelector_GetSurrogate_m14993_ParameterInfos[] = 
{
	{"type", 0, 134222247, 0, &Type_t_0_0_0},
	{"context", 1, 134222248, 0, &StreamingContext_t1674_0_0_0},
	{"selector", 2, 134222249, 0, &ISurrogateSelector_t2620_1_0_2},
};
extern Il2CppType ISerializationSurrogate_t2671_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1674_ISurrogateSelectorU26_t3361 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Serialization.ISurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
MethodInfo ISurrogateSelector_GetSurrogate_m14993_MethodInfo = 
{
	"GetSurrogate"/* name */
	, NULL/* method */
	, &ISurrogateSelector_t2620_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t2671_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t1674_ISurrogateSelectorU26_t3361/* invoker_method */
	, ISurrogateSelector_t2620_ISurrogateSelector_GetSurrogate_m14993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ISurrogateSelector_t2620_MethodInfos[] =
{
	&ISurrogateSelector_GetSurrogate_m14993_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
struct ISurrogateSelector_t2620;
const Il2CppTypeDefinitionMetadata ISurrogateSelector_t2620_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ISurrogateSelector_t2620_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISurrogateSelector"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, ISurrogateSelector_t2620_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ISurrogateSelector_t2620_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 572/* custom_attributes_cache */
	, &ISurrogateSelector_t2620_0_0_0/* byval_arg */
	, &ISurrogateSelector_t2620_1_0_0/* this_arg */
	, &ISurrogateSelector_t2620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
