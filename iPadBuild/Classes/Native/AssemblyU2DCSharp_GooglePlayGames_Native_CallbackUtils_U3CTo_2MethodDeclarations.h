﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey17_2_t3709;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19615_gshared (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3709 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19615(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3709 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey17_2__ctor_m19615_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2/<ToOnGameThread>c__AnonStorey17`2<System.Object,System.Object>::<>m__B()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19616_gshared (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3709 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19616(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey17_2_t3709 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey17_2_U3CU3Em__B_m19616_gshared)(__this, method)
