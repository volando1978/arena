﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53
struct U3CTakeTurnU3Ec__AnonStorey53_t640;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::.ctor()
extern "C" void U3CTakeTurnU3Ec__AnonStorey53__ctor_m2554 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::<>m__5D(GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__5D_m2555 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, MultiplayerParticipant_t672 * ___pendingParticipant, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<TakeTurn>c__AnonStorey53::<>m__68(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CTakeTurnU3Ec__AnonStorey53_U3CU3Em__68_m2556 (U3CTakeTurnU3Ec__AnonStorey53_t640 * __this, TurnBasedMatchResponse_t707 * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
