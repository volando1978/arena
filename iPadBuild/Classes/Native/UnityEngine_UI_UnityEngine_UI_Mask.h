﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t989;
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// UnityEngine.RectTransform
struct RectTransform_t1227;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.Mask
struct  Mask_t1329  : public UIBehaviour_t1156
{
	// System.Boolean UnityEngine.UI.Mask::m_ShowMaskGraphic
	bool ___m_ShowMaskGraphic_2;
	// UnityEngine.Material UnityEngine.UI.Mask::m_RenderMaterial
	Material_t989 * ___m_RenderMaterial_3;
	// UnityEngine.UI.Graphic UnityEngine.UI.Mask::m_Graphic
	Graphic_t1233 * ___m_Graphic_4;
	// UnityEngine.RectTransform UnityEngine.UI.Mask::m_RectTransform
	RectTransform_t1227 * ___m_RectTransform_5;
};
