﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.ParticipantResults
struct ParticipantResults_t683;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.ParticipantResults::.ctor(System.IntPtr)
extern "C" void ParticipantResults__ctor_m2862 (ParticipantResults_t683 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.ParticipantResults::HasResultsForParticipant(System.String)
extern "C" bool ParticipantResults_HasResultsForParticipant_m2863 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.ParticipantResults::PlacingForParticipant(System.String)
extern "C" uint32_t ParticipantResults_PlacingForParticipant_m2864 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.PInvoke.ParticipantResults::ResultsForParticipant(System.String)
extern "C" int32_t ParticipantResults_ResultsForParticipant_m2865 (ParticipantResults_t683 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.ParticipantResults GooglePlayGames.Native.PInvoke.ParticipantResults::WithResult(System.String,System.UInt32,GooglePlayGames.Native.Cwrapper.Types/MatchResult)
extern "C" ParticipantResults_t683 * ParticipantResults_WithResult_m2866 (ParticipantResults_t683 * __this, String_t* ___participantId, uint32_t ___placing, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.ParticipantResults::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ParticipantResults_CallDispose_m2867 (ParticipantResults_t683 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
