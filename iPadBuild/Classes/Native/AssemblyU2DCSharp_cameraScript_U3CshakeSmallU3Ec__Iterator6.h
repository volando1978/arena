﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// cameraScript
struct cameraScript_t774;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// cameraScript/<shakeSmall>c__Iterator6
struct  U3CshakeSmallU3Ec__Iterator6_t776  : public Object_t
{
	// System.Single cameraScript/<shakeSmall>c__Iterator6::t
	float ___t_0;
	// UnityEngine.Vector3 cameraScript/<shakeSmall>c__Iterator6::<p>__0
	Vector3_t758  ___U3CpU3E__0_1;
	// UnityEngine.Vector3 cameraScript/<shakeSmall>c__Iterator6::<ip>__1
	Vector3_t758  ___U3CipU3E__1_2;
	// UnityEngine.Vector3 cameraScript/<shakeSmall>c__Iterator6::<ip2>__2
	Vector3_t758  ___U3Cip2U3E__2_3;
	// System.Int32 cameraScript/<shakeSmall>c__Iterator6::$PC
	int32_t ___U24PC_4;
	// System.Object cameraScript/<shakeSmall>c__Iterator6::$current
	Object_t * ___U24current_5;
	// System.Single cameraScript/<shakeSmall>c__Iterator6::<$>t
	float ___U3CU24U3Et_6;
	// cameraScript cameraScript/<shakeSmall>c__Iterator6::<>f__this
	cameraScript_t774 * ___U3CU3Ef__this_7;
};
