﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/Kernel
struct Kernel_t2388;
// Mono.Math.BigInteger
struct BigInteger_t2386;
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t2879;
// System.UInt32[]
struct UInt32U5BU5D_t2194;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"

// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::AddSameSign(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * Kernel_AddSameSign_m10866 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi1, BigInteger_t2386 * ___bi2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::Subtract(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * Kernel_Subtract_m10867 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___big, BigInteger_t2386 * ___small, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MinusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_MinusEq_m10868 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___big, BigInteger_t2386 * ___small, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::PlusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_PlusEq_m10869 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi1, BigInteger_t2386 * ___bi2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger/Sign Mono.Math.BigInteger/Kernel::Compare(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" int32_t Kernel_Compare_m10870 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi1, BigInteger_t2386 * ___bi2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::SingleByteDivideInPlace(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_SingleByteDivideInPlace_m10871 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___n, uint32_t ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::DwordMod(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_DwordMod_m10872 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___n, uint32_t ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::DwordDivMod(Mono.Math.BigInteger,System.UInt32)
extern "C" BigIntegerU5BU5D_t2879* Kernel_DwordDivMod_m10873 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___n, uint32_t ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::multiByteDivide(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigIntegerU5BU5D_t2879* Kernel_multiByteDivide_m10874 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi1, BigInteger_t2386 * ___bi2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::LeftShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t2386 * Kernel_LeftShift_m10875 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi, int32_t ___n, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::RightShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t2386 * Kernel_RightShift_m10876 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi, int32_t ___n, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::MultiplyByDword(Mono.Math.BigInteger,System.UInt32)
extern "C" BigInteger_t2386 * Kernel_MultiplyByDword_m10877 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___n, uint32_t ___f, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::Multiply(System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32)
extern "C" void Kernel_Multiply_m10878 (Object_t * __this /* static, unused */, UInt32U5BU5D_t2194* ___x, uint32_t ___xOffset, uint32_t ___xLen, UInt32U5BU5D_t2194* ___y, uint32_t ___yOffset, uint32_t ___yLen, UInt32U5BU5D_t2194* ___d, uint32_t ___dOffset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MultiplyMod2p32pmod(System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32)
extern "C" void Kernel_MultiplyMod2p32pmod_m10879 (Object_t * __this /* static, unused */, UInt32U5BU5D_t2194* ___x, int32_t ___xOffset, int32_t ___xLen, UInt32U5BU5D_t2194* ___y, int32_t ___yOffest, int32_t ___yLen, UInt32U5BU5D_t2194* ___d, int32_t ___dOffset, int32_t ___mod, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_modInverse_m10880 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi, uint32_t ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * Kernel_modInverse_m10881 (Object_t * __this /* static, unused */, BigInteger_t2386 * ___bi, BigInteger_t2386 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
