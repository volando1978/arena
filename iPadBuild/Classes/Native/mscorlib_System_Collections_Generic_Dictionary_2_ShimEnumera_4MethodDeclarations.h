﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>
struct ShimEnumerator_t4181;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t4170;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m25694_gshared (ShimEnumerator_t4181 * __this, Dictionary_2_t4170 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m25694(__this, ___host, method) (( void (*) (ShimEnumerator_t4181 *, Dictionary_2_t4170 *, MethodInfo*))ShimEnumerator__ctor_m25694_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m25695_gshared (ShimEnumerator_t4181 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m25695(__this, method) (( bool (*) (ShimEnumerator_t4181 *, MethodInfo*))ShimEnumerator_MoveNext_m25695_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m25696_gshared (ShimEnumerator_t4181 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m25696(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t4181 *, MethodInfo*))ShimEnumerator_get_Entry_m25696_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m25697_gshared (ShimEnumerator_t4181 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m25697(__this, method) (( Object_t * (*) (ShimEnumerator_t4181 *, MethodInfo*))ShimEnumerator_get_Key_m25697_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m25698_gshared (ShimEnumerator_t4181 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m25698(__this, method) (( Object_t * (*) (ShimEnumerator_t4181 *, MethodInfo*))ShimEnumerator_get_Value_m25698_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m25699_gshared (ShimEnumerator_t4181 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m25699(__this, method) (( Object_t * (*) (ShimEnumerator_t4181 *, MethodInfo*))ShimEnumerator_get_Current_m25699_gshared)(__this, method)
