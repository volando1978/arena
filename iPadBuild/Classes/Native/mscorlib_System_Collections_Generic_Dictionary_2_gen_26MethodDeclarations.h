﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t3789;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3792;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t3797;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3761;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4391;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t4392;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m20443_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2__ctor_m20443(__this, method) (( void (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2__ctor_m20443_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20445_gshared (Dictionary_2_t3789 * __this, Object_t* ___comparer, MethodInfo* method);
#define Dictionary_2__ctor_m20445(__this, ___comparer, method) (( void (*) (Dictionary_2_t3789 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m20445_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m20447_gshared (Dictionary_2_t3789 * __this, int32_t ___capacity, MethodInfo* method);
#define Dictionary_2__ctor_m20447(__this, ___capacity, method) (( void (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2__ctor_m20447_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m20449_gshared (Dictionary_2_t3789 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2__ctor_m20449(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3789 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m20449_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m20451_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m20451(__this, method) (( Object_t * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m20451_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m20453_gshared (Dictionary_2_t3789 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20453(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3789 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20453_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20455_gshared (Dictionary_2_t3789 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20455(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3789 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20455_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20457_gshared (Dictionary_2_t3789 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m20457(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3789 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20457_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20459_gshared (Dictionary_2_t3789 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m20459(__this, ___key, method) (( void (*) (Dictionary_2_t3789 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20459_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461(__this, method) (( bool (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20461_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463(__this, method) (( Object_t * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465(__this, method) (( bool (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20465_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2_t3790  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3789 *, KeyValuePair_2_t3790 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20467_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2_t3790  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3789 *, KeyValuePair_2_t3790 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20469_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2U5BU5D_t4391* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3789 *, KeyValuePair_2U5BU5D_t4391*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20471_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2_t3790  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3789 *, KeyValuePair_2_t3790 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20473_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20475_gshared (Dictionary_2_t3789 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20475(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3789 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20475_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477(__this, method) (( Object_t * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20477_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479(__this, method) (( Object_t* (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20479_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481(__this, method) (( Object_t * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20481_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m20483_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_get_Count_m20483(__this, method) (( int32_t (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_get_Count_m20483_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m20485_gshared (Dictionary_2_t3789 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_get_Item_m20485(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2_get_Item_m20485_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m20487_gshared (Dictionary_2_t3789 * __this, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_set_Item_m20487(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3789 *, int32_t, int32_t, MethodInfo*))Dictionary_2_set_Item_m20487_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m20489_gshared (Dictionary_2_t3789 * __this, int32_t ___capacity, Object_t* ___hcp, MethodInfo* method);
#define Dictionary_2_Init_m20489(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3789 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m20489_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m20491_gshared (Dictionary_2_t3789 * __this, int32_t ___size, MethodInfo* method);
#define Dictionary_2_InitArrays_m20491(__this, ___size, method) (( void (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m20491_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m20493_gshared (Dictionary_2_t3789 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyToCheck_m20493(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3789 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m20493_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3790  Dictionary_2_make_pair_m20495_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_make_pair_m20495(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3790  (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_make_pair_m20495_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m20497_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_pick_key_m20497(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_pick_key_m20497_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m20499_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_pick_value_m20499(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_pick_value_m20499_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m20501_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2U5BU5D_t4391* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyTo_m20501(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3789 *, KeyValuePair_2U5BU5D_t4391*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m20501_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m20503_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_Resize_m20503(__this, method) (( void (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_Resize_m20503_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m20505_gshared (Dictionary_2_t3789 * __this, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_Add_m20505(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3789 *, int32_t, int32_t, MethodInfo*))Dictionary_2_Add_m20505_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m20507_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_Clear_m20507(__this, method) (( void (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_Clear_m20507_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m20509_gshared (Dictionary_2_t3789 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_ContainsKey_m20509(__this, ___key, method) (( bool (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2_ContainsKey_m20509_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m20511_gshared (Dictionary_2_t3789 * __this, int32_t ___value, MethodInfo* method);
#define Dictionary_2_ContainsValue_m20511(__this, ___value, method) (( bool (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2_ContainsValue_m20511_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m20513_gshared (Dictionary_2_t3789 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2_GetObjectData_m20513(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3789 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m20513_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m20515_gshared (Dictionary_2_t3789 * __this, Object_t * ___sender, MethodInfo* method);
#define Dictionary_2_OnDeserialization_m20515(__this, ___sender, method) (( void (*) (Dictionary_2_t3789 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m20515_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m20517_gshared (Dictionary_2_t3789 * __this, int32_t ___key, MethodInfo* method);
#define Dictionary_2_Remove_m20517(__this, ___key, method) (( bool (*) (Dictionary_2_t3789 *, int32_t, MethodInfo*))Dictionary_2_Remove_m20517_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m20519_gshared (Dictionary_2_t3789 * __this, int32_t ___key, int32_t* ___value, MethodInfo* method);
#define Dictionary_2_TryGetValue_m20519(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3789 *, int32_t, int32_t*, MethodInfo*))Dictionary_2_TryGetValue_m20519_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t3792 * Dictionary_2_get_Keys_m20521_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_get_Keys_m20521(__this, method) (( KeyCollection_t3792 * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_get_Keys_m20521_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t3797 * Dictionary_2_get_Values_m20523_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_get_Values_m20523(__this, method) (( ValueCollection_t3797 * (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_get_Values_m20523_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m20525_gshared (Dictionary_2_t3789 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ToTKey_m20525(__this, ___key, method) (( int32_t (*) (Dictionary_2_t3789 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m20525_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m20527_gshared (Dictionary_2_t3789 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ToTValue_m20527(__this, ___value, method) (( int32_t (*) (Dictionary_2_t3789 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m20527_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m20529_gshared (Dictionary_2_t3789 * __this, KeyValuePair_2_t3790  ___pair, MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m20529(__this, ___pair, method) (( bool (*) (Dictionary_2_t3789 *, KeyValuePair_2_t3790 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20529_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t3794  Dictionary_2_GetEnumerator_m20531_gshared (Dictionary_2_t3789 * __this, MethodInfo* method);
#define Dictionary_2_GetEnumerator_m20531(__this, method) (( Enumerator_t3794  (*) (Dictionary_2_t3789 *, MethodInfo*))Dictionary_2_GetEnumerator_m20531_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2128  Dictionary_2_U3CCopyToU3Em__0_m20533_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m20533(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, int32_t, int32_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20533_gshared)(__this /* static, unused */, ___key, ___value, method)
