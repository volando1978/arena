﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t2691;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.HMACSHA256::.ctor()
extern "C" void HMACSHA256__ctor_m12983 (HMACSHA256_t2691 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
extern "C" void HMACSHA256__ctor_m12984 (HMACSHA256_t2691 * __this, ByteU5BU5D_t350* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
