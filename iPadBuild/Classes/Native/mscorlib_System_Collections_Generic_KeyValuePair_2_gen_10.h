﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>
struct  KeyValuePair_2_t3612 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>::value
	VirtualCategory_t126 * ___value_1;
};
