﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Collections.DictionaryEntry>
struct Transform_1_t3654;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m19026_gshared (Transform_1_t3654 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Transform_1__ctor_m19026(__this, ___object, ___method, method) (( void (*) (Transform_1_t3654 *, Object_t *, IntPtr_t, MethodInfo*))Transform_1__ctor_m19026_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t2128  Transform_1_Invoke_m19027_gshared (Transform_1_t3654 * __this, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Transform_1_Invoke_m19027(__this, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3654 *, Object_t *, uint32_t, MethodInfo*))Transform_1_Invoke_m19027_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m19028_gshared (Transform_1_t3654 * __this, Object_t * ___key, uint32_t ___value, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Transform_1_BeginInvoke_m19028(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3654 *, Object_t *, uint32_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Transform_1_BeginInvoke_m19028_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t2128  Transform_1_EndInvoke_m19029_gshared (Transform_1_t3654 * __this, Object_t * ___result, MethodInfo* method);
#define Transform_1_EndInvoke_m19029(__this, ___result, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3654 *, Object_t *, MethodInfo*))Transform_1_EndInvoke_m19029_gshared)(__this, ___result, method)
