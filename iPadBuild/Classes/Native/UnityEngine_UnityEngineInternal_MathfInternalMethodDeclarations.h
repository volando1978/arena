﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.MathfInternal
struct MathfInternal_t1539;

// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern "C" void MathfInternal__cctor_m6825 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
