﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// globales
struct globales_t807;
// UnityEngine.GameObject
struct GameObject_t144;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void globales::.ctor()
extern "C" void globales__ctor_m3517 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::.cctor()
extern "C" void globales__cctor_m3518 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::Awake()
extern "C" void globales_Awake_m3519 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::BeginGUI()
extern "C" void globales_BeginGUI_m3520 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::EndGUI()
extern "C" void globales_EndGUI_m3521 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setCamera()
extern "C" void globales_setCamera_m3522 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::Start()
extern "C" void globales_Start_m3523 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::onSoomlaStoreInitialized()
extern "C" void globales_onSoomlaStoreInitialized_m3524 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::Update()
extern "C" void globales_Update_m3525 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setPosAgujeros()
extern "C" void globales_setPosAgujeros_m3526 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::clearMenu()
extern "C" void globales_clearMenu_m3527 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 globales::getRandomPos()
extern "C" Vector2_t739  globales_getRandomPos_m3528 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 globales::getRandomRot()
extern "C" Vector3_t758  globales_getRandomRot_m3529 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setCameraLevelColor()
extern "C" void globales_setCameraLevelColor_m3530 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::resetCameraColor()
extern "C" void globales_resetCameraColor_m3531 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setCameraRandomColor()
extern "C" void globales_setCameraRandomColor_m3532 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color globales::getRandomColor()
extern "C" Color_t747  globales_getRandomColor_m3533 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setGrey()
extern "C" void globales_setGrey_m3534 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::lanzaEnDirecciones(UnityEngine.GameObject)
extern "C" void globales_lanzaEnDirecciones_m3535 (Object_t * __this /* static, unused */, GameObject_t144 * ___go, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::setData()
extern "C" void globales_setData_m3536 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::getData()
extern "C" void globales_getData_m3537 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::deleteData()
extern "C" void globales_deleteData_m3538 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::audioToggle()
extern "C" void globales_audioToggle_m3539 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::musicToggle()
extern "C" void globales_musicToggle_m3540 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::soundCheck()
extern "C" void globales_soundCheck_m3541 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::OnApplicationQuit()
extern "C" void globales_OnApplicationQuit_m3542 (globales_t807 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void globales::SetBool(System.String,System.Boolean)
extern "C" void globales_SetBool_m3543 (Object_t * __this /* static, unused */, String_t* ___name, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean globales::GetBool(System.String)
extern "C" bool globales_GetBool_m3544 (Object_t * __this /* static, unused */, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean globales::GetBool(System.String,System.Boolean)
extern "C" bool globales_GetBool_m3545 (Object_t * __this /* static, unused */, String_t* ___name, bool ___defaultValue, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator globales::sleep(System.Single)
extern "C" Object_t * globales_sleep_m3546 (Object_t * __this /* static, unused */, float ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
