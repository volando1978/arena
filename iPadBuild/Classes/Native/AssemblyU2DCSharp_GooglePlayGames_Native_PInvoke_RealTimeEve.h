﻿#pragma once
#include <stdint.h>
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom>
struct Action_1_t693;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStorey63
struct  U3CToCallbackPointerU3Ec__AnonStorey63_t694  : public Object_t
{
	// System.Action`1<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom> GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStorey63::callback
	Action_1_t693 * ___callback_0;
};
