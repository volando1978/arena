﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// gameOverSrc
struct gameOverSrc_t801;
// System.Object
#include "mscorlib_System_Object.h"
// gameOverSrc/<run>c__IteratorD
struct  U3CrunU3Ec__IteratorD_t802  : public Object_t
{
	// System.Int32 gameOverSrc/<run>c__IteratorD::$PC
	int32_t ___U24PC_0;
	// System.Object gameOverSrc/<run>c__IteratorD::$current
	Object_t * ___U24current_1;
	// gameOverSrc gameOverSrc/<run>c__IteratorD::<>f__this
	gameOverSrc_t801 * ___U3CU3Ef__this_2;
};
