﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.DummyClient
struct DummyClient_t337;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.String
struct String_t;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct List_1_t841;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t530;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t842;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient
struct IRealTimeMultiplayerClient_t843;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient
struct ITurnBasedMultiplayerClient_t844;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
struct ISavedGameClient_t537;
// GooglePlayGames.BasicApi.Events.IEventsClient
struct IEventsClient_t538;
// GooglePlayGames.BasicApi.Quests.IQuestsClient
struct IQuestsClient_t539;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;

// System.Void GooglePlayGames.BasicApi.DummyClient::.ctor()
extern "C" void DummyClient__ctor_m1345 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void DummyClient_Authenticate_m1346 (DummyClient_t337 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.DummyClient::IsAuthenticated()
extern "C" bool DummyClient_IsAuthenticated_m1347 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::SignOut()
extern "C" void DummyClient_SignOut_m1348 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserId()
extern "C" String_t* DummyClient_GetUserId_m1349 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserDisplayName()
extern "C" String_t* DummyClient_GetUserDisplayName_m1350 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.DummyClient::GetUserImageUrl()
extern "C" String_t* DummyClient_GetUserImageUrl_m1351 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement> GooglePlayGames.BasicApi.DummyClient::GetAchievements()
extern "C" List_1_t841 * DummyClient_GetAchievements_m1352 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.BasicApi.DummyClient::GetAchievement(System.String)
extern "C" Achievement_t333 * DummyClient_GetAchievement_m1353 (DummyClient_t337 * __this, String_t* ___achId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void DummyClient_UnlockAchievement_m1354 (DummyClient_t337 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::RevealAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void DummyClient_RevealAchievement_m1355 (DummyClient_t337 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern "C" void DummyClient_IncrementAchievement_m1356 (DummyClient_t337 * __this, String_t* ___achId, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void DummyClient_ShowAchievementsUI_m1357 (DummyClient_t337 * __this, Action_1_t530 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void DummyClient_ShowLeaderboardUI_m1358 (DummyClient_t337 * __this, String_t* ___lbId, Action_1_t530 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>)
extern "C" void DummyClient_SubmitScore_m1359 (DummyClient_t337 * __this, String_t* ___lbId, int64_t ___score, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void DummyClient_LoadState_m1360 (DummyClient_t337 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void DummyClient_UpdateState_m1361 (DummyClient_t337 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetRtmpClient()
extern "C" Object_t * DummyClient_GetRtmpClient_m1362 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.BasicApi.DummyClient::GetTbmpClient()
extern "C" Object_t * DummyClient_GetTbmpClient_m1363 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.BasicApi.DummyClient::GetSavedGameClient()
extern "C" Object_t * DummyClient_GetSavedGameClient_m1364 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.BasicApi.DummyClient::GetEventsClient()
extern "C" Object_t * DummyClient_GetEventsClient_m1365 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.BasicApi.DummyClient::GetQuestsClient()
extern "C" Object_t * DummyClient_GetQuestsClient_m1366 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C" void DummyClient_RegisterInvitationDelegate_m1367 (DummyClient_t337 * __this, InvitationReceivedDelegate_t363 * ___deleg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Invitation GooglePlayGames.BasicApi.DummyClient::GetInvitationFromNotification()
extern "C" Invitation_t341 * DummyClient_GetInvitationFromNotification_m1368 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.DummyClient::HasInvitationFromNotification()
extern "C" bool DummyClient_HasInvitationFromNotification_m1369 (DummyClient_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.DummyClient::LogUsage()
extern "C" void DummyClient_LogUsage_m1370 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
