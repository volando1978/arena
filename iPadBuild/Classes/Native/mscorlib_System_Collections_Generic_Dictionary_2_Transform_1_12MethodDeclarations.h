﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>
struct Transform_1_t3600;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_1MethodDeclarations.h"
#define Transform_1__ctor_m17993(__this, ___object, ___method, method) (( void (*) (Transform_1_t3600 *, Object_t *, IntPtr_t, MethodInfo*))Transform_1__ctor_m16126_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m17994(__this, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3600 *, String_t*, VirtualItem_t125 *, MethodInfo*))Transform_1_Invoke_m16128_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m17995(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3600 *, String_t*, VirtualItem_t125 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Transform_1_BeginInvoke_m16130_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,Soomla.Store.VirtualItem,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m17996(__this, ___result, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3600 *, Object_t *, MethodInfo*))Transform_1_EndInvoke_m16132_gshared)(__this, ___result, method)
