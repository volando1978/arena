﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t3877  : public Object_t
{
	// T[] System.Collections.Generic.Stack`1<System.Object>::_array
	ObjectU5BU5D_t208* ____array_1;
	// System.Int32 System.Collections.Generic.Stack`1<System.Object>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Stack`1<System.Object>::_version
	int32_t ____version_3;
};
