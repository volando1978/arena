﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.EventManager
struct EventManager_t548;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
struct Action_1_t869;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
struct Action_1_t870;
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.EventManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void EventManager__ctor_m2655 (EventManager_t548 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager::FetchAll(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>)
extern "C" void EventManager_FetchAll_m2656 (EventManager_t548 * __this, int32_t ___source, Action_1_t869 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
extern "C" void EventManager_InternalFetchAllCallback_m2657 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager::Fetch(GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>)
extern "C" void EventManager_Fetch_m2658 (EventManager_t548 * __this, int32_t ___source, String_t* ___eventId, Action_1_t870 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager::InternalFetchCallback(System.IntPtr,System.IntPtr)
extern "C" void EventManager_InternalFetchCallback_m2659 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager::Increment(System.String,System.UInt32)
extern "C" void EventManager_Increment_m2660 (EventManager_t548 * __this, String_t* ___eventId, uint32_t ___steps, MethodInfo* method) IL2CPP_METHOD_ATTR;
