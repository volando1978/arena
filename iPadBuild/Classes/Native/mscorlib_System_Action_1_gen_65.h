﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t913;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.BasicApi.Events.IEvent>
struct  Action_1_t3644  : public MulticastDelegate_t22
{
};
