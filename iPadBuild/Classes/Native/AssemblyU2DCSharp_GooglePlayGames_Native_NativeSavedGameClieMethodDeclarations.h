﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver
struct NativeConflictResolver_t613;
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t610;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// System.Action
struct Action_t588;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t703;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::.ctor(GooglePlayGames.Native.PInvoke.SnapshotManager,System.String,GooglePlayGames.Native.NativeSnapshotMetadata,GooglePlayGames.Native.NativeSnapshotMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>,System.Action)
extern "C" void NativeConflictResolver__ctor_m2496 (NativeConflictResolver_t613 * __this, SnapshotManager_t610 * ___manager, String_t* ___conflictId, NativeSnapshotMetadata_t611 * ___original, NativeSnapshotMetadata_t611 * ___unmerged, Action_2_t612 * ___completeCallback, Action_t588 * ___retryOpen, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::ChooseMetadata(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void NativeConflictResolver_ChooseMetadata_m2497 (NativeConflictResolver_t613 * __this, Object_t * ___chosenMetadata, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/NativeConflictResolver::<ChooseMetadata>m__54(GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse)
extern "C" void NativeConflictResolver_U3CChooseMetadataU3Em__54_m2498 (NativeConflictResolver_t613 * __this, CommitResponse_t703 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
