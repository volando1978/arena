﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
struct BeforeRoomCreateStartedState_t581;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void BeforeRoomCreateStartedState__ctor_m2405 (BeforeRoomCreateStartedState_t581 * __this, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState::LeaveRoom()
extern "C" void BeforeRoomCreateStartedState_LeaveRoom_m2406 (BeforeRoomCreateStartedState_t581 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
