﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WritableAttribute
struct WritableAttribute_t1613;

// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m7310 (WritableAttribute_t1613 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
