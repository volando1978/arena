﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<runTutorial>c__IteratorB
struct U3CrunTutorialU3Ec__IteratorB_t799;
// System.Object
struct Object_t;

// System.Void gameControl/<runTutorial>c__IteratorB::.ctor()
extern "C" void U3CrunTutorialU3Ec__IteratorB__ctor_m3437 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<runTutorial>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<runTutorial>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<runTutorial>c__IteratorB::MoveNext()
extern "C" bool U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<runTutorial>c__IteratorB::Dispose()
extern "C" void U3CrunTutorialU3Ec__IteratorB_Dispose_m3441 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<runTutorial>c__IteratorB::Reset()
extern "C" void U3CrunTutorialU3Ec__IteratorB_Reset_m3442 (U3CrunTutorialU3Ec__IteratorB_t799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
