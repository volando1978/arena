﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t1422;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
extern "C" void AssemblyProductAttribute__ctor_m6178 (AssemblyProductAttribute_t1422 * __this, String_t* ___product, MethodInfo* method) IL2CPP_METHOD_ATTR;
