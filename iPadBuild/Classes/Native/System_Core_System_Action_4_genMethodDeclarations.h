﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>
struct Action_4_t882;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Action`4<System.Object,System.Object,System.Object,System.Byte>
#include "System_Core_System_Action_4_gen_0MethodDeclarations.h"
#define Action_4__ctor_m3837(__this, ___object, ___method, method) (( void (*) (Action_4_t882 *, Object_t *, IntPtr_t, MethodInfo*))Action_4__ctor_m19898_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m19899(__this, ___arg1, ___arg2, ___arg3, ___arg4, method) (( void (*) (Action_4_t882 *, NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 *, ByteU5BU5D_t350*, bool, MethodInfo*))Action_4_Invoke_m19900_gshared)(__this, ___arg1, ___arg2, ___arg3, ___arg4, method)
// System.IAsyncResult System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m19901(__this, ___arg1, ___arg2, ___arg3, ___arg4, ___callback, ___object, method) (( Object_t * (*) (Action_4_t882 *, NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 *, ByteU5BU5D_t350*, bool, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_4_BeginInvoke_m19902_gshared)(__this, ___arg1, ___arg2, ___arg3, ___arg4, ___callback, ___object, method)
// System.Void System.Action`4<GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m19903(__this, ___result, method) (( void (*) (Action_4_t882 *, Object_t *, MethodInfo*))Action_4_EndInvoke_m19904_gshared)(__this, ___result, method)
