﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// orlaScr
struct orlaScr_t826;

// System.Void orlaScr::.ctor()
extern "C" void orlaScr__ctor_m3615 (orlaScr_t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void orlaScr::Start()
extern "C" void orlaScr_Start_m3616 (orlaScr_t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void orlaScr::Update()
extern "C" void orlaScr_Update_m3617 (orlaScr_t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void orlaScr::OnGUI()
extern "C" void orlaScr_OnGUI_m3618 (orlaScr_t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
