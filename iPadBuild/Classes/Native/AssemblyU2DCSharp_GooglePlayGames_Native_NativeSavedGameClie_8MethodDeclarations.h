﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49
struct U3CCommitUpdateU3Ec__AnonStorey49_t629;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t703;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49::.ctor()
extern "C" void U3CCommitUpdateU3Ec__AnonStorey49__ctor_m2520 (U3CCommitUpdateU3Ec__AnonStorey49_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<CommitUpdate>c__AnonStorey49::<>m__4D(GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse)
extern "C" void U3CCommitUpdateU3Ec__AnonStorey49_U3CU3Em__4D_m2521 (U3CCommitUpdateU3Ec__AnonStorey49_t629 * __this, CommitResponse_t703 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
