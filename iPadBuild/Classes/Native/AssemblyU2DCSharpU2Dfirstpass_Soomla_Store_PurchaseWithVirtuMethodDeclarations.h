﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8
struct U3CBuyU3Ec__AnonStorey8_t140;

// System.Void Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::.ctor()
extern "C" void U3CBuyU3Ec__AnonStorey8__ctor_m676 (U3CBuyU3Ec__AnonStorey8_t140 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::<>m__23()
extern "C" void U3CBuyU3Ec__AnonStorey8_U3CU3Em__23_m677 (U3CBuyU3Ec__AnonStorey8_t140 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
