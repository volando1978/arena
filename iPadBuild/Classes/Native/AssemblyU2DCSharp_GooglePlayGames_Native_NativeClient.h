﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct NativeTurnBasedMultiplayerClient_t535;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct NativeRealtimeMultiplayerClient_t536;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct ISavedGameClient_t537;
// GooglePlayGames.BasicApi.Events.IEventsClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct IEventsClient_t538;
// GooglePlayGames.BasicApi.Quests.IQuestsClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct IQuestsClient_t539;
// GooglePlayGames.Native.AppStateClient modreq(System.Runtime.CompilerServices.IsVolatile)
struct AppStateClient_t540;
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile)
struct Action_2_t541;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement> modreq(System.Runtime.CompilerServices.IsVolatile)
struct Dictionary_2_t542;
// GooglePlayGames.BasicApi.Multiplayer.Player modreq(System.Runtime.CompilerServices.IsVolatile)
struct Player_t347;
// System.Action`1<System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile)
struct Action_1_t98;
// System.Predicate`1<GooglePlayGames.BasicApi.Achievement>
struct Predicate_1_t543;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
// GooglePlayGames.Native.NativeClient/AuthState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_AuthSt.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// GooglePlayGames.Native.NativeClient
struct  NativeClient_t524  : public Object_t
{
	// System.Object GooglePlayGames.Native.NativeClient::GameServicesLock
	Object_t * ___GameServicesLock_3;
	// System.Object GooglePlayGames.Native.NativeClient::AuthStateLock
	Object_t * ___AuthStateLock_4;
	// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.Native.NativeClient::mConfiguration
	PlayGamesClientConfiguration_t366  ___mConfiguration_5;
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.NativeClient::mServices
	GameServices_t534 * ___mServices_6;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mTurnBasedClient
	NativeTurnBasedMultiplayerClient_t535 * ___mTurnBasedClient_7;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mRealTimeClient
	NativeRealtimeMultiplayerClient_t536 * ___mRealTimeClient_8;
	// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mSavedGameClient
	Object_t * ___mSavedGameClient_9;
	// GooglePlayGames.BasicApi.Events.IEventsClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mEventsClient
	Object_t * ___mEventsClient_10;
	// GooglePlayGames.BasicApi.Quests.IQuestsClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mQuestsClient
	Object_t * ___mQuestsClient_11;
	// GooglePlayGames.Native.AppStateClient modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mAppStateClient
	Object_t * ___mAppStateClient_12;
	// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mInvitationDelegate
	Action_2_t541 * ___mInvitationDelegate_13;
	// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement> modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mAchievements
	Dictionary_2_t542 * ___mAchievements_14;
	// GooglePlayGames.BasicApi.Multiplayer.Player modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mUser
	Player_t347 * ___mUser_15;
	// System.Action`1<System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mPendingAuthCallbacks
	Action_1_t98 * ___mPendingAuthCallbacks_16;
	// System.Action`1<System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mSilentAuthCallbacks
	Action_1_t98 * ___mSilentAuthCallbacks_17;
	// GooglePlayGames.Native.NativeClient/AuthState modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mAuthState
	int32_t ___mAuthState_18;
	// System.UInt32 modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mAuthGeneration
	uint32_t ___mAuthGeneration_19;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeClient::mSilentAuthFailed
	bool ___mSilentAuthFailed_20;
};
struct NativeClient_t524_StaticFields{
	// System.Predicate`1<GooglePlayGames.BasicApi.Achievement> GooglePlayGames.Native.NativeClient::<>f__am$cache12
	Predicate_1_t543 * ___U3CU3Ef__amU24cache12_21;
	// System.Predicate`1<GooglePlayGames.BasicApi.Achievement> GooglePlayGames.Native.NativeClient::<>f__am$cache13
	Predicate_1_t543 * ___U3CU3Ef__amU24cache13_22;
};
