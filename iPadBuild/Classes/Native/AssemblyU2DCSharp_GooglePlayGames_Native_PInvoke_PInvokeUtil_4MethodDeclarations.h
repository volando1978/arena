﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>
struct OutMethod_1_t3805;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void OutMethod_1__ctor_m20673_gshared (OutMethod_1_t3805 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define OutMethod_1__ctor_m20673(__this, ___object, ___method, method) (( void (*) (OutMethod_1_t3805 *, Object_t *, IntPtr_t, MethodInfo*))OutMethod_1__ctor_m20673_gshared)(__this, ___object, ___method, method)
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::Invoke(T[],System.UIntPtr)
extern "C" UIntPtr_t  OutMethod_1_Invoke_m20674_gshared (OutMethod_1_t3805 * __this, ObjectU5BU5D_t208* ___out_bytes, UIntPtr_t  ___out_size, MethodInfo* method);
#define OutMethod_1_Invoke_m20674(__this, ___out_bytes, ___out_size, method) (( UIntPtr_t  (*) (OutMethod_1_t3805 *, ObjectU5BU5D_t208*, UIntPtr_t , MethodInfo*))OutMethod_1_Invoke_m20674_gshared)(__this, ___out_bytes, ___out_size, method)
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OutMethod_1_BeginInvoke_m20675_gshared (OutMethod_1_t3805 * __this, ObjectU5BU5D_t208* ___out_bytes, UIntPtr_t  ___out_size, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define OutMethod_1_BeginInvoke_m20675(__this, ___out_bytes, ___out_size, ___callback, ___object, method) (( Object_t * (*) (OutMethod_1_t3805 *, ObjectU5BU5D_t208*, UIntPtr_t , AsyncCallback_t20 *, Object_t *, MethodInfo*))OutMethod_1_BeginInvoke_m20675_gshared)(__this, ___out_bytes, ___out_size, ___callback, ___object, method)
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" UIntPtr_t  OutMethod_1_EndInvoke_m20676_gshared (OutMethod_1_t3805 * __this, Object_t * ___result, MethodInfo* method);
#define OutMethod_1_EndInvoke_m20676(__this, ___result, method) (( UIntPtr_t  (*) (OutMethod_1_t3805 *, Object_t *, MethodInfo*))OutMethod_1_EndInvoke_m20676_gshared)(__this, ___result, method)
