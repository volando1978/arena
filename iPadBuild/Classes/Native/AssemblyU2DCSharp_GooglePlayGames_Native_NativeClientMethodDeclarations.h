﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// GooglePlayGames.Native.AppStateClient
struct AppStateClient_t540;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.PInvoke.PlatformConfiguration
struct PlatformConfiguration_t669;
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
struct FetchAllResponse_t655;
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
struct FetchSelfResponse_t684;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Predicate`1<GooglePlayGames.BasicApi.Achievement>
struct Predicate_1_t543;
// System.Action`1<GooglePlayGames.BasicApi.Achievement>
struct Action_1_t860;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t530;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t842;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient
struct IRealTimeMultiplayerClient_t843;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient
struct ITurnBasedMultiplayerClient_t844;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
struct ISavedGameClient_t537;
// GooglePlayGames.BasicApi.Events.IEventsClient
struct IEventsClient_t538;
// GooglePlayGames.BasicApi.Quests.IQuestsClient
struct IQuestsClient_t539;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"

// System.Void GooglePlayGames.Native.NativeClient::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern "C" void NativeClient__ctor_m2255 (NativeClient_t524 * __this, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.NativeClient::GameServices()
extern "C" GameServices_t534 * NativeClient_GameServices_m2256 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void NativeClient_Authenticate_m2257 (NativeClient_t524 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::InitializeGameServices()
extern "C" void NativeClient_InitializeGameServices_m2258 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.AppStateClient GooglePlayGames.Native.NativeClient::CreateAppStateClient()
extern "C" Object_t * NativeClient_CreateAppStateClient_m2259 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::HandleInvitation(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void NativeClient_HandleInvitation_m2260 (NativeClient_t524 * __this, int32_t ___eventType, String_t* ___invitationId, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.PlatformConfiguration GooglePlayGames.Native.NativeClient::CreatePlatformConfiguration()
extern "C" PlatformConfiguration_t669 * NativeClient_CreatePlatformConfiguration_m2261 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated()
extern "C" bool NativeClient_IsAuthenticated_m2262 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::PopulateAchievements(System.UInt32,GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse)
extern "C" void NativeClient_PopulateAchievements_m2263 (NativeClient_t524 * __this, uint32_t ___authGeneration, FetchAllResponse_t655 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::MaybeFinishAuthentication()
extern "C" void NativeClient_MaybeFinishAuthentication_m2264 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::PopulateUser(System.UInt32,GooglePlayGames.Native.PlayerManager/FetchSelfResponse)
extern "C" void NativeClient_PopulateUser_m2265 (NativeClient_t524 * __this, uint32_t ___authGeneration, FetchSelfResponse_t684 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::HandleAuthTransition(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus)
extern "C" void NativeClient_HandleAuthTransition_m2266 (NativeClient_t524 * __this, int32_t ___operation, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::ToUnauthenticated()
extern "C" void NativeClient_ToUnauthenticated_m2267 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::SignOut()
extern "C" void NativeClient_SignOut_m2268 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeClient::GetUserId()
extern "C" String_t* NativeClient_GetUserId_m2269 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeClient::GetUserDisplayName()
extern "C" String_t* NativeClient_GetUserDisplayName_m2270 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeClient::GetUserImageUrl()
extern "C" String_t* NativeClient_GetUserImageUrl_m2271 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeClient::GetAchievement(System.String)
extern "C" Achievement_t333 * NativeClient_GetAchievement_m2272 (NativeClient_t524 * __this, String_t* ___achId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void NativeClient_UnlockAchievement_m2273 (NativeClient_t524 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::RevealAchievement(System.String,System.Action`1<System.Boolean>)
extern "C" void NativeClient_RevealAchievement_m2274 (NativeClient_t524 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::UpdateAchievement(System.String,System.String,System.Action`1<System.Boolean>,System.Predicate`1<GooglePlayGames.BasicApi.Achievement>,System.Action`1<GooglePlayGames.BasicApi.Achievement>)
extern "C" void NativeClient_UpdateAchievement_m2275 (NativeClient_t524 * __this, String_t* ___updateType, String_t* ___achId, Action_1_t98 * ___callback, Predicate_1_t543 * ___alreadyDone, Action_1_t860 * ___updateAchievment, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern "C" void NativeClient_IncrementAchievement_m2276 (NativeClient_t524 * __this, String_t* ___achId, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void NativeClient_ShowAchievementsUI_m2277 (NativeClient_t524 * __this, Action_1_t530 * ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern "C" void NativeClient_ShowLeaderboardUI_m2278 (NativeClient_t524 * __this, String_t* ___leaderboardId, Action_1_t530 * ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>)
extern "C" void NativeClient_SubmitScore_m2279 (NativeClient_t524 * __this, String_t* ___leaderboardId, int64_t ___score, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void NativeClient_LoadState_m2280 (NativeClient_t524 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void NativeClient_UpdateState_m2281 (NativeClient_t524 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.Native.NativeClient::GetRtmpClient()
extern "C" Object_t * NativeClient_GetRtmpClient_m2282 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.Native.NativeClient::GetTbmpClient()
extern "C" Object_t * NativeClient_GetTbmpClient_m2283 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.Native.NativeClient::GetSavedGameClient()
extern "C" Object_t * NativeClient_GetSavedGameClient_m2284 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.Native.NativeClient::GetEventsClient()
extern "C" Object_t * NativeClient_GetEventsClient_m2285 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.Native.NativeClient::GetQuestsClient()
extern "C" Object_t * NativeClient_GetQuestsClient_m2286 (NativeClient_t524 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern "C" void NativeClient_RegisterInvitationDelegate_m2287 (NativeClient_t524 * __this, InvitationReceivedDelegate_t363 * ___invitationDelegate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient::<InitializeGameServices>m__10(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void NativeClient_U3CInitializeGameServicesU3Em__10_m2288 (NativeClient_t524 * __this, int32_t ___eventType, String_t* ___matchId, NativeTurnBasedMatch_t680 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeClient::<UnlockAchievement>m__13(GooglePlayGames.BasicApi.Achievement)
extern "C" bool NativeClient_U3CUnlockAchievementU3Em__13_m2289 (Object_t * __this /* static, unused */, Achievement_t333 * ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeClient::<RevealAchievement>m__15(GooglePlayGames.BasicApi.Achievement)
extern "C" bool NativeClient_U3CRevealAchievementU3Em__15_m2290 (Object_t * __this /* static, unused */, Achievement_t333 * ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
