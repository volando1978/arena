﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2790;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t2790  : public Object_t
{
};
struct DBNull_t2790_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2790 * ___Value_0;
};
