﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.StoreInfo
struct StoreInfo_t67;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>
struct Dictionary_2_t110;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct Dictionary_2_t111;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualCategory>
struct Dictionary_2_t112;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct Dictionary_2_t113;
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>
struct List_1_t114;
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>
struct List_1_t115;
// System.Collections.Generic.List`1<Soomla.Store.VirtualGood>
struct List_1_t116;
// System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>
struct List_1_t117;
// System.Func`2<Soomla.Store.UpgradeVG,System.Boolean>
struct Func_2_t118;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.StoreInfo
struct  StoreInfo_t67  : public Object_t
{
};
struct StoreInfo_t67_StaticFields{
	// Soomla.Store.StoreInfo Soomla.Store.StoreInfo::_instance
	StoreInfo_t67 * ____instance_1;
	// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem> Soomla.Store.StoreInfo::VirtualItems
	Dictionary_2_t110 * ___VirtualItems_2;
	// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem> Soomla.Store.StoreInfo::PurchasableItems
	Dictionary_2_t111 * ___PurchasableItems_3;
	// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualCategory> Soomla.Store.StoreInfo::GoodsCategories
	Dictionary_2_t112 * ___GoodsCategories_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>> Soomla.Store.StoreInfo::GoodsUpgrades
	Dictionary_2_t113 * ___GoodsUpgrades_5;
	// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency> Soomla.Store.StoreInfo::Currencies
	List_1_t114 * ___Currencies_6;
	// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack> Soomla.Store.StoreInfo::CurrencyPacks
	List_1_t115 * ___CurrencyPacks_7;
	// System.Collections.Generic.List`1<Soomla.Store.VirtualGood> Soomla.Store.StoreInfo::Goods
	List_1_t116 * ___Goods_8;
	// System.Collections.Generic.List`1<Soomla.Store.VirtualCategory> Soomla.Store.StoreInfo::Categories
	List_1_t117 * ___Categories_9;
	// System.Func`2<Soomla.Store.UpgradeVG,System.Boolean> Soomla.Store.StoreInfo::<>f__am$cache9
	Func_2_t118 * ___U3CU3Ef__amU24cache9_10;
	// System.Func`2<Soomla.Store.UpgradeVG,System.Boolean> Soomla.Store.StoreInfo::<>f__am$cacheA
	Func_2_t118 * ___U3CU3Ef__amU24cacheA_11;
};
