﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t2149;
struct DSAParameters_t2149_marshaled;

void DSAParameters_t2149_marshal(const DSAParameters_t2149& unmarshaled, DSAParameters_t2149_marshaled& marshaled);
void DSAParameters_t2149_marshal_back(const DSAParameters_t2149_marshaled& marshaled, DSAParameters_t2149& unmarshaled);
void DSAParameters_t2149_marshal_cleanup(DSAParameters_t2149_marshaled& marshaled);
