﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Player
struct Player_t438;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_CurrentLevel(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t Player_Player_CurrentLevel_m1818 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Player_Player_Name_m1819 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Player::Player_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void Player_Player_Dispose_m1820 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_AvatarUrl(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/ImageResolution,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Player_Player_AvatarUrl_m1821 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___resolution, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_LastLevelUpTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t Player_Player_LastLevelUpTime_m1822 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Title(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Player_Player_Title_m1823 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.Player::Player_CurrentXP(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t Player_Player_CurrentXP_m1824 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool Player_Player_Valid_m1825 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Player::Player_HasLevelInfo(System.Runtime.InteropServices.HandleRef)
extern "C" bool Player_Player_HasLevelInfo_m1826 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.Player::Player_NextLevel(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t Player_Player_NextLevel_m1827 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Player::Player_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Player_Player_Id_m1828 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
