﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
struct InternalEnumerator_1_t4131;
// System.Object
struct Object_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1693;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25175(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4131 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25176(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4131 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m25177(__this, method) (( void (*) (InternalEnumerator_1_t4131 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25178(__this, method) (( bool (*) (InternalEnumerator_1_t4131 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m25179(__this, method) (( ParameterInfo_t1693 * (*) (InternalEnumerator_1_t4131 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
