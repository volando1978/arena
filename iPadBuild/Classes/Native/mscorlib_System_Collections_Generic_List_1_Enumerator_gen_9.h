﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct  Enumerator_t904 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::l
	List_1_t351 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::current
	Participant_t340 * ___current_3;
};
