﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// number
struct  number_t825  : public MonoBehaviour_t26
{
	// System.Int32 number::n
	int32_t ___n_2;
	// System.Boolean number::isCleared
	bool ___isCleared_3;
};
