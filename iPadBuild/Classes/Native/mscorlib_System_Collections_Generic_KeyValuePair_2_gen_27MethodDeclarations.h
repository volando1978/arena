﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t4136;
// UnityEngine.Event
struct Event_t1269;
struct Event_t1269_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25230(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t4136 *, Event_t1269 *, int32_t, MethodInfo*))KeyValuePair_2__ctor_m17099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m25231(__this, method) (( Event_t1269 * (*) (KeyValuePair_2_t4136 *, MethodInfo*))KeyValuePair_2_get_Key_m17100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25232(__this, ___value, method) (( void (*) (KeyValuePair_2_t4136 *, Event_t1269 *, MethodInfo*))KeyValuePair_2_set_Key_m17101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m25233(__this, method) (( int32_t (*) (KeyValuePair_2_t4136 *, MethodInfo*))KeyValuePair_2_get_Value_m17102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25234(__this, ___value, method) (( void (*) (KeyValuePair_2_t4136 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m17103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m25235(__this, method) (( String_t* (*) (KeyValuePair_2_t4136 *, MethodInfo*))KeyValuePair_2_ToString_m17104_gshared)(__this, method)
