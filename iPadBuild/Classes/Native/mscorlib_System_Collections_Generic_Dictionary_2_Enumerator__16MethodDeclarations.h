﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Enumerator_t3742;
// System.Object
struct Object_t;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Dictionary_2_t575;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m20000(__this, ___dictionary, method) (( void (*) (Enumerator_t3742 *, Dictionary_2_t575 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20001(__this, method) (( Object_t * (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20002(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20003(__this, method) (( Object_t * (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20004(__this, method) (( Object_t * (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::MoveNext()
#define Enumerator_MoveNext_m20005(__this, method) (( bool (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current()
#define Enumerator_get_Current_m20006(__this, method) (( KeyValuePair_2_t3741  (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20007(__this, method) (( String_t* (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20008(__this, method) (( MultiplayerParticipant_t672 * (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::VerifyState()
#define Enumerator_VerifyState_m20009(__this, method) (( void (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20010(__this, method) (( void (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Dispose()
#define Enumerator_Dispose_m20011(__this, method) (( void (*) (Enumerator_t3742 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
