﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t1393;
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t4462;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3983;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3985;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t1236;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m5911(__this, method) (( void (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1__ctor_m22554_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m23348(__this, method) (( Object_t * (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m23349(__this, ___item, method) (( void (*) (IndexedSet_1_t1393 *, Graphic_t1233 *, MethodInfo*))IndexedSet_1_Add_m22558_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m23350(__this, ___item, method) (( bool (*) (IndexedSet_1_t1393 *, Graphic_t1233 *, MethodInfo*))IndexedSet_1_Remove_m22560_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m23351(__this, method) (( Object_t* (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1_GetEnumerator_m22562_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m23352(__this, method) (( void (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1_Clear_m22564_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m23353(__this, ___item, method) (( bool (*) (IndexedSet_1_t1393 *, Graphic_t1233 *, MethodInfo*))IndexedSet_1_Contains_m22566_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m23354(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t1393 *, GraphicU5BU5D_t3983*, int32_t, MethodInfo*))IndexedSet_1_CopyTo_m22568_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m23355(__this, method) (( int32_t (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1_get_Count_m22570_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m23356(__this, method) (( bool (*) (IndexedSet_1_t1393 *, MethodInfo*))IndexedSet_1_get_IsReadOnly_m22572_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m23357(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t1393 *, Graphic_t1233 *, MethodInfo*))IndexedSet_1_IndexOf_m22574_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m23358(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t1393 *, int32_t, Graphic_t1233 *, MethodInfo*))IndexedSet_1_Insert_m22576_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m23359(__this, ___index, method) (( void (*) (IndexedSet_1_t1393 *, int32_t, MethodInfo*))IndexedSet_1_RemoveAt_m22578_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m23360(__this, ___index, method) (( Graphic_t1233 * (*) (IndexedSet_1_t1393 *, int32_t, MethodInfo*))IndexedSet_1_get_Item_m22580_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m23361(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t1393 *, int32_t, Graphic_t1233 *, MethodInfo*))IndexedSet_1_set_Item_m22582_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m23362(__this, ___match, method) (( void (*) (IndexedSet_1_t1393 *, Predicate_1_t3985 *, MethodInfo*))IndexedSet_1_RemoveAll_m22583_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m23363(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t1393 *, Comparison_1_t1236 *, MethodInfo*))IndexedSet_1_Sort_m22584_gshared)(__this, ___sortLayoutFunction, method)
