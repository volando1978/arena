﻿#pragma once
#include <stdint.h>
// Soomla.Store.MarketItem[]
struct MarketItemU5BU5D_t3557;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct  List_1_t177  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.MarketItem>::_items
	MarketItemU5BU5D_t3557* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::_version
	int32_t ____version_3;
};
struct List_1_t177_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.MarketItem>::EmptyArray
	MarketItemU5BU5D_t3557* ___EmptyArray_4;
};
