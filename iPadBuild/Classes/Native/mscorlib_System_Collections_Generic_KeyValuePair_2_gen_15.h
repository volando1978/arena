﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct  KeyValuePair_2_t3714 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>::value
	Achievement_t333 * ___value_1;
};
