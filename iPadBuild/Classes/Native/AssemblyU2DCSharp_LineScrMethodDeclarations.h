﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LineScr
struct LineScr_t748;
// UnityEngine.GameObject
struct GameObject_t144;

// System.Void LineScr::.ctor()
extern "C" void LineScr__ctor_m3239 (LineScr_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineScr::Start()
extern "C" void LineScr_Start_m3240 (LineScr_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineScr::Update()
extern "C" void LineScr_Update_m3241 (LineScr_t748 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineScr::setParams(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C" void LineScr_setParams_m3242 (LineScr_t748 * __this, GameObject_t144 * ___a, GameObject_t144 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
