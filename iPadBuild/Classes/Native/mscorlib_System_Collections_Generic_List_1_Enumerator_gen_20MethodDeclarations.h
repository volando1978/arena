﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>
struct Enumerator_t3645;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t913;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
struct List_1_t915;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m18693(__this, ___l, method) (( void (*) (Enumerator_t3645 *, List_1_t915 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18694(__this, method) (( Object_t * (*) (Enumerator_t3645 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::Dispose()
#define Enumerator_Dispose_m18695(__this, method) (( void (*) (Enumerator_t3645 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::VerifyState()
#define Enumerator_VerifyState_m18696(__this, method) (( void (*) (Enumerator_t3645 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::MoveNext()
#define Enumerator_MoveNext_m18697(__this, method) (( bool (*) (Enumerator_t3645 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::get_Current()
#define Enumerator_get_Current_m18698(__this, method) (( Object_t * (*) (Enumerator_t3645 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
