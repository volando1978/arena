﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>
struct  Enumerator_t215 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::l
	List_1_t178 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::current
	UpgradeVG_t133 * ___current_3;
};
