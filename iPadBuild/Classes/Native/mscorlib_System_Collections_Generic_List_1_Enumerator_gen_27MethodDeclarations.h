﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>
struct Enumerator_t3828;
// System.Object
struct Object_t;
// cabezaScr/Frame
struct Frame_t770;
// System.Collections.Generic.List`1<cabezaScr/Frame>
struct List_1_t772;

// System.Void System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m21020(__this, ___l, method) (( void (*) (Enumerator_t3828 *, List_1_t772 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21021(__this, method) (( Object_t * (*) (Enumerator_t3828 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::Dispose()
#define Enumerator_Dispose_m21022(__this, method) (( void (*) (Enumerator_t3828 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::VerifyState()
#define Enumerator_VerifyState_m21023(__this, method) (( void (*) (Enumerator_t3828 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::MoveNext()
#define Enumerator_MoveNext_m21024(__this, method) (( bool (*) (Enumerator_t3828 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<cabezaScr/Frame>::get_Current()
#define Enumerator_get_Current_m21025(__this, method) (( Frame_t770 * (*) (Enumerator_t3828 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
