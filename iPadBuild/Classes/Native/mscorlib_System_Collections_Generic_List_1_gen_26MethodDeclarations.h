﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t1192;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t4419;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t4420;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t4421;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t3892;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t3888;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t3896;
// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t3897;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1154;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m5776_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1__ctor_m5776(__this, method) (( void (*) (List_1_t1192 *, MethodInfo*))List_1__ctor_m5776_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m21803_gshared (List_1_t1192 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m21803(__this, ___collection, method) (( void (*) (List_1_t1192 *, Object_t*, MethodInfo*))List_1__ctor_m21803_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m21804_gshared (List_1_t1192 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m21804(__this, ___capacity, method) (( void (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1__ctor_m21804_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m21805_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m21805(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m21805_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21806_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21806(__this, method) (( Object_t* (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21806_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21807_gshared (List_1_t1192 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m21807(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1192 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m21807_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m21808_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21808(__this, method) (( Object_t * (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m21808_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m21809_gshared (List_1_t1192 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m21809(__this, ___item, method) (( int32_t (*) (List_1_t1192 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m21809_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m21810_gshared (List_1_t1192 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m21810(__this, ___item, method) (( bool (*) (List_1_t1192 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m21810_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m21811_gshared (List_1_t1192 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m21811(__this, ___item, method) (( int32_t (*) (List_1_t1192 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m21811_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m21812_gshared (List_1_t1192 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m21812(__this, ___index, ___item, method) (( void (*) (List_1_t1192 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m21812_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m21813_gshared (List_1_t1192 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m21813(__this, ___item, method) (( void (*) (List_1_t1192 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m21813_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21814_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21814(__this, method) (( bool (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21814_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m21815_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21815(__this, method) (( bool (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m21815_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m21816_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m21816(__this, method) (( Object_t * (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m21816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m21817_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m21817(__this, method) (( bool (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m21817_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m21818_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m21818(__this, method) (( bool (*) (List_1_t1192 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m21818_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m21819_gshared (List_1_t1192 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m21819(__this, ___index, method) (( Object_t * (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m21819_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m21820_gshared (List_1_t1192 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m21820(__this, ___index, ___value, method) (( void (*) (List_1_t1192 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m21820_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m21821_gshared (List_1_t1192 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define List_1_Add_m21821(__this, ___item, method) (( void (*) (List_1_t1192 *, RaycastResult_t1187 , MethodInfo*))List_1_Add_m21821_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m21822_gshared (List_1_t1192 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m21822(__this, ___newCount, method) (( void (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m21822_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m21823_gshared (List_1_t1192 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m21823(__this, ___collection, method) (( void (*) (List_1_t1192 *, Object_t*, MethodInfo*))List_1_AddCollection_m21823_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m21824_gshared (List_1_t1192 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m21824(__this, ___enumerable, method) (( void (*) (List_1_t1192 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m21824_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m21825_gshared (List_1_t1192 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m21825(__this, ___collection, method) (( void (*) (List_1_t1192 *, Object_t*, MethodInfo*))List_1_AddRange_m21825_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3892 * List_1_AsReadOnly_m21826_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m21826(__this, method) (( ReadOnlyCollection_1_t3892 * (*) (List_1_t1192 *, MethodInfo*))List_1_AsReadOnly_m21826_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m21827_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_Clear_m21827(__this, method) (( void (*) (List_1_t1192 *, MethodInfo*))List_1_Clear_m21827_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m21828_gshared (List_1_t1192 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define List_1_Contains_m21828(__this, ___item, method) (( bool (*) (List_1_t1192 *, RaycastResult_t1187 , MethodInfo*))List_1_Contains_m21828_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m21829_gshared (List_1_t1192 * __this, RaycastResultU5BU5D_t3888* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m21829(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1192 *, RaycastResultU5BU5D_t3888*, int32_t, MethodInfo*))List_1_CopyTo_m21829_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t1187  List_1_Find_m21830_gshared (List_1_t1192 * __this, Predicate_1_t3896 * ___match, MethodInfo* method);
#define List_1_Find_m21830(__this, ___match, method) (( RaycastResult_t1187  (*) (List_1_t1192 *, Predicate_1_t3896 *, MethodInfo*))List_1_Find_m21830_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m21831_gshared (Object_t * __this /* static, unused */, Predicate_1_t3896 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m21831(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3896 *, MethodInfo*))List_1_CheckMatch_m21831_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m21832_gshared (List_1_t1192 * __this, Predicate_1_t3896 * ___match, MethodInfo* method);
#define List_1_FindIndex_m21832(__this, ___match, method) (( int32_t (*) (List_1_t1192 *, Predicate_1_t3896 *, MethodInfo*))List_1_FindIndex_m21832_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m21833_gshared (List_1_t1192 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3896 * ___match, MethodInfo* method);
#define List_1_GetIndex_m21833(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1192 *, int32_t, int32_t, Predicate_1_t3896 *, MethodInfo*))List_1_GetIndex_m21833_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m21834_gshared (List_1_t1192 * __this, Action_1_t3897 * ___action, MethodInfo* method);
#define List_1_ForEach_m21834(__this, ___action, method) (( void (*) (List_1_t1192 *, Action_1_t3897 *, MethodInfo*))List_1_ForEach_m21834_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t3890  List_1_GetEnumerator_m21835_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m21835(__this, method) (( Enumerator_t3890  (*) (List_1_t1192 *, MethodInfo*))List_1_GetEnumerator_m21835_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m21836_gshared (List_1_t1192 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define List_1_IndexOf_m21836(__this, ___item, method) (( int32_t (*) (List_1_t1192 *, RaycastResult_t1187 , MethodInfo*))List_1_IndexOf_m21836_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m21837_gshared (List_1_t1192 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m21837(__this, ___start, ___delta, method) (( void (*) (List_1_t1192 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m21837_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m21838_gshared (List_1_t1192 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m21838(__this, ___index, method) (( void (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_CheckIndex_m21838_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m21839_gshared (List_1_t1192 * __this, int32_t ___index, RaycastResult_t1187  ___item, MethodInfo* method);
#define List_1_Insert_m21839(__this, ___index, ___item, method) (( void (*) (List_1_t1192 *, int32_t, RaycastResult_t1187 , MethodInfo*))List_1_Insert_m21839_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m21840_gshared (List_1_t1192 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m21840(__this, ___collection, method) (( void (*) (List_1_t1192 *, Object_t*, MethodInfo*))List_1_CheckCollection_m21840_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m21841_gshared (List_1_t1192 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define List_1_Remove_m21841(__this, ___item, method) (( bool (*) (List_1_t1192 *, RaycastResult_t1187 , MethodInfo*))List_1_Remove_m21841_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m21842_gshared (List_1_t1192 * __this, Predicate_1_t3896 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m21842(__this, ___match, method) (( int32_t (*) (List_1_t1192 *, Predicate_1_t3896 *, MethodInfo*))List_1_RemoveAll_m21842_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m21843_gshared (List_1_t1192 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m21843(__this, ___index, method) (( void (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_RemoveAt_m21843_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m21844_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_Reverse_m21844(__this, method) (( void (*) (List_1_t1192 *, MethodInfo*))List_1_Reverse_m21844_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m21845_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_Sort_m21845(__this, method) (( void (*) (List_1_t1192 *, MethodInfo*))List_1_Sort_m21845_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m5741_gshared (List_1_t1192 * __this, Comparison_1_t1154 * ___comparison, MethodInfo* method);
#define List_1_Sort_m5741(__this, ___comparison, method) (( void (*) (List_1_t1192 *, Comparison_1_t1154 *, MethodInfo*))List_1_Sort_m5741_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t3888* List_1_ToArray_m21846_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_ToArray_m21846(__this, method) (( RaycastResultU5BU5D_t3888* (*) (List_1_t1192 *, MethodInfo*))List_1_ToArray_m21846_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m21847_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_TrimExcess_m21847(__this, method) (( void (*) (List_1_t1192 *, MethodInfo*))List_1_TrimExcess_m21847_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m21848_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_get_Capacity_m21848(__this, method) (( int32_t (*) (List_1_t1192 *, MethodInfo*))List_1_get_Capacity_m21848_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m21849_gshared (List_1_t1192 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m21849(__this, ___value, method) (( void (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_set_Capacity_m21849_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m21850_gshared (List_1_t1192 * __this, MethodInfo* method);
#define List_1_get_Count_m21850(__this, method) (( int32_t (*) (List_1_t1192 *, MethodInfo*))List_1_get_Count_m21850_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t1187  List_1_get_Item_m21851_gshared (List_1_t1192 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m21851(__this, ___index, method) (( RaycastResult_t1187  (*) (List_1_t1192 *, int32_t, MethodInfo*))List_1_get_Item_m21851_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m21852_gshared (List_1_t1192 * __this, int32_t ___index, RaycastResult_t1187  ___value, MethodInfo* method);
#define List_1_set_Item_m21852(__this, ___index, ___value, method) (( void (*) (List_1_t1192 *, int32_t, RaycastResult_t1187 , MethodInfo*))List_1_set_Item_m21852_gshared)(__this, ___index, ___value, method)
