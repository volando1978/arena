﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t592;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfig::.ctor(System.IntPtr)
extern "C" void RealtimeRoomConfig__ctor_m2997 (RealtimeRoomConfig_t592 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeRoomConfig::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealtimeRoomConfig_CallDispose_m2998 (RealtimeRoomConfig_t592 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig GooglePlayGames.Native.PInvoke.RealtimeRoomConfig::FromPointer(System.IntPtr)
extern "C" RealtimeRoomConfig_t592 * RealtimeRoomConfig_FromPointer_m2999 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
