﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t1156;

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
extern "C" void UIBehaviour__ctor_m4649 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
extern "C" void UIBehaviour_Awake_m4650 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
extern "C" void UIBehaviour_OnEnable_m4651 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
extern "C" void UIBehaviour_Start_m4652 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
extern "C" void UIBehaviour_OnDisable_m4653 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
extern "C" void UIBehaviour_OnDestroy_m4654 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
extern "C" bool UIBehaviour_IsActive_m4655 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m4656 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m4657 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
extern "C" void UIBehaviour_OnTransformParentChanged_m4658 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m4659 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
extern "C" void UIBehaviour_OnCanvasGroupChanged_m4660 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
extern "C" bool UIBehaviour_IsDestroyed_m4661 (UIBehaviour_t1156 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
