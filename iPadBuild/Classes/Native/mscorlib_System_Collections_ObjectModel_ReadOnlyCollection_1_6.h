﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Soomla.Schedule/DateTimeRange>
struct IList_1_t3510;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Schedule/DateTimeRange>
struct  ReadOnlyCollection_1_t3511  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Schedule/DateTimeRange>::list
	Object_t* ___list_0;
};
