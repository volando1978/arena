﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Action_3_t872;
// System.Object
struct Object_t;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"

// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::.ctor(System.Object,System.IntPtr)
// System.Action`3<System.Int32,System.Object,System.Object>
#include "System_Core_System_Action_3_gen_8MethodDeclarations.h"
#define Action_3__ctor_m3750(__this, ___object, ___method, method) (( void (*) (Action_3_t872 *, Object_t *, IntPtr_t, MethodInfo*))Action_3__ctor_m19345_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m19737(__this, ___arg1, ___arg2, ___arg3, method) (( void (*) (Action_3_t872 *, int32_t, String_t*, MultiplayerInvitation_t601 *, MethodInfo*))Action_3_Invoke_m19347_gshared)(__this, ___arg1, ___arg2, ___arg3, method)
// System.IAsyncResult System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m19738(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method) (( Object_t * (*) (Action_3_t872 *, int32_t, String_t*, MultiplayerInvitation_t601 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_3_BeginInvoke_m19349_gshared)(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method)
// System.Void System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m19739(__this, ___result, method) (( void (*) (Action_3_t872 *, Object_t *, MethodInfo*))Action_3_EndInvoke_m19351_gshared)(__this, ___result, method)
