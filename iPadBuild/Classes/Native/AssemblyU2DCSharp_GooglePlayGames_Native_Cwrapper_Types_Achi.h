﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
struct  AchievementType_t506 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/AchievementType::value__
	int32_t ___value___1;
};
