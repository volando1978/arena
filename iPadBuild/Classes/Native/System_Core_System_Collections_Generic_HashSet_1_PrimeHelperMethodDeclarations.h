﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
struct PrimeHelper_t3583;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern "C" void PrimeHelper__cctor_m17656_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define PrimeHelper__cctor_m17656(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))PrimeHelper__cctor_m17656_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m17657_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_TestPrime_m17657(__this /* static, unused */, ___x, method) (( bool (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_TestPrime_m17657_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern "C" int32_t PrimeHelper_CalcPrime_m17658_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_CalcPrime_m17658(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_CalcPrime_m17658_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern "C" int32_t PrimeHelper_ToPrime_m17659_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_ToPrime_m17659(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_ToPrime_m17659_gshared)(__this /* static, unused */, ___x, method)
