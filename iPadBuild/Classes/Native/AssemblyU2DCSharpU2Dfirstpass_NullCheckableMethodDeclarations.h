﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// NullCheckable
struct NullCheckable_t45;

// System.Void NullCheckable::.ctor()
extern "C" void NullCheckable__ctor_m181 (NullCheckable_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NullCheckable::op_Implicit(NullCheckable)
extern "C" bool NullCheckable_op_Implicit_m182 (Object_t * __this /* static, unused */, NullCheckable_t45 * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
