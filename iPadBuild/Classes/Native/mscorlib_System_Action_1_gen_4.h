﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>
struct  Action_1_t100  : public MulticastDelegate_t22
{
};
