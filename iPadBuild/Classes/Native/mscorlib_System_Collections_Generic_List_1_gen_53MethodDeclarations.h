﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct List_1_t3465;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct IEnumerable_1_t4270;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t4271;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct ICollection_1_t4272;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct ReadOnlyCollection_1_t4273;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>[]
struct UnityKeyValuePair_2U5BU5D_t3464;
// System.Predicate`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct Predicate_1_t4274;
// System.Action`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct Action_1_t4275;
// System.Comparison`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct Comparison_1_t4276;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_54.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m15912(__this, method) (( void (*) (List_1_t3465 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15913(__this, ___collection, method) (( void (*) (List_1_t3465 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::.ctor(System.Int32)
#define List_1__ctor_m15914(__this, ___capacity, method) (( void (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::.cctor()
#define List_1__cctor_m15915(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15916(__this, method) (( Object_t* (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15917(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3465 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15918(__this, method) (( Object_t * (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15919(__this, ___item, method) (( int32_t (*) (List_1_t3465 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15920(__this, ___item, method) (( bool (*) (List_1_t3465 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15921(__this, ___item, method) (( int32_t (*) (List_1_t3465 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15922(__this, ___index, ___item, method) (( void (*) (List_1_t3465 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15923(__this, ___item, method) (( void (*) (List_1_t3465 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15924(__this, method) (( bool (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15925(__this, method) (( bool (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15926(__this, method) (( Object_t * (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15927(__this, method) (( bool (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15928(__this, method) (( bool (*) (List_1_t3465 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15929(__this, ___index, method) (( Object_t * (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15930(__this, ___index, ___value, method) (( void (*) (List_1_t3465 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Add(T)
#define List_1_Add_m15931(__this, ___item, method) (( void (*) (List_1_t3465 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15932(__this, ___newCount, method) (( void (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15933(__this, ___collection, method) (( void (*) (List_1_t3465 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15934(__this, ___enumerable, method) (( void (*) (List_1_t3465 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15935(__this, ___collection, method) (( void (*) (List_1_t3465 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::AsReadOnly()
#define List_1_AsReadOnly_m15936(__this, method) (( ReadOnlyCollection_1_t4273 * (*) (List_1_t3465 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Clear()
#define List_1_Clear_m15937(__this, method) (( void (*) (List_1_t3465 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Contains(T)
#define List_1_Contains_m15938(__this, ___item, method) (( bool (*) (List_1_t3465 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15939(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3465 *, UnityKeyValuePair_2U5BU5D_t3464*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Find(System.Predicate`1<T>)
#define List_1_Find_m15940(__this, ___match, method) (( UnityKeyValuePair_2_t3457 * (*) (List_1_t3465 *, Predicate_1_t4274 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15941(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4274 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m15942(__this, ___match, method) (( int32_t (*) (List_1_t3465 *, Predicate_1_t4274 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15943(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3465 *, int32_t, int32_t, Predicate_1_t4274 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m15944(__this, ___action, method) (( void (*) (List_1_t3465 *, Action_1_t4275 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::GetEnumerator()
#define List_1_GetEnumerator_m15945(__this, method) (( Enumerator_t4277  (*) (List_1_t3465 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::IndexOf(T)
#define List_1_IndexOf_m15946(__this, ___item, method) (( int32_t (*) (List_1_t3465 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15947(__this, ___start, ___delta, method) (( void (*) (List_1_t3465 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15948(__this, ___index, method) (( void (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Insert(System.Int32,T)
#define List_1_Insert_m15949(__this, ___index, ___item, method) (( void (*) (List_1_t3465 *, int32_t, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15950(__this, ___collection, method) (( void (*) (List_1_t3465 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Remove(T)
#define List_1_Remove_m15951(__this, ___item, method) (( bool (*) (List_1_t3465 *, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15952(__this, ___match, method) (( int32_t (*) (List_1_t3465 *, Predicate_1_t4274 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15953(__this, ___index, method) (( void (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Reverse()
#define List_1_Reverse_m15954(__this, method) (( void (*) (List_1_t3465 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Sort()
#define List_1_Sort_m15955(__this, method) (( void (*) (List_1_t3465 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15956(__this, ___comparison, method) (( void (*) (List_1_t3465 *, Comparison_1_t4276 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::ToArray()
#define List_1_ToArray_m15957(__this, method) (( UnityKeyValuePair_2U5BU5D_t3464* (*) (List_1_t3465 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::TrimExcess()
#define List_1_TrimExcess_m15958(__this, method) (( void (*) (List_1_t3465 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::get_Capacity()
#define List_1_get_Capacity_m15959(__this, method) (( int32_t (*) (List_1_t3465 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15960(__this, ___value, method) (( void (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::get_Count()
#define List_1_get_Count_m15961(__this, method) (( int32_t (*) (List_1_t3465 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::get_Item(System.Int32)
#define List_1_get_Item_m15962(__this, ___index, method) (( UnityKeyValuePair_2_t3457 * (*) (List_1_t3465 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::set_Item(System.Int32,T)
#define List_1_set_Item_m15963(__this, ___index, ___value, method) (( void (*) (List_1_t3465 *, int32_t, UnityKeyValuePair_2_t3457 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
