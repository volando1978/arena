﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F
struct  U3CUpdateAchievementU3Ec__AnonStorey1F_t528  : public Object_t
{
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::callback
	Action_1_t98 * ___callback_0;
	// System.String GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::achId
	String_t* ___achId_1;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::<>f__this
	NativeClient_t524 * ___U3CU3Ef__this_2;
};
