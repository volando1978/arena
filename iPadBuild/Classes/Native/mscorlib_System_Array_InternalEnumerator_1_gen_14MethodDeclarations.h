﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>
struct InternalEnumerator_1_t3555;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17296(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3555 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17297(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3555 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>::Dispose()
#define InternalEnumerator_1_Dispose_m17298(__this, method) (( void (*) (InternalEnumerator_1_t3555 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17299(__this, method) (( bool (*) (InternalEnumerator_1_t3555 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Soomla.Store.VirtualCategory>::get_Current()
#define InternalEnumerator_1_get_Current_m17300(__this, method) (( VirtualCategory_t126 * (*) (InternalEnumerator_1_t3555 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
