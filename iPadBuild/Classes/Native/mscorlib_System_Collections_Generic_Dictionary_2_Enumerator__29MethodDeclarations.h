﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t4188;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t2008;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__28MethodDeclarations.h"
#define Enumerator__ctor_m25749(__this, ___dictionary, method) (( void (*) (Enumerator_t4188 *, Dictionary_2_t2008 *, MethodInfo*))Enumerator__ctor_m25647_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25750(__this, method) (( Object_t * (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25648_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25751(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25752(__this, method) (( Object_t * (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25753(__this, method) (( Object_t * (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m25754(__this, method) (( bool (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_MoveNext_m25652_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m25755(__this, method) (( KeyValuePair_2_t4185  (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_get_Current_m25653_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25756(__this, method) (( String_t* (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_get_CurrentKey_m25654_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25757(__this, method) (( bool (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_get_CurrentValue_m25655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m25758(__this, method) (( void (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_VerifyState_m25656_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25759(__this, method) (( void (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_VerifyCurrent_m25657_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m25760(__this, method) (( void (*) (Enumerator_t4188 *, MethodInfo*))Enumerator_Dispose_m25658_gshared)(__this, method)
