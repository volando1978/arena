﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>
struct List_1_t114;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.VirtualCurrency>
struct IEnumerable_1_t223;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualCurrency>
struct IEnumerator_1_t4321;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.VirtualCurrency>
struct ICollection_1_t4322;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualCurrency>
struct ReadOnlyCollection_1_t3591;
// Soomla.Store.VirtualCurrency[]
struct VirtualCurrencyU5BU5D_t172;
// System.Predicate`1<Soomla.Store.VirtualCurrency>
struct Predicate_1_t3592;
// System.Action`1<Soomla.Store.VirtualCurrency>
struct Action_1_t3593;
// System.Comparison`1<Soomla.Store.VirtualCurrency>
struct Comparison_1_t3594;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m1009(__this, method) (( void (*) (List_1_t114 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17760(__this, ___collection, method) (( void (*) (List_1_t114 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::.ctor(System.Int32)
#define List_1__ctor_m17761(__this, ___capacity, method) (( void (*) (List_1_t114 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::.cctor()
#define List_1__cctor_m17762(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17763(__this, method) (( Object_t* (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17764(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t114 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17765(__this, method) (( Object_t * (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17766(__this, ___item, method) (( int32_t (*) (List_1_t114 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17767(__this, ___item, method) (( bool (*) (List_1_t114 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17768(__this, ___item, method) (( int32_t (*) (List_1_t114 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17769(__this, ___index, ___item, method) (( void (*) (List_1_t114 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17770(__this, ___item, method) (( void (*) (List_1_t114 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17771(__this, method) (( bool (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17772(__this, method) (( bool (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17773(__this, method) (( Object_t * (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17774(__this, method) (( bool (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17775(__this, method) (( bool (*) (List_1_t114 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17776(__this, ___index, method) (( Object_t * (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17777(__this, ___index, ___value, method) (( void (*) (List_1_t114 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Add(T)
#define List_1_Add_m17778(__this, ___item, method) (( void (*) (List_1_t114 *, VirtualCurrency_t77 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17779(__this, ___newCount, method) (( void (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17780(__this, ___collection, method) (( void (*) (List_1_t114 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17781(__this, ___enumerable, method) (( void (*) (List_1_t114 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17782(__this, ___collection, method) (( void (*) (List_1_t114 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::AsReadOnly()
#define List_1_AsReadOnly_m17783(__this, method) (( ReadOnlyCollection_1_t3591 * (*) (List_1_t114 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Clear()
#define List_1_Clear_m17784(__this, method) (( void (*) (List_1_t114 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Contains(T)
#define List_1_Contains_m17785(__this, ___item, method) (( bool (*) (List_1_t114 *, VirtualCurrency_t77 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17786(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t114 *, VirtualCurrencyU5BU5D_t172*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Find(System.Predicate`1<T>)
#define List_1_Find_m17787(__this, ___match, method) (( VirtualCurrency_t77 * (*) (List_1_t114 *, Predicate_1_t3592 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17788(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3592 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m17789(__this, ___match, method) (( int32_t (*) (List_1_t114 *, Predicate_1_t3592 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17790(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t114 *, int32_t, int32_t, Predicate_1_t3592 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m17791(__this, ___action, method) (( void (*) (List_1_t114 *, Action_1_t3593 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::GetEnumerator()
#define List_1_GetEnumerator_m984(__this, method) (( Enumerator_t216  (*) (List_1_t114 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::IndexOf(T)
#define List_1_IndexOf_m17792(__this, ___item, method) (( int32_t (*) (List_1_t114 *, VirtualCurrency_t77 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17793(__this, ___start, ___delta, method) (( void (*) (List_1_t114 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17794(__this, ___index, method) (( void (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Insert(System.Int32,T)
#define List_1_Insert_m17795(__this, ___index, ___item, method) (( void (*) (List_1_t114 *, int32_t, VirtualCurrency_t77 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17796(__this, ___collection, method) (( void (*) (List_1_t114 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Remove(T)
#define List_1_Remove_m17797(__this, ___item, method) (( bool (*) (List_1_t114 *, VirtualCurrency_t77 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17798(__this, ___match, method) (( int32_t (*) (List_1_t114 *, Predicate_1_t3592 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17799(__this, ___index, method) (( void (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Reverse()
#define List_1_Reverse_m17800(__this, method) (( void (*) (List_1_t114 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Sort()
#define List_1_Sort_m17801(__this, method) (( void (*) (List_1_t114 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17802(__this, ___comparison, method) (( void (*) (List_1_t114 *, Comparison_1_t3594 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::ToArray()
#define List_1_ToArray_m17803(__this, method) (( VirtualCurrencyU5BU5D_t172* (*) (List_1_t114 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::TrimExcess()
#define List_1_TrimExcess_m17804(__this, method) (( void (*) (List_1_t114 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::get_Capacity()
#define List_1_get_Capacity_m17805(__this, method) (( int32_t (*) (List_1_t114 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17806(__this, ___value, method) (( void (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::get_Count()
#define List_1_get_Count_m17807(__this, method) (( int32_t (*) (List_1_t114 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::get_Item(System.Int32)
#define List_1_get_Item_m17808(__this, ___index, method) (( VirtualCurrency_t77 * (*) (List_1_t114 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>::set_Item(System.Int32,T)
#define List_1_set_Item_m17809(__this, ___index, ___value, method) (( void (*) (List_1_t114 *, int32_t, VirtualCurrency_t77 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
