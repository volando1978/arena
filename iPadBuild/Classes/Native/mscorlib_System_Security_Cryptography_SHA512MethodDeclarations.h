﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA512
struct SHA512_t2713;

// System.Void System.Security.Cryptography.SHA512::.ctor()
extern "C" void SHA512__ctor_m13160 (SHA512_t2713 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
