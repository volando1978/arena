﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BackgroundScript
struct BackgroundScript_t723;

// System.Void BackgroundScript::.ctor()
extern "C" void BackgroundScript__ctor_m3143 (BackgroundScript_t723 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundScript::Start()
extern "C" void BackgroundScript_Start_m3144 (BackgroundScript_t723 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackgroundScript::fillBg()
extern "C" void BackgroundScript_fillBg_m3145 (BackgroundScript_t723 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
