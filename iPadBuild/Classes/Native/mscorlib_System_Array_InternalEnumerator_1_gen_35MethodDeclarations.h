﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t4006;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_3MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m23515(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4006 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15521_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23516(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4006 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15522_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
#define InternalEnumerator_1_Dispose_m23517(__this, method) (( void (*) (InternalEnumerator_1_t4006 *, MethodInfo*))InternalEnumerator_1_Dispose_m15523_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m23518(__this, method) (( bool (*) (InternalEnumerator_1_t4006 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15524_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
#define InternalEnumerator_1_get_Current_m23519(__this, method) (( int32_t (*) (InternalEnumerator_1_t4006 *, MethodInfo*))InternalEnumerator_1_get_Current_m15525_gshared)(__this, method)
