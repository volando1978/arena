﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20334_gshared (U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20334(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey4C_2__ctor_m20334_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>::<>m__53()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20335_gshared (U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20335(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey4C_2_U3CU3Em__53_m20335_gshared)(__this, method)
