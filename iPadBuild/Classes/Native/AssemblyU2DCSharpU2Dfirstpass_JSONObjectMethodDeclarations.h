﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject
struct JSONObject_t30;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t167;
// JSONObject/AddJSONConents
struct AddJSONConents_t31;
// JSONObject[]
struct JSONObjectU5BU5D_t168;
// JSONObject/FieldNotFound
struct FieldNotFound_t32;
// JSONObject/GetFieldResponse
struct GetFieldResponse_t33;
// System.String[]
struct StringU5BU5D_t169;
// System.Collections.IEnumerable
struct IEnumerable_t38;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t170;
// System.Text.StringBuilder
struct StringBuilder_t36;
// UnityEngine.WWWForm
struct WWWForm_t171;
// JSONObject/Type
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_Type.h"

// System.Void JSONObject::.ctor(JSONObject/Type)
extern "C" void JSONObject__ctor_m98 (JSONObject_t30 * __this, int32_t ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(System.Boolean)
extern "C" void JSONObject__ctor_m99 (JSONObject_t30 * __this, bool ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(System.Single)
extern "C" void JSONObject__ctor_m100 (JSONObject_t30 * __this, float ___f, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" void JSONObject__ctor_m101 (JSONObject_t30 * __this, Dictionary_2_t165 * ___dic, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(System.Collections.Generic.Dictionary`2<System.String,JSONObject>)
extern "C" void JSONObject__ctor_m102 (JSONObject_t30 * __this, Dictionary_2_t167 * ___dic, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(JSONObject/AddJSONConents)
extern "C" void JSONObject__ctor_m103 (JSONObject_t30 * __this, AddJSONConents_t31 * ___content, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(JSONObject[])
extern "C" void JSONObject__ctor_m104 (JSONObject_t30 * __this, JSONObjectU5BU5D_t168* ___objs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor()
extern "C" void JSONObject__ctor_m105 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.ctor(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" void JSONObject__ctor_m106 (JSONObject_t30 * __this, String_t* ___str, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::.cctor()
extern "C" void JSONObject__cctor_m107 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_isContainer()
extern "C" bool JSONObject_get_isContainer_m108 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSONObject::get_Count()
extern "C" int32_t JSONObject_get_Count_m109 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single JSONObject::get_f()
extern "C" float JSONObject_get_f_m110 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::get_nullJO()
extern "C" JSONObject_t30 * JSONObject_get_nullJO_m111 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::get_obj()
extern "C" JSONObject_t30 * JSONObject_get_obj_m112 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::get_arr()
extern "C" JSONObject_t30 * JSONObject_get_arr_m113 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::StringObject(System.String)
extern "C" JSONObject_t30 * JSONObject_StringObject_m114 (Object_t * __this /* static, unused */, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Absorb(JSONObject)
extern "C" void JSONObject_Absorb_m115 (JSONObject_t30 * __this, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create()
extern "C" JSONObject_t30 * JSONObject_Create_m116 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(JSONObject/Type)
extern "C" JSONObject_t30 * JSONObject_Create_m117 (Object_t * __this /* static, unused */, int32_t ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.Boolean)
extern "C" JSONObject_t30 * JSONObject_Create_m118 (Object_t * __this /* static, unused */, bool ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.Single)
extern "C" JSONObject_t30 * JSONObject_Create_m119 (Object_t * __this /* static, unused */, float ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.Int32)
extern "C" JSONObject_t30 * JSONObject_Create_m120 (Object_t * __this /* static, unused */, int32_t ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::CreateStringObject(System.String)
extern "C" JSONObject_t30 * JSONObject_CreateStringObject_m121 (Object_t * __this /* static, unused */, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::EncodeJsString(System.String)
extern "C" String_t* JSONObject_EncodeJsString_m122 (Object_t * __this /* static, unused */, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::CreateBakedObject(System.String)
extern "C" JSONObject_t30 * JSONObject_CreateBakedObject_m123 (Object_t * __this /* static, unused */, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" JSONObject_t30 * JSONObject_Create_m124 (Object_t * __this /* static, unused */, String_t* ___val, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(JSONObject/AddJSONConents)
extern "C" JSONObject_t30 * JSONObject_Create_m125 (Object_t * __this /* static, unused */, AddJSONConents_t31 * ___content, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Create(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" JSONObject_t30 * JSONObject_Create_m126 (Object_t * __this /* static, unused */, Dictionary_2_t165 * ___dic, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Parse(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" void JSONObject_Parse_m127 (JSONObject_t30 * __this, String_t* ___str, int32_t ___maxDepth, bool ___storeExcessLevels, bool ___strict, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsNumber()
extern "C" bool JSONObject_get_IsNumber_m128 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsNull()
extern "C" bool JSONObject_get_IsNull_m129 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsString()
extern "C" bool JSONObject_get_IsString_m130 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsBool()
extern "C" bool JSONObject_get_IsBool_m131 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsArray()
extern "C" bool JSONObject_get_IsArray_m132 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::get_IsObject()
extern "C" bool JSONObject_get_IsObject_m133 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(System.Boolean)
extern "C" void JSONObject_Add_m134 (JSONObject_t30 * __this, bool ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(System.Single)
extern "C" void JSONObject_Add_m135 (JSONObject_t30 * __this, float ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(System.Int32)
extern "C" void JSONObject_Add_m136 (JSONObject_t30 * __this, int32_t ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(System.String)
extern "C" void JSONObject_Add_m137 (JSONObject_t30 * __this, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(JSONObject/AddJSONConents)
extern "C" void JSONObject_Add_m138 (JSONObject_t30 * __this, AddJSONConents_t31 * ___content, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Add(JSONObject)
extern "C" void JSONObject_Add_m139 (JSONObject_t30 * __this, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,System.Boolean)
extern "C" void JSONObject_AddField_m140 (JSONObject_t30 * __this, String_t* ___name, bool ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,System.Single)
extern "C" void JSONObject_AddField_m141 (JSONObject_t30 * __this, String_t* ___name, float ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,System.Int32)
extern "C" void JSONObject_AddField_m142 (JSONObject_t30 * __this, String_t* ___name, int32_t ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,JSONObject/AddJSONConents)
extern "C" void JSONObject_AddField_m143 (JSONObject_t30 * __this, String_t* ___name, AddJSONConents_t31 * ___content, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,System.String)
extern "C" void JSONObject_AddField_m144 (JSONObject_t30 * __this, String_t* ___name, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::AddField(System.String,JSONObject)
extern "C" void JSONObject_AddField_m145 (JSONObject_t30 * __this, String_t* ___name, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::SetField(System.String,System.Boolean)
extern "C" void JSONObject_SetField_m146 (JSONObject_t30 * __this, String_t* ___name, bool ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::SetField(System.String,System.Single)
extern "C" void JSONObject_SetField_m147 (JSONObject_t30 * __this, String_t* ___name, float ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::SetField(System.String,System.Int32)
extern "C" void JSONObject_SetField_m148 (JSONObject_t30 * __this, String_t* ___name, int32_t ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::SetField(System.String,System.String)
extern "C" void JSONObject_SetField_m149 (JSONObject_t30 * __this, String_t* ___name, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::SetField(System.String,JSONObject)
extern "C" void JSONObject_SetField_m150 (JSONObject_t30 * __this, String_t* ___name, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::RemoveField(System.String)
extern "C" void JSONObject_RemoveField_m151 (JSONObject_t30 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.Boolean&,System.String,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m152 (JSONObject_t30 * __this, bool* ___field, String_t* ___name, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.Single&,System.String,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m153 (JSONObject_t30 * __this, float* ___field, String_t* ___name, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.Int32&,System.String,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m154 (JSONObject_t30 * __this, int32_t* ___field, String_t* ___name, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.UInt32&,System.String,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m155 (JSONObject_t30 * __this, uint32_t* ___field, String_t* ___name, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.String&,System.String,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m156 (JSONObject_t30 * __this, String_t** ___field, String_t* ___name, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::GetField(System.String,JSONObject/GetFieldResponse,JSONObject/FieldNotFound)
extern "C" void JSONObject_GetField_m157 (JSONObject_t30 * __this, String_t* ___name, GetFieldResponse_t33 * ___response, FieldNotFound_t32 * ___fail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::GetField(System.String)
extern "C" JSONObject_t30 * JSONObject_GetField_m158 (JSONObject_t30 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::HasFields(System.String[])
extern "C" bool JSONObject_HasFields_m159 (JSONObject_t30 * __this, StringU5BU5D_t169* ___names, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::HasField(System.String)
extern "C" bool JSONObject_HasField_m160 (JSONObject_t30 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Clear()
extern "C" void JSONObject_Clear_m161 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::Copy()
extern "C" JSONObject_t30 * JSONObject_Copy_m162 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Merge(JSONObject)
extern "C" void JSONObject_Merge_m163 (JSONObject_t30 * __this, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::MergeRecur(JSONObject,JSONObject)
extern "C" void JSONObject_MergeRecur_m164 (Object_t * __this /* static, unused */, JSONObject_t30 * ___left, JSONObject_t30 * ___right, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Bake()
extern "C" void JSONObject_Bake_m165 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable JSONObject::BakeAsync()
extern "C" Object_t * JSONObject_BakeAsync_m166 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::print(System.Boolean)
extern "C" String_t* JSONObject_print_m167 (JSONObject_t30 * __this, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::Print(System.Boolean)
extern "C" String_t* JSONObject_Print_m168 (JSONObject_t30 * __this, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> JSONObject::PrintAsync(System.Boolean)
extern "C" Object_t* JSONObject_PrintAsync_m169 (JSONObject_t30 * __this, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable JSONObject::StringifyAsync(System.Int32,System.Text.StringBuilder,System.Boolean)
extern "C" Object_t * JSONObject_StringifyAsync_m170 (JSONObject_t30 * __this, int32_t ___depth, StringBuilder_t36 * ___builder, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::Stringify(System.Int32,System.Text.StringBuilder,System.Boolean)
extern "C" void JSONObject_Stringify_m171 (JSONObject_t30 * __this, int32_t ___depth, StringBuilder_t36 * ___builder, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::get_Item(System.Int32)
extern "C" JSONObject_t30 * JSONObject_get_Item_m172 (JSONObject_t30 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::set_Item(System.Int32,JSONObject)
extern "C" void JSONObject_set_Item_m173 (JSONObject_t30 * __this, int32_t ___index, JSONObject_t30 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject JSONObject::get_Item(System.String)
extern "C" JSONObject_t30 * JSONObject_get_Item_m174 (JSONObject_t30 * __this, String_t* ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject::set_Item(System.String,JSONObject)
extern "C" void JSONObject_set_Item_m175 (JSONObject_t30 * __this, String_t* ___index, JSONObject_t30 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::ToString()
extern "C" String_t* JSONObject_ToString_m176 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject::ToString(System.Boolean)
extern "C" String_t* JSONObject_ToString_m177 (JSONObject_t30 * __this, bool ___pretty, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> JSONObject::ToDictionary()
extern "C" Dictionary_2_t165 * JSONObject_ToDictionary_m178 (JSONObject_t30 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm JSONObject::op_Implicit(JSONObject)
extern "C" WWWForm_t171 * JSONObject_op_Implicit_m179 (Object_t * __this /* static, unused */, JSONObject_t30 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject::op_Implicit(JSONObject)
extern "C" bool JSONObject_op_Implicit_m180 (Object_t * __this /* static, unused */, JSONObject_t30 * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
