﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct List_1_t931;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct IEnumerable_1_t930;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct IEnumerator_1_t4369;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct ICollection_1_t4370;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct ReadOnlyCollection_1_t3691;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
struct ISavedGameMetadataU5BU5D_t3689;
// System.Predicate`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Predicate_1_t3692;
// System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_1_t3693;
// System.Comparison`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Comparison_1_t3695;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3854(__this, method) (( void (*) (List_1_t931 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19423(__this, ___collection, method) (( void (*) (List_1_t931 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Int32)
#define List_1__ctor_m19424(__this, ___capacity, method) (( void (*) (List_1_t931 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.cctor()
#define List_1__cctor_m19425(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19426(__this, method) (( Object_t* (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19427(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t931 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19428(__this, method) (( Object_t * (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19429(__this, ___item, method) (( int32_t (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19430(__this, ___item, method) (( bool (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19431(__this, ___item, method) (( int32_t (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19432(__this, ___index, ___item, method) (( void (*) (List_1_t931 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19433(__this, ___item, method) (( void (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19434(__this, method) (( bool (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19435(__this, method) (( bool (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19436(__this, method) (( Object_t * (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19437(__this, method) (( bool (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19438(__this, method) (( bool (*) (List_1_t931 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19439(__this, ___index, method) (( Object_t * (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19440(__this, ___index, ___value, method) (( void (*) (List_1_t931 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Add(T)
#define List_1_Add_m19441(__this, ___item, method) (( void (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19442(__this, ___newCount, method) (( void (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19443(__this, ___collection, method) (( void (*) (List_1_t931 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19444(__this, ___enumerable, method) (( void (*) (List_1_t931 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19445(__this, ___collection, method) (( void (*) (List_1_t931 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::AsReadOnly()
#define List_1_AsReadOnly_m19446(__this, method) (( ReadOnlyCollection_1_t3691 * (*) (List_1_t931 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Clear()
#define List_1_Clear_m19447(__this, method) (( void (*) (List_1_t931 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Contains(T)
#define List_1_Contains_m19448(__this, ___item, method) (( bool (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19449(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t931 *, ISavedGameMetadataU5BU5D_t3689*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Find(System.Predicate`1<T>)
#define List_1_Find_m19450(__this, ___match, method) (( Object_t * (*) (List_1_t931 *, Predicate_1_t3692 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19451(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3692 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m19452(__this, ___match, method) (( int32_t (*) (List_1_t931 *, Predicate_1_t3692 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19453(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t931 *, int32_t, int32_t, Predicate_1_t3692 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m19454(__this, ___action, method) (( void (*) (List_1_t931 *, Action_1_t3693 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::GetEnumerator()
#define List_1_GetEnumerator_m19455(__this, method) (( Enumerator_t3694  (*) (List_1_t931 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::IndexOf(T)
#define List_1_IndexOf_m19456(__this, ___item, method) (( int32_t (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19457(__this, ___start, ___delta, method) (( void (*) (List_1_t931 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19458(__this, ___index, method) (( void (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Insert(System.Int32,T)
#define List_1_Insert_m19459(__this, ___index, ___item, method) (( void (*) (List_1_t931 *, int32_t, Object_t *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19460(__this, ___collection, method) (( void (*) (List_1_t931 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Remove(T)
#define List_1_Remove_m19461(__this, ___item, method) (( bool (*) (List_1_t931 *, Object_t *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19462(__this, ___match, method) (( int32_t (*) (List_1_t931 *, Predicate_1_t3692 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19463(__this, ___index, method) (( void (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Reverse()
#define List_1_Reverse_m19464(__this, method) (( void (*) (List_1_t931 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Sort()
#define List_1_Sort_m19465(__this, method) (( void (*) (List_1_t931 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19466(__this, ___comparison, method) (( void (*) (List_1_t931 *, Comparison_1_t3695 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::ToArray()
#define List_1_ToArray_m19467(__this, method) (( ISavedGameMetadataU5BU5D_t3689* (*) (List_1_t931 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::TrimExcess()
#define List_1_TrimExcess_m19468(__this, method) (( void (*) (List_1_t931 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::get_Capacity()
#define List_1_get_Capacity_m19469(__this, method) (( int32_t (*) (List_1_t931 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19470(__this, ___value, method) (( void (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::get_Count()
#define List_1_get_Count_m19471(__this, method) (( int32_t (*) (List_1_t931 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::get_Item(System.Int32)
#define List_1_get_Item_m19472(__this, ___index, method) (( Object_t * (*) (List_1_t931 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::set_Item(System.Int32,T)
#define List_1_set_Item_m19473(__this, ___index, ___value, method) (( void (*) (List_1_t931 *, int32_t, Object_t *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
