﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t3892;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t3891;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t3888;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t4420;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m21864_gshared (ReadOnlyCollection_1_t3892 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m21864(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3892 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m21864_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21865_gshared (ReadOnlyCollection_1_t3892 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21865(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3892 *, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m21865_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21866_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21866(__this, method) (( void (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m21866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21867_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, RaycastResult_t1187  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21867(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m21867_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21868_gshared (ReadOnlyCollection_1_t3892 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21868(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m21868_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21869_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21869(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m21869_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" RaycastResult_t1187  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21870_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21870(__this, ___index, method) (( RaycastResult_t1187  (*) (ReadOnlyCollection_1_t3892 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m21870_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21871_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, RaycastResult_t1187  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21871(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m21871_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21872_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21872(__this, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21872_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21873_gshared (ReadOnlyCollection_1_t3892 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21873(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3892 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m21873_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21874_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21874(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m21874_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m21875_gshared (ReadOnlyCollection_1_t3892 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m21875(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3892 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m21875_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m21876_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m21876(__this, method) (( void (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m21876_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m21877_gshared (ReadOnlyCollection_1_t3892 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m21877(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m21877_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21878_gshared (ReadOnlyCollection_1_t3892 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21878(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3892 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m21878_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m21879_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m21879(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m21879_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m21880_gshared (ReadOnlyCollection_1_t3892 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m21880(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3892 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m21880_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21881_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21881(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m21881_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21882_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21882(__this, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m21882_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21883_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21883(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m21883_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21884_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21884(__this, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m21884_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21885_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21885(__this, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m21885_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m21886_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m21886(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3892 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m21886_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m21887_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m21887(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3892 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m21887_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m21888_gshared (ReadOnlyCollection_1_t3892 * __this, RaycastResult_t1187  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m21888(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3892 *, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_Contains_m21888_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m21889_gshared (ReadOnlyCollection_1_t3892 * __this, RaycastResultU5BU5D_t3888* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m21889(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3892 *, RaycastResultU5BU5D_t3888*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m21889_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m21890_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m21890(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m21890_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m21891_gshared (ReadOnlyCollection_1_t3892 * __this, RaycastResult_t1187  ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m21891(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3892 *, RaycastResult_t1187 , MethodInfo*))ReadOnlyCollection_1_IndexOf_m21891_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m21892_gshared (ReadOnlyCollection_1_t3892 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m21892(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3892 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m21892_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t1187  ReadOnlyCollection_1_get_Item_m21893_gshared (ReadOnlyCollection_1_t3892 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m21893(__this, ___index, method) (( RaycastResult_t1187  (*) (ReadOnlyCollection_1_t3892 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m21893_gshared)(__this, ___index, method)
