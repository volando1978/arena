﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// playerMovement
struct  playerMovement_t830  : public MonoBehaviour_t26
{
	// System.Single playerMovement::speed
	float ___speed_2;
	// System.Boolean playerMovement::isDead
	bool ___isDead_3;
	// System.Boolean playerMovement::isMoving
	bool ___isMoving_4;
	// UnityEngine.GameObject playerMovement::enemyController
	GameObject_t144 * ___enemyController_5;
	// UnityEngine.GameObject playerMovement::movingText
	GameObject_t144 * ___movingText_6;
	// UnityEngine.GameObject playerMovement::feedback
	GameObject_t144 * ___feedback_7;
	// UnityEngine.GameObject playerMovement::closest
	GameObject_t144 * ___closest_8;
	// UnityEngine.GameObject playerMovement::rayo
	GameObject_t144 * ___rayo_9;
	// UnityEngine.GameObject playerMovement::dust
	GameObject_t144 * ___dust_10;
	// UnityEngine.GameObject playerMovement::currentDust
	GameObject_t144 * ___currentDust_11;
	// System.Single playerMovement::sizeDust
	float ___sizeDust_12;
	// UnityEngine.GameObject playerMovement::bulletFeedbackObj
	GameObject_t144 * ___bulletFeedbackObj_13;
	// System.Single playerMovement::accX
	float ___accX_14;
	// System.Single playerMovement::accY
	float ___accY_15;
	// System.Single playerMovement::speedX
	float ___speedX_16;
	// System.Single playerMovement::speedY
	float ___speedY_17;
	// System.Single playerMovement::decc
	float ___decc_18;
	// System.Single playerMovement::acceleartionTouch
	float ___acceleartionTouch_19;
};
