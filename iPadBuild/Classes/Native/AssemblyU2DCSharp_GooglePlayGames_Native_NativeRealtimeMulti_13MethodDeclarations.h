﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41
struct U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585;
// System.String
struct String_t;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::.ctor()
extern "C" void U3CHandleConnectedSetChangedU3Ec__AnonStorey41__ctor_m2420 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__41(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__41_m2421 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__42(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__42_m2422 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__43(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__43_m2423 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::<>m__44(System.String)
extern "C" bool U3CHandleConnectedSetChangedU3Ec__AnonStorey41_U3CU3Em__44_m2424 (U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585 * __this, String_t* ___peer, MethodInfo* method) IL2CPP_METHOD_ATTR;
