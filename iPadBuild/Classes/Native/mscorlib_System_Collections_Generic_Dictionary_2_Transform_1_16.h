﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.Object>
struct  Transform_1_t3662  : public MulticastDelegate_t22
{
};
