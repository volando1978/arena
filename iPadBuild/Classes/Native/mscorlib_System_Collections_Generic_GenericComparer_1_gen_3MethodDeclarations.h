﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t4194;

// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C" void GenericComparer_1__ctor_m25785_gshared (GenericComparer_1_t4194 * __this, MethodInfo* method);
#define GenericComparer_1__ctor_m25785(__this, method) (( void (*) (GenericComparer_1_t4194 *, MethodInfo*))GenericComparer_1__ctor_m25785_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m25786_gshared (GenericComparer_1_t4194 * __this, int32_t ___x, int32_t ___y, MethodInfo* method);
#define GenericComparer_1_Compare_m25786(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t4194 *, int32_t, int32_t, MethodInfo*))GenericComparer_1_Compare_m25786_gshared)(__this, ___x, ___y, method)
