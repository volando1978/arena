﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct HashSet_1_t583;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t103;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/MessagingEnabledState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_9.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState
struct  ConnectingState_t584  : public MessagingEnabledState_t580
{
	// System.Collections.Generic.HashSet`1<System.String> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::mConnectedParticipants
	HashSet_1_t103 * ___mConnectedParticipants_10;
	// System.Single GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::mPercentComplete
	float ___mPercentComplete_11;
	// System.Single GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::mPercentPerParticipant
	float ___mPercentPerParticipant_12;
};
struct ConnectingState_t584_StaticFields{
	// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ConnectingState::FailedStatuses
	HashSet_1_t583 * ___FailedStatuses_9;
};
