﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t3423;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C" void Comparer_1__ctor_m15515_gshared (Comparer_1_t3423 * __this, MethodInfo* method);
#define Comparer_1__ctor_m15515(__this, method) (( void (*) (Comparer_1_t3423 *, MethodInfo*))Comparer_1__ctor_m15515_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern "C" void Comparer_1__cctor_m15516_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1__cctor_m15516(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1__cctor_m15516_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m15517_gshared (Comparer_1_t3423 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m15517(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3423 *, Object_t *, Object_t *, MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m15517_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C" Comparer_1_t3423 * Comparer_1_get_Default_m15518_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1_get_Default_m15518(__this /* static, unused */, method) (( Comparer_1_t3423 * (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1_get_Default_m15518_gshared)(__this /* static, unused */, method)
