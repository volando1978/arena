﻿#pragma once
#include <stdint.h>
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Soomla.Store.PurchasableVirtualItem>
struct  Action_1_t96  : public MulticastDelegate_t22
{
};
