﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t1378;
// UnityEngine.Collider2D
struct Collider2D_t1379;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1578;
// UnityEngine.Transform
struct Transform_t809;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t739  RaycastHit2D_get_point_m5827 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t739  RaycastHit2D_get_normal_m5828 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m5901 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t1379 * RaycastHit2D_get_collider_m5829 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t1578 * RaycastHit2D_get_rigidbody_m7155 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t809 * RaycastHit2D_get_transform_m5830 (RaycastHit2D_t1378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
