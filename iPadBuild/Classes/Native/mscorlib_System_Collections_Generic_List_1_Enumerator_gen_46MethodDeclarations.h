﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>
struct Enumerator_t4045;
// System.Object
struct Object_t;
// UnityEngine.RectTransform
struct RectTransform_t1227;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t1322;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m24042(__this, ___l, method) (( void (*) (Enumerator_t4045 *, List_1_t1322 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24043(__this, method) (( Object_t * (*) (Enumerator_t4045 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m24044(__this, method) (( void (*) (Enumerator_t4045 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::VerifyState()
#define Enumerator_VerifyState_m24045(__this, method) (( void (*) (Enumerator_t4045 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m24046(__this, method) (( bool (*) (Enumerator_t4045 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m24047(__this, method) (( RectTransform_t1227 * (*) (Enumerator_t4045 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
