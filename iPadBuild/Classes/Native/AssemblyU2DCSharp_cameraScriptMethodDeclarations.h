﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// cameraScript
struct cameraScript_t774;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void cameraScript::.ctor()
extern "C" void cameraScript__ctor_m3346 (cameraScript_t774 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraScript::Start()
extern "C" void cameraScript_Start_m3347 (cameraScript_t774 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator cameraScript::shake(System.Single)
extern "C" Object_t * cameraScript_shake_m3348 (cameraScript_t774 * __this, float ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator cameraScript::shakeSmall(System.Single)
extern "C" Object_t * cameraScript_shakeSmall_m3349 (cameraScript_t774 * __this, float ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
