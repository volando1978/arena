﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<waitForTouchDown>c__Iterator9
struct U3CwaitForTouchDownU3Ec__Iterator9_t797;
// System.Object
struct Object_t;

// System.Void gameControl/<waitForTouchDown>c__Iterator9::.ctor()
extern "C" void U3CwaitForTouchDownU3Ec__Iterator9__ctor_m3425 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<waitForTouchDown>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<waitForTouchDown>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<waitForTouchDown>c__Iterator9::MoveNext()
extern "C" bool U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<waitForTouchDown>c__Iterator9::Dispose()
extern "C" void U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<waitForTouchDown>c__Iterator9::Reset()
extern "C" void U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430 (U3CwaitForTouchDownU3Ec__Iterator9_t797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
