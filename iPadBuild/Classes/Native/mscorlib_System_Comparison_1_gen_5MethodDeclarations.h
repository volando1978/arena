﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparison_1_t3451;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m15767_gshared (Comparison_1_t3451 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m15767(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3451 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m15767_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m15768_gshared (Comparison_1_t3451 * __this, KeyValuePair_2_t3407  ___x, KeyValuePair_2_t3407  ___y, MethodInfo* method);
#define Comparison_1_Invoke_m15768(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3451 *, KeyValuePair_2_t3407 , KeyValuePair_2_t3407 , MethodInfo*))Comparison_1_Invoke_m15768_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m15769_gshared (Comparison_1_t3451 * __this, KeyValuePair_2_t3407  ___x, KeyValuePair_2_t3407  ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m15769(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3451 *, KeyValuePair_2_t3407 , KeyValuePair_2_t3407 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m15769_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m15770_gshared (Comparison_1_t3451 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m15770(__this, ___result, method) (( int32_t (*) (Comparison_1_t3451 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m15770_gshared)(__this, ___result, method)
