﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// enemyLife
struct enemyLife_t788;

// System.Void enemyLife::.ctor()
extern "C" void enemyLife__ctor_m3397 (enemyLife_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyLife::decreaseHP()
extern "C" void enemyLife_decreaseHP_m3398 (enemyLife_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyLife::decreaseDoubleHP()
extern "C" void enemyLife_decreaseDoubleHP_m3399 (enemyLife_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 enemyLife::getHP()
extern "C" int32_t enemyLife_getHP_m3400 (enemyLife_t788 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
