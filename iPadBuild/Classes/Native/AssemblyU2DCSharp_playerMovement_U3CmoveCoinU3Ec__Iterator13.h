﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// playerMovement/<moveCoin>c__Iterator13
struct  U3CmoveCoinU3Ec__Iterator13_t829  : public Object_t
{
	// UnityEngine.Vector2 playerMovement/<moveCoin>c__Iterator13::<target>__0
	Vector2_t739  ___U3CtargetU3E__0_0;
	// UnityEngine.GameObject playerMovement/<moveCoin>c__Iterator13::coin
	GameObject_t144 * ___coin_1;
	// UnityEngine.Vector2 playerMovement/<moveCoin>c__Iterator13::<mov>__1
	Vector2_t739  ___U3CmovU3E__1_2;
	// System.Int32 playerMovement/<moveCoin>c__Iterator13::$PC
	int32_t ___U24PC_3;
	// System.Object playerMovement/<moveCoin>c__Iterator13::$current
	Object_t * ___U24current_4;
	// UnityEngine.GameObject playerMovement/<moveCoin>c__Iterator13::<$>coin
	GameObject_t144 * ___U3CU24U3Ecoin_5;
};
