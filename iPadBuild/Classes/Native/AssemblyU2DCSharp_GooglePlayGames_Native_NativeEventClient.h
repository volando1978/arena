﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.EventManager
struct EventManager_t548;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeEventClient
struct  NativeEventClient_t549  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.EventManager GooglePlayGames.Native.NativeEventClient::mEventManager
	EventManager_t548 * ___mEventManager_0;
};
