﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t1304;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t720;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Byte>
#include "System_Core_System_Func_2_gen_38MethodDeclarations.h"
#define Func_2__ctor_m6127(__this, ___object, ___method, method) (( void (*) (Func_2_t1304 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m18520_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m23950(__this, ___arg1, method) (( bool (*) (Func_2_t1304 *, Toggle_t720 *, MethodInfo*))Func_2_Invoke_m18522_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m23951(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t1304 *, Toggle_t720 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m18524_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m23952(__this, ___result, method) (( bool (*) (Func_2_t1304 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m18526_gshared)(__this, ___result, method)
