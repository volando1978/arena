﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33
struct U3CAcceptFromInboxU3Ec__AnonStorey33_t602;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34
struct U3CAcceptFromInboxU3Ec__AnonStorey34_t603;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35
struct  U3CAcceptFromInboxU3Ec__AnonStorey35_t604  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::helper
	RealTimeEventListenerHelper_t594 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>f__ref$51
	U3CAcceptFromInboxU3Ec__AnonStorey33_t602 * ___U3CU3Ef__refU2451_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey34 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptFromInbox>c__AnonStorey33/<AcceptFromInbox>c__AnonStorey35::<>f__ref$52
	U3CAcceptFromInboxU3Ec__AnonStorey34_t603 * ___U3CU3Ef__refU2452_2;
};
