﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t347;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.Participant::.ctor(System.String,System.String,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Player,System.Boolean)
extern "C" void Participant__ctor_m1385 (Participant_t340 * __this, String_t* ___displayName, String_t* ___participantId, int32_t ___status, Player_t347 * ___player, bool ___connectedToRoom, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::get_DisplayName()
extern "C" String_t* Participant_get_DisplayName_m1386 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::get_ParticipantId()
extern "C" String_t* Participant_get_ParticipantId_m1387 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus GooglePlayGames.BasicApi.Multiplayer.Participant::get_Status()
extern "C" int32_t Participant_get_Status_m1388 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Player GooglePlayGames.BasicApi.Multiplayer.Participant::get_Player()
extern "C" Player_t347 * Participant_get_Player_m1389 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::get_IsConnectedToRoom()
extern "C" bool Participant_get_IsConnectedToRoom_m1390 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::get_IsAutomatch()
extern "C" bool Participant_get_IsAutomatch_m1391 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Participant::ToString()
extern "C" String_t* Participant_ToString_m1392 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Participant::CompareTo(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" int32_t Participant_CompareTo_m1393 (Participant_t340 * __this, Participant_t340 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Participant::Equals(System.Object)
extern "C" bool Participant_Equals_m1394 (Participant_t340 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Participant::GetHashCode()
extern "C" int32_t Participant_GetHashCode_m1395 (Participant_t340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
