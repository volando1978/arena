﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct Enumerator_t3880;
// System.Object
struct Object_t;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3877;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m21679_gshared (Enumerator_t3880 * __this, Stack_1_t3877 * ___t, MethodInfo* method);
#define Enumerator__ctor_m21679(__this, ___t, method) (( void (*) (Enumerator_t3880 *, Stack_1_t3877 *, MethodInfo*))Enumerator__ctor_m21679_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21680_gshared (Enumerator_t3880 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21680(__this, method) (( Object_t * (*) (Enumerator_t3880 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21680_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m21681_gshared (Enumerator_t3880 * __this, MethodInfo* method);
#define Enumerator_Dispose_m21681(__this, method) (( void (*) (Enumerator_t3880 *, MethodInfo*))Enumerator_Dispose_m21681_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21682_gshared (Enumerator_t3880 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m21682(__this, method) (( bool (*) (Enumerator_t3880 *, MethodInfo*))Enumerator_MoveNext_m21682_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21683_gshared (Enumerator_t3880 * __this, MethodInfo* method);
#define Enumerator_get_Current_m21683(__this, method) (( Object_t * (*) (Enumerator_t3880 *, MethodInfo*))Enumerator_get_Current_m21683_gshared)(__this, method)
