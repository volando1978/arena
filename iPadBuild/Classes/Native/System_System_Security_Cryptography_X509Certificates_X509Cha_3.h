﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct  X509ChainElementEnumerator_t2038  : public Object_t
{
	// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator::enumerator
	Object_t * ___enumerator_0;
};
