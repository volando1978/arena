﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t811;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t4405;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t4406;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t4407;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t3852;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1284;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t3856;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3857;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t3860;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m4257_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1__ctor_m4257(__this, method) (( void (*) (List_1_t811 *, MethodInfo*))List_1__ctor_m4257_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m21282_gshared (List_1_t811 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m21282(__this, ___collection, method) (( void (*) (List_1_t811 *, Object_t*, MethodInfo*))List_1__ctor_m21282_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m21283_gshared (List_1_t811 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m21283(__this, ___capacity, method) (( void (*) (List_1_t811 *, int32_t, MethodInfo*))List_1__ctor_m21283_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m21284_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m21284(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m21284_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21285_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21285(__this, method) (( Object_t* (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21285_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m21286_gshared (List_1_t811 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m21286(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t811 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m21286_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m21287_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21287(__this, method) (( Object_t * (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m21287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m21288_gshared (List_1_t811 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m21288(__this, ___item, method) (( int32_t (*) (List_1_t811 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m21288_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m21289_gshared (List_1_t811 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m21289(__this, ___item, method) (( bool (*) (List_1_t811 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m21289_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m21290_gshared (List_1_t811 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m21290(__this, ___item, method) (( int32_t (*) (List_1_t811 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m21290_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m21291_gshared (List_1_t811 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m21291(__this, ___index, ___item, method) (( void (*) (List_1_t811 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m21291_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m21292_gshared (List_1_t811 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m21292(__this, ___item, method) (( void (*) (List_1_t811 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m21292_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21293_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21293(__this, method) (( bool (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21293_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m21294_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21294(__this, method) (( bool (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m21294_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m21295_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m21295(__this, method) (( Object_t * (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m21295_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m21296_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m21296(__this, method) (( bool (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m21296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m21297_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m21297(__this, method) (( bool (*) (List_1_t811 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m21297_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m21298_gshared (List_1_t811 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m21298(__this, ___index, method) (( Object_t * (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m21298_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m21299_gshared (List_1_t811 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m21299(__this, ___index, ___value, method) (( void (*) (List_1_t811 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m21299_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m21300_gshared (List_1_t811 * __this, Vector3_t758  ___item, MethodInfo* method);
#define List_1_Add_m21300(__this, ___item, method) (( void (*) (List_1_t811 *, Vector3_t758 , MethodInfo*))List_1_Add_m21300_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m21301_gshared (List_1_t811 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m21301(__this, ___newCount, method) (( void (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m21301_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m21302_gshared (List_1_t811 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m21302(__this, ___collection, method) (( void (*) (List_1_t811 *, Object_t*, MethodInfo*))List_1_AddCollection_m21302_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m21303_gshared (List_1_t811 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m21303(__this, ___enumerable, method) (( void (*) (List_1_t811 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m21303_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m21304_gshared (List_1_t811 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m21304(__this, ___collection, method) (( void (*) (List_1_t811 *, Object_t*, MethodInfo*))List_1_AddRange_m21304_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3852 * List_1_AsReadOnly_m21305_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m21305(__this, method) (( ReadOnlyCollection_1_t3852 * (*) (List_1_t811 *, MethodInfo*))List_1_AsReadOnly_m21305_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m21306_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_Clear_m21306(__this, method) (( void (*) (List_1_t811 *, MethodInfo*))List_1_Clear_m21306_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m21307_gshared (List_1_t811 * __this, Vector3_t758  ___item, MethodInfo* method);
#define List_1_Contains_m21307(__this, ___item, method) (( bool (*) (List_1_t811 *, Vector3_t758 , MethodInfo*))List_1_Contains_m21307_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m21308_gshared (List_1_t811 * __this, Vector3U5BU5D_t1284* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m21308(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t811 *, Vector3U5BU5D_t1284*, int32_t, MethodInfo*))List_1_CopyTo_m21308_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t758  List_1_Find_m21309_gshared (List_1_t811 * __this, Predicate_1_t3856 * ___match, MethodInfo* method);
#define List_1_Find_m21309(__this, ___match, method) (( Vector3_t758  (*) (List_1_t811 *, Predicate_1_t3856 *, MethodInfo*))List_1_Find_m21309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m21310_gshared (Object_t * __this /* static, unused */, Predicate_1_t3856 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m21310(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3856 *, MethodInfo*))List_1_CheckMatch_m21310_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m21311_gshared (List_1_t811 * __this, Predicate_1_t3856 * ___match, MethodInfo* method);
#define List_1_FindIndex_m21311(__this, ___match, method) (( int32_t (*) (List_1_t811 *, Predicate_1_t3856 *, MethodInfo*))List_1_FindIndex_m21311_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m21312_gshared (List_1_t811 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3856 * ___match, MethodInfo* method);
#define List_1_GetIndex_m21312(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t811 *, int32_t, int32_t, Predicate_1_t3856 *, MethodInfo*))List_1_GetIndex_m21312_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m21313_gshared (List_1_t811 * __this, Action_1_t3857 * ___action, MethodInfo* method);
#define List_1_ForEach_m21313(__this, ___action, method) (( void (*) (List_1_t811 *, Action_1_t3857 *, MethodInfo*))List_1_ForEach_m21313_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t3850  List_1_GetEnumerator_m21314_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m21314(__this, method) (( Enumerator_t3850  (*) (List_1_t811 *, MethodInfo*))List_1_GetEnumerator_m21314_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m21315_gshared (List_1_t811 * __this, Vector3_t758  ___item, MethodInfo* method);
#define List_1_IndexOf_m21315(__this, ___item, method) (( int32_t (*) (List_1_t811 *, Vector3_t758 , MethodInfo*))List_1_IndexOf_m21315_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m21316_gshared (List_1_t811 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m21316(__this, ___start, ___delta, method) (( void (*) (List_1_t811 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m21316_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m21317_gshared (List_1_t811 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m21317(__this, ___index, method) (( void (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_CheckIndex_m21317_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m21318_gshared (List_1_t811 * __this, int32_t ___index, Vector3_t758  ___item, MethodInfo* method);
#define List_1_Insert_m21318(__this, ___index, ___item, method) (( void (*) (List_1_t811 *, int32_t, Vector3_t758 , MethodInfo*))List_1_Insert_m21318_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m21319_gshared (List_1_t811 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m21319(__this, ___collection, method) (( void (*) (List_1_t811 *, Object_t*, MethodInfo*))List_1_CheckCollection_m21319_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m21320_gshared (List_1_t811 * __this, Vector3_t758  ___item, MethodInfo* method);
#define List_1_Remove_m21320(__this, ___item, method) (( bool (*) (List_1_t811 *, Vector3_t758 , MethodInfo*))List_1_Remove_m21320_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m21321_gshared (List_1_t811 * __this, Predicate_1_t3856 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m21321(__this, ___match, method) (( int32_t (*) (List_1_t811 *, Predicate_1_t3856 *, MethodInfo*))List_1_RemoveAll_m21321_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m21322_gshared (List_1_t811 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m21322(__this, ___index, method) (( void (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_RemoveAt_m21322_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m21323_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_Reverse_m21323(__this, method) (( void (*) (List_1_t811 *, MethodInfo*))List_1_Reverse_m21323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m21324_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_Sort_m21324(__this, method) (( void (*) (List_1_t811 *, MethodInfo*))List_1_Sort_m21324_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m21325_gshared (List_1_t811 * __this, Comparison_1_t3860 * ___comparison, MethodInfo* method);
#define List_1_Sort_m21325(__this, ___comparison, method) (( void (*) (List_1_t811 *, Comparison_1_t3860 *, MethodInfo*))List_1_Sort_m21325_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t1284* List_1_ToArray_m21326_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_ToArray_m21326(__this, method) (( Vector3U5BU5D_t1284* (*) (List_1_t811 *, MethodInfo*))List_1_ToArray_m21326_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m21327_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_TrimExcess_m21327(__this, method) (( void (*) (List_1_t811 *, MethodInfo*))List_1_TrimExcess_m21327_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m21328_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_get_Capacity_m21328(__this, method) (( int32_t (*) (List_1_t811 *, MethodInfo*))List_1_get_Capacity_m21328_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m21329_gshared (List_1_t811 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m21329(__this, ___value, method) (( void (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_set_Capacity_m21329_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m21330_gshared (List_1_t811 * __this, MethodInfo* method);
#define List_1_get_Count_m21330(__this, method) (( int32_t (*) (List_1_t811 *, MethodInfo*))List_1_get_Count_m21330_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t758  List_1_get_Item_m21331_gshared (List_1_t811 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m21331(__this, ___index, method) (( Vector3_t758  (*) (List_1_t811 *, int32_t, MethodInfo*))List_1_get_Item_m21331_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m21332_gshared (List_1_t811 * __this, int32_t ___index, Vector3_t758  ___value, MethodInfo* method);
#define List_1_set_Item_m21332(__this, ___index, ___value, method) (( void (*) (List_1_t811 *, int32_t, Vector3_t758 , MethodInfo*))List_1_set_Item_m21332_gshared)(__this, ___index, ___value, method)
