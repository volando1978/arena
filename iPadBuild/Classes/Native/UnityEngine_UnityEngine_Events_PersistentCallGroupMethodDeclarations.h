﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1652;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1654;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1655;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m7443 (PersistentCallGroup_t1652 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m7444 (PersistentCallGroup_t1652 * __this, InvokableCallList_t1654 * ___invokableList, UnityEventBase_t1655 * ___unityEventBase, MethodInfo* method) IL2CPP_METHOD_ATTR;
