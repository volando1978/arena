﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct  KeyValuePair_2_t3801 
{
	// TKey System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::value
	int32_t ___value_1;
};
