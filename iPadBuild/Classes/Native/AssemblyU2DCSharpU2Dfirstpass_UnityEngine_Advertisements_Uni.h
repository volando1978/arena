﻿#pragma once
#include <stdint.h>
// UnityEngine.Advertisements.UnityAds
struct UnityAds_t154;
// System.String
struct String_t;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t155;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Advertisements.UnityAds
struct  UnityAds_t154  : public MonoBehaviour_t26
{
};
struct UnityAds_t154_StaticFields{
	// System.Boolean UnityEngine.Advertisements.UnityAds::isShowing
	bool ___isShowing_2;
	// System.Boolean UnityEngine.Advertisements.UnityAds::isInitialized
	bool ___isInitialized_3;
	// System.Boolean UnityEngine.Advertisements.UnityAds::allowPrecache
	bool ___allowPrecache_4;
	// System.Boolean UnityEngine.Advertisements.UnityAds::initCalled
	bool ___initCalled_5;
	// UnityEngine.Advertisements.UnityAds UnityEngine.Advertisements.UnityAds::sharedInstance
	UnityAds_t154 * ___sharedInstance_6;
	// System.String UnityEngine.Advertisements.UnityAds::_rewardItemNameKey
	String_t* ____rewardItemNameKey_7;
	// System.String UnityEngine.Advertisements.UnityAds::_rewardItemPictureKey
	String_t* ____rewardItemPictureKey_8;
	// System.Boolean UnityEngine.Advertisements.UnityAds::_resultDelivered
	bool ____resultDelivered_9;
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.UnityAds::resultCallback
	Action_1_t155 * ___resultCallback_10;
	// System.String UnityEngine.Advertisements.UnityAds::_versionString
	String_t* ____versionString_11;
};
