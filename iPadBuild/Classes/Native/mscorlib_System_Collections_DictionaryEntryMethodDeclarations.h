﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.DictionaryEntry
struct DictionaryEntry_t2128;
// System.Object
struct Object_t;

// System.Void System.Collections.DictionaryEntry::.ctor(System.Object,System.Object)
extern "C" void DictionaryEntry__ctor_m8739 (DictionaryEntry_t2128 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Key()
extern "C" Object_t * DictionaryEntry_get_Key_m11357 (DictionaryEntry_t2128 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Value()
extern "C" Object_t * DictionaryEntry_get_Value_m11358 (DictionaryEntry_t2128 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
