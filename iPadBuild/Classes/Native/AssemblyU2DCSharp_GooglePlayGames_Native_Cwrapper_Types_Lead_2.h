﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
struct  LeaderboardCollection_t512 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection::value__
	int32_t ___value___1;
};
