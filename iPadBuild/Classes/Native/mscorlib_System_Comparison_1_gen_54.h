﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1647;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
struct  Comparison_1_t4163  : public MulticastDelegate_t22
{
};
