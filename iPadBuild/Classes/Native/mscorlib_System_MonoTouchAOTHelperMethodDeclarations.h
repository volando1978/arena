﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTouchAOTHelper
struct MonoTouchAOTHelper_t2824;

// System.Void System.MonoTouchAOTHelper::.cctor()
extern "C" void MonoTouchAOTHelper__cctor_m14093 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
