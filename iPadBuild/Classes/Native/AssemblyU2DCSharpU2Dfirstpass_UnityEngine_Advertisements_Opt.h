﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Advertisements.ShowOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho.h"
// UnityEngine.Advertisements.Optional.ShowOptionsExtended
struct  ShowOptionsExtended_t152  : public ShowOptions_t153
{
	// System.String UnityEngine.Advertisements.Optional.ShowOptionsExtended::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_3;
};
