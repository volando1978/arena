﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.Advertisement
struct Advertisement_t143;
// System.String
struct String_t;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t153;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"

// System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern "C" void Advertisement__cctor_m689 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.Advertisement/DebugLevel UnityEngine.Advertisements.Advertisement::get_debugLevel()
extern "C" int32_t Advertisement_get_debugLevel_m690 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::set_debugLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C" void Advertisement_set_debugLevel_m691 (Object_t * __this /* static, unused */, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern "C" bool Advertisement_get_isSupported_m692 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern "C" bool Advertisement_get_isInitialized_m693 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern "C" void Advertisement_Initialize_m694 (Object_t * __this /* static, unused */, String_t* ___appId, bool ___testMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern "C" void Advertisement_Show_m695 (Object_t * __this /* static, unused */, String_t* ___zoneId, ShowOptions_t153 * ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_allowPrecache()
extern "C" bool Advertisement_get_allowPrecache_m696 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::set_allowPrecache(System.Boolean)
extern "C" void Advertisement_set_allowPrecache_m697 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern "C" bool Advertisement_IsReady_m698 (Object_t * __this /* static, unused */, String_t* ___zoneId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::isReady(System.String)
extern "C" bool Advertisement_isReady_m699 (Object_t * __this /* static, unused */, String_t* ___zoneId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern "C" bool Advertisement_get_isShowing_m700 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_UnityDeveloperInternalTestMode()
extern "C" bool Advertisement_get_UnityDeveloperInternalTestMode_m701 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::set_UnityDeveloperInternalTestMode(System.Boolean)
extern "C" void Advertisement_set_UnityDeveloperInternalTestMode_m702 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
