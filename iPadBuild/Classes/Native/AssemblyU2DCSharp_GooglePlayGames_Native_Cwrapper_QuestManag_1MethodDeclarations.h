﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
struct AcceptCallback_t446;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AcceptCallback__ctor_m1877 (AcceptCallback_t446 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void AcceptCallback_Invoke_m1878 (AcceptCallback_t446 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AcceptCallback_t446(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * AcceptCallback_BeginInvoke_m1879 (AcceptCallback_t446 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback::EndInvoke(System.IAsyncResult)
extern "C" void AcceptCallback_EndInvoke_m1880 (AcceptCallback_t446 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
