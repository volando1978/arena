﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct Enumerator_t3575;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Soomla.Store.StoreInventory/LocalUpgrade
struct LocalUpgrade_t101;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct Dictionary_2_t102;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m17589(__this, ___dictionary, method) (( void (*) (Enumerator_t3575 *, Dictionary_2_t102 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17590(__this, method) (( Object_t * (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17591(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17592(__this, method) (( Object_t * (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17593(__this, method) (( Object_t * (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::MoveNext()
#define Enumerator_MoveNext_m17594(__this, method) (( bool (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Current()
#define Enumerator_get_Current_m17595(__this, method) (( KeyValuePair_2_t3572  (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17596(__this, method) (( String_t* (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17597(__this, method) (( LocalUpgrade_t101 * (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::VerifyState()
#define Enumerator_VerifyState_m17598(__this, method) (( void (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17599(__this, method) (( void (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Dispose()
#define Enumerator_Dispose_m17600(__this, method) (( void (*) (Enumerator_t3575 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
