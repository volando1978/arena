﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback
struct OnQuestCompletedCallback_t407;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnQuestCompletedCallback__ctor_m1673 (OnQuestCompletedCallback_t407 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OnQuestCompletedCallback_Invoke_m1674 (OnQuestCompletedCallback_t407 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnQuestCompletedCallback_t407(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnQuestCompletedCallback_BeginInvoke_m1675 (OnQuestCompletedCallback_t407 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnQuestCompletedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnQuestCompletedCallback_EndInvoke_m1676 (OnQuestCompletedCallback_t407 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
