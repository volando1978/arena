﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityNameValuePair`1<System.String>
struct UnityNameValuePair_1_t7;
// System.String
struct String_t;

// System.Void UnityEngine.UnityNameValuePair`1<System.String>::.ctor()
// UnityEngine.UnityNameValuePair`1<System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityNameValuePair_0MethodDeclarations.h"
#define UnityNameValuePair_1__ctor_m15856(__this, method) (( void (*) (UnityNameValuePair_1_t7 *, MethodInfo*))UnityNameValuePair_1__ctor_m15857_gshared)(__this, method)
// System.Void UnityEngine.UnityNameValuePair`1<System.String>::.ctor(System.String,V)
#define UnityNameValuePair_1__ctor_m834(__this, ___key, ___value, method) (( void (*) (UnityNameValuePair_1_t7 *, String_t*, String_t*, MethodInfo*))UnityNameValuePair_1__ctor_m15858_gshared)(__this, ___key, ___value, method)
// System.String UnityEngine.UnityNameValuePair`1<System.String>::get_Key()
#define UnityNameValuePair_1_get_Key_m1180(__this, method) (( String_t* (*) (UnityNameValuePair_1_t7 *, MethodInfo*))UnityNameValuePair_1_get_Key_m15859_gshared)(__this, method)
// System.Void UnityEngine.UnityNameValuePair`1<System.String>::set_Key(System.String)
#define UnityNameValuePair_1_set_Key_m1181(__this, ___value, method) (( void (*) (UnityNameValuePair_1_t7 *, String_t*, MethodInfo*))UnityNameValuePair_1_set_Key_m15860_gshared)(__this, ___value, method)
