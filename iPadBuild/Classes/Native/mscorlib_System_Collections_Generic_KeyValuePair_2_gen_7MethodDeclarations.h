﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct KeyValuePair_2_t3572;
// System.String
struct String_t;
// Soomla.Store.StoreInventory/LocalUpgrade
struct LocalUpgrade_t101;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17555(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3572 *, String_t*, LocalUpgrade_t101 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Key()
#define KeyValuePair_2_get_Key_m17556(__this, method) (( String_t* (*) (KeyValuePair_2_t3572 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17557(__this, ___value, method) (( void (*) (KeyValuePair_2_t3572 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Value()
#define KeyValuePair_2_get_Value_m17558(__this, method) (( LocalUpgrade_t101 * (*) (KeyValuePair_2_t3572 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17559(__this, ___value, method) (( void (*) (KeyValuePair_2_t3572 *, LocalUpgrade_t101 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ToString()
#define KeyValuePair_2_ToString_m17560(__this, method) (( String_t* (*) (KeyValuePair_2_t3572 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
