﻿#pragma once
#include <stdint.h>
// System.Action[]
struct ActionU5BU5D_t3700;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Action>
struct  List_1_t393  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Action>::_items
	ActionU5BU5D_t3700* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Action>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Action>::_version
	int32_t ____version_3;
};
struct List_1_t393_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Action>::EmptyArray
	ActionU5BU5D_t3700* ___EmptyArray_4;
};
