﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Status/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Que.h"
// GooglePlayGames.Native.Cwrapper.Status/QuestAcceptStatus
struct  QuestAcceptStatus_t487 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Status/QuestAcceptStatus::value__
	int32_t ___value___1;
};
