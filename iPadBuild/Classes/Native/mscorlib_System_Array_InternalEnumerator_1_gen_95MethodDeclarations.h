﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct InternalEnumerator_1_t4238;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m25993(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4238 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m17272_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25994(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4238 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
#define InternalEnumerator_1_Dispose_m25995(__this, method) (( void (*) (InternalEnumerator_1_t4238 *, MethodInfo*))InternalEnumerator_1_Dispose_m17276_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
#define InternalEnumerator_1_MoveNext_m25996(__this, method) (( bool (*) (InternalEnumerator_1_t4238 *, MethodInfo*))InternalEnumerator_1_MoveNext_m17278_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
#define InternalEnumerator_1_get_Current_m25997(__this, method) (( uint8_t (*) (InternalEnumerator_1_t4238 *, MethodInfo*))InternalEnumerator_1_get_Current_m17280_gshared)(__this, method)
