﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>
struct InternalEnumerator_1_t4220;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25905_gshared (InternalEnumerator_1_t4220 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25905(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4220 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25905_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25906_gshared (InternalEnumerator_1_t4220 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25906(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4220 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25907_gshared (InternalEnumerator_1_t4220 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25907(__this, method) (( void (*) (InternalEnumerator_1_t4220 *, MethodInfo*))InternalEnumerator_1_Dispose_m25907_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25908_gshared (InternalEnumerator_1_t4220 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25908(__this, method) (( bool (*) (InternalEnumerator_1_t4220 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25908_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern "C" Slot_t2443  InternalEnumerator_1_get_Current_m25909_gshared (InternalEnumerator_1_t4220 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25909(__this, method) (( Slot_t2443  (*) (InternalEnumerator_1_t4220 *, MethodInfo*))InternalEnumerator_1_get_Current_m25909_gshared)(__this, method)
