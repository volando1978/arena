﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Collections.Generic.HashSet`1/Link<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>[]
struct LinkU5BU5D_t3755;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3757;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3758;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct  HashSet_1_t583  : public Object_t
{
	// System.Int32[] System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::table
	Int32U5BU5D_t107* ___table_0;
	// System.Collections.Generic.HashSet`1/Link<T>[] System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::links
	LinkU5BU5D_t3755* ___links_1;
	// T[] System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::slots
	ParticipantStatusU5BU5D_t3757* ___slots_2;
	// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::touched
	int32_t ___touched_3;
	// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::empty_slot
	int32_t ___empty_slot_4;
	// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::count
	int32_t ___count_5;
	// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::threshold
	int32_t ___threshold_6;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::comparer
	Object_t* ___comparer_7;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::si
	SerializationInfo_t1673 * ___si_8;
	// System.Int32 System.Collections.Generic.HashSet`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>::generation
	int32_t ___generation_9;
};
