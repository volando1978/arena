﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Matrix4x4>
struct DefaultComparer_t3841;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Matrix4x4>::.ctor()
extern "C" void DefaultComparer__ctor_m21178_gshared (DefaultComparer_t3841 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21178(__this, method) (( void (*) (DefaultComparer_t3841 *, MethodInfo*))DefaultComparer__ctor_m21178_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Matrix4x4>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m21179_gshared (DefaultComparer_t3841 * __this, Matrix4x4_t997  ___x, Matrix4x4_t997  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m21179(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3841 *, Matrix4x4_t997 , Matrix4x4_t997 , MethodInfo*))DefaultComparer_Compare_m21179_gshared)(__this, ___x, ___y, method)
