﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t2187;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.EnvoyInfo
struct  EnvoyInfo_t2631  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.EnvoyInfo::envoySinks
	Object_t * ___envoySinks_0;
};
