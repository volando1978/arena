﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t4231;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void StaticGetter_1__ctor_m25959_gshared (StaticGetter_1_t4231 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define StaticGetter_1__ctor_m25959(__this, ___object, ___method, method) (( void (*) (StaticGetter_1_t4231 *, Object_t *, IntPtr_t, MethodInfo*))StaticGetter_1__ctor_m25959_gshared)(__this, ___object, ___method, method)
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C" Object_t * StaticGetter_1_Invoke_m25960_gshared (StaticGetter_1_t4231 * __this, MethodInfo* method);
#define StaticGetter_1_Invoke_m25960(__this, method) (( Object_t * (*) (StaticGetter_1_t4231 *, MethodInfo*))StaticGetter_1_Invoke_m25960_gshared)(__this, method)
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * StaticGetter_1_BeginInvoke_m25961_gshared (StaticGetter_1_t4231 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define StaticGetter_1_BeginInvoke_m25961(__this, ___callback, ___object, method) (( Object_t * (*) (StaticGetter_1_t4231 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))StaticGetter_1_BeginInvoke_m25961_gshared)(__this, ___callback, ___object, method)
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * StaticGetter_1_EndInvoke_m25962_gshared (StaticGetter_1_t4231 * __this, Object_t * ___result, MethodInfo* method);
#define StaticGetter_1_EndInvoke_m25962(__this, ___result, method) (( Object_t * (*) (StaticGetter_1_t4231 *, Object_t *, MethodInfo*))StaticGetter_1_EndInvoke_m25962_gshared)(__this, ___result, method)
