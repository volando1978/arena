﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// copyPlayerPosition
struct copyPlayerPosition_t780;

// System.Void copyPlayerPosition::.ctor()
extern "C" void copyPlayerPosition__ctor_m3363 (copyPlayerPosition_t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void copyPlayerPosition::Update()
extern "C" void copyPlayerPosition_Update_m3364 (copyPlayerPosition_t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
