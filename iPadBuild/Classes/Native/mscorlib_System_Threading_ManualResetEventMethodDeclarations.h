﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ManualResetEvent
struct ManualResetEvent_t2262;

// System.Void System.Threading.ManualResetEvent::.ctor(System.Boolean)
extern "C" void ManualResetEvent__ctor_m9874 (ManualResetEvent_t2262 * __this, bool ___initialState, MethodInfo* method) IL2CPP_METHOD_ATTR;
