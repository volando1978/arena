﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WeaponsController
struct WeaponsController_t109;
// WeaponsController/WEAPONS
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsController_WEAPONS.h"

// System.Void WeaponsController::.ctor()
extern "C" void WeaponsController__ctor_m507 (WeaponsController_t109 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WeaponsController::.cctor()
extern "C" void WeaponsController__cctor_m508 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WeaponsController::switcher(System.Boolean)
extern "C" bool WeaponsController_switcher_m509 (Object_t * __this /* static, unused */, bool ___cond, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WeaponsController::updateWeapons()
extern "C" void WeaponsController_updateWeapons_m510 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// WeaponsController/WEAPONS WeaponsController::getWeaponType()
extern "C" int32_t WeaponsController_getWeaponType_m511 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WeaponsController::setWeaponOrder()
extern "C" void WeaponsController_setWeaponOrder_m512 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WeaponsController::setMeOnly(System.Int32)
extern "C" bool WeaponsController_setMeOnly_m513 (Object_t * __this /* static, unused */, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WeaponsController::setWeaponsState(System.Int32)
extern "C" void WeaponsController_setWeaponsState_m514 (Object_t * __this /* static, unused */, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WeaponsController::setBombasState(System.Int32)
extern "C" void WeaponsController_setBombasState_m515 (Object_t * __this /* static, unused */, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
