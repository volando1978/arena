﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// logoScene
struct logoScene_t812;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void logoScene::.ctor()
extern "C" void logoScene__ctor_m3561 (logoScene_t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void logoScene::Start()
extern "C" void logoScene_Start_m3562 (logoScene_t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator logoScene::wait()
extern "C" Object_t * logoScene_wait_m3563 (logoScene_t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void logoScene::Update()
extern "C" void logoScene_Update_m3564 (logoScene_t812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
