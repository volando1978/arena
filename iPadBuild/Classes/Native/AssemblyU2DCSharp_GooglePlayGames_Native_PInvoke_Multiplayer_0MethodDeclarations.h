﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.String
struct String_t;
// GooglePlayGames.Native.NativePlayer
struct NativePlayer_t675;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::.ctor(System.IntPtr)
extern "C" void MultiplayerParticipant__ctor_m2711 (MultiplayerParticipant_t672 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::.cctor()
extern "C" void MultiplayerParticipant__cctor_m2712 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Status()
extern "C" int32_t MultiplayerParticipant_Status_m2713 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.MultiplayerParticipant::IsConnectedToRoom()
extern "C" bool MultiplayerParticipant_IsConnectedToRoom_m2714 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.MultiplayerParticipant::DisplayName()
extern "C" String_t* MultiplayerParticipant_DisplayName_m2715 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativePlayer GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Player()
extern "C" NativePlayer_t675 * MultiplayerParticipant_Player_m2716 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Id()
extern "C" String_t* MultiplayerParticipant_Id_m2717 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.MultiplayerParticipant::Valid()
extern "C" bool MultiplayerParticipant_Valid_m2718 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.MultiplayerParticipant::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MultiplayerParticipant_CallDispose_m2719 (MultiplayerParticipant_t672 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::AsParticipant()
extern "C" Participant_t340 * MultiplayerParticipant_AsParticipant_m2720 (MultiplayerParticipant_t672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::FromPointer(System.IntPtr)
extern "C" MultiplayerParticipant_t672 * MultiplayerParticipant_FromPointer_m2721 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.MultiplayerParticipant::AutomatchingSentinel()
extern "C" MultiplayerParticipant_t672 * MultiplayerParticipant_AutomatchingSentinel_m2722 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerParticipant::<DisplayName>m__7C(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_U3CDisplayNameU3Em__7C_m2723 (MultiplayerParticipant_t672 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.MultiplayerParticipant::<Id>m__7D(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  MultiplayerParticipant_U3CIdU3Em__7D_m2724 (MultiplayerParticipant_t672 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
