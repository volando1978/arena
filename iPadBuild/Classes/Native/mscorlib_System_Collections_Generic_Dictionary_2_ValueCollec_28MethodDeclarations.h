﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t3798;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t3789;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20598_gshared (Enumerator_t3798 * __this, Dictionary_2_t3789 * ___host, MethodInfo* method);
#define Enumerator__ctor_m20598(__this, ___host, method) (( void (*) (Enumerator_t3798 *, Dictionary_2_t3789 *, MethodInfo*))Enumerator__ctor_m20598_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20599_gshared (Enumerator_t3798 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20599(__this, method) (( Object_t * (*) (Enumerator_t3798 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20599_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m20600_gshared (Enumerator_t3798 * __this, MethodInfo* method);
#define Enumerator_Dispose_m20600(__this, method) (( void (*) (Enumerator_t3798 *, MethodInfo*))Enumerator_Dispose_m20600_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20601_gshared (Enumerator_t3798 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m20601(__this, method) (( bool (*) (Enumerator_t3798 *, MethodInfo*))Enumerator_MoveNext_m20601_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m20602_gshared (Enumerator_t3798 * __this, MethodInfo* method);
#define Enumerator_get_Current_m20602(__this, method) (( int32_t (*) (Enumerator_t3798 *, MethodInfo*))Enumerator_get_Current_m20602_gshared)(__this, method)
