﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t169;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct  MethodReturnDictionary_t2617  : public MethodDictionary_t2609
{
};
struct MethodReturnDictionary_t2617_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodReturnDictionary::InternalReturnKeys
	StringU5BU5D_t169* ___InternalReturnKeys_6;
	// System.String[] System.Runtime.Remoting.Messaging.MethodReturnDictionary::InternalExceptionKeys
	StringU5BU5D_t169* ___InternalExceptionKeys_7;
};
