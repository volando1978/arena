﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t1427;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
extern "C" void AssemblyTrademarkAttribute__ctor_m6183 (AssemblyTrademarkAttribute_t1427 * __this, String_t* ___trademark, MethodInfo* method) IL2CPP_METHOD_ATTR;
