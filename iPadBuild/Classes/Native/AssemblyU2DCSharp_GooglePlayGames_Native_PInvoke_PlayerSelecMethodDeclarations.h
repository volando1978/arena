﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62
struct U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::.ctor()
extern "C" void U3CPlayerIdAtIndexU3Ec__AnonStorey62__ctor_m2878 (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse/<PlayerIdAtIndex>c__AnonStorey62::<>m__98(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  U3CPlayerIdAtIndexU3Ec__AnonStorey62_U3CU3Em__98_m2879 (U3CPlayerIdAtIndexU3Ec__AnonStorey62_t687 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
