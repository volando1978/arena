﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>
struct U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723;

// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>::.ctor()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19756_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19756(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1__ctor_m19756_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>::<>m__F()
extern "C" void U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19757_gshared (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723 * __this, MethodInfo* method);
#define U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19757(__this, method) (( void (*) (U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723 *, MethodInfo*))U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_U3CU3Em__F_m19757_gshared)(__this, method)
