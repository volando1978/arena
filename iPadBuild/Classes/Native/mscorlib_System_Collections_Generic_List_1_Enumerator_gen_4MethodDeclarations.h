﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>
struct Enumerator_t216;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrency>
struct List_1_t114;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m17756(__this, ___l, method) (( void (*) (Enumerator_t216 *, List_1_t114 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17757(__this, method) (( Object_t * (*) (Enumerator_t216 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::Dispose()
#define Enumerator_Dispose_m17758(__this, method) (( void (*) (Enumerator_t216 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::VerifyState()
#define Enumerator_VerifyState_m17759(__this, method) (( void (*) (Enumerator_t216 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::MoveNext()
#define Enumerator_MoveNext_m986(__this, method) (( bool (*) (Enumerator_t216 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrency>::get_Current()
#define Enumerator_get_Current_m985(__this, method) (( VirtualCurrency_t77 * (*) (Enumerator_t216 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
