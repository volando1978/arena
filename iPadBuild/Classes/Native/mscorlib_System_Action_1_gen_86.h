﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.UI.Graphic>
struct  Action_1_t3986  : public MulticastDelegate_t22
{
};
