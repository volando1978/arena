﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t710;
// System.Object
#include "mscorlib_System_Object.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64
struct  U3CPlayerIdAtIndexU3Ec__AnonStorey64_t711  : public Object_t
{
	// System.UIntPtr GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64::index
	UIntPtr_t  ___index_0;
	// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig/<PlayerIdAtIndex>c__AnonStorey64::<>f__this
	TurnBasedMatchConfig_t710 * ___U3CU3Ef__this_1;
};
