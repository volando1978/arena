﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t2435;
// System.Globalization.CompareInfo
struct CompareInfo_t2325;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct  Comparer_t2435  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t2325 * ___m_compareInfo_2;
};
struct Comparer_t2435_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t2435 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t2435 * ___DefaultInvariant_1;
};
