﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F
struct U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F::.ctor()
extern "C" void U3CRealTimeMessageReceivedU3Ec__AnonStorey3F__ctor_m2360 (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RealTimeMessageReceived>c__AnonStorey3F::<>m__35()
extern "C" void U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_U3CU3Em__35_m2361 (U3CRealTimeMessageReceivedU3Ec__AnonStorey3F_t571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
