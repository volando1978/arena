﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeAchievement
struct NativeAchievement_t673;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<System.UIntPtr,GooglePlayGames.Native.NativeAchievement>
struct  Func_2_t934  : public MulticastDelegate_t22
{
};
