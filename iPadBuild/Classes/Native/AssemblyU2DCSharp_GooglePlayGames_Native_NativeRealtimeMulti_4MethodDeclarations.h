﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E
struct U3CPeersDisconnectedU3Ec__AnonStorey3E_t570;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::.ctor()
extern "C" void U3CPeersDisconnectedU3Ec__AnonStorey3E__ctor_m2358 (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::<>m__34()
extern "C" void U3CPeersDisconnectedU3Ec__AnonStorey3E_U3CU3Em__34_m2359 (U3CPeersDisconnectedU3Ec__AnonStorey3E_t570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
