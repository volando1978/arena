﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Byte,System.Object>
struct Action_2_t932;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Byte,System.Object>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771  : public Object_t
{
	// System.Action`2<T1,T2> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Byte,System.Object>::toInvokeOnGameThread
	Action_2_t932 * ___toInvokeOnGameThread_0;
};
