﻿#pragma once
#include <stdint.h>
// UnityEngine.Advertisements.UnityAdsPlatform
struct UnityAdsPlatform_t156;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.UnityAdsExternal
struct  UnityAdsExternal_t157  : public Object_t
{
};
struct UnityAdsExternal_t157_StaticFields{
	// UnityEngine.Advertisements.UnityAdsPlatform UnityEngine.Advertisements.UnityAdsExternal::impl
	UnityAdsPlatform_t156 * ___impl_0;
	// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::initialized
	bool ___initialized_1;
};
