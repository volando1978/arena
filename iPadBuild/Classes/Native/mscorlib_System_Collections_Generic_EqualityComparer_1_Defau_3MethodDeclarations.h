﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>
struct DefaultComparer_t3670;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::.ctor()
extern "C" void DefaultComparer__ctor_m19048_gshared (DefaultComparer_t3670 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m19048(__this, method) (( void (*) (DefaultComparer_t3670 *, MethodInfo*))DefaultComparer__ctor_m19048_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m19049_gshared (DefaultComparer_t3670 * __this, uint32_t ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m19049(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3670 *, uint32_t, MethodInfo*))DefaultComparer_GetHashCode_m19049_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.UInt32>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m19050_gshared (DefaultComparer_t3670 * __this, uint32_t ___x, uint32_t ___y, MethodInfo* method);
#define DefaultComparer_Equals_m19050(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3670 *, uint32_t, uint32_t, MethodInfo*))DefaultComparer_Equals_m19050_gshared)(__this, ___x, ___y, method)
