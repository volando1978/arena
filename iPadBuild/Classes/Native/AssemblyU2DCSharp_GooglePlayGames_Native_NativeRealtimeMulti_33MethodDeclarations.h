﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A
struct U3CDeclineInvitationU3Ec__AnonStorey3A_t609;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t698;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::.ctor()
extern "C" void U3CDeclineInvitationU3Ec__AnonStorey3A__ctor_m2475 (U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<DeclineInvitation>c__AnonStorey3A::<>m__2B(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern "C" void U3CDeclineInvitationU3Ec__AnonStorey3A_U3CU3Em__2B_m2476 (U3CDeclineInvitationU3Ec__AnonStorey3A_t609 * __this, FetchInvitationsResponse_t698 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
