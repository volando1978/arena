﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityKeyValuePair__1.h"
// UnityEngine.UnityNameValuePair`1<System.Object>
struct  UnityNameValuePair_1_t3456  : public UnityKeyValuePair_2_t3457
{
	// System.String UnityEngine.UnityNameValuePair`1<System.Object>::name
	String_t* ___name_2;
};
