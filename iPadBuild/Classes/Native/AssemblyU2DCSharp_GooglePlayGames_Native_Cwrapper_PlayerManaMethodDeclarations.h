﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
struct FetchSelfCallback_t439;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchSelfCallback__ctor_m1829 (FetchSelfCallback_t439 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchSelfCallback_Invoke_m1830 (FetchSelfCallback_t439 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchSelfCallback_t439(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchSelfCallback_BeginInvoke_m1831 (FetchSelfCallback_t439 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchSelfCallback_EndInvoke_m1832 (FetchSelfCallback_t439 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
