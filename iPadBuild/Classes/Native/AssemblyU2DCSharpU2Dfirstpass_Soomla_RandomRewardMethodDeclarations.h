﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.RandomReward
struct RandomReward_t57;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.RandomReward::.ctor(System.String,System.String,System.Collections.Generic.List`1<Soomla.Reward>)
extern "C" void RandomReward__ctor_m228 (RandomReward_t57 * __this, String_t* ___id, String_t* ___name, List_1_t56 * ___rewards, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RandomReward::.ctor(JSONObject)
extern "C" void RandomReward__ctor_m229 (RandomReward_t57 * __this, JSONObject_t30 * ___jsonReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.RandomReward::.cctor()
extern "C" void RandomReward__cctor_m230 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.RandomReward::toJSONObject()
extern "C" JSONObject_t30 * RandomReward_toJSONObject_m231 (RandomReward_t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.RandomReward::giveInner()
extern "C" bool RandomReward_giveInner_m232 (RandomReward_t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.RandomReward::takeInner()
extern "C" bool RandomReward_takeInner_m233 (RandomReward_t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
