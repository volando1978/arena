﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t2364;
// System.Char[]
struct CharU5BU5D_t41;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m10745 (Contraction_t2364 * __this, CharU5BU5D_t41* ___source, String_t* ___replacement, ByteU5BU5D_t350* ___sortkey, MethodInfo* method) IL2CPP_METHOD_ATTR;
