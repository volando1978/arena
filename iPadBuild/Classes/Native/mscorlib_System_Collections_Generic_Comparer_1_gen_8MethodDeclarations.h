﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t4193;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.ctor()
extern "C" void Comparer_1__ctor_m25781_gshared (Comparer_1_t4193 * __this, MethodInfo* method);
#define Comparer_1__ctor_m25781(__this, method) (( void (*) (Comparer_1_t4193 *, MethodInfo*))Comparer_1__ctor_m25781_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.cctor()
extern "C" void Comparer_1__cctor_m25782_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1__cctor_m25782(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1__cctor_m25782_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m25783_gshared (Comparer_1_t4193 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m25783(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t4193 *, Object_t *, Object_t *, MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m25783_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::get_Default()
extern "C" Comparer_1_t4193 * Comparer_1_get_Default_m25784_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1_get_Default_m25784(__this /* static, unused */, method) (( Comparer_1_t4193 * (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1_get_Default_m25784_gshared)(__this /* static, unused */, method)
