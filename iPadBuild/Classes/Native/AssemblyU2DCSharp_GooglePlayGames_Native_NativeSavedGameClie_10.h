﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Regex
struct Regex_t200;
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t610;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient
struct  NativeSavedGameClient_t624  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.SnapshotManager GooglePlayGames.Native.NativeSavedGameClient::mSnapshotManager
	SnapshotManager_t610 * ___mSnapshotManager_1;
};
struct NativeSavedGameClient_t624_StaticFields{
	// System.Text.RegularExpressions.Regex GooglePlayGames.Native.NativeSavedGameClient::ValidFilenameRegex
	Regex_t200 * ___ValidFilenameRegex_0;
};
