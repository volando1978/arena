﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct ValueCollection_t3803;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct IEnumerator_1_t4394;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3786;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_53.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"
#define ValueCollection__ctor_m20641(__this, ___dictionary, method) (( void (*) (ValueCollection_t3803 *, Dictionary_2_t671 *, MethodInfo*))ValueCollection__ctor_m20584_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20642(__this, ___item, method) (( void (*) (ValueCollection_t3803 *, int32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20585_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20643(__this, method) (( void (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20586_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20644(__this, ___item, method) (( bool (*) (ValueCollection_t3803 *, int32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20587_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20645(__this, ___item, method) (( bool (*) (ValueCollection_t3803 *, int32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20588_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20646(__this, method) (( Object_t* (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20589_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m20647(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3803 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m20590_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20648(__this, method) (( Object_t * (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20649(__this, method) (( bool (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20592_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20650(__this, method) (( bool (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20593_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m20651(__this, method) (( Object_t * (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m20594_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m20652(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3803 *, ParticipantStatusU5BU5D_t3786*, int32_t, MethodInfo*))ValueCollection_CopyTo_m20595_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
#define ValueCollection_GetEnumerator_m20653(__this, method) (( Enumerator_t4395  (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_GetEnumerator_m20596_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
#define ValueCollection_get_Count_m20654(__this, method) (( int32_t (*) (ValueCollection_t3803 *, MethodInfo*))ValueCollection_get_Count_m20597_gshared)(__this, method)
