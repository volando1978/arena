﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>
struct Action_2_t550;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26
struct  U3CFetchU3Ec__AnonStorey26_t551  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest> GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26::callback
	Action_2_t550 * ___callback_0;
};
