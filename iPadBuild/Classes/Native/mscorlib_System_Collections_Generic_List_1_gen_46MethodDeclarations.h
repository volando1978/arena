﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1604;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t4516;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4517;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t1402;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t4110;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1670;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t4114;
// System.Action`1<UnityEngine.UILineInfo>
struct Action_1_t4115;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t4118;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m24890_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1__ctor_m24890(__this, method) (( void (*) (List_1_t1604 *, MethodInfo*))List_1__ctor_m24890_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m24891_gshared (List_1_t1604 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m24891(__this, ___collection, method) (( void (*) (List_1_t1604 *, Object_t*, MethodInfo*))List_1__ctor_m24891_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m7515_gshared (List_1_t1604 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m7515(__this, ___capacity, method) (( void (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1__ctor_m7515_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m24892_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m24892(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m24892_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24893_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24893(__this, method) (( Object_t* (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24893_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24894_gshared (List_1_t1604 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m24894(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1604 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m24894_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m24895_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24895(__this, method) (( Object_t * (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m24895_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m24896_gshared (List_1_t1604 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m24896(__this, ___item, method) (( int32_t (*) (List_1_t1604 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m24896_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m24897_gshared (List_1_t1604 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m24897(__this, ___item, method) (( bool (*) (List_1_t1604 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m24897_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m24898_gshared (List_1_t1604 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m24898(__this, ___item, method) (( int32_t (*) (List_1_t1604 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m24898_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m24899_gshared (List_1_t1604 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m24899(__this, ___index, ___item, method) (( void (*) (List_1_t1604 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m24899_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m24900_gshared (List_1_t1604 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m24900(__this, ___item, method) (( void (*) (List_1_t1604 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m24900_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24901_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24901(__this, method) (( bool (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24901_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m24902_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24902(__this, method) (( bool (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m24902_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m24903_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m24903(__this, method) (( Object_t * (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m24903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m24904_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m24904(__this, method) (( bool (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m24904_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m24905_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m24905(__this, method) (( bool (*) (List_1_t1604 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m24905_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m24906_gshared (List_1_t1604 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m24906(__this, ___index, method) (( Object_t * (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m24906_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m24907_gshared (List_1_t1604 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m24907(__this, ___index, ___value, method) (( void (*) (List_1_t1604 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m24907_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m24908_gshared (List_1_t1604 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define List_1_Add_m24908(__this, ___item, method) (( void (*) (List_1_t1604 *, UILineInfo_t1398 , MethodInfo*))List_1_Add_m24908_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m24909_gshared (List_1_t1604 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m24909(__this, ___newCount, method) (( void (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m24909_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m24910_gshared (List_1_t1604 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m24910(__this, ___collection, method) (( void (*) (List_1_t1604 *, Object_t*, MethodInfo*))List_1_AddCollection_m24910_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m24911_gshared (List_1_t1604 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m24911(__this, ___enumerable, method) (( void (*) (List_1_t1604 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m24911_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m24912_gshared (List_1_t1604 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m24912(__this, ___collection, method) (( void (*) (List_1_t1604 *, Object_t*, MethodInfo*))List_1_AddRange_m24912_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t4110 * List_1_AsReadOnly_m24913_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m24913(__this, method) (( ReadOnlyCollection_1_t4110 * (*) (List_1_t1604 *, MethodInfo*))List_1_AsReadOnly_m24913_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m24914_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_Clear_m24914(__this, method) (( void (*) (List_1_t1604 *, MethodInfo*))List_1_Clear_m24914_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m24915_gshared (List_1_t1604 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define List_1_Contains_m24915(__this, ___item, method) (( bool (*) (List_1_t1604 *, UILineInfo_t1398 , MethodInfo*))List_1_Contains_m24915_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m24916_gshared (List_1_t1604 * __this, UILineInfoU5BU5D_t1670* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m24916(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1604 *, UILineInfoU5BU5D_t1670*, int32_t, MethodInfo*))List_1_CopyTo_m24916_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t1398  List_1_Find_m24917_gshared (List_1_t1604 * __this, Predicate_1_t4114 * ___match, MethodInfo* method);
#define List_1_Find_m24917(__this, ___match, method) (( UILineInfo_t1398  (*) (List_1_t1604 *, Predicate_1_t4114 *, MethodInfo*))List_1_Find_m24917_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m24918_gshared (Object_t * __this /* static, unused */, Predicate_1_t4114 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m24918(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4114 *, MethodInfo*))List_1_CheckMatch_m24918_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m24919_gshared (List_1_t1604 * __this, Predicate_1_t4114 * ___match, MethodInfo* method);
#define List_1_FindIndex_m24919(__this, ___match, method) (( int32_t (*) (List_1_t1604 *, Predicate_1_t4114 *, MethodInfo*))List_1_FindIndex_m24919_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m24920_gshared (List_1_t1604 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t4114 * ___match, MethodInfo* method);
#define List_1_GetIndex_m24920(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1604 *, int32_t, int32_t, Predicate_1_t4114 *, MethodInfo*))List_1_GetIndex_m24920_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m24921_gshared (List_1_t1604 * __this, Action_1_t4115 * ___action, MethodInfo* method);
#define List_1_ForEach_m24921(__this, ___action, method) (( void (*) (List_1_t1604 *, Action_1_t4115 *, MethodInfo*))List_1_ForEach_m24921_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t4109  List_1_GetEnumerator_m24922_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m24922(__this, method) (( Enumerator_t4109  (*) (List_1_t1604 *, MethodInfo*))List_1_GetEnumerator_m24922_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m24923_gshared (List_1_t1604 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define List_1_IndexOf_m24923(__this, ___item, method) (( int32_t (*) (List_1_t1604 *, UILineInfo_t1398 , MethodInfo*))List_1_IndexOf_m24923_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m24924_gshared (List_1_t1604 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m24924(__this, ___start, ___delta, method) (( void (*) (List_1_t1604 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m24924_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m24925_gshared (List_1_t1604 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m24925(__this, ___index, method) (( void (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_CheckIndex_m24925_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m24926_gshared (List_1_t1604 * __this, int32_t ___index, UILineInfo_t1398  ___item, MethodInfo* method);
#define List_1_Insert_m24926(__this, ___index, ___item, method) (( void (*) (List_1_t1604 *, int32_t, UILineInfo_t1398 , MethodInfo*))List_1_Insert_m24926_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m24927_gshared (List_1_t1604 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m24927(__this, ___collection, method) (( void (*) (List_1_t1604 *, Object_t*, MethodInfo*))List_1_CheckCollection_m24927_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m24928_gshared (List_1_t1604 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define List_1_Remove_m24928(__this, ___item, method) (( bool (*) (List_1_t1604 *, UILineInfo_t1398 , MethodInfo*))List_1_Remove_m24928_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m24929_gshared (List_1_t1604 * __this, Predicate_1_t4114 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m24929(__this, ___match, method) (( int32_t (*) (List_1_t1604 *, Predicate_1_t4114 *, MethodInfo*))List_1_RemoveAll_m24929_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m24930_gshared (List_1_t1604 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m24930(__this, ___index, method) (( void (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_RemoveAt_m24930_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m24931_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_Reverse_m24931(__this, method) (( void (*) (List_1_t1604 *, MethodInfo*))List_1_Reverse_m24931_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m24932_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_Sort_m24932(__this, method) (( void (*) (List_1_t1604 *, MethodInfo*))List_1_Sort_m24932_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m24933_gshared (List_1_t1604 * __this, Comparison_1_t4118 * ___comparison, MethodInfo* method);
#define List_1_Sort_m24933(__this, ___comparison, method) (( void (*) (List_1_t1604 *, Comparison_1_t4118 *, MethodInfo*))List_1_Sort_m24933_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t1670* List_1_ToArray_m24934_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_ToArray_m24934(__this, method) (( UILineInfoU5BU5D_t1670* (*) (List_1_t1604 *, MethodInfo*))List_1_ToArray_m24934_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m24935_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_TrimExcess_m24935(__this, method) (( void (*) (List_1_t1604 *, MethodInfo*))List_1_TrimExcess_m24935_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m24936_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_get_Capacity_m24936(__this, method) (( int32_t (*) (List_1_t1604 *, MethodInfo*))List_1_get_Capacity_m24936_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m24937_gshared (List_1_t1604 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m24937(__this, ___value, method) (( void (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_set_Capacity_m24937_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m24938_gshared (List_1_t1604 * __this, MethodInfo* method);
#define List_1_get_Count_m24938(__this, method) (( int32_t (*) (List_1_t1604 *, MethodInfo*))List_1_get_Count_m24938_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t1398  List_1_get_Item_m24939_gshared (List_1_t1604 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m24939(__this, ___index, method) (( UILineInfo_t1398  (*) (List_1_t1604 *, int32_t, MethodInfo*))List_1_get_Item_m24939_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m24940_gshared (List_1_t1604 * __this, int32_t ___index, UILineInfo_t1398  ___value, MethodInfo* method);
#define List_1_set_Item_m24940(__this, ___index, ___value, method) (( void (*) (List_1_t1604 *, int32_t, UILineInfo_t1398 , MethodInfo*))List_1_set_Item_m24940_gshared)(__this, ___index, ___value, method)
