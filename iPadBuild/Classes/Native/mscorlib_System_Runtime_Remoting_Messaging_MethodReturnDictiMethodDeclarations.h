﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct MethodReturnDictionary_t2617;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t2887;

// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void MethodReturnDictionary__ctor_m12644 (MethodReturnDictionary_t2617 * __this, Object_t * ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern "C" void MethodReturnDictionary__cctor_m12645 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
