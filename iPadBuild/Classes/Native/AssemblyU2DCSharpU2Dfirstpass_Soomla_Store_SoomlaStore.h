﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.SoomlaStore
struct SoomlaStore_t63;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.SoomlaStore
struct  SoomlaStore_t63  : public Object_t
{
};
struct SoomlaStore_t63_StaticFields{
	// Soomla.Store.SoomlaStore Soomla.Store.SoomlaStore::_instance
	SoomlaStore_t63 * ____instance_1;
	// System.Boolean Soomla.Store.SoomlaStore::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_2;
};
