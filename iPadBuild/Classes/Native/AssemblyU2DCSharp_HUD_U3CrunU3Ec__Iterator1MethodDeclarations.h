﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// HUD/<run>c__Iterator1
struct U3CrunU3Ec__Iterator1_t734;
// System.Object
struct Object_t;

// System.Void HUD/<run>c__Iterator1::.ctor()
extern "C" void U3CrunU3Ec__Iterator1__ctor_m3178 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUD/<run>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CrunU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3179 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUD/<run>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CrunU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3180 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HUD/<run>c__Iterator1::MoveNext()
extern "C" bool U3CrunU3Ec__Iterator1_MoveNext_m3181 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD/<run>c__Iterator1::Dispose()
extern "C" void U3CrunU3Ec__Iterator1_Dispose_m3182 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD/<run>c__Iterator1::Reset()
extern "C" void U3CrunU3Ec__Iterator1_Reset_m3183 (U3CrunU3Ec__Iterator1_t734 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
