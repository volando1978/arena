﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t4249;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m26133_gshared (DefaultComparer_t4249 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26133(__this, method) (( void (*) (DefaultComparer_t4249 *, MethodInfo*))DefaultComparer__ctor_m26133_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26134_gshared (DefaultComparer_t4249 * __this, DateTimeOffset_t2793  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m26134(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4249 *, DateTimeOffset_t2793 , MethodInfo*))DefaultComparer_GetHashCode_m26134_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26135_gshared (DefaultComparer_t4249 * __this, DateTimeOffset_t2793  ___x, DateTimeOffset_t2793  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m26135(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4249 *, DateTimeOffset_t2793 , DateTimeOffset_t2793 , MethodInfo*))DefaultComparer_Equals_m26135_gshared)(__this, ___x, ___y, method)
