﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
struct  PlayGamesClientConfiguration_t366 
{
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mEnableSavedGames
	bool ___mEnableSavedGames_1;
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mEnableDeprecatedCloudSave
	bool ___mEnableDeprecatedCloudSave_2;
	// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mInvitationDelegate
	InvitationReceivedDelegate_t363 * ___mInvitationDelegate_3;
	// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mMatchDelegate
	MatchDelegate_t364 * ___mMatchDelegate_4;
};
struct PlayGamesClientConfiguration_t366_StaticFields{
	// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.BasicApi.PlayGamesClientConfiguration::DefaultConfiguration
	PlayGamesClientConfiguration_t366  ___DefaultConfiguration_0;
};
