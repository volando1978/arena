﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// tutorialController
struct tutorialController_t837;

// System.Void tutorialController::.ctor()
extern "C" void tutorialController__ctor_m3657 (tutorialController_t837 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tutorialController::Start()
extern "C" void tutorialController_Start_m3658 (tutorialController_t837 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void tutorialController::Update()
extern "C" void tutorialController_Update_m3659 (tutorialController_t837 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
