﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
struct Builder_t376;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_0.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedDescription(System.String)
extern "C" Builder_t376  Builder_WithUpdatedDescription_m1483 (Builder_t376 * __this, String_t* ___description, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPngCoverImage(System.Byte[])
extern "C" Builder_t376  Builder_WithUpdatedPngCoverImage_m1484 (Builder_t376 * __this, ByteU5BU5D_t350* ___newPngCoverImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPlayedTime(System.TimeSpan)
extern "C" Builder_t376  Builder_WithUpdatedPlayedTime_m1485 (Builder_t376 * __this, TimeSpan_t190  ___newPlayedTime, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::Build()
extern "C" SavedGameMetadataUpdate_t378  Builder_Build_m1486 (Builder_t376 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
