﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<GooglePlayGames.BasicApi.Multiplayer.Participant,System.String>
struct  Func_2_t352  : public MulticastDelegate_t22
{
};
