﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t536;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37
struct  U3CAcceptInvitationU3Ec__AnonStorey37_t605  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37::invitationId
	String_t* ___invitationId_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey37::<>f__this
	NativeRealtimeMultiplayerClient_t536 * ___U3CU3Ef__this_1;
};
