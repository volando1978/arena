﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t4247;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m26116_gshared (DefaultComparer_t4247 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26116(__this, method) (( void (*) (DefaultComparer_t4247 *, MethodInfo*))DefaultComparer__ctor_m26116_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m26117_gshared (DefaultComparer_t4247 * __this, DateTime_t48  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m26117(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4247 *, DateTime_t48 , MethodInfo*))DefaultComparer_GetHashCode_m26117_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m26118_gshared (DefaultComparer_t4247 * __this, DateTime_t48  ___x, DateTime_t48  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m26118(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4247 *, DateTime_t48 , DateTime_t48 , MethodInfo*))DefaultComparer_Equals_m26118_gshared)(__this, ___x, ___y, method)
