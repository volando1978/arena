﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A
struct U3CClaimMilestoneU3Ec__AnonStorey2A_t559;
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
struct ClaimMilestoneResponse_t690;

// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A::.ctor()
extern "C" void U3CClaimMilestoneU3Ec__AnonStorey2A__ctor_m2307 (U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A::<>m__22(GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse)
extern "C" void U3CClaimMilestoneU3Ec__AnonStorey2A_U3CU3Em__22_m2308 (U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * __this, ClaimMilestoneResponse_t690 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
