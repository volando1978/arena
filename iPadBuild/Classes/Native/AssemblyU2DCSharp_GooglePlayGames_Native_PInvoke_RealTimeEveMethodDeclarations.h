﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStorey63
struct U3CToCallbackPointerU3Ec__AnonStorey63_t694;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStorey63::.ctor()
extern "C" void U3CToCallbackPointerU3Ec__AnonStorey63__ctor_m2934 (U3CToCallbackPointerU3Ec__AnonStorey63_t694 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper/<ToCallbackPointer>c__AnonStorey63::<>m__9A(System.IntPtr)
extern "C" void U3CToCallbackPointerU3Ec__AnonStorey63_U3CU3Em__9A_m2935 (U3CToCallbackPointerU3Ec__AnonStorey63_t694 * __this, IntPtr_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
