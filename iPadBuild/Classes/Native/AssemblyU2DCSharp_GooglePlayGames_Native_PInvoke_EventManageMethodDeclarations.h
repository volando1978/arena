﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
struct FetchResponse_t662;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::.ctor(System.IntPtr)
extern "C" void FetchResponse__ctor_m2641 (FetchResponse_t662 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::ResponseStatus()
extern "C" int32_t FetchResponse_ResponseStatus_m2642 (FetchResponse_t662 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::RequestSucceeded()
extern "C" bool FetchResponse_RequestSucceeded_m2643 (FetchResponse_t662 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::Data()
extern "C" NativeEvent_t674 * FetchResponse_Data_m2644 (FetchResponse_t662 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchResponse_CallDispose_m2645 (FetchResponse_t662 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse GooglePlayGames.Native.PInvoke.EventManager/FetchResponse::FromPointer(System.IntPtr)
extern "C" FetchResponse_t662 * FetchResponse_FromPointer_m2646 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
