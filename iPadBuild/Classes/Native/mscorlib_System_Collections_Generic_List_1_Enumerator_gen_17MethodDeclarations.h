﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<ObjectKvp>
struct Enumerator_t3476;
// System.Object
struct Object_t;
// ObjectKvp
struct ObjectKvp_t6;
// System.Collections.Generic.List`1<ObjectKvp>
struct List_1_t9;

// System.Void System.Collections.Generic.List`1/Enumerator<ObjectKvp>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m16099(__this, ___l, method) (( void (*) (Enumerator_t3476 *, List_1_t9 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ObjectKvp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16100(__this, method) (( Object_t * (*) (Enumerator_t3476 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ObjectKvp>::Dispose()
#define Enumerator_Dispose_m16101(__this, method) (( void (*) (Enumerator_t3476 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ObjectKvp>::VerifyState()
#define Enumerator_VerifyState_m16102(__this, method) (( void (*) (Enumerator_t3476 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ObjectKvp>::MoveNext()
#define Enumerator_MoveNext_m16103(__this, method) (( bool (*) (Enumerator_t3476 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ObjectKvp>::get_Current()
#define Enumerator_get_Current_m16104(__this, method) (( ObjectKvp_t6 * (*) (Enumerator_t3476 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
