﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>
struct Action_2_t546;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25
struct  U3CFetchEventU3Ec__AnonStorey25_t547  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent> GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25::callback
	Action_2_t546 * ___callback_0;
};
