﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t208;
// UnityEngine.Events.InvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct  CachedInvokableCall_1_t1694  : public InvokableCall_1_t4010
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Single>::m_Arg1
	ObjectU5BU5D_t208* ___m_Arg1_1;
};
