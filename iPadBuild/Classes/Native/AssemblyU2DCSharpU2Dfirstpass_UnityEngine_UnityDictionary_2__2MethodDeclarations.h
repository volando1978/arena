﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2/<Remove>c__AnonStorey5<System.Object,System.Object>
struct U3CRemoveU3Ec__AnonStorey5_t3452;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;

// System.Void UnityEngine.UnityDictionary`2/<Remove>c__AnonStorey5<System.Object,System.Object>::.ctor()
extern "C" void U3CRemoveU3Ec__AnonStorey5__ctor_m15771_gshared (U3CRemoveU3Ec__AnonStorey5_t3452 * __this, MethodInfo* method);
#define U3CRemoveU3Ec__AnonStorey5__ctor_m15771(__this, method) (( void (*) (U3CRemoveU3Ec__AnonStorey5_t3452 *, MethodInfo*))U3CRemoveU3Ec__AnonStorey5__ctor_m15771_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2/<Remove>c__AnonStorey5<System.Object,System.Object>::<>m__4(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" bool U3CRemoveU3Ec__AnonStorey5_U3CU3Em__4_m15772_gshared (U3CRemoveU3Ec__AnonStorey5_t3452 * __this, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define U3CRemoveU3Ec__AnonStorey5_U3CU3Em__4_m15772(__this, ___x, method) (( bool (*) (U3CRemoveU3Ec__AnonStorey5_t3452 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))U3CRemoveU3Ec__AnonStorey5_U3CU3Em__4_m15772_gshared)(__this, ___x, method)
