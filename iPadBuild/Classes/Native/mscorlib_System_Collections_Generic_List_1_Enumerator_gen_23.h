﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Action>
struct List_1_t393;
// System.Action
struct Action_t588;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Action>
struct  Enumerator_t3704 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Action>::l
	List_1_t393 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Action>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Action>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Action>::current
	Action_t588 * ___current_3;
};
