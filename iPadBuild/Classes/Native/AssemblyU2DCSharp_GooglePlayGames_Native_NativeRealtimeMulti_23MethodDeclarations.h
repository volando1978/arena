﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey31::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey31__ctor_m2458 (U3CCreateWithInvitationScreenU3Ec__AnonStorey31_t599 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
