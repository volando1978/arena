﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.UnityAdsIosBridge
struct UnityAdsIosBridge_t159;
// System.String
struct String_t;

// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsInit(System.String,System.Boolean,System.Boolean,System.String,System.String)
extern "C" void UnityAdsIosBridge_UnityAdsInit_m804 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, bool ___debugModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsShow(System.String,System.String,System.String)
extern "C" bool UnityAdsIosBridge_UnityAdsShow_m805 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHide()
extern "C" void UnityAdsIosBridge_UnityAdsHide_m806 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsIsSupported()
extern "C" bool UnityAdsIosBridge_UnityAdsIsSupported_m807 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetSDKVersion()
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetSDKVersion_m808 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShow()
extern "C" bool UnityAdsIosBridge_UnityAdsCanShow_m809 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShowZone(System.String)
extern "C" bool UnityAdsIosBridge_UnityAdsCanShowZone_m810 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHasMultipleRewardItems()
extern "C" bool UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemKeys()
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetDefaultRewardItemKey()
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetCurrentRewardItemKey()
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetRewardItemKey(System.String)
extern "C" bool UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDefaultRewardItemAsRewardItem()
extern "C" void UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsWithKey(System.String)
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsKeys()
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDebugMode(System.Boolean)
extern "C" void UnityAdsIosBridge_UnityAdsSetDebugMode_m819 (Object_t * __this /* static, unused */, bool ___debugMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsEnableUnityDeveloperInternalTestMode()
extern "C" void UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
