﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse
struct WaitingRoomUIResponse_t697;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::.ctor(System.IntPtr)
extern "C" void WaitingRoomUIResponse__ctor_m2964 (WaitingRoomUIResponse_t697 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::ResponseStatus()
extern "C" int32_t WaitingRoomUIResponse_ResponseStatus_m2965 (WaitingRoomUIResponse_t697 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::Room()
extern "C" NativeRealTimeRoom_t574 * WaitingRoomUIResponse_Room_m2966 (WaitingRoomUIResponse_t697 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void WaitingRoomUIResponse_CallDispose_m2967 (WaitingRoomUIResponse_t697 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse::FromPointer(System.IntPtr)
extern "C" WaitingRoomUIResponse_t697 * WaitingRoomUIResponse_FromPointer_m2968 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
