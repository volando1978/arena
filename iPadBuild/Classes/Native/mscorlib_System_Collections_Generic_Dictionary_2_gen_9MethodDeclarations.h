﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t343;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt32>
struct KeyCollection_t3672;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.UInt32>
struct ValueCollection_t3673;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>[]
struct KeyValuePair_2U5BU5D_t4355;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>>
struct IEnumerator_1_t4356;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_25MethodDeclarations.h"
#define Dictionary_2__ctor_m3691(__this, method) (( void (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2__ctor_m18861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18862(__this, ___comparer, method) (( void (*) (Dictionary_2_t343 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m18863_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m18864(__this, ___capacity, method) (( void (*) (Dictionary_2_t343 *, int32_t, MethodInfo*))Dictionary_2__ctor_m18865_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m18866(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t343 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m18867_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18868(__this, method) (( Object_t * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m18869_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18870(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t343 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m18871_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18872(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t343 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m18873_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m18874(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t343 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m18875_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m18876(__this, ___key, method) (( void (*) (Dictionary_2_t343 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m18877_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18878(__this, method) (( bool (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18880(__this, method) (( Object_t * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18882(__this, method) (( bool (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18884(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t343 *, KeyValuePair_2_t3671 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18886(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t343 *, KeyValuePair_2_t3671 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18888(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t343 *, KeyValuePair_2U5BU5D_t4355*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18890(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t343 *, KeyValuePair_2_t3671 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18892(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t343 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m18893_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18894(__this, method) (( Object_t * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18896(__this, method) (( Object_t* (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18898(__this, method) (( Object_t * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::get_Count()
#define Dictionary_2_get_Count_m18900(__this, method) (( int32_t (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_get_Count_m18901_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::get_Item(TKey)
#define Dictionary_2_get_Item_m18902(__this, ___key, method) (( uint32_t (*) (Dictionary_2_t343 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m18903_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m18904(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t343 *, String_t*, uint32_t, MethodInfo*))Dictionary_2_set_Item_m18905_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m18906(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t343 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m18907_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m18908(__this, ___size, method) (( void (*) (Dictionary_2_t343 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m18909_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m18910(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t343 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m18911_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m18912(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3671  (*) (Object_t * /* static, unused */, String_t*, uint32_t, MethodInfo*))Dictionary_2_make_pair_m18913_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m18914(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, uint32_t, MethodInfo*))Dictionary_2_pick_key_m18915_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m18916(__this /* static, unused */, ___key, ___value, method) (( uint32_t (*) (Object_t * /* static, unused */, String_t*, uint32_t, MethodInfo*))Dictionary_2_pick_value_m18917_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m18918(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t343 *, KeyValuePair_2U5BU5D_t4355*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m18919_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::Resize()
#define Dictionary_2_Resize_m18920(__this, method) (( void (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_Resize_m18921_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::Add(TKey,TValue)
#define Dictionary_2_Add_m18922(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t343 *, String_t*, uint32_t, MethodInfo*))Dictionary_2_Add_m18923_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::Clear()
#define Dictionary_2_Clear_m18924(__this, method) (( void (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_Clear_m18925_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m18926(__this, ___key, method) (( bool (*) (Dictionary_2_t343 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m18927_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m18928(__this, ___value, method) (( bool (*) (Dictionary_2_t343 *, uint32_t, MethodInfo*))Dictionary_2_ContainsValue_m18929_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m18930(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t343 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m18931_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m18932(__this, ___sender, method) (( void (*) (Dictionary_2_t343 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m18933_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::Remove(TKey)
#define Dictionary_2_Remove_m18934(__this, ___key, method) (( bool (*) (Dictionary_2_t343 *, String_t*, MethodInfo*))Dictionary_2_Remove_m18935_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m18936(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t343 *, String_t*, uint32_t*, MethodInfo*))Dictionary_2_TryGetValue_m18937_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::get_Keys()
#define Dictionary_2_get_Keys_m18938(__this, method) (( KeyCollection_t3672 * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_get_Keys_m18939_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::get_Values()
#define Dictionary_2_get_Values_m18940(__this, method) (( ValueCollection_t3673 * (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_get_Values_m18941_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m18942(__this, ___key, method) (( String_t* (*) (Dictionary_2_t343 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m18943_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m18944(__this, ___value, method) (( uint32_t (*) (Dictionary_2_t343 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m18945_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m18946(__this, ___pair, method) (( bool (*) (Dictionary_2_t343 *, KeyValuePair_2_t3671 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m18947_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m18948(__this, method) (( Enumerator_t3674  (*) (Dictionary_2_t343 *, MethodInfo*))Dictionary_2_GetEnumerator_m18949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.UInt32>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m18950(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, uint32_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m18951_gshared)(__this /* static, unused */, ___key, ___value, method)
