﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>
struct Enumerator_t228;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.Collections.Generic.List`1<Soomla.Store.VirtualCategory>
struct List_1_t117;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m18512(__this, ___l, method) (( void (*) (Enumerator_t228 *, List_1_t117 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18513(__this, method) (( Object_t * (*) (Enumerator_t228 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::Dispose()
#define Enumerator_Dispose_m18514(__this, method) (( void (*) (Enumerator_t228 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::VerifyState()
#define Enumerator_VerifyState_m18515(__this, method) (( void (*) (Enumerator_t228 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::MoveNext()
#define Enumerator_MoveNext_m1024(__this, method) (( bool (*) (Enumerator_t228 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCategory>::get_Current()
#define Enumerator_get_Current_m1023(__this, method) (( VirtualCategory_t126 * (*) (Enumerator_t228 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
