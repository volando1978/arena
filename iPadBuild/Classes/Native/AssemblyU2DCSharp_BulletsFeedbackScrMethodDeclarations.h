﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BulletsFeedbackScr
struct BulletsFeedbackScr_t725;

// System.Void BulletsFeedbackScr::.ctor()
extern "C" void BulletsFeedbackScr__ctor_m3146 (BulletsFeedbackScr_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletsFeedbackScr::Update()
extern "C" void BulletsFeedbackScr_Update_m3147 (BulletsFeedbackScr_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletsFeedbackScr::OnGUI()
extern "C" void BulletsFeedbackScr_OnGUI_m3148 (BulletsFeedbackScr_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
