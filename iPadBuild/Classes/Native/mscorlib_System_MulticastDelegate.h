﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t22;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t22  : public Delegate_t211
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t22 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t22 * ___kpm_next_10;
};
