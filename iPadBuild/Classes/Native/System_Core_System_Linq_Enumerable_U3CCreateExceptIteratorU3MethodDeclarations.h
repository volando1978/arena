﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>
struct U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::.ctor()
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m20288_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m20288(__this, method) (( void (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1__ctor_m20288_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20289_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20289(__this, method) (( Object_t * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20289_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m20290_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m20290(__this, method) (( Object_t * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m20290_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m20291_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m20291(__this, method) (( Object_t * (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m20291_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20292_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20292(__this, method) (( Object_t* (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20292_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::MoveNext()
extern "C" bool U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m20293_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m20293(__this, method) (( bool (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_MoveNext_m20293_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::Dispose()
extern "C" void U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m20294_gshared (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 * __this, MethodInfo* method);
#define U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m20294(__this, method) (( void (*) (U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766 *, MethodInfo*))U3CCreateExceptIteratorU3Ec__Iterator4_1_Dispose_m20294_gshared)(__this, method)
