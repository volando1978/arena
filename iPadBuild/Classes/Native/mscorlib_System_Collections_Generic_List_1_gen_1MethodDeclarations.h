﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t181;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t221;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t4257;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t3416;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Predicate`1<System.Object>
struct Predicate_1_t3421;
// System.Action`1<System.Object>
struct Action_1_t3422;
// System.Comparison`1<System.Object>
struct Comparison_1_t3429;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m1042_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1__ctor_m1042(__this, method) (( void (*) (List_1_t181 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15321_gshared (List_1_t181 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m15321(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15323_gshared (List_1_t181 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m15323(__this, ___capacity, method) (( void (*) (List_1_t181 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m15325_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m15325(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327(__this, method) (( Object_t* (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15329_gshared (List_1_t181 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15329(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t181 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15331(__this, method) (( Object_t * (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15333_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15333(__this, ___item, method) (( int32_t (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15335_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15335(__this, ___item, method) (( bool (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15337_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15337(__this, ___item, method) (( int32_t (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15339_gshared (List_1_t181 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15339(__this, ___index, ___item, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15341_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15341(__this, ___item, method) (( void (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343(__this, method) (( bool (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15345(__this, method) (( bool (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15347(__this, method) (( Object_t * (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15349(__this, method) (( bool (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15351(__this, method) (( bool (*) (List_1_t181 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15353_gshared (List_1_t181 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15353(__this, ___index, method) (( Object_t * (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15355_gshared (List_1_t181 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15355(__this, ___index, ___value, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m15357_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Add_m15357(__this, ___item, method) (( void (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15359_gshared (List_1_t181 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m15359(__this, ___newCount, method) (( void (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15361_gshared (List_1_t181 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m15361(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15363_gshared (List_1_t181 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m15363(__this, ___enumerable, method) (( void (*) (List_1_t181 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15365_gshared (List_1_t181 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m15365(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3416 * List_1_AsReadOnly_m15367_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m15367(__this, method) (( ReadOnlyCollection_1_t3416 * (*) (List_1_t181 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m15369_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_Clear_m15369(__this, method) (( void (*) (List_1_t181 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m15371_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Contains_m15371(__this, ___item, method) (( bool (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15373_gshared (List_1_t181 * __this, ObjectU5BU5D_t208* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m15373(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t181 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m15375_gshared (List_1_t181 * __this, Predicate_1_t3421 * ___match, MethodInfo* method);
#define List_1_Find_m15375(__this, ___match, method) (( Object_t * (*) (List_1_t181 *, Predicate_1_t3421 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15377_gshared (Object_t * __this /* static, unused */, Predicate_1_t3421 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m15377(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3421 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m15379_gshared (List_1_t181 * __this, Predicate_1_t3421 * ___match, MethodInfo* method);
#define List_1_FindIndex_m15379(__this, ___match, method) (( int32_t (*) (List_1_t181 *, Predicate_1_t3421 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15381_gshared (List_1_t181 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3421 * ___match, MethodInfo* method);
#define List_1_GetIndex_m15381(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t181 *, int32_t, int32_t, Predicate_1_t3421 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m15383_gshared (List_1_t181 * __this, Action_1_t3422 * ___action, MethodInfo* method);
#define List_1_ForEach_m15383(__this, ___action, method) (( void (*) (List_1_t181 *, Action_1_t3422 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3414  List_1_GetEnumerator_m15385_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m15385(__this, method) (( Enumerator_t3414  (*) (List_1_t181 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15387_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_IndexOf_m15387(__this, ___item, method) (( int32_t (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15389_gshared (List_1_t181 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m15389(__this, ___start, ___delta, method) (( void (*) (List_1_t181 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15391_gshared (List_1_t181 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m15391(__this, ___index, method) (( void (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15393_gshared (List_1_t181 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_Insert_m15393(__this, ___index, ___item, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15395_gshared (List_1_t181 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m15395(__this, ___collection, method) (( void (*) (List_1_t181 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m15397_gshared (List_1_t181 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Remove_m15397(__this, ___item, method) (( bool (*) (List_1_t181 *, Object_t *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15399_gshared (List_1_t181 * __this, Predicate_1_t3421 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m15399(__this, ___match, method) (( int32_t (*) (List_1_t181 *, Predicate_1_t3421 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15401_gshared (List_1_t181 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m15401(__this, ___index, method) (( void (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m15403_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_Reverse_m15403(__this, method) (( void (*) (List_1_t181 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m15405_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_Sort_m15405(__this, method) (( void (*) (List_1_t181 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15407_gshared (List_1_t181 * __this, Comparison_1_t3429 * ___comparison, MethodInfo* method);
#define List_1_Sort_m15407(__this, ___comparison, method) (( void (*) (List_1_t181 *, Comparison_1_t3429 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t208* List_1_ToArray_m15409_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_ToArray_m15409(__this, method) (( ObjectU5BU5D_t208* (*) (List_1_t181 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m15411_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_TrimExcess_m15411(__this, method) (( void (*) (List_1_t181 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15413_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_get_Capacity_m15413(__this, method) (( int32_t (*) (List_1_t181 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15415_gshared (List_1_t181 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m15415(__this, ___value, method) (( void (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m15417_gshared (List_1_t181 * __this, MethodInfo* method);
#define List_1_get_Count_m15417(__this, method) (( int32_t (*) (List_1_t181 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m15419_gshared (List_1_t181 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m15419(__this, ___index, method) (( Object_t * (*) (List_1_t181 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15421_gshared (List_1_t181 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_set_Item_m15421(__this, ___index, ___value, method) (( void (*) (List_1_t181 *, int32_t, Object_t *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
