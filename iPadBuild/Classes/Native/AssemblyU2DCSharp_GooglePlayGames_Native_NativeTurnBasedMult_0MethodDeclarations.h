﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E
struct U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::.ctor()
extern "C" void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E__ctor_m2544 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<BridgeMatchToUserCallback>c__AnonStorey4E::<>m__58(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_U3CU3Em__58_m2545 (U3CBridgeMatchToUserCallbackU3Ec__AnonStorey4E_t634 * __this, TurnBasedMatchResponse_t707 * ___callbackResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
