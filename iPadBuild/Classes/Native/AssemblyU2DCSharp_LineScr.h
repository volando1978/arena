﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// LineScr
struct  LineScr_t748  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject LineScr::target
	GameObject_t144 * ___target_2;
	// System.Int32 LineScr::lengthOfLineRenderer
	int32_t ___lengthOfLineRenderer_3;
	// UnityEngine.Color LineScr::blue
	Color_t747  ___blue_4;
	// UnityEngine.Color LineScr::white
	Color_t747  ___white_5;
	// UnityEngine.GameObject LineScr::a
	GameObject_t144 * ___a_6;
	// UnityEngine.GameObject LineScr::b
	GameObject_t144 * ___b_7;
};
