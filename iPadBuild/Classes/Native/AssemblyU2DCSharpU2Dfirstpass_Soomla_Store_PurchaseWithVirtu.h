﻿#pragma once
#include <stdint.h>
// JSONObject
struct JSONObject_t30;
// System.String
struct String_t;
// Soomla.Store.PurchaseWithVirtualItem
struct PurchaseWithVirtualItem_t139;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8
struct  U3CBuyU3Ec__AnonStorey8_t140  : public Object_t
{
	// JSONObject Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::eventJSON
	JSONObject_t30 * ___eventJSON_0;
	// System.String Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::payload
	String_t* ___payload_1;
	// Soomla.Store.PurchaseWithVirtualItem Soomla.Store.PurchaseWithVirtualItem/<Buy>c__AnonStorey8::<>f__this
	PurchaseWithVirtualItem_t139 * ___U3CU3Ef__this_2;
};
