﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Object
struct Object_t;
// gameControl
struct gameControl_t794;
// System.Object
#include "mscorlib_System_Object.h"
// gameControl/<wait>c__Iterator8
struct  U3CwaitU3Ec__Iterator8_t796  : public Object_t
{
	// System.Int32 gameControl/<wait>c__Iterator8::<t>__0
	int32_t ___U3CtU3E__0_0;
	// UnityEngine.GameObject gameControl/<wait>c__Iterator8::<explosionRed>__1
	GameObject_t144 * ___U3CexplosionRedU3E__1_1;
	// System.Int32 gameControl/<wait>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object gameControl/<wait>c__Iterator8::$current
	Object_t * ___U24current_3;
	// gameControl gameControl/<wait>c__Iterator8::<>f__this
	gameControl_t794 * ___U3CU3Ef__this_4;
};
