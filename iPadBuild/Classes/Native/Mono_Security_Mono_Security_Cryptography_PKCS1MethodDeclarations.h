﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.PKCS1
struct PKCS1_t2212;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Security.Cryptography.RSA
struct RSA_t2130;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2210;

// System.Void Mono.Security.Cryptography.PKCS1::.cctor()
extern "C" void PKCS1__cctor_m9118 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Compare(System.Byte[],System.Byte[])
extern "C" bool PKCS1_Compare_m9119 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___array1, ByteU5BU5D_t350* ___array2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::I2OSP(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t350* PKCS1_I2OSP_m9120 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___x, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::OS2IP(System.Byte[])
extern "C" ByteU5BU5D_t350* PKCS1_OS2IP_m9121 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSASP1(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t350* PKCS1_RSASP1_m9122 (Object_t * __this /* static, unused */, RSA_t2130 * ___rsa, ByteU5BU5D_t350* ___m, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSAVP1(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t350* PKCS1_RSAVP1_m9123 (Object_t * __this /* static, unused */, RSA_t2130 * ___rsa, ByteU5BU5D_t350* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Sign_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern "C" ByteU5BU5D_t350* PKCS1_Sign_v15_m9124 (Object_t * __this /* static, unused */, RSA_t2130 * ___rsa, HashAlgorithm_t2210 * ___hash, ByteU5BU5D_t350* ___hashValue, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Verify_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Byte[])
extern "C" bool PKCS1_Verify_v15_m9125 (Object_t * __this /* static, unused */, RSA_t2130 * ___rsa, HashAlgorithm_t2210 * ___hash, ByteU5BU5D_t350* ___hashValue, ByteU5BU5D_t350* ___signature, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Verify_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Byte[],System.Boolean)
extern "C" bool PKCS1_Verify_v15_m9126 (Object_t * __this /* static, unused */, RSA_t2130 * ___rsa, HashAlgorithm_t2210 * ___hash, ByteU5BU5D_t350* ___hashValue, ByteU5BU5D_t350* ___signature, bool ___tryNonStandardEncoding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Encode_v15(System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t350* PKCS1_Encode_v15_m9127 (Object_t * __this /* static, unused */, HashAlgorithm_t2210 * ___hash, ByteU5BU5D_t350* ___hashValue, int32_t ___emLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
