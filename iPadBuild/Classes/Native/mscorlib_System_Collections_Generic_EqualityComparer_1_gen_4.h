﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct EqualityComparer_1_t3812;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct  EqualityComparer_1_t3812  : public Object_t
{
};
struct EqualityComparer_1_t3812_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.IntPtr>::_default
	EqualityComparer_1_t3812 * ____default_0;
};
