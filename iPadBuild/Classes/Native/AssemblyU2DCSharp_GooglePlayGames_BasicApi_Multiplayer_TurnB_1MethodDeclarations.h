﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::.ctor(System.String,System.Byte[],System.Boolean,System.String,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>,System.UInt32,System.String,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus,System.UInt32,System.UInt32)
extern "C" void TurnBasedMatch__ctor_m1403 (TurnBasedMatch_t353 * __this, String_t* ___matchId, ByteU5BU5D_t350* ___data, bool ___canRematch, String_t* ___selfParticipantId, List_1_t351 * ___participants, uint32_t ___availableAutomatchSlots, String_t* ___pendingParticipantId, int32_t ___turnStatus, int32_t ___matchStatus, uint32_t ___variant, uint32_t ___version, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_MatchId()
extern "C" String_t* TurnBasedMatch_get_MatchId_m1404 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Data()
extern "C" ByteU5BU5D_t350* TurnBasedMatch_get_Data_m1405 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_CanRematch()
extern "C" bool TurnBasedMatch_get_CanRematch_m1406 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_SelfParticipantId()
extern "C" String_t* TurnBasedMatch_get_SelfParticipantId_m1407 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Self()
extern "C" Participant_t340 * TurnBasedMatch_get_Self_m1408 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::GetParticipant(System.String)
extern "C" Participant_t340 * TurnBasedMatch_GetParticipant_m1409 (TurnBasedMatch_t353 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Participants()
extern "C" List_1_t351 * TurnBasedMatch_get_Participants_m1410 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_PendingParticipantId()
extern "C" String_t* TurnBasedMatch_get_PendingParticipantId_m1411 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_PendingParticipant()
extern "C" Participant_t340 * TurnBasedMatch_get_PendingParticipant_m1412 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_TurnStatus()
extern "C" int32_t TurnBasedMatch_get_TurnStatus_m1413 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Status()
extern "C" int32_t TurnBasedMatch_get_Status_m1414 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Variant()
extern "C" uint32_t TurnBasedMatch_get_Variant_m1415 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_Version()
extern "C" uint32_t TurnBasedMatch_get_Version_m1416 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::get_AvailableAutomatchSlots()
extern "C" uint32_t TurnBasedMatch_get_AvailableAutomatchSlots_m1417 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::ToString()
extern "C" String_t* TurnBasedMatch_ToString_m1418 (TurnBasedMatch_t353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch::<ToString>m__0(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* TurnBasedMatch_U3CToStringU3Em__0_m1419 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
