﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct InternalEnumerator_1_t3408;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15298_gshared (InternalEnumerator_1_t3408 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m15298(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3408 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15298_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15299_gshared (InternalEnumerator_1_t3408 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15299(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3408 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15299_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15300_gshared (InternalEnumerator_1_t3408 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15300(__this, method) (( void (*) (InternalEnumerator_1_t3408 *, MethodInfo*))InternalEnumerator_1_Dispose_m15300_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15301_gshared (InternalEnumerator_1_t3408 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15301(__this, method) (( bool (*) (InternalEnumerator_1_t3408 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15301_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3407  InternalEnumerator_1_get_Current_m15302_gshared (InternalEnumerator_1_t3408 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15302(__this, method) (( KeyValuePair_2_t3407  (*) (InternalEnumerator_1_t3408 *, MethodInfo*))InternalEnumerator_1_get_Current_m15302_gshared)(__this, method)
