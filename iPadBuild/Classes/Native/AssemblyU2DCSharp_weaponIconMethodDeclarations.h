﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// weaponIcon
struct weaponIcon_t839;

// System.Void weaponIcon::.ctor()
extern "C" void weaponIcon__ctor_m3663 (weaponIcon_t839 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponIcon::Start()
extern "C" void weaponIcon_Start_m3664 (weaponIcon_t839 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void weaponIcon::Update()
extern "C" void weaponIcon_Update_m3665 (weaponIcon_t839 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
