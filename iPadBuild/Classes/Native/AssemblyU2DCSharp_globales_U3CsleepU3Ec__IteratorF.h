﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// globales/<sleep>c__IteratorF
struct  U3CsleepU3Ec__IteratorF_t805  : public Object_t
{
	// System.Single globales/<sleep>c__IteratorF::t
	float ___t_0;
	// System.Int32 globales/<sleep>c__IteratorF::$PC
	int32_t ___U24PC_1;
	// System.Object globales/<sleep>c__IteratorF::$current
	Object_t * ___U24current_2;
	// System.Single globales/<sleep>c__IteratorF::<$>t
	float ___U3CU24U3Et_3;
};
