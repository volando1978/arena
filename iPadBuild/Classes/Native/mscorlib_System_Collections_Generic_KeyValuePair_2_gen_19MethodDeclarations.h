﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct KeyValuePair_2_t3801;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"

// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20621(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3801 *, int32_t, int32_t, MethodInfo*))KeyValuePair_2__ctor_m20539_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Key()
#define KeyValuePair_2_get_Key_m20622(__this, method) (( int32_t (*) (KeyValuePair_2_t3801 *, MethodInfo*))KeyValuePair_2_get_Key_m20540_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20623(__this, ___value, method) (( void (*) (KeyValuePair_2_t3801 *, int32_t, MethodInfo*))KeyValuePair_2_set_Key_m20541_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Value()
#define KeyValuePair_2_get_Value_m20624(__this, method) (( int32_t (*) (KeyValuePair_2_t3801 *, MethodInfo*))KeyValuePair_2_get_Value_m20542_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20625(__this, ___value, method) (( void (*) (KeyValuePair_2_t3801 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m20543_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::ToString()
#define KeyValuePair_2_ToString_m20626(__this, method) (( String_t* (*) (KeyValuePair_2_t3801 *, MethodInfo*))KeyValuePair_2_ToString_m20544_gshared)(__this, method)
