﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
struct OnP2PConnectedCallback_t453;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnP2PConnectedCallback__ctor_m1931 (OnP2PConnectedCallback_t453 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void OnP2PConnectedCallback_Invoke_m1932 (OnP2PConnectedCallback_t453 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnP2PConnectedCallback_t453(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnP2PConnectedCallback_BeginInvoke_m1933 (OnP2PConnectedCallback_t453 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnP2PConnectedCallback_EndInvoke_m1934 (OnP2PConnectedCallback_t453 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
