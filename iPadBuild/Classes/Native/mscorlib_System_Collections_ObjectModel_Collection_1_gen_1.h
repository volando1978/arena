﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<System.IntPtr>
struct IList_1_t3809;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<System.IntPtr>
struct  Collection_1_t3811  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.IntPtr>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<System.IntPtr>::syncRoot
	Object_t * ___syncRoot_1;
};
