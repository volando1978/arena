﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct Dictionary_2_t102;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t103;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.StoreInventory
struct  StoreInventory_t104  : public Object_t
{
};
struct StoreInventory_t104_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Soomla.Store.StoreInventory::localItemBalances
	Dictionary_2_t75 * ___localItemBalances_1;
	// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade> Soomla.Store.StoreInventory::localUpgrades
	Dictionary_2_t102 * ___localUpgrades_2;
	// System.Collections.Generic.HashSet`1<System.String> Soomla.Store.StoreInventory::localEquippedGoods
	HashSet_1_t103 * ___localEquippedGoods_3;
};
