﻿#pragma once
#include <stdint.h>
// SoundManager/soundLibrary
struct soundLibrary_t754;
// UnityEngine.AudioClip
struct AudioClip_t753;
// UnityEngine.AudioSource
struct AudioSource_t755;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SoundManager
struct  SoundManager_t756  : public MonoBehaviour_t26
{
	// SoundManager/soundLibrary SoundManager::library
	soundLibrary_t754 * ___library_2;
};
struct SoundManager_t756_StaticFields{
	// UnityEngine.AudioClip SoundManager::nBulletSnd
	AudioClip_t753 * ___nBulletSnd_3;
	// UnityEngine.AudioClip SoundManager::wBulletSnd
	AudioClip_t753 * ___wBulletSnd_4;
	// UnityEngine.AudioClip SoundManager::laserBulletSnd
	AudioClip_t753 * ___laserBulletSnd_5;
	// UnityEngine.AudioClip SoundManager::triBulletSnd
	AudioClip_t753 * ___triBulletSnd_6;
	// UnityEngine.AudioClip SoundManager::circularBulletSnd
	AudioClip_t753 * ___circularBulletSnd_7;
	// UnityEngine.AudioClip SoundManager::moireBulletSnd
	AudioClip_t753 * ___moireBulletSnd_8;
	// UnityEngine.AudioClip SoundManager::dustXPLSnd
	AudioClip_t753 * ___dustXPLSnd_9;
	// UnityEngine.AudioClip SoundManager::bombaXPLSnd
	AudioClip_t753 * ___bombaXPLSnd_10;
	// UnityEngine.AudioClip SoundManager::smallBombaXPLSnd
	AudioClip_t753 * ___smallBombaXPLSnd_11;
	// UnityEngine.AudioClip SoundManager::bombaAmarillaXPLSnd
	AudioClip_t753 * ___bombaAmarillaXPLSnd_12;
	// UnityEngine.AudioClip SoundManager::risaSpeechSnd
	AudioClip_t753 * ___risaSpeechSnd_13;
	// UnityEngine.AudioClip SoundManager::bolasSpeechSnd
	AudioClip_t753 * ___bolasSpeechSnd_14;
	// UnityEngine.AudioClip SoundManager::tresViasSpeechSnd
	AudioClip_t753 * ___tresViasSpeechSnd_15;
	// UnityEngine.AudioClip SoundManager::laserSpeechSnd
	AudioClip_t753 * ___laserSpeechSnd_16;
	// UnityEngine.AudioClip SoundManager::circularSpeechSnd
	AudioClip_t753 * ___circularSpeechSnd_17;
	// UnityEngine.AudioClip SoundManager::repeteaSpeechSnd
	AudioClip_t753 * ___repeteaSpeechSnd_18;
	// UnityEngine.AudioClip SoundManager::rayosSpeechSnd
	AudioClip_t753 * ___rayosSpeechSnd_19;
	// UnityEngine.AudioClip SoundManager::fantasmaSpeechSnd
	AudioClip_t753 * ___fantasmaSpeechSnd_20;
	// UnityEngine.AudioClip SoundManager::bombaSpeechSnd
	AudioClip_t753 * ___bombaSpeechSnd_21;
	// UnityEngine.AudioClip SoundManager::boomSpeechSnd
	AudioClip_t753 * ___boomSpeechSnd_22;
	// UnityEngine.AudioClip SoundManager::caralludoSpeechSnd
	AudioClip_t753 * ___caralludoSpeechSnd_23;
	// UnityEngine.AudioClip SoundManager::crispadoSpeechSnd
	AudioClip_t753 * ___crispadoSpeechSnd_24;
	// UnityEngine.AudioClip SoundManager::arenaWomanSnd
	AudioClip_t753 * ___arenaWomanSnd_25;
	// UnityEngine.AudioClip SoundManager::raysWomanSnd
	AudioClip_t753 * ___raysWomanSnd_26;
	// UnityEngine.AudioClip SoundManager::explosionWomanSnd
	AudioClip_t753 * ___explosionWomanSnd_27;
	// UnityEngine.AudioClip SoundManager::awesomeWomanSnd
	AudioClip_t753 * ___awesomeWomanSnd_28;
	// UnityEngine.AudioClip SoundManager::interferenciaGameSnd
	AudioClip_t753 * ___interferenciaGameSnd_29;
	// UnityEngine.AudioClip SoundManager::pillaPaqueteGameSnd
	AudioClip_t753 * ___pillaPaqueteGameSnd_30;
	// UnityEngine.AudioClip SoundManager::buttonShortSnd
	AudioClip_t753 * ___buttonShortSnd_31;
	// UnityEngine.AudioClip SoundManager::buttonLongSnd
	AudioClip_t753 * ___buttonLongSnd_32;
	// UnityEngine.AudioClip SoundManager::dingSnd
	AudioClip_t753 * ___dingSnd_33;
	// UnityEngine.AudioSource SoundManager::soundPlayer
	AudioSource_t755 * ___soundPlayer_34;
	// System.Single SoundManager::pan
	float ___pan_35;
};
