﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>
struct Func_2_t577;
// System.Object
struct Object_t;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_40MethodDeclarations.h"
#define Func_2__ctor_m3807(__this, ___object, ___method, method) (( void (*) (Func_2_t577 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m19196_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>::Invoke(T)
#define Func_2_Invoke_m20106(__this, ___arg1, method) (( String_t* (*) (Func_2_t577 *, MultiplayerParticipant_t672 *, MethodInfo*))Func_2_Invoke_m19198_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20107(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t577 *, MultiplayerParticipant_t672 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m19200_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20108(__this, ___result, method) (( String_t* (*) (Func_2_t577 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m19202_gshared)(__this, ___result, method)
