﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t1368;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t1229;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Canvas>
struct IEnumerable_1_t4458;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Canvas>
struct IEnumerator_1_t4459;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.Canvas>
struct ICollection_1_t4460;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Canvas>
struct ReadOnlyCollection_1_t3978;
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t3976;
// System.Predicate`1<UnityEngine.Canvas>
struct Predicate_1_t3979;
// System.Action`1<UnityEngine.Canvas>
struct Action_1_t3980;
// System.Comparison`1<UnityEngine.Canvas>
struct Comparison_1_t3982;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_40.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m23056(__this, method) (( void (*) (List_1_t1368 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23057(__this, ___collection, method) (( void (*) (List_1_t1368 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::.ctor(System.Int32)
#define List_1__ctor_m23058(__this, ___capacity, method) (( void (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::.cctor()
#define List_1__cctor_m23059(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23060(__this, method) (( Object_t* (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23061(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1368 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23062(__this, method) (( Object_t * (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23063(__this, ___item, method) (( int32_t (*) (List_1_t1368 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23064(__this, ___item, method) (( bool (*) (List_1_t1368 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23065(__this, ___item, method) (( int32_t (*) (List_1_t1368 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23066(__this, ___index, ___item, method) (( void (*) (List_1_t1368 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23067(__this, ___item, method) (( void (*) (List_1_t1368 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23068(__this, method) (( bool (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23069(__this, method) (( bool (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23070(__this, method) (( Object_t * (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23071(__this, method) (( bool (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23072(__this, method) (( bool (*) (List_1_t1368 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23073(__this, ___index, method) (( Object_t * (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23074(__this, ___index, ___value, method) (( void (*) (List_1_t1368 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Add(T)
#define List_1_Add_m23075(__this, ___item, method) (( void (*) (List_1_t1368 *, Canvas_t1229 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23076(__this, ___newCount, method) (( void (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23077(__this, ___collection, method) (( void (*) (List_1_t1368 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23078(__this, ___enumerable, method) (( void (*) (List_1_t1368 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23079(__this, ___collection, method) (( void (*) (List_1_t1368 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Canvas>::AsReadOnly()
#define List_1_AsReadOnly_m23080(__this, method) (( ReadOnlyCollection_1_t3978 * (*) (List_1_t1368 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Clear()
#define List_1_Clear_m23081(__this, method) (( void (*) (List_1_t1368 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::Contains(T)
#define List_1_Contains_m23082(__this, ___item, method) (( bool (*) (List_1_t1368 *, Canvas_t1229 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23083(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1368 *, CanvasU5BU5D_t3976*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Canvas>::Find(System.Predicate`1<T>)
#define List_1_Find_m23084(__this, ___match, method) (( Canvas_t1229 * (*) (List_1_t1368 *, Predicate_1_t3979 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23085(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3979 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m23086(__this, ___match, method) (( int32_t (*) (List_1_t1368 *, Predicate_1_t3979 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23087(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1368 *, int32_t, int32_t, Predicate_1_t3979 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m23088(__this, ___action, method) (( void (*) (List_1_t1368 *, Action_1_t3980 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Canvas>::GetEnumerator()
#define List_1_GetEnumerator_m23089(__this, method) (( Enumerator_t3981  (*) (List_1_t1368 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::IndexOf(T)
#define List_1_IndexOf_m23090(__this, ___item, method) (( int32_t (*) (List_1_t1368 *, Canvas_t1229 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23091(__this, ___start, ___delta, method) (( void (*) (List_1_t1368 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23092(__this, ___index, method) (( void (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Insert(System.Int32,T)
#define List_1_Insert_m23093(__this, ___index, ___item, method) (( void (*) (List_1_t1368 *, int32_t, Canvas_t1229 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23094(__this, ___collection, method) (( void (*) (List_1_t1368 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Canvas>::Remove(T)
#define List_1_Remove_m23095(__this, ___item, method) (( bool (*) (List_1_t1368 *, Canvas_t1229 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23096(__this, ___match, method) (( int32_t (*) (List_1_t1368 *, Predicate_1_t3979 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23097(__this, ___index, method) (( void (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Reverse()
#define List_1_Reverse_m23098(__this, method) (( void (*) (List_1_t1368 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Sort()
#define List_1_Sort_m23099(__this, method) (( void (*) (List_1_t1368 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23100(__this, ___comparison, method) (( void (*) (List_1_t1368 *, Comparison_1_t3982 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Canvas>::ToArray()
#define List_1_ToArray_m23101(__this, method) (( CanvasU5BU5D_t3976* (*) (List_1_t1368 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::TrimExcess()
#define List_1_TrimExcess_m23102(__this, method) (( void (*) (List_1_t1368 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::get_Capacity()
#define List_1_get_Capacity_m23103(__this, method) (( int32_t (*) (List_1_t1368 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23104(__this, ___value, method) (( void (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Canvas>::get_Count()
#define List_1_get_Count_m23105(__this, method) (( int32_t (*) (List_1_t1368 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Canvas>::get_Item(System.Int32)
#define List_1_get_Item_m23106(__this, ___index, method) (( Canvas_t1229 * (*) (List_1_t1368 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Canvas>::set_Item(System.Int32,T)
#define List_1_set_Item_m23107(__this, ___index, ___value, method) (( void (*) (List_1_t1368 *, int32_t, Canvas_t1229 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
