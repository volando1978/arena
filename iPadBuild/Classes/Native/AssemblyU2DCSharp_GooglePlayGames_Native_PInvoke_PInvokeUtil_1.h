﻿#pragma once
#include <stdint.h>
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>
struct  OutMethod_1_t942  : public MulticastDelegate_t22
{
};
