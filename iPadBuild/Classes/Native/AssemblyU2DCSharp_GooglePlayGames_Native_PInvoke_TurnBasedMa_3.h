﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager
struct  TurnBasedManager_t651  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.TurnBasedManager::mGameServices
	GameServices_t534 * ___mGameServices_0;
};
