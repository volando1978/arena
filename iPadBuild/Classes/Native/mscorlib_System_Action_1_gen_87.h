﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t1215;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.UI.Selectable>
struct  Action_1_t4017  : public MulticastDelegate_t22
{
};
