﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Graphic>
struct  Comparison_1_t1236  : public MulticastDelegate_t22
{
};
