﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t1428;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.IEventSystemHandler>
struct  Comparison_1_t3875  : public MulticastDelegate_t22
{
};
