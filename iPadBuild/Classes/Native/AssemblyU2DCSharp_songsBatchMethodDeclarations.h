﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// songsBatch
struct songsBatch_t834;

// System.Void songsBatch::.ctor()
extern "C" void songsBatch__ctor_m3647 (songsBatch_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void songsBatch::Awake()
extern "C" void songsBatch_Awake_m3648 (songsBatch_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void songsBatch::selectSong()
extern "C" void songsBatch_selectSong_m3649 (songsBatch_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void songsBatch::switcherButton()
extern "C" void songsBatch_switcherButton_m3650 (songsBatch_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
