﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2945);
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 14;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t1702 * tmp;
		tmp = (InternalsVisibleToAttribute_t1702 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t1702_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m7551(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1485_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m6349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1485_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m6350(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m6354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m6355(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m6356(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_LayerToName_m6359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m6360(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_t1205_LayerMask_GetMask_m6361_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void RuntimePlatform_t1491_CustomAttributesCacheGenerator_NaCl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("NaCl export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void RuntimePlatform_t1491_CustomAttributesCacheGenerator_FlashPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("FlashPlayer export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerX86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX86 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerX64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX64 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerARM(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerARM instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Coroutine_t1268_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m6364(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m6366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_t15_ScriptableObject_Internal_CreateScriptableObject_m6366_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m6367(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m6369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m6374(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m6375(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m6376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m6377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m6378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m6379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m6380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m6381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m6382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m6383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m6384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m6385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m6386(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m6387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m6388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m6389(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6390(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m6436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m6437(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m6438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m6439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_enabled_m4112(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_castShadows_m4124(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_receiveShadows_m4125(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_material_m4140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_material_m4121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_bounds_m4019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m4113(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_sortingLayerID_m4114(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m4115(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_sortingOrder_m4110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var;
void Renderer_t976_CustomAttributesCacheGenerator_Renderer_t976____castShadows_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2949);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Property castShadows has been deprecated. Use shadowCastingMode instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t1703 * tmp;
		tmp = (EditorBrowsableAttribute_t1703 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7553(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetWidth_m6460(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetColors_m6461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetVertexCount_m6462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetPosition_m6463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Graphics_t1504_CustomAttributesCacheGenerator_Graphics_DrawTexture_m6464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Screen_t1506_CustomAttributesCacheGenerator_Screen_get_width_m4246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Screen_t1506_CustomAttributesCacheGenerator_Screen_get_height_m4088(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Screen_t1506_CustomAttributesCacheGenerator_Screen_get_dpi_m6133(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Screen_t1506_CustomAttributesCacheGenerator_Screen_get_orientation_m4195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Texture_t736_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m6472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Texture_t736_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m6473(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m6477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_t384_Texture2D_Internal_Create_m6477_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m5873(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m5948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RenderTexture_t1507_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m6478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RenderTexture_t1507_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m6479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUILayer_t1510_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m6483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Gradient_t1513_CustomAttributesCacheGenerator_Gradient_Init_m6487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Gradient_t1513_CustomAttributesCacheGenerator_Gradient_Cleanup_m6488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m6496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_get_color_m6498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_set_color_m6499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_set_changed_m6501(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m6504(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_DrawTexture_m4089(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_DrawTexture_m4154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("ScaleMode.StretchToFill"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_get_blendMaterial_m6506(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_get_blitMaterial_m6507(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m6510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoToggle_m6513(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1522_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m6527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1522_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_GetControlID_m6560(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m6564(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m6565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m6567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m6569(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m6573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_set_mouseUsed_m6574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_Push_m6576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_Pop_m6577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_GetMatrix_m6578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_SetMatrix_m6580(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISettings_t1531_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISettings_t1531_CustomAttributesCacheGenerator_m_TripleClickSelectsLine(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISettings_t1531_CustomAttributesCacheGenerator_m_CursorColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISettings_t1531_CustomAttributesCacheGenerator_m_CursorFlashSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISettings_t1531_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_box(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_button(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_toggle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_label(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_textField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_textArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_window(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_ScrollView(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_CustomStyles(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUISkin_t985_CustomAttributesCacheGenerator_m_Settings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUIContent_t986_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUIContent_t986_CustomAttributesCacheGenerator_m_Image(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GUIContent_t986_CustomAttributesCacheGenerator_m_Tooltip(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_Init_m6647(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m6648(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m6649(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m6650(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_Init_m6655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_Cleanup_m6656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_left_m6148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_left_m6657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_right_m6658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_right_m6659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_top_m6149(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_top_m6660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_bottom_m6661(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_bottom_m6662(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m6142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_vertical_m6143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m6664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Init_m6668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m6669(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_name_m6670(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_name_m6671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m6673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m6676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m6677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m6678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m6679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m6680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m6681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m6682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m6683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fontSize_m4085(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_fontSize_m4045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Draw_m6685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_t724_GUIStyle_Draw_m6686_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_Draw2_m6688(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m6689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m6694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m6696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m6699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m6039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m6040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("TouchScreenKeyboardType.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m5975(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m5976(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m6038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m5974(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m6037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m5987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m5986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_Init_m6703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_Cleanup_m6705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_rawType_m6001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_type_m6706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m6708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_modifiers_m5997(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_character_m5999(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_commandName_m6709(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_get_keyCode_m5998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m6711(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Event_t1269_CustomAttributesCacheGenerator_Event_PopEvent_m6002(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void EventModifiers_t1538_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Vector2_t739_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Vector3_t758_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Color_t747_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern TypeInfo* IL2CPPStructAlignmentAttribute_t1607_il2cpp_TypeInfo_var;
void Color32_t992_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IL2CPPStructAlignmentAttribute_t1607_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2951);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		IL2CPPStructAlignmentAttribute_t1607 * tmp;
		tmp = (IL2CPPStructAlignmentAttribute_t1607 *)il2cpp_codegen_object_new (IL2CPPStructAlignmentAttribute_t1607_il2cpp_TypeInfo_var);
		IL2CPPStructAlignmentAttribute__ctor_m7302(tmp, NULL);
		tmp->___Align_0 = 4;
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_UnclampedSlerp_m6740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6741(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6745(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6781(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6782(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6808(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Vector4_t1362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Mathf_t980_CustomAttributesCacheGenerator_Mathf_t980_Mathf_SmoothDamp_m6060_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Mathf_t980_CustomAttributesCacheGenerator_Mathf_t980_Mathf_SmoothDamp_m6060_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void DrivenTransformProperties_t1540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6841(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6842(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6843(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6845(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6847(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6848(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6849(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Resources_t1544_CustomAttributesCacheGenerator_Resources_Load_m6856(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void SerializePrivateVariables_t1545_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Shader_t988_CustomAttributesCacheGenerator_Shader_Find_m4119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Shader_t988_CustomAttributesCacheGenerator_Shader_PropertyToID_m6858(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6861(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_GetTexture_m6863(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_SetFloat_m6865(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_HasProperty_m6866(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6867(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_t989_Material_Internal_CreateWithShader_m6867_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6868(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void Material_t989_CustomAttributesCacheGenerator_Material_t989_Material_Internal_CreateWithMaterial_m6868_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6871(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6874(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6877(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_bounds_m4187(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_rect_m5928(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m5924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_texture_m5921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_textureRect_m5945(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_border_m5922(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_GetSprite_INTERNAL_m6887(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_SetSprite_INTERNAL_m6888(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_INTERNAL_get_color_m6889(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_INTERNAL_set_color_m6890(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m5935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m5934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetPadding_m5927(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6891(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Hash128_t1549_CustomAttributesCacheGenerator_Hash128_ToString_m6893(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW__ctor_m6896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("This overload is deprecated. Use the one with Dictionary argument."), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6901(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_InitWWW_m6902(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_enforceWebSecurityRestrictions_m6903(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_EscapeURL_m6904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_EscapeURL_m6905_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_UnEscapeURL_m6906(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_UnEscapeURL_m6907_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6909(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_bytes_m6914(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_size_m6915(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_error_m6916(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_GetTexture_m6917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_GetAudioClipInternal_m6926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_LoadImageIntoTexture_m6927(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_isDone_m3720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_GetURL_m6928(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("All blocking WWW functions have been deprecated, please use one of the asynchronous functions instead."), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_progress_m6930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_uploadProgress_m6931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_bytesDownloaded_m6932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_LoadUnityWeb_m6934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("LoadUnityWeb is no longer supported. Please use javascript to reload the web player on a different url instead"), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_url_m3717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_assetBundle_m6935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_get_threadPriority_m6936(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_set_threadPriority_m6937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_INTERNAL_CALL_WWW_m6938(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_LoadFromCacheOrDownload_m6939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_LoadFromCacheOrDownload_m6940_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_LoadFromCacheOrDownload_m6941(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_LoadFromCacheOrDownload_m6942_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use WWW.text instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void WWW_t383_CustomAttributesCacheGenerator_WWW_t383____oggVorbis_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("Property WWW.oggVorbis has been deprecated. Use WWW.audioClip instead (UnityUpgradable)."), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void WWWForm_t171_CustomAttributesCacheGenerator_WWWForm_AddField_m915(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWWForm_t171_CustomAttributesCacheGenerator_WWWForm_t171_WWWForm_AddField_m6945_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_URLEncode_m6951_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_QPEncode_m6953_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_URLDecode_m6956_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_SevenBitClean_m6958_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void CacheIndex_t1552_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("this API is not for public use."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void UnityString_t1553_CustomAttributesCacheGenerator_UnityString_t1553_UnityString_Format_m6960_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AsyncOperation_t1486_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6962(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_Quit_m1017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_LoadLevelAsync_m6968(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_get_isPlaying_m3733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_get_isEditor_m1032(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_get_platform_m1033(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_get_unityVersion_m1058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_set_targetFrameRate_m4239(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Application_t1556_CustomAttributesCacheGenerator_Application_get_internetReachability_m1063(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_get_enabled_m5770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_set_enabled_m4228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m5771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m5823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m5822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_orthographicSize_m4024(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_set_orthographic_m4022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_depth_m5738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_aspect_m4023(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_cullingMask_m5833(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_eventMask_m6975(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_get_backgroundColor_m6976(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_set_backgroundColor_m6977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6978(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6979(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_targetTexture_m6981(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6982(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_main_m4021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6988(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6996(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var;
void CameraCallback_t1557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2949);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1703 * tmp;
		tmp = (EditorBrowsableAttribute_t1703 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7553(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_DrawRay_m4135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_Internal_Log_m7000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_Internal_Log_m7000_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_Internal_LogException_m7001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_Internal_LogException_m7001_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Debug_t1558_CustomAttributesCacheGenerator_Debug_get_isDebugBuild_m931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m7027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m7028(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m7029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m7030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m7031(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_SetParamsImpl_m7032(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m7033(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Display_t1561_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m7034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m7035(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m4178(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_t26_MonoBehaviour_StartCoroutine_m4178_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m4065(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m7038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetKeyString_m7040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetKeyDownString_m7041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetAxisRaw_m5814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetButtonDown_m4095(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButton_m5819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m5785(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m5786(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_get_mousePosition_m5787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m5788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_GetTouch_m4097(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_get_touchCount_m4096(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m6041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_get_compositionString_m5977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Input_t987_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m7042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void HideFlags_t1563_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m7044(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_Destroy_m7047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_t187_Object_Destroy_m7047_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_Destroy_m855(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_DestroyImmediate_m7048(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_t187_Object_DestroyImmediate_m7048_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m7049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_get_name_m1064(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_set_name_m1062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m854(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_set_hideFlags_m1037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_ToString_m1201(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_Instantiate_m4028(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Object_t187_CustomAttributesCacheGenerator_Object_FindObjectOfType_m1060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_get_transform_m4030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_get_gameObject_m853(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_GetComponent_m6155(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m7056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_GetComponent_m7558(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2952);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1704 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1704 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7559(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m7057(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Component_t230_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m7058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponent_m7059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m7060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponent_m7564(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2952);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1704 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1704 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7559(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m7061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m7062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_transform_m4029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_layer_m6016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_set_layer_m6017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_SetActive_m4272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m4271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_tag_m4048(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_FindGameObjectWithTag_m4082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_FindGameObjectsWithTag_m4018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_SendMessage_m7063(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_SendMessage_m7063_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_SendMessage_m7063_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m7064(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_AddComponent_m7065(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2947);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1660 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1660 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1660_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m7469(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m7066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1613_il2cpp_TypeInfo_var;
void GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_Internal_CreateGameObject_m7066_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2948);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1613 * tmp;
		tmp = (WritableAttribute_t1613 *)il2cpp_codegen_object_new (WritableAttribute_t1613_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m7310(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m7070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m7071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m7072(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m7073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m7074(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m7075(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m7076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m7077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m7078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m7079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_get_parentInternal_m7080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_set_parentInternal_m7081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_SetParent_m7082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m7083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_Translate_m4201(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Translate_m7084_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_Translate_m4047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Translate_m7085_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_Rotate_m4259(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Rotate_m7086_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_RotateAroundInternal_m7088(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformDirection_m7090(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m7091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m7092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_get_childCount_m4051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m6015(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Transform_t809_CustomAttributesCacheGenerator_Transform_GetChild_m4050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Time_t1565_CustomAttributesCacheGenerator_Time_get_deltaTime_m4036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Time_t1565_CustomAttributesCacheGenerator_Time_get_unscaledTime_m5816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Time_t1565_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m5846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Time_t1565_CustomAttributesCacheGenerator_Time_get_timeScale_m4203(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Time_t1565_CustomAttributesCacheGenerator_Time_set_timeScale_m4091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Random_t1566_CustomAttributesCacheGenerator_Random_Range_m4128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Random_t1566_CustomAttributesCacheGenerator_Random_RandomRangeInt_m7094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_TrySetInt_m7097(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m7098(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_t1568_PlayerPrefs_GetInt_m7098_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m4252(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_HasKey_m4251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_DeleteAll_m4253(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_Save_m4254(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADBannerView_t717_CustomAttributesCacheGenerator_ADBannerView_Native_CreateBanner_m7108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADBannerView_t717_CustomAttributesCacheGenerator_ADBannerView_Native_DestroyBanner_m7109(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_CreateInterstitial_m7118(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_ShowInterstitial_m7119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_ReloadInterstitial_m7120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_InterstitialAdLoaded_m7121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_DestroyInterstitial_m7122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DeviceGeneration_t995_CustomAttributesCacheGenerator_iPad5Gen(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use iPadAir1 instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Device_t1573_CustomAttributesCacheGenerator_Device_get_generation_m4205(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m7143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_Raycast_m4136(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m7144_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m7144_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m5899_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m5899_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m5835_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m5835_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m7145_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m7145_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Physics_t1576_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m7146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Rigidbody_t994_CustomAttributesCacheGenerator_Rigidbody_AddForce_m4188(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Rigidbody_t994_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForce_m7147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Collider_t900_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m7148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m7152(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_Raycast_m5900(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m5826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m7154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Collider2D_t1379_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m7156(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_volume_m4226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_volume_m4227(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_pitch_m4221(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_pitch_m4145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_clip_m4144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_clip_m4282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_Play_m7172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_t755_AudioSource_Play_m7172_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_Play_m4283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m4147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_PlayOneShot_m7173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_t755_AudioSource_PlayOneShot_m7173_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("1.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_PlayOneShot_m4146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_mute_m4255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_minDistance_m4222(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_minDistance_m4223(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_maxDistance_m4224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_maxDistance_m4225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void AnimationEvent_t1588_CustomAttributesCacheGenerator_AnimationEvent_t1588____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void AnimationCurve_t1592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_t1592_AnimationCurve__ctor_m7198_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m7200(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_Init_m7202(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_INTERNAL_CALL_Rewind_m7206(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m4268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_t982_Animation_Play_m7207_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("PlayMode.StopSameLayer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m7208(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_t982_Animation_Play_m7208_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("PlayMode.StopSameLayer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m4049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_PlayDefaultAnimation_m7209(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_GetStateAtIndex_m7211(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animation_t982_CustomAttributesCacheGenerator_Animation_GetStateCount_m7212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void AnimatorStateInfo_t1589_CustomAttributesCacheGenerator_AnimatorStateInfo_t1589____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_set_speed_m4129(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_Play_m4214(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2950);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1658 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1658 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1658_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m7468(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7231_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("-1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7231_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("float.NegativeInfinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_Play_m7232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7232_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("-1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1657_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7232_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2943);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1657 * tmp;
		tmp = (DefaultValueAttribute_t1657 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1657_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7464(tmp, il2cpp_codegen_string_new_wrapper("float.NegativeInfinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m6098(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_StringToHash_m7233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_SetTriggerString_m7234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Animator_t749_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m7235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void CharacterInfo_t1601_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void CharacterInfo_t1601_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void CharacterInfo_t1601_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void CharacterInfo_t1601_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Font_t1222_CustomAttributesCacheGenerator_Font_get_material_m6110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Font_t1222_CustomAttributesCacheGenerator_Font_HasCharacter_m6000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Font_t1222_CustomAttributesCacheGenerator_Font_get_dynamic_m6113(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Font_t1222_CustomAttributesCacheGenerator_Font_get_fontSize_m6115(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var;
void FontTextureRebuildCallback_t1602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2949);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1703 * tmp;
		tmp = (EditorBrowsableAttribute_t1703 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1703_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7553(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_Init_m7264(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m7265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m6013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m7269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m7270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m7271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m7272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m7273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m7274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m5993(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m7275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m7276(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m6029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_renderMode_m5895(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m6130(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m5906(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m6114(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m6134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m5925(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m6135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m5882(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m5897(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m5896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_sortingLayerID_m5905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m5870(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void Canvas_t1229_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m6109(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m6087(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m7288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m5881(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m7291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m5885(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m6165(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m5879(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m7292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m7293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m5874(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m5871(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1606_il2cpp_TypeInfo_var;
void RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m5884(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2946);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1606 * tmp;
		tmp = (WrapperlessIcall_t1606 *)il2cpp_codegen_object_new (WrapperlessIcall_t1606_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m7301(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void MonoPInvokeCallbackAttribute_t1010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void IL2CPPStructAlignmentAttribute_t1607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 8, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DisallowMultipleComponent_t1437_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void RequireComponent_t1432_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void WritableAttribute_t1613_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2048, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyIsEditorAssembly_t1614_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_Achievement_get_id_m7337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_Achievement_set_id_m7338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m7339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Achievement_t1625_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m7340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AchievementDescription_t1626_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AchievementDescription_t1626_CustomAttributesCacheGenerator_AchievementDescription_get_id_m7347(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AchievementDescription_t1626_CustomAttributesCacheGenerator_AchievementDescription_set_id_m7348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_Score_get_leaderboardID_m7357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_Score_set_leaderboardID_m7358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_Score_get_value_m7359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Score_t1627_CustomAttributesCacheGenerator_Score_set_value_m7360(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_id_m7368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_id_m7369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m7370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m7371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_range_m7372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_range_m7373(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m7374(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m7375(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void PropertyAttribute_t1637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void TooltipAttribute_t1442_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SpaceAttribute_t1440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void RangeAttribute_t1436_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void TextAreaAttribute_t1443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SelectionBaseAttribute_t1441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var;
void StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m7391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2952);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1704 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1704 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7559(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var;
void StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m7394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2952);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1704 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1704 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7559(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var;
void StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m7396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2952);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1704 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1704 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1704_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7559(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SharedBetweenAnimatorsAttribute_t1640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ArgumentCache_t1646_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void PersistentCall_t1650_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void PersistentCall_t1650_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void PersistentCall_t1650_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void PersistentCall_t1650_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void PersistentCall_t1650_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void PersistentCallGroup_t1652_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void UnityEventBase_t1655_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void UnityEventBase_t1655_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void UserAuthorizationDialog_t1656_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t1657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 18432, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ExcludeFromDocsAttribute_t1658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void FormerlySerializedAsAttribute_t1430_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void TypeInferenceRuleAttribute_t1660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_Assembly_AttributeGenerators[665] = 
{
	NULL,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	AssetBundleCreateRequest_t1485_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m6349,
	AssetBundleCreateRequest_t1485_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m6350,
	AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m6354,
	AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m6355,
	AssetBundle_t1487_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m6356,
	LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_LayerToName_m6359,
	LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m6360,
	LayerMask_t1205_CustomAttributesCacheGenerator_LayerMask_t1205_LayerMask_GetMask_m6361_Arg0_ParameterInfo,
	RuntimePlatform_t1491_CustomAttributesCacheGenerator_NaCl,
	RuntimePlatform_t1491_CustomAttributesCacheGenerator_FlashPlayer,
	RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerX86,
	RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerX64,
	RuntimePlatform_t1491_CustomAttributesCacheGenerator_MetroPlayerARM,
	Coroutine_t1268_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m6364,
	ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m6366,
	ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_t15_ScriptableObject_Internal_CreateScriptableObject_m6366_Arg0_ParameterInfo,
	ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m6367,
	ScriptableObject_t15_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m6369,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m6374,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m6375,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m6376,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m6377,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m6378,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m6379,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m6380,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m6381,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m6382,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m6383,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m6384,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m6385,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m6386,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m6387,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m6388,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m6389,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m6390,
	GameCenterPlatform_t984_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m6392,
	GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m6436,
	GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m6437,
	GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m6438,
	GcLeaderboard_t1501_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m6439,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_enabled_m4112,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_castShadows_m4124,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_receiveShadows_m4125,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_material_m4140,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_material_m4121,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_bounds_m4019,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m4113,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_sortingLayerID_m4114,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m4115,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_set_sortingOrder_m4110,
	Renderer_t976_CustomAttributesCacheGenerator_Renderer_t976____castShadows_PropertyInfo,
	LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetWidth_m6460,
	LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetColors_m6461,
	LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetVertexCount_m6462,
	LineRenderer_t743_CustomAttributesCacheGenerator_LineRenderer_INTERNAL_CALL_SetPosition_m6463,
	Graphics_t1504_CustomAttributesCacheGenerator_Graphics_DrawTexture_m6464,
	Screen_t1506_CustomAttributesCacheGenerator_Screen_get_width_m4246,
	Screen_t1506_CustomAttributesCacheGenerator_Screen_get_height_m4088,
	Screen_t1506_CustomAttributesCacheGenerator_Screen_get_dpi_m6133,
	Screen_t1506_CustomAttributesCacheGenerator_Screen_get_orientation_m4195,
	Texture_t736_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m6472,
	Texture_t736_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m6473,
	Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m6477,
	Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_t384_Texture2D_Internal_Create_m6477_Arg0_ParameterInfo,
	Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m5873,
	Texture2D_t384_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m5948,
	RenderTexture_t1507_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m6478,
	RenderTexture_t1507_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m6479,
	GUILayer_t1510_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m6483,
	Gradient_t1513_CustomAttributesCacheGenerator_Gradient_Init_m6487,
	Gradient_t1513_CustomAttributesCacheGenerator_Gradient_Cleanup_m6488,
	GUI_t981_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField,
	GUI_t981_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField,
	GUI_t981_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m6496,
	GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_get_color_m6498,
	GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_set_color_m6499,
	GUI_t981_CustomAttributesCacheGenerator_GUI_set_changed_m6501,
	GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m6504,
	GUI_t981_CustomAttributesCacheGenerator_GUI_DrawTexture_m4089,
	GUI_t981_CustomAttributesCacheGenerator_GUI_DrawTexture_m4154,
	GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg2_ParameterInfo,
	GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg3_ParameterInfo,
	GUI_t981_CustomAttributesCacheGenerator_GUI_t981_GUI_DrawTexture_m6505_Arg4_ParameterInfo,
	GUI_t981_CustomAttributesCacheGenerator_GUI_get_blendMaterial_m6506,
	GUI_t981_CustomAttributesCacheGenerator_GUI_get_blitMaterial_m6507,
	GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m6510,
	GUI_t981_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoToggle_m6513,
	GUILayoutUtility_t1522_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m6527,
	GUILayoutUtility_t1522_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6529,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_GetControlID_m6560,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6563,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m6564,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m6565,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m6567,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m6569,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m6573,
	GUIUtility_t1529_CustomAttributesCacheGenerator_GUIUtility_set_mouseUsed_m6574,
	GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_Push_m6576,
	GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_Pop_m6577,
	GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_GetMatrix_m6578,
	GUIClip_t1530_CustomAttributesCacheGenerator_GUIClip_INTERNAL_CALL_SetMatrix_m6580,
	GUISettings_t1531_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord,
	GUISettings_t1531_CustomAttributesCacheGenerator_m_TripleClickSelectsLine,
	GUISettings_t1531_CustomAttributesCacheGenerator_m_CursorColor,
	GUISettings_t1531_CustomAttributesCacheGenerator_m_CursorFlashSpeed,
	GUISettings_t1531_CustomAttributesCacheGenerator_m_SelectionColor,
	GUISkin_t985_CustomAttributesCacheGenerator,
	GUISkin_t985_CustomAttributesCacheGenerator_m_Font,
	GUISkin_t985_CustomAttributesCacheGenerator_m_box,
	GUISkin_t985_CustomAttributesCacheGenerator_m_button,
	GUISkin_t985_CustomAttributesCacheGenerator_m_toggle,
	GUISkin_t985_CustomAttributesCacheGenerator_m_label,
	GUISkin_t985_CustomAttributesCacheGenerator_m_textField,
	GUISkin_t985_CustomAttributesCacheGenerator_m_textArea,
	GUISkin_t985_CustomAttributesCacheGenerator_m_window,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalSlider,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalSliderThumb,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalSlider,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalSliderThumb,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbar,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton,
	GUISkin_t985_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbar,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarThumb,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton,
	GUISkin_t985_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton,
	GUISkin_t985_CustomAttributesCacheGenerator_m_ScrollView,
	GUISkin_t985_CustomAttributesCacheGenerator_m_CustomStyles,
	GUISkin_t985_CustomAttributesCacheGenerator_m_Settings,
	GUIContent_t986_CustomAttributesCacheGenerator_m_Text,
	GUIContent_t986_CustomAttributesCacheGenerator_m_Image,
	GUIContent_t986_CustomAttributesCacheGenerator_m_Tooltip,
	GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_Init_m6647,
	GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m6648,
	GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m6649,
	GUIStyleState_t991_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m6650,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_Init_m6655,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_Cleanup_m6656,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_left_m6148,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_left_m6657,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_right_m6658,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_right_m6659,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_top_m6149,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_top_m6660,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_bottom_m6661,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_set_bottom_m6662,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m6142,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_get_vertical_m6143,
	RectOffset_t1321_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m6664,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Init_m6668,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m6669,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_name_m6670,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_name_m6671,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m6673,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m6676,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m6677,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m6678,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m6679,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m6680,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m6681,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m6682,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m6683,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_get_fontSize_m4085,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_set_fontSize_m4045,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Draw_m6685,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_t724_GUIStyle_Draw_m6686_Arg3_ParameterInfo,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_Draw2_m6688,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m6689,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6693,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m6694,
	GUIStyle_t724_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m6696,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m6699,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m6701,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m6039,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m6040,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg1_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg2_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg3_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg4_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg5_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_t1262_TouchScreenKeyboard_Open_m6702_Arg6_ParameterInfo,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m5975,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m5976,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m6038,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m5974,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m6037,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m5987,
	TouchScreenKeyboard_t1262_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m5986,
	Event_t1269_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	Event_t1269_CustomAttributesCacheGenerator_Event_Init_m6703,
	Event_t1269_CustomAttributesCacheGenerator_Event_Cleanup_m6705,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_rawType_m6001,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_type_m6706,
	Event_t1269_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m6708,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_modifiers_m5997,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_character_m5999,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_commandName_m6709,
	Event_t1269_CustomAttributesCacheGenerator_Event_get_keyCode_m5998,
	Event_t1269_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m6711,
	Event_t1269_CustomAttributesCacheGenerator_Event_PopEvent_m6002,
	EventModifiers_t1538_CustomAttributesCacheGenerator,
	Vector2_t739_CustomAttributesCacheGenerator,
	Vector3_t758_CustomAttributesCacheGenerator,
	Color_t747_CustomAttributesCacheGenerator,
	Color32_t992_CustomAttributesCacheGenerator,
	Quaternion_t771_CustomAttributesCacheGenerator,
	Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6737,
	Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6738,
	Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_UnclampedSlerp_m6740,
	Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6741,
	Quaternion_t771_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6745,
	Matrix4x4_t997_CustomAttributesCacheGenerator,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6760,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6762,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6764,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6767,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6778,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6781,
	Matrix4x4_t997_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6782,
	Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6798,
	Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6801,
	Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6804,
	Bounds_t979_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6808,
	Vector4_t1362_CustomAttributesCacheGenerator,
	Mathf_t980_CustomAttributesCacheGenerator_Mathf_t980_Mathf_SmoothDamp_m6060_Arg4_ParameterInfo,
	Mathf_t980_CustomAttributesCacheGenerator_Mathf_t980_Mathf_SmoothDamp_m6060_Arg5_ParameterInfo,
	DrivenTransformProperties_t1540_CustomAttributesCacheGenerator,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6840,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6841,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6842,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6843,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6844,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6845,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6846,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6847,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6848,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6849,
	RectTransform_t1227_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6850,
	Resources_t1544_CustomAttributesCacheGenerator_Resources_Load_m6856,
	SerializePrivateVariables_t1545_CustomAttributesCacheGenerator,
	Shader_t988_CustomAttributesCacheGenerator_Shader_Find_m4119,
	Shader_t988_CustomAttributesCacheGenerator_Shader_PropertyToID_m6858,
	Material_t989_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6861,
	Material_t989_CustomAttributesCacheGenerator_Material_GetTexture_m6863,
	Material_t989_CustomAttributesCacheGenerator_Material_SetFloat_m6865,
	Material_t989_CustomAttributesCacheGenerator_Material_HasProperty_m6866,
	Material_t989_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6867,
	Material_t989_CustomAttributesCacheGenerator_Material_t989_Material_Internal_CreateWithShader_m6867_Arg0_ParameterInfo,
	Material_t989_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6868,
	Material_t989_CustomAttributesCacheGenerator_Material_t989_Material_Internal_CreateWithMaterial_m6868_Arg0_ParameterInfo,
	SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator,
	SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6871,
	SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6874,
	SphericalHarmonicsL2_t1547_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6877,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_bounds_m4187,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_rect_m5928,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m5924,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_texture_m5921,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_textureRect_m5945,
	Sprite_t766_CustomAttributesCacheGenerator_Sprite_get_border_m5922,
	SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_GetSprite_INTERNAL_m6887,
	SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_SetSprite_INTERNAL_m6888,
	SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_INTERNAL_get_color_m6889,
	SpriteRenderer_t744_CustomAttributesCacheGenerator_SpriteRenderer_INTERNAL_set_color_m6890,
	DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m5935,
	DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m5934,
	DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_GetPadding_m5927,
	DataUtility_t1548_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6891,
	Hash128_t1549_CustomAttributesCacheGenerator_Hash128_ToString_m6893,
	WWW_t383_CustomAttributesCacheGenerator_WWW__ctor_m6896,
	WWW_t383_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6901,
	WWW_t383_CustomAttributesCacheGenerator_WWW_InitWWW_m6902,
	WWW_t383_CustomAttributesCacheGenerator_WWW_enforceWebSecurityRestrictions_m6903,
	WWW_t383_CustomAttributesCacheGenerator_WWW_EscapeURL_m6904,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_EscapeURL_m6905_Arg1_ParameterInfo,
	WWW_t383_CustomAttributesCacheGenerator_WWW_UnEscapeURL_m6906,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_UnEscapeURL_m6907_Arg1_ParameterInfo,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6909,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_bytes_m6914,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_size_m6915,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_error_m6916,
	WWW_t383_CustomAttributesCacheGenerator_WWW_GetTexture_m6917,
	WWW_t383_CustomAttributesCacheGenerator_WWW_GetAudioClipInternal_m6926,
	WWW_t383_CustomAttributesCacheGenerator_WWW_LoadImageIntoTexture_m6927,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_isDone_m3720,
	WWW_t383_CustomAttributesCacheGenerator_WWW_GetURL_m6928,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_progress_m6930,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_uploadProgress_m6931,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_bytesDownloaded_m6932,
	WWW_t383_CustomAttributesCacheGenerator_WWW_LoadUnityWeb_m6934,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_url_m3717,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_assetBundle_m6935,
	WWW_t383_CustomAttributesCacheGenerator_WWW_get_threadPriority_m6936,
	WWW_t383_CustomAttributesCacheGenerator_WWW_set_threadPriority_m6937,
	WWW_t383_CustomAttributesCacheGenerator_WWW_INTERNAL_CALL_WWW_m6938,
	WWW_t383_CustomAttributesCacheGenerator_WWW_LoadFromCacheOrDownload_m6939,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_LoadFromCacheOrDownload_m6940_Arg2_ParameterInfo,
	WWW_t383_CustomAttributesCacheGenerator_WWW_LoadFromCacheOrDownload_m6941,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383_WWW_LoadFromCacheOrDownload_m6942_Arg2_ParameterInfo,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383____data_PropertyInfo,
	WWW_t383_CustomAttributesCacheGenerator_WWW_t383____oggVorbis_PropertyInfo,
	WWWForm_t171_CustomAttributesCacheGenerator_WWWForm_AddField_m915,
	WWWForm_t171_CustomAttributesCacheGenerator_WWWForm_t171_WWWForm_AddField_m6945_Arg2_ParameterInfo,
	WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_URLEncode_m6951_Arg1_ParameterInfo,
	WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_QPEncode_m6953_Arg1_ParameterInfo,
	WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_URLDecode_m6956_Arg1_ParameterInfo,
	WWWTranscoder_t1551_CustomAttributesCacheGenerator_WWWTranscoder_t1551_WWWTranscoder_SevenBitClean_m6958_Arg1_ParameterInfo,
	CacheIndex_t1552_CustomAttributesCacheGenerator,
	UnityString_t1553_CustomAttributesCacheGenerator_UnityString_t1553_UnityString_Format_m6960_Arg1_ParameterInfo,
	AsyncOperation_t1486_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6962,
	Application_t1556_CustomAttributesCacheGenerator_Application_Quit_m1017,
	Application_t1556_CustomAttributesCacheGenerator_Application_LoadLevelAsync_m6968,
	Application_t1556_CustomAttributesCacheGenerator_Application_get_isPlaying_m3733,
	Application_t1556_CustomAttributesCacheGenerator_Application_get_isEditor_m1032,
	Application_t1556_CustomAttributesCacheGenerator_Application_get_platform_m1033,
	Application_t1556_CustomAttributesCacheGenerator_Application_get_unityVersion_m1058,
	Application_t1556_CustomAttributesCacheGenerator_Application_set_targetFrameRate_m4239,
	Application_t1556_CustomAttributesCacheGenerator_Application_get_internetReachability_m1063,
	Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_get_enabled_m5770,
	Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_set_enabled_m4228,
	Behaviour_t1416_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m5771,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m5823,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m5822,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_orthographicSize_m4024,
	Camera_t978_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4173,
	Camera_t978_CustomAttributesCacheGenerator_Camera_set_orthographic_m4022,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_depth_m5738,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_aspect_m4023,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_cullingMask_m5833,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_eventMask_m6975,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_get_backgroundColor_m6976,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_set_backgroundColor_m6977,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6978,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6979,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_targetTexture_m6981,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6982,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6983,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ViewportToWorldPoint_m6984,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToWorldPoint_m6985,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6986,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6987,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_main_m4021,
	Camera_t978_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6988,
	Camera_t978_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6989,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6994,
	Camera_t978_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6996,
	CameraCallback_t1557_CustomAttributesCacheGenerator,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg2_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg3_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawLine_m6997_Arg4_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6998,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_DrawRay_m4135,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg2_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg3_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_DrawRay_m6999_Arg4_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_Internal_Log_m7000,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_Internal_Log_m7000_Arg2_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_Internal_LogException_m7001,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_t1558_Debug_Internal_LogException_m7001_Arg1_ParameterInfo,
	Debug_t1558_CustomAttributesCacheGenerator_Debug_get_isDebugBuild_m931,
	Display_t1561_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m7027,
	Display_t1561_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m7028,
	Display_t1561_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m7029,
	Display_t1561_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m7030,
	Display_t1561_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m7031,
	Display_t1561_CustomAttributesCacheGenerator_Display_SetParamsImpl_m7032,
	Display_t1561_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m7033,
	Display_t1561_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m7034,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m7035,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m4178,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_t26_MonoBehaviour_StartCoroutine_m4178_Arg1_ParameterInfo,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_m4065,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m7037,
	MonoBehaviour_t26_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m7038,
	Input_t987_CustomAttributesCacheGenerator_Input_GetKeyString_m7040,
	Input_t987_CustomAttributesCacheGenerator_Input_GetKeyDownString_m7041,
	Input_t987_CustomAttributesCacheGenerator_Input_GetAxisRaw_m5814,
	Input_t987_CustomAttributesCacheGenerator_Input_GetButtonDown_m4095,
	Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButton_m5819,
	Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m5785,
	Input_t987_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m5786,
	Input_t987_CustomAttributesCacheGenerator_Input_get_mousePosition_m5787,
	Input_t987_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m5788,
	Input_t987_CustomAttributesCacheGenerator_Input_GetTouch_m4097,
	Input_t987_CustomAttributesCacheGenerator_Input_get_touchCount_m4096,
	Input_t987_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m6041,
	Input_t987_CustomAttributesCacheGenerator_Input_get_compositionString_m5977,
	Input_t987_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m7042,
	HideFlags_t1563_CustomAttributesCacheGenerator,
	Object_t187_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m7044,
	Object_t187_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m7046,
	Object_t187_CustomAttributesCacheGenerator_Object_Destroy_m7047,
	Object_t187_CustomAttributesCacheGenerator_Object_t187_Object_Destroy_m7047_Arg1_ParameterInfo,
	Object_t187_CustomAttributesCacheGenerator_Object_Destroy_m855,
	Object_t187_CustomAttributesCacheGenerator_Object_DestroyImmediate_m7048,
	Object_t187_CustomAttributesCacheGenerator_Object_t187_Object_DestroyImmediate_m7048_Arg1_ParameterInfo,
	Object_t187_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6042,
	Object_t187_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m7049,
	Object_t187_CustomAttributesCacheGenerator_Object_get_name_m1064,
	Object_t187_CustomAttributesCacheGenerator_Object_set_name_m1062,
	Object_t187_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m854,
	Object_t187_CustomAttributesCacheGenerator_Object_set_hideFlags_m1037,
	Object_t187_CustomAttributesCacheGenerator_Object_ToString_m1201,
	Object_t187_CustomAttributesCacheGenerator_Object_Instantiate_m4028,
	Object_t187_CustomAttributesCacheGenerator_Object_FindObjectOfType_m1060,
	Component_t230_CustomAttributesCacheGenerator_Component_get_transform_m4030,
	Component_t230_CustomAttributesCacheGenerator_Component_get_gameObject_m853,
	Component_t230_CustomAttributesCacheGenerator_Component_GetComponent_m6155,
	Component_t230_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m7056,
	Component_t230_CustomAttributesCacheGenerator_Component_GetComponent_m7558,
	Component_t230_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m7057,
	Component_t230_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m7058,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponent_m7059,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m7060,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponent_m7564,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m7061,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m7062,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_transform_m4029,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_layer_m6016,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_set_layer_m6017,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_SetActive_m4272,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m4271,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_get_tag_m4048,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_FindGameObjectWithTag_m4082,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_FindGameObjectsWithTag_m4018,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_SendMessage_m7063,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_SendMessage_m7063_Arg1_ParameterInfo,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_SendMessage_m7063_Arg2_ParameterInfo,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m7064,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_AddComponent_m7065,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m7066,
	GameObject_t144_CustomAttributesCacheGenerator_GameObject_t144_GameObject_Internal_CreateGameObject_m7066_Arg0_ParameterInfo,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m7070,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m7071,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m7072,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m7073,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m7074,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m7075,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m7076,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m7077,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m7078,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m7079,
	Transform_t809_CustomAttributesCacheGenerator_Transform_get_parentInternal_m7080,
	Transform_t809_CustomAttributesCacheGenerator_Transform_set_parentInternal_m7081,
	Transform_t809_CustomAttributesCacheGenerator_Transform_SetParent_m7082,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m7083,
	Transform_t809_CustomAttributesCacheGenerator_Transform_Translate_m4201,
	Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Translate_m7084_Arg1_ParameterInfo,
	Transform_t809_CustomAttributesCacheGenerator_Transform_Translate_m4047,
	Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Translate_m7085_Arg3_ParameterInfo,
	Transform_t809_CustomAttributesCacheGenerator_Transform_Rotate_m4259,
	Transform_t809_CustomAttributesCacheGenerator_Transform_t809_Transform_Rotate_m7086_Arg1_ParameterInfo,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_RotateAroundInternal_m7088,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformDirection_m7090,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m7091,
	Transform_t809_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m7092,
	Transform_t809_CustomAttributesCacheGenerator_Transform_get_childCount_m4051,
	Transform_t809_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m6015,
	Transform_t809_CustomAttributesCacheGenerator_Transform_GetChild_m4050,
	Time_t1565_CustomAttributesCacheGenerator_Time_get_deltaTime_m4036,
	Time_t1565_CustomAttributesCacheGenerator_Time_get_unscaledTime_m5816,
	Time_t1565_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m5846,
	Time_t1565_CustomAttributesCacheGenerator_Time_get_timeScale_m4203,
	Time_t1565_CustomAttributesCacheGenerator_Time_set_timeScale_m4091,
	Random_t1566_CustomAttributesCacheGenerator_Random_Range_m4128,
	Random_t1566_CustomAttributesCacheGenerator_Random_RandomRangeInt_m7094,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_TrySetInt_m7097,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m7098,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_t1568_PlayerPrefs_GetInt_m7098_Arg1_ParameterInfo,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_GetInt_m4252,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_HasKey_m4251,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_DeleteAll_m4253,
	PlayerPrefs_t1568_CustomAttributesCacheGenerator_PlayerPrefs_Save_m4254,
	ADBannerView_t717_CustomAttributesCacheGenerator_ADBannerView_Native_CreateBanner_m7108,
	ADBannerView_t717_CustomAttributesCacheGenerator_ADBannerView_Native_DestroyBanner_m7109,
	ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_CreateInterstitial_m7118,
	ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_ShowInterstitial_m7119,
	ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_ReloadInterstitial_m7120,
	ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_InterstitialAdLoaded_m7121,
	ADInterstitialAd_t331_CustomAttributesCacheGenerator_ADInterstitialAd_Native_DestroyInterstitial_m7122,
	DeviceGeneration_t995_CustomAttributesCacheGenerator_iPad5Gen,
	Device_t1573_CustomAttributesCacheGenerator_Device_get_generation_m4205,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m7143,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_Raycast_m4136,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m7144_Arg3_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m7144_Arg4_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m5899_Arg2_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_Raycast_m5899_Arg3_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m5835_Arg1_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m5835_Arg2_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m7145_Arg2_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_t1576_Physics_RaycastAll_m7145_Arg3_ParameterInfo,
	Physics_t1576_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m7146,
	Rigidbody_t994_CustomAttributesCacheGenerator_Rigidbody_AddForce_m4188,
	Rigidbody_t994_CustomAttributesCacheGenerator_Rigidbody_INTERNAL_CALL_AddForce_m7147,
	Collider_t900_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m7148,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m7152,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_Raycast_m5900,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg2_ParameterInfo,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg3_ParameterInfo,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg4_ParameterInfo,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_t1377_Physics2D_Raycast_m7153_Arg5_ParameterInfo,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m5826,
	Physics2D_t1377_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m7154,
	Collider2D_t1379_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m7156,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_volume_m4226,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_volume_m4227,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_pitch_m4221,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_pitch_m4145,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_clip_m4144,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_clip_m4282,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_Play_m7172,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_t755_AudioSource_Play_m7172_Arg0_ParameterInfo,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_Play_m4283,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_isPlaying_m4147,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_PlayOneShot_m7173,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_t755_AudioSource_PlayOneShot_m7173_Arg1_ParameterInfo,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_PlayOneShot_m4146,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_mute_m4255,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_minDistance_m4222,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_minDistance_m4223,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_get_maxDistance_m4224,
	AudioSource_t755_CustomAttributesCacheGenerator_AudioSource_set_maxDistance_m4225,
	AnimationEvent_t1588_CustomAttributesCacheGenerator_AnimationEvent_t1588____data_PropertyInfo,
	AnimationCurve_t1592_CustomAttributesCacheGenerator,
	AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_t1592_AnimationCurve__ctor_m7198_Arg0_ParameterInfo,
	AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m7200,
	AnimationCurve_t1592_CustomAttributesCacheGenerator_AnimationCurve_Init_m7202,
	Animation_t982_CustomAttributesCacheGenerator,
	Animation_t982_CustomAttributesCacheGenerator_Animation_INTERNAL_CALL_Rewind_m7206,
	Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m4268,
	Animation_t982_CustomAttributesCacheGenerator_Animation_t982_Animation_Play_m7207_Arg0_ParameterInfo,
	Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m7208,
	Animation_t982_CustomAttributesCacheGenerator_Animation_t982_Animation_Play_m7208_Arg1_ParameterInfo,
	Animation_t982_CustomAttributesCacheGenerator_Animation_Play_m4049,
	Animation_t982_CustomAttributesCacheGenerator_Animation_PlayDefaultAnimation_m7209,
	Animation_t982_CustomAttributesCacheGenerator_Animation_GetStateAtIndex_m7211,
	Animation_t982_CustomAttributesCacheGenerator_Animation_GetStateCount_m7212,
	AnimatorStateInfo_t1589_CustomAttributesCacheGenerator_AnimatorStateInfo_t1589____nameHash_PropertyInfo,
	Animator_t749_CustomAttributesCacheGenerator_Animator_set_speed_m4129,
	Animator_t749_CustomAttributesCacheGenerator_Animator_Play_m4214,
	Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7231_Arg1_ParameterInfo,
	Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7231_Arg2_ParameterInfo,
	Animator_t749_CustomAttributesCacheGenerator_Animator_Play_m7232,
	Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7232_Arg1_ParameterInfo,
	Animator_t749_CustomAttributesCacheGenerator_Animator_t749_Animator_Play_m7232_Arg2_ParameterInfo,
	Animator_t749_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m6098,
	Animator_t749_CustomAttributesCacheGenerator_Animator_StringToHash_m7233,
	Animator_t749_CustomAttributesCacheGenerator_Animator_SetTriggerString_m7234,
	Animator_t749_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m7235,
	CharacterInfo_t1601_CustomAttributesCacheGenerator_uv,
	CharacterInfo_t1601_CustomAttributesCacheGenerator_vert,
	CharacterInfo_t1601_CustomAttributesCacheGenerator_width,
	CharacterInfo_t1601_CustomAttributesCacheGenerator_flipped,
	Font_t1222_CustomAttributesCacheGenerator_Font_get_material_m6110,
	Font_t1222_CustomAttributesCacheGenerator_Font_HasCharacter_m6000,
	Font_t1222_CustomAttributesCacheGenerator_Font_get_dynamic_m6113,
	Font_t1222_CustomAttributesCacheGenerator_Font_get_fontSize_m6115,
	FontTextureRebuildCallback_t1602_CustomAttributesCacheGenerator,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_Init_m7264,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m7265,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m7268,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m6013,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m7269,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m7270,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m7271,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m7272,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m7273,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m7274,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m5993,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m7275,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m7276,
	TextGenerator_t1266_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m6029,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_renderMode_m5895,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m6130,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m5906,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m6114,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m6134,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m5925,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m6135,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m5882,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m5897,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m5896,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_get_sortingLayerID_m5905,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m5870,
	Canvas_t1229_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m6109,
	CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m6087,
	CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m7288,
	CanvasGroup_t1387_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m5881,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m7291,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m5885,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m6165,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m5879,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m7292,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m7293,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m5874,
	CanvasRenderer_t1228_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m5871,
	RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m7295,
	RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m7297,
	RectTransformUtility_t1389_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m5884,
	MonoPInvokeCallbackAttribute_t1010_CustomAttributesCacheGenerator,
	IL2CPPStructAlignmentAttribute_t1607_CustomAttributesCacheGenerator,
	DisallowMultipleComponent_t1437_CustomAttributesCacheGenerator,
	RequireComponent_t1432_CustomAttributesCacheGenerator,
	WritableAttribute_t1613_CustomAttributesCacheGenerator,
	AssemblyIsEditorAssembly_t1614_CustomAttributesCacheGenerator,
	Achievement_t1625_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t1625_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t1625_CustomAttributesCacheGenerator_Achievement_get_id_m7337,
	Achievement_t1625_CustomAttributesCacheGenerator_Achievement_set_id_m7338,
	Achievement_t1625_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m7339,
	Achievement_t1625_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m7340,
	AchievementDescription_t1626_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t1626_CustomAttributesCacheGenerator_AchievementDescription_get_id_m7347,
	AchievementDescription_t1626_CustomAttributesCacheGenerator_AchievementDescription_set_id_m7348,
	Score_t1627_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t1627_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t1627_CustomAttributesCacheGenerator_Score_get_leaderboardID_m7357,
	Score_t1627_CustomAttributesCacheGenerator_Score_set_leaderboardID_m7358,
	Score_t1627_CustomAttributesCacheGenerator_Score_get_value_m7359,
	Score_t1627_CustomAttributesCacheGenerator_Score_set_value_m7360,
	Leaderboard_t1500_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t1500_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t1500_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t1500_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_id_m7368,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_id_m7369,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m7370,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m7371,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_range_m7372,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_range_m7373,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m7374,
	Leaderboard_t1500_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m7375,
	PropertyAttribute_t1637_CustomAttributesCacheGenerator,
	TooltipAttribute_t1442_CustomAttributesCacheGenerator,
	SpaceAttribute_t1440_CustomAttributesCacheGenerator,
	RangeAttribute_t1436_CustomAttributesCacheGenerator,
	TextAreaAttribute_t1443_CustomAttributesCacheGenerator,
	SelectionBaseAttribute_t1441_CustomAttributesCacheGenerator,
	StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m7391,
	StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m7394,
	StackTraceUtility_t1639_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m7396,
	SharedBetweenAnimatorsAttribute_t1640_CustomAttributesCacheGenerator,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t1646_CustomAttributesCacheGenerator_m_BoolArgument,
	PersistentCall_t1650_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t1650_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t1650_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t1650_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t1650_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t1652_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t1655_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t1655_CustomAttributesCacheGenerator_m_TypeName,
	UserAuthorizationDialog_t1656_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t1657_CustomAttributesCacheGenerator,
	ExcludeFromDocsAttribute_t1658_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t1430_CustomAttributesCacheGenerator,
	TypeInferenceRuleAttribute_t1660_CustomAttributesCacheGenerator,
};
