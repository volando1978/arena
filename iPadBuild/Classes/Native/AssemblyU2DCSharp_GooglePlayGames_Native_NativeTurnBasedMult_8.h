﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
struct MatchOutcome_t345;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56
struct  U3CFinishU3Ec__AnonStorey56_t645  : public Object_t
{
	// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::outcome
	MatchOutcome_t345 * ___outcome_0;
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::callback
	Action_1_t98 * ___callback_1;
	// System.Byte[] GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::data
	ByteU5BU5D_t350* ___data_2;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_3;
};
