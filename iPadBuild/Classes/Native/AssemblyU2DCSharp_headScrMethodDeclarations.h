﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// headScr
struct headScr_t781;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void headScr::.ctor()
extern "C" void headScr__ctor_m3550 (headScr_t781 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void headScr::OnExplosion(UnityEngine.Vector3)
extern "C" void headScr_OnExplosion_m3551 (headScr_t781 * __this, Vector3_t758  ___pos, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void headScr::Start()
extern "C" void headScr_Start_m3552 (headScr_t781 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void headScr::Update()
extern "C" void headScr_Update_m3553 (headScr_t781 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void headScr::destroyParts()
extern "C" void headScr_destroyParts_m3554 (headScr_t781 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
