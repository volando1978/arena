﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// Soomla.Reward
struct Reward_t55;
// Soomla.Reward
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Reward.h"
// Soomla.RandomReward
struct  RandomReward_t57  : public Reward_t55
{
	// System.Collections.Generic.List`1<Soomla.Reward> Soomla.RandomReward::Rewards
	List_1_t56 * ___Rewards_8;
	// Soomla.Reward Soomla.RandomReward::LastGivenReward
	Reward_t55 * ___LastGivenReward_9;
};
struct RandomReward_t57_StaticFields{
	// System.String Soomla.RandomReward::TAG
	String_t* ___TAG_7;
};
