﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Touch>
struct InternalEnumerator_1_t3862;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21441_gshared (InternalEnumerator_1_t3862 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m21441(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3862 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m21441_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21442_gshared (InternalEnumerator_1_t3862 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21442(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3862 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21442_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21443_gshared (InternalEnumerator_1_t3862 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21443(__this, method) (( void (*) (InternalEnumerator_1_t3862 *, MethodInfo*))InternalEnumerator_1_Dispose_m21443_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21444_gshared (InternalEnumerator_1_t3862 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21444(__this, method) (( bool (*) (InternalEnumerator_1_t3862 *, MethodInfo*))InternalEnumerator_1_MoveNext_m21444_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern "C" Touch_t901  InternalEnumerator_1_get_Current_m21445_gshared (InternalEnumerator_1_t3862 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21445(__this, method) (( Touch_t901  (*) (InternalEnumerator_1_t3862 *, MethodInfo*))InternalEnumerator_1_get_Current_m21445_gshared)(__this, method)
