﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t4117;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25033_gshared (DefaultComparer_t4117 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m25033(__this, method) (( void (*) (DefaultComparer_t4117 *, MethodInfo*))DefaultComparer__ctor_m25033_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m25034_gshared (DefaultComparer_t4117 * __this, UILineInfo_t1398  ___x, UILineInfo_t1398  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m25034(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4117 *, UILineInfo_t1398 , UILineInfo_t1398 , MethodInfo*))DefaultComparer_Compare_m25034_gshared)(__this, ___x, ___y, method)
