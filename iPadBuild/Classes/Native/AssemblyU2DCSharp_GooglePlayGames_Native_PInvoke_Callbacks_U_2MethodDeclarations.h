﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Byte,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3772;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Byte,System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20344_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3772 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20344(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3772 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2__ctor_m20344_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2/<AsOnGameThreadCallback>c__AnonStorey61`2<System.Byte,System.Object>::<>m__78()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20345_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3772 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20345(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_t3772 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey61_2_U3CU3Em__78_m20345_gshared)(__this, method)
