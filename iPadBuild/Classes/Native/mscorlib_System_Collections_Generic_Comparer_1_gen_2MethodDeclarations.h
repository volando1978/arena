﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>
struct Comparer_1_t3840;
// System.Object
struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::.ctor()
extern "C" void Comparer_1__ctor_m21174_gshared (Comparer_1_t3840 * __this, MethodInfo* method);
#define Comparer_1__ctor_m21174(__this, method) (( void (*) (Comparer_1_t3840 *, MethodInfo*))Comparer_1__ctor_m21174_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::.cctor()
extern "C" void Comparer_1__cctor_m21175_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1__cctor_m21175(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1__cctor_m21175_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m21176_gshared (Comparer_1_t3840 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m21176(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3840 *, Object_t *, Object_t *, MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m21176_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::get_Default()
extern "C" Comparer_1_t3840 * Comparer_1_get_Default_m21177_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1_get_Default_m21177(__this /* static, unused */, method) (( Comparer_1_t3840 * (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1_get_Default_m21177_gshared)(__this /* static, unused */, method)
