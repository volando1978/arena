﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct  Predicate_1_t3468  : public MulticastDelegate_t22
{
};
