﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapper.h"
// System.Collections.ArrayList/SynchronizedArrayListWrapper
struct  SynchronizedArrayListWrapper_t2430  : public ArrayListWrapper_t2429
{
	// System.Object System.Collections.ArrayList/SynchronizedArrayListWrapper::m_SyncRoot
	Object_t * ___m_SyncRoot_6;
};
