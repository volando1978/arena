﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t706;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::.ctor(System.IntPtr)
extern "C" void MatchInboxUIResponse__ctor_m3059 (MatchInboxUIResponse_t706 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::UiStatus()
extern "C" int32_t MatchInboxUIResponse_UiStatus_m3060 (MatchInboxUIResponse_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::Match()
extern "C" NativeTurnBasedMatch_t680 * MatchInboxUIResponse_Match_m3061 (MatchInboxUIResponse_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void MatchInboxUIResponse_CallDispose_m3062 (MatchInboxUIResponse_t706 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse::FromPointer(System.IntPtr)
extern "C" MatchInboxUIResponse_t706 * MatchInboxUIResponse_FromPointer_m3063 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
