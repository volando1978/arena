﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.Utils
struct Utils_t151;
// System.String
struct String_t;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"

// System.Void UnityEngine.Advertisements.Utils::Log(UnityEngine.Advertisements.Advertisement/DebugLevel,System.String)
extern "C" void Utils_Log_m731 (Object_t * __this /* static, unused */, int32_t ___debugLevel, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Utils::LogDebug(System.String)
extern "C" void Utils_LogDebug_m732 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Utils::LogInfo(System.String)
extern "C" void Utils_LogInfo_m733 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Utils::LogWarning(System.String)
extern "C" void Utils_LogWarning_m734 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Utils::LogError(System.String)
extern "C" void Utils_LogError_m735 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
