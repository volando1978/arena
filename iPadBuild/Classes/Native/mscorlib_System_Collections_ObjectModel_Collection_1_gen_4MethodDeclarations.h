﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t3893;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t3888;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t4420;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t3891;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m21894_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1__ctor_m21894(__this, method) (( void (*) (Collection_1_t3893 *, MethodInfo*))Collection_1__ctor_m21894_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21895_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21895(__this, method) (( bool (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21895_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21896_gshared (Collection_1_t3893 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m21896(__this, ___array, ___index, method) (( void (*) (Collection_1_t3893 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m21896_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m21897_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m21897(__this, method) (( Object_t * (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m21897_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m21898_gshared (Collection_1_t3893 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m21898(__this, ___value, method) (( int32_t (*) (Collection_1_t3893 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m21898_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m21899_gshared (Collection_1_t3893 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m21899(__this, ___value, method) (( bool (*) (Collection_1_t3893 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m21899_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m21900_gshared (Collection_1_t3893 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m21900(__this, ___value, method) (( int32_t (*) (Collection_1_t3893 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m21900_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m21901_gshared (Collection_1_t3893 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m21901(__this, ___index, ___value, method) (( void (*) (Collection_1_t3893 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m21901_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m21902_gshared (Collection_1_t3893 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m21902(__this, ___value, method) (( void (*) (Collection_1_t3893 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m21902_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m21903_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m21903(__this, method) (( bool (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m21903_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m21904_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m21904(__this, method) (( Object_t * (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m21904_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m21905_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m21905(__this, method) (( bool (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m21905_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m21906_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m21906(__this, method) (( bool (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m21906_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m21907_gshared (Collection_1_t3893 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m21907(__this, ___index, method) (( Object_t * (*) (Collection_1_t3893 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m21907_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m21908_gshared (Collection_1_t3893 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m21908(__this, ___index, ___value, method) (( void (*) (Collection_1_t3893 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m21908_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m21909_gshared (Collection_1_t3893 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_Add_m21909(__this, ___item, method) (( void (*) (Collection_1_t3893 *, RaycastResult_t1187 , MethodInfo*))Collection_1_Add_m21909_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m21910_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_Clear_m21910(__this, method) (( void (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_Clear_m21910_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m21911_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m21911(__this, method) (( void (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_ClearItems_m21911_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m21912_gshared (Collection_1_t3893 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_Contains_m21912(__this, ___item, method) (( bool (*) (Collection_1_t3893 *, RaycastResult_t1187 , MethodInfo*))Collection_1_Contains_m21912_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m21913_gshared (Collection_1_t3893 * __this, RaycastResultU5BU5D_t3888* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m21913(__this, ___array, ___index, method) (( void (*) (Collection_1_t3893 *, RaycastResultU5BU5D_t3888*, int32_t, MethodInfo*))Collection_1_CopyTo_m21913_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m21914_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m21914(__this, method) (( Object_t* (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_GetEnumerator_m21914_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m21915_gshared (Collection_1_t3893 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m21915(__this, ___item, method) (( int32_t (*) (Collection_1_t3893 *, RaycastResult_t1187 , MethodInfo*))Collection_1_IndexOf_m21915_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m21916_gshared (Collection_1_t3893 * __this, int32_t ___index, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_Insert_m21916(__this, ___index, ___item, method) (( void (*) (Collection_1_t3893 *, int32_t, RaycastResult_t1187 , MethodInfo*))Collection_1_Insert_m21916_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m21917_gshared (Collection_1_t3893 * __this, int32_t ___index, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m21917(__this, ___index, ___item, method) (( void (*) (Collection_1_t3893 *, int32_t, RaycastResult_t1187 , MethodInfo*))Collection_1_InsertItem_m21917_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m21918_gshared (Collection_1_t3893 * __this, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_Remove_m21918(__this, ___item, method) (( bool (*) (Collection_1_t3893 *, RaycastResult_t1187 , MethodInfo*))Collection_1_Remove_m21918_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m21919_gshared (Collection_1_t3893 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m21919(__this, ___index, method) (( void (*) (Collection_1_t3893 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m21919_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m21920_gshared (Collection_1_t3893 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m21920(__this, ___index, method) (( void (*) (Collection_1_t3893 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m21920_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m21921_gshared (Collection_1_t3893 * __this, MethodInfo* method);
#define Collection_1_get_Count_m21921(__this, method) (( int32_t (*) (Collection_1_t3893 *, MethodInfo*))Collection_1_get_Count_m21921_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t1187  Collection_1_get_Item_m21922_gshared (Collection_1_t3893 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m21922(__this, ___index, method) (( RaycastResult_t1187  (*) (Collection_1_t3893 *, int32_t, MethodInfo*))Collection_1_get_Item_m21922_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m21923_gshared (Collection_1_t3893 * __this, int32_t ___index, RaycastResult_t1187  ___value, MethodInfo* method);
#define Collection_1_set_Item_m21923(__this, ___index, ___value, method) (( void (*) (Collection_1_t3893 *, int32_t, RaycastResult_t1187 , MethodInfo*))Collection_1_set_Item_m21923_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m21924_gshared (Collection_1_t3893 * __this, int32_t ___index, RaycastResult_t1187  ___item, MethodInfo* method);
#define Collection_1_SetItem_m21924(__this, ___index, ___item, method) (( void (*) (Collection_1_t3893 *, int32_t, RaycastResult_t1187 , MethodInfo*))Collection_1_SetItem_m21924_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m21925_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m21925(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m21925_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t1187  Collection_1_ConvertItem_m21926_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m21926(__this /* static, unused */, ___item, method) (( RaycastResult_t1187  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m21926_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m21927_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m21927(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m21927_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m21928_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m21928(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m21928_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m21929_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m21929(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m21929_gshared)(__this /* static, unused */, ___list, method)
