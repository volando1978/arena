﻿#pragma once
#include <stdint.h>
// Soomla.Store.UpgradeVG[]
struct UpgradeVGU5BU5D_t3584;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct  List_1_t178  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::_items
	UpgradeVGU5BU5D_t3584* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::_version
	int32_t ____version_3;
};
struct List_1_t178_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::EmptyArray
	UpgradeVGU5BU5D_t3584* ___EmptyArray_4;
};
