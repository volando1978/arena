﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
#ifndef _MSC_VER
#else
#endif
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"


// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C" void ExtensionAttribute__ctor_m1071 (ExtensionAttribute_t242 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
#ifndef _MSC_VER
#else
#endif
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"



// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m7679 (MonoTODOAttribute_t1801 * __this, MethodInfo* method)
{
	{
		Attribute__ctor_m7499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
#ifndef _MSC_VER
#else
#endif
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"


// System.Void System.Linq.Check::Source(System.Object)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern "C" void Check_Source_m7680 (Object_t * __this /* static, unused */, Object_t * ___source, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral1032, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndSelector(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern "C" void Check_SourceAndSelector_m7681 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___selector, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral1032, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___selector;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t908 * L_3 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_3, (String_t*) &_stringLiteral1033, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern "C" void Check_SourceAndPredicate_m7682 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___predicate, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral1032, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___predicate;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t908 * L_3 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_3, (String_t*) &_stringLiteral1034, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Linq.Check::FirstAndSecond(System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern "C" void Check_FirstAndSecond_m7683 (Object_t * __this /* static, unused */, Object_t * ___first, Object_t * ___second, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___first;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral1035, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___second;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t908 * L_3 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_3, (String_t*) &_stringLiteral1036, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndKeyElementSelectors(System.Object,System.Object,System.Object)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern "C" void Check_SourceAndKeyElementSelectors_m7684 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___keySelector, Object_t * ___elementSelector, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___source;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t908 * L_1 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_1, (String_t*) &_stringLiteral1032, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___keySelector;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t908 * L_3 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_3, (String_t*) &_stringLiteral1037, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0022:
	{
		Object_t * L_4 = ___elementSelector;
		if (L_4)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentNullException_t908 * L_5 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_5, (String_t*) &_stringLiteral1038, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_Fallback.h"
#ifndef _MSC_VER
#else
#endif
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_FallbackMethodDeclarations.h"



// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
#ifndef _MSC_VER
#else
#endif
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"



// System.Action
#include "System_Core_System_Action.h"
#ifndef _MSC_VER
#else
#endif
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" void Action__ctor_m3796 (Action_t588 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action::Invoke()
extern "C" void Action_Invoke_m7685 (Action_t588 * __this, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Action_Invoke_m7685((Action_t588 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_Action_t588(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * Action_BeginInvoke_m7686 (Action_t588 * __this, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern "C" void Action_EndInvoke_m7687 (Action_t588 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void U24ArrayTypeU24136_t1804_marshal(const U24ArrayTypeU24136_t1804& unmarshaled, U24ArrayTypeU24136_t1804_marshaled& marshaled)
{
}
void U24ArrayTypeU24136_t1804_marshal_back(const U24ArrayTypeU24136_t1804_marshaled& marshaled, U24ArrayTypeU24136_t1804& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void U24ArrayTypeU24136_t1804_marshal_cleanup(U24ArrayTypeU24136_t1804_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"



#ifdef __clang__
#pragma clang diagnostic pop
#endif
