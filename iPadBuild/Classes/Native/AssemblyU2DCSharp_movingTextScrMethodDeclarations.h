﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// movingTextScr
struct movingTextScr_t818;

// System.Void movingTextScr::.ctor()
extern "C" void movingTextScr__ctor_m3581 (movingTextScr_t818 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextScr::Update()
extern "C" void movingTextScr_Update_m3582 (movingTextScr_t818 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextScr::OnGUI()
extern "C" void movingTextScr_OnGUI_m3583 (movingTextScr_t818 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
