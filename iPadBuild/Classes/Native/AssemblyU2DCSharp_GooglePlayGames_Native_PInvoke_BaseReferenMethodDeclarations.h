﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct BaseReferenceHolder_t654;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::.ctor(System.IntPtr)
extern "C" void BaseReferenceHolder__ctor_m2624 (BaseReferenceHolder_t654 * __this, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.BaseReferenceHolder::IsDisposed()
extern "C" bool BaseReferenceHolder_IsDisposed_m2625 (BaseReferenceHolder_t654 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.BaseReferenceHolder::SelfPtr()
extern "C" HandleRef_t657  BaseReferenceHolder_SelfPtr_m2626 (BaseReferenceHolder_t654 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::CallDispose(System.Runtime.InteropServices.HandleRef)
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Finalize()
extern "C" void BaseReferenceHolder_Finalize_m2627 (BaseReferenceHolder_t654 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose()
extern "C" void BaseReferenceHolder_Dispose_m2628 (BaseReferenceHolder_t654 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.BaseReferenceHolder::AsPointer()
extern "C" IntPtr_t BaseReferenceHolder_AsPointer_m2629 (BaseReferenceHolder_t654 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose(System.Boolean)
extern "C" void BaseReferenceHolder_Dispose_m2630 (BaseReferenceHolder_t654 * __this, bool ___fromFinalizer, MethodInfo* method) IL2CPP_METHOD_ATTR;
