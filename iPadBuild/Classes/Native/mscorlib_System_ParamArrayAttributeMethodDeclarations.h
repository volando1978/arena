﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ParamArrayAttribute
struct ParamArrayAttribute_t1439;

// System.Void System.ParamArrayAttribute::.ctor()
extern "C" void ParamArrayAttribute__ctor_m6243 (ParamArrayAttribute_t1439 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
