﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t2387;
// Mono.Math.BigInteger
struct BigInteger_t2386;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m10860 (ModulusRing_t2387 * __this, BigInteger_t2386 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m10861 (ModulusRing_t2387 * __this, BigInteger_t2386 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * ModulusRing_Multiply_m10862 (ModulusRing_t2387 * __this, BigInteger_t2386 * ___a, BigInteger_t2386 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * ModulusRing_Difference_m10863 (ModulusRing_t2387 * __this, BigInteger_t2386 * ___a, BigInteger_t2386 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * ModulusRing_Pow_m10864 (ModulusRing_t2387 * __this, BigInteger_t2386 * ___a, BigInteger_t2386 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t2386 * ModulusRing_Pow_m10865 (ModulusRing_t2387 * __this, uint32_t ___b, BigInteger_t2386 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
