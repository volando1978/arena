﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>
struct List_1_t49;
// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>
struct  Enumerator_t205 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::l
	List_1_t49 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Schedule/DateTimeRange>::current
	DateTimeRange_t47 * ___current_3;
};
