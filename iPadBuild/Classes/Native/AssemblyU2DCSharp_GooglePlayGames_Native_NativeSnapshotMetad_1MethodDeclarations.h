﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t679;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange::.ctor(System.IntPtr)
extern "C" void NativeSnapshotMetadataChange__ctor_m2823 (NativeSnapshotMetadataChange_t679 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSnapshotMetadataChange::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeSnapshotMetadataChange_CallDispose_m2824 (NativeSnapshotMetadataChange_t679 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSnapshotMetadataChange::FromPointer(System.IntPtr)
extern "C" NativeSnapshotMetadataChange_t679 * NativeSnapshotMetadataChange_FromPointer_m2825 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
