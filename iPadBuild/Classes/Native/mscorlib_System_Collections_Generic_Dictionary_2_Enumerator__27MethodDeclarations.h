﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t4139;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t1269;
struct Event_t1269_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1644;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m25264(__this, ___dictionary, method) (( void (*) (Enumerator_t4139 *, Dictionary_2_t1644 *, MethodInfo*))Enumerator__ctor_m17124_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25265(__this, method) (( Object_t * (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25266(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25267(__this, method) (( Object_t * (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25268(__this, method) (( Object_t * (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m25269(__this, method) (( bool (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_MoveNext_m17129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m25270(__this, method) (( KeyValuePair_2_t4136  (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_get_Current_m17130_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m25271(__this, method) (( Event_t1269 * (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_get_CurrentKey_m17131_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m25272(__this, method) (( int32_t (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_get_CurrentValue_m17132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m25273(__this, method) (( void (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_VerifyState_m17133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m25274(__this, method) (( void (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_VerifyCurrent_m17134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m25275(__this, method) (( void (*) (Enumerator_t4139 *, MethodInfo*))Enumerator_Dispose_m17135_gshared)(__this, method)
