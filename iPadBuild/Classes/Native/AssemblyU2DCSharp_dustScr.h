﻿#pragma once
#include <stdint.h>
// UnityEngine.Sprite
struct Sprite_t766;
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t744;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// dustScr
struct  dustScr_t787  : public MonoBehaviour_t26
{
	// UnityEngine.Sprite dustScr::s1
	Sprite_t766 * ___s1_2;
	// UnityEngine.Sprite dustScr::s2
	Sprite_t766 * ___s2_3;
	// UnityEngine.Sprite dustScr::s3
	Sprite_t766 * ___s3_4;
	// UnityEngine.Sprite dustScr::s4
	Sprite_t766 * ___s4_5;
	// UnityEngine.Sprite dustScr::s5
	Sprite_t766 * ___s5_6;
	// UnityEngine.Sprite dustScr::s6
	Sprite_t766 * ___s6_7;
	// UnityEngine.Sprite dustScr::s7
	Sprite_t766 * ___s7_8;
	// UnityEngine.Sprite dustScr::s8
	Sprite_t766 * ___s8_9;
	// UnityEngine.Sprite dustScr::s9
	Sprite_t766 * ___s9_10;
	// UnityEngine.Sprite dustScr::s10
	Sprite_t766 * ___s10_11;
	// UnityEngine.Sprite dustScr::s11
	Sprite_t766 * ___s11_12;
	// System.Collections.ArrayList dustScr::sprites
	ArrayList_t737 * ___sprites_13;
	// UnityEngine.SpriteRenderer dustScr::r
	SpriteRenderer_t744 * ___r_14;
	// System.Single dustScr::lifeTime
	float ___lifeTime_15;
	// System.Single dustScr::offset
	float ___offset_16;
};
