﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Camera>
struct InternalEnumerator_1_t4088;
// System.Object
struct Object_t;
// UnityEngine.Camera
struct Camera_t978;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m24622(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4088 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24623(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4088 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::Dispose()
#define InternalEnumerator_1_Dispose_m24624(__this, method) (( void (*) (InternalEnumerator_1_t4088 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Camera>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24625(__this, method) (( bool (*) (InternalEnumerator_1_t4088 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Camera>::get_Current()
#define InternalEnumerator_1_get_Current_m24626(__this, method) (( Camera_t978 * (*) (InternalEnumerator_1_t4088 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
