﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.CanvasGroup>
struct Action_1_t4024;
// System.Object
struct Object_t;
// UnityEngine.CanvasGroup
struct CanvasGroup_t1387;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.CanvasGroup>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m23740(__this, ___object, ___method, method) (( void (*) (Action_1_t4024 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.CanvasGroup>::Invoke(T)
#define Action_1_Invoke_m23741(__this, ___obj, method) (( void (*) (Action_1_t4024 *, CanvasGroup_t1387 *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.CanvasGroup>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m23742(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t4024 *, CanvasGroup_t1387 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.CanvasGroup>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m23743(__this, ___result, method) (( void (*) (Action_1_t4024 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
