﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<GooglePlayGames.BasicApi.Achievement>
struct  Comparison_1_t3639  : public MulticastDelegate_t22
{
};
