﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t4195;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m25787_gshared (DefaultComparer_t4195 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m25787(__this, method) (( void (*) (DefaultComparer_t4195 *, MethodInfo*))DefaultComparer__ctor_m25787_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m25788_gshared (DefaultComparer_t4195 * __this, int32_t ___x, int32_t ___y, MethodInfo* method);
#define DefaultComparer_Compare_m25788(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4195 *, int32_t, int32_t, MethodInfo*))DefaultComparer_Compare_m25788_gshared)(__this, ___x, ___y, method)
