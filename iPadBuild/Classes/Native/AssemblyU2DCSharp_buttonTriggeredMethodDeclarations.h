﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// buttonTriggered
struct buttonTriggered_t769;

// System.Void buttonTriggered::.ctor()
extern "C" void buttonTriggered__ctor_m3328 (buttonTriggered_t769 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
