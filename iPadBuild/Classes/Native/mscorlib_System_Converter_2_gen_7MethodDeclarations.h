﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.String>,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct Converter_2_t3402;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.String>,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
// System.Converter`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Converter_2_gen_4MethodDeclarations.h"
#define Converter_2__ctor_m15796(__this, ___object, ___method, method) (( void (*) (Converter_2_t3402 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15610_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.String>,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::Invoke(TInput)
#define Converter_2_Invoke_m15797(__this, ___input, method) (( KeyValuePair_2_t196  (*) (Converter_2_t3402 *, UnityKeyValuePair_2_t164 *, MethodInfo*))Converter_2_Invoke_m15612_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.String>,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
#define Converter_2_BeginInvoke_m15798(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3402 *, UnityKeyValuePair_2_t164 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15614_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.String,System.String>,System.Collections.Generic.KeyValuePair`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Converter_2_EndInvoke_m15799(__this, ___result, method) (( KeyValuePair_2_t196  (*) (Converter_2_t3402 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15616_gshared)(__this, ___result, method)
