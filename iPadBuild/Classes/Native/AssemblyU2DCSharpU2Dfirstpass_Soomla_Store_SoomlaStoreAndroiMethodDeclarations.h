﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.SoomlaStoreAndroid
struct SoomlaStoreAndroid_t62;

// System.Void Soomla.Store.SoomlaStoreAndroid::.ctor()
extern "C" void SoomlaStoreAndroid__ctor_m254 (SoomlaStoreAndroid_t62 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
