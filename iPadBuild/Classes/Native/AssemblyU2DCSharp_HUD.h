﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Texture
struct Texture_t736;
// System.Collections.ArrayList
struct ArrayList_t737;
// System.String[]
struct StringU5BU5D_t169;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// HUD
struct  HUD_t733  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle HUD::bulletsSt
	GUIStyle_t724 * ___bulletsSt_2;
	// UnityEngine.GUIStyle HUD::killsSt
	GUIStyle_t724 * ___killsSt_3;
	// UnityEngine.GUIStyle HUD::coinsSt
	GUIStyle_t724 * ___coinsSt_4;
	// UnityEngine.GUIStyle HUD::bestkillsSt
	GUIStyle_t724 * ___bestkillsSt_5;
	// UnityEngine.GUIStyle HUD::currentWeaponSt
	GUIStyle_t724 * ___currentWeaponSt_6;
	// UnityEngine.GUIStyle HUD::readySt
	GUIStyle_t724 * ___readySt_7;
	// UnityEngine.GUIStyle HUD::keyHelpSt
	GUIStyle_t724 * ___keyHelpSt_8;
	// UnityEngine.GUIStyle HUD::inGameSt
	GUIStyle_t724 * ___inGameSt_9;
	// UnityEngine.GUIStyle HUD::buttonSt
	GUIStyle_t724 * ___buttonSt_10;
	// UnityEngine.Texture HUD::blackTexture
	Texture_t736 * ___blackTexture_11;
	// System.Int32 HUD::left
	int32_t ___left_12;
	// System.Int32 HUD::top
	int32_t ___top_13;
	// UnityEngine.Rect HUD::messageRect
	Rect_t738  ___messageRect_17;
	// System.Single HUD::offsetLetterShadow
	float ___offsetLetterShadow_21;
	// System.Int32 HUD::shadowValue
	int32_t ___shadowValue_22;
	// UnityEngine.Vector2 HUD::startPos
	Vector2_t739  ___startPos_23;
	// UnityEngine.Vector2 HUD::endPos
	Vector2_t739  ___endPos_24;
	// System.Boolean HUD::screenPos
	bool ___screenPos_25;
	// System.Collections.ArrayList HUD::iconGfx
	ArrayList_t737 * ___iconGfx_26;
	// UnityEngine.Texture HUD::iconWBlock
	Texture_t736 * ___iconWBlock_27;
	// UnityEngine.Texture HUD::iconLaser
	Texture_t736 * ___iconLaser_28;
	// UnityEngine.Texture HUD::iconTWay
	Texture_t736 * ___iconTWay_29;
	// UnityEngine.Texture HUD::iconCirculo
	Texture_t736 * ___iconCirculo_30;
	// UnityEngine.Texture HUD::iconMoire
	Texture_t736 * ___iconMoire_31;
	// System.Collections.ArrayList HUD::keyGfx
	ArrayList_t737 * ___keyGfx_32;
	// UnityEngine.Texture HUD::iconCoin
	Texture_t736 * ___iconCoin_33;
	// UnityEngine.Texture HUD::iconAmmo
	Texture_t736 * ___iconAmmo_34;
	// UnityEngine.Texture HUD::iconRayos
	Texture_t736 * ___iconRayos_35;
	// UnityEngine.Texture HUD::iconBomb
	Texture_t736 * ___iconBomb_36;
	// UnityEngine.Texture HUD::iconRepetea
	Texture_t736 * ___iconRepetea_37;
	// UnityEngine.Texture HUD::dedo
	Texture_t736 * ___dedo_38;
	// UnityEngine.Texture HUD::flechas
	Texture_t736 * ___flechas_39;
	// System.String[] HUD::helpText
	StringU5BU5D_t169* ___helpText_40;
	// System.Single HUD::v
	float ___v_41;
	// System.Single HUD::xU
	float ___xU_42;
	// System.Single HUD::yU
	float ___yU_43;
	// System.Int32 HUD::t
	int32_t ___t_44;
	// System.String HUD::coinsText
	String_t* ___coinsText_45;
	// System.Int32 HUD::coinsDisplay
	int32_t ___coinsDisplay_46;
	// System.Int32 HUD::oleadaT
	int32_t ___oleadaT_47;
};
struct HUD_t733_StaticFields{
	// UnityEngine.Rect HUD::bulletsHUD
	Rect_t738  ___bulletsHUD_14;
	// UnityEngine.Rect HUD::killsHUD
	Rect_t738  ___killsHUD_15;
	// UnityEngine.Rect HUD::bestKillsHUD
	Rect_t738  ___bestKillsHUD_16;
	// UnityEngine.Vector2 HUD::bulletsHUDPos
	Vector2_t739  ___bulletsHUDPos_18;
	// UnityEngine.Vector2 HUD::killsHUDPos
	Vector2_t739  ___killsHUDPos_19;
	// System.Int32 HUD::counter
	int32_t ___counter_20;
};
