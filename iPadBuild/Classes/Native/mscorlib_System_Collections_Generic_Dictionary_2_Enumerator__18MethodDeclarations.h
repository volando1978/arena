﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
struct Enumerator_t3794;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t3789;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20564_gshared (Enumerator_t3794 * __this, Dictionary_2_t3789 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m20564(__this, ___dictionary, method) (( void (*) (Enumerator_t3794 *, Dictionary_2_t3789 *, MethodInfo*))Enumerator__ctor_m20564_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20565_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20565(__this, method) (( Object_t * (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20565_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20566_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567(__this, method) (( Object_t * (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568(__this, method) (( Object_t * (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20569_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m20569(__this, method) (( bool (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_MoveNext_m20569_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t3790  Enumerator_get_Current_m20570_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_get_Current_m20570(__this, method) (( KeyValuePair_2_t3790  (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_get_Current_m20570_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m20571_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m20571(__this, method) (( int32_t (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_get_CurrentKey_m20571_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m20572_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m20572(__this, method) (( int32_t (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_get_CurrentValue_m20572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m20573_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m20573(__this, method) (( void (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_VerifyState_m20573_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20574_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m20574(__this, method) (( void (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_VerifyCurrent_m20574_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m20575_gshared (Enumerator_t3794 * __this, MethodInfo* method);
#define Enumerator_Dispose_m20575(__this, method) (( void (*) (Enumerator_t3794 *, MethodInfo*))Enumerator_Dispose_m20575_gshared)(__this, method)
