﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t1296;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct  Comparison_1_t4033  : public MulticastDelegate_t22
{
};
