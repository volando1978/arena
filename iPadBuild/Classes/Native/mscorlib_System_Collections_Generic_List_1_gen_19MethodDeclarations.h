﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct List_1_t891;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IEnumerable_1_t874;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IEnumerator_1_t925;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct ICollection_1_t4385;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct ReadOnlyCollection_1_t3750;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
struct MultiplayerParticipantU5BU5D_t3739;
// System.Predicate`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Predicate_1_t3751;
// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Action_1_t3752;
// System.Comparison`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Comparison_1_t3754;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3812(__this, method) (( void (*) (List_1_t891 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20124(__this, ___collection, method) (( void (*) (List_1_t891 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Int32)
#define List_1__ctor_m20125(__this, ___capacity, method) (( void (*) (List_1_t891 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.cctor()
#define List_1__cctor_m20126(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20127(__this, method) (( Object_t* (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20128(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t891 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20129(__this, method) (( Object_t * (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20130(__this, ___item, method) (( int32_t (*) (List_1_t891 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20131(__this, ___item, method) (( bool (*) (List_1_t891 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20132(__this, ___item, method) (( int32_t (*) (List_1_t891 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20133(__this, ___index, ___item, method) (( void (*) (List_1_t891 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20134(__this, ___item, method) (( void (*) (List_1_t891 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20135(__this, method) (( bool (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20136(__this, method) (( bool (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20137(__this, method) (( Object_t * (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20138(__this, method) (( bool (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20139(__this, method) (( bool (*) (List_1_t891 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20140(__this, ___index, method) (( Object_t * (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20141(__this, ___index, ___value, method) (( void (*) (List_1_t891 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Add(T)
#define List_1_Add_m20142(__this, ___item, method) (( void (*) (List_1_t891 *, MultiplayerParticipant_t672 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20143(__this, ___newCount, method) (( void (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20144(__this, ___collection, method) (( void (*) (List_1_t891 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20145(__this, ___enumerable, method) (( void (*) (List_1_t891 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20146(__this, ___collection, method) (( void (*) (List_1_t891 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::AsReadOnly()
#define List_1_AsReadOnly_m20147(__this, method) (( ReadOnlyCollection_1_t3750 * (*) (List_1_t891 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Clear()
#define List_1_Clear_m20148(__this, method) (( void (*) (List_1_t891 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Contains(T)
#define List_1_Contains_m20149(__this, ___item, method) (( bool (*) (List_1_t891 *, MultiplayerParticipant_t672 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20150(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t891 *, MultiplayerParticipantU5BU5D_t3739*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Find(System.Predicate`1<T>)
#define List_1_Find_m20151(__this, ___match, method) (( MultiplayerParticipant_t672 * (*) (List_1_t891 *, Predicate_1_t3751 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20152(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3751 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m20153(__this, ___match, method) (( int32_t (*) (List_1_t891 *, Predicate_1_t3751 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20154(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t891 *, int32_t, int32_t, Predicate_1_t3751 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m20155(__this, ___action, method) (( void (*) (List_1_t891 *, Action_1_t3752 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::GetEnumerator()
#define List_1_GetEnumerator_m20156(__this, method) (( Enumerator_t3753  (*) (List_1_t891 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::IndexOf(T)
#define List_1_IndexOf_m20157(__this, ___item, method) (( int32_t (*) (List_1_t891 *, MultiplayerParticipant_t672 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20158(__this, ___start, ___delta, method) (( void (*) (List_1_t891 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20159(__this, ___index, method) (( void (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Insert(System.Int32,T)
#define List_1_Insert_m20160(__this, ___index, ___item, method) (( void (*) (List_1_t891 *, int32_t, MultiplayerParticipant_t672 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20161(__this, ___collection, method) (( void (*) (List_1_t891 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Remove(T)
#define List_1_Remove_m20162(__this, ___item, method) (( bool (*) (List_1_t891 *, MultiplayerParticipant_t672 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20163(__this, ___match, method) (( int32_t (*) (List_1_t891 *, Predicate_1_t3751 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20164(__this, ___index, method) (( void (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Reverse()
#define List_1_Reverse_m20165(__this, method) (( void (*) (List_1_t891 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Sort()
#define List_1_Sort_m20166(__this, method) (( void (*) (List_1_t891 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20167(__this, ___comparison, method) (( void (*) (List_1_t891 *, Comparison_1_t3754 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ToArray()
#define List_1_ToArray_m20168(__this, method) (( MultiplayerParticipantU5BU5D_t3739* (*) (List_1_t891 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::TrimExcess()
#define List_1_TrimExcess_m20169(__this, method) (( void (*) (List_1_t891 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Capacity()
#define List_1_get_Capacity_m20170(__this, method) (( int32_t (*) (List_1_t891 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20171(__this, ___value, method) (( void (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Count()
#define List_1_get_Count_m20172(__this, method) (( int32_t (*) (List_1_t891 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Item(System.Int32)
#define List_1_get_Item_m20173(__this, ___index, method) (( MultiplayerParticipant_t672 * (*) (List_1_t891 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::set_Item(System.Int32,T)
#define List_1_set_Item_m20174(__this, ___index, ___value, method) (( void (*) (List_1_t891 *, int32_t, MultiplayerParticipant_t672 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
