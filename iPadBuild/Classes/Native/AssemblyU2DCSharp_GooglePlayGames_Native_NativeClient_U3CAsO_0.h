﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Byte>
struct Action_1_t910;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Byte>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Byte>::callback
	Action_1_t910 * ___callback_0;
};
