﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t4130;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25170_gshared (InternalEnumerator_1_t4130 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25170(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4130 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25170_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171_gshared (InternalEnumerator_1_t4130 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4130 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25171_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25172_gshared (InternalEnumerator_1_t4130 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25172(__this, method) (( void (*) (InternalEnumerator_1_t4130 *, MethodInfo*))InternalEnumerator_1_Dispose_m25172_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25173_gshared (InternalEnumerator_1_t4130 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25173(__this, method) (( bool (*) (InternalEnumerator_1_t4130 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25173_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t1629  InternalEnumerator_1_get_Current_m25174_gshared (InternalEnumerator_1_t4130 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25174(__this, method) (( HitInfo_t1629  (*) (InternalEnumerator_1_t4130 *, MethodInfo*))InternalEnumerator_1_get_Current_m25174_gshared)(__this, method)
