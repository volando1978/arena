﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIContent
struct GUIContent_t986;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t736;

// System.Void UnityEngine.GUIContent::.ctor()
extern "C" void GUIContent__ctor_m6638 (GUIContent_t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern "C" void GUIContent__ctor_m4069 (GUIContent_t986 * __this, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.Texture)
extern "C" void GUIContent__ctor_m4092 (GUIContent_t986 * __this, Texture_t736 * ___image, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.cctor()
extern "C" void GUIContent__cctor_m6639 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m5982 (GUIContent_t986 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m6640 (GUIContent_t986 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern "C" GUIContent_t986 * GUIContent_Temp_m6641 (Object_t * __this /* static, unused */, String_t* ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(UnityEngine.Texture)
extern "C" GUIContent_t986 * GUIContent_Temp_m6642 (Object_t * __this /* static, unused */, Texture_t736 * ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern "C" void GUIContent_ClearStaticCache_m6643 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
