﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ADInterstitial
struct ADInterstitial_t332;

// System.Void ADInterstitial::.ctor()
extern "C" void ADInterstitial__ctor_m1322 (ADInterstitial_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADInterstitial::.cctor()
extern "C" void ADInterstitial__cctor_m1323 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADInterstitial::Start()
extern "C" void ADInterstitial_Start_m1324 (ADInterstitial_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADInterstitial::OnFullscreenLoaded()
extern "C" void ADInterstitial_OnFullscreenLoaded_m1325 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADInterstitial::Update()
extern "C" void ADInterstitial_Update_m1326 (ADInterstitial_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
