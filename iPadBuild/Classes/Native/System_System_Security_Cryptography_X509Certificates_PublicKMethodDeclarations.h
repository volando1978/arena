﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t2016;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t2014;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t2013;
// System.Security.Cryptography.Oid
struct Oid_t2015;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t2024;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Security.Cryptography.DSA
struct DSA_t2129;
// System.Security.Cryptography.RSA
struct RSA_t2130;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m8077 (PublicKey_t2016 * __this, X509Certificate_t2024 * ___certificate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t2014 * PublicKey_get_EncodedKeyValue_m8078 (PublicKey_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t2014 * PublicKey_get_EncodedParameters_m8079 (PublicKey_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t2013 * PublicKey_get_Key_m8080 (PublicKey_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t2015 * PublicKey_get_Oid_m8081 (PublicKey_t2016 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t350* PublicKey_GetUnsignedBigInteger_m8082 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___integer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t2129 * PublicKey_DecodeDSA_m8083 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___rawPublicKey, ByteU5BU5D_t350* ___rawParameters, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t2130 * PublicKey_DecodeRSA_m8084 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___rawPublicKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
