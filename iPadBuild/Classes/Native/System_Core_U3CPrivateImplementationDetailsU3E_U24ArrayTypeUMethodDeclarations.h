﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct U24ArrayTypeU24136_t1804;
struct U24ArrayTypeU24136_t1804_marshaled;

void U24ArrayTypeU24136_t1804_marshal(const U24ArrayTypeU24136_t1804& unmarshaled, U24ArrayTypeU24136_t1804_marshaled& marshaled);
void U24ArrayTypeU24136_t1804_marshal_back(const U24ArrayTypeU24136_t1804_marshaled& marshaled, U24ArrayTypeU24136_t1804& unmarshaled);
void U24ArrayTypeU24136_t1804_marshal_cleanup(U24ArrayTypeU24136_t1804_marshaled& marshaled);
