﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesScore
struct PlayGamesScore_t389;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void GooglePlayGames.PlayGamesScore::.ctor()
extern "C" void PlayGamesScore__ctor_m1569 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesScore::ReportScore(System.Action`1<System.Boolean>)
extern "C" void PlayGamesScore_ReportScore_m1570 (PlayGamesScore_t389 * __this, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesScore::get_leaderboardID()
extern "C" String_t* PlayGamesScore_get_leaderboardID_m1571 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesScore::set_leaderboardID(System.String)
extern "C" void PlayGamesScore_set_leaderboardID_m1572 (PlayGamesScore_t389 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.PlayGamesScore::get_value()
extern "C" int64_t PlayGamesScore_get_value_m1573 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesScore::set_value(System.Int64)
extern "C" void PlayGamesScore_set_value_m1574 (PlayGamesScore_t389 * __this, int64_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.PlayGamesScore::get_date()
extern "C" DateTime_t48  PlayGamesScore_get_date_m1575 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesScore::get_formattedValue()
extern "C" String_t* PlayGamesScore_get_formattedValue_m1576 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesScore::get_userID()
extern "C" String_t* PlayGamesScore_get_userID_m1577 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.PlayGamesScore::get_rank()
extern "C" int32_t PlayGamesScore_get_rank_m1578 (PlayGamesScore_t389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
