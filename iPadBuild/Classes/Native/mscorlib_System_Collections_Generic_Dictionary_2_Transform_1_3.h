﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,JSONObject,System.Collections.DictionaryEntry>
struct  Transform_1_t3503  : public MulticastDelegate_t22
{
};
