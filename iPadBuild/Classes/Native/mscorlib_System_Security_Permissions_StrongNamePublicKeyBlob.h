﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Permissions.StrongNamePublicKeyBlob
struct  StrongNamePublicKeyBlob_t2722  : public Object_t
{
	// System.Byte[] System.Security.Permissions.StrongNamePublicKeyBlob::pubkey
	ByteU5BU5D_t350* ___pubkey_0;
};
