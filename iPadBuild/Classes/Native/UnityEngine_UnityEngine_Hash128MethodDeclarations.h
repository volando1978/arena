﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Hash128
struct Hash128_t1549;
// System.String
struct String_t;

// System.Void UnityEngine.Hash128::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern "C" void Hash128__ctor_m6892 (Hash128_t1549 * __this, uint32_t ___u32_0, uint32_t ___u32_1, uint32_t ___u32_2, uint32_t ___u32_3, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Hash128::ToString()
extern "C" String_t* Hash128_ToString_m6893 (Hash128_t1549 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
