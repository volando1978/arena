﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B
struct U3CCreateQuickGameU3Ec__AnonStorey2B_t593;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B::.ctor()
extern "C" void U3CCreateQuickGameU3Ec__AnonStorey2B__ctor_m2449 (U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
