﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t1618;
struct GcScoreData_t1618_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1627;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern "C" Score_t1627 * GcScoreData_ToScore_m7316 (GcScoreData_t1618 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t1618_marshal(const GcScoreData_t1618& unmarshaled, GcScoreData_t1618_marshaled& marshaled);
void GcScoreData_t1618_marshal_back(const GcScoreData_t1618_marshaled& marshaled, GcScoreData_t1618& unmarshaled);
void GcScoreData_t1618_marshal_cleanup(GcScoreData_t1618_marshaled& marshaled);
