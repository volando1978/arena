﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23
struct  U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533  : public Object_t
{
	// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23::invitationDelegate
	InvitationReceivedDelegate_t363 * ___invitationDelegate_0;
};
