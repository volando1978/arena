﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// enemyController
struct  enemyController_t785  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject enemyController::hole
	GameObject_t144 * ___hole_2;
	// UnityEngine.GameObject enemyController::snake
	GameObject_t144 * ___snake_3;
	// UnityEngine.GameObject enemyController::spider
	GameObject_t144 * ___spider_4;
	// UnityEngine.GameObject enemyController::explosion
	GameObject_t144 * ___explosion_5;
	// System.Collections.ArrayList enemyController::posHoles
	ArrayList_t737 * ___posHoles_6;
	// UnityEngine.Vector2 enemyController::tutorialPos
	Vector2_t739  ___tutorialPos_8;
	// UnityEngine.GameObject enemyController::agujeroTutorialObj
	GameObject_t144 * ___agujeroTutorialObj_9;
	// UnityEngine.GameObject enemyController::agujerosPadre
	GameObject_t144 * ___agujerosPadre_10;
	// UnityEngine.GameObject enemyController::currentAgujerosPadre
	GameObject_t144 * ___currentAgujerosPadre_11;
	// System.Int32 enemyController::maxNumSnakes
	int32_t ___maxNumSnakes_12;
};
struct enemyController_t785_StaticFields{
	// System.Collections.ArrayList enemyController::enemies
	ArrayList_t737 * ___enemies_7;
};
