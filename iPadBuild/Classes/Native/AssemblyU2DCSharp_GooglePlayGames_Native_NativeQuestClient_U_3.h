﻿#pragma once
#include <stdint.h>
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct Action_3_t558;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A
struct  U3CClaimMilestoneU3Ec__AnonStorey2A_t559  : public Object_t
{
	// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone> GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A::callback
	Action_3_t558 * ___callback_0;
};
