﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.NotEnoughGoodsException
struct NotEnoughGoodsException_t136;
// System.String
struct String_t;

// System.Void Soomla.Store.NotEnoughGoodsException::.ctor()
extern "C" void NotEnoughGoodsException__ctor_m667 (NotEnoughGoodsException_t136 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.NotEnoughGoodsException::.ctor(System.String)
extern "C" void NotEnoughGoodsException__ctor_m668 (NotEnoughGoodsException_t136 * __this, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
