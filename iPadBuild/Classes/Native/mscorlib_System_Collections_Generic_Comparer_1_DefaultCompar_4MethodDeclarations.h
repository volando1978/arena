﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t3899;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m21950_gshared (DefaultComparer_t3899 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21950(__this, method) (( void (*) (DefaultComparer_t3899 *, MethodInfo*))DefaultComparer__ctor_m21950_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m21951_gshared (DefaultComparer_t3899 * __this, RaycastResult_t1187  ___x, RaycastResult_t1187  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m21951(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3899 *, RaycastResult_t1187 , RaycastResult_t1187 , MethodInfo*))DefaultComparer_Compare_m21951_gshared)(__this, ___x, ___y, method)
