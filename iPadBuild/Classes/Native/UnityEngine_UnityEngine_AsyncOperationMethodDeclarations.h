﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t1486;
struct AsyncOperation_t1486_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m6961 (AsyncOperation_t1486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m6962 (AsyncOperation_t1486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m6963 (AsyncOperation_t1486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t1486_marshal(const AsyncOperation_t1486& unmarshaled, AsyncOperation_t1486_marshaled& marshaled);
void AsyncOperation_t1486_marshal_back(const AsyncOperation_t1486_marshaled& marshaled, AsyncOperation_t1486& unmarshaled);
void AsyncOperation_t1486_marshal_cleanup(AsyncOperation_t1486_marshaled& marshaled);
