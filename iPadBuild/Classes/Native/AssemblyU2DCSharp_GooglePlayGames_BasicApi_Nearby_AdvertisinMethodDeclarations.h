﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
struct AdvertisingResult_t354;
struct AdvertisingResult_t354_marshaled;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"

// System.Void GooglePlayGames.BasicApi.Nearby.AdvertisingResult::.ctor(GooglePlayGames.BasicApi.ResponseStatus,System.String)
extern "C" void AdvertisingResult__ctor_m1420 (AdvertisingResult_t354 * __this, int32_t ___status, String_t* ___localEndpointName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Succeeded()
extern "C" bool AdvertisingResult_get_Succeeded_m1421 (AdvertisingResult_t354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_Status()
extern "C" int32_t AdvertisingResult_get_Status_m1422 (AdvertisingResult_t354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.AdvertisingResult::get_LocalEndpointName()
extern "C" String_t* AdvertisingResult_get_LocalEndpointName_m1423 (AdvertisingResult_t354 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AdvertisingResult_t354_marshal(const AdvertisingResult_t354& unmarshaled, AdvertisingResult_t354_marshaled& marshaled);
void AdvertisingResult_t354_marshal_back(const AdvertisingResult_t354_marshaled& marshaled, AdvertisingResult_t354& unmarshaled);
void AdvertisingResult_t354_marshal_cleanup(AdvertisingResult_t354_marshaled& marshaled);
