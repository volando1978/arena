﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>
struct List_1_t115;
// System.Object
struct Object_t;
// Soomla.Store.VirtualCurrencyPack
struct VirtualCurrencyPack_t78;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.VirtualCurrencyPack>
struct IEnumerable_1_t224;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualCurrencyPack>
struct IEnumerator_1_t4344;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.VirtualCurrencyPack>
struct ICollection_1_t4345;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualCurrencyPack>
struct ReadOnlyCollection_1_t3623;
// Soomla.Store.VirtualCurrencyPack[]
struct VirtualCurrencyPackU5BU5D_t174;
// System.Predicate`1<Soomla.Store.VirtualCurrencyPack>
struct Predicate_1_t3624;
// System.Action`1<Soomla.Store.VirtualCurrencyPack>
struct Action_1_t3625;
// System.Comparison`1<Soomla.Store.VirtualCurrencyPack>
struct Comparison_1_t3626;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualCurrencyPack>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m1010(__this, method) (( void (*) (List_1_t115 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18328(__this, ___collection, method) (( void (*) (List_1_t115 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::.ctor(System.Int32)
#define List_1__ctor_m18329(__this, ___capacity, method) (( void (*) (List_1_t115 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::.cctor()
#define List_1__cctor_m18330(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18331(__this, method) (( Object_t* (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18332(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t115 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18333(__this, method) (( Object_t * (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18334(__this, ___item, method) (( int32_t (*) (List_1_t115 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18335(__this, ___item, method) (( bool (*) (List_1_t115 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18336(__this, ___item, method) (( int32_t (*) (List_1_t115 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18337(__this, ___index, ___item, method) (( void (*) (List_1_t115 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18338(__this, ___item, method) (( void (*) (List_1_t115 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18339(__this, method) (( bool (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18340(__this, method) (( bool (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18341(__this, method) (( Object_t * (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18342(__this, method) (( bool (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18343(__this, method) (( bool (*) (List_1_t115 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18344(__this, ___index, method) (( Object_t * (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18345(__this, ___index, ___value, method) (( void (*) (List_1_t115 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Add(T)
#define List_1_Add_m18346(__this, ___item, method) (( void (*) (List_1_t115 *, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18347(__this, ___newCount, method) (( void (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18348(__this, ___collection, method) (( void (*) (List_1_t115 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18349(__this, ___enumerable, method) (( void (*) (List_1_t115 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18350(__this, ___collection, method) (( void (*) (List_1_t115 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::AsReadOnly()
#define List_1_AsReadOnly_m18351(__this, method) (( ReadOnlyCollection_1_t3623 * (*) (List_1_t115 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Clear()
#define List_1_Clear_m18352(__this, method) (( void (*) (List_1_t115 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Contains(T)
#define List_1_Contains_m18353(__this, ___item, method) (( bool (*) (List_1_t115 *, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18354(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t115 *, VirtualCurrencyPackU5BU5D_t174*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Find(System.Predicate`1<T>)
#define List_1_Find_m18355(__this, ___match, method) (( VirtualCurrencyPack_t78 * (*) (List_1_t115 *, Predicate_1_t3624 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18356(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3624 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m18357(__this, ___match, method) (( int32_t (*) (List_1_t115 *, Predicate_1_t3624 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18358(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t115 *, int32_t, int32_t, Predicate_1_t3624 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m18359(__this, ___action, method) (( void (*) (List_1_t115 *, Action_1_t3625 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::GetEnumerator()
#define List_1_GetEnumerator_m1018(__this, method) (( Enumerator_t227  (*) (List_1_t115 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::IndexOf(T)
#define List_1_IndexOf_m18360(__this, ___item, method) (( int32_t (*) (List_1_t115 *, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18361(__this, ___start, ___delta, method) (( void (*) (List_1_t115 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18362(__this, ___index, method) (( void (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Insert(System.Int32,T)
#define List_1_Insert_m18363(__this, ___index, ___item, method) (( void (*) (List_1_t115 *, int32_t, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18364(__this, ___collection, method) (( void (*) (List_1_t115 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Remove(T)
#define List_1_Remove_m18365(__this, ___item, method) (( bool (*) (List_1_t115 *, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18366(__this, ___match, method) (( int32_t (*) (List_1_t115 *, Predicate_1_t3624 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18367(__this, ___index, method) (( void (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Reverse()
#define List_1_Reverse_m18368(__this, method) (( void (*) (List_1_t115 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Sort()
#define List_1_Sort_m18369(__this, method) (( void (*) (List_1_t115 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18370(__this, ___comparison, method) (( void (*) (List_1_t115 *, Comparison_1_t3626 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::ToArray()
#define List_1_ToArray_m18371(__this, method) (( VirtualCurrencyPackU5BU5D_t174* (*) (List_1_t115 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::TrimExcess()
#define List_1_TrimExcess_m18372(__this, method) (( void (*) (List_1_t115 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::get_Capacity()
#define List_1_get_Capacity_m18373(__this, method) (( int32_t (*) (List_1_t115 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18374(__this, ___value, method) (( void (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::get_Count()
#define List_1_get_Count_m18375(__this, method) (( int32_t (*) (List_1_t115 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::get_Item(System.Int32)
#define List_1_get_Item_m18376(__this, ___index, method) (( VirtualCurrencyPack_t78 * (*) (List_1_t115 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.VirtualCurrencyPack>::set_Item(System.Int32,T)
#define List_1_set_Item_m18377(__this, ___index, ___value, method) (( void (*) (List_1_t115 *, int32_t, VirtualCurrencyPack_t78 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
