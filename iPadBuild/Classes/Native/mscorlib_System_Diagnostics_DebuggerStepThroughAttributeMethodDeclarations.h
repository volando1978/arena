﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t2453;

// System.Void System.Diagnostics.DebuggerStepThroughAttribute::.ctor()
extern "C" void DebuggerStepThroughAttribute__ctor_m11480 (DebuggerStepThroughAttribute_t2453 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
