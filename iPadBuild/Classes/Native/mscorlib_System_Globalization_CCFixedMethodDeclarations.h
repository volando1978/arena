﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.CCFixed
struct CCFixed_t2458;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"

// System.Int32 System.Globalization.CCFixed::FromDateTime(System.DateTime)
extern "C" int32_t CCFixed_FromDateTime_m11508 (Object_t * __this /* static, unused */, DateTime_t48  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.CCFixed::day_of_week(System.Int32)
extern "C" int32_t CCFixed_day_of_week_m11509 (Object_t * __this /* static, unused */, int32_t ___date, MethodInfo* method) IL2CPP_METHOD_ATTR;
