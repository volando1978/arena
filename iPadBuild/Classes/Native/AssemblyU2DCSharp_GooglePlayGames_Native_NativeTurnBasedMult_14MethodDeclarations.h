﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// GooglePlayGames.Native.PInvoke.TurnBasedManager
struct TurnBasedManager_t651;
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
struct Action_2_t632;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
struct Action_1_t864;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Action_1_t637;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Action_1_t641;
// System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Action_2_t643;
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome
struct MatchOutcome_t345;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::.ctor(GooglePlayGames.Native.NativeClient,GooglePlayGames.Native.PInvoke.TurnBasedManager)
extern "C" void NativeTurnBasedMultiplayerClient__ctor_m2579 (NativeTurnBasedMultiplayerClient_t535 * __this, NativeClient_t524 * ___nativeClient, TurnBasedManager_t651 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::CreateQuickMatch(System.UInt32,System.UInt32,System.UInt32,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_CreateQuickMatch_m2580 (NativeTurnBasedMultiplayerClient_t535 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Action_2_t632 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::CreateWithInvitationScreen(System.UInt32,System.UInt32,System.UInt32,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_CreateWithInvitationScreen_m2581 (NativeTurnBasedMultiplayerClient_t535 * __this, uint32_t ___minOpponents, uint32_t ___maxOpponents, uint32_t ___variant, Action_2_t632 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::BridgeMatchToUserCallback(System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" Action_1_t864 * NativeTurnBasedMultiplayerClient_BridgeMatchToUserCallback_m2582 (NativeTurnBasedMultiplayerClient_t535 * __this, Action_2_t632 * ___userCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcceptFromInbox(System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_AcceptFromInbox_m2583 (NativeTurnBasedMultiplayerClient_t535 * __this, Action_2_t632 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcceptInvitation(System.String,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_AcceptInvitation_m2584 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, Action_2_t632 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindInvitationWithId(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>)
extern "C" void NativeTurnBasedMultiplayerClient_FindInvitationWithId_m2585 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, Action_1_t637 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::RegisterMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate)
extern "C" void NativeTurnBasedMultiplayerClient_RegisterMatchDelegate_m2586 (NativeTurnBasedMultiplayerClient_t535 * __this, MatchDelegate_t364 * ___del, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::HandleMatchEvent(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void NativeTurnBasedMultiplayerClient_HandleMatchEvent_m2587 (NativeTurnBasedMultiplayerClient_t535 * __this, int32_t ___eventType, String_t* ___matchId, NativeTurnBasedMatch_t680 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::TakeTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Byte[],System.String,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_TakeTurn_m2588 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, ByteU5BU5D_t350* ___data, String_t* ___pendingParticipantId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindEqualVersionMatch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>,System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_FindEqualVersionMatch_m2589 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___onFailure, Action_1_t641 * ___onVersionMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::FindEqualVersionMatchWithParticipant(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.String,System.Action`1<System.Boolean>,System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_FindEqualVersionMatchWithParticipant_m2590 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, String_t* ___participantId, Action_1_t98 * ___onFailure, Action_2_t643 * ___onFoundParticipantAndMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::GetMaxMatchDataSize()
extern "C" int32_t NativeTurnBasedMultiplayerClient_GetMaxMatchDataSize_m2591 (NativeTurnBasedMultiplayerClient_t535 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Finish(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Byte[],GooglePlayGames.BasicApi.Multiplayer.MatchOutcome,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_Finish_m2592 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, ByteU5BU5D_t350* ___data, MatchOutcome_t345 * ___outcome, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::ResultToMatchResult(GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult)
extern "C" int32_t NativeTurnBasedMultiplayerClient_ResultToMatchResult_m2593 (Object_t * __this /* static, unused */, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::AcknowledgeFinished(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_AcknowledgeFinished_m2594 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Leave(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_Leave_m2595 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::LeaveDuringTurn(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.String,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_LeaveDuringTurn_m2596 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, String_t* ___pendingParticipantId, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Cancel(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`1<System.Boolean>)
extern "C" void NativeTurnBasedMultiplayerClient_Cancel_m2597 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::Rematch(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>)
extern "C" void NativeTurnBasedMultiplayerClient_Rematch_m2598 (NativeTurnBasedMultiplayerClient_t535 * __this, TurnBasedMatch_t353 * ___match, Action_2_t632 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::DeclineInvitation(System.String)
extern "C" void NativeTurnBasedMultiplayerClient_DeclineInvitation_m2599 (NativeTurnBasedMultiplayerClient_t535 * __this, String_t* ___invitationId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::<DeclineInvitation>m__67(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void NativeTurnBasedMultiplayerClient_U3CDeclineInvitationU3Em__67_m2600 (NativeTurnBasedMultiplayerClient_t535 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
