﻿#pragma once
#include <stdint.h>
// InterludeScr
struct InterludeScr_t741;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// InterludeScr
struct  InterludeScr_t741  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle InterludeScr::marqueeSt
	GUIStyle_t724 * ___marqueeSt_3;
	// UnityEngine.GUIStyle InterludeScr::marqueeBigSt
	GUIStyle_t724 * ___marqueeBigSt_4;
	// UnityEngine.GUIStyle InterludeScr::boxStyle
	GUIStyle_t724 * ___boxStyle_5;
	// System.String InterludeScr::maxText
	String_t* ___maxText_6;
	// System.String InterludeScr::killsText
	String_t* ___killsText_7;
	// System.String InterludeScr::stageTimeText
	String_t* ___stageTimeText_8;
	// System.String InterludeScr::timeText
	String_t* ___timeText_9;
	// System.String InterludeScr::bulletsCollectedText
	String_t* ___bulletsCollectedText_10;
	// UnityEngine.Rect InterludeScr::messageRect
	Rect_t738  ___messageRect_11;
	// UnityEngine.Rect InterludeScr::boxRect
	Rect_t738  ___boxRect_12;
};
struct InterludeScr_t741_StaticFields{
	// InterludeScr InterludeScr::instance
	InterludeScr_t741 * ___instance_2;
};
