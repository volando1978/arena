﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Matrix4x4>
struct List_1_t806;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>
struct  Enumerator_t3832 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::l
	List_1_t806 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>::current
	Matrix4x4_t997  ___current_3;
};
