﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1650;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.PersistentCall>
struct  Comparison_1_t4157  : public MulticastDelegate_t22
{
};
