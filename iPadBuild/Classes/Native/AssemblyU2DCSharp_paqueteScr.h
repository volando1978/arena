﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// paqueteScr
struct  paqueteScr_t828  : public MonoBehaviour_t26
{
	// System.Int32 paqueteScr::wBlockBonus
	int32_t ___wBlockBonus_2;
	// System.Int32 paqueteScr::laserBonus
	int32_t ___laserBonus_3;
	// System.Int32 paqueteScr::TWayBonus
	int32_t ___TWayBonus_4;
	// System.Int32 paqueteScr::circularBonus
	int32_t ___circularBonus_5;
	// System.Int32 paqueteScr::triBonus
	int32_t ___triBonus_6;
	// System.Int32 paqueteScr::moireBonus
	int32_t ___moireBonus_7;
	// System.Int32 paqueteScr::bombaBonus
	int32_t ___bombaBonus_8;
	// System.Int32 paqueteScr::rayoBonus
	int32_t ___rayoBonus_9;
	// System.Single paqueteScr::aumento
	float ___aumento_10;
	// UnityEngine.GameObject paqueteScr::animPaquete
	GameObject_t144 * ___animPaquete_11;
	// System.Int32 paqueteScr::bulletType
	int32_t ___bulletType_12;
	// System.Int32 paqueteScr::lifeTime
	int32_t ___lifeTime_13;
	// System.Boolean paqueteScr::lift
	bool ___lift_14;
};
