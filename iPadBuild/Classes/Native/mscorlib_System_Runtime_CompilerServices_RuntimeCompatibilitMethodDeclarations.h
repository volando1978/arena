﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_t241;

// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
extern "C" void RuntimeCompatibilityAttribute__ctor_m1069 (RuntimeCompatibilityAttribute_t241 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070 (RuntimeCompatibilityAttribute_t241 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
