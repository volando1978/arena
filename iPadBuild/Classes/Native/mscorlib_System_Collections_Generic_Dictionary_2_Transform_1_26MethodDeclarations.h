﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Object>
struct Transform_1_t3796;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m20580_gshared (Transform_1_t3796 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Transform_1__ctor_m20580(__this, ___object, ___method, method) (( void (*) (Transform_1_t3796 *, Object_t *, IntPtr_t, MethodInfo*))Transform_1__ctor_m20580_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m20581_gshared (Transform_1_t3796 * __this, int32_t ___key, int32_t ___value, MethodInfo* method);
#define Transform_1_Invoke_m20581(__this, ___key, ___value, method) (( Object_t * (*) (Transform_1_t3796 *, int32_t, int32_t, MethodInfo*))Transform_1_Invoke_m20581_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m20582_gshared (Transform_1_t3796 * __this, int32_t ___key, int32_t ___value, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Transform_1_BeginInvoke_m20582(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3796 *, int32_t, int32_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Transform_1_BeginInvoke_m20582_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m20583_gshared (Transform_1_t3796 * __this, Object_t * ___result, MethodInfo* method);
#define Transform_1_EndInvoke_m20583(__this, ___result, method) (( Object_t * (*) (Transform_1_t3796 *, Object_t *, MethodInfo*))Transform_1_EndInvoke_m20583_gshared)(__this, ___result, method)
