﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.LeaderboardManager
struct LeaderboardManager_t670;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct Action_1_t660;

// System.Void GooglePlayGames.Native.LeaderboardManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void LeaderboardManager__ctor_m2695 (LeaderboardManager_t670 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.LeaderboardManager::SubmitScore(System.String,System.Int64)
extern "C" void LeaderboardManager_SubmitScore_m2696 (LeaderboardManager_t670 * __this, String_t* ___leaderboardId, int64_t ___score, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.LeaderboardManager::ShowAllUI(System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern "C" void LeaderboardManager_ShowAllUI_m2697 (LeaderboardManager_t670 * __this, Action_1_t660 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.LeaderboardManager::ShowUI(System.String,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern "C" void LeaderboardManager_ShowUI_m2698 (LeaderboardManager_t670 * __this, String_t* ___leaderboardId, Action_1_t660 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
