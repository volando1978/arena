﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.PurchaseWithMarket
struct PurchaseWithMarket_t138;
// System.String
struct String_t;
// Soomla.Store.MarketItem
struct MarketItem_t122;

// System.Void Soomla.Store.PurchaseWithMarket::.ctor(System.String,System.Double)
extern "C" void PurchaseWithMarket__ctor_m672 (PurchaseWithMarket_t138 * __this, String_t* ___productId, double ___price, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchaseWithMarket::.ctor(Soomla.Store.MarketItem)
extern "C" void PurchaseWithMarket__ctor_m673 (PurchaseWithMarket_t138 * __this, MarketItem_t122 * ___marketItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchaseWithMarket::Buy(System.String)
extern "C" void PurchaseWithMarket_Buy_m674 (PurchaseWithMarket_t138 * __this, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.PurchaseWithMarket::CanAfford()
extern "C" bool PurchaseWithMarket_CanAfford_m675 (PurchaseWithMarket_t138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
