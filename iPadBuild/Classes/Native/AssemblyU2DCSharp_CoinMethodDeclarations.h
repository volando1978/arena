﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Coin
struct Coin_t727;
// UnityEngine.Collider
struct Collider_t900;
// Coin/COINTYPE
#include "AssemblyU2DCSharp_Coin_COINTYPE.h"

// System.Void Coin::.ctor()
extern "C" void Coin__ctor_m3149 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Coin::Start()
extern "C" void Coin_Start_m3150 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Coin/COINTYPE Coin::getCoinType()
extern "C" int32_t Coin_getCoinType_m3151 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Coin::Update()
extern "C" void Coin_Update_m3152 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Coin::OnTriggerEnter(UnityEngine.Collider)
extern "C" void Coin_OnTriggerEnter_m3153 (Coin_t727 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Coin::animOffset()
extern "C" void Coin_animOffset_m3154 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Coin::triggerAnim()
extern "C" void Coin_triggerAnim_m3155 (Coin_t727 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
