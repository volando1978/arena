﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t4182;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct  EqualityComparer_1_t4182  : public Object_t
{
};
struct EqualityComparer_1_t4182_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::_default
	EqualityComparer_1_t4182 * ____default_0;
};
