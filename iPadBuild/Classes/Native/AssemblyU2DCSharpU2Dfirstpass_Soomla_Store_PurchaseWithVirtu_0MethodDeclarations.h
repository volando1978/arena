﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.PurchaseWithVirtualItem
struct PurchaseWithVirtualItem_t139;
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.PurchaseWithVirtualItem::.ctor(System.String,System.Int32)
extern "C" void PurchaseWithVirtualItem__ctor_m678 (PurchaseWithVirtualItem_t139 * __this, String_t* ___targetItemId, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.PurchaseWithVirtualItem::Buy(System.String)
extern "C" void PurchaseWithVirtualItem_Buy_m679 (PurchaseWithVirtualItem_t139 * __this, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.PurchaseWithVirtualItem::CanAfford()
extern "C" bool PurchaseWithVirtualItem_CanAfford_m680 (PurchaseWithVirtualItem_t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualItem Soomla.Store.PurchaseWithVirtualItem::getTargetVirtualItem()
extern "C" VirtualItem_t125 * PurchaseWithVirtualItem_getTargetVirtualItem_m681 (PurchaseWithVirtualItem_t139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.PurchaseWithVirtualItem::checkTargetBalance(Soomla.Store.VirtualItem)
extern "C" bool PurchaseWithVirtualItem_checkTargetBalance_m682 (PurchaseWithVirtualItem_t139 * __this, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
