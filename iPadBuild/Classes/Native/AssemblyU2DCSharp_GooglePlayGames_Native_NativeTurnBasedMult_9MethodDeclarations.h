﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57
struct U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::.ctor()
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57__ctor_m2564 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::<>m__61(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__61_m2565 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcknowledgeFinished>c__AnonStorey57::<>m__6A(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CAcknowledgeFinishedU3Ec__AnonStorey57_U3CU3Em__6A_m2566 (U3CAcknowledgeFinishedU3Ec__AnonStorey57_t646 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
