﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t155;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.ShowOptions
struct  ShowOptions_t153  : public Object_t
{
	// System.Boolean UnityEngine.Advertisements.ShowOptions::<pause>k__BackingField
	bool ___U3CpauseU3Ek__BackingField_0;
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::<resultCallback>k__BackingField
	Action_1_t155 * ___U3CresultCallbackU3Ek__BackingField_1;
	// System.String UnityEngine.Advertisements.ShowOptions::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_2;
};
