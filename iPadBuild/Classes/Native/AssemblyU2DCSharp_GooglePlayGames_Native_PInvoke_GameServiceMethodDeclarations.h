﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// GooglePlayGames.Native.PInvoke.AchievementManager
struct AchievementManager_t656;
// GooglePlayGames.Native.LeaderboardManager
struct LeaderboardManager_t670;
// GooglePlayGames.Native.PlayerManager
struct PlayerManager_t685;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.GameServices::.ctor(System.IntPtr)
extern "C" void GameServices__ctor_m2661 (GameServices_t534 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.GameServices::IsAuthenticated()
extern "C" bool GameServices_IsAuthenticated_m2662 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServices::SignOut()
extern "C" void GameServices_SignOut_m2663 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServices::StartAuthorizationUI()
extern "C" void GameServices_StartAuthorizationUI_m2664 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.AchievementManager GooglePlayGames.Native.PInvoke.GameServices::AchievementManager()
extern "C" AchievementManager_t656 * GameServices_AchievementManager_m2665 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.LeaderboardManager GooglePlayGames.Native.PInvoke.GameServices::LeaderboardManager()
extern "C" LeaderboardManager_t670 * GameServices_LeaderboardManager_m2666 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PlayerManager GooglePlayGames.Native.PInvoke.GameServices::PlayerManager()
extern "C" PlayerManager_t685 * GameServices_PlayerManager_m2667 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.HandleRef GooglePlayGames.Native.PInvoke.GameServices::AsHandle()
extern "C" HandleRef_t657  GameServices_AsHandle_m2668 (GameServices_t534 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServices::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void GameServices_CallDispose_m2669 (GameServices_t534 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
