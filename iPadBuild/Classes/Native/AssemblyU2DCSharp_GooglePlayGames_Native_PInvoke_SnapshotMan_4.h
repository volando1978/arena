﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct  SnapshotManager_t610  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.SnapshotManager::mServices
	GameServices_t534 * ___mServices_0;
};
