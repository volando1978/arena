﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// System.Object
struct Object_t;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.UpgradeVG>
struct IEnumerable_1_t220;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.UpgradeVG>
struct IEnumerator_1_t4319;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.UpgradeVG>
struct ICollection_1_t4320;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.UpgradeVG>
struct ReadOnlyCollection_1_t3586;
// Soomla.Store.UpgradeVG[]
struct UpgradeVGU5BU5D_t3584;
// System.Predicate`1<Soomla.Store.UpgradeVG>
struct Predicate_1_t3587;
// System.Action`1<Soomla.Store.UpgradeVG>
struct Action_1_t3588;
// System.Comparison`1<Soomla.Store.UpgradeVG>
struct Comparison_1_t3589;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m1021(__this, method) (( void (*) (List_1_t178 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17660(__this, ___collection, method) (( void (*) (List_1_t178 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::.ctor(System.Int32)
#define List_1__ctor_m17661(__this, ___capacity, method) (( void (*) (List_1_t178 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::.cctor()
#define List_1__cctor_m17662(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17663(__this, method) (( Object_t* (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17664(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t178 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17665(__this, method) (( Object_t * (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17666(__this, ___item, method) (( int32_t (*) (List_1_t178 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17667(__this, ___item, method) (( bool (*) (List_1_t178 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17668(__this, ___item, method) (( int32_t (*) (List_1_t178 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17669(__this, ___index, ___item, method) (( void (*) (List_1_t178 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17670(__this, ___item, method) (( void (*) (List_1_t178 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17671(__this, method) (( bool (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17672(__this, method) (( bool (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17673(__this, method) (( Object_t * (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17674(__this, method) (( bool (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17675(__this, method) (( bool (*) (List_1_t178 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17676(__this, ___index, method) (( Object_t * (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17677(__this, ___index, ___value, method) (( void (*) (List_1_t178 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Add(T)
#define List_1_Add_m17678(__this, ___item, method) (( void (*) (List_1_t178 *, UpgradeVG_t133 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17679(__this, ___newCount, method) (( void (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17680(__this, ___collection, method) (( void (*) (List_1_t178 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17681(__this, ___enumerable, method) (( void (*) (List_1_t178 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17682(__this, ___collection, method) (( void (*) (List_1_t178 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::AsReadOnly()
#define List_1_AsReadOnly_m17683(__this, method) (( ReadOnlyCollection_1_t3586 * (*) (List_1_t178 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Clear()
#define List_1_Clear_m17684(__this, method) (( void (*) (List_1_t178 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Contains(T)
#define List_1_Contains_m17685(__this, ___item, method) (( bool (*) (List_1_t178 *, UpgradeVG_t133 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17686(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t178 *, UpgradeVGU5BU5D_t3584*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Find(System.Predicate`1<T>)
#define List_1_Find_m17687(__this, ___match, method) (( UpgradeVG_t133 * (*) (List_1_t178 *, Predicate_1_t3587 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17688(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3587 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m17689(__this, ___match, method) (( int32_t (*) (List_1_t178 *, Predicate_1_t3587 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17690(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t178 *, int32_t, int32_t, Predicate_1_t3587 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m17691(__this, ___action, method) (( void (*) (List_1_t178 *, Action_1_t3588 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::GetEnumerator()
#define List_1_GetEnumerator_m978(__this, method) (( Enumerator_t215  (*) (List_1_t178 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::IndexOf(T)
#define List_1_IndexOf_m17692(__this, ___item, method) (( int32_t (*) (List_1_t178 *, UpgradeVG_t133 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17693(__this, ___start, ___delta, method) (( void (*) (List_1_t178 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17694(__this, ___index, method) (( void (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Insert(System.Int32,T)
#define List_1_Insert_m17695(__this, ___index, ___item, method) (( void (*) (List_1_t178 *, int32_t, UpgradeVG_t133 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17696(__this, ___collection, method) (( void (*) (List_1_t178 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Remove(T)
#define List_1_Remove_m17697(__this, ___item, method) (( bool (*) (List_1_t178 *, UpgradeVG_t133 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17698(__this, ___match, method) (( int32_t (*) (List_1_t178 *, Predicate_1_t3587 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17699(__this, ___index, method) (( void (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Reverse()
#define List_1_Reverse_m17700(__this, method) (( void (*) (List_1_t178 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Sort()
#define List_1_Sort_m17701(__this, method) (( void (*) (List_1_t178 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17702(__this, ___comparison, method) (( void (*) (List_1_t178 *, Comparison_1_t3589 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::ToArray()
#define List_1_ToArray_m17703(__this, method) (( UpgradeVGU5BU5D_t3584* (*) (List_1_t178 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::TrimExcess()
#define List_1_TrimExcess_m17704(__this, method) (( void (*) (List_1_t178 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::get_Capacity()
#define List_1_get_Capacity_m17705(__this, method) (( int32_t (*) (List_1_t178 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17706(__this, ___value, method) (( void (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::get_Count()
#define List_1_get_Count_m17707(__this, method) (( int32_t (*) (List_1_t178 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::get_Item(System.Int32)
#define List_1_get_Item_m17708(__this, ___index, method) (( UpgradeVG_t133 * (*) (List_1_t178 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>::set_Item(System.Int32,T)
#define List_1_set_Item_m17709(__this, ___index, ___value, method) (( void (*) (List_1_t178 *, int32_t, UpgradeVG_t133 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
