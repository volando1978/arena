﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t3409;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15303_gshared (InternalEnumerator_1_t3409 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m15303(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3409 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m15303_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared (InternalEnumerator_1_t3409 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3409 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15304_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15305_gshared (InternalEnumerator_1_t3409 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15305(__this, method) (( void (*) (InternalEnumerator_1_t3409 *, MethodInfo*))InternalEnumerator_1_Dispose_m15305_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15306_gshared (InternalEnumerator_1_t3409 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15306(__this, method) (( bool (*) (InternalEnumerator_1_t3409 *, MethodInfo*))InternalEnumerator_1_MoveNext_m15306_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m15307_gshared (InternalEnumerator_1_t3409 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15307(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3409 *, MethodInfo*))InternalEnumerator_1_get_Current_m15307_gshared)(__this, method)
