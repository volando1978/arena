﻿#pragma once
#include <stdint.h>
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t3556;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Object,System.Object,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey18_3_t3710  : public Object_t
{
	// System.Action`3<T1,T2,T3> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey18`3<System.Object,System.Object,System.Object>::toConvert
	Action_3_t3556 * ___toConvert_0;
};
