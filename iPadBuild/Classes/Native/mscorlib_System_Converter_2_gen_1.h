﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>,System.Object>
struct  Converter_2_t3404  : public MulticastDelegate_t22
{
};
