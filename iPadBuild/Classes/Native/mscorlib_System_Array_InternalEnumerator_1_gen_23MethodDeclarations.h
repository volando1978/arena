﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>
struct InternalEnumerator_1_t3763;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<System.Int32>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_1.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20273_gshared (InternalEnumerator_1_t3763 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m20273(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3763 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m20273_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20274_gshared (InternalEnumerator_1_t3763 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20274(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3763 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20274_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20275_gshared (InternalEnumerator_1_t3763 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20275(__this, method) (( void (*) (InternalEnumerator_1_t3763 *, MethodInfo*))InternalEnumerator_1_Dispose_m20275_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20276_gshared (InternalEnumerator_1_t3763 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20276(__this, method) (( bool (*) (InternalEnumerator_1_t3763 *, MethodInfo*))InternalEnumerator_1_MoveNext_m20276_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::get_Current()
extern "C" Link_t3760  InternalEnumerator_1_get_Current_m20277_gshared (InternalEnumerator_1_t3763 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20277(__this, method) (( Link_t3760  (*) (InternalEnumerator_1_t3763 *, MethodInfo*))InternalEnumerator_1_get_Current_m20277_gshared)(__this, method)
