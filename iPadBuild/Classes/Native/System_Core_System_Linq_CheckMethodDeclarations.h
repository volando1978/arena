﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Check
struct Check_t1802;
// System.Object
struct Object_t;

// System.Void System.Linq.Check::Source(System.Object)
extern "C" void Check_Source_m7680 (Object_t * __this /* static, unused */, Object_t * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndSelector(System.Object,System.Object)
extern "C" void Check_SourceAndSelector_m7681 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___selector, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern "C" void Check_SourceAndPredicate_m7682 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___predicate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::FirstAndSecond(System.Object,System.Object)
extern "C" void Check_FirstAndSecond_m7683 (Object_t * __this /* static, unused */, Object_t * ___first, Object_t * ___second, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndKeyElementSelectors(System.Object,System.Object,System.Object)
extern "C" void Check_SourceAndKeyElementSelectors_m7684 (Object_t * __this /* static, unused */, Object_t * ___source, Object_t * ___keySelector, Object_t * ___elementSelector, MethodInfo* method) IL2CPP_METHOD_ATTR;
