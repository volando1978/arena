﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativeSnapshotMetadata::.ctor(System.IntPtr)
extern "C" void NativeSnapshotMetadata__ctor_m2805 (NativeSnapshotMetadata_t611 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeSnapshotMetadata::get_IsOpen()
extern "C" bool NativeSnapshotMetadata_get_IsOpen_m2806 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Filename()
extern "C" String_t* NativeSnapshotMetadata_get_Filename_m2807 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_Description()
extern "C" String_t* NativeSnapshotMetadata_get_Description_m2808 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::get_CoverImageURL()
extern "C" String_t* NativeSnapshotMetadata_get_CoverImageURL_m2809 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan GooglePlayGames.Native.NativeSnapshotMetadata::get_TotalTimePlayed()
extern "C" TimeSpan_t190  NativeSnapshotMetadata_get_TotalTimePlayed_m2810 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime GooglePlayGames.Native.NativeSnapshotMetadata::get_LastModifiedTimestamp()
extern "C" DateTime_t48  NativeSnapshotMetadata_get_LastModifiedTimestamp_m2811 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeSnapshotMetadata::ToString()
extern "C" String_t* NativeSnapshotMetadata_ToString_m2812 (NativeSnapshotMetadata_t611 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSnapshotMetadata::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeSnapshotMetadata_CallDispose_m2813 (NativeSnapshotMetadata_t611 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_Filename>m__90(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_FilenameU3Em__90_m2814 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_Description>m__91(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_DescriptionU3Em__91_m2815 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeSnapshotMetadata::<get_CoverImageURL>m__92(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeSnapshotMetadata_U3Cget_CoverImageURLU3Em__92_m2816 (NativeSnapshotMetadata_t611 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
