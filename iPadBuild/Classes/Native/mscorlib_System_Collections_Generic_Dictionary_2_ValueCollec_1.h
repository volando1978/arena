﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Dictionary_2_t575;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct  ValueCollection_t921  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::dictionary
	Dictionary_2_t575 * ___dictionary_0;
};
