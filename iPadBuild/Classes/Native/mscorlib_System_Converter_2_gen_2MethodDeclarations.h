﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<System.Object,System.Object>
struct Converter_2_t185;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Converter_2__ctor_m15602_gshared (Converter_2_t185 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Converter_2__ctor_m15602(__this, ___object, ___method, method) (( void (*) (Converter_2_t185 *, Object_t *, IntPtr_t, MethodInfo*))Converter_2__ctor_m15602_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C" Object_t * Converter_2_Invoke_m15604_gshared (Converter_2_t185 * __this, Object_t * ___input, MethodInfo* method);
#define Converter_2_Invoke_m15604(__this, ___input, method) (( Object_t * (*) (Converter_2_t185 *, Object_t *, MethodInfo*))Converter_2_Invoke_m15604_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C" Object_t * Converter_2_BeginInvoke_m15606_gshared (Converter_2_t185 * __this, Object_t * ___input, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Converter_2_BeginInvoke_m15606(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t185 *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Converter_2_BeginInvoke_m15606_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Converter_2_EndInvoke_m15608_gshared (Converter_2_t185 * __this, Object_t * ___result, MethodInfo* method);
#define Converter_2_EndInvoke_m15608(__this, ___result, method) (( Object_t * (*) (Converter_2_t185 *, Object_t *, MethodInfo*))Converter_2_EndInvoke_m15608_gshared)(__this, ___result, method)
