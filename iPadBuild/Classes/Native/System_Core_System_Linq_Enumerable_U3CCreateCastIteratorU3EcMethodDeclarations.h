﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t3735;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19872_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19872(__this, method) (( void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19872_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19873_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19873(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19873_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19874_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19874(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19874_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19875_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19875(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19875_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C" Object_t* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19876_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19876(__this, method) (( Object_t* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19876_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19877_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19877(__this, method) (( bool (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19877_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19878_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19878(__this, method) (( void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3735 *, MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19878_gshared)(__this, method)
