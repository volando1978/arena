﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Schedule
struct Schedule_t50;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Reward>
struct Dictionary_2_t58;
// Soomla.SoomlaEntity`1<Soomla.Reward>
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_SoomlaEntity_1_gen.h"
// Soomla.Reward
struct  Reward_t55  : public SoomlaEntity_1_t59
{
	// Soomla.Schedule Soomla.Reward::Schedule
	Schedule_t50 * ___Schedule_5;
};
struct Reward_t55_StaticFields{
	// System.String Soomla.Reward::TAG
	String_t* ___TAG_4;
	// System.Collections.Generic.Dictionary`2<System.String,Soomla.Reward> Soomla.Reward::RewardsMap
	Dictionary_2_t58 * ___RewardsMap_6;
};
