﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
struct QuestUIResponse_t692;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>
struct  Action_1_t862  : public MulticastDelegate_t22
{
};
