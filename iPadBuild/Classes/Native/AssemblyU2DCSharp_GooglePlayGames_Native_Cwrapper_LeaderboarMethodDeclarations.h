﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Leaderboard
struct Leaderboard_t426;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Name(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Leaderboard_Leaderboard_Name_m1730 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Leaderboard_Leaderboard_Id_m1731 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_IconUrl(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  Leaderboard_Leaderboard_IconUrl_m1732 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void Leaderboard_Leaderboard_Dispose_m1733 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool Leaderboard_Leaderboard_Valid_m1734 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder GooglePlayGames.Native.Cwrapper.Leaderboard::Leaderboard_Order(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t Leaderboard_Leaderboard_Order_m1735 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
