﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.ReportProgress
struct  ReportProgress_t380  : public MulticastDelegate_t22
{
};
