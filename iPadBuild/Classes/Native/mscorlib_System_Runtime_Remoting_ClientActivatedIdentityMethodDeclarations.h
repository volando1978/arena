﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2642;
// System.MarshalByRefObject
struct MarshalByRefObject_t2011;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t2011 * ClientActivatedIdentity_GetServerObject_m12763 (ClientActivatedIdentity_t2642 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
