﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t1543;
// UnityEngine.Object
struct Object_t187;
struct Object_t187_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m6854 (ResourceRequest_t1543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t187 * ResourceRequest_get_asset_m6855 (ResourceRequest_t1543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
