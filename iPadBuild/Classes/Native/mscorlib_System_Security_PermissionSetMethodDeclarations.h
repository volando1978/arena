﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.PermissionSet
struct PermissionSet_t2525;

// System.Void System.Security.PermissionSet::.ctor()
extern "C" void PermissionSet__ctor_m13240 (PermissionSet_t2525 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
