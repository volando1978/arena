﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,JSONObject>
struct Dictionary_2_t167;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,JSONObject>
struct  ValueCollection_t3505  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,JSONObject>::dictionary
	Dictionary_2_t167 * ___dictionary_0;
};
