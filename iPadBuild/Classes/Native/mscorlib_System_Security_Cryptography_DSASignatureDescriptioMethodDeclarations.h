﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDescription
struct DSASignatureDescription_t2717;

// System.Void System.Security.Cryptography.DSASignatureDescription::.ctor()
extern "C" void DSASignatureDescription__ctor_m13185 (DSASignatureDescription_t2717 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
