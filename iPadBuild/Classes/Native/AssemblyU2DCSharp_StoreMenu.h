﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.GameObject
struct GameObject_t144;
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.Texture
struct Texture_t736;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t155;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// StoreMenu
struct  StoreMenu_t757  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle StoreMenu::labelSt
	GUIStyle_t724 * ___labelSt_2;
	// UnityEngine.GUIStyle StoreMenu::storeButtonSt
	GUIStyle_t724 * ___storeButtonSt_3;
	// UnityEngine.GUIStyle StoreMenu::storeButtonWinSt
	GUIStyle_t724 * ___storeButtonWinSt_4;
	// UnityEngine.GUIStyle StoreMenu::weaponsRackSt
	GUIStyle_t724 * ___weaponsRackSt_5;
	// UnityEngine.GUIStyle StoreMenu::lowBarSt
	GUIStyle_t724 * ___lowBarSt_6;
	// UnityEngine.GUIStyle StoreMenu::lowBarPlaySt
	GUIStyle_t724 * ___lowBarPlaySt_7;
	// UnityEngine.GUIStyle StoreMenu::gearWordSt
	GUIStyle_t724 * ___gearWordSt_8;
	// UnityEngine.GUIStyle StoreMenu::titleSt
	GUIStyle_t724 * ___titleSt_9;
	// System.Single StoreMenu::offsetLetterShadow
	float ___offsetLetterShadow_10;
	// UnityEngine.Rect StoreMenu::messageRect
	Rect_t738  ___messageRect_11;
	// UnityEngine.Rect StoreMenu::boxRect
	Rect_t738  ___boxRect_12;
	// UnityEngine.GameObject StoreMenu::gameControlObj
	GameObject_t144 * ___gameControlObj_14;
	// System.Collections.ArrayList StoreMenu::products
	ArrayList_t737 * ___products_15;
	// System.Collections.ArrayList StoreMenu::itemsToBuy
	ArrayList_t737 * ___itemsToBuy_16;
	// System.Collections.ArrayList StoreMenu::unlockableItems
	ArrayList_t737 * ___unlockableItems_17;
	// System.Collections.ArrayList StoreMenu::unlockableMiis
	ArrayList_t737 * ___unlockableMiis_18;
	// System.Collections.ArrayList StoreMenu::unlockablesToBuy
	ArrayList_t737 * ___unlockablesToBuy_19;
	// System.Collections.ArrayList StoreMenu::productPacks
	ArrayList_t737 * ___productPacks_20;
	// System.Collections.ArrayList StoreMenu::miiPacks
	ArrayList_t737 * ___miiPacks_21;
	// System.Collections.ArrayList StoreMenu::packsToBuy
	ArrayList_t737 * ___packsToBuy_22;
	// UnityEngine.Texture StoreMenu::hexGfx
	Texture_t736 * ___hexGfx_23;
	// UnityEngine.Texture StoreMenu::iconWBlock
	Texture_t736 * ___iconWBlock_24;
	// UnityEngine.Texture StoreMenu::iconLaser
	Texture_t736 * ___iconLaser_25;
	// UnityEngine.Texture StoreMenu::iconTWay
	Texture_t736 * ___iconTWay_26;
	// UnityEngine.Texture StoreMenu::iconCirculo
	Texture_t736 * ___iconCirculo_27;
	// UnityEngine.Texture StoreMenu::iconMoire
	Texture_t736 * ___iconMoire_28;
	// UnityEngine.Texture StoreMenu::iconBomba
	Texture_t736 * ___iconBomba_29;
	// UnityEngine.Texture StoreMenu::iconRayos
	Texture_t736 * ___iconRayos_30;
	// System.Collections.ArrayList StoreMenu::iconGfx
	ArrayList_t737 * ___iconGfx_31;
	// System.Collections.ArrayList StoreMenu::iconBombaGfx
	ArrayList_t737 * ___iconBombaGfx_32;
	// System.Int32 StoreMenu::runable
	int32_t ___runable_33;
	// System.Boolean StoreMenu::weaponSwitcher
	bool ___weaponSwitcher_34;
	// UnityEngine.Vector3 StoreMenu::posButton
	Vector3_t758  ___posButton_35;
	// UnityEngine.Vector2 StoreMenu::sizeButton
	Vector2_t739  ___sizeButton_36;
	// UnityEngine.Vector3 StoreMenu::posBox
	Vector3_t758  ___posBox_37;
	// UnityEngine.Vector2 StoreMenu::sizeBox
	Vector2_t739  ___sizeBox_38;
};
struct StoreMenu_t757_StaticFields{
	// UnityEngine.Rect StoreMenu::bulletsHUD
	Rect_t738  ___bulletsHUD_13;
	// System.Action`1<UnityEngine.Advertisements.ShowResult> StoreMenu::<>f__am$cache25
	Action_1_t155 * ___U3CU3Ef__amU24cache25_39;
};
