﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaUtils
struct SoomlaUtils_t52;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void Soomla.SoomlaUtils::LogDebug(System.String,System.String)
extern "C" void SoomlaUtils_LogDebug_m194 (Object_t * __this /* static, unused */, String_t* ___tag, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SoomlaUtils::LogError(System.String,System.String)
extern "C" void SoomlaUtils_LogError_m195 (Object_t * __this /* static, unused */, String_t* ___tag, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.SoomlaUtils::LogWarning(System.String,System.String)
extern "C" void SoomlaUtils_LogWarning_m196 (Object_t * __this /* static, unused */, String_t* ___tag, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.SoomlaUtils::GetClassName(System.Object)
extern "C" String_t* SoomlaUtils_GetClassName_m197 (Object_t * __this /* static, unused */, Object_t * ___target, MethodInfo* method) IL2CPP_METHOD_ATTR;
