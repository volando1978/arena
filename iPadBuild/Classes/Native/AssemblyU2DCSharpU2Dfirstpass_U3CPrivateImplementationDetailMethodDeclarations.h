﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$8
struct U24ArrayTypeU248_t161;
struct U24ArrayTypeU248_t161_marshaled;

void U24ArrayTypeU248_t161_marshal(const U24ArrayTypeU248_t161& unmarshaled, U24ArrayTypeU248_t161_marshaled& marshaled);
void U24ArrayTypeU248_t161_marshal_back(const U24ArrayTypeU248_t161_marshaled& marshaled, U24ArrayTypeU248_t161& unmarshaled);
void U24ArrayTypeU248_t161_marshal_cleanup(U24ArrayTypeU248_t161_marshaled& marshaled);
