﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Int32>
struct Enumerator_t3764;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t3762;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m20278_gshared (Enumerator_t3764 * __this, HashSet_1_t3762 * ___hashset, MethodInfo* method);
#define Enumerator__ctor_m20278(__this, ___hashset, method) (( void (*) (Enumerator_t3764 *, HashSet_1_t3762 *, MethodInfo*))Enumerator__ctor_m20278_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared (Enumerator_t3764 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20279(__this, method) (( Object_t * (*) (Enumerator_t3764 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20280_gshared (Enumerator_t3764 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m20280(__this, method) (( bool (*) (Enumerator_t3764 *, MethodInfo*))Enumerator_MoveNext_m20280_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m20281_gshared (Enumerator_t3764 * __this, MethodInfo* method);
#define Enumerator_get_Current_m20281(__this, method) (( int32_t (*) (Enumerator_t3764 *, MethodInfo*))Enumerator_get_Current_m20281_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m20282_gshared (Enumerator_t3764 * __this, MethodInfo* method);
#define Enumerator_Dispose_m20282(__this, method) (( void (*) (Enumerator_t3764 *, MethodInfo*))Enumerator_Dispose_m20282_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::CheckState()
extern "C" void Enumerator_CheckState_m20283_gshared (Enumerator_t3764 * __this, MethodInfo* method);
#define Enumerator_CheckState_m20283(__this, method) (( void (*) (Enumerator_t3764 *, MethodInfo*))Enumerator_CheckState_m20283_gshared)(__this, method)
