﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.IntPtr>
struct Comparer_1_t3815;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Comparer`1<System.IntPtr>::.ctor()
extern "C" void Comparer_1__ctor_m20863_gshared (Comparer_1_t3815 * __this, MethodInfo* method);
#define Comparer_1__ctor_m20863(__this, method) (( void (*) (Comparer_1_t3815 *, MethodInfo*))Comparer_1__ctor_m20863_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.IntPtr>::.cctor()
extern "C" void Comparer_1__cctor_m20864_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1__cctor_m20864(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1__cctor_m20864_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.IntPtr>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m20865_gshared (Comparer_1_t3815 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m20865(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3815 *, Object_t *, Object_t *, MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m20865_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.IntPtr>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.IntPtr>::get_Default()
extern "C" Comparer_1_t3815 * Comparer_1_get_Default_m20866_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Comparer_1_get_Default_m20866(__this /* static, unused */, method) (( Comparer_1_t3815 * (*) (Object_t * /* static, unused */, MethodInfo*))Comparer_1_get_Default_m20866_gshared)(__this /* static, unused */, method)
