﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t230;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t1324  : public MulticastDelegate_t22
{
};
