﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
struct FetchAllCallback_t419;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m1699 (FetchAllCallback_t419 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m1700 (FetchAllCallback_t419 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t419(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * FetchAllCallback_BeginInvoke_m1701 (FetchAllCallback_t419 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m1702 (FetchAllCallback_t419 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
