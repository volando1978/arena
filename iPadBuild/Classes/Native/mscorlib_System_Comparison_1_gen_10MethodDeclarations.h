﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Soomla.Schedule/DateTimeRange>
struct Comparison_1_t3514;
// System.Object
struct Object_t;
// Soomla.Schedule/DateTimeRange
struct DateTimeRange_t47;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Soomla.Schedule/DateTimeRange>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m16723(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3514 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m15541_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Soomla.Schedule/DateTimeRange>::Invoke(T,T)
#define Comparison_1_Invoke_m16724(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3514 *, DateTimeRange_t47 *, DateTimeRange_t47 *, MethodInfo*))Comparison_1_Invoke_m15542_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Soomla.Schedule/DateTimeRange>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m16725(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3514 *, DateTimeRange_t47 *, DateTimeRange_t47 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m15543_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Soomla.Schedule/DateTimeRange>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m16726(__this, ___result, method) (( int32_t (*) (Comparison_1_t3514 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m15544_gshared)(__this, ___result, method)
