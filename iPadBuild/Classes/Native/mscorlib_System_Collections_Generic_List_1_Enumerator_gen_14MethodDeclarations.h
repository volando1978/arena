﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3434;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>
struct List_1_t3413;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m15589(__this, ___l, method) (( void (*) (Enumerator_t3434 *, List_1_t3413 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15590(__this, method) (( Object_t * (*) (Enumerator_t3434 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::Dispose()
#define Enumerator_Dispose_m15591(__this, method) (( void (*) (Enumerator_t3434 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m15592(__this, method) (( void (*) (Enumerator_t3434 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m15593(__this, method) (( bool (*) (Enumerator_t3434 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>>::get_Current()
#define Enumerator_get_Current_m15594(__this, method) (( UnityKeyValuePair_2_t3412 * (*) (Enumerator_t3434 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
