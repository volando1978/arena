﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781  : public Object_t
{
	// T GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>::result
	Object_t * ___result_0;
	// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<T> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>::<>f__ref$94
	U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780 * ___U3CU3Ef__refU2494_1;
};
