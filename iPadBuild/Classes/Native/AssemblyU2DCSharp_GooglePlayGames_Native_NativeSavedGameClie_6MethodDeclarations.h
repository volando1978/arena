﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47
struct U3CReadBinaryDataU3Ec__AnonStorey47_t626;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t704;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47::.ctor()
extern "C" void U3CReadBinaryDataU3Ec__AnonStorey47__ctor_m2516 (U3CReadBinaryDataU3Ec__AnonStorey47_t626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ReadBinaryData>c__AnonStorey47::<>m__4B(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern "C" void U3CReadBinaryDataU3Ec__AnonStorey47_U3CU3Em__4B_m2517 (U3CReadBinaryDataU3Ec__AnonStorey47_t626 * __this, ReadResponse_t704 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
