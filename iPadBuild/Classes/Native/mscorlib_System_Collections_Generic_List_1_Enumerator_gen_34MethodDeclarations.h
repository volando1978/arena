﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t3890;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t1192;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m21858_gshared (Enumerator_t3890 * __this, List_1_t1192 * ___l, MethodInfo* method);
#define Enumerator__ctor_m21858(__this, ___l, method) (( void (*) (Enumerator_t3890 *, List_1_t1192 *, MethodInfo*))Enumerator__ctor_m21858_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21859_gshared (Enumerator_t3890 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21859(__this, method) (( Object_t * (*) (Enumerator_t3890 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m21860_gshared (Enumerator_t3890 * __this, MethodInfo* method);
#define Enumerator_Dispose_m21860(__this, method) (( void (*) (Enumerator_t3890 *, MethodInfo*))Enumerator_Dispose_m21860_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m21861_gshared (Enumerator_t3890 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m21861(__this, method) (( void (*) (Enumerator_t3890 *, MethodInfo*))Enumerator_VerifyState_m21861_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21862_gshared (Enumerator_t3890 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m21862(__this, method) (( bool (*) (Enumerator_t3890 *, MethodInfo*))Enumerator_MoveNext_m21862_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t1187  Enumerator_get_Current_m21863_gshared (Enumerator_t3890 * __this, MethodInfo* method);
#define Enumerator_get_Current_m21863(__this, method) (( RaycastResult_t1187  (*) (Enumerator_t3890 *, MethodInfo*))Enumerator_get_Current_m21863_gshared)(__this, method)
