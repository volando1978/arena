﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t1667;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2612;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct  LogicalCallContext_t2613  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.Messaging.LogicalCallContext::_data
	Hashtable_t1667 * ____data_0;
	// System.Runtime.Remoting.Messaging.CallContextRemotingData System.Runtime.Remoting.Messaging.LogicalCallContext::_remotingData
	CallContextRemotingData_t2612 * ____remotingData_1;
};
