﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_t1423;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
extern "C" void AssemblyCopyrightAttribute__ctor_m6179 (AssemblyCopyrightAttribute_t1423 * __this, String_t* ___copyright, MethodInfo* method) IL2CPP_METHOD_ATTR;
