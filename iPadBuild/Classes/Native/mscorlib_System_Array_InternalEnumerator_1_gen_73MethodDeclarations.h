﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct InternalEnumerator_1_t4210;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25870_gshared (InternalEnumerator_1_t4210 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25870(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4210 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25870_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25871_gshared (InternalEnumerator_1_t4210 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25871(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4210 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25871_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25872_gshared (InternalEnumerator_1_t4210 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25872(__this, method) (( void (*) (InternalEnumerator_1_t4210 *, MethodInfo*))InternalEnumerator_1_Dispose_m25872_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25873_gshared (InternalEnumerator_1_t4210 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25873(__this, method) (( bool (*) (InternalEnumerator_1_t4210 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25873_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t2360  InternalEnumerator_1_get_Current_m25874_gshared (InternalEnumerator_1_t4210 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25874(__this, method) (( TableRange_t2360  (*) (InternalEnumerator_1_t4210 *, MethodInfo*))InternalEnumerator_1_get_Current_m25874_gshared)(__this, method)
