﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse
struct TurnBasedMatchesResponse_t708;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct IEnumerable_1_t883;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::.ctor(System.IntPtr)
extern "C" void TurnBasedMatchesResponse__ctor_m3070 (TurnBasedMatchesResponse_t708 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchesResponse_CallDispose_m3071 (TurnBasedMatchesResponse_t708 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Status()
extern "C" int32_t TurnBasedMatchesResponse_Status_m3072 (TurnBasedMatchesResponse_t708 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::Invitations()
extern "C" Object_t* TurnBasedMatchesResponse_Invitations_m3073 (TurnBasedMatchesResponse_t708 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::FromPointer(System.IntPtr)
extern "C" TurnBasedMatchesResponse_t708 * TurnBasedMatchesResponse_FromPointer_m3074 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse::<Invitations>m__A0(System.UIntPtr)
extern "C" MultiplayerInvitation_t601 * TurnBasedMatchesResponse_U3CInvitationsU3Em__A0_m3075 (TurnBasedMatchesResponse_t708 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
