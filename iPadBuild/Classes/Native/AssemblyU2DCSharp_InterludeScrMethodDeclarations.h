﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InterludeScr
struct InterludeScr_t741;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void InterludeScr::.ctor()
extern "C" void InterludeScr__ctor_m3221 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr::.cctor()
extern "C" void InterludeScr__cctor_m3222 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// InterludeScr InterludeScr::get_Instance()
extern "C" InterludeScr_t741 * InterludeScr_get_Instance_m3223 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr::Start()
extern "C" void InterludeScr_Start_m3224 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InterludeScr::run()
extern "C" Object_t * InterludeScr_run_m3225 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr::OnGUI()
extern "C" void InterludeScr_OnGUI_m3226 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr::OnApplicationQuit()
extern "C" void InterludeScr_OnApplicationQuit_m3227 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterludeScr::DestroyInstance()
extern "C" void InterludeScr_DestroyInstance_m3228 (InterludeScr_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
