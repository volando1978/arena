﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>
struct ReadOnlyCollection_1_t3810;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.IntPtr>
struct IList_1_t3809;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t3784;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m20785_gshared (ReadOnlyCollection_1_t3810 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m20785(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3810 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m20785_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20786_gshared (ReadOnlyCollection_1_t3810 * __this, IntPtr_t ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20786(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3810 *, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20786_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20787_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20787(__this, method) (( void (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20787_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20788_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, IntPtr_t ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20788(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20788_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20789_gshared (ReadOnlyCollection_1_t3810 * __this, IntPtr_t ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20789(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20789_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20790_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20790(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20790_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" IntPtr_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20791_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20791(__this, ___index, method) (( IntPtr_t (*) (ReadOnlyCollection_1_t3810 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20791_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20792_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, IntPtr_t ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20792(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20792_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20793_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20793(__this, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20793_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20794_gshared (ReadOnlyCollection_1_t3810 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20794(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3810 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20794_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20795_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20795(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20795_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m20796_gshared (ReadOnlyCollection_1_t3810 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m20796(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3810 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m20796_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m20797_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m20797(__this, method) (( void (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m20797_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m20798_gshared (ReadOnlyCollection_1_t3810 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m20798(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m20798_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20799_gshared (ReadOnlyCollection_1_t3810 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20799(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3810 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20799_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m20800_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m20800(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m20800_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m20801_gshared (ReadOnlyCollection_1_t3810 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m20801(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3810 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m20801_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20802_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20802(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20802_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20803_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20803(__this, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20803_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20804_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20804(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20804_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20805_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20805(__this, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20805_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20806_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20806(__this, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20806_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m20807_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m20807(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3810 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m20807_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m20808_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m20808(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3810 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m20808_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m20809_gshared (ReadOnlyCollection_1_t3810 * __this, IntPtr_t ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m20809(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3810 *, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_Contains_m20809_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m20810_gshared (ReadOnlyCollection_1_t3810 * __this, IntPtrU5BU5D_t859* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m20810(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3810 *, IntPtrU5BU5D_t859*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m20810_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m20811_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m20811(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m20811_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m20812_gshared (ReadOnlyCollection_1_t3810 * __this, IntPtr_t ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m20812(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3810 *, IntPtr_t, MethodInfo*))ReadOnlyCollection_1_IndexOf_m20812_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m20813_gshared (ReadOnlyCollection_1_t3810 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m20813(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3810 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m20813_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>::get_Item(System.Int32)
extern "C" IntPtr_t ReadOnlyCollection_1_get_Item_m20814_gshared (ReadOnlyCollection_1_t3810 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m20814(__this, ___index, method) (( IntPtr_t (*) (ReadOnlyCollection_1_t3810 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m20814_gshared)(__this, ___index, method)
