﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener
struct RealTimeMultiplayerListener_t573;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct  OnGameThreadForwardingListener_t563  : public Object_t
{
	// GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener::mListener
	Object_t * ___mListener_0;
};
