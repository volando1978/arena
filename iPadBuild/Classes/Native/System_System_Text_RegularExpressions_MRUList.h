﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.MRUList/Node
struct Node_t2074;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MRUList
struct  MRUList_t2073  : public Object_t
{
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::head
	Node_t2074 * ___head_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::tail
	Node_t2074 * ___tail_1;
};
