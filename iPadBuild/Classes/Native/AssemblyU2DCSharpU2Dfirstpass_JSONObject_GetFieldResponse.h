﻿#pragma once
#include <stdint.h>
// JSONObject
struct JSONObject_t30;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// JSONObject/GetFieldResponse
struct  GetFieldResponse_t33  : public MulticastDelegate_t22
{
};
