﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Outline
struct Outline_t1337;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1267;

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m5718 (Outline_t1337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m5719 (Outline_t1337 * __this, List_1_t1267 * ___verts, MethodInfo* method) IL2CPP_METHOD_ATTR;
