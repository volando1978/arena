﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t457;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
struct OnParticipantStatusChangedCallback_t455;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
struct OnP2PDisconnectedCallback_t454;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
struct OnDataReceivedCallback_t456;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
struct OnRoomStatusChangedCallback_t451;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
struct OnP2PConnectedCallback_t453;
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
struct OnRoomConnectedSetChangedCallback_t452;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m1947 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnParticipantStatusChangedCallback_t455 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Construct()
extern "C" IntPtr_t RealTimeEventListenerHelper_RealTimeEventListenerHelper_Construct_m1948 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback_m1949 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnP2PDisconnectedCallback_t454 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnDataReceivedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnDataReceivedCallback_m1950 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnDataReceivedCallback_t456 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m1951 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnRoomStatusChangedCallback_t451 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PConnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PConnectedCallback_m1952 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnP2PConnectedCallback_t453 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback,System.IntPtr)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m1953 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnRoomConnectedSetChangedCallback_t452 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_Dispose_m1954 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
