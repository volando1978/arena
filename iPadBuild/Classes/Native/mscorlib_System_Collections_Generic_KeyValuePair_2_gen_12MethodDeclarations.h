﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
struct KeyValuePair_2_t3656;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m18957_gshared (KeyValuePair_2_t3656 * __this, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m18957(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3656 *, Object_t *, uint32_t, MethodInfo*))KeyValuePair_2__ctor_m18957_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m18958_gshared (KeyValuePair_2_t3656 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m18958(__this, method) (( Object_t * (*) (KeyValuePair_2_t3656 *, MethodInfo*))KeyValuePair_2_get_Key_m18958_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m18959_gshared (KeyValuePair_2_t3656 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m18959(__this, ___value, method) (( void (*) (KeyValuePair_2_t3656 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Key_m18959_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::get_Value()
extern "C" uint32_t KeyValuePair_2_get_Value_m18960_gshared (KeyValuePair_2_t3656 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m18960(__this, method) (( uint32_t (*) (KeyValuePair_2_t3656 *, MethodInfo*))KeyValuePair_2_get_Value_m18960_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m18961_gshared (KeyValuePair_2_t3656 * __this, uint32_t ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m18961(__this, ___value, method) (( void (*) (KeyValuePair_2_t3656 *, uint32_t, MethodInfo*))KeyValuePair_2_set_Value_m18961_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m18962_gshared (KeyValuePair_2_t3656 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m18962(__this, method) (( String_t* (*) (KeyValuePair_2_t3656 *, MethodInfo*))KeyValuePair_2_ToString_m18962_gshared)(__this, method)
