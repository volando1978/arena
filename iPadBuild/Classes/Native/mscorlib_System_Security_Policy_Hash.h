﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t2144;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Policy.Hash
struct  Hash_t2726  : public Object_t
{
	// System.Reflection.Assembly System.Security.Policy.Hash::assembly
	Assembly_t2144 * ___assembly_0;
	// System.Byte[] System.Security.Policy.Hash::data
	ByteU5BU5D_t350* ___data_1;
};
