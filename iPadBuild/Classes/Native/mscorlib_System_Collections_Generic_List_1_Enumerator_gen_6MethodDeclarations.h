﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>
struct Enumerator_t226;
// System.Object
struct Object_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.Collections.Generic.List`1<Soomla.Store.VirtualItem>
struct List_1_t179;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m17498(__this, ___l, method) (( void (*) (Enumerator_t226 *, List_1_t179 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17499(__this, method) (( Object_t * (*) (Enumerator_t226 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::Dispose()
#define Enumerator_Dispose_m17500(__this, method) (( void (*) (Enumerator_t226 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::VerifyState()
#define Enumerator_VerifyState_m17501(__this, method) (( void (*) (Enumerator_t226 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::MoveNext()
#define Enumerator_MoveNext_m1016(__this, method) (( bool (*) (Enumerator_t226 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualItem>::get_Current()
#define Enumerator_get_Current_m1015(__this, method) (( VirtualItem_t125 * (*) (Enumerator_t226 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
