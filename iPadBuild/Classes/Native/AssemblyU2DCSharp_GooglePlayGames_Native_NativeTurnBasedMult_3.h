﻿#pragma once
#include <stdint.h>
// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Action_1_t637;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51
struct  U3CFindInvitationWithIdU3Ec__AnonStorey51_t638  : public Object_t
{
	// System.Action`1<GooglePlayGames.Native.PInvoke.MultiplayerInvitation> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::callback
	Action_1_t637 * ___callback_0;
	// System.String GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindInvitationWithId>c__AnonStorey51::invitationId
	String_t* ___invitationId_1;
};
