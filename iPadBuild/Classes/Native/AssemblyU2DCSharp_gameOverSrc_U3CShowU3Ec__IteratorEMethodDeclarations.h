﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameOverSrc/<Show>c__IteratorE
struct U3CShowU3Ec__IteratorE_t803;
// System.Object
struct Object_t;

// System.Void gameOverSrc/<Show>c__IteratorE::.ctor()
extern "C" void U3CShowU3Ec__IteratorE__ctor_m3494 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameOverSrc/<Show>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameOverSrc/<Show>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameOverSrc/<Show>c__IteratorE::MoveNext()
extern "C" bool U3CShowU3Ec__IteratorE_MoveNext_m3497 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc/<Show>c__IteratorE::Dispose()
extern "C" void U3CShowU3Ec__IteratorE_Dispose_m3498 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc/<Show>c__IteratorE::Reset()
extern "C" void U3CShowU3Ec__IteratorE_Reset_m3499 (U3CShowU3Ec__IteratorE_t803 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
