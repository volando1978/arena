﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55
struct U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::.ctor()
extern "C" void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55__ctor_m2559 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::<>m__5F(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_U3CU3Em__5F_m2560 (U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
