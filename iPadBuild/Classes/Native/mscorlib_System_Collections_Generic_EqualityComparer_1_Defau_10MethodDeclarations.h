﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t4113;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25018_gshared (DefaultComparer_t4113 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m25018(__this, method) (( void (*) (DefaultComparer_t4113 *, MethodInfo*))DefaultComparer__ctor_m25018_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25019_gshared (DefaultComparer_t4113 * __this, UILineInfo_t1398  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m25019(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4113 *, UILineInfo_t1398 , MethodInfo*))DefaultComparer_GetHashCode_m25019_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25020_gshared (DefaultComparer_t4113 * __this, UILineInfo_t1398  ___x, UILineInfo_t1398  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m25020(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4113 *, UILineInfo_t1398 , UILineInfo_t1398 , MethodInfo*))DefaultComparer_Equals_m25020_gshared)(__this, ___x, ___y, method)
