﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t809;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t810;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t811;
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// headScr
struct  headScr_t781  : public MonoBehaviour_t26
{
	// UnityEngine.Transform headScr::head
	Transform_t809 * ___head_2;
	// System.Collections.Generic.List`1<UnityEngine.Transform> headScr::segments
	List_1_t810 * ___segments_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> headScr::breadcrumbs
	List_1_t811 * ___breadcrumbs_4;
	// System.Int32 headScr::mybodyLenght
	int32_t ___mybodyLenght_5;
	// UnityEngine.GameObject headScr::cuerpo
	GameObject_t144 * ___cuerpo_6;
	// System.Single headScr::segmentSpacing
	float ___segmentSpacing_7;
	// UnityEngine.GameObject headScr::explosion
	GameObject_t144 * ___explosion_8;
};
