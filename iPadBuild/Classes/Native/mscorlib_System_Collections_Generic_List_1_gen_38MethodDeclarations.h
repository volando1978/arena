﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t1297;
// System.Object
struct Object_t;
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t1296;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerable_1_t4480;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerator_1_t4481;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ICollection_1_t4482;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ReadOnlyCollection_1_t4029;
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t4027;
// System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Predicate_1_t4030;
// System.Action`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Action_1_t4031;
// System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Comparison_1_t4033;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_44.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m6103(__this, method) (( void (*) (List_1_t1297 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23754(__this, ___collection, method) (( void (*) (List_1_t1297 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Int32)
#define List_1__ctor_m23755(__this, ___capacity, method) (( void (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.cctor()
#define List_1__cctor_m23756(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23757(__this, method) (( Object_t* (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23758(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1297 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23759(__this, method) (( Object_t * (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23760(__this, ___item, method) (( int32_t (*) (List_1_t1297 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23761(__this, ___item, method) (( bool (*) (List_1_t1297 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23762(__this, ___item, method) (( int32_t (*) (List_1_t1297 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23763(__this, ___index, ___item, method) (( void (*) (List_1_t1297 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23764(__this, ___item, method) (( void (*) (List_1_t1297 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23765(__this, method) (( bool (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23766(__this, method) (( bool (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23767(__this, method) (( Object_t * (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23768(__this, method) (( bool (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23769(__this, method) (( bool (*) (List_1_t1297 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23770(__this, ___index, method) (( Object_t * (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23771(__this, ___index, ___value, method) (( void (*) (List_1_t1297 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Add(T)
#define List_1_Add_m23772(__this, ___item, method) (( void (*) (List_1_t1297 *, MatEntry_t1296 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23773(__this, ___newCount, method) (( void (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23774(__this, ___collection, method) (( void (*) (List_1_t1297 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23775(__this, ___enumerable, method) (( void (*) (List_1_t1297 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23776(__this, ___collection, method) (( void (*) (List_1_t1297 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AsReadOnly()
#define List_1_AsReadOnly_m23777(__this, method) (( ReadOnlyCollection_1_t4029 * (*) (List_1_t1297 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Clear()
#define List_1_Clear_m23778(__this, method) (( void (*) (List_1_t1297 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Contains(T)
#define List_1_Contains_m23779(__this, ___item, method) (( bool (*) (List_1_t1297 *, MatEntry_t1296 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23780(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1297 *, MatEntryU5BU5D_t4027*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Find(System.Predicate`1<T>)
#define List_1_Find_m23781(__this, ___match, method) (( MatEntry_t1296 * (*) (List_1_t1297 *, Predicate_1_t4030 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23782(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4030 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m23783(__this, ___match, method) (( int32_t (*) (List_1_t1297 *, Predicate_1_t4030 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23784(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1297 *, int32_t, int32_t, Predicate_1_t4030 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m23785(__this, ___action, method) (( void (*) (List_1_t1297 *, Action_1_t4031 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetEnumerator()
#define List_1_GetEnumerator_m23786(__this, method) (( Enumerator_t4032  (*) (List_1_t1297 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::IndexOf(T)
#define List_1_IndexOf_m23787(__this, ___item, method) (( int32_t (*) (List_1_t1297 *, MatEntry_t1296 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23788(__this, ___start, ___delta, method) (( void (*) (List_1_t1297 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23789(__this, ___index, method) (( void (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Insert(System.Int32,T)
#define List_1_Insert_m23790(__this, ___index, ___item, method) (( void (*) (List_1_t1297 *, int32_t, MatEntry_t1296 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23791(__this, ___collection, method) (( void (*) (List_1_t1297 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Remove(T)
#define List_1_Remove_m23792(__this, ___item, method) (( bool (*) (List_1_t1297 *, MatEntry_t1296 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23793(__this, ___match, method) (( int32_t (*) (List_1_t1297 *, Predicate_1_t4030 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23794(__this, ___index, method) (( void (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Reverse()
#define List_1_Reverse_m23795(__this, method) (( void (*) (List_1_t1297 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort()
#define List_1_Sort_m23796(__this, method) (( void (*) (List_1_t1297 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23797(__this, ___comparison, method) (( void (*) (List_1_t1297 *, Comparison_1_t4033 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::ToArray()
#define List_1_ToArray_m23798(__this, method) (( MatEntryU5BU5D_t4027* (*) (List_1_t1297 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::TrimExcess()
#define List_1_TrimExcess_m23799(__this, method) (( void (*) (List_1_t1297 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Capacity()
#define List_1_get_Capacity_m23800(__this, method) (( int32_t (*) (List_1_t1297 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23801(__this, ___value, method) (( void (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count()
#define List_1_get_Count_m23802(__this, method) (( int32_t (*) (List_1_t1297 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32)
#define List_1_get_Item_m23803(__this, ___index, method) (( MatEntry_t1296 * (*) (List_1_t1297 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m23804(__this, ___index, ___value, method) (( void (*) (List_1_t1297 *, int32_t, MatEntry_t1296 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
