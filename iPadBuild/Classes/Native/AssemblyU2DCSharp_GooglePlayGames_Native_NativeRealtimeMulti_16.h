﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.Action
struct Action_t588;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom
struct  LeavingRoom_t589  : public State_t565
{
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::mSession
	RoomSession_t566 * ___mSession_0;
	// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::mRoomToLeave
	NativeRealTimeRoom_t574 * ___mRoomToLeave_1;
	// System.Action GooglePlayGames.Native.NativeRealtimeMultiplayerClient/LeavingRoom::mLeavingCompleteCallback
	Action_t588 * ___mLeavingCompleteCallback_2;
};
