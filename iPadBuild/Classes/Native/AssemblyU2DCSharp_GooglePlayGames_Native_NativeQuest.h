﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeQuestMilestone modreq(System.Runtime.CompilerServices.IsVolatile)
struct NativeQuestMilestone_t676;
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferen.h"
// GooglePlayGames.Native.NativeQuest
struct  NativeQuest_t677  : public BaseReferenceHolder_t654
{
	// GooglePlayGames.Native.NativeQuestMilestone modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeQuest::mCachedMilestone
	NativeQuestMilestone_t676 * ___mCachedMilestone_1;
};
