﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.StoreInventory/LocalUpgrade
struct LocalUpgrade_t101;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct  KeyValuePair_2_t3572 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::value
	LocalUpgrade_t101 * ___value_1;
};
