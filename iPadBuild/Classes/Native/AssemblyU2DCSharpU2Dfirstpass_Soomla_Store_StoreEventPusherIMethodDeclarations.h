﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEventPusherIOS
struct StoreEventPusherIOS_t83;
// System.String
struct String_t;

// System.Void Soomla.Store.StoreEventPusherIOS::.ctor()
extern "C" void StoreEventPusherIOS__ctor_m320 (StoreEventPusherIOS_t83 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventSoomlaStoreInitialized(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventSoomlaStoreInitialized_m321 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventUnexpectedStoreError(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventUnexpectedStoreError_m322 (Object_t * __this /* static, unused */, String_t* ___errMessage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventCurrencyBalanceChanged(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventCurrencyBalanceChanged_m323 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventGoodBalanceChanged(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventGoodBalanceChanged_m324 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventGoodEquipped(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventGoodEquipped_m325 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventGoodUnEquipped(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventGoodUnEquipped_m326 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventGoodUpgrade(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventGoodUpgrade_m327 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventItemPurchased(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventItemPurchased_m328 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::eventDispatcher_PushEventItemPurchaseStarted(System.String)
extern "C" void StoreEventPusherIOS_eventDispatcher_PushEventItemPurchaseStarted_m329 (Object_t * __this /* static, unused */, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventSoomlaStoreInitialized(System.String)
extern "C" void StoreEventPusherIOS__pushEventSoomlaStoreInitialized_m330 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventUnexpectedStoreError(System.String)
extern "C" void StoreEventPusherIOS__pushEventUnexpectedStoreError_m331 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventCurrencyBalanceChanged(System.String)
extern "C" void StoreEventPusherIOS__pushEventCurrencyBalanceChanged_m332 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventGoodBalanceChanged(System.String)
extern "C" void StoreEventPusherIOS__pushEventGoodBalanceChanged_m333 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventGoodEquipped(System.String)
extern "C" void StoreEventPusherIOS__pushEventGoodEquipped_m334 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventGoodUnequipped(System.String)
extern "C" void StoreEventPusherIOS__pushEventGoodUnequipped_m335 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventGoodUpgrade(System.String)
extern "C" void StoreEventPusherIOS__pushEventGoodUpgrade_m336 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventItemPurchased(System.String)
extern "C" void StoreEventPusherIOS__pushEventItemPurchased_m337 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEventPusherIOS::_pushEventItemPurchaseStarted(System.String)
extern "C" void StoreEventPusherIOS__pushEventItemPurchaseStarted_m338 (StoreEventPusherIOS_t83 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
