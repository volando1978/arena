﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>
struct Enumerator_t215;
// System.Object
struct Object_t;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m17748(__this, ___l, method) (( void (*) (Enumerator_t215 *, List_1_t178 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17749(__this, method) (( Object_t * (*) (Enumerator_t215 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::Dispose()
#define Enumerator_Dispose_m17750(__this, method) (( void (*) (Enumerator_t215 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::VerifyState()
#define Enumerator_VerifyState_m17751(__this, method) (( void (*) (Enumerator_t215 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::MoveNext()
#define Enumerator_MoveNext_m980(__this, method) (( bool (*) (Enumerator_t215 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.UpgradeVG>::get_Current()
#define Enumerator_get_Current_m979(__this, method) (( UpgradeVG_t133 * (*) (Enumerator_t215 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
