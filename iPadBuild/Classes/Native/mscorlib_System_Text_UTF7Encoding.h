﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// System.SByte[]
struct SByteU5BU5D_t2754;
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.UTF7Encoding
struct  UTF7Encoding_t2755  : public Encoding_t1666
{
	// System.Boolean System.Text.UTF7Encoding::allowOptionals
	bool ___allowOptionals_28;
};
struct UTF7Encoding_t2755_StaticFields{
	// System.Byte[] System.Text.UTF7Encoding::encodingRules
	ByteU5BU5D_t350* ___encodingRules_29;
	// System.SByte[] System.Text.UTF7Encoding::base64Values
	SByteU5BU5D_t2754* ___base64Values_30;
};
