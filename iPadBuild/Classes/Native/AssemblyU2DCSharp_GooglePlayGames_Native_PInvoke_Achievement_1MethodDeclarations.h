﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.AchievementManager
struct AchievementManager_t656;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
struct Action_1_t660;
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
struct Action_1_t866;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
struct Action_1_t867;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void AchievementManager__ctor_m2615 (AchievementManager_t656 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::ShowAllUI(System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>)
extern "C" void AchievementManager_ShowAllUI_m2616 (AchievementManager_t656 * __this, Action_1_t660 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::FetchAll(System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>)
extern "C" void AchievementManager_FetchAll_m2617 (AchievementManager_t656 * __this, Action_1_t866 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::InternalFetchAllCallback(System.IntPtr,System.IntPtr)
extern "C" void AchievementManager_InternalFetchAllCallback_m2618 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Fetch(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>)
extern "C" void AchievementManager_Fetch_m2619 (AchievementManager_t656 * __this, String_t* ___achId, Action_1_t867 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::InternalFetchCallback(System.IntPtr,System.IntPtr)
extern "C" void AchievementManager_InternalFetchCallback_m2620 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Increment(System.String,System.UInt32)
extern "C" void AchievementManager_Increment_m2621 (AchievementManager_t656 * __this, String_t* ___achievementId, uint32_t ___numSteps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Reveal(System.String)
extern "C" void AchievementManager_Reveal_m2622 (AchievementManager_t656 * __this, String_t* ___achievementId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.AchievementManager::Unlock(System.String)
extern "C" void AchievementManager_Unlock_m2623 (AchievementManager_t656 * __this, String_t* ___achievementId, MethodInfo* method) IL2CPP_METHOD_ATTR;
