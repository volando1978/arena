﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AppearInGame
struct AppearInGame_t719;

// System.Void AppearInGame::.ctor()
extern "C" void AppearInGame__ctor_m3139 (AppearInGame_t719 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
