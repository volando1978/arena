﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject/FieldNotFound
struct FieldNotFound_t32;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void JSONObject/FieldNotFound::.ctor(System.Object,System.IntPtr)
extern "C" void FieldNotFound__ctor_m66 (FieldNotFound_t32 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/FieldNotFound::Invoke(System.String)
extern "C" void FieldNotFound_Invoke_m67 (FieldNotFound_t32 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String
#include "mscorlib_System_String.h"
extern "C" void pinvoke_delegate_wrapper_FieldNotFound_t32(Il2CppObject* delegate, String_t* ___name);
// System.IAsyncResult JSONObject/FieldNotFound::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * FieldNotFound_BeginInvoke_m68 (FieldNotFound_t32 * __this, String_t* ___name, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/FieldNotFound::EndInvoke(System.IAsyncResult)
extern "C" void FieldNotFound_EndInvoke_m69 (FieldNotFound_t32 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
