﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit
struct RaycastHit_t990;
// UnityEngine.Collider
struct Collider_t900;
// UnityEngine.Rigidbody
struct Rigidbody_t994;
// UnityEngine.Transform
struct Transform_t809;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t758  RaycastHit_get_point_m5837 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t758  RaycastHit_get_normal_m5838 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m5840 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t900 * RaycastHit_get_collider_m5839 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t994 * RaycastHit_get_rigidbody_m7149 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t809 * RaycastHit_get_transform_m4137 (RaycastHit_t990 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
