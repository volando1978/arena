﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Object
struct Object_t;

// System.Void Soomla.Store.VirtualItem::.ctor(System.String,System.String,System.String)
extern "C" void VirtualItem__ctor_m593 (VirtualItem_t125 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualItem::.ctor(JSONObject)
extern "C" void VirtualItem__ctor_m594 (VirtualItem_t125 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.VirtualItem::get_ItemId()
extern "C" String_t* VirtualItem_get_ItemId_m595 (VirtualItem_t125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualItem::set_ItemId(System.String)
extern "C" void VirtualItem_set_ItemId_m596 (VirtualItem_t125 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.VirtualItem::Equals(System.Object)
extern "C" bool VirtualItem_Equals_m597 (VirtualItem_t125 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItem::GetHashCode()
extern "C" int32_t VirtualItem_GetHashCode_m598 (VirtualItem_t125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItem::Give(System.Int32)
extern "C" int32_t VirtualItem_Give_m599 (VirtualItem_t125 * __this, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItem::Give(System.Int32,System.Boolean)
// System.Int32 Soomla.Store.VirtualItem::Take(System.Int32)
extern "C" int32_t VirtualItem_Take_m600 (VirtualItem_t125 * __this, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItem::Take(System.Int32,System.Boolean)
// System.Int32 Soomla.Store.VirtualItem::ResetBalance(System.Int32)
extern "C" int32_t VirtualItem_ResetBalance_m601 (VirtualItem_t125 * __this, int32_t ___balance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualItem::ResetBalance(System.Int32,System.Boolean)
// System.Int32 Soomla.Store.VirtualItem::GetBalance()
// System.Void Soomla.Store.VirtualItem::Save(System.Boolean)
extern "C" void VirtualItem_Save_m602 (VirtualItem_t125 * __this, bool ___saveToDB, MethodInfo* method) IL2CPP_METHOD_ATTR;
