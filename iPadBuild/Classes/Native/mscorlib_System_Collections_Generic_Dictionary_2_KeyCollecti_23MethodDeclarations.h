﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct KeyCollection_t3802;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct Dictionary_2_t671;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEnumerator_1_t4387;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3757;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_51.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_21MethodDeclarations.h"
#define KeyCollection__ctor_m20627(__this, ___dictionary, method) (( void (*) (KeyCollection_t3802 *, Dictionary_2_t671 *, MethodInfo*))KeyCollection__ctor_m20545_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20628(__this, ___item, method) (( void (*) (KeyCollection_t3802 *, int32_t, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m20546_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20629(__this, method) (( void (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m20547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20630(__this, ___item, method) (( bool (*) (KeyCollection_t3802 *, int32_t, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m20548_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20631(__this, ___item, method) (( bool (*) (KeyCollection_t3802 *, int32_t, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m20549_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20632(__this, method) (( Object_t* (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m20550_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m20633(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3802 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m20551_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20634(__this, method) (( Object_t * (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m20552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20635(__this, method) (( bool (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m20553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20636(__this, method) (( bool (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m20554_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m20637(__this, method) (( Object_t * (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m20555_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m20638(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3802 *, ParticipantStatusU5BU5D_t3757*, int32_t, MethodInfo*))KeyCollection_CopyTo_m20556_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::GetEnumerator()
#define KeyCollection_GetEnumerator_m20639(__this, method) (( Enumerator_t4393  (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_GetEnumerator_m20557_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::get_Count()
#define KeyCollection_get_Count_m20640(__this, method) (( int32_t (*) (KeyCollection_t3802 *, MethodInfo*))KeyCollection_get_Count_m20558_gshared)(__this, method)
