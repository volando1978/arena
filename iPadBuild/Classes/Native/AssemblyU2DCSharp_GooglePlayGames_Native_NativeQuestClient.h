﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.QuestManager
struct QuestManager_t560;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient
struct  NativeQuestClient_t561  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.QuestManager GooglePlayGames.Native.NativeQuestClient::mManager
	QuestManager_t560 * ___mManager_0;
};
