﻿#pragma once
#include <stdint.h>
// UnityEngine.Animation
struct Animation_t982;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Animation/Enumerator
struct  Enumerator_t1594  : public Object_t
{
	// UnityEngine.Animation UnityEngine.Animation/Enumerator::m_Outer
	Animation_t982 * ___m_Outer_0;
	// System.Int32 UnityEngine.Animation/Enumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_1;
};
