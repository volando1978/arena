﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Enumerator_t904;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m18845(__this, ___l, method) (( void (*) (Enumerator_t904 *, List_1_t351 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18846(__this, method) (( Object_t * (*) (Enumerator_t904 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::Dispose()
#define Enumerator_Dispose_m18847(__this, method) (( void (*) (Enumerator_t904 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::VerifyState()
#define Enumerator_VerifyState_m18848(__this, method) (( void (*) (Enumerator_t904 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::MoveNext()
#define Enumerator_MoveNext_m3702(__this, method) (( bool (*) (Enumerator_t904 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Current()
#define Enumerator_get_Current_m3701(__this, method) (( Participant_t340 * (*) (Enumerator_t904 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
