﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t4252;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m26158_gshared (DefaultComparer_t4252 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26158(__this, method) (( void (*) (DefaultComparer_t4252 *, MethodInfo*))DefaultComparer__ctor_m26158_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m26159_gshared (DefaultComparer_t4252 * __this, TimeSpan_t190  ___x, TimeSpan_t190  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m26159(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4252 *, TimeSpan_t190 , TimeSpan_t190 , MethodInfo*))DefaultComparer_Compare_m26159_gshared)(__this, ___x, ___y, method)
