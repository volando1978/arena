﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Int32>
struct Action_1_t911;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>
struct  U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3723  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>::callback
	Action_1_t911 * ___callback_0;
	// T GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Int32>::data
	int32_t ___data_1;
};
