﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_1_t3693;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m19409(__this, ___object, ___method, method) (( void (*) (Action_1_t3693 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(T)
#define Action_1_Invoke_m19410(__this, ___obj, method) (( void (*) (Action_1_t3693 *, Object_t *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m19411(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3693 *, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m19412(__this, ___result, method) (( void (*) (Action_1_t3693 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
