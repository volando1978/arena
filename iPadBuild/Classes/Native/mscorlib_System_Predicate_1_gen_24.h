﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t588;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Action>
struct  Predicate_1_t3703  : public MulticastDelegate_t22
{
};
