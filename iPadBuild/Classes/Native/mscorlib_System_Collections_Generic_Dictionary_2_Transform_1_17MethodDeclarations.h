﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.UInt32>
struct Transform_1_t3665;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m19022_gshared (Transform_1_t3665 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Transform_1__ctor_m19022(__this, ___object, ___method, method) (( void (*) (Transform_1_t3665 *, Object_t *, IntPtr_t, MethodInfo*))Transform_1__ctor_m19022_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.UInt32>::Invoke(TKey,TValue)
extern "C" uint32_t Transform_1_Invoke_m19023_gshared (Transform_1_t3665 * __this, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Transform_1_Invoke_m19023(__this, ___key, ___value, method) (( uint32_t (*) (Transform_1_t3665 *, Object_t *, uint32_t, MethodInfo*))Transform_1_Invoke_m19023_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.UInt32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m19024_gshared (Transform_1_t3665 * __this, Object_t * ___key, uint32_t ___value, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Transform_1_BeginInvoke_m19024(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3665 *, Object_t *, uint32_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Transform_1_BeginInvoke_m19024_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.UInt32,System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C" uint32_t Transform_1_EndInvoke_m19025_gshared (Transform_1_t3665 * __this, Object_t * ___result, MethodInfo* method);
#define Transform_1_EndInvoke_m19025(__this, ___result, method) (( uint32_t (*) (Transform_1_t3665 *, Object_t *, MethodInfo*))Transform_1_EndInvoke_m19025_gshared)(__this, ___result, method)
