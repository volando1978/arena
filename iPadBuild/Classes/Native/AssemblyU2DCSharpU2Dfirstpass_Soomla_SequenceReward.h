﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Reward>
struct List_1_t56;
// Soomla.Reward
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Reward.h"
// Soomla.SequenceReward
struct  SequenceReward_t60  : public Reward_t55
{
	// System.Collections.Generic.List`1<Soomla.Reward> Soomla.SequenceReward::Rewards
	List_1_t56 * ___Rewards_8;
};
struct SequenceReward_t60_StaticFields{
	// System.String Soomla.SequenceReward::TAG
	String_t* ___TAG_7;
};
