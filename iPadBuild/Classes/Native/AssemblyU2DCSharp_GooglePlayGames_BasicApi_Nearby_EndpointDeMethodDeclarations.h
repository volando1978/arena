﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.EndpointDetails
struct EndpointDetails_t356;
struct EndpointDetails_t356_marshaled;
// System.String
struct String_t;

// System.Void GooglePlayGames.BasicApi.Nearby.EndpointDetails::.ctor(System.String,System.String,System.String,System.String)
extern "C" void EndpointDetails__ctor_m1457 (EndpointDetails_t356 * __this, String_t* ___endpointId, String_t* ___deviceId, String_t* ___name, String_t* ___serviceId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_EndpointId()
extern "C" String_t* EndpointDetails_get_EndpointId_m1458 (EndpointDetails_t356 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_DeviceId()
extern "C" String_t* EndpointDetails_get_DeviceId_m1459 (EndpointDetails_t356 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_Name()
extern "C" String_t* EndpointDetails_get_Name_m1460 (EndpointDetails_t356 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Nearby.EndpointDetails::get_ServiceId()
extern "C" String_t* EndpointDetails_get_ServiceId_m1461 (EndpointDetails_t356 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void EndpointDetails_t356_marshal(const EndpointDetails_t356& unmarshaled, EndpointDetails_t356_marshaled& marshaled);
void EndpointDetails_t356_marshal_back(const EndpointDetails_t356_marshaled& marshaled, EndpointDetails_t356& unmarshaled);
void EndpointDetails_t356_marshal_cleanup(EndpointDetails_t356_marshaled& marshaled);
