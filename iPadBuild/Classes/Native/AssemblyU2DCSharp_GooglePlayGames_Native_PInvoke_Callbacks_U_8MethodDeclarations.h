﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20418_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20418(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1__ctor_m20418_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1/<AsOnGameThreadCallback>c__AnonStorey5F`1<System.Object>::<>m__77()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20419_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20419(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_t3781 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5F_1_U3CU3Em__77_m20419_gshared)(__this, method)
