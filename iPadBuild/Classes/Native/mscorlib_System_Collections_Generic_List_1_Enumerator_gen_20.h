﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
struct List_1_t915;
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t913;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>
struct  Enumerator_t3645 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::l
	List_1_t915 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Events.IEvent>::current
	Object_t * ___current_3;
};
