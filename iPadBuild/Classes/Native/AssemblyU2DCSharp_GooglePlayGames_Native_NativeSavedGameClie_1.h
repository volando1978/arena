﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic.h"
// GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42
struct  U3COpenWithAutomaticConflictResolutionU3Ec__AnonStorey42_t616  : public Object_t
{
	// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::resolutionStrategy
	int32_t ___resolutionStrategy_0;
	// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata> GooglePlayGames.Native.NativeSavedGameClient/<OpenWithAutomaticConflictResolution>c__AnonStorey42::callback
	Action_2_t612 * ___callback_1;
};
