﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// circularScr
struct circularScr_t778;
// UnityEngine.Collider
struct Collider_t900;

// System.Void circularScr::.ctor()
extern "C" void circularScr__ctor_m3354 (circularScr_t778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circularScr::Start()
extern "C" void circularScr_Start_m3355 (circularScr_t778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circularScr::Update()
extern "C" void circularScr_Update_m3356 (circularScr_t778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circularScr::OnExplosion()
extern "C" void circularScr_OnExplosion_m3357 (circularScr_t778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circularScr::OnTriggerEnter(UnityEngine.Collider)
extern "C" void circularScr_OnTriggerEnter_m3358 (circularScr_t778 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void circularScr::leavePow()
extern "C" void circularScr_leavePow_m3359 (circularScr_t778 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
