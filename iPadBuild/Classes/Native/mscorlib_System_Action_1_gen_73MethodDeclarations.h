﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Matrix4x4>
struct Action_1_t3839;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Action`1<UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m21170_gshared (Action_1_t3839 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m21170(__this, ___object, ___method, method) (( void (*) (Action_1_t3839 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m21170_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Matrix4x4>::Invoke(T)
extern "C" void Action_1_Invoke_m21171_gshared (Action_1_t3839 * __this, Matrix4x4_t997  ___obj, MethodInfo* method);
#define Action_1_Invoke_m21171(__this, ___obj, method) (( void (*) (Action_1_t3839 *, Matrix4x4_t997 , MethodInfo*))Action_1_Invoke_m21171_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Matrix4x4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m21172_gshared (Action_1_t3839 * __this, Matrix4x4_t997  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m21172(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3839 *, Matrix4x4_t997 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m21172_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Matrix4x4>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m21173_gshared (Action_1_t3839 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m21173(__this, ___result, method) (( void (*) (Action_1_t3839 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m21173_gshared)(__this, ___result, method)
