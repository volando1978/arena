﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t809;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Transform>
struct  Comparison_1_t3848  : public MulticastDelegate_t22
{
};
