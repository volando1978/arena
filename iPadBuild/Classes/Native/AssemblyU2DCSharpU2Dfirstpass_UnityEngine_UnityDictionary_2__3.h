﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UnityDictionary`2/<ContainsKey>c__AnonStorey6<System.Object,System.Object>
struct  U3CContainsKeyU3Ec__AnonStorey6_t3453  : public Object_t
{
	// K UnityEngine.UnityDictionary`2/<ContainsKey>c__AnonStorey6<System.Object,System.Object>::key
	Object_t * ___key_0;
};
