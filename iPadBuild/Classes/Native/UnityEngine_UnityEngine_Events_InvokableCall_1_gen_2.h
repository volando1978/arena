﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t1405;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t4010  : public BaseInvokableCall_t1647
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Single>::Delegate
	UnityAction_1_t1405 * ___Delegate_0;
};
