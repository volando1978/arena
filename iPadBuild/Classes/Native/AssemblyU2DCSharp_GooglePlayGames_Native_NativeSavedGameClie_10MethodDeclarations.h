﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient
struct NativeSavedGameClient_t624;
// GooglePlayGames.Native.PInvoke.SnapshotManager
struct SnapshotManager_t610;
// System.String
struct String_t;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t621;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>
struct Action_2_t625;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t627;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
struct Action_2_t630;
// GooglePlayGames.Native.NativeSnapshotMetadataChange
struct NativeSnapshotMetadataChange_t679;
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectU.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient::.ctor(GooglePlayGames.Native.PInvoke.SnapshotManager)
extern "C" void NativeSavedGameClient__ctor_m2524 (NativeSavedGameClient_t624 * __this, SnapshotManager_t610 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::.cctor()
extern "C" void NativeSavedGameClient__cctor_m2525 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::OpenWithAutomaticConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void NativeSavedGameClient_OpenWithAutomaticConflictResolution_m2526 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, int32_t ___resolutionStrategy, Action_2_t612 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback GooglePlayGames.Native.NativeSavedGameClient::ToOnGameThread(GooglePlayGames.BasicApi.SavedGame.ConflictCallback)
extern "C" ConflictCallback_t621 * NativeSavedGameClient_ToOnGameThread_m2527 (NativeSavedGameClient_t624 * __this, ConflictCallback_t621 * ___conflictCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void NativeSavedGameClient_OpenWithManualConflictResolution_m2528 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, bool ___prefetchDataOnConflict, ConflictCallback_t621 * ___conflictCallback, Action_2_t612 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::InternalManualOpen(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void NativeSavedGameClient_InternalManualOpen_m2529 (NativeSavedGameClient_t624 * __this, String_t* ___filename, int32_t ___source, bool ___prefetchDataOnConflict, ConflictCallback_t621 * ___conflictCallback, Action_2_t612 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::ReadBinaryData(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>)
extern "C" void NativeSavedGameClient_ReadBinaryData_m2530 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, Action_2_t625 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::ShowSelectSavedGameUI(System.String,System.UInt32,System.Boolean,System.Boolean,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void NativeSavedGameClient_ShowSelectSavedGameUI_m2531 (NativeSavedGameClient_t624 * __this, String_t* ___uiTitle, uint32_t ___maxDisplayedSavedGames, bool ___showCreateSaveUI, bool ___showDeleteSaveUI, Action_2_t627 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::CommitUpdate(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[],System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void NativeSavedGameClient_CommitUpdate_m2532 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, SavedGameMetadataUpdate_t378  ___updateForMetadata, ByteU5BU5D_t350* ___updatedBinaryData, Action_2_t612 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::FetchAllSavedGames(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>)
extern "C" void NativeSavedGameClient_FetchAllSavedGames_m2533 (NativeSavedGameClient_t624 * __this, int32_t ___source, Action_2_t630 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient::Delete(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void NativeSavedGameClient_Delete_m2534 (NativeSavedGameClient_t624 * __this, Object_t * ___metadata, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeSavedGameClient::IsValidFilename(System.String)
extern "C" bool NativeSavedGameClient_IsValidFilename_m2535 (Object_t * __this /* static, unused */, String_t* ___filename, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy GooglePlayGames.Native.NativeSavedGameClient::AsConflictPolicy(GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy)
extern "C" int32_t NativeSavedGameClient_AsConflictPolicy_m2536 (Object_t * __this /* static, unused */, int32_t ___strategy, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus GooglePlayGames.Native.NativeSavedGameClient::AsRequestStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus)
extern "C" int32_t NativeSavedGameClient_AsRequestStatus_m2537 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/DataSource GooglePlayGames.Native.NativeSavedGameClient::AsDataSource(GooglePlayGames.BasicApi.DataSource)
extern "C" int32_t NativeSavedGameClient_AsDataSource_m2538 (Object_t * __this /* static, unused */, int32_t ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus GooglePlayGames.Native.NativeSavedGameClient::AsRequestStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern "C" int32_t NativeSavedGameClient_AsRequestStatus_m2539 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus GooglePlayGames.Native.NativeSavedGameClient::AsUIStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" int32_t NativeSavedGameClient_AsUIStatus_m2540 (Object_t * __this /* static, unused */, int32_t ___uiStatus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadataChange GooglePlayGames.Native.NativeSavedGameClient::AsMetadataChange(GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate)
extern "C" NativeSnapshotMetadataChange_t679 * NativeSavedGameClient_AsMetadataChange_m2541 (Object_t * __this /* static, unused */, SavedGameMetadataUpdate_t378  ___update, MethodInfo* method) IL2CPP_METHOD_ATTR;
