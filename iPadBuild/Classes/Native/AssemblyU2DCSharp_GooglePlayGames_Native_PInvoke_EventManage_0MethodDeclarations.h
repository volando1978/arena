﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
struct FetchAllResponse_t664;
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
struct List_1_t868;
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::.ctor(System.IntPtr)
extern "C" void FetchAllResponse__ctor_m2647 (FetchAllResponse_t664 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::ResponseStatus()
extern "C" int32_t FetchAllResponse_ResponseStatus_m2648 (FetchAllResponse_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent> GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::Data()
extern "C" List_1_t868 * FetchAllResponse_Data_m2649 (FetchAllResponse_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::RequestSucceeded()
extern "C" bool FetchAllResponse_RequestSucceeded_m2650 (FetchAllResponse_t664 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchAllResponse_CallDispose_m2651 (FetchAllResponse_t664 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::FromPointer(System.IntPtr)
extern "C" FetchAllResponse_t664 * FetchAllResponse_FromPointer_m2652 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__79(System.IntPtr[],System.UIntPtr)
extern "C" UIntPtr_t  FetchAllResponse_U3CDataU3Em__79_m2653 (FetchAllResponse_t664 * __this, IntPtrU5BU5D_t859* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeEvent GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse::<Data>m__7A(System.IntPtr)
extern "C" NativeEvent_t674 * FetchAllResponse_U3CDataU3Em__7A_m2654 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, MethodInfo* method) IL2CPP_METHOD_ATTR;
