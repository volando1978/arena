﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// backgroundDetails
struct backgroundDetails_t763;

// System.Void backgroundDetails::.ctor()
extern "C" void backgroundDetails__ctor_m3308 (backgroundDetails_t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void backgroundDetails::Start()
extern "C" void backgroundDetails_Start_m3309 (backgroundDetails_t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void backgroundDetails::Update()
extern "C" void backgroundDetails_Update_m3310 (backgroundDetails_t763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
