﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>
struct  KeyValuePair_2_t3601 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::value
	VirtualItem_t125 * ___value_1;
};
