﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Byte>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721;

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Byte>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19746_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19746(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19746_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Byte>::<>m__E(T)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19747_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721 * __this, uint8_t ___result, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19747(__this, ___result, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3721 *, uint8_t, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19747_gshared)(__this, ___result, method)
