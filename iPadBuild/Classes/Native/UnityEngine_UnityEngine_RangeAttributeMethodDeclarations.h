﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RangeAttribute
struct RangeAttribute_t1436;

// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C" void RangeAttribute__ctor_m6237 (RangeAttribute_t1436 * __this, float ___min, float ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
