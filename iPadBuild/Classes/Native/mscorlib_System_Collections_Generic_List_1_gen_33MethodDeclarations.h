﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1267;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t4457;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4454;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t1408;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t3964;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1264;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3965;
// System.Action`1<UnityEngine.UIVertex>
struct Action_1_t3966;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t3968;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_39.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m5971_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1__ctor_m5971(__this, method) (( void (*) (List_1_t1267 *, MethodInfo*))List_1__ctor_m5971_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m22949_gshared (List_1_t1267 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m22949(__this, ___collection, method) (( void (*) (List_1_t1267 *, Object_t*, MethodInfo*))List_1__ctor_m22949_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m7513_gshared (List_1_t1267 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m7513(__this, ___capacity, method) (( void (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1__ctor_m7513_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m22950_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m22950(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m22950_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22951_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22951(__this, method) (( Object_t* (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22951_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m22952_gshared (List_1_t1267 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m22952(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1267 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m22952_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m22953_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22953(__this, method) (( Object_t * (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m22953_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m22954_gshared (List_1_t1267 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m22954(__this, ___item, method) (( int32_t (*) (List_1_t1267 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m22954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m22955_gshared (List_1_t1267 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m22955(__this, ___item, method) (( bool (*) (List_1_t1267 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m22955_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m22956_gshared (List_1_t1267 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m22956(__this, ___item, method) (( int32_t (*) (List_1_t1267 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m22956_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m22957_gshared (List_1_t1267 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m22957(__this, ___index, ___item, method) (( void (*) (List_1_t1267 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m22957_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m22958_gshared (List_1_t1267 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m22958(__this, ___item, method) (( void (*) (List_1_t1267 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m22958_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22959_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22959(__this, method) (( bool (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22959_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m22960_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22960(__this, method) (( bool (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m22960_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m22961_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m22961(__this, method) (( Object_t * (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m22961_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m22962_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m22962(__this, method) (( bool (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m22962_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m22963_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m22963(__this, method) (( bool (*) (List_1_t1267 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m22963_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m22964_gshared (List_1_t1267 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m22964(__this, ___index, method) (( Object_t * (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m22964_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m22965_gshared (List_1_t1267 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m22965(__this, ___index, ___value, method) (( void (*) (List_1_t1267 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m22965_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m22966_gshared (List_1_t1267 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define List_1_Add_m22966(__this, ___item, method) (( void (*) (List_1_t1267 *, UIVertex_t1265 , MethodInfo*))List_1_Add_m22966_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m22967_gshared (List_1_t1267 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m22967(__this, ___newCount, method) (( void (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m22967_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m22968_gshared (List_1_t1267 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m22968(__this, ___collection, method) (( void (*) (List_1_t1267 *, Object_t*, MethodInfo*))List_1_AddCollection_m22968_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m22969_gshared (List_1_t1267 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m22969(__this, ___enumerable, method) (( void (*) (List_1_t1267 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m22969_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m22970_gshared (List_1_t1267 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m22970(__this, ___collection, method) (( void (*) (List_1_t1267 *, Object_t*, MethodInfo*))List_1_AddRange_m22970_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3964 * List_1_AsReadOnly_m22971_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m22971(__this, method) (( ReadOnlyCollection_1_t3964 * (*) (List_1_t1267 *, MethodInfo*))List_1_AsReadOnly_m22971_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m22972_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_Clear_m22972(__this, method) (( void (*) (List_1_t1267 *, MethodInfo*))List_1_Clear_m22972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m22973_gshared (List_1_t1267 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define List_1_Contains_m22973(__this, ___item, method) (( bool (*) (List_1_t1267 *, UIVertex_t1265 , MethodInfo*))List_1_Contains_m22973_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m22974_gshared (List_1_t1267 * __this, UIVertexU5BU5D_t1264* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m22974(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1267 *, UIVertexU5BU5D_t1264*, int32_t, MethodInfo*))List_1_CopyTo_m22974_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t1265  List_1_Find_m22975_gshared (List_1_t1267 * __this, Predicate_1_t3965 * ___match, MethodInfo* method);
#define List_1_Find_m22975(__this, ___match, method) (( UIVertex_t1265  (*) (List_1_t1267 *, Predicate_1_t3965 *, MethodInfo*))List_1_Find_m22975_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m22976_gshared (Object_t * __this /* static, unused */, Predicate_1_t3965 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m22976(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3965 *, MethodInfo*))List_1_CheckMatch_m22976_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m22977_gshared (List_1_t1267 * __this, Predicate_1_t3965 * ___match, MethodInfo* method);
#define List_1_FindIndex_m22977(__this, ___match, method) (( int32_t (*) (List_1_t1267 *, Predicate_1_t3965 *, MethodInfo*))List_1_FindIndex_m22977_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m22978_gshared (List_1_t1267 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3965 * ___match, MethodInfo* method);
#define List_1_GetIndex_m22978(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1267 *, int32_t, int32_t, Predicate_1_t3965 *, MethodInfo*))List_1_GetIndex_m22978_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m22979_gshared (List_1_t1267 * __this, Action_1_t3966 * ___action, MethodInfo* method);
#define List_1_ForEach_m22979(__this, ___action, method) (( void (*) (List_1_t1267 *, Action_1_t3966 *, MethodInfo*))List_1_ForEach_m22979_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t3967  List_1_GetEnumerator_m22980_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m22980(__this, method) (( Enumerator_t3967  (*) (List_1_t1267 *, MethodInfo*))List_1_GetEnumerator_m22980_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m22981_gshared (List_1_t1267 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define List_1_IndexOf_m22981(__this, ___item, method) (( int32_t (*) (List_1_t1267 *, UIVertex_t1265 , MethodInfo*))List_1_IndexOf_m22981_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m22982_gshared (List_1_t1267 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m22982(__this, ___start, ___delta, method) (( void (*) (List_1_t1267 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m22982_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m22983_gshared (List_1_t1267 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m22983(__this, ___index, method) (( void (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_CheckIndex_m22983_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m22984_gshared (List_1_t1267 * __this, int32_t ___index, UIVertex_t1265  ___item, MethodInfo* method);
#define List_1_Insert_m22984(__this, ___index, ___item, method) (( void (*) (List_1_t1267 *, int32_t, UIVertex_t1265 , MethodInfo*))List_1_Insert_m22984_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m22985_gshared (List_1_t1267 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m22985(__this, ___collection, method) (( void (*) (List_1_t1267 *, Object_t*, MethodInfo*))List_1_CheckCollection_m22985_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m22986_gshared (List_1_t1267 * __this, UIVertex_t1265  ___item, MethodInfo* method);
#define List_1_Remove_m22986(__this, ___item, method) (( bool (*) (List_1_t1267 *, UIVertex_t1265 , MethodInfo*))List_1_Remove_m22986_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m22987_gshared (List_1_t1267 * __this, Predicate_1_t3965 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m22987(__this, ___match, method) (( int32_t (*) (List_1_t1267 *, Predicate_1_t3965 *, MethodInfo*))List_1_RemoveAll_m22987_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m22988_gshared (List_1_t1267 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m22988(__this, ___index, method) (( void (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_RemoveAt_m22988_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m22989_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_Reverse_m22989(__this, method) (( void (*) (List_1_t1267 *, MethodInfo*))List_1_Reverse_m22989_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m22990_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_Sort_m22990(__this, method) (( void (*) (List_1_t1267 *, MethodInfo*))List_1_Sort_m22990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m22991_gshared (List_1_t1267 * __this, Comparison_1_t3968 * ___comparison, MethodInfo* method);
#define List_1_Sort_m22991(__this, ___comparison, method) (( void (*) (List_1_t1267 *, Comparison_1_t3968 *, MethodInfo*))List_1_Sort_m22991_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t1264* List_1_ToArray_m6018_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_ToArray_m6018(__this, method) (( UIVertexU5BU5D_t1264* (*) (List_1_t1267 *, MethodInfo*))List_1_ToArray_m6018_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m22992_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_TrimExcess_m22992(__this, method) (( void (*) (List_1_t1267 *, MethodInfo*))List_1_TrimExcess_m22992_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m5891_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_get_Capacity_m5891(__this, method) (( int32_t (*) (List_1_t1267 *, MethodInfo*))List_1_get_Capacity_m5891_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m5892_gshared (List_1_t1267 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m5892(__this, ___value, method) (( void (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_set_Capacity_m5892_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m22993_gshared (List_1_t1267 * __this, MethodInfo* method);
#define List_1_get_Count_m22993(__this, method) (( int32_t (*) (List_1_t1267 *, MethodInfo*))List_1_get_Count_m22993_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t1265  List_1_get_Item_m22994_gshared (List_1_t1267 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m22994(__this, ___index, method) (( UIVertex_t1265  (*) (List_1_t1267 *, int32_t, MethodInfo*))List_1_get_Item_m22994_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m22995_gshared (List_1_t1267 * __this, int32_t ___index, UIVertex_t1265  ___value, MethodInfo* method);
#define List_1_set_Item_m22995(__this, ___index, ___value, method) (( void (*) (List_1_t1267 *, int32_t, UIVertex_t1265 , MethodInfo*))List_1_set_Item_m22995_gshared)(__this, ___index, ___value, method)
