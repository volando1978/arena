﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaPostBuildTools
struct SoomlaPostBuildTools_t16;

// System.Void Soomla.SoomlaPostBuildTools::.ctor()
extern "C" void SoomlaPostBuildTools__ctor_m26 (SoomlaPostBuildTools_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
