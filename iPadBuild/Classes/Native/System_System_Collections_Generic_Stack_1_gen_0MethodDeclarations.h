﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3877;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" void Stack_1__ctor_m21668_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1__ctor_m21668(__this, method) (( void (*) (Stack_1_t3877 *, MethodInfo*))Stack_1__ctor_m21668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669(__this, method) (( bool (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m21669_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m21670_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m21670(__this, method) (( Object_t * (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m21670_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m21671_gshared (Stack_1_t3877 * __this, Array_t * ___dest, int32_t ___idx, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m21671(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3877 *, Array_t *, int32_t, MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m21671_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672(__this, method) (( Object_t* (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21672_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673(__this, method) (( Object_t * (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m21673_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Peek()
extern "C" Object_t * Stack_1_Peek_m21674_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_Peek_m21674(__this, method) (( Object_t * (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_Peek_m21674_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" Object_t * Stack_1_Pop_m21675_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_Pop_m21675(__this, method) (( Object_t * (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_Pop_m21675_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C" void Stack_1_Push_m21676_gshared (Stack_1_t3877 * __this, Object_t * ___t, MethodInfo* method);
#define Stack_1_Push_m21676(__this, ___t, method) (( void (*) (Stack_1_t3877 *, Object_t *, MethodInfo*))Stack_1_Push_m21676_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" int32_t Stack_1_get_Count_m21677_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_get_Count_m21677(__this, method) (( int32_t (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_get_Count_m21677_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3880  Stack_1_GetEnumerator_m21678_gshared (Stack_1_t3877 * __this, MethodInfo* method);
#define Stack_1_GetEnumerator_m21678(__this, method) (( Enumerator_t3880  (*) (Stack_1_t3877 *, MethodInfo*))Stack_1_GetEnumerator_m21678_gshared)(__this, method)
