﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
struct OutStringMethod_t681;
// System.Object
struct Object_t;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::.ctor(System.Object,System.IntPtr)
extern "C" void OutStringMethod__ctor_m2851 (OutStringMethod_t681 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::Invoke(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  OutStringMethod_Invoke_m2852 (OutStringMethod_t681 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
extern "C" UIntPtr_t  pinvoke_delegate_wrapper_OutStringMethod_t681(Il2CppObject* delegate, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::BeginInvoke(System.Text.StringBuilder,System.UIntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OutStringMethod_BeginInvoke_m2853 (OutStringMethod_t681 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::EndInvoke(System.IAsyncResult)
extern "C" UIntPtr_t  OutStringMethod_EndInvoke_m2854 (OutStringMethod_t681 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
