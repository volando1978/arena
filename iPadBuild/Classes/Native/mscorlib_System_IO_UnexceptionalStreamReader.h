﻿#pragma once
#include <stdint.h>
// System.Boolean[]
struct BooleanU5BU5D_t108;
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReader.h"
// System.IO.UnexceptionalStreamReader
struct  UnexceptionalStreamReader_t2503  : public StreamReader_t2497
{
};
struct UnexceptionalStreamReader_t2503_StaticFields{
	// System.Boolean[] System.IO.UnexceptionalStreamReader::newline
	BooleanU5BU5D_t108* ___newline_14;
	// System.Char System.IO.UnexceptionalStreamReader::newlineChar
	uint16_t ___newlineChar_15;
};
