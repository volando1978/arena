﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<startGame>c__Iterator7
struct U3CstartGameU3Ec__Iterator7_t795;
// System.Object
struct Object_t;

// System.Void gameControl/<startGame>c__Iterator7::.ctor()
extern "C" void U3CstartGameU3Ec__Iterator7__ctor_m3413 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<startGame>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<startGame>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<startGame>c__Iterator7::MoveNext()
extern "C" bool U3CstartGameU3Ec__Iterator7_MoveNext_m3416 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<startGame>c__Iterator7::Dispose()
extern "C" void U3CstartGameU3Ec__Iterator7_Dispose_m3417 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<startGame>c__Iterator7::Reset()
extern "C" void U3CstartGameU3Ec__Iterator7_Reset_m3418 (U3CstartGameU3Ec__Iterator7_t795 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
