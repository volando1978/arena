﻿#pragma once
#include <stdint.h>
// Mono.Globalization.Unicode.Level2MapComparer
struct Level2MapComparer_t2367;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.Level2MapComparer
struct  Level2MapComparer_t2367  : public Object_t
{
};
struct Level2MapComparer_t2367_StaticFields{
	// Mono.Globalization.Unicode.Level2MapComparer Mono.Globalization.Unicode.Level2MapComparer::Instance
	Level2MapComparer_t2367 * ___Instance_0;
};
