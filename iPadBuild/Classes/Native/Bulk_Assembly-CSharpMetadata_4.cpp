﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// cabezaScr
#include "AssemblyU2DCSharp_cabezaScr.h"
// Metadata Definition cabezaScr
extern TypeInfo cabezaScr_t773_il2cpp_TypeInfo;
// cabezaScr
#include "AssemblyU2DCSharp_cabezaScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cabezaScr::.ctor()
MethodInfo cabezaScr__ctor_m3330_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&cabezaScr__ctor_m3330/* method */
	, &cabezaScr_t773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cabezaScr::Start()
MethodInfo cabezaScr_Start_m3331_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&cabezaScr_Start_m3331/* method */
	, &cabezaScr_t773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cabezaScr::Update()
MethodInfo cabezaScr_Update_m3332_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&cabezaScr_Update_m3332/* method */
	, &cabezaScr_t773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType Single_t202_0_0_4112;
extern Il2CppType Single_t202_0_0_0;
extern Il2CppType Single_t202_0_0_4112;
static ParameterInfo cabezaScr_t773_cabezaScr_MoveFollower_m3333_ParameterInfos[] = 
{
	{"target", 0, 134220467, 0, &GameObject_t144_0_0_0},
	{"maxSpeed", 1, 134220468, 0, &Single_t202_0_0_4112},
	{"maxAngleSpeed", 2, 134220469, 0, &Single_t202_0_0_4112},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Void cabezaScr::MoveFollower(UnityEngine.GameObject,System.Single,System.Single)
MethodInfo cabezaScr_MoveFollower_m3333_MethodInfo = 
{
	"MoveFollower"/* name */
	, (methodPointerType)&cabezaScr_MoveFollower_m3333/* method */
	, &cabezaScr_t773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Single_t202_Single_t202/* invoker_method */
	, cabezaScr_t773_cabezaScr_MoveFollower_m3333_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* cabezaScr_t773_MethodInfos[] =
{
	&cabezaScr__ctor_m3330_MethodInfo,
	&cabezaScr_Start_m3331_MethodInfo,
	&cabezaScr_Update_m3332_MethodInfo,
	&cabezaScr_MoveFollower_m3333_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo cabezaScr_t773____mybody_2_FieldInfo = 
{
	"mybody"/* name */
	, &Int32_t189_0_0_6/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___mybody_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo cabezaScr_t773____bodyObj_3_FieldInfo = 
{
	"bodyObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___bodyObj_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t772_0_0_6;
FieldInfo cabezaScr_t773____formerPositions_4_FieldInfo = 
{
	"formerPositions"/* name */
	, &List_1_t772_0_0_6/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___formerPositions_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo cabezaScr_t773____frameDelay_5_FieldInfo = 
{
	"frameDelay"/* name */
	, &Int32_t189_0_0_6/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___frameDelay_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo cabezaScr_t773____minimumDistance_6_FieldInfo = 
{
	"minimumDistance"/* name */
	, &Single_t202_0_0_6/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___minimumDistance_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_1;
FieldInfo cabezaScr_t773____formerPosition_7_FieldInfo = 
{
	"formerPosition"/* name */
	, &Vector3_t758_0_0_1/* type */
	, &cabezaScr_t773_il2cpp_TypeInfo/* parent */
	, offsetof(cabezaScr_t773, ___formerPosition_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* cabezaScr_t773_FieldInfos[] =
{
	&cabezaScr_t773____mybody_2_FieldInfo,
	&cabezaScr_t773____bodyObj_3_FieldInfo,
	&cabezaScr_t773____formerPositions_4_FieldInfo,
	&cabezaScr_t773____frameDelay_5_FieldInfo,
	&cabezaScr_t773____minimumDistance_6_FieldInfo,
	&cabezaScr_t773____formerPosition_7_FieldInfo,
	NULL
};
extern Il2CppType Frame_t770_0_0_0;
static const Il2CppType* cabezaScr_t773_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Frame_t770_0_0_0,
};
extern MethodInfo Object_Equals_m1199_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1200_MethodInfo;
extern MethodInfo Object_ToString_m1201_MethodInfo;
static Il2CppMethodReference cabezaScr_t773_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool cabezaScr_t773_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType cabezaScr_t773_0_0_0;
extern Il2CppType cabezaScr_t773_1_0_0;
extern Il2CppType MonoBehaviour_t26_0_0_0;
struct cabezaScr_t773;
const Il2CppTypeDefinitionMetadata cabezaScr_t773_DefinitionMetadata = 
{
	NULL/* declaringType */
	, cabezaScr_t773_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, cabezaScr_t773_VTable/* vtableMethods */
	, cabezaScr_t773_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo cabezaScr_t773_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "cabezaScr"/* name */
	, ""/* namespaze */
	, cabezaScr_t773_MethodInfos/* methods */
	, NULL/* properties */
	, cabezaScr_t773_FieldInfos/* fields */
	, NULL/* events */
	, &cabezaScr_t773_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &cabezaScr_t773_0_0_0/* byval_arg */
	, &cabezaScr_t773_1_0_0/* this_arg */
	, &cabezaScr_t773_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (cabezaScr_t773)/* instance_size */
	, sizeof (cabezaScr_t773)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// cameraScript/<shake>c__Iterator5
#include "AssemblyU2DCSharp_cameraScript_U3CshakeU3Ec__Iterator5.h"
// Metadata Definition cameraScript/<shake>c__Iterator5
extern TypeInfo U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo;
// cameraScript/<shake>c__Iterator5
#include "AssemblyU2DCSharp_cameraScript_U3CshakeU3Ec__Iterator5MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shake>c__Iterator5::.ctor()
MethodInfo U3CshakeU3Ec__Iterator5__ctor_m3334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5__ctor_m3334/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object cameraScript/<shake>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 247/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object cameraScript/<shake>c__Iterator5::System.Collections.IEnumerator.get_Current()
MethodInfo U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 248/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean cameraScript/<shake>c__Iterator5::MoveNext()
MethodInfo U3CshakeU3Ec__Iterator5_MoveNext_m3337_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5_MoveNext_m3337/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shake>c__Iterator5::Dispose()
MethodInfo U3CshakeU3Ec__Iterator5_Dispose_m3338_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5_Dispose_m3338/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 249/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shake>c__Iterator5::Reset()
MethodInfo U3CshakeU3Ec__Iterator5_Reset_m3339_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CshakeU3Ec__Iterator5_Reset_m3339/* method */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 250/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CshakeU3Ec__Iterator5_t775_MethodInfos[] =
{
	&U3CshakeU3Ec__Iterator5__ctor_m3334_MethodInfo,
	&U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335_MethodInfo,
	&U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336_MethodInfo,
	&U3CshakeU3Ec__Iterator5_MoveNext_m3337_MethodInfo,
	&U3CshakeU3Ec__Iterator5_Dispose_m3338_MethodInfo,
	&U3CshakeU3Ec__Iterator5_Reset_m3339_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____t_0_FieldInfo = 
{
	"t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___t_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U3CpU3E__0_1_FieldInfo = 
{
	"<p>__0"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U3CpU3E__0_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U3CipU3E__1_2_FieldInfo = 
{
	"<ip>__1"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U3CipU3E__1_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U3Cip2U3E__2_3_FieldInfo = 
{
	"<ip2>__2"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U3Cip2U3E__2_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U24PC_4_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U24PC_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U24current_5_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U24current_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U3CU24U3Et_6_FieldInfo = 
{
	"<$>t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U3CU24U3Et_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType cameraScript_t774_0_0_3;
FieldInfo U3CshakeU3Ec__Iterator5_t775____U3CU3Ef__this_7_FieldInfo = 
{
	"<>f__this"/* name */
	, &cameraScript_t774_0_0_3/* type */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeU3Ec__Iterator5_t775, ___U3CU3Ef__this_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CshakeU3Ec__Iterator5_t775_FieldInfos[] =
{
	&U3CshakeU3Ec__Iterator5_t775____t_0_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U3CpU3E__0_1_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U3CipU3E__1_2_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U3Cip2U3E__2_3_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U24PC_4_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U24current_5_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U3CU24U3Et_6_FieldInfo,
	&U3CshakeU3Ec__Iterator5_t775____U3CU3Ef__this_7_FieldInfo,
	NULL
};
extern MethodInfo U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335_MethodInfo;
static PropertyInfo U3CshakeU3Ec__Iterator5_t775____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336_MethodInfo;
static PropertyInfo U3CshakeU3Ec__Iterator5_t775____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CshakeU3Ec__Iterator5_t775_PropertyInfos[] =
{
	&U3CshakeU3Ec__Iterator5_t775____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CshakeU3Ec__Iterator5_t775____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
extern MethodInfo U3CshakeU3Ec__Iterator5_Dispose_m3338_MethodInfo;
extern MethodInfo U3CshakeU3Ec__Iterator5_MoveNext_m3337_MethodInfo;
extern MethodInfo U3CshakeU3Ec__Iterator5_Reset_m3339_MethodInfo;
static Il2CppMethodReference U3CshakeU3Ec__Iterator5_t775_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CshakeU3Ec__Iterator5_Dispose_m3338_MethodInfo,
	&U3CshakeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3336_MethodInfo,
	&U3CshakeU3Ec__Iterator5_MoveNext_m3337_MethodInfo,
	&U3CshakeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3335_MethodInfo,
	&U3CshakeU3Ec__Iterator5_Reset_m3339_MethodInfo,
};
static bool U3CshakeU3Ec__Iterator5_t775_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IDisposable_t191_0_0_0;
extern Il2CppType IEnumerator_t37_0_0_0;
extern Il2CppType IEnumerator_1_t166_0_0_0;
static const Il2CppType* U3CshakeU3Ec__Iterator5_t775_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CshakeU3Ec__Iterator5_t775_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CshakeU3Ec__Iterator5_t775_0_0_0;
extern Il2CppType U3CshakeU3Ec__Iterator5_t775_1_0_0;
extern Il2CppType Object_t_0_0_0;
extern TypeInfo cameraScript_t774_il2cpp_TypeInfo;
extern Il2CppType cameraScript_t774_0_0_0;
struct U3CshakeU3Ec__Iterator5_t775;
const Il2CppTypeDefinitionMetadata U3CshakeU3Ec__Iterator5_t775_DefinitionMetadata = 
{
	&cameraScript_t774_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CshakeU3Ec__Iterator5_t775_InterfacesTypeInfos/* implementedInterfaces */
	, U3CshakeU3Ec__Iterator5_t775_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CshakeU3Ec__Iterator5_t775_VTable/* vtableMethods */
	, U3CshakeU3Ec__Iterator5_t775_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<shake>c__Iterator5"/* name */
	, ""/* namespaze */
	, U3CshakeU3Ec__Iterator5_t775_MethodInfos/* methods */
	, U3CshakeU3Ec__Iterator5_t775_PropertyInfos/* properties */
	, U3CshakeU3Ec__Iterator5_t775_FieldInfos/* fields */
	, NULL/* events */
	, &U3CshakeU3Ec__Iterator5_t775_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 246/* custom_attributes_cache */
	, &U3CshakeU3Ec__Iterator5_t775_0_0_0/* byval_arg */
	, &U3CshakeU3Ec__Iterator5_t775_1_0_0/* this_arg */
	, &U3CshakeU3Ec__Iterator5_t775_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CshakeU3Ec__Iterator5_t775)/* instance_size */
	, sizeof (U3CshakeU3Ec__Iterator5_t775)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// cameraScript/<shakeSmall>c__Iterator6
#include "AssemblyU2DCSharp_cameraScript_U3CshakeSmallU3Ec__Iterator6.h"
// Metadata Definition cameraScript/<shakeSmall>c__Iterator6
extern TypeInfo U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo;
// cameraScript/<shakeSmall>c__Iterator6
#include "AssemblyU2DCSharp_cameraScript_U3CshakeSmallU3Ec__Iterator6MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shakeSmall>c__Iterator6::.ctor()
MethodInfo U3CshakeSmallU3Ec__Iterator6__ctor_m3340_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6__ctor_m3340/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object cameraScript/<shakeSmall>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 252/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object cameraScript/<shakeSmall>c__Iterator6::System.Collections.IEnumerator.get_Current()
MethodInfo U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 253/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean cameraScript/<shakeSmall>c__Iterator6::MoveNext()
MethodInfo U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shakeSmall>c__Iterator6::Dispose()
MethodInfo U3CshakeSmallU3Ec__Iterator6_Dispose_m3344_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6_Dispose_m3344/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 254/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript/<shakeSmall>c__Iterator6::Reset()
MethodInfo U3CshakeSmallU3Ec__Iterator6_Reset_m3345_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CshakeSmallU3Ec__Iterator6_Reset_m3345/* method */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 255/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CshakeSmallU3Ec__Iterator6_t776_MethodInfos[] =
{
	&U3CshakeSmallU3Ec__Iterator6__ctor_m3340_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_Dispose_m3344_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_Reset_m3345_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____t_0_FieldInfo = 
{
	"t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___t_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U3CpU3E__0_1_FieldInfo = 
{
	"<p>__0"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U3CpU3E__0_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U3CipU3E__1_2_FieldInfo = 
{
	"<ip>__1"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U3CipU3E__1_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U3Cip2U3E__2_3_FieldInfo = 
{
	"<ip2>__2"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U3Cip2U3E__2_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U24PC_4_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U24PC_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U24current_5_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U24current_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U3CU24U3Et_6_FieldInfo = 
{
	"<$>t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U3CU24U3Et_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType cameraScript_t774_0_0_3;
FieldInfo U3CshakeSmallU3Ec__Iterator6_t776____U3CU3Ef__this_7_FieldInfo = 
{
	"<>f__this"/* name */
	, &cameraScript_t774_0_0_3/* type */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, offsetof(U3CshakeSmallU3Ec__Iterator6_t776, ___U3CU3Ef__this_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CshakeSmallU3Ec__Iterator6_t776_FieldInfos[] =
{
	&U3CshakeSmallU3Ec__Iterator6_t776____t_0_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U3CpU3E__0_1_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U3CipU3E__1_2_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U3Cip2U3E__2_3_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U24PC_4_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U24current_5_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U3CU24U3Et_6_FieldInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____U3CU3Ef__this_7_FieldInfo,
	NULL
};
extern MethodInfo U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341_MethodInfo;
static PropertyInfo U3CshakeSmallU3Ec__Iterator6_t776____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342_MethodInfo;
static PropertyInfo U3CshakeSmallU3Ec__Iterator6_t776____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CshakeSmallU3Ec__Iterator6_t776_PropertyInfos[] =
{
	&U3CshakeSmallU3Ec__Iterator6_t776____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CshakeSmallU3Ec__Iterator6_t776____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CshakeSmallU3Ec__Iterator6_Dispose_m3344_MethodInfo;
extern MethodInfo U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343_MethodInfo;
extern MethodInfo U3CshakeSmallU3Ec__Iterator6_Reset_m3345_MethodInfo;
static Il2CppMethodReference U3CshakeSmallU3Ec__Iterator6_t776_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_Dispose_m3344_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m3342_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_MoveNext_m3343_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3341_MethodInfo,
	&U3CshakeSmallU3Ec__Iterator6_Reset_m3345_MethodInfo,
};
static bool U3CshakeSmallU3Ec__Iterator6_t776_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CshakeSmallU3Ec__Iterator6_t776_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CshakeSmallU3Ec__Iterator6_t776_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CshakeSmallU3Ec__Iterator6_t776_0_0_0;
extern Il2CppType U3CshakeSmallU3Ec__Iterator6_t776_1_0_0;
struct U3CshakeSmallU3Ec__Iterator6_t776;
const Il2CppTypeDefinitionMetadata U3CshakeSmallU3Ec__Iterator6_t776_DefinitionMetadata = 
{
	&cameraScript_t774_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CshakeSmallU3Ec__Iterator6_t776_InterfacesTypeInfos/* implementedInterfaces */
	, U3CshakeSmallU3Ec__Iterator6_t776_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CshakeSmallU3Ec__Iterator6_t776_VTable/* vtableMethods */
	, U3CshakeSmallU3Ec__Iterator6_t776_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<shakeSmall>c__Iterator6"/* name */
	, ""/* namespaze */
	, U3CshakeSmallU3Ec__Iterator6_t776_MethodInfos/* methods */
	, U3CshakeSmallU3Ec__Iterator6_t776_PropertyInfos/* properties */
	, U3CshakeSmallU3Ec__Iterator6_t776_FieldInfos/* fields */
	, NULL/* events */
	, &U3CshakeSmallU3Ec__Iterator6_t776_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 251/* custom_attributes_cache */
	, &U3CshakeSmallU3Ec__Iterator6_t776_0_0_0/* byval_arg */
	, &U3CshakeSmallU3Ec__Iterator6_t776_1_0_0/* this_arg */
	, &U3CshakeSmallU3Ec__Iterator6_t776_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CshakeSmallU3Ec__Iterator6_t776)/* instance_size */
	, sizeof (U3CshakeSmallU3Ec__Iterator6_t776)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// cameraScript
#include "AssemblyU2DCSharp_cameraScript.h"
// Metadata Definition cameraScript
// cameraScript
#include "AssemblyU2DCSharp_cameraScriptMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript::.ctor()
MethodInfo cameraScript__ctor_m3346_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&cameraScript__ctor_m3346/* method */
	, &cameraScript_t774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void cameraScript::Start()
MethodInfo cameraScript_Start_m3347_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&cameraScript_Start_m3347/* method */
	, &cameraScript_t774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo cameraScript_t774_cameraScript_shake_m3348_ParameterInfos[] = 
{
	{"t", 0, 134220470, 0, &Single_t202_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator cameraScript::shake(System.Single)
MethodInfo cameraScript_shake_m3348_MethodInfo = 
{
	"shake"/* name */
	, (methodPointerType)&cameraScript_shake_m3348/* method */
	, &cameraScript_t774_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t202/* invoker_method */
	, cameraScript_t774_cameraScript_shake_m3348_ParameterInfos/* parameters */
	, 244/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo cameraScript_t774_cameraScript_shakeSmall_m3349_ParameterInfos[] = 
{
	{"t", 0, 134220471, 0, &Single_t202_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator cameraScript::shakeSmall(System.Single)
MethodInfo cameraScript_shakeSmall_m3349_MethodInfo = 
{
	"shakeSmall"/* name */
	, (methodPointerType)&cameraScript_shakeSmall_m3349/* method */
	, &cameraScript_t774_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t202/* invoker_method */
	, cameraScript_t774_cameraScript_shakeSmall_m3349_ParameterInfos/* parameters */
	, 245/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* cameraScript_t774_MethodInfos[] =
{
	&cameraScript__ctor_m3346_MethodInfo,
	&cameraScript_Start_m3347_MethodInfo,
	&cameraScript_shake_m3348_MethodInfo,
	&cameraScript_shakeSmall_m3349_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_22;
FieldInfo cameraScript_t774____initOrthoSize_2_FieldInfo = 
{
	"initOrthoSize"/* name */
	, &Single_t202_0_0_22/* type */
	, &cameraScript_t774_il2cpp_TypeInfo/* parent */
	, offsetof(cameraScript_t774_StaticFields, ___initOrthoSize_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* cameraScript_t774_FieldInfos[] =
{
	&cameraScript_t774____initOrthoSize_2_FieldInfo,
	NULL
};
static const Il2CppType* cameraScript_t774_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CshakeU3Ec__Iterator5_t775_0_0_0,
	&U3CshakeSmallU3Ec__Iterator6_t776_0_0_0,
};
static Il2CppMethodReference cameraScript_t774_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool cameraScript_t774_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType cameraScript_t774_1_0_0;
struct cameraScript_t774;
const Il2CppTypeDefinitionMetadata cameraScript_t774_DefinitionMetadata = 
{
	NULL/* declaringType */
	, cameraScript_t774_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, cameraScript_t774_VTable/* vtableMethods */
	, cameraScript_t774_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo cameraScript_t774_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "cameraScript"/* name */
	, ""/* namespaze */
	, cameraScript_t774_MethodInfos/* methods */
	, NULL/* properties */
	, cameraScript_t774_FieldInfos/* fields */
	, NULL/* events */
	, &cameraScript_t774_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &cameraScript_t774_0_0_0/* byval_arg */
	, &cameraScript_t774_1_0_0/* this_arg */
	, &cameraScript_t774_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (cameraScript_t774)/* instance_size */
	, sizeof (cameraScript_t774)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(cameraScript_t774_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// circCont
#include "AssemblyU2DCSharp_circCont.h"
// Metadata Definition circCont
extern TypeInfo circCont_t777_il2cpp_TypeInfo;
// circCont
#include "AssemblyU2DCSharp_circContMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circCont::.ctor()
MethodInfo circCont__ctor_m3350_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&circCont__ctor_m3350/* method */
	, &circCont_t777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circCont::Start()
MethodInfo circCont_Start_m3351_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&circCont_Start_m3351/* method */
	, &circCont_t777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circCont::Update()
MethodInfo circCont_Update_m3352_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&circCont_Update_m3352/* method */
	, &circCont_t777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circCont::destroyCirculares()
MethodInfo circCont_destroyCirculares_m3353_MethodInfo = 
{
	"destroyCirculares"/* name */
	, (methodPointerType)&circCont_destroyCirculares_m3353/* method */
	, &circCont_t777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* circCont_t777_MethodInfos[] =
{
	&circCont__ctor_m3350_MethodInfo,
	&circCont_Start_m3351_MethodInfo,
	&circCont_Update_m3352_MethodInfo,
	&circCont_destroyCirculares_m3353_MethodInfo,
	NULL
};
extern Il2CppType Vector3_t758_0_0_6;
FieldInfo circCont_t777____tra_2_FieldInfo = 
{
	"tra"/* name */
	, &Vector3_t758_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___tra_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_6;
FieldInfo circCont_t777____rot_3_FieldInfo = 
{
	"rot"/* name */
	, &Vector3_t758_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___rot_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo circCont_t777____traV_4_FieldInfo = 
{
	"traV"/* name */
	, &Single_t202_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___traV_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo circCont_t777____rotV_5_FieldInfo = 
{
	"rotV"/* name */
	, &Single_t202_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___rotV_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circCont_t777____circular_6_FieldInfo = 
{
	"circular"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___circular_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_6;
FieldInfo circCont_t777____abec_7_FieldInfo = 
{
	"abec"/* name */
	, &ArrayList_t737_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___abec_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo circCont_t777____dia_8_FieldInfo = 
{
	"dia"/* name */
	, &Int32_t189_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___dia_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo circCont_t777____len_9_FieldInfo = 
{
	"len"/* name */
	, &Single_t202_0_0_6/* type */
	, &circCont_t777_il2cpp_TypeInfo/* parent */
	, offsetof(circCont_t777, ___len_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* circCont_t777_FieldInfos[] =
{
	&circCont_t777____tra_2_FieldInfo,
	&circCont_t777____rot_3_FieldInfo,
	&circCont_t777____traV_4_FieldInfo,
	&circCont_t777____rotV_5_FieldInfo,
	&circCont_t777____circular_6_FieldInfo,
	&circCont_t777____abec_7_FieldInfo,
	&circCont_t777____dia_8_FieldInfo,
	&circCont_t777____len_9_FieldInfo,
	NULL
};
static Il2CppMethodReference circCont_t777_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool circCont_t777_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType circCont_t777_0_0_0;
extern Il2CppType circCont_t777_1_0_0;
struct circCont_t777;
const Il2CppTypeDefinitionMetadata circCont_t777_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, circCont_t777_VTable/* vtableMethods */
	, circCont_t777_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo circCont_t777_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "circCont"/* name */
	, ""/* namespaze */
	, circCont_t777_MethodInfos/* methods */
	, NULL/* properties */
	, circCont_t777_FieldInfos/* fields */
	, NULL/* events */
	, &circCont_t777_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &circCont_t777_0_0_0/* byval_arg */
	, &circCont_t777_1_0_0/* this_arg */
	, &circCont_t777_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (circCont_t777)/* instance_size */
	, sizeof (circCont_t777)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// circularScr
#include "AssemblyU2DCSharp_circularScr.h"
// Metadata Definition circularScr
extern TypeInfo circularScr_t778_il2cpp_TypeInfo;
// circularScr
#include "AssemblyU2DCSharp_circularScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::.ctor()
MethodInfo circularScr__ctor_m3354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&circularScr__ctor_m3354/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::Start()
MethodInfo circularScr_Start_m3355_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&circularScr_Start_m3355/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::Update()
MethodInfo circularScr_Update_m3356_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&circularScr_Update_m3356/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::OnExplosion()
MethodInfo circularScr_OnExplosion_m3357_MethodInfo = 
{
	"OnExplosion"/* name */
	, (methodPointerType)&circularScr_OnExplosion_m3357/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Collider_t900_0_0_0;
extern Il2CppType Collider_t900_0_0_0;
static ParameterInfo circularScr_t778_circularScr_OnTriggerEnter_m3358_ParameterInfos[] = 
{
	{"other", 0, 134220472, 0, &Collider_t900_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::OnTriggerEnter(UnityEngine.Collider)
MethodInfo circularScr_OnTriggerEnter_m3358_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&circularScr_OnTriggerEnter_m3358/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, circularScr_t778_circularScr_OnTriggerEnter_m3358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void circularScr::leavePow()
MethodInfo circularScr_leavePow_m3359_MethodInfo = 
{
	"leavePow"/* name */
	, (methodPointerType)&circularScr_leavePow_m3359/* method */
	, &circularScr_t778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* circularScr_t778_MethodInfos[] =
{
	&circularScr__ctor_m3354_MethodInfo,
	&circularScr_Start_m3355_MethodInfo,
	&circularScr_Update_m3356_MethodInfo,
	&circularScr_OnExplosion_m3357_MethodInfo,
	&circularScr_OnTriggerEnter_m3358_MethodInfo,
	&circularScr_leavePow_m3359_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____enemyController_2_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___enemyController_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____paquete_3_FieldInfo = 
{
	"paquete"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___paquete_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____coin_4_FieldInfo = 
{
	"coin"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___coin_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____movingText_5_FieldInfo = 
{
	"movingText"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___movingText_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____movingTextKills_6_FieldInfo = 
{
	"movingTextKills"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___movingTextKills_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____explosion_7_FieldInfo = 
{
	"explosion"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___explosion_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____circularControler_8_FieldInfo = 
{
	"circularControler"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___circularControler_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo circularScr_t778____center_9_FieldInfo = 
{
	"center"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___center_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo circularScr_t778____radius_10_FieldInfo = 
{
	"radius"/* name */
	, &Single_t202_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___radius_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo circularScr_t778____release_11_FieldInfo = 
{
	"release"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___release_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____explosionDelayed_12_FieldInfo = 
{
	"explosionDelayed"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___explosionDelayed_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____dust_13_FieldInfo = 
{
	"dust"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___dust_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo circularScr_t778____randomExplosion_14_FieldInfo = 
{
	"randomExplosion"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &circularScr_t778_il2cpp_TypeInfo/* parent */
	, offsetof(circularScr_t778, ___randomExplosion_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* circularScr_t778_FieldInfos[] =
{
	&circularScr_t778____enemyController_2_FieldInfo,
	&circularScr_t778____paquete_3_FieldInfo,
	&circularScr_t778____coin_4_FieldInfo,
	&circularScr_t778____movingText_5_FieldInfo,
	&circularScr_t778____movingTextKills_6_FieldInfo,
	&circularScr_t778____explosion_7_FieldInfo,
	&circularScr_t778____circularControler_8_FieldInfo,
	&circularScr_t778____center_9_FieldInfo,
	&circularScr_t778____radius_10_FieldInfo,
	&circularScr_t778____release_11_FieldInfo,
	&circularScr_t778____explosionDelayed_12_FieldInfo,
	&circularScr_t778____dust_13_FieldInfo,
	&circularScr_t778____randomExplosion_14_FieldInfo,
	NULL
};
static Il2CppMethodReference circularScr_t778_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool circularScr_t778_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType circularScr_t778_0_0_0;
extern Il2CppType circularScr_t778_1_0_0;
struct circularScr_t778;
const Il2CppTypeDefinitionMetadata circularScr_t778_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, circularScr_t778_VTable/* vtableMethods */
	, circularScr_t778_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo circularScr_t778_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "circularScr"/* name */
	, ""/* namespaze */
	, circularScr_t778_MethodInfos/* methods */
	, NULL/* properties */
	, circularScr_t778_FieldInfos/* fields */
	, NULL/* events */
	, &circularScr_t778_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &circularScr_t778_0_0_0/* byval_arg */
	, &circularScr_t778_1_0_0/* this_arg */
	, &circularScr_t778_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (circularScr_t778)/* instance_size */
	, sizeof (circularScr_t778)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// collideAndDie
#include "AssemblyU2DCSharp_collideAndDie.h"
// Metadata Definition collideAndDie
extern TypeInfo collideAndDie_t779_il2cpp_TypeInfo;
// collideAndDie
#include "AssemblyU2DCSharp_collideAndDieMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void collideAndDie::.ctor()
MethodInfo collideAndDie__ctor_m3360_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&collideAndDie__ctor_m3360/* method */
	, &collideAndDie_t779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void collideAndDie::OnExplosion()
MethodInfo collideAndDie_OnExplosion_m3361_MethodInfo = 
{
	"OnExplosion"/* name */
	, (methodPointerType)&collideAndDie_OnExplosion_m3361/* method */
	, &collideAndDie_t779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Collider_t900_0_0_0;
static ParameterInfo collideAndDie_t779_collideAndDie_OnTriggerEnter_m3362_ParameterInfos[] = 
{
	{"other", 0, 134220473, 0, &Collider_t900_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void collideAndDie::OnTriggerEnter(UnityEngine.Collider)
MethodInfo collideAndDie_OnTriggerEnter_m3362_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&collideAndDie_OnTriggerEnter_m3362/* method */
	, &collideAndDie_t779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, collideAndDie_t779_collideAndDie_OnTriggerEnter_m3362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* collideAndDie_t779_MethodInfos[] =
{
	&collideAndDie__ctor_m3360_MethodInfo,
	&collideAndDie_OnExplosion_m3361_MethodInfo,
	&collideAndDie_OnTriggerEnter_m3362_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo collideAndDie_t779____explosionBlue_2_FieldInfo = 
{
	"explosionBlue"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &collideAndDie_t779_il2cpp_TypeInfo/* parent */
	, offsetof(collideAndDie_t779, ___explosionBlue_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* collideAndDie_t779_FieldInfos[] =
{
	&collideAndDie_t779____explosionBlue_2_FieldInfo,
	NULL
};
static Il2CppMethodReference collideAndDie_t779_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool collideAndDie_t779_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType collideAndDie_t779_0_0_0;
extern Il2CppType collideAndDie_t779_1_0_0;
struct collideAndDie_t779;
const Il2CppTypeDefinitionMetadata collideAndDie_t779_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, collideAndDie_t779_VTable/* vtableMethods */
	, collideAndDie_t779_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo collideAndDie_t779_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "collideAndDie"/* name */
	, ""/* namespaze */
	, collideAndDie_t779_MethodInfos/* methods */
	, NULL/* properties */
	, collideAndDie_t779_FieldInfos/* fields */
	, NULL/* events */
	, &collideAndDie_t779_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &collideAndDie_t779_0_0_0/* byval_arg */
	, &collideAndDie_t779_1_0_0/* this_arg */
	, &collideAndDie_t779_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (collideAndDie_t779)/* instance_size */
	, sizeof (collideAndDie_t779)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// copyPlayerPosition
#include "AssemblyU2DCSharp_copyPlayerPosition.h"
// Metadata Definition copyPlayerPosition
extern TypeInfo copyPlayerPosition_t780_il2cpp_TypeInfo;
// copyPlayerPosition
#include "AssemblyU2DCSharp_copyPlayerPositionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void copyPlayerPosition::.ctor()
MethodInfo copyPlayerPosition__ctor_m3363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&copyPlayerPosition__ctor_m3363/* method */
	, &copyPlayerPosition_t780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void copyPlayerPosition::Update()
MethodInfo copyPlayerPosition_Update_m3364_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&copyPlayerPosition_Update_m3364/* method */
	, &copyPlayerPosition_t780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* copyPlayerPosition_t780_MethodInfos[] =
{
	&copyPlayerPosition__ctor_m3363_MethodInfo,
	&copyPlayerPosition_Update_m3364_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo copyPlayerPosition_t780____p_2_FieldInfo = 
{
	"p"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &copyPlayerPosition_t780_il2cpp_TypeInfo/* parent */
	, offsetof(copyPlayerPosition_t780, ___p_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* copyPlayerPosition_t780_FieldInfos[] =
{
	&copyPlayerPosition_t780____p_2_FieldInfo,
	NULL
};
static Il2CppMethodReference copyPlayerPosition_t780_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool copyPlayerPosition_t780_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType copyPlayerPosition_t780_0_0_0;
extern Il2CppType copyPlayerPosition_t780_1_0_0;
struct copyPlayerPosition_t780;
const Il2CppTypeDefinitionMetadata copyPlayerPosition_t780_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, copyPlayerPosition_t780_VTable/* vtableMethods */
	, copyPlayerPosition_t780_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo copyPlayerPosition_t780_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "copyPlayerPosition"/* name */
	, ""/* namespaze */
	, copyPlayerPosition_t780_MethodInfos/* methods */
	, NULL/* properties */
	, copyPlayerPosition_t780_FieldInfos/* fields */
	, NULL/* events */
	, &copyPlayerPosition_t780_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &copyPlayerPosition_t780_0_0_0/* byval_arg */
	, &copyPlayerPosition_t780_1_0_0/* this_arg */
	, &copyPlayerPosition_t780_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (copyPlayerPosition_t780)/* instance_size */
	, sizeof (copyPlayerPosition_t780)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// culobichoScr
#include "AssemblyU2DCSharp_culobichoScr.h"
// Metadata Definition culobichoScr
extern TypeInfo culobichoScr_t782_il2cpp_TypeInfo;
// culobichoScr
#include "AssemblyU2DCSharp_culobichoScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void culobichoScr::.ctor()
MethodInfo culobichoScr__ctor_m3365_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&culobichoScr__ctor_m3365/* method */
	, &culobichoScr_t782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void culobichoScr::Start()
MethodInfo culobichoScr_Start_m3366_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&culobichoScr_Start_m3366/* method */
	, &culobichoScr_t782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void culobichoScr::Update()
MethodInfo culobichoScr_Update_m3367_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&culobichoScr_Update_m3367/* method */
	, &culobichoScr_t782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* culobichoScr_t782_MethodInfos[] =
{
	&culobichoScr__ctor_m3365_MethodInfo,
	&culobichoScr_Start_m3366_MethodInfo,
	&culobichoScr_Update_m3367_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo culobichoScr_t782____target_2_FieldInfo = 
{
	"target"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &culobichoScr_t782_il2cpp_TypeInfo/* parent */
	, offsetof(culobichoScr_t782, ___target_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType headScr_t781_0_0_1;
FieldInfo culobichoScr_t782_____target_3_FieldInfo = 
{
	"_target"/* name */
	, &headScr_t781_0_0_1/* type */
	, &culobichoScr_t782_il2cpp_TypeInfo/* parent */
	, offsetof(culobichoScr_t782, ____target_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo culobichoScr_t782____maxSpeed_4_FieldInfo = 
{
	"maxSpeed"/* name */
	, &Single_t202_0_0_6/* type */
	, &culobichoScr_t782_il2cpp_TypeInfo/* parent */
	, offsetof(culobichoScr_t782, ___maxSpeed_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo culobichoScr_t782____maxAngleSpeed_5_FieldInfo = 
{
	"maxAngleSpeed"/* name */
	, &Single_t202_0_0_6/* type */
	, &culobichoScr_t782_il2cpp_TypeInfo/* parent */
	, offsetof(culobichoScr_t782, ___maxAngleSpeed_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* culobichoScr_t782_FieldInfos[] =
{
	&culobichoScr_t782____target_2_FieldInfo,
	&culobichoScr_t782_____target_3_FieldInfo,
	&culobichoScr_t782____maxSpeed_4_FieldInfo,
	&culobichoScr_t782____maxAngleSpeed_5_FieldInfo,
	NULL
};
static Il2CppMethodReference culobichoScr_t782_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool culobichoScr_t782_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType culobichoScr_t782_0_0_0;
extern Il2CppType culobichoScr_t782_1_0_0;
struct culobichoScr_t782;
const Il2CppTypeDefinitionMetadata culobichoScr_t782_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, culobichoScr_t782_VTable/* vtableMethods */
	, culobichoScr_t782_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo culobichoScr_t782_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "culobichoScr"/* name */
	, ""/* namespaze */
	, culobichoScr_t782_MethodInfos/* methods */
	, NULL/* properties */
	, culobichoScr_t782_FieldInfos/* fields */
	, NULL/* events */
	, &culobichoScr_t782_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &culobichoScr_t782_0_0_0/* byval_arg */
	, &culobichoScr_t782_1_0_0/* this_arg */
	, &culobichoScr_t782_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (culobichoScr_t782)/* instance_size */
	, sizeof (culobichoScr_t782)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// destroyOnCollision
#include "AssemblyU2DCSharp_destroyOnCollision.h"
// Metadata Definition destroyOnCollision
extern TypeInfo destroyOnCollision_t783_il2cpp_TypeInfo;
// destroyOnCollision
#include "AssemblyU2DCSharp_destroyOnCollisionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void destroyOnCollision::.ctor()
MethodInfo destroyOnCollision__ctor_m3368_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&destroyOnCollision__ctor_m3368/* method */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void destroyOnCollision::Start()
MethodInfo destroyOnCollision_Start_m3369_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&destroyOnCollision_Start_m3369/* method */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void destroyOnCollision::Update()
MethodInfo destroyOnCollision_Update_m3370_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&destroyOnCollision_Update_m3370/* method */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Collider_t900_0_0_0;
static ParameterInfo destroyOnCollision_t783_destroyOnCollision_OnTriggerEnter_m3371_ParameterInfos[] = 
{
	{"other", 0, 134220474, 0, &Collider_t900_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void destroyOnCollision::OnTriggerEnter(UnityEngine.Collider)
MethodInfo destroyOnCollision_OnTriggerEnter_m3371_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&destroyOnCollision_OnTriggerEnter_m3371/* method */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, destroyOnCollision_t783_destroyOnCollision_OnTriggerEnter_m3371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* destroyOnCollision_t783_MethodInfos[] =
{
	&destroyOnCollision__ctor_m3368_MethodInfo,
	&destroyOnCollision_Start_m3369_MethodInfo,
	&destroyOnCollision_Update_m3370_MethodInfo,
	&destroyOnCollision_OnTriggerEnter_m3371_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo destroyOnCollision_t783____eC_2_FieldInfo = 
{
	"eC"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* parent */
	, offsetof(destroyOnCollision_t783, ___eC_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* destroyOnCollision_t783_FieldInfos[] =
{
	&destroyOnCollision_t783____eC_2_FieldInfo,
	NULL
};
static Il2CppMethodReference destroyOnCollision_t783_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool destroyOnCollision_t783_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType destroyOnCollision_t783_0_0_0;
extern Il2CppType destroyOnCollision_t783_1_0_0;
struct destroyOnCollision_t783;
const Il2CppTypeDefinitionMetadata destroyOnCollision_t783_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, destroyOnCollision_t783_VTable/* vtableMethods */
	, destroyOnCollision_t783_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo destroyOnCollision_t783_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "destroyOnCollision"/* name */
	, ""/* namespaze */
	, destroyOnCollision_t783_MethodInfos/* methods */
	, NULL/* properties */
	, destroyOnCollision_t783_FieldInfos/* fields */
	, NULL/* events */
	, &destroyOnCollision_t783_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &destroyOnCollision_t783_0_0_0/* byval_arg */
	, &destroyOnCollision_t783_1_0_0/* this_arg */
	, &destroyOnCollision_t783_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (destroyOnCollision_t783)/* instance_size */
	, sizeof (destroyOnCollision_t783)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// disparo
#include "AssemblyU2DCSharp_disparo.h"
// Metadata Definition disparo
extern TypeInfo disparo_t784_il2cpp_TypeInfo;
// disparo
#include "AssemblyU2DCSharp_disparoMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void disparo::.ctor()
MethodInfo disparo__ctor_m3372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&disparo__ctor_m3372/* method */
	, &disparo_t784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType enemyController_t785_0_0_0;
extern Il2CppType enemyController_t785_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
extern Il2CppType GameObject_t144_0_0_0;
static ParameterInfo disparo_t784_disparo_dispara_m3373_ParameterInfos[] = 
{
	{"_enemyController", 0, 134220475, 0, &enemyController_t785_0_0_0},
	{"bullet", 1, 134220476, 0, &GameObject_t144_0_0_0},
	{"bulletThin", 2, 134220477, 0, &GameObject_t144_0_0_0},
	{"cirController", 3, 134220478, 0, &GameObject_t144_0_0_0},
	{"bulletLaser", 4, 134220479, 0, &GameObject_t144_0_0_0},
	{"raycastObj", 5, 134220480, 0, &GameObject_t144_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void disparo::dispara(enemyController,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject)
MethodInfo disparo_dispara_m3373_MethodInfo = 
{
	"dispara"/* name */
	, (methodPointerType)&disparo_dispara_m3373/* method */
	, &disparo_t784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, disparo_t784_disparo_dispara_m3373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean disparo::isBullets()
MethodInfo disparo_isBullets_m3374_MethodInfo = 
{
	"isBullets"/* name */
	, (methodPointerType)&disparo_isBullets_m3374/* method */
	, &disparo_t784_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* disparo_t784_MethodInfos[] =
{
	&disparo__ctor_m3372_MethodInfo,
	&disparo_dispara_m3373_MethodInfo,
	&disparo_isBullets_m3374_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shootTime_2_FieldInfo = 
{
	"shootTime"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shootTime_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeNormal_3_FieldInfo = 
{
	"shotTimeNormal"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeNormal_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimewBock_4_FieldInfo = 
{
	"shotTimewBock"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimewBock_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeLaser_5_FieldInfo = 
{
	"shotTimeLaser"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeLaser_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeMoire_6_FieldInfo = 
{
	"shotTimeMoire"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeMoire_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeTWay_7_FieldInfo = 
{
	"shotTimeTWay"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeTWay_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeTri_8_FieldInfo = 
{
	"shotTimeTri"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeTri_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____shotTimeCircular_9_FieldInfo = 
{
	"shotTimeCircular"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotTimeCircular_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____largo_10_FieldInfo = 
{
	"largo"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___largo_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparo_t784____tLaser_11_FieldInfo = 
{
	"tLaser"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___tLaser_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo disparo_t784____isLaserState_12_FieldInfo = 
{
	"isLaserState"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___isLaserState_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo disparo_t784____shotForce_13_FieldInfo = 
{
	"shotForce"/* name */
	, &Single_t202_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___shotForce_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo disparo_t784____rayo_14_FieldInfo = 
{
	"rayo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &disparo_t784_il2cpp_TypeInfo/* parent */
	, offsetof(disparo_t784, ___rayo_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* disparo_t784_FieldInfos[] =
{
	&disparo_t784____shootTime_2_FieldInfo,
	&disparo_t784____shotTimeNormal_3_FieldInfo,
	&disparo_t784____shotTimewBock_4_FieldInfo,
	&disparo_t784____shotTimeLaser_5_FieldInfo,
	&disparo_t784____shotTimeMoire_6_FieldInfo,
	&disparo_t784____shotTimeTWay_7_FieldInfo,
	&disparo_t784____shotTimeTri_8_FieldInfo,
	&disparo_t784____shotTimeCircular_9_FieldInfo,
	&disparo_t784____largo_10_FieldInfo,
	&disparo_t784____tLaser_11_FieldInfo,
	&disparo_t784____isLaserState_12_FieldInfo,
	&disparo_t784____shotForce_13_FieldInfo,
	&disparo_t784____rayo_14_FieldInfo,
	NULL
};
static Il2CppMethodReference disparo_t784_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool disparo_t784_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType disparo_t784_0_0_0;
extern Il2CppType disparo_t784_1_0_0;
struct disparo_t784;
const Il2CppTypeDefinitionMetadata disparo_t784_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, disparo_t784_VTable/* vtableMethods */
	, disparo_t784_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo disparo_t784_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "disparo"/* name */
	, ""/* namespaze */
	, disparo_t784_MethodInfos/* methods */
	, NULL/* properties */
	, disparo_t784_FieldInfos/* fields */
	, NULL/* events */
	, &disparo_t784_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &disparo_t784_0_0_0/* byval_arg */
	, &disparo_t784_1_0_0/* this_arg */
	, &disparo_t784_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (disparo_t784)/* instance_size */
	, sizeof (disparo_t784)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// disparoProxy
#include "AssemblyU2DCSharp_disparoProxy.h"
// Metadata Definition disparoProxy
extern TypeInfo disparoProxy_t786_il2cpp_TypeInfo;
// disparoProxy
#include "AssemblyU2DCSharp_disparoProxyMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void disparoProxy::.ctor()
MethodInfo disparoProxy__ctor_m3375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&disparoProxy__ctor_m3375/* method */
	, &disparoProxy_t786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void disparoProxy::Start()
MethodInfo disparoProxy_Start_m3376_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&disparoProxy_Start_m3376/* method */
	, &disparoProxy_t786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void disparoProxy::dispara()
MethodInfo disparoProxy_dispara_m3377_MethodInfo = 
{
	"dispara"/* name */
	, (methodPointerType)&disparoProxy_dispara_m3377/* method */
	, &disparoProxy_t786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* disparoProxy_t786_MethodInfos[] =
{
	&disparoProxy__ctor_m3375_MethodInfo,
	&disparoProxy_Start_m3376_MethodInfo,
	&disparoProxy_dispara_m3377_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparoProxy_t786____shootTime_2_FieldInfo = 
{
	"shootTime"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ___shootTime_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo disparoProxy_t786____shotTimeNormal_3_FieldInfo = 
{
	"shotTimeNormal"/* name */
	, &Int32_t189_0_0_6/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ___shotTimeNormal_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo disparoProxy_t786____shotForce_4_FieldInfo = 
{
	"shotForce"/* name */
	, &Single_t202_0_0_6/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ___shotForce_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo disparoProxy_t786____bulletThin_5_FieldInfo = 
{
	"bulletThin"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ___bulletThin_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo disparoProxy_t786____enemyController_6_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ___enemyController_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyController_t785_0_0_1;
FieldInfo disparoProxy_t786_____enemyController_7_FieldInfo = 
{
	"_enemyController"/* name */
	, &enemyController_t785_0_0_1/* type */
	, &disparoProxy_t786_il2cpp_TypeInfo/* parent */
	, offsetof(disparoProxy_t786, ____enemyController_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* disparoProxy_t786_FieldInfos[] =
{
	&disparoProxy_t786____shootTime_2_FieldInfo,
	&disparoProxy_t786____shotTimeNormal_3_FieldInfo,
	&disparoProxy_t786____shotForce_4_FieldInfo,
	&disparoProxy_t786____bulletThin_5_FieldInfo,
	&disparoProxy_t786____enemyController_6_FieldInfo,
	&disparoProxy_t786_____enemyController_7_FieldInfo,
	NULL
};
static Il2CppMethodReference disparoProxy_t786_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool disparoProxy_t786_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType disparoProxy_t786_0_0_0;
extern Il2CppType disparoProxy_t786_1_0_0;
struct disparoProxy_t786;
const Il2CppTypeDefinitionMetadata disparoProxy_t786_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, disparoProxy_t786_VTable/* vtableMethods */
	, disparoProxy_t786_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo disparoProxy_t786_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "disparoProxy"/* name */
	, ""/* namespaze */
	, disparoProxy_t786_MethodInfos/* methods */
	, NULL/* properties */
	, disparoProxy_t786_FieldInfos/* fields */
	, NULL/* events */
	, &disparoProxy_t786_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &disparoProxy_t786_0_0_0/* byval_arg */
	, &disparoProxy_t786_1_0_0/* this_arg */
	, &disparoProxy_t786_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (disparoProxy_t786)/* instance_size */
	, sizeof (disparoProxy_t786)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// dustScr
#include "AssemblyU2DCSharp_dustScr.h"
// Metadata Definition dustScr
extern TypeInfo dustScr_t787_il2cpp_TypeInfo;
// dustScr
#include "AssemblyU2DCSharp_dustScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void dustScr::.ctor()
MethodInfo dustScr__ctor_m3378_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&dustScr__ctor_m3378/* method */
	, &dustScr_t787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void dustScr::Start()
MethodInfo dustScr_Start_m3379_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&dustScr_Start_m3379/* method */
	, &dustScr_t787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void dustScr::Update()
MethodInfo dustScr_Update_m3380_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&dustScr_Update_m3380/* method */
	, &dustScr_t787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* dustScr_t787_MethodInfos[] =
{
	&dustScr__ctor_m3378_MethodInfo,
	&dustScr_Start_m3379_MethodInfo,
	&dustScr_Update_m3380_MethodInfo,
	NULL
};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s1_2_FieldInfo = 
{
	"s1"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s1_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s2_3_FieldInfo = 
{
	"s2"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s2_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s3_4_FieldInfo = 
{
	"s3"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s3_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s4_5_FieldInfo = 
{
	"s4"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s4_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s5_6_FieldInfo = 
{
	"s5"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s5_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s6_7_FieldInfo = 
{
	"s6"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s6_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s7_8_FieldInfo = 
{
	"s7"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s7_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s8_9_FieldInfo = 
{
	"s8"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s8_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s9_10_FieldInfo = 
{
	"s9"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s9_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s10_11_FieldInfo = 
{
	"s10"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s10_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Sprite_t766_0_0_6;
FieldInfo dustScr_t787____s11_12_FieldInfo = 
{
	"s11"/* name */
	, &Sprite_t766_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___s11_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_6;
FieldInfo dustScr_t787____sprites_13_FieldInfo = 
{
	"sprites"/* name */
	, &ArrayList_t737_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___sprites_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SpriteRenderer_t744_0_0_1;
FieldInfo dustScr_t787____r_14_FieldInfo = 
{
	"r"/* name */
	, &SpriteRenderer_t744_0_0_1/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___r_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo dustScr_t787____lifeTime_15_FieldInfo = 
{
	"lifeTime"/* name */
	, &Single_t202_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___lifeTime_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo dustScr_t787____offset_16_FieldInfo = 
{
	"offset"/* name */
	, &Single_t202_0_0_6/* type */
	, &dustScr_t787_il2cpp_TypeInfo/* parent */
	, offsetof(dustScr_t787, ___offset_16)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* dustScr_t787_FieldInfos[] =
{
	&dustScr_t787____s1_2_FieldInfo,
	&dustScr_t787____s2_3_FieldInfo,
	&dustScr_t787____s3_4_FieldInfo,
	&dustScr_t787____s4_5_FieldInfo,
	&dustScr_t787____s5_6_FieldInfo,
	&dustScr_t787____s6_7_FieldInfo,
	&dustScr_t787____s7_8_FieldInfo,
	&dustScr_t787____s8_9_FieldInfo,
	&dustScr_t787____s9_10_FieldInfo,
	&dustScr_t787____s10_11_FieldInfo,
	&dustScr_t787____s11_12_FieldInfo,
	&dustScr_t787____sprites_13_FieldInfo,
	&dustScr_t787____r_14_FieldInfo,
	&dustScr_t787____lifeTime_15_FieldInfo,
	&dustScr_t787____offset_16_FieldInfo,
	NULL
};
static Il2CppMethodReference dustScr_t787_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool dustScr_t787_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType dustScr_t787_0_0_0;
extern Il2CppType dustScr_t787_1_0_0;
struct dustScr_t787;
const Il2CppTypeDefinitionMetadata dustScr_t787_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, dustScr_t787_VTable/* vtableMethods */
	, dustScr_t787_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo dustScr_t787_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "dustScr"/* name */
	, ""/* namespaze */
	, dustScr_t787_MethodInfos/* methods */
	, NULL/* properties */
	, dustScr_t787_FieldInfos/* fields */
	, NULL/* events */
	, &dustScr_t787_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &dustScr_t787_0_0_0/* byval_arg */
	, &dustScr_t787_1_0_0/* this_arg */
	, &dustScr_t787_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (dustScr_t787)/* instance_size */
	, sizeof (dustScr_t787)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// enemyController
#include "AssemblyU2DCSharp_enemyController.h"
// Metadata Definition enemyController
extern TypeInfo enemyController_t785_il2cpp_TypeInfo;
// enemyController
#include "AssemblyU2DCSharp_enemyControllerMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::.ctor()
MethodInfo enemyController__ctor_m3381_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&enemyController__ctor_m3381/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::.cctor()
MethodInfo enemyController__cctor_m3382_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&enemyController__cctor_m3382/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::Awake()
MethodInfo enemyController_Awake_m3383_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&enemyController_Awake_m3383/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::initHolesTutorial()
MethodInfo enemyController_initHolesTutorial_m3384_MethodInfo = 
{
	"initHolesTutorial"/* name */
	, (methodPointerType)&enemyController_initHolesTutorial_m3384/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::initHolesGame()
MethodInfo enemyController_initHolesGame_m3385_MethodInfo = 
{
	"initHolesGame"/* name */
	, (methodPointerType)&enemyController_initHolesGame_m3385/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::updatePosHoles()
MethodInfo enemyController_updatePosHoles_m3386_MethodInfo = 
{
	"updatePosHoles"/* name */
	, (methodPointerType)&enemyController_updatePosHoles_m3386/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::moveEnemies()
MethodInfo enemyController_moveEnemies_m3387_MethodInfo = 
{
	"moveEnemies"/* name */
	, (methodPointerType)&enemyController_moveEnemies_m3387/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::deleteHoles()
MethodInfo enemyController_deleteHoles_m3388_MethodInfo = 
{
	"deleteHoles"/* name */
	, (methodPointerType)&enemyController_deleteHoles_m3388/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::createSpider()
MethodInfo enemyController_createSpider_m3389_MethodInfo = 
{
	"createSpider"/* name */
	, (methodPointerType)&enemyController_createSpider_m3389/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
static ParameterInfo enemyController_t785_enemyController_deleteSpider_m3390_ParameterInfos[] = 
{
	{"sp", 0, 134220481, 0, &GameObject_t144_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::deleteSpider(UnityEngine.GameObject)
MethodInfo enemyController_deleteSpider_m3390_MethodInfo = 
{
	"deleteSpider"/* name */
	, (methodPointerType)&enemyController_deleteSpider_m3390/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, enemyController_t785_enemyController_deleteSpider_m3390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::createSnake()
MethodInfo enemyController_createSnake_m3391_MethodInfo = 
{
	"createSnake"/* name */
	, (methodPointerType)&enemyController_createSnake_m3391/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
static ParameterInfo enemyController_t785_enemyController_deleteSnake_m3392_ParameterInfos[] = 
{
	{"sn", 0, 134220482, 0, &GameObject_t144_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::deleteSnake(UnityEngine.GameObject)
MethodInfo enemyController_deleteSnake_m3392_MethodInfo = 
{
	"deleteSnake"/* name */
	, (methodPointerType)&enemyController_deleteSnake_m3392/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, enemyController_t785_enemyController_deleteSnake_m3392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::clearEnemies()
MethodInfo enemyController_clearEnemies_m3393_MethodInfo = 
{
	"clearEnemies"/* name */
	, (methodPointerType)&enemyController_clearEnemies_m3393/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 enemyController::getNumberEnemies()
MethodInfo enemyController_getNumberEnemies_m3394_MethodInfo = 
{
	"getNumberEnemies"/* name */
	, (methodPointerType)&enemyController_getNumberEnemies_m3394/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t758_0_0_0;
extern Il2CppType Vector3_t758_0_0_0;
static ParameterInfo enemyController_t785_enemyController_getClosest_m3395_ParameterInfos[] = 
{
	{"v", 0, 134220483, 0, &Vector3_t758_0_0_0},
};
extern Il2CppType GameObject_t144_0_0_0;
extern void* RuntimeInvoker_Object_t_Vector3_t758 (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject enemyController::getClosest(UnityEngine.Vector3)
MethodInfo enemyController_getClosest_m3395_MethodInfo = 
{
	"getClosest"/* name */
	, (methodPointerType)&enemyController_getClosest_m3395/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t144_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t758/* invoker_method */
	, enemyController_t785_enemyController_getClosest_m3395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyController::setMaxNumSnakes()
MethodInfo enemyController_setMaxNumSnakes_m3396_MethodInfo = 
{
	"setMaxNumSnakes"/* name */
	, (methodPointerType)&enemyController_setMaxNumSnakes_m3396/* method */
	, &enemyController_t785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* enemyController_t785_MethodInfos[] =
{
	&enemyController__ctor_m3381_MethodInfo,
	&enemyController__cctor_m3382_MethodInfo,
	&enemyController_Awake_m3383_MethodInfo,
	&enemyController_initHolesTutorial_m3384_MethodInfo,
	&enemyController_initHolesGame_m3385_MethodInfo,
	&enemyController_updatePosHoles_m3386_MethodInfo,
	&enemyController_moveEnemies_m3387_MethodInfo,
	&enemyController_deleteHoles_m3388_MethodInfo,
	&enemyController_createSpider_m3389_MethodInfo,
	&enemyController_deleteSpider_m3390_MethodInfo,
	&enemyController_createSnake_m3391_MethodInfo,
	&enemyController_deleteSnake_m3392_MethodInfo,
	&enemyController_clearEnemies_m3393_MethodInfo,
	&enemyController_getNumberEnemies_m3394_MethodInfo,
	&enemyController_getClosest_m3395_MethodInfo,
	&enemyController_setMaxNumSnakes_m3396_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____hole_2_FieldInfo = 
{
	"hole"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___hole_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____snake_3_FieldInfo = 
{
	"snake"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___snake_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____spider_4_FieldInfo = 
{
	"spider"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___spider_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____explosion_5_FieldInfo = 
{
	"explosion"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___explosion_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_6;
FieldInfo enemyController_t785____posHoles_6_FieldInfo = 
{
	"posHoles"/* name */
	, &ArrayList_t737_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___posHoles_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_22;
FieldInfo enemyController_t785____enemies_7_FieldInfo = 
{
	"enemies"/* name */
	, &ArrayList_t737_0_0_22/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785_StaticFields, ___enemies_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo enemyController_t785____tutorialPos_8_FieldInfo = 
{
	"tutorialPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___tutorialPos_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo enemyController_t785____agujeroTutorialObj_9_FieldInfo = 
{
	"agujeroTutorialObj"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___agujeroTutorialObj_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____agujerosPadre_10_FieldInfo = 
{
	"agujerosPadre"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___agujerosPadre_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyController_t785____currentAgujerosPadre_11_FieldInfo = 
{
	"currentAgujerosPadre"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___currentAgujerosPadre_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo enemyController_t785____maxNumSnakes_12_FieldInfo = 
{
	"maxNumSnakes"/* name */
	, &Int32_t189_0_0_1/* type */
	, &enemyController_t785_il2cpp_TypeInfo/* parent */
	, offsetof(enemyController_t785, ___maxNumSnakes_12)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* enemyController_t785_FieldInfos[] =
{
	&enemyController_t785____hole_2_FieldInfo,
	&enemyController_t785____snake_3_FieldInfo,
	&enemyController_t785____spider_4_FieldInfo,
	&enemyController_t785____explosion_5_FieldInfo,
	&enemyController_t785____posHoles_6_FieldInfo,
	&enemyController_t785____enemies_7_FieldInfo,
	&enemyController_t785____tutorialPos_8_FieldInfo,
	&enemyController_t785____agujeroTutorialObj_9_FieldInfo,
	&enemyController_t785____agujerosPadre_10_FieldInfo,
	&enemyController_t785____currentAgujerosPadre_11_FieldInfo,
	&enemyController_t785____maxNumSnakes_12_FieldInfo,
	NULL
};
static Il2CppMethodReference enemyController_t785_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool enemyController_t785_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType enemyController_t785_1_0_0;
struct enemyController_t785;
const Il2CppTypeDefinitionMetadata enemyController_t785_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, enemyController_t785_VTable/* vtableMethods */
	, enemyController_t785_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo enemyController_t785_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "enemyController"/* name */
	, ""/* namespaze */
	, enemyController_t785_MethodInfos/* methods */
	, NULL/* properties */
	, enemyController_t785_FieldInfos/* fields */
	, NULL/* events */
	, &enemyController_t785_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &enemyController_t785_0_0_0/* byval_arg */
	, &enemyController_t785_1_0_0/* this_arg */
	, &enemyController_t785_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (enemyController_t785)/* instance_size */
	, sizeof (enemyController_t785)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(enemyController_t785_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// enemyLife
#include "AssemblyU2DCSharp_enemyLife.h"
// Metadata Definition enemyLife
extern TypeInfo enemyLife_t788_il2cpp_TypeInfo;
// enemyLife
#include "AssemblyU2DCSharp_enemyLifeMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyLife::.ctor()
MethodInfo enemyLife__ctor_m3397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&enemyLife__ctor_m3397/* method */
	, &enemyLife_t788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyLife::decreaseHP()
MethodInfo enemyLife_decreaseHP_m3398_MethodInfo = 
{
	"decreaseHP"/* name */
	, (methodPointerType)&enemyLife_decreaseHP_m3398/* method */
	, &enemyLife_t788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyLife::decreaseDoubleHP()
MethodInfo enemyLife_decreaseDoubleHP_m3399_MethodInfo = 
{
	"decreaseDoubleHP"/* name */
	, (methodPointerType)&enemyLife_decreaseDoubleHP_m3399/* method */
	, &enemyLife_t788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 enemyLife::getHP()
MethodInfo enemyLife_getHP_m3400_MethodInfo = 
{
	"getHP"/* name */
	, (methodPointerType)&enemyLife_getHP_m3400/* method */
	, &enemyLife_t788_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* enemyLife_t788_MethodInfos[] =
{
	&enemyLife__ctor_m3397_MethodInfo,
	&enemyLife_decreaseHP_m3398_MethodInfo,
	&enemyLife_decreaseDoubleHP_m3399_MethodInfo,
	&enemyLife_getHP_m3400_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo enemyLife_t788____hp_2_FieldInfo = 
{
	"hp"/* name */
	, &Int32_t189_0_0_6/* type */
	, &enemyLife_t788_il2cpp_TypeInfo/* parent */
	, offsetof(enemyLife_t788, ___hp_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* enemyLife_t788_FieldInfos[] =
{
	&enemyLife_t788____hp_2_FieldInfo,
	NULL
};
static Il2CppMethodReference enemyLife_t788_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool enemyLife_t788_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType enemyLife_t788_0_0_0;
extern Il2CppType enemyLife_t788_1_0_0;
struct enemyLife_t788;
const Il2CppTypeDefinitionMetadata enemyLife_t788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, enemyLife_t788_VTable/* vtableMethods */
	, enemyLife_t788_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo enemyLife_t788_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "enemyLife"/* name */
	, ""/* namespaze */
	, enemyLife_t788_MethodInfos/* methods */
	, NULL/* properties */
	, enemyLife_t788_FieldInfos/* fields */
	, NULL/* events */
	, &enemyLife_t788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &enemyLife_t788_0_0_0/* byval_arg */
	, &enemyLife_t788_1_0_0/* this_arg */
	, &enemyLife_t788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (enemyLife_t788)/* instance_size */
	, sizeof (enemyLife_t788)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// enemyMov/enemyState
#include "AssemblyU2DCSharp_enemyMov_enemyState.h"
// Metadata Definition enemyMov/enemyState
extern TypeInfo enemyState_t789_il2cpp_TypeInfo;
// enemyMov/enemyState
#include "AssemblyU2DCSharp_enemyMov_enemyStateMethodDeclarations.h"
static MethodInfo* enemyState_t789_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo enemyState_t789____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &enemyState_t789_il2cpp_TypeInfo/* parent */
	, offsetof(enemyState_t789, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyState_t789_0_0_32854;
FieldInfo enemyState_t789____ATTACK_2_FieldInfo = 
{
	"ATTACK"/* name */
	, &enemyState_t789_0_0_32854/* type */
	, &enemyState_t789_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyState_t789_0_0_32854;
FieldInfo enemyState_t789____WANDER_3_FieldInfo = 
{
	"WANDER"/* name */
	, &enemyState_t789_0_0_32854/* type */
	, &enemyState_t789_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* enemyState_t789_FieldInfos[] =
{
	&enemyState_t789____value___1_FieldInfo,
	&enemyState_t789____ATTACK_2_FieldInfo,
	&enemyState_t789____WANDER_3_FieldInfo,
	NULL
};
static const int32_t enemyState_t789____ATTACK_2_DefaultValueData = 0;
extern Il2CppType Int32_t189_0_0_0;
static Il2CppFieldDefaultValueEntry enemyState_t789____ATTACK_2_DefaultValue = 
{
	&enemyState_t789____ATTACK_2_FieldInfo/* field */
	, { (char*)&enemyState_t789____ATTACK_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t enemyState_t789____WANDER_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry enemyState_t789____WANDER_3_DefaultValue = 
{
	&enemyState_t789____WANDER_3_FieldInfo/* field */
	, { (char*)&enemyState_t789____WANDER_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* enemyState_t789_FieldDefaultValues[] = 
{
	&enemyState_t789____ATTACK_2_DefaultValue,
	&enemyState_t789____WANDER_3_DefaultValue,
	NULL
};
extern MethodInfo Enum_Equals_m1279_MethodInfo;
extern MethodInfo Enum_GetHashCode_m1280_MethodInfo;
extern MethodInfo Enum_ToString_m1281_MethodInfo;
extern MethodInfo Enum_ToString_m1282_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m1283_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m1284_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m1285_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m1286_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m1287_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m1288_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m1289_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m1290_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m1291_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m1292_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m1293_MethodInfo;
extern MethodInfo Enum_ToString_m1294_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m1295_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m1296_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m1297_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m1298_MethodInfo;
extern MethodInfo Enum_CompareTo_m1299_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m1300_MethodInfo;
static Il2CppMethodReference enemyState_t789_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool enemyState_t789_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType IFormattable_t312_0_0_0;
extern Il2CppType IConvertible_t313_0_0_0;
extern Il2CppType IComparable_t314_0_0_0;
static Il2CppInterfaceOffsetPair enemyState_t789_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType enemyState_t789_0_0_0;
extern Il2CppType enemyState_t789_1_0_0;
extern Il2CppType Enum_t218_0_0_0;
extern TypeInfo enemyMov_t790_il2cpp_TypeInfo;
extern Il2CppType enemyMov_t790_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t189_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata enemyState_t789_DefinitionMetadata = 
{
	&enemyMov_t790_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, enemyState_t789_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, enemyState_t789_VTable/* vtableMethods */
	, enemyState_t789_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo enemyState_t789_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "enemyState"/* name */
	, ""/* namespaze */
	, enemyState_t789_MethodInfos/* methods */
	, NULL/* properties */
	, enemyState_t789_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &enemyState_t789_0_0_0/* byval_arg */
	, &enemyState_t789_1_0_0/* this_arg */
	, &enemyState_t789_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, enemyState_t789_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (enemyState_t789)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (enemyState_t789)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// enemyMov
#include "AssemblyU2DCSharp_enemyMov.h"
// Metadata Definition enemyMov
// enemyMov
#include "AssemblyU2DCSharp_enemyMovMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::.ctor()
MethodInfo enemyMov__ctor_m3401_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&enemyMov__ctor_m3401/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::Start()
MethodInfo enemyMov_Start_m3402_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&enemyMov_Start_m3402/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::setSpeed()
MethodInfo enemyMov_setSpeed_m3403_MethodInfo = 
{
	"setSpeed"/* name */
	, (methodPointerType)&enemyMov_setSpeed_m3403/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::Update()
MethodInfo enemyMov_Update_m3404_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&enemyMov_Update_m3404/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::resetMovement()
MethodInfo enemyMov_resetMovement_m3405_MethodInfo = 
{
	"resetMovement"/* name */
	, (methodPointerType)&enemyMov_resetMovement_m3405/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void enemyMov::move()
MethodInfo enemyMov_move_m3406_MethodInfo = 
{
	"move"/* name */
	, (methodPointerType)&enemyMov_move_m3406/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo enemyMov_t790_enemyMov_calculateAngle_m3407_ParameterInfos[] = 
{
	{"target", 0, 134220484, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Single enemyMov::calculateAngle(UnityEngine.Vector2)
MethodInfo enemyMov_calculateAngle_m3407_MethodInfo = 
{
	"calculateAngle"/* name */
	, (methodPointerType)&enemyMov_calculateAngle_m3407/* method */
	, &enemyMov_t790_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Vector2_t739/* invoker_method */
	, enemyMov_t790_enemyMov_calculateAngle_m3407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* enemyMov_t790_MethodInfos[] =
{
	&enemyMov__ctor_m3401_MethodInfo,
	&enemyMov_Start_m3402_MethodInfo,
	&enemyMov_setSpeed_m3403_MethodInfo,
	&enemyMov_Update_m3404_MethodInfo,
	&enemyMov_resetMovement_m3405_MethodInfo,
	&enemyMov_move_m3406_MethodInfo,
	&enemyMov_calculateAngle_m3407_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_6;
FieldInfo enemyMov_t790____maxSpeed_2_FieldInfo = 
{
	"maxSpeed"/* name */
	, &Single_t202_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___maxSpeed_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo enemyMov_t790____minSpeed_3_FieldInfo = 
{
	"minSpeed"/* name */
	, &Single_t202_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___minSpeed_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo enemyMov_t790____speed_4_FieldInfo = 
{
	"speed"/* name */
	, &Single_t202_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___speed_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo enemyMov_t790____turningSpeedMax_5_FieldInfo = 
{
	"turningSpeedMax"/* name */
	, &Single_t202_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___turningSpeedMax_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo enemyMov_t790____turningSpeed_6_FieldInfo = 
{
	"turningSpeed"/* name */
	, &Single_t202_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___turningSpeed_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo enemyMov_t790____target_7_FieldInfo = 
{
	"target"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___target_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo enemyMov_t790____randomPos_8_FieldInfo = 
{
	"randomPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___randomPos_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector3_t758_0_0_1;
FieldInfo enemyMov_t790____pos_9_FieldInfo = 
{
	"pos"/* name */
	, &Vector3_t758_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___pos_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Quaternion_t771_0_0_1;
FieldInfo enemyMov_t790____lookRotation_10_FieldInfo = 
{
	"lookRotation"/* name */
	, &Quaternion_t771_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___lookRotation_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo enemyMov_t790____direction_11_FieldInfo = 
{
	"direction"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___direction_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo enemyMov_t790____timer_12_FieldInfo = 
{
	"timer"/* name */
	, &Int32_t189_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___timer_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyState_t789_0_0_6;
FieldInfo enemyMov_t790____optionState_13_FieldInfo = 
{
	"optionState"/* name */
	, &enemyState_t789_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___optionState_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo enemyMov_t790____player_14_FieldInfo = 
{
	"player"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &enemyMov_t790_il2cpp_TypeInfo/* parent */
	, offsetof(enemyMov_t790, ___player_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* enemyMov_t790_FieldInfos[] =
{
	&enemyMov_t790____maxSpeed_2_FieldInfo,
	&enemyMov_t790____minSpeed_3_FieldInfo,
	&enemyMov_t790____speed_4_FieldInfo,
	&enemyMov_t790____turningSpeedMax_5_FieldInfo,
	&enemyMov_t790____turningSpeed_6_FieldInfo,
	&enemyMov_t790____target_7_FieldInfo,
	&enemyMov_t790____randomPos_8_FieldInfo,
	&enemyMov_t790____pos_9_FieldInfo,
	&enemyMov_t790____lookRotation_10_FieldInfo,
	&enemyMov_t790____direction_11_FieldInfo,
	&enemyMov_t790____timer_12_FieldInfo,
	&enemyMov_t790____optionState_13_FieldInfo,
	&enemyMov_t790____player_14_FieldInfo,
	NULL
};
static const Il2CppType* enemyMov_t790_il2cpp_TypeInfo__nestedTypes[1] =
{
	&enemyState_t789_0_0_0,
};
static Il2CppMethodReference enemyMov_t790_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool enemyMov_t790_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType enemyMov_t790_1_0_0;
struct enemyMov_t790;
const Il2CppTypeDefinitionMetadata enemyMov_t790_DefinitionMetadata = 
{
	NULL/* declaringType */
	, enemyMov_t790_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, enemyMov_t790_VTable/* vtableMethods */
	, enemyMov_t790_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo enemyMov_t790_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "enemyMov"/* name */
	, ""/* namespaze */
	, enemyMov_t790_MethodInfos/* methods */
	, NULL/* properties */
	, enemyMov_t790_FieldInfos/* fields */
	, NULL/* events */
	, &enemyMov_t790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &enemyMov_t790_0_0_0/* byval_arg */
	, &enemyMov_t790_1_0_0/* this_arg */
	, &enemyMov_t790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (enemyMov_t790)/* instance_size */
	, sizeof (enemyMov_t790)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// explosionGenerator
#include "AssemblyU2DCSharp_explosionGenerator.h"
// Metadata Definition explosionGenerator
extern TypeInfo explosionGenerator_t791_il2cpp_TypeInfo;
// explosionGenerator
#include "AssemblyU2DCSharp_explosionGeneratorMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void explosionGenerator::.ctor()
MethodInfo explosionGenerator__ctor_m3408_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&explosionGenerator__ctor_m3408/* method */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void explosionGenerator::Start()
MethodInfo explosionGenerator_Start_m3409_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&explosionGenerator_Start_m3409/* method */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* explosionGenerator_t791_MethodInfos[] =
{
	&explosionGenerator__ctor_m3408_MethodInfo,
	&explosionGenerator_Start_m3409_MethodInfo,
	NULL
};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo explosionGenerator_t791____randomPos_2_FieldInfo = 
{
	"randomPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* parent */
	, offsetof(explosionGenerator_t791, ___randomPos_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo explosionGenerator_t791____explosion_3_FieldInfo = 
{
	"explosion"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* parent */
	, offsetof(explosionGenerator_t791, ___explosion_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo explosionGenerator_t791____distanciaMax_4_FieldInfo = 
{
	"distanciaMax"/* name */
	, &Single_t202_0_0_1/* type */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* parent */
	, offsetof(explosionGenerator_t791, ___distanciaMax_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo explosionGenerator_t791____factor_5_FieldInfo = 
{
	"factor"/* name */
	, &Int32_t189_0_0_6/* type */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* parent */
	, offsetof(explosionGenerator_t791, ___factor_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* explosionGenerator_t791_FieldInfos[] =
{
	&explosionGenerator_t791____randomPos_2_FieldInfo,
	&explosionGenerator_t791____explosion_3_FieldInfo,
	&explosionGenerator_t791____distanciaMax_4_FieldInfo,
	&explosionGenerator_t791____factor_5_FieldInfo,
	NULL
};
static Il2CppMethodReference explosionGenerator_t791_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool explosionGenerator_t791_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType explosionGenerator_t791_0_0_0;
extern Il2CppType explosionGenerator_t791_1_0_0;
struct explosionGenerator_t791;
const Il2CppTypeDefinitionMetadata explosionGenerator_t791_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, explosionGenerator_t791_VTable/* vtableMethods */
	, explosionGenerator_t791_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo explosionGenerator_t791_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "explosionGenerator"/* name */
	, ""/* namespaze */
	, explosionGenerator_t791_MethodInfos/* methods */
	, NULL/* properties */
	, explosionGenerator_t791_FieldInfos/* fields */
	, NULL/* events */
	, &explosionGenerator_t791_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &explosionGenerator_t791_0_0_0/* byval_arg */
	, &explosionGenerator_t791_1_0_0/* this_arg */
	, &explosionGenerator_t791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (explosionGenerator_t791)/* instance_size */
	, sizeof (explosionGenerator_t791)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// fpsDisplay
#include "AssemblyU2DCSharp_fpsDisplay.h"
// Metadata Definition fpsDisplay
extern TypeInfo fpsDisplay_t792_il2cpp_TypeInfo;
// fpsDisplay
#include "AssemblyU2DCSharp_fpsDisplayMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void fpsDisplay::.ctor()
MethodInfo fpsDisplay__ctor_m3410_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&fpsDisplay__ctor_m3410/* method */
	, &fpsDisplay_t792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void fpsDisplay::Start()
MethodInfo fpsDisplay_Start_m3411_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&fpsDisplay_Start_m3411/* method */
	, &fpsDisplay_t792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void fpsDisplay::OnGUI()
MethodInfo fpsDisplay_OnGUI_m3412_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&fpsDisplay_OnGUI_m3412/* method */
	, &fpsDisplay_t792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* fpsDisplay_t792_MethodInfos[] =
{
	&fpsDisplay__ctor_m3410_MethodInfo,
	&fpsDisplay_Start_m3411_MethodInfo,
	&fpsDisplay_OnGUI_m3412_MethodInfo,
	NULL
};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo fpsDisplay_t792____fpsSt_2_FieldInfo = 
{
	"fpsSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &fpsDisplay_t792_il2cpp_TypeInfo/* parent */
	, offsetof(fpsDisplay_t792, ___fpsSt_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* fpsDisplay_t792_FieldInfos[] =
{
	&fpsDisplay_t792____fpsSt_2_FieldInfo,
	NULL
};
static Il2CppMethodReference fpsDisplay_t792_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool fpsDisplay_t792_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType fpsDisplay_t792_0_0_0;
extern Il2CppType fpsDisplay_t792_1_0_0;
struct fpsDisplay_t792;
const Il2CppTypeDefinitionMetadata fpsDisplay_t792_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, fpsDisplay_t792_VTable/* vtableMethods */
	, fpsDisplay_t792_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo fpsDisplay_t792_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "fpsDisplay"/* name */
	, ""/* namespaze */
	, fpsDisplay_t792_MethodInfos/* methods */
	, NULL/* properties */
	, fpsDisplay_t792_FieldInfos/* fields */
	, NULL/* events */
	, &fpsDisplay_t792_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &fpsDisplay_t792_0_0_0/* byval_arg */
	, &fpsDisplay_t792_1_0_0/* this_arg */
	, &fpsDisplay_t792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (fpsDisplay_t792)/* instance_size */
	, sizeof (fpsDisplay_t792)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// gameControl/State
#include "AssemblyU2DCSharp_gameControl_State.h"
// Metadata Definition gameControl/State
extern TypeInfo State_t793_il2cpp_TypeInfo;
// gameControl/State
#include "AssemblyU2DCSharp_gameControl_StateMethodDeclarations.h"
static MethodInfo* State_t793_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t189_0_0_1542;
FieldInfo State_t793____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t189_0_0_1542/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, offsetof(State_t793, ___value___1) + sizeof(Object_t)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____MENU_2_FieldInfo = 
{
	"MENU"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____INTERLUDE_3_FieldInfo = 
{
	"INTERLUDE"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____WEAPONROOM_4_FieldInfo = 
{
	"WEAPONROOM"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____MAP_5_FieldInfo = 
{
	"MAP"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____INGAME_6_FieldInfo = 
{
	"INGAME"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____NEWWEAPON_7_FieldInfo = 
{
	"NEWWEAPON"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____GAMEOVER_8_FieldInfo = 
{
	"GAMEOVER"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____STORE_9_FieldInfo = 
{
	"STORE"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_32854;
FieldInfo State_t793____TUTORIAL_10_FieldInfo = 
{
	"TUTORIAL"/* name */
	, &State_t793_0_0_32854/* type */
	, &State_t793_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* State_t793_FieldInfos[] =
{
	&State_t793____value___1_FieldInfo,
	&State_t793____MENU_2_FieldInfo,
	&State_t793____INTERLUDE_3_FieldInfo,
	&State_t793____WEAPONROOM_4_FieldInfo,
	&State_t793____MAP_5_FieldInfo,
	&State_t793____INGAME_6_FieldInfo,
	&State_t793____NEWWEAPON_7_FieldInfo,
	&State_t793____GAMEOVER_8_FieldInfo,
	&State_t793____STORE_9_FieldInfo,
	&State_t793____TUTORIAL_10_FieldInfo,
	NULL
};
static const int32_t State_t793____MENU_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry State_t793____MENU_2_DefaultValue = 
{
	&State_t793____MENU_2_FieldInfo/* field */
	, { (char*)&State_t793____MENU_2_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____INTERLUDE_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry State_t793____INTERLUDE_3_DefaultValue = 
{
	&State_t793____INTERLUDE_3_FieldInfo/* field */
	, { (char*)&State_t793____INTERLUDE_3_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____WEAPONROOM_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry State_t793____WEAPONROOM_4_DefaultValue = 
{
	&State_t793____WEAPONROOM_4_FieldInfo/* field */
	, { (char*)&State_t793____WEAPONROOM_4_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____MAP_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry State_t793____MAP_5_DefaultValue = 
{
	&State_t793____MAP_5_FieldInfo/* field */
	, { (char*)&State_t793____MAP_5_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____INGAME_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry State_t793____INGAME_6_DefaultValue = 
{
	&State_t793____INGAME_6_FieldInfo/* field */
	, { (char*)&State_t793____INGAME_6_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____NEWWEAPON_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry State_t793____NEWWEAPON_7_DefaultValue = 
{
	&State_t793____NEWWEAPON_7_FieldInfo/* field */
	, { (char*)&State_t793____NEWWEAPON_7_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____GAMEOVER_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry State_t793____GAMEOVER_8_DefaultValue = 
{
	&State_t793____GAMEOVER_8_FieldInfo/* field */
	, { (char*)&State_t793____GAMEOVER_8_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____STORE_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry State_t793____STORE_9_DefaultValue = 
{
	&State_t793____STORE_9_FieldInfo/* field */
	, { (char*)&State_t793____STORE_9_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static const int32_t State_t793____TUTORIAL_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry State_t793____TUTORIAL_10_DefaultValue = 
{
	&State_t793____TUTORIAL_10_FieldInfo/* field */
	, { (char*)&State_t793____TUTORIAL_10_DefaultValueData, &Int32_t189_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* State_t793_FieldDefaultValues[] = 
{
	&State_t793____MENU_2_DefaultValue,
	&State_t793____INTERLUDE_3_DefaultValue,
	&State_t793____WEAPONROOM_4_DefaultValue,
	&State_t793____MAP_5_DefaultValue,
	&State_t793____INGAME_6_DefaultValue,
	&State_t793____NEWWEAPON_7_DefaultValue,
	&State_t793____GAMEOVER_8_DefaultValue,
	&State_t793____STORE_9_DefaultValue,
	&State_t793____TUTORIAL_10_DefaultValue,
	NULL
};
static Il2CppMethodReference State_t793_VTable[] =
{
	&Enum_Equals_m1279_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Enum_GetHashCode_m1280_MethodInfo,
	&Enum_ToString_m1281_MethodInfo,
	&Enum_ToString_m1282_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m1283_MethodInfo,
	&Enum_System_IConvertible_ToByte_m1284_MethodInfo,
	&Enum_System_IConvertible_ToChar_m1285_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m1286_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m1287_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m1288_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m1289_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m1290_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m1291_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m1292_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m1293_MethodInfo,
	&Enum_ToString_m1294_MethodInfo,
	&Enum_System_IConvertible_ToType_m1295_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m1296_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m1297_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m1298_MethodInfo,
	&Enum_CompareTo_m1299_MethodInfo,
	&Enum_GetTypeCode_m1300_MethodInfo,
};
static bool State_t793_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair State_t793_InterfacesOffsets[] = 
{
	{ &IFormattable_t312_0_0_0, 4},
	{ &IConvertible_t313_0_0_0, 5},
	{ &IComparable_t314_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType State_t793_0_0_0;
extern Il2CppType State_t793_1_0_0;
extern TypeInfo gameControl_t794_il2cpp_TypeInfo;
extern Il2CppType gameControl_t794_0_0_0;
const Il2CppTypeDefinitionMetadata State_t793_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, State_t793_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t218_0_0_0/* parent */
	, State_t793_VTable/* vtableMethods */
	, State_t793_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo State_t793_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "State"/* name */
	, ""/* namespaze */
	, State_t793_MethodInfos/* methods */
	, NULL/* properties */
	, State_t793_FieldInfos/* fields */
	, NULL/* events */
	, &Int32_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &State_t793_0_0_0/* byval_arg */
	, &State_t793_1_0_0/* this_arg */
	, &State_t793_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, State_t793_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (State_t793)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (State_t793)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<startGame>c__Iterator7
#include "AssemblyU2DCSharp_gameControl_U3CstartGameU3Ec__Iterator7.h"
// Metadata Definition gameControl/<startGame>c__Iterator7
extern TypeInfo U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo;
// gameControl/<startGame>c__Iterator7
#include "AssemblyU2DCSharp_gameControl_U3CstartGameU3Ec__Iterator7MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<startGame>c__Iterator7::.ctor()
MethodInfo U3CstartGameU3Ec__Iterator7__ctor_m3413_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7__ctor_m3413/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<startGame>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 265/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<startGame>c__Iterator7::System.Collections.IEnumerator.get_Current()
MethodInfo U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 266/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<startGame>c__Iterator7::MoveNext()
MethodInfo U3CstartGameU3Ec__Iterator7_MoveNext_m3416_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7_MoveNext_m3416/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<startGame>c__Iterator7::Dispose()
MethodInfo U3CstartGameU3Ec__Iterator7_Dispose_m3417_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7_Dispose_m3417/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 267/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<startGame>c__Iterator7::Reset()
MethodInfo U3CstartGameU3Ec__Iterator7_Reset_m3418_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CstartGameU3Ec__Iterator7_Reset_m3418/* method */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 268/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CstartGameU3Ec__Iterator7_t795_MethodInfos[] =
{
	&U3CstartGameU3Ec__Iterator7__ctor_m3413_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_MoveNext_m3416_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_Dispose_m3417_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_Reset_m3418_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CstartU3E__0_0_FieldInfo = 
{
	"<start>__0"/* name */
	, &Boolean_t203_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CstartU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CindexU3E__1_1_FieldInfo = 
{
	"<index>__1"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CindexU3E__1_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CrU3E__2_2_FieldInfo = 
{
	"<r>__2"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CrU3E__2_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CrBU3E__3_3_FieldInfo = 
{
	"<rB>__3"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CrBU3E__3_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CrRU3E__4_4_FieldInfo = 
{
	"<rR>__4"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CrRU3E__4_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U24PC_5_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U24PC_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U24current_6_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U24current_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType gameControl_t794_0_0_3;
FieldInfo U3CstartGameU3Ec__Iterator7_t795____U3CU3Ef__this_7_FieldInfo = 
{
	"<>f__this"/* name */
	, &gameControl_t794_0_0_3/* type */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, offsetof(U3CstartGameU3Ec__Iterator7_t795, ___U3CU3Ef__this_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CstartGameU3Ec__Iterator7_t795_FieldInfos[] =
{
	&U3CstartGameU3Ec__Iterator7_t795____U3CstartU3E__0_0_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U3CindexU3E__1_1_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U3CrU3E__2_2_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U3CrBU3E__3_3_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U3CrRU3E__4_4_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U24PC_5_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U24current_6_FieldInfo,
	&U3CstartGameU3Ec__Iterator7_t795____U3CU3Ef__this_7_FieldInfo,
	NULL
};
extern MethodInfo U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414_MethodInfo;
static PropertyInfo U3CstartGameU3Ec__Iterator7_t795____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415_MethodInfo;
static PropertyInfo U3CstartGameU3Ec__Iterator7_t795____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CstartGameU3Ec__Iterator7_t795_PropertyInfos[] =
{
	&U3CstartGameU3Ec__Iterator7_t795____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CstartGameU3Ec__Iterator7_t795____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CstartGameU3Ec__Iterator7_Dispose_m3417_MethodInfo;
extern MethodInfo U3CstartGameU3Ec__Iterator7_MoveNext_m3416_MethodInfo;
extern MethodInfo U3CstartGameU3Ec__Iterator7_Reset_m3418_MethodInfo;
static Il2CppMethodReference U3CstartGameU3Ec__Iterator7_t795_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_Dispose_m3417_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m3415_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_MoveNext_m3416_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3414_MethodInfo,
	&U3CstartGameU3Ec__Iterator7_Reset_m3418_MethodInfo,
};
static bool U3CstartGameU3Ec__Iterator7_t795_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CstartGameU3Ec__Iterator7_t795_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CstartGameU3Ec__Iterator7_t795_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CstartGameU3Ec__Iterator7_t795_0_0_0;
extern Il2CppType U3CstartGameU3Ec__Iterator7_t795_1_0_0;
struct U3CstartGameU3Ec__Iterator7_t795;
const Il2CppTypeDefinitionMetadata U3CstartGameU3Ec__Iterator7_t795_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CstartGameU3Ec__Iterator7_t795_InterfacesTypeInfos/* implementedInterfaces */
	, U3CstartGameU3Ec__Iterator7_t795_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CstartGameU3Ec__Iterator7_t795_VTable/* vtableMethods */
	, U3CstartGameU3Ec__Iterator7_t795_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<startGame>c__Iterator7"/* name */
	, ""/* namespaze */
	, U3CstartGameU3Ec__Iterator7_t795_MethodInfos/* methods */
	, U3CstartGameU3Ec__Iterator7_t795_PropertyInfos/* properties */
	, U3CstartGameU3Ec__Iterator7_t795_FieldInfos/* fields */
	, NULL/* events */
	, &U3CstartGameU3Ec__Iterator7_t795_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 264/* custom_attributes_cache */
	, &U3CstartGameU3Ec__Iterator7_t795_0_0_0/* byval_arg */
	, &U3CstartGameU3Ec__Iterator7_t795_1_0_0/* this_arg */
	, &U3CstartGameU3Ec__Iterator7_t795_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CstartGameU3Ec__Iterator7_t795)/* instance_size */
	, sizeof (U3CstartGameU3Ec__Iterator7_t795)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<wait>c__Iterator8
#include "AssemblyU2DCSharp_gameControl_U3CwaitU3Ec__Iterator8.h"
// Metadata Definition gameControl/<wait>c__Iterator8
extern TypeInfo U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo;
// gameControl/<wait>c__Iterator8
#include "AssemblyU2DCSharp_gameControl_U3CwaitU3Ec__Iterator8MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<wait>c__Iterator8::.ctor()
MethodInfo U3CwaitU3Ec__Iterator8__ctor_m3419_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8__ctor_m3419/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<wait>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 270/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<wait>c__Iterator8::System.Collections.IEnumerator.get_Current()
MethodInfo U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 271/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<wait>c__Iterator8::MoveNext()
MethodInfo U3CwaitU3Ec__Iterator8_MoveNext_m3422_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8_MoveNext_m3422/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<wait>c__Iterator8::Dispose()
MethodInfo U3CwaitU3Ec__Iterator8_Dispose_m3423_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8_Dispose_m3423/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 272/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<wait>c__Iterator8::Reset()
MethodInfo U3CwaitU3Ec__Iterator8_Reset_m3424_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator8_Reset_m3424/* method */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 273/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CwaitU3Ec__Iterator8_t796_MethodInfos[] =
{
	&U3CwaitU3Ec__Iterator8__ctor_m3419_MethodInfo,
	&U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420_MethodInfo,
	&U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421_MethodInfo,
	&U3CwaitU3Ec__Iterator8_MoveNext_m3422_MethodInfo,
	&U3CwaitU3Ec__Iterator8_Dispose_m3423_MethodInfo,
	&U3CwaitU3Ec__Iterator8_Reset_m3424_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator8_t796____U3CtU3E__0_0_FieldInfo = 
{
	"<t>__0"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator8_t796, ___U3CtU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator8_t796____U3CexplosionRedU3E__1_1_FieldInfo = 
{
	"<explosionRed>__1"/* name */
	, &GameObject_t144_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator8_t796, ___U3CexplosionRedU3E__1_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator8_t796____U24PC_2_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator8_t796, ___U24PC_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator8_t796____U24current_3_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator8_t796, ___U24current_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType gameControl_t794_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator8_t796____U3CU3Ef__this_4_FieldInfo = 
{
	"<>f__this"/* name */
	, &gameControl_t794_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator8_t796, ___U3CU3Ef__this_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CwaitU3Ec__Iterator8_t796_FieldInfos[] =
{
	&U3CwaitU3Ec__Iterator8_t796____U3CtU3E__0_0_FieldInfo,
	&U3CwaitU3Ec__Iterator8_t796____U3CexplosionRedU3E__1_1_FieldInfo,
	&U3CwaitU3Ec__Iterator8_t796____U24PC_2_FieldInfo,
	&U3CwaitU3Ec__Iterator8_t796____U24current_3_FieldInfo,
	&U3CwaitU3Ec__Iterator8_t796____U3CU3Ef__this_4_FieldInfo,
	NULL
};
extern MethodInfo U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420_MethodInfo;
static PropertyInfo U3CwaitU3Ec__Iterator8_t796____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421_MethodInfo;
static PropertyInfo U3CwaitU3Ec__Iterator8_t796____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CwaitU3Ec__Iterator8_t796_PropertyInfos[] =
{
	&U3CwaitU3Ec__Iterator8_t796____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CwaitU3Ec__Iterator8_t796____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CwaitU3Ec__Iterator8_Dispose_m3423_MethodInfo;
extern MethodInfo U3CwaitU3Ec__Iterator8_MoveNext_m3422_MethodInfo;
extern MethodInfo U3CwaitU3Ec__Iterator8_Reset_m3424_MethodInfo;
static Il2CppMethodReference U3CwaitU3Ec__Iterator8_t796_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CwaitU3Ec__Iterator8_Dispose_m3423_MethodInfo,
	&U3CwaitU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m3421_MethodInfo,
	&U3CwaitU3Ec__Iterator8_MoveNext_m3422_MethodInfo,
	&U3CwaitU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3420_MethodInfo,
	&U3CwaitU3Ec__Iterator8_Reset_m3424_MethodInfo,
};
static bool U3CwaitU3Ec__Iterator8_t796_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CwaitU3Ec__Iterator8_t796_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CwaitU3Ec__Iterator8_t796_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CwaitU3Ec__Iterator8_t796_0_0_0;
extern Il2CppType U3CwaitU3Ec__Iterator8_t796_1_0_0;
struct U3CwaitU3Ec__Iterator8_t796;
const Il2CppTypeDefinitionMetadata U3CwaitU3Ec__Iterator8_t796_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CwaitU3Ec__Iterator8_t796_InterfacesTypeInfos/* implementedInterfaces */
	, U3CwaitU3Ec__Iterator8_t796_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CwaitU3Ec__Iterator8_t796_VTable/* vtableMethods */
	, U3CwaitU3Ec__Iterator8_t796_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<wait>c__Iterator8"/* name */
	, ""/* namespaze */
	, U3CwaitU3Ec__Iterator8_t796_MethodInfos/* methods */
	, U3CwaitU3Ec__Iterator8_t796_PropertyInfos/* properties */
	, U3CwaitU3Ec__Iterator8_t796_FieldInfos/* fields */
	, NULL/* events */
	, &U3CwaitU3Ec__Iterator8_t796_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 269/* custom_attributes_cache */
	, &U3CwaitU3Ec__Iterator8_t796_0_0_0/* byval_arg */
	, &U3CwaitU3Ec__Iterator8_t796_1_0_0/* this_arg */
	, &U3CwaitU3Ec__Iterator8_t796_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CwaitU3Ec__Iterator8_t796)/* instance_size */
	, sizeof (U3CwaitU3Ec__Iterator8_t796)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<waitForTouchDown>c__Iterator9
#include "AssemblyU2DCSharp_gameControl_U3CwaitForTouchDownU3Ec__Itera.h"
// Metadata Definition gameControl/<waitForTouchDown>c__Iterator9
extern TypeInfo U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo;
// gameControl/<waitForTouchDown>c__Iterator9
#include "AssemblyU2DCSharp_gameControl_U3CwaitForTouchDownU3Ec__IteraMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitForTouchDown>c__Iterator9::.ctor()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9__ctor_m3425_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9__ctor_m3425/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<waitForTouchDown>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 275/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<waitForTouchDown>c__Iterator9::System.Collections.IEnumerator.get_Current()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 276/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<waitForTouchDown>c__Iterator9::MoveNext()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitForTouchDown>c__Iterator9::Dispose()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 277/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitForTouchDown>c__Iterator9::Reset()
MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430/* method */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 278/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CwaitForTouchDownU3Ec__Iterator9_t797_MethodInfos[] =
{
	&U3CwaitForTouchDownU3Ec__Iterator9__ctor_m3425_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitForTouchDownU3Ec__Iterator9_t797____U24PC_0_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitForTouchDownU3Ec__Iterator9_t797, ___U24PC_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CwaitForTouchDownU3Ec__Iterator9_t797____U24current_1_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitForTouchDownU3Ec__Iterator9_t797, ___U24current_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CwaitForTouchDownU3Ec__Iterator9_t797_FieldInfos[] =
{
	&U3CwaitForTouchDownU3Ec__Iterator9_t797____U24PC_0_FieldInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_t797____U24current_1_FieldInfo,
	NULL
};
extern MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426_MethodInfo;
static PropertyInfo U3CwaitForTouchDownU3Ec__Iterator9_t797____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427_MethodInfo;
static PropertyInfo U3CwaitForTouchDownU3Ec__Iterator9_t797____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CwaitForTouchDownU3Ec__Iterator9_t797_PropertyInfos[] =
{
	&U3CwaitForTouchDownU3Ec__Iterator9_t797____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_t797____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429_MethodInfo;
extern MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428_MethodInfo;
extern MethodInfo U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430_MethodInfo;
static Il2CppMethodReference U3CwaitForTouchDownU3Ec__Iterator9_t797_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_Dispose_m3429_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m3427_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_MoveNext_m3428_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3426_MethodInfo,
	&U3CwaitForTouchDownU3Ec__Iterator9_Reset_m3430_MethodInfo,
};
static bool U3CwaitForTouchDownU3Ec__Iterator9_t797_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CwaitForTouchDownU3Ec__Iterator9_t797_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CwaitForTouchDownU3Ec__Iterator9_t797_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CwaitForTouchDownU3Ec__Iterator9_t797_0_0_0;
extern Il2CppType U3CwaitForTouchDownU3Ec__Iterator9_t797_1_0_0;
struct U3CwaitForTouchDownU3Ec__Iterator9_t797;
const Il2CppTypeDefinitionMetadata U3CwaitForTouchDownU3Ec__Iterator9_t797_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_InterfacesTypeInfos/* implementedInterfaces */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_VTable/* vtableMethods */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<waitForTouchDown>c__Iterator9"/* name */
	, ""/* namespaze */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_MethodInfos/* methods */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_PropertyInfos/* properties */
	, U3CwaitForTouchDownU3Ec__Iterator9_t797_FieldInfos/* fields */
	, NULL/* events */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 274/* custom_attributes_cache */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_0_0_0/* byval_arg */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_1_0_0/* this_arg */
	, &U3CwaitForTouchDownU3Ec__Iterator9_t797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CwaitForTouchDownU3Ec__Iterator9_t797)/* instance_size */
	, sizeof (U3CwaitForTouchDownU3Ec__Iterator9_t797)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<waitTime>c__IteratorA
#include "AssemblyU2DCSharp_gameControl_U3CwaitTimeU3Ec__IteratorA.h"
// Metadata Definition gameControl/<waitTime>c__IteratorA
extern TypeInfo U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo;
// gameControl/<waitTime>c__IteratorA
#include "AssemblyU2DCSharp_gameControl_U3CwaitTimeU3Ec__IteratorAMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitTime>c__IteratorA::.ctor()
MethodInfo U3CwaitTimeU3Ec__IteratorA__ctor_m3431_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA__ctor_m3431/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<waitTime>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 280/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<waitTime>c__IteratorA::System.Collections.IEnumerator.get_Current()
MethodInfo U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 281/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<waitTime>c__IteratorA::MoveNext()
MethodInfo U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitTime>c__IteratorA::Dispose()
MethodInfo U3CwaitTimeU3Ec__IteratorA_Dispose_m3435_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA_Dispose_m3435/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 282/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<waitTime>c__IteratorA::Reset()
MethodInfo U3CwaitTimeU3Ec__IteratorA_Reset_m3436_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CwaitTimeU3Ec__IteratorA_Reset_m3436/* method */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 283/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CwaitTimeU3Ec__IteratorA_t798_MethodInfos[] =
{
	&U3CwaitTimeU3Ec__IteratorA__ctor_m3431_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_Dispose_m3435_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_Reset_m3436_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitTimeU3Ec__IteratorA_t798____U3CtU3E__0_0_FieldInfo = 
{
	"<t>__0"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitTimeU3Ec__IteratorA_t798, ___U3CtU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CwaitTimeU3Ec__IteratorA_t798____seconds_1_FieldInfo = 
{
	"seconds"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitTimeU3Ec__IteratorA_t798, ___seconds_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitTimeU3Ec__IteratorA_t798____U24PC_2_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitTimeU3Ec__IteratorA_t798, ___U24PC_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CwaitTimeU3Ec__IteratorA_t798____U24current_3_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitTimeU3Ec__IteratorA_t798, ___U24current_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CwaitTimeU3Ec__IteratorA_t798____U3CU24U3Eseconds_4_FieldInfo = 
{
	"<$>seconds"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitTimeU3Ec__IteratorA_t798, ___U3CU24U3Eseconds_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CwaitTimeU3Ec__IteratorA_t798_FieldInfos[] =
{
	&U3CwaitTimeU3Ec__IteratorA_t798____U3CtU3E__0_0_FieldInfo,
	&U3CwaitTimeU3Ec__IteratorA_t798____seconds_1_FieldInfo,
	&U3CwaitTimeU3Ec__IteratorA_t798____U24PC_2_FieldInfo,
	&U3CwaitTimeU3Ec__IteratorA_t798____U24current_3_FieldInfo,
	&U3CwaitTimeU3Ec__IteratorA_t798____U3CU24U3Eseconds_4_FieldInfo,
	NULL
};
extern MethodInfo U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432_MethodInfo;
static PropertyInfo U3CwaitTimeU3Ec__IteratorA_t798____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433_MethodInfo;
static PropertyInfo U3CwaitTimeU3Ec__IteratorA_t798____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CwaitTimeU3Ec__IteratorA_t798_PropertyInfos[] =
{
	&U3CwaitTimeU3Ec__IteratorA_t798____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CwaitTimeU3Ec__IteratorA_t798____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CwaitTimeU3Ec__IteratorA_Dispose_m3435_MethodInfo;
extern MethodInfo U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434_MethodInfo;
extern MethodInfo U3CwaitTimeU3Ec__IteratorA_Reset_m3436_MethodInfo;
static Il2CppMethodReference U3CwaitTimeU3Ec__IteratorA_t798_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_Dispose_m3435_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3433_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_MoveNext_m3434_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3432_MethodInfo,
	&U3CwaitTimeU3Ec__IteratorA_Reset_m3436_MethodInfo,
};
static bool U3CwaitTimeU3Ec__IteratorA_t798_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CwaitTimeU3Ec__IteratorA_t798_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CwaitTimeU3Ec__IteratorA_t798_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CwaitTimeU3Ec__IteratorA_t798_0_0_0;
extern Il2CppType U3CwaitTimeU3Ec__IteratorA_t798_1_0_0;
struct U3CwaitTimeU3Ec__IteratorA_t798;
const Il2CppTypeDefinitionMetadata U3CwaitTimeU3Ec__IteratorA_t798_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CwaitTimeU3Ec__IteratorA_t798_InterfacesTypeInfos/* implementedInterfaces */
	, U3CwaitTimeU3Ec__IteratorA_t798_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CwaitTimeU3Ec__IteratorA_t798_VTable/* vtableMethods */
	, U3CwaitTimeU3Ec__IteratorA_t798_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<waitTime>c__IteratorA"/* name */
	, ""/* namespaze */
	, U3CwaitTimeU3Ec__IteratorA_t798_MethodInfos/* methods */
	, U3CwaitTimeU3Ec__IteratorA_t798_PropertyInfos/* properties */
	, U3CwaitTimeU3Ec__IteratorA_t798_FieldInfos/* fields */
	, NULL/* events */
	, &U3CwaitTimeU3Ec__IteratorA_t798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 279/* custom_attributes_cache */
	, &U3CwaitTimeU3Ec__IteratorA_t798_0_0_0/* byval_arg */
	, &U3CwaitTimeU3Ec__IteratorA_t798_1_0_0/* this_arg */
	, &U3CwaitTimeU3Ec__IteratorA_t798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CwaitTimeU3Ec__IteratorA_t798)/* instance_size */
	, sizeof (U3CwaitTimeU3Ec__IteratorA_t798)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<runTutorial>c__IteratorB
#include "AssemblyU2DCSharp_gameControl_U3CrunTutorialU3Ec__IteratorB.h"
// Metadata Definition gameControl/<runTutorial>c__IteratorB
extern TypeInfo U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo;
// gameControl/<runTutorial>c__IteratorB
#include "AssemblyU2DCSharp_gameControl_U3CrunTutorialU3Ec__IteratorBMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<runTutorial>c__IteratorB::.ctor()
MethodInfo U3CrunTutorialU3Ec__IteratorB__ctor_m3437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB__ctor_m3437/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<runTutorial>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 285/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<runTutorial>c__IteratorB::System.Collections.IEnumerator.get_Current()
MethodInfo U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 286/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<runTutorial>c__IteratorB::MoveNext()
MethodInfo U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<runTutorial>c__IteratorB::Dispose()
MethodInfo U3CrunTutorialU3Ec__IteratorB_Dispose_m3441_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB_Dispose_m3441/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 287/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<runTutorial>c__IteratorB::Reset()
MethodInfo U3CrunTutorialU3Ec__IteratorB_Reset_m3442_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CrunTutorialU3Ec__IteratorB_Reset_m3442/* method */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 288/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CrunTutorialU3Ec__IteratorB_t799_MethodInfos[] =
{
	&U3CrunTutorialU3Ec__IteratorB__ctor_m3437_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_Dispose_m3441_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_Reset_m3442_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CrunTutorialU3Ec__IteratorB_t799____U24PC_0_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunTutorialU3Ec__IteratorB_t799, ___U24PC_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CrunTutorialU3Ec__IteratorB_t799____U24current_1_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunTutorialU3Ec__IteratorB_t799, ___U24current_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType gameControl_t794_0_0_3;
FieldInfo U3CrunTutorialU3Ec__IteratorB_t799____U3CU3Ef__this_2_FieldInfo = 
{
	"<>f__this"/* name */
	, &gameControl_t794_0_0_3/* type */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunTutorialU3Ec__IteratorB_t799, ___U3CU3Ef__this_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CrunTutorialU3Ec__IteratorB_t799_FieldInfos[] =
{
	&U3CrunTutorialU3Ec__IteratorB_t799____U24PC_0_FieldInfo,
	&U3CrunTutorialU3Ec__IteratorB_t799____U24current_1_FieldInfo,
	&U3CrunTutorialU3Ec__IteratorB_t799____U3CU3Ef__this_2_FieldInfo,
	NULL
};
extern MethodInfo U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438_MethodInfo;
static PropertyInfo U3CrunTutorialU3Ec__IteratorB_t799____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439_MethodInfo;
static PropertyInfo U3CrunTutorialU3Ec__IteratorB_t799____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CrunTutorialU3Ec__IteratorB_t799_PropertyInfos[] =
{
	&U3CrunTutorialU3Ec__IteratorB_t799____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CrunTutorialU3Ec__IteratorB_t799____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CrunTutorialU3Ec__IteratorB_Dispose_m3441_MethodInfo;
extern MethodInfo U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440_MethodInfo;
extern MethodInfo U3CrunTutorialU3Ec__IteratorB_Reset_m3442_MethodInfo;
static Il2CppMethodReference U3CrunTutorialU3Ec__IteratorB_t799_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_Dispose_m3441_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3439_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_MoveNext_m3440_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438_MethodInfo,
	&U3CrunTutorialU3Ec__IteratorB_Reset_m3442_MethodInfo,
};
static bool U3CrunTutorialU3Ec__IteratorB_t799_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CrunTutorialU3Ec__IteratorB_t799_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CrunTutorialU3Ec__IteratorB_t799_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CrunTutorialU3Ec__IteratorB_t799_0_0_0;
extern Il2CppType U3CrunTutorialU3Ec__IteratorB_t799_1_0_0;
struct U3CrunTutorialU3Ec__IteratorB_t799;
const Il2CppTypeDefinitionMetadata U3CrunTutorialU3Ec__IteratorB_t799_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CrunTutorialU3Ec__IteratorB_t799_InterfacesTypeInfos/* implementedInterfaces */
	, U3CrunTutorialU3Ec__IteratorB_t799_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CrunTutorialU3Ec__IteratorB_t799_VTable/* vtableMethods */
	, U3CrunTutorialU3Ec__IteratorB_t799_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<runTutorial>c__IteratorB"/* name */
	, ""/* namespaze */
	, U3CrunTutorialU3Ec__IteratorB_t799_MethodInfos/* methods */
	, U3CrunTutorialU3Ec__IteratorB_t799_PropertyInfos/* properties */
	, U3CrunTutorialU3Ec__IteratorB_t799_FieldInfos/* fields */
	, NULL/* events */
	, &U3CrunTutorialU3Ec__IteratorB_t799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 284/* custom_attributes_cache */
	, &U3CrunTutorialU3Ec__IteratorB_t799_0_0_0/* byval_arg */
	, &U3CrunTutorialU3Ec__IteratorB_t799_1_0_0/* this_arg */
	, &U3CrunTutorialU3Ec__IteratorB_t799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CrunTutorialU3Ec__IteratorB_t799)/* instance_size */
	, sizeof (U3CrunTutorialU3Ec__IteratorB_t799)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl/<ChangeLevelWait>c__IteratorC
#include "AssemblyU2DCSharp_gameControl_U3CChangeLevelWaitU3Ec__Iterat.h"
// Metadata Definition gameControl/<ChangeLevelWait>c__IteratorC
extern TypeInfo U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo;
// gameControl/<ChangeLevelWait>c__IteratorC
#include "AssemblyU2DCSharp_gameControl_U3CChangeLevelWaitU3Ec__IteratMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<ChangeLevelWait>c__IteratorC::.ctor()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC__ctor_m3443_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC__ctor_m3443/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<ChangeLevelWait>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 290/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameControl/<ChangeLevelWait>c__IteratorC::System.Collections.IEnumerator.get_Current()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 291/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl/<ChangeLevelWait>c__IteratorC::MoveNext()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<ChangeLevelWait>c__IteratorC::Dispose()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 292/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl/<ChangeLevelWait>c__IteratorC::Reset()
MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448/* method */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 293/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CChangeLevelWaitU3Ec__IteratorC_t800_MethodInfos[] =
{
	&U3CChangeLevelWaitU3Ec__IteratorC__ctor_m3443_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CChangeLevelWaitU3Ec__IteratorC_t800____U3CtimerU3E__0_0_FieldInfo = 
{
	"<timer>__0"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* parent */
	, offsetof(U3CChangeLevelWaitU3Ec__IteratorC_t800, ___U3CtimerU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CChangeLevelWaitU3Ec__IteratorC_t800____U24PC_1_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* parent */
	, offsetof(U3CChangeLevelWaitU3Ec__IteratorC_t800, ___U24PC_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CChangeLevelWaitU3Ec__IteratorC_t800____U24current_2_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* parent */
	, offsetof(U3CChangeLevelWaitU3Ec__IteratorC_t800, ___U24current_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CChangeLevelWaitU3Ec__IteratorC_t800_FieldInfos[] =
{
	&U3CChangeLevelWaitU3Ec__IteratorC_t800____U3CtimerU3E__0_0_FieldInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_t800____U24PC_1_FieldInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_t800____U24current_2_FieldInfo,
	NULL
};
extern MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444_MethodInfo;
static PropertyInfo U3CChangeLevelWaitU3Ec__IteratorC_t800____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445_MethodInfo;
static PropertyInfo U3CChangeLevelWaitU3Ec__IteratorC_t800____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CChangeLevelWaitU3Ec__IteratorC_t800_PropertyInfos[] =
{
	&U3CChangeLevelWaitU3Ec__IteratorC_t800____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_t800____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447_MethodInfo;
extern MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446_MethodInfo;
extern MethodInfo U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448_MethodInfo;
static Il2CppMethodReference U3CChangeLevelWaitU3Ec__IteratorC_t800_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444_MethodInfo,
	&U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448_MethodInfo,
};
static bool U3CChangeLevelWaitU3Ec__IteratorC_t800_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CChangeLevelWaitU3Ec__IteratorC_t800_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CChangeLevelWaitU3Ec__IteratorC_t800_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CChangeLevelWaitU3Ec__IteratorC_t800_0_0_0;
extern Il2CppType U3CChangeLevelWaitU3Ec__IteratorC_t800_1_0_0;
struct U3CChangeLevelWaitU3Ec__IteratorC_t800;
const Il2CppTypeDefinitionMetadata U3CChangeLevelWaitU3Ec__IteratorC_t800_DefinitionMetadata = 
{
	&gameControl_t794_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_InterfacesTypeInfos/* implementedInterfaces */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_VTable/* vtableMethods */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ChangeLevelWait>c__IteratorC"/* name */
	, ""/* namespaze */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_MethodInfos/* methods */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_PropertyInfos/* properties */
	, U3CChangeLevelWaitU3Ec__IteratorC_t800_FieldInfos/* fields */
	, NULL/* events */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 289/* custom_attributes_cache */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_0_0_0/* byval_arg */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_1_0_0/* this_arg */
	, &U3CChangeLevelWaitU3Ec__IteratorC_t800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CChangeLevelWaitU3Ec__IteratorC_t800)/* instance_size */
	, sizeof (U3CChangeLevelWaitU3Ec__IteratorC_t800)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameControl
#include "AssemblyU2DCSharp_gameControl.h"
// Metadata Definition gameControl
// gameControl
#include "AssemblyU2DCSharp_gameControlMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::.ctor()
MethodInfo gameControl__ctor_m3449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&gameControl__ctor_m3449/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::.cctor()
MethodInfo gameControl__cctor_m3450_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&gameControl__cctor_m3450/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType gameControl_t794_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// gameControl gameControl::get_Instance()
MethodInfo gameControl_get_Instance_m3451_MethodInfo = 
{
	"get_Instance"/* name */
	, (methodPointerType)&gameControl_get_Instance_m3451/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &gameControl_t794_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::Awake()
MethodInfo gameControl_Awake_m3452_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&gameControl_Awake_m3452/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::callScoreTable()
MethodInfo gameControl_callScoreTable_m3453_MethodInfo = 
{
	"callScoreTable"/* name */
	, (methodPointerType)&gameControl_callScoreTable_m3453/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::Start()
MethodInfo gameControl_Start_m3454_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&gameControl_Start_m3454/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::Update()
MethodInfo gameControl_Update_m3455_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&gameControl_Update_m3455/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::FixedUpdate()
MethodInfo gameControl_FixedUpdate_m3456_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&gameControl_FixedUpdate_m3456/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameControl::checkTutorial()
MethodInfo gameControl_checkTutorial_m3457_MethodInfo = 
{
	"checkTutorial"/* name */
	, (methodPointerType)&gameControl_checkTutorial_m3457/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::toGame()
MethodInfo gameControl_toGame_m3458_MethodInfo = 
{
	"toGame"/* name */
	, (methodPointerType)&gameControl_toGame_m3458/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::startGame()
MethodInfo gameControl_startGame_m3459_MethodInfo = 
{
	"startGame"/* name */
	, (methodPointerType)&gameControl_startGame_m3459/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 258/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::finishGame()
MethodInfo gameControl_finishGame_m3460_MethodInfo = 
{
	"finishGame"/* name */
	, (methodPointerType)&gameControl_finishGame_m3460/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::wait()
MethodInfo gameControl_wait_m3461_MethodInfo = 
{
	"wait"/* name */
	, (methodPointerType)&gameControl_wait_m3461/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 259/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::waitForTouchDown()
MethodInfo gameControl_waitForTouchDown_m3462_MethodInfo = 
{
	"waitForTouchDown"/* name */
	, (methodPointerType)&gameControl_waitForTouchDown_m3462/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 260/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo gameControl_t794_gameControl_waitTime_m3463_ParameterInfos[] = 
{
	{"seconds", 0, 134220485, 0, &Single_t202_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::waitTime(System.Single)
MethodInfo gameControl_waitTime_m3463_MethodInfo = 
{
	"waitTime"/* name */
	, (methodPointerType)&gameControl_waitTime_m3463/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t202/* invoker_method */
	, gameControl_t794_gameControl_waitTime_m3463_ParameterInfos/* parameters */
	, 261/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::runTutorial()
MethodInfo gameControl_runTutorial_m3464_MethodInfo = 
{
	"runTutorial"/* name */
	, (methodPointerType)&gameControl_runTutorial_m3464/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 262/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo gameControl_t794_gameControl_ChangeLevelWait_m3465_ParameterInfos[] = 
{
	{"seconds", 0, 134220486, 0, &Single_t202_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameControl::ChangeLevelWait(System.Single)
MethodInfo gameControl_ChangeLevelWait_m3465_MethodInfo = 
{
	"ChangeLevelWait"/* name */
	, (methodPointerType)&gameControl_ChangeLevelWait_m3465/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t202/* invoker_method */
	, gameControl_t794_gameControl_ChangeLevelWait_m3465_ParameterInfos/* parameters */
	, 263/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::levelUpgrade()
MethodInfo gameControl_levelUpgrade_m3466_MethodInfo = 
{
	"levelUpgrade"/* name */
	, (methodPointerType)&gameControl_levelUpgrade_m3466/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::setScores()
MethodInfo gameControl_setScores_m3467_MethodInfo = 
{
	"setScores"/* name */
	, (methodPointerType)&gameControl_setScores_m3467/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::setLastKills()
MethodInfo gameControl_setLastKills_m3468_MethodInfo = 
{
	"setLastKills"/* name */
	, (methodPointerType)&gameControl_setLastKills_m3468/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::clearEnemies()
MethodInfo gameControl_clearEnemies_m3469_MethodInfo = 
{
	"clearEnemies"/* name */
	, (methodPointerType)&gameControl_clearEnemies_m3469/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::clearPlayer()
MethodInfo gameControl_clearPlayer_m3470_MethodInfo = 
{
	"clearPlayer"/* name */
	, (methodPointerType)&gameControl_clearPlayer_m3470/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::clearPowerUps()
MethodInfo gameControl_clearPowerUps_m3471_MethodInfo = 
{
	"clearPowerUps"/* name */
	, (methodPointerType)&gameControl_clearPowerUps_m3471/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject gameControl::createNaveProxy()
MethodInfo gameControl_createNaveProxy_m3472_MethodInfo = 
{
	"createNaveProxy"/* name */
	, (methodPointerType)&gameControl_createNaveProxy_m3472/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t144_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::initMenu()
MethodInfo gameControl_initMenu_m3473_MethodInfo = 
{
	"initMenu"/* name */
	, (methodPointerType)&gameControl_initMenu_m3473/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::removeMenu()
MethodInfo gameControl_removeMenu_m3474_MethodInfo = 
{
	"removeMenu"/* name */
	, (methodPointerType)&gameControl_removeMenu_m3474/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::toWeaponRoom()
MethodInfo gameControl_toWeaponRoom_m3475_MethodInfo = 
{
	"toWeaponRoom"/* name */
	, (methodPointerType)&gameControl_toWeaponRoom_m3475/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::removeWeaponRoom()
MethodInfo gameControl_removeWeaponRoom_m3476_MethodInfo = 
{
	"removeWeaponRoom"/* name */
	, (methodPointerType)&gameControl_removeWeaponRoom_m3476/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::toGameOverRoom()
MethodInfo gameControl_toGameOverRoom_m3477_MethodInfo = 
{
	"toGameOverRoom"/* name */
	, (methodPointerType)&gameControl_toGameOverRoom_m3477/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::removeGameOverRoom()
MethodInfo gameControl_removeGameOverRoom_m3478_MethodInfo = 
{
	"removeGameOverRoom"/* name */
	, (methodPointerType)&gameControl_removeGameOverRoom_m3478/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::toStoreRoom()
MethodInfo gameControl_toStoreRoom_m3479_MethodInfo = 
{
	"toStoreRoom"/* name */
	, (methodPointerType)&gameControl_toStoreRoom_m3479/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::removeStoreRoom()
MethodInfo gameControl_removeStoreRoom_m3480_MethodInfo = 
{
	"removeStoreRoom"/* name */
	, (methodPointerType)&gameControl_removeStoreRoom_m3480/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::resetTutorial()
MethodInfo gameControl_resetTutorial_m3481_MethodInfo = 
{
	"resetTutorial"/* name */
	, (methodPointerType)&gameControl_resetTutorial_m3481/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::finishTutorial()
MethodInfo gameControl_finishTutorial_m3482_MethodInfo = 
{
	"finishTutorial"/* name */
	, (methodPointerType)&gameControl_finishTutorial_m3482/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::OnApplicationQuit()
MethodInfo gameControl_OnApplicationQuit_m3483_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&gameControl_OnApplicationQuit_m3483/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::hideAgujeros()
MethodInfo gameControl_hideAgujeros_m3484_MethodInfo = 
{
	"hideAgujeros"/* name */
	, (methodPointerType)&gameControl_hideAgujeros_m3484/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::showAgujeros()
MethodInfo gameControl_showAgujeros_m3485_MethodInfo = 
{
	"showAgujeros"/* name */
	, (methodPointerType)&gameControl_showAgujeros_m3485/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::failingTutorial()
MethodInfo gameControl_failingTutorial_m3486_MethodInfo = 
{
	"failingTutorial"/* name */
	, (methodPointerType)&gameControl_failingTutorial_m3486/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameControl::checkRemoveTutorial()
MethodInfo gameControl_checkRemoveTutorial_m3487_MethodInfo = 
{
	"checkRemoveTutorial"/* name */
	, (methodPointerType)&gameControl_checkRemoveTutorial_m3487/* method */
	, &gameControl_t794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* gameControl_t794_MethodInfos[] =
{
	&gameControl__ctor_m3449_MethodInfo,
	&gameControl__cctor_m3450_MethodInfo,
	&gameControl_get_Instance_m3451_MethodInfo,
	&gameControl_Awake_m3452_MethodInfo,
	&gameControl_callScoreTable_m3453_MethodInfo,
	&gameControl_Start_m3454_MethodInfo,
	&gameControl_Update_m3455_MethodInfo,
	&gameControl_FixedUpdate_m3456_MethodInfo,
	&gameControl_checkTutorial_m3457_MethodInfo,
	&gameControl_toGame_m3458_MethodInfo,
	&gameControl_startGame_m3459_MethodInfo,
	&gameControl_finishGame_m3460_MethodInfo,
	&gameControl_wait_m3461_MethodInfo,
	&gameControl_waitForTouchDown_m3462_MethodInfo,
	&gameControl_waitTime_m3463_MethodInfo,
	&gameControl_runTutorial_m3464_MethodInfo,
	&gameControl_ChangeLevelWait_m3465_MethodInfo,
	&gameControl_levelUpgrade_m3466_MethodInfo,
	&gameControl_setScores_m3467_MethodInfo,
	&gameControl_setLastKills_m3468_MethodInfo,
	&gameControl_clearEnemies_m3469_MethodInfo,
	&gameControl_clearPlayer_m3470_MethodInfo,
	&gameControl_clearPowerUps_m3471_MethodInfo,
	&gameControl_createNaveProxy_m3472_MethodInfo,
	&gameControl_initMenu_m3473_MethodInfo,
	&gameControl_removeMenu_m3474_MethodInfo,
	&gameControl_toWeaponRoom_m3475_MethodInfo,
	&gameControl_removeWeaponRoom_m3476_MethodInfo,
	&gameControl_toGameOverRoom_m3477_MethodInfo,
	&gameControl_removeGameOverRoom_m3478_MethodInfo,
	&gameControl_toStoreRoom_m3479_MethodInfo,
	&gameControl_removeStoreRoom_m3480_MethodInfo,
	&gameControl_resetTutorial_m3481_MethodInfo,
	&gameControl_finishTutorial_m3482_MethodInfo,
	&gameControl_OnApplicationQuit_m3483_MethodInfo,
	&gameControl_hideAgujeros_m3484_MethodInfo,
	&gameControl_showAgujeros_m3485_MethodInfo,
	&gameControl_failingTutorial_m3486_MethodInfo,
	&gameControl_checkRemoveTutorial_m3487_MethodInfo,
	NULL
};
extern Il2CppType gameControl_t794_0_0_17;
FieldInfo gameControl_t794____instance_2_FieldInfo = 
{
	"instance"/* name */
	, &gameControl_t794_0_0_17/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___instance_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_6;
FieldInfo gameControl_t794____status_3_FieldInfo = 
{
	"status"/* name */
	, &State_t793_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___status_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo gameControl_t794____factorLevel_4_FieldInfo = 
{
	"factorLevel"/* name */
	, &Int32_t189_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___factorLevel_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo gameControl_t794____tempLevel_5_FieldInfo = 
{
	"tempLevel"/* name */
	, &Int32_t189_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___tempLevel_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType State_t793_0_0_22;
FieldInfo gameControl_t794____currentState_6_FieldInfo = 
{
	"currentState"/* name */
	, &State_t793_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___currentState_6)/* offset */
	, 256/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_22;
FieldInfo gameControl_t794____midPos_7_FieldInfo = 
{
	"midPos"/* name */
	, &Vector2_t739_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___midPos_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_22;
FieldInfo gameControl_t794____highPos_8_FieldInfo = 
{
	"highPos"/* name */
	, &Vector2_t739_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___highPos_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo gameControl_t794____posH_9_FieldInfo = 
{
	"posH"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___posH_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____admob_10_FieldInfo = 
{
	"admob"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___admob_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____player_11_FieldInfo = 
{
	"player"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___player_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo gameControl_t794____currentPlayer_12_FieldInfo = 
{
	"currentPlayer"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentPlayer_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____enemyController_13_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___enemyController_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____currentEnemyController_14_FieldInfo = 
{
	"currentEnemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentEnemyController_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyController_t785_0_0_1;
FieldInfo gameControl_t794_____enemyController_15_FieldInfo = 
{
	"_enemyController"/* name */
	, &enemyController_t785_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ____enemyController_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____menuObj_16_FieldInfo = 
{
	"menuObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___menuObj_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo gameControl_t794____currentMenu_17_FieldInfo = 
{
	"currentMenu"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentMenu_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo gameControl_t794____currentWeaponRoom_18_FieldInfo = 
{
	"currentWeaponRoom"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentWeaponRoom_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo gameControl_t794____currentStoreRoom_19_FieldInfo = 
{
	"currentStoreRoom"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentStoreRoom_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____tutorialControllerObj_20_FieldInfo = 
{
	"tutorialControllerObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___tutorialControllerObj_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____weaponRoom_21_FieldInfo = 
{
	"weaponRoom"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___weaponRoom_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____storeObj_22_FieldInfo = 
{
	"storeObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___storeObj_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____bombaObj_23_FieldInfo = 
{
	"bombaObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___bombaObj_23)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____dustObj_24_FieldInfo = 
{
	"dustObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___dustObj_24)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____explosionSmall_25_FieldInfo = 
{
	"explosionSmall"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___explosionSmall_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____paquete_26_FieldInfo = 
{
	"paquete"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___paquete_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____gameOverObj_27_FieldInfo = 
{
	"gameOverObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___gameOverObj_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____currentGameOver_28_FieldInfo = 
{
	"currentGameOver"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentGameOver_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____bullet_29_FieldInfo = 
{
	"bullet"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___bullet_29)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____bulletThin_30_FieldInfo = 
{
	"bulletThin"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___bulletThin_30)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____bulletLaser_31_FieldInfo = 
{
	"bulletLaser"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___bulletLaser_31)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____raycastObj_32_FieldInfo = 
{
	"raycastObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___raycastObj_32)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____circController_33_FieldInfo = 
{
	"circController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___circController_33)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____rayo_34_FieldInfo = 
{
	"rayo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___rayo_34)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____music_35_FieldInfo = 
{
	"music"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___music_35)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioSource_t755_0_0_1;
FieldInfo gameControl_t794_____music_36_FieldInfo = 
{
	"_music"/* name */
	, &AudioSource_t755_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ____music_36)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____bomba_37_FieldInfo = 
{
	"bomba"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___bomba_37)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____rayoBomba_38_FieldInfo = 
{
	"rayoBomba"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___rayoBomba_38)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameControl_t794____naveProxy_39_FieldInfo = 
{
	"naveProxy"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___naveProxy_39)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_1;
FieldInfo gameControl_t794____currentNaveProxy_40_FieldInfo = 
{
	"currentNaveProxy"/* name */
	, &GameObject_t144_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___currentNaveProxy_40)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo gameControl_t794____gatheringTime_41_FieldInfo = 
{
	"gatheringTime"/* name */
	, &Single_t202_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___gatheringTime_41)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo gameControl_t794____stageTime_42_FieldInfo = 
{
	"stageTime"/* name */
	, &Single_t202_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___stageTime_42)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo gameControl_t794____maxNaveProxy_43_FieldInfo = 
{
	"maxNaveProxy"/* name */
	, &Int32_t189_0_0_6/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___maxNaveProxy_43)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo gameControl_t794____newWeaponFlag_44_FieldInfo = 
{
	"newWeaponFlag"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___newWeaponFlag_44)/* offset */
	, 257/* custom_attributes_cache */

};
extern Il2CppType GameCenterSingleton_t730_0_0_1;
FieldInfo gameControl_t794____gameCenter_45_FieldInfo = 
{
	"gameCenter"/* name */
	, &GameCenterSingleton_t730_0_0_1/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794, ___gameCenter_45)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo gameControl_t794____slowMotion_46_FieldInfo = 
{
	"slowMotion"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___slowMotion_46)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo gameControl_t794____slowDead_47_FieldInfo = 
{
	"slowDead"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &gameControl_t794_il2cpp_TypeInfo/* parent */
	, offsetof(gameControl_t794_StaticFields, ___slowDead_47)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* gameControl_t794_FieldInfos[] =
{
	&gameControl_t794____instance_2_FieldInfo,
	&gameControl_t794____status_3_FieldInfo,
	&gameControl_t794____factorLevel_4_FieldInfo,
	&gameControl_t794____tempLevel_5_FieldInfo,
	&gameControl_t794____currentState_6_FieldInfo,
	&gameControl_t794____midPos_7_FieldInfo,
	&gameControl_t794____highPos_8_FieldInfo,
	&gameControl_t794____posH_9_FieldInfo,
	&gameControl_t794____admob_10_FieldInfo,
	&gameControl_t794____player_11_FieldInfo,
	&gameControl_t794____currentPlayer_12_FieldInfo,
	&gameControl_t794____enemyController_13_FieldInfo,
	&gameControl_t794____currentEnemyController_14_FieldInfo,
	&gameControl_t794_____enemyController_15_FieldInfo,
	&gameControl_t794____menuObj_16_FieldInfo,
	&gameControl_t794____currentMenu_17_FieldInfo,
	&gameControl_t794____currentWeaponRoom_18_FieldInfo,
	&gameControl_t794____currentStoreRoom_19_FieldInfo,
	&gameControl_t794____tutorialControllerObj_20_FieldInfo,
	&gameControl_t794____weaponRoom_21_FieldInfo,
	&gameControl_t794____storeObj_22_FieldInfo,
	&gameControl_t794____bombaObj_23_FieldInfo,
	&gameControl_t794____dustObj_24_FieldInfo,
	&gameControl_t794____explosionSmall_25_FieldInfo,
	&gameControl_t794____paquete_26_FieldInfo,
	&gameControl_t794____gameOverObj_27_FieldInfo,
	&gameControl_t794____currentGameOver_28_FieldInfo,
	&gameControl_t794____bullet_29_FieldInfo,
	&gameControl_t794____bulletThin_30_FieldInfo,
	&gameControl_t794____bulletLaser_31_FieldInfo,
	&gameControl_t794____raycastObj_32_FieldInfo,
	&gameControl_t794____circController_33_FieldInfo,
	&gameControl_t794____rayo_34_FieldInfo,
	&gameControl_t794____music_35_FieldInfo,
	&gameControl_t794_____music_36_FieldInfo,
	&gameControl_t794____bomba_37_FieldInfo,
	&gameControl_t794____rayoBomba_38_FieldInfo,
	&gameControl_t794____naveProxy_39_FieldInfo,
	&gameControl_t794____currentNaveProxy_40_FieldInfo,
	&gameControl_t794____gatheringTime_41_FieldInfo,
	&gameControl_t794____stageTime_42_FieldInfo,
	&gameControl_t794____maxNaveProxy_43_FieldInfo,
	&gameControl_t794____newWeaponFlag_44_FieldInfo,
	&gameControl_t794____gameCenter_45_FieldInfo,
	&gameControl_t794____slowMotion_46_FieldInfo,
	&gameControl_t794____slowDead_47_FieldInfo,
	NULL
};
extern MethodInfo gameControl_get_Instance_m3451_MethodInfo;
static PropertyInfo gameControl_t794____Instance_PropertyInfo = 
{
	&gameControl_t794_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &gameControl_get_Instance_m3451_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* gameControl_t794_PropertyInfos[] =
{
	&gameControl_t794____Instance_PropertyInfo,
	NULL
};
static const Il2CppType* gameControl_t794_il2cpp_TypeInfo__nestedTypes[7] =
{
	&State_t793_0_0_0,
	&U3CstartGameU3Ec__Iterator7_t795_0_0_0,
	&U3CwaitU3Ec__Iterator8_t796_0_0_0,
	&U3CwaitForTouchDownU3Ec__Iterator9_t797_0_0_0,
	&U3CwaitTimeU3Ec__IteratorA_t798_0_0_0,
	&U3CrunTutorialU3Ec__IteratorB_t799_0_0_0,
	&U3CChangeLevelWaitU3Ec__IteratorC_t800_0_0_0,
};
static Il2CppMethodReference gameControl_t794_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool gameControl_t794_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType gameControl_t794_1_0_0;
struct gameControl_t794;
const Il2CppTypeDefinitionMetadata gameControl_t794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, gameControl_t794_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, gameControl_t794_VTable/* vtableMethods */
	, gameControl_t794_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo gameControl_t794_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "gameControl"/* name */
	, ""/* namespaze */
	, gameControl_t794_MethodInfos/* methods */
	, gameControl_t794_PropertyInfos/* properties */
	, gameControl_t794_FieldInfos/* fields */
	, NULL/* events */
	, &gameControl_t794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &gameControl_t794_0_0_0/* byval_arg */
	, &gameControl_t794_1_0_0/* this_arg */
	, &gameControl_t794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (gameControl_t794)/* instance_size */
	, sizeof (gameControl_t794)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(gameControl_t794_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 39/* method_count */
	, 1/* property_count */
	, 46/* field_count */
	, 0/* event_count */
	, 7/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// gameOverSrc/<run>c__IteratorD
#include "AssemblyU2DCSharp_gameOverSrc_U3CrunU3Ec__IteratorD.h"
// Metadata Definition gameOverSrc/<run>c__IteratorD
extern TypeInfo U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo;
// gameOverSrc/<run>c__IteratorD
#include "AssemblyU2DCSharp_gameOverSrc_U3CrunU3Ec__IteratorDMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<run>c__IteratorD::.ctor()
MethodInfo U3CrunU3Ec__IteratorD__ctor_m3488_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD__ctor_m3488/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameOverSrc/<run>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 297/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameOverSrc/<run>c__IteratorD::System.Collections.IEnumerator.get_Current()
MethodInfo U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 298/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameOverSrc/<run>c__IteratorD::MoveNext()
MethodInfo U3CrunU3Ec__IteratorD_MoveNext_m3491_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD_MoveNext_m3491/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<run>c__IteratorD::Dispose()
MethodInfo U3CrunU3Ec__IteratorD_Dispose_m3492_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD_Dispose_m3492/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 299/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<run>c__IteratorD::Reset()
MethodInfo U3CrunU3Ec__IteratorD_Reset_m3493_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CrunU3Ec__IteratorD_Reset_m3493/* method */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 300/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CrunU3Ec__IteratorD_t802_MethodInfos[] =
{
	&U3CrunU3Ec__IteratorD__ctor_m3488_MethodInfo,
	&U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489_MethodInfo,
	&U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490_MethodInfo,
	&U3CrunU3Ec__IteratorD_MoveNext_m3491_MethodInfo,
	&U3CrunU3Ec__IteratorD_Dispose_m3492_MethodInfo,
	&U3CrunU3Ec__IteratorD_Reset_m3493_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CrunU3Ec__IteratorD_t802____U24PC_0_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunU3Ec__IteratorD_t802, ___U24PC_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CrunU3Ec__IteratorD_t802____U24current_1_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunU3Ec__IteratorD_t802, ___U24current_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType gameOverSrc_t801_0_0_3;
FieldInfo U3CrunU3Ec__IteratorD_t802____U3CU3Ef__this_2_FieldInfo = 
{
	"<>f__this"/* name */
	, &gameOverSrc_t801_0_0_3/* type */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* parent */
	, offsetof(U3CrunU3Ec__IteratorD_t802, ___U3CU3Ef__this_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CrunU3Ec__IteratorD_t802_FieldInfos[] =
{
	&U3CrunU3Ec__IteratorD_t802____U24PC_0_FieldInfo,
	&U3CrunU3Ec__IteratorD_t802____U24current_1_FieldInfo,
	&U3CrunU3Ec__IteratorD_t802____U3CU3Ef__this_2_FieldInfo,
	NULL
};
extern MethodInfo U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489_MethodInfo;
static PropertyInfo U3CrunU3Ec__IteratorD_t802____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490_MethodInfo;
static PropertyInfo U3CrunU3Ec__IteratorD_t802____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CrunU3Ec__IteratorD_t802_PropertyInfos[] =
{
	&U3CrunU3Ec__IteratorD_t802____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CrunU3Ec__IteratorD_t802____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CrunU3Ec__IteratorD_Dispose_m3492_MethodInfo;
extern MethodInfo U3CrunU3Ec__IteratorD_MoveNext_m3491_MethodInfo;
extern MethodInfo U3CrunU3Ec__IteratorD_Reset_m3493_MethodInfo;
static Il2CppMethodReference U3CrunU3Ec__IteratorD_t802_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CrunU3Ec__IteratorD_Dispose_m3492_MethodInfo,
	&U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490_MethodInfo,
	&U3CrunU3Ec__IteratorD_MoveNext_m3491_MethodInfo,
	&U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489_MethodInfo,
	&U3CrunU3Ec__IteratorD_Reset_m3493_MethodInfo,
};
static bool U3CrunU3Ec__IteratorD_t802_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CrunU3Ec__IteratorD_t802_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CrunU3Ec__IteratorD_t802_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CrunU3Ec__IteratorD_t802_0_0_0;
extern Il2CppType U3CrunU3Ec__IteratorD_t802_1_0_0;
extern TypeInfo gameOverSrc_t801_il2cpp_TypeInfo;
extern Il2CppType gameOverSrc_t801_0_0_0;
struct U3CrunU3Ec__IteratorD_t802;
const Il2CppTypeDefinitionMetadata U3CrunU3Ec__IteratorD_t802_DefinitionMetadata = 
{
	&gameOverSrc_t801_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CrunU3Ec__IteratorD_t802_InterfacesTypeInfos/* implementedInterfaces */
	, U3CrunU3Ec__IteratorD_t802_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CrunU3Ec__IteratorD_t802_VTable/* vtableMethods */
	, U3CrunU3Ec__IteratorD_t802_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<run>c__IteratorD"/* name */
	, ""/* namespaze */
	, U3CrunU3Ec__IteratorD_t802_MethodInfos/* methods */
	, U3CrunU3Ec__IteratorD_t802_PropertyInfos/* properties */
	, U3CrunU3Ec__IteratorD_t802_FieldInfos/* fields */
	, NULL/* events */
	, &U3CrunU3Ec__IteratorD_t802_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 296/* custom_attributes_cache */
	, &U3CrunU3Ec__IteratorD_t802_0_0_0/* byval_arg */
	, &U3CrunU3Ec__IteratorD_t802_1_0_0/* this_arg */
	, &U3CrunU3Ec__IteratorD_t802_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CrunU3Ec__IteratorD_t802)/* instance_size */
	, sizeof (U3CrunU3Ec__IteratorD_t802)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameOverSrc/<Show>c__IteratorE
#include "AssemblyU2DCSharp_gameOverSrc_U3CShowU3Ec__IteratorE.h"
// Metadata Definition gameOverSrc/<Show>c__IteratorE
extern TypeInfo U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo;
// gameOverSrc/<Show>c__IteratorE
#include "AssemblyU2DCSharp_gameOverSrc_U3CShowU3Ec__IteratorEMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<Show>c__IteratorE::.ctor()
MethodInfo U3CShowU3Ec__IteratorE__ctor_m3494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE__ctor_m3494/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameOverSrc/<Show>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 302/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object gameOverSrc/<Show>c__IteratorE::System.Collections.IEnumerator.get_Current()
MethodInfo U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 303/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameOverSrc/<Show>c__IteratorE::MoveNext()
MethodInfo U3CShowU3Ec__IteratorE_MoveNext_m3497_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE_MoveNext_m3497/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<Show>c__IteratorE::Dispose()
MethodInfo U3CShowU3Ec__IteratorE_Dispose_m3498_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE_Dispose_m3498/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 304/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc/<Show>c__IteratorE::Reset()
MethodInfo U3CShowU3Ec__IteratorE_Reset_m3499_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CShowU3Ec__IteratorE_Reset_m3499/* method */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 305/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CShowU3Ec__IteratorE_t803_MethodInfos[] =
{
	&U3CShowU3Ec__IteratorE__ctor_m3494_MethodInfo,
	&U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495_MethodInfo,
	&U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496_MethodInfo,
	&U3CShowU3Ec__IteratorE_MoveNext_m3497_MethodInfo,
	&U3CShowU3Ec__IteratorE_Dispose_m3498_MethodInfo,
	&U3CShowU3Ec__IteratorE_Reset_m3499_MethodInfo,
	NULL
};
extern Il2CppType Color_t747_0_0_3;
FieldInfo U3CShowU3Ec__IteratorE_t803____U3CbackColorU3E__0_0_FieldInfo = 
{
	"<backColor>__0"/* name */
	, &Color_t747_0_0_3/* type */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, offsetof(U3CShowU3Ec__IteratorE_t803, ___U3CbackColorU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_3;
FieldInfo U3CShowU3Ec__IteratorE_t803____U3CisPadU3E__1_1_FieldInfo = 
{
	"<isPad>__1"/* name */
	, &Boolean_t203_0_0_3/* type */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, offsetof(U3CShowU3Ec__IteratorE_t803, ___U3CisPadU3E__1_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CShowU3Ec__IteratorE_t803____U24PC_2_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, offsetof(U3CShowU3Ec__IteratorE_t803, ___U24PC_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CShowU3Ec__IteratorE_t803____U24current_3_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, offsetof(U3CShowU3Ec__IteratorE_t803, ___U24current_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType gameOverSrc_t801_0_0_3;
FieldInfo U3CShowU3Ec__IteratorE_t803____U3CU3Ef__this_4_FieldInfo = 
{
	"<>f__this"/* name */
	, &gameOverSrc_t801_0_0_3/* type */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, offsetof(U3CShowU3Ec__IteratorE_t803, ___U3CU3Ef__this_4)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CShowU3Ec__IteratorE_t803_FieldInfos[] =
{
	&U3CShowU3Ec__IteratorE_t803____U3CbackColorU3E__0_0_FieldInfo,
	&U3CShowU3Ec__IteratorE_t803____U3CisPadU3E__1_1_FieldInfo,
	&U3CShowU3Ec__IteratorE_t803____U24PC_2_FieldInfo,
	&U3CShowU3Ec__IteratorE_t803____U24current_3_FieldInfo,
	&U3CShowU3Ec__IteratorE_t803____U3CU3Ef__this_4_FieldInfo,
	NULL
};
extern MethodInfo U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495_MethodInfo;
static PropertyInfo U3CShowU3Ec__IteratorE_t803____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496_MethodInfo;
static PropertyInfo U3CShowU3Ec__IteratorE_t803____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CShowU3Ec__IteratorE_t803_PropertyInfos[] =
{
	&U3CShowU3Ec__IteratorE_t803____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CShowU3Ec__IteratorE_t803____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CShowU3Ec__IteratorE_Dispose_m3498_MethodInfo;
extern MethodInfo U3CShowU3Ec__IteratorE_MoveNext_m3497_MethodInfo;
extern MethodInfo U3CShowU3Ec__IteratorE_Reset_m3499_MethodInfo;
static Il2CppMethodReference U3CShowU3Ec__IteratorE_t803_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CShowU3Ec__IteratorE_Dispose_m3498_MethodInfo,
	&U3CShowU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m3496_MethodInfo,
	&U3CShowU3Ec__IteratorE_MoveNext_m3497_MethodInfo,
	&U3CShowU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3495_MethodInfo,
	&U3CShowU3Ec__IteratorE_Reset_m3499_MethodInfo,
};
static bool U3CShowU3Ec__IteratorE_t803_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CShowU3Ec__IteratorE_t803_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CShowU3Ec__IteratorE_t803_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CShowU3Ec__IteratorE_t803_0_0_0;
extern Il2CppType U3CShowU3Ec__IteratorE_t803_1_0_0;
struct U3CShowU3Ec__IteratorE_t803;
const Il2CppTypeDefinitionMetadata U3CShowU3Ec__IteratorE_t803_DefinitionMetadata = 
{
	&gameOverSrc_t801_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CShowU3Ec__IteratorE_t803_InterfacesTypeInfos/* implementedInterfaces */
	, U3CShowU3Ec__IteratorE_t803_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CShowU3Ec__IteratorE_t803_VTable/* vtableMethods */
	, U3CShowU3Ec__IteratorE_t803_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Show>c__IteratorE"/* name */
	, ""/* namespaze */
	, U3CShowU3Ec__IteratorE_t803_MethodInfos/* methods */
	, U3CShowU3Ec__IteratorE_t803_PropertyInfos/* properties */
	, U3CShowU3Ec__IteratorE_t803_FieldInfos/* fields */
	, NULL/* events */
	, &U3CShowU3Ec__IteratorE_t803_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 301/* custom_attributes_cache */
	, &U3CShowU3Ec__IteratorE_t803_0_0_0/* byval_arg */
	, &U3CShowU3Ec__IteratorE_t803_1_0_0/* this_arg */
	, &U3CShowU3Ec__IteratorE_t803_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CShowU3Ec__IteratorE_t803)/* instance_size */
	, sizeof (U3CShowU3Ec__IteratorE_t803)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// gameOverSrc
#include "AssemblyU2DCSharp_gameOverSrc.h"
// Metadata Definition gameOverSrc
// gameOverSrc
#include "AssemblyU2DCSharp_gameOverSrcMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc::.ctor()
MethodInfo gameOverSrc__ctor_m3500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&gameOverSrc__ctor_m3500/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc::Start()
MethodInfo gameOverSrc_Start_m3501_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&gameOverSrc_Start_m3501/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc::Update()
MethodInfo gameOverSrc_Update_m3502_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&gameOverSrc_Update_m3502/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameOverSrc::run()
MethodInfo gameOverSrc_run_m3503_MethodInfo = 
{
	"run"/* name */
	, (methodPointerType)&gameOverSrc_run_m3503/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 294/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator gameOverSrc::Show()
MethodInfo gameOverSrc_Show_m3504_MethodInfo = 
{
	"Show"/* name */
	, (methodPointerType)&gameOverSrc_Show_m3504/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 295/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc::OnGUI()
MethodInfo gameOverSrc_OnGUI_m3505_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&gameOverSrc_OnGUI_m3505/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t189_0_0_0;
extern void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args);
// System.Int32 gameOverSrc::getFontsize()
MethodInfo gameOverSrc_getFontsize_m3506_MethodInfo = 
{
	"getFontsize"/* name */
	, (methodPointerType)&gameOverSrc_getFontsize_m3506/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t189_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t189/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean gameOverSrc::isReady()
MethodInfo gameOverSrc_isReady_m3507_MethodInfo = 
{
	"isReady"/* name */
	, (methodPointerType)&gameOverSrc_isReady_m3507/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo gameOverSrc_t801_gameOverSrc_setReady_m3508_ParameterInfos[] = 
{
	{"readyState", 0, 134220487, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void gameOverSrc::setReady(System.Boolean)
MethodInfo gameOverSrc_setReady_m3508_MethodInfo = 
{
	"setReady"/* name */
	, (methodPointerType)&gameOverSrc_setReady_m3508/* method */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, gameOverSrc_t801_gameOverSrc_setReady_m3508_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* gameOverSrc_t801_MethodInfos[] =
{
	&gameOverSrc__ctor_m3500_MethodInfo,
	&gameOverSrc_Start_m3501_MethodInfo,
	&gameOverSrc_Update_m3502_MethodInfo,
	&gameOverSrc_run_m3503_MethodInfo,
	&gameOverSrc_Show_m3504_MethodInfo,
	&gameOverSrc_OnGUI_m3505_MethodInfo,
	&gameOverSrc_getFontsize_m3506_MethodInfo,
	&gameOverSrc_isReady_m3507_MethodInfo,
	&gameOverSrc_setReady_m3508_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gameOverSrc_t801____gameControlObj_2_FieldInfo = 
{
	"gameControlObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___gameControlObj_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo gameOverSrc_t801____isGameOverReady_3_FieldInfo = 
{
	"isGameOverReady"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801_StaticFields, ___isGameOverReady_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____marqueeSt_4_FieldInfo = 
{
	"marqueeSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___marqueeSt_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____marqueeStLeft_5_FieldInfo = 
{
	"marqueeStLeft"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___marqueeStLeft_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____lowBarSt_6_FieldInfo = 
{
	"lowBarSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___lowBarSt_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____lowBarPlaySt_7_FieldInfo = 
{
	"lowBarPlaySt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___lowBarPlaySt_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____storeButtonSt_8_FieldInfo = 
{
	"storeButtonSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___storeButtonSt_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_22;
FieldInfo gameOverSrc_t801____bulletsHUD_9_FieldInfo = 
{
	"bulletsHUD"/* name */
	, &Rect_t738_0_0_22/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801_StaticFields, ___bulletsHUD_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____bulletsSt_10_FieldInfo = 
{
	"bulletsSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___bulletsSt_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____marqueeBigSt_11_FieldInfo = 
{
	"marqueeBigSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___marqueeBigSt_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo gameOverSrc_t801____boxStyle_12_FieldInfo = 
{
	"boxStyle"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___boxStyle_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____maxText_13_FieldInfo = 
{
	"maxText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___maxText_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____lastText_14_FieldInfo = 
{
	"lastText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___lastText_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____killsText_15_FieldInfo = 
{
	"killsText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___killsText_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____stageTimeText_16_FieldInfo = 
{
	"stageTimeText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___stageTimeText_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____timeText_17_FieldInfo = 
{
	"timeText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___timeText_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo gameOverSrc_t801____coinsText_18_FieldInfo = 
{
	"coinsText"/* name */
	, &String_t_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___coinsText_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo gameOverSrc_t801____messageRect_19_FieldInfo = 
{
	"messageRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___messageRect_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo gameOverSrc_t801____messageMax_20_FieldInfo = 
{
	"messageMax"/* name */
	, &Rect_t738_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___messageMax_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo gameOverSrc_t801____messageLast_21_FieldInfo = 
{
	"messageLast"/* name */
	, &Rect_t738_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___messageLast_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo gameOverSrc_t801____boxRect_22_FieldInfo = 
{
	"boxRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___boxRect_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo gameOverSrc_t801____offsetLetter_23_FieldInfo = 
{
	"offsetLetter"/* name */
	, &Single_t202_0_0_6/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___offsetLetter_23)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo gameOverSrc_t801____startPos_24_FieldInfo = 
{
	"startPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___startPos_24)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo gameOverSrc_t801____endPos_25_FieldInfo = 
{
	"endPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___endPos_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo gameOverSrc_t801____counterK_26_FieldInfo = 
{
	"counterK"/* name */
	, &Int32_t189_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___counterK_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo gameOverSrc_t801____counterL_27_FieldInfo = 
{
	"counterL"/* name */
	, &Int32_t189_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___counterL_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo gameOverSrc_t801____counterM_28_FieldInfo = 
{
	"counterM"/* name */
	, &Int32_t189_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___counterM_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo gameOverSrc_t801____counterC_29_FieldInfo = 
{
	"counterC"/* name */
	, &Int32_t189_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___counterC_29)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo gameOverSrc_t801____counterDeathColor_30_FieldInfo = 
{
	"counterDeathColor"/* name */
	, &Single_t202_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___counterDeathColor_30)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo gameOverSrc_t801____v_31_FieldInfo = 
{
	"v"/* name */
	, &Single_t202_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___v_31)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo gameOverSrc_t801____xU_32_FieldInfo = 
{
	"xU"/* name */
	, &Single_t202_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___xU_32)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo gameOverSrc_t801____yU_33_FieldInfo = 
{
	"yU"/* name */
	, &Single_t202_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___yU_33)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Color_t747_0_0_1;
FieldInfo gameOverSrc_t801____cc_34_FieldInfo = 
{
	"cc"/* name */
	, &Color_t747_0_0_1/* type */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* parent */
	, offsetof(gameOverSrc_t801, ___cc_34)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* gameOverSrc_t801_FieldInfos[] =
{
	&gameOverSrc_t801____gameControlObj_2_FieldInfo,
	&gameOverSrc_t801____isGameOverReady_3_FieldInfo,
	&gameOverSrc_t801____marqueeSt_4_FieldInfo,
	&gameOverSrc_t801____marqueeStLeft_5_FieldInfo,
	&gameOverSrc_t801____lowBarSt_6_FieldInfo,
	&gameOverSrc_t801____lowBarPlaySt_7_FieldInfo,
	&gameOverSrc_t801____storeButtonSt_8_FieldInfo,
	&gameOverSrc_t801____bulletsHUD_9_FieldInfo,
	&gameOverSrc_t801____bulletsSt_10_FieldInfo,
	&gameOverSrc_t801____marqueeBigSt_11_FieldInfo,
	&gameOverSrc_t801____boxStyle_12_FieldInfo,
	&gameOverSrc_t801____maxText_13_FieldInfo,
	&gameOverSrc_t801____lastText_14_FieldInfo,
	&gameOverSrc_t801____killsText_15_FieldInfo,
	&gameOverSrc_t801____stageTimeText_16_FieldInfo,
	&gameOverSrc_t801____timeText_17_FieldInfo,
	&gameOverSrc_t801____coinsText_18_FieldInfo,
	&gameOverSrc_t801____messageRect_19_FieldInfo,
	&gameOverSrc_t801____messageMax_20_FieldInfo,
	&gameOverSrc_t801____messageLast_21_FieldInfo,
	&gameOverSrc_t801____boxRect_22_FieldInfo,
	&gameOverSrc_t801____offsetLetter_23_FieldInfo,
	&gameOverSrc_t801____startPos_24_FieldInfo,
	&gameOverSrc_t801____endPos_25_FieldInfo,
	&gameOverSrc_t801____counterK_26_FieldInfo,
	&gameOverSrc_t801____counterL_27_FieldInfo,
	&gameOverSrc_t801____counterM_28_FieldInfo,
	&gameOverSrc_t801____counterC_29_FieldInfo,
	&gameOverSrc_t801____counterDeathColor_30_FieldInfo,
	&gameOverSrc_t801____v_31_FieldInfo,
	&gameOverSrc_t801____xU_32_FieldInfo,
	&gameOverSrc_t801____yU_33_FieldInfo,
	&gameOverSrc_t801____cc_34_FieldInfo,
	NULL
};
static const Il2CppType* gameOverSrc_t801_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CrunU3Ec__IteratorD_t802_0_0_0,
	&U3CShowU3Ec__IteratorE_t803_0_0_0,
};
static Il2CppMethodReference gameOverSrc_t801_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool gameOverSrc_t801_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType gameOverSrc_t801_1_0_0;
struct gameOverSrc_t801;
const Il2CppTypeDefinitionMetadata gameOverSrc_t801_DefinitionMetadata = 
{
	NULL/* declaringType */
	, gameOverSrc_t801_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, gameOverSrc_t801_VTable/* vtableMethods */
	, gameOverSrc_t801_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo gameOverSrc_t801_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "gameOverSrc"/* name */
	, ""/* namespaze */
	, gameOverSrc_t801_MethodInfos/* methods */
	, NULL/* properties */
	, gameOverSrc_t801_FieldInfos/* fields */
	, NULL/* events */
	, &gameOverSrc_t801_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &gameOverSrc_t801_0_0_0/* byval_arg */
	, &gameOverSrc_t801_1_0_0/* this_arg */
	, &gameOverSrc_t801_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (gameOverSrc_t801)/* instance_size */
	, sizeof (gameOverSrc_t801)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(gameOverSrc_t801_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 33/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// gizmosProxy
#include "AssemblyU2DCSharp_gizmosProxy.h"
// Metadata Definition gizmosProxy
extern TypeInfo gizmosProxy_t804_il2cpp_TypeInfo;
// gizmosProxy
#include "AssemblyU2DCSharp_gizmosProxyMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gizmosProxy::.ctor()
MethodInfo gizmosProxy__ctor_m3509_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&gizmosProxy__ctor_m3509/* method */
	, &gizmosProxy_t804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void gizmosProxy::createGizmos()
MethodInfo gizmosProxy_createGizmos_m3510_MethodInfo = 
{
	"createGizmos"/* name */
	, (methodPointerType)&gizmosProxy_createGizmos_m3510/* method */
	, &gizmosProxy_t804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* gizmosProxy_t804_MethodInfos[] =
{
	&gizmosProxy__ctor_m3509_MethodInfo,
	&gizmosProxy_createGizmos_m3510_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gizmosProxy_t804____currentNaveProxy_2_FieldInfo = 
{
	"currentNaveProxy"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gizmosProxy_t804_il2cpp_TypeInfo/* parent */
	, offsetof(gizmosProxy_t804, ___currentNaveProxy_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo gizmosProxy_t804____line_3_FieldInfo = 
{
	"line"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &gizmosProxy_t804_il2cpp_TypeInfo/* parent */
	, offsetof(gizmosProxy_t804, ___line_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* gizmosProxy_t804_FieldInfos[] =
{
	&gizmosProxy_t804____currentNaveProxy_2_FieldInfo,
	&gizmosProxy_t804____line_3_FieldInfo,
	NULL
};
static Il2CppMethodReference gizmosProxy_t804_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool gizmosProxy_t804_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType gizmosProxy_t804_0_0_0;
extern Il2CppType gizmosProxy_t804_1_0_0;
struct gizmosProxy_t804;
const Il2CppTypeDefinitionMetadata gizmosProxy_t804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, gizmosProxy_t804_VTable/* vtableMethods */
	, gizmosProxy_t804_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo gizmosProxy_t804_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "gizmosProxy"/* name */
	, ""/* namespaze */
	, gizmosProxy_t804_MethodInfos/* methods */
	, NULL/* properties */
	, gizmosProxy_t804_FieldInfos/* fields */
	, NULL/* events */
	, &gizmosProxy_t804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &gizmosProxy_t804_0_0_0/* byval_arg */
	, &gizmosProxy_t804_1_0_0/* this_arg */
	, &gizmosProxy_t804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (gizmosProxy_t804)/* instance_size */
	, sizeof (gizmosProxy_t804)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// globales/<sleep>c__IteratorF
#include "AssemblyU2DCSharp_globales_U3CsleepU3Ec__IteratorF.h"
// Metadata Definition globales/<sleep>c__IteratorF
extern TypeInfo U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo;
// globales/<sleep>c__IteratorF
#include "AssemblyU2DCSharp_globales_U3CsleepU3Ec__IteratorFMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales/<sleep>c__IteratorF::.ctor()
MethodInfo U3CsleepU3Ec__IteratorF__ctor_m3511_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF__ctor_m3511/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object globales/<sleep>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 309/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object globales/<sleep>c__IteratorF::System.Collections.IEnumerator.get_Current()
MethodInfo U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 310/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean globales/<sleep>c__IteratorF::MoveNext()
MethodInfo U3CsleepU3Ec__IteratorF_MoveNext_m3514_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF_MoveNext_m3514/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales/<sleep>c__IteratorF::Dispose()
MethodInfo U3CsleepU3Ec__IteratorF_Dispose_m3515_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF_Dispose_m3515/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 311/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales/<sleep>c__IteratorF::Reset()
MethodInfo U3CsleepU3Ec__IteratorF_Reset_m3516_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CsleepU3Ec__IteratorF_Reset_m3516/* method */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 312/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CsleepU3Ec__IteratorF_t805_MethodInfos[] =
{
	&U3CsleepU3Ec__IteratorF__ctor_m3511_MethodInfo,
	&U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512_MethodInfo,
	&U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513_MethodInfo,
	&U3CsleepU3Ec__IteratorF_MoveNext_m3514_MethodInfo,
	&U3CsleepU3Ec__IteratorF_Dispose_m3515_MethodInfo,
	&U3CsleepU3Ec__IteratorF_Reset_m3516_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CsleepU3Ec__IteratorF_t805____t_0_FieldInfo = 
{
	"t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, offsetof(U3CsleepU3Ec__IteratorF_t805, ___t_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CsleepU3Ec__IteratorF_t805____U24PC_1_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, offsetof(U3CsleepU3Ec__IteratorF_t805, ___U24PC_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CsleepU3Ec__IteratorF_t805____U24current_2_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, offsetof(U3CsleepU3Ec__IteratorF_t805, ___U24current_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_3;
FieldInfo U3CsleepU3Ec__IteratorF_t805____U3CU24U3Et_3_FieldInfo = 
{
	"<$>t"/* name */
	, &Single_t202_0_0_3/* type */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, offsetof(U3CsleepU3Ec__IteratorF_t805, ___U3CU24U3Et_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CsleepU3Ec__IteratorF_t805_FieldInfos[] =
{
	&U3CsleepU3Ec__IteratorF_t805____t_0_FieldInfo,
	&U3CsleepU3Ec__IteratorF_t805____U24PC_1_FieldInfo,
	&U3CsleepU3Ec__IteratorF_t805____U24current_2_FieldInfo,
	&U3CsleepU3Ec__IteratorF_t805____U3CU24U3Et_3_FieldInfo,
	NULL
};
extern MethodInfo U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512_MethodInfo;
static PropertyInfo U3CsleepU3Ec__IteratorF_t805____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513_MethodInfo;
static PropertyInfo U3CsleepU3Ec__IteratorF_t805____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CsleepU3Ec__IteratorF_t805_PropertyInfos[] =
{
	&U3CsleepU3Ec__IteratorF_t805____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CsleepU3Ec__IteratorF_t805____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CsleepU3Ec__IteratorF_Dispose_m3515_MethodInfo;
extern MethodInfo U3CsleepU3Ec__IteratorF_MoveNext_m3514_MethodInfo;
extern MethodInfo U3CsleepU3Ec__IteratorF_Reset_m3516_MethodInfo;
static Il2CppMethodReference U3CsleepU3Ec__IteratorF_t805_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CsleepU3Ec__IteratorF_Dispose_m3515_MethodInfo,
	&U3CsleepU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3513_MethodInfo,
	&U3CsleepU3Ec__IteratorF_MoveNext_m3514_MethodInfo,
	&U3CsleepU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3512_MethodInfo,
	&U3CsleepU3Ec__IteratorF_Reset_m3516_MethodInfo,
};
static bool U3CsleepU3Ec__IteratorF_t805_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CsleepU3Ec__IteratorF_t805_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CsleepU3Ec__IteratorF_t805_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CsleepU3Ec__IteratorF_t805_0_0_0;
extern Il2CppType U3CsleepU3Ec__IteratorF_t805_1_0_0;
extern TypeInfo globales_t807_il2cpp_TypeInfo;
extern Il2CppType globales_t807_0_0_0;
struct U3CsleepU3Ec__IteratorF_t805;
const Il2CppTypeDefinitionMetadata U3CsleepU3Ec__IteratorF_t805_DefinitionMetadata = 
{
	&globales_t807_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CsleepU3Ec__IteratorF_t805_InterfacesTypeInfos/* implementedInterfaces */
	, U3CsleepU3Ec__IteratorF_t805_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CsleepU3Ec__IteratorF_t805_VTable/* vtableMethods */
	, U3CsleepU3Ec__IteratorF_t805_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<sleep>c__IteratorF"/* name */
	, ""/* namespaze */
	, U3CsleepU3Ec__IteratorF_t805_MethodInfos/* methods */
	, U3CsleepU3Ec__IteratorF_t805_PropertyInfos/* properties */
	, U3CsleepU3Ec__IteratorF_t805_FieldInfos/* fields */
	, NULL/* events */
	, &U3CsleepU3Ec__IteratorF_t805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 308/* custom_attributes_cache */
	, &U3CsleepU3Ec__IteratorF_t805_0_0_0/* byval_arg */
	, &U3CsleepU3Ec__IteratorF_t805_1_0_0/* this_arg */
	, &U3CsleepU3Ec__IteratorF_t805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CsleepU3Ec__IteratorF_t805)/* instance_size */
	, sizeof (U3CsleepU3Ec__IteratorF_t805)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// globales
#include "AssemblyU2DCSharp_globales.h"
// Metadata Definition globales
// globales
#include "AssemblyU2DCSharp_globalesMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::.ctor()
MethodInfo globales__ctor_m3517_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&globales__ctor_m3517/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::.cctor()
MethodInfo globales__cctor_m3518_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&globales__cctor_m3518/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::Awake()
MethodInfo globales_Awake_m3519_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&globales_Awake_m3519/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::BeginGUI()
MethodInfo globales_BeginGUI_m3520_MethodInfo = 
{
	"BeginGUI"/* name */
	, (methodPointerType)&globales_BeginGUI_m3520/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::EndGUI()
MethodInfo globales_EndGUI_m3521_MethodInfo = 
{
	"EndGUI"/* name */
	, (methodPointerType)&globales_EndGUI_m3521/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setCamera()
MethodInfo globales_setCamera_m3522_MethodInfo = 
{
	"setCamera"/* name */
	, (methodPointerType)&globales_setCamera_m3522/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::Start()
MethodInfo globales_Start_m3523_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&globales_Start_m3523/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::onSoomlaStoreInitialized()
MethodInfo globales_onSoomlaStoreInitialized_m3524_MethodInfo = 
{
	"onSoomlaStoreInitialized"/* name */
	, (methodPointerType)&globales_onSoomlaStoreInitialized_m3524/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::Update()
MethodInfo globales_Update_m3525_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&globales_Update_m3525/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setPosAgujeros()
MethodInfo globales_setPosAgujeros_m3526_MethodInfo = 
{
	"setPosAgujeros"/* name */
	, (methodPointerType)&globales_setPosAgujeros_m3526/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::clearMenu()
MethodInfo globales_clearMenu_m3527_MethodInfo = 
{
	"clearMenu"/* name */
	, (methodPointerType)&globales_clearMenu_m3527/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
extern void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 globales::getRandomPos()
MethodInfo globales_getRandomPos_m3528_MethodInfo = 
{
	"getRandomPos"/* name */
	, (methodPointerType)&globales_getRandomPos_m3528/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t739_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t739/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t758_0_0_0;
extern void* RuntimeInvoker_Vector3_t758 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 globales::getRandomRot()
MethodInfo globales_getRandomRot_m3529_MethodInfo = 
{
	"getRandomRot"/* name */
	, (methodPointerType)&globales_getRandomRot_m3529/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t758_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t758/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setCameraLevelColor()
MethodInfo globales_setCameraLevelColor_m3530_MethodInfo = 
{
	"setCameraLevelColor"/* name */
	, (methodPointerType)&globales_setCameraLevelColor_m3530/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::resetCameraColor()
MethodInfo globales_resetCameraColor_m3531_MethodInfo = 
{
	"resetCameraColor"/* name */
	, (methodPointerType)&globales_resetCameraColor_m3531/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setCameraRandomColor()
MethodInfo globales_setCameraRandomColor_m3532_MethodInfo = 
{
	"setCameraRandomColor"/* name */
	, (methodPointerType)&globales_setCameraRandomColor_m3532/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Color_t747_0_0_0;
extern void* RuntimeInvoker_Color_t747 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Color globales::getRandomColor()
MethodInfo globales_getRandomColor_m3533_MethodInfo = 
{
	"getRandomColor"/* name */
	, (methodPointerType)&globales_getRandomColor_m3533/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Color_t747_0_0_0/* return_type */
	, RuntimeInvoker_Color_t747/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setGrey()
MethodInfo globales_setGrey_m3534_MethodInfo = 
{
	"setGrey"/* name */
	, (methodPointerType)&globales_setGrey_m3534/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
static ParameterInfo globales_t807_globales_lanzaEnDirecciones_m3535_ParameterInfos[] = 
{
	{"go", 0, 134220488, 0, &GameObject_t144_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void globales::lanzaEnDirecciones(UnityEngine.GameObject)
MethodInfo globales_lanzaEnDirecciones_m3535_MethodInfo = 
{
	"lanzaEnDirecciones"/* name */
	, (methodPointerType)&globales_lanzaEnDirecciones_m3535/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, globales_t807_globales_lanzaEnDirecciones_m3535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::setData()
MethodInfo globales_setData_m3536_MethodInfo = 
{
	"setData"/* name */
	, (methodPointerType)&globales_setData_m3536/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::getData()
MethodInfo globales_getData_m3537_MethodInfo = 
{
	"getData"/* name */
	, (methodPointerType)&globales_getData_m3537/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::deleteData()
MethodInfo globales_deleteData_m3538_MethodInfo = 
{
	"deleteData"/* name */
	, (methodPointerType)&globales_deleteData_m3538/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::audioToggle()
MethodInfo globales_audioToggle_m3539_MethodInfo = 
{
	"audioToggle"/* name */
	, (methodPointerType)&globales_audioToggle_m3539/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::musicToggle()
MethodInfo globales_musicToggle_m3540_MethodInfo = 
{
	"musicToggle"/* name */
	, (methodPointerType)&globales_musicToggle_m3540/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::soundCheck()
MethodInfo globales_soundCheck_m3541_MethodInfo = 
{
	"soundCheck"/* name */
	, (methodPointerType)&globales_soundCheck_m3541/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void globales::OnApplicationQuit()
MethodInfo globales_OnApplicationQuit_m3542_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&globales_OnApplicationQuit_m3542/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo globales_t807_globales_SetBool_m3543_ParameterInfos[] = 
{
	{"name", 0, 134220489, 0, &String_t_0_0_0},
	{"value", 1, 134220490, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void globales::SetBool(System.String,System.Boolean)
MethodInfo globales_SetBool_m3543_MethodInfo = 
{
	"SetBool"/* name */
	, (methodPointerType)&globales_SetBool_m3543/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, globales_t807_globales_SetBool_m3543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo globales_t807_globales_GetBool_m3544_ParameterInfos[] = 
{
	{"name", 0, 134220491, 0, &String_t_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean globales::GetBool(System.String)
MethodInfo globales_GetBool_m3544_MethodInfo = 
{
	"GetBool"/* name */
	, (methodPointerType)&globales_GetBool_m3544/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t/* invoker_method */
	, globales_t807_globales_GetBool_m3544_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo globales_t807_globales_GetBool_m3545_ParameterInfos[] = 
{
	{"name", 0, 134220492, 0, &String_t_0_0_0},
	{"defaultValue", 1, 134220493, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Boolean globales::GetBool(System.String,System.Boolean)
MethodInfo globales_GetBool_m3545_MethodInfo = 
{
	"GetBool"/* name */
	, (methodPointerType)&globales_GetBool_m3545/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203_Object_t_SByte_t236/* invoker_method */
	, globales_t807_globales_GetBool_m3545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t202_0_0_0;
static ParameterInfo globales_t807_globales_sleep_m3546_ParameterInfos[] = 
{
	{"t", 0, 134220494, 0, &Single_t202_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator globales::sleep(System.Single)
MethodInfo globales_sleep_m3546_MethodInfo = 
{
	"sleep"/* name */
	, (methodPointerType)&globales_sleep_m3546/* method */
	, &globales_t807_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t202/* invoker_method */
	, globales_t807_globales_sleep_m3546_ParameterInfos/* parameters */
	, 307/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* globales_t807_MethodInfos[] =
{
	&globales__ctor_m3517_MethodInfo,
	&globales__cctor_m3518_MethodInfo,
	&globales_Awake_m3519_MethodInfo,
	&globales_BeginGUI_m3520_MethodInfo,
	&globales_EndGUI_m3521_MethodInfo,
	&globales_setCamera_m3522_MethodInfo,
	&globales_Start_m3523_MethodInfo,
	&globales_onSoomlaStoreInitialized_m3524_MethodInfo,
	&globales_Update_m3525_MethodInfo,
	&globales_setPosAgujeros_m3526_MethodInfo,
	&globales_clearMenu_m3527_MethodInfo,
	&globales_getRandomPos_m3528_MethodInfo,
	&globales_getRandomRot_m3529_MethodInfo,
	&globales_setCameraLevelColor_m3530_MethodInfo,
	&globales_resetCameraColor_m3531_MethodInfo,
	&globales_setCameraRandomColor_m3532_MethodInfo,
	&globales_getRandomColor_m3533_MethodInfo,
	&globales_setGrey_m3534_MethodInfo,
	&globales_lanzaEnDirecciones_m3535_MethodInfo,
	&globales_setData_m3536_MethodInfo,
	&globales_getData_m3537_MethodInfo,
	&globales_deleteData_m3538_MethodInfo,
	&globales_audioToggle_m3539_MethodInfo,
	&globales_musicToggle_m3540_MethodInfo,
	&globales_soundCheck_m3541_MethodInfo,
	&globales_OnApplicationQuit_m3542_MethodInfo,
	&globales_SetBool_m3543_MethodInfo,
	&globales_GetBool_m3544_MethodInfo,
	&globales_GetBool_m3545_MethodInfo,
	&globales_sleep_m3546_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo globales_t807____frameRate_2_FieldInfo = 
{
	"frameRate"/* name */
	, &Int32_t189_0_0_1/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807, ___frameRate_2)/* offset */
	, 306/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____goldenVersion_3_FieldInfo = 
{
	"goldenVersion"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___goldenVersion_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____kills_4_FieldInfo = 
{
	"kills"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___kills_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____maxKills1_5_FieldInfo = 
{
	"maxKills1"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___maxKills1_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____lastKills_6_FieldInfo = 
{
	"lastKills"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___lastKills_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____hview_7_FieldInfo = 
{
	"hview"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___hview_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____wview_8_FieldInfo = 
{
	"wview"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___wview_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____numberOfGames_9_FieldInfo = 
{
	"numberOfGames"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___numberOfGames_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____milisecsEnemyDestroyed_10_FieldInfo = 
{
	"milisecsEnemyDestroyed"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___milisecsEnemyDestroyed_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____dustLevel_11_FieldInfo = 
{
	"dustLevel"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___dustLevel_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____currentMap_12_FieldInfo = 
{
	"currentMap"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___currentMap_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____totalNumberMaps_13_FieldInfo = 
{
	"totalNumberMaps"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___totalNumberMaps_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____currentStage_14_FieldInfo = 
{
	"currentStage"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___currentStage_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_22;
FieldInfo globales_t807____level_15_FieldInfo = 
{
	"level"/* name */
	, &Int32_t189_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___level_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____camWidth_16_FieldInfo = 
{
	"camWidth"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___camWidth_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____camHeight_17_FieldInfo = 
{
	"camHeight"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___camHeight_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____minX_18_FieldInfo = 
{
	"minX"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___minX_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____maxX_19_FieldInfo = 
{
	"maxX"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___maxX_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____minY_20_FieldInfo = 
{
	"minY"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___minY_20)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____maxY_21_FieldInfo = 
{
	"maxY"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___maxY_21)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____WIDTH_22_FieldInfo = 
{
	"WIDTH"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___WIDTH_22)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____HEIGHT_23_FieldInfo = 
{
	"HEIGHT"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___HEIGHT_23)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____SCREENH_24_FieldInfo = 
{
	"SCREENH"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___SCREENH_24)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_22;
FieldInfo globales_t807____SCREENW_25_FieldInfo = 
{
	"SCREENW"/* name */
	, &Single_t202_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___SCREENW_25)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____isLandscape_26_FieldInfo = 
{
	"isLandscape"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___isLandscape_26)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____ISWIDE_27_FieldInfo = 
{
	"ISWIDE"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___ISWIDE_27)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____showAdApple_28_FieldInfo = 
{
	"showAdApple"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___showAdApple_28)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____showAdmob_29_FieldInfo = 
{
	"showAdmob"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___showAdmob_29)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____loadedAdmob_30_FieldInfo = 
{
	"loadedAdmob"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___loadedAdmob_30)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____shaking_31_FieldInfo = 
{
	"shaking"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___shaking_31)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____showNewRecord_32_FieldInfo = 
{
	"showNewRecord"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___showNewRecord_32)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____showNewLevel_33_FieldInfo = 
{
	"showNewLevel"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___showNewLevel_33)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____tutorial_34_FieldInfo = 
{
	"tutorial"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___tutorial_34)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____tutorialEnemiesReady_35_FieldInfo = 
{
	"tutorialEnemiesReady"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___tutorialEnemiesReady_35)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____failedTutorial_36_FieldInfo = 
{
	"failedTutorial"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___failedTutorial_36)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____sfxSwitch_37_FieldInfo = 
{
	"sfxSwitch"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___sfxSwitch_37)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____musicSwitch_38_FieldInfo = 
{
	"musicSwitch"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___musicSwitch_38)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____tutorialIsFinished_39_FieldInfo = 
{
	"tutorialIsFinished"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___tutorialIsFinished_39)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo globales_t807____showUnity_40_FieldInfo = 
{
	"showUnity"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___showUnity_40)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType EventHandler_t76_0_0_17;
FieldInfo globales_t807____handler_41_FieldInfo = 
{
	"handler"/* name */
	, &EventHandler_t76_0_0_17/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___handler_41)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_22;
FieldInfo globales_t807____SCREENVECTOR_42_FieldInfo = 
{
	"SCREENVECTOR"/* name */
	, &Vector2_t739_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___SCREENVECTOR_42)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_22;
FieldInfo globales_t807____SCREENSCALE_43_FieldInfo = 
{
	"SCREENSCALE"/* name */
	, &Vector2_t739_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___SCREENSCALE_43)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_22;
FieldInfo globales_t807____OLEADA_44_FieldInfo = 
{
	"OLEADA"/* name */
	, &String_t_0_0_22/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___OLEADA_44)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t806_0_0_17;
FieldInfo globales_t807____stack_45_FieldInfo = 
{
	"stack"/* name */
	, &List_1_t806_0_0_17/* type */
	, &globales_t807_il2cpp_TypeInfo/* parent */
	, offsetof(globales_t807_StaticFields, ___stack_45)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* globales_t807_FieldInfos[] =
{
	&globales_t807____frameRate_2_FieldInfo,
	&globales_t807____goldenVersion_3_FieldInfo,
	&globales_t807____kills_4_FieldInfo,
	&globales_t807____maxKills1_5_FieldInfo,
	&globales_t807____lastKills_6_FieldInfo,
	&globales_t807____hview_7_FieldInfo,
	&globales_t807____wview_8_FieldInfo,
	&globales_t807____numberOfGames_9_FieldInfo,
	&globales_t807____milisecsEnemyDestroyed_10_FieldInfo,
	&globales_t807____dustLevel_11_FieldInfo,
	&globales_t807____currentMap_12_FieldInfo,
	&globales_t807____totalNumberMaps_13_FieldInfo,
	&globales_t807____currentStage_14_FieldInfo,
	&globales_t807____level_15_FieldInfo,
	&globales_t807____camWidth_16_FieldInfo,
	&globales_t807____camHeight_17_FieldInfo,
	&globales_t807____minX_18_FieldInfo,
	&globales_t807____maxX_19_FieldInfo,
	&globales_t807____minY_20_FieldInfo,
	&globales_t807____maxY_21_FieldInfo,
	&globales_t807____WIDTH_22_FieldInfo,
	&globales_t807____HEIGHT_23_FieldInfo,
	&globales_t807____SCREENH_24_FieldInfo,
	&globales_t807____SCREENW_25_FieldInfo,
	&globales_t807____isLandscape_26_FieldInfo,
	&globales_t807____ISWIDE_27_FieldInfo,
	&globales_t807____showAdApple_28_FieldInfo,
	&globales_t807____showAdmob_29_FieldInfo,
	&globales_t807____loadedAdmob_30_FieldInfo,
	&globales_t807____shaking_31_FieldInfo,
	&globales_t807____showNewRecord_32_FieldInfo,
	&globales_t807____showNewLevel_33_FieldInfo,
	&globales_t807____tutorial_34_FieldInfo,
	&globales_t807____tutorialEnemiesReady_35_FieldInfo,
	&globales_t807____failedTutorial_36_FieldInfo,
	&globales_t807____sfxSwitch_37_FieldInfo,
	&globales_t807____musicSwitch_38_FieldInfo,
	&globales_t807____tutorialIsFinished_39_FieldInfo,
	&globales_t807____showUnity_40_FieldInfo,
	&globales_t807____handler_41_FieldInfo,
	&globales_t807____SCREENVECTOR_42_FieldInfo,
	&globales_t807____SCREENSCALE_43_FieldInfo,
	&globales_t807____OLEADA_44_FieldInfo,
	&globales_t807____stack_45_FieldInfo,
	NULL
};
static const Il2CppType* globales_t807_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CsleepU3Ec__IteratorF_t805_0_0_0,
};
static Il2CppMethodReference globales_t807_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool globales_t807_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType globales_t807_1_0_0;
struct globales_t807;
const Il2CppTypeDefinitionMetadata globales_t807_DefinitionMetadata = 
{
	NULL/* declaringType */
	, globales_t807_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, globales_t807_VTable/* vtableMethods */
	, globales_t807_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo globales_t807_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "globales"/* name */
	, ""/* namespaze */
	, globales_t807_MethodInfos/* methods */
	, NULL/* properties */
	, globales_t807_FieldInfos/* fields */
	, NULL/* events */
	, &globales_t807_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &globales_t807_0_0_0/* byval_arg */
	, &globales_t807_1_0_0/* this_arg */
	, &globales_t807_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (globales_t807)/* instance_size */
	, sizeof (globales_t807)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(globales_t807_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 44/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// growOnCollision
#include "AssemblyU2DCSharp_growOnCollision.h"
// Metadata Definition growOnCollision
extern TypeInfo growOnCollision_t808_il2cpp_TypeInfo;
// growOnCollision
#include "AssemblyU2DCSharp_growOnCollisionMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void growOnCollision::.ctor()
MethodInfo growOnCollision__ctor_m3547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&growOnCollision__ctor_m3547/* method */
	, &growOnCollision_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void growOnCollision::Start()
MethodInfo growOnCollision_Start_m3548_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&growOnCollision_Start_m3548/* method */
	, &growOnCollision_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void growOnCollision::Update()
MethodInfo growOnCollision_Update_m3549_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&growOnCollision_Update_m3549/* method */
	, &growOnCollision_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* growOnCollision_t808_MethodInfos[] =
{
	&growOnCollision__ctor_m3547_MethodInfo,
	&growOnCollision_Start_m3548_MethodInfo,
	&growOnCollision_Update_m3549_MethodInfo,
	NULL
};
static Il2CppMethodReference growOnCollision_t808_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool growOnCollision_t808_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType growOnCollision_t808_0_0_0;
extern Il2CppType growOnCollision_t808_1_0_0;
struct growOnCollision_t808;
const Il2CppTypeDefinitionMetadata growOnCollision_t808_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, growOnCollision_t808_VTable/* vtableMethods */
	, growOnCollision_t808_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo growOnCollision_t808_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "growOnCollision"/* name */
	, ""/* namespaze */
	, growOnCollision_t808_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &growOnCollision_t808_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &growOnCollision_t808_0_0_0/* byval_arg */
	, &growOnCollision_t808_1_0_0/* this_arg */
	, &growOnCollision_t808_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (growOnCollision_t808)/* instance_size */
	, sizeof (growOnCollision_t808)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// headScr
#include "AssemblyU2DCSharp_headScr.h"
// Metadata Definition headScr
extern TypeInfo headScr_t781_il2cpp_TypeInfo;
// headScr
#include "AssemblyU2DCSharp_headScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void headScr::.ctor()
MethodInfo headScr__ctor_m3550_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&headScr__ctor_m3550/* method */
	, &headScr_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t758_0_0_0;
static ParameterInfo headScr_t781_headScr_OnExplosion_m3551_ParameterInfos[] = 
{
	{"pos", 0, 134220495, 0, &Vector3_t758_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Vector3_t758 (MethodInfo* method, void* obj, void** args);
// System.Void headScr::OnExplosion(UnityEngine.Vector3)
MethodInfo headScr_OnExplosion_m3551_MethodInfo = 
{
	"OnExplosion"/* name */
	, (methodPointerType)&headScr_OnExplosion_m3551/* method */
	, &headScr_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Vector3_t758/* invoker_method */
	, headScr_t781_headScr_OnExplosion_m3551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void headScr::Start()
MethodInfo headScr_Start_m3552_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&headScr_Start_m3552/* method */
	, &headScr_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void headScr::Update()
MethodInfo headScr_Update_m3553_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&headScr_Update_m3553/* method */
	, &headScr_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void headScr::destroyParts()
MethodInfo headScr_destroyParts_m3554_MethodInfo = 
{
	"destroyParts"/* name */
	, (methodPointerType)&headScr_destroyParts_m3554/* method */
	, &headScr_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* headScr_t781_MethodInfos[] =
{
	&headScr__ctor_m3550_MethodInfo,
	&headScr_OnExplosion_m3551_MethodInfo,
	&headScr_Start_m3552_MethodInfo,
	&headScr_Update_m3553_MethodInfo,
	&headScr_destroyParts_m3554_MethodInfo,
	NULL
};
extern Il2CppType Transform_t809_0_0_6;
FieldInfo headScr_t781____head_2_FieldInfo = 
{
	"head"/* name */
	, &Transform_t809_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___head_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t810_0_0_6;
FieldInfo headScr_t781____segments_3_FieldInfo = 
{
	"segments"/* name */
	, &List_1_t810_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___segments_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType List_1_t811_0_0_1;
FieldInfo headScr_t781____breadcrumbs_4_FieldInfo = 
{
	"breadcrumbs"/* name */
	, &List_1_t811_0_0_1/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___breadcrumbs_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo headScr_t781____mybodyLenght_5_FieldInfo = 
{
	"mybodyLenght"/* name */
	, &Int32_t189_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___mybodyLenght_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo headScr_t781____cuerpo_6_FieldInfo = 
{
	"cuerpo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___cuerpo_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo headScr_t781____segmentSpacing_7_FieldInfo = 
{
	"segmentSpacing"/* name */
	, &Single_t202_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___segmentSpacing_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo headScr_t781____explosion_8_FieldInfo = 
{
	"explosion"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &headScr_t781_il2cpp_TypeInfo/* parent */
	, offsetof(headScr_t781, ___explosion_8)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* headScr_t781_FieldInfos[] =
{
	&headScr_t781____head_2_FieldInfo,
	&headScr_t781____segments_3_FieldInfo,
	&headScr_t781____breadcrumbs_4_FieldInfo,
	&headScr_t781____mybodyLenght_5_FieldInfo,
	&headScr_t781____cuerpo_6_FieldInfo,
	&headScr_t781____segmentSpacing_7_FieldInfo,
	&headScr_t781____explosion_8_FieldInfo,
	NULL
};
static Il2CppMethodReference headScr_t781_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool headScr_t781_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType headScr_t781_0_0_0;
extern Il2CppType headScr_t781_1_0_0;
struct headScr_t781;
const Il2CppTypeDefinitionMetadata headScr_t781_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, headScr_t781_VTable/* vtableMethods */
	, headScr_t781_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo headScr_t781_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "headScr"/* name */
	, ""/* namespaze */
	, headScr_t781_MethodInfos/* methods */
	, NULL/* properties */
	, headScr_t781_FieldInfos/* fields */
	, NULL/* events */
	, &headScr_t781_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &headScr_t781_0_0_0/* byval_arg */
	, &headScr_t781_1_0_0/* this_arg */
	, &headScr_t781_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (headScr_t781)/* instance_size */
	, sizeof (headScr_t781)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// logoScene/<wait>c__Iterator10
#include "AssemblyU2DCSharp_logoScene_U3CwaitU3Ec__Iterator10.h"
// Metadata Definition logoScene/<wait>c__Iterator10
extern TypeInfo U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo;
// logoScene/<wait>c__Iterator10
#include "AssemblyU2DCSharp_logoScene_U3CwaitU3Ec__Iterator10MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene/<wait>c__Iterator10::.ctor()
MethodInfo U3CwaitU3Ec__Iterator10__ctor_m3555_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10__ctor_m3555/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object logoScene/<wait>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 315/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object logoScene/<wait>c__Iterator10::System.Collections.IEnumerator.get_Current()
MethodInfo U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 316/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean logoScene/<wait>c__Iterator10::MoveNext()
MethodInfo U3CwaitU3Ec__Iterator10_MoveNext_m3558_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10_MoveNext_m3558/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene/<wait>c__Iterator10::Dispose()
MethodInfo U3CwaitU3Ec__Iterator10_Dispose_m3559_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10_Dispose_m3559/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 317/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene/<wait>c__Iterator10::Reset()
MethodInfo U3CwaitU3Ec__Iterator10_Reset_m3560_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CwaitU3Ec__Iterator10_Reset_m3560/* method */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 318/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CwaitU3Ec__Iterator10_t813_MethodInfos[] =
{
	&U3CwaitU3Ec__Iterator10__ctor_m3555_MethodInfo,
	&U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556_MethodInfo,
	&U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557_MethodInfo,
	&U3CwaitU3Ec__Iterator10_MoveNext_m3558_MethodInfo,
	&U3CwaitU3Ec__Iterator10_Dispose_m3559_MethodInfo,
	&U3CwaitU3Ec__Iterator10_Reset_m3560_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator10_t813____U24PC_0_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator10_t813, ___U24PC_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator10_t813____U24current_1_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator10_t813, ___U24current_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType logoScene_t812_0_0_3;
FieldInfo U3CwaitU3Ec__Iterator10_t813____U3CU3Ef__this_2_FieldInfo = 
{
	"<>f__this"/* name */
	, &logoScene_t812_0_0_3/* type */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitU3Ec__Iterator10_t813, ___U3CU3Ef__this_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CwaitU3Ec__Iterator10_t813_FieldInfos[] =
{
	&U3CwaitU3Ec__Iterator10_t813____U24PC_0_FieldInfo,
	&U3CwaitU3Ec__Iterator10_t813____U24current_1_FieldInfo,
	&U3CwaitU3Ec__Iterator10_t813____U3CU3Ef__this_2_FieldInfo,
	NULL
};
extern MethodInfo U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556_MethodInfo;
static PropertyInfo U3CwaitU3Ec__Iterator10_t813____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557_MethodInfo;
static PropertyInfo U3CwaitU3Ec__Iterator10_t813____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CwaitU3Ec__Iterator10_t813_PropertyInfos[] =
{
	&U3CwaitU3Ec__Iterator10_t813____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CwaitU3Ec__Iterator10_t813____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CwaitU3Ec__Iterator10_Dispose_m3559_MethodInfo;
extern MethodInfo U3CwaitU3Ec__Iterator10_MoveNext_m3558_MethodInfo;
extern MethodInfo U3CwaitU3Ec__Iterator10_Reset_m3560_MethodInfo;
static Il2CppMethodReference U3CwaitU3Ec__Iterator10_t813_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CwaitU3Ec__Iterator10_Dispose_m3559_MethodInfo,
	&U3CwaitU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m3557_MethodInfo,
	&U3CwaitU3Ec__Iterator10_MoveNext_m3558_MethodInfo,
	&U3CwaitU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3556_MethodInfo,
	&U3CwaitU3Ec__Iterator10_Reset_m3560_MethodInfo,
};
static bool U3CwaitU3Ec__Iterator10_t813_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CwaitU3Ec__Iterator10_t813_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CwaitU3Ec__Iterator10_t813_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CwaitU3Ec__Iterator10_t813_0_0_0;
extern Il2CppType U3CwaitU3Ec__Iterator10_t813_1_0_0;
extern TypeInfo logoScene_t812_il2cpp_TypeInfo;
extern Il2CppType logoScene_t812_0_0_0;
struct U3CwaitU3Ec__Iterator10_t813;
const Il2CppTypeDefinitionMetadata U3CwaitU3Ec__Iterator10_t813_DefinitionMetadata = 
{
	&logoScene_t812_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CwaitU3Ec__Iterator10_t813_InterfacesTypeInfos/* implementedInterfaces */
	, U3CwaitU3Ec__Iterator10_t813_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CwaitU3Ec__Iterator10_t813_VTable/* vtableMethods */
	, U3CwaitU3Ec__Iterator10_t813_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<wait>c__Iterator10"/* name */
	, ""/* namespaze */
	, U3CwaitU3Ec__Iterator10_t813_MethodInfos/* methods */
	, U3CwaitU3Ec__Iterator10_t813_PropertyInfos/* properties */
	, U3CwaitU3Ec__Iterator10_t813_FieldInfos/* fields */
	, NULL/* events */
	, &U3CwaitU3Ec__Iterator10_t813_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 314/* custom_attributes_cache */
	, &U3CwaitU3Ec__Iterator10_t813_0_0_0/* byval_arg */
	, &U3CwaitU3Ec__Iterator10_t813_1_0_0/* this_arg */
	, &U3CwaitU3Ec__Iterator10_t813_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CwaitU3Ec__Iterator10_t813)/* instance_size */
	, sizeof (U3CwaitU3Ec__Iterator10_t813)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// logoScene
#include "AssemblyU2DCSharp_logoScene.h"
// Metadata Definition logoScene
// logoScene
#include "AssemblyU2DCSharp_logoSceneMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene::.ctor()
MethodInfo logoScene__ctor_m3561_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&logoScene__ctor_m3561/* method */
	, &logoScene_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene::Start()
MethodInfo logoScene_Start_m3562_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&logoScene_Start_m3562/* method */
	, &logoScene_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator logoScene::wait()
MethodInfo logoScene_wait_m3563_MethodInfo = 
{
	"wait"/* name */
	, (methodPointerType)&logoScene_wait_m3563/* method */
	, &logoScene_t812_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 313/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void logoScene::Update()
MethodInfo logoScene_Update_m3564_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&logoScene_Update_m3564/* method */
	, &logoScene_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* logoScene_t812_MethodInfos[] =
{
	&logoScene__ctor_m3561_MethodInfo,
	&logoScene_Start_m3562_MethodInfo,
	&logoScene_wait_m3563_MethodInfo,
	&logoScene_Update_m3564_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_6;
FieldInfo logoScene_t812____fadeTime_2_FieldInfo = 
{
	"fadeTime"/* name */
	, &Single_t202_0_0_6/* type */
	, &logoScene_t812_il2cpp_TypeInfo/* parent */
	, offsetof(logoScene_t812, ___fadeTime_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo logoScene_t812____fadeOut_3_FieldInfo = 
{
	"fadeOut"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &logoScene_t812_il2cpp_TypeInfo/* parent */
	, offsetof(logoScene_t812, ___fadeOut_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo logoScene_t812____fadeIn_4_FieldInfo = 
{
	"fadeIn"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &logoScene_t812_il2cpp_TypeInfo/* parent */
	, offsetof(logoScene_t812, ___fadeIn_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_6;
FieldInfo logoScene_t812____levelName_5_FieldInfo = 
{
	"levelName"/* name */
	, &String_t_0_0_6/* type */
	, &logoScene_t812_il2cpp_TypeInfo/* parent */
	, offsetof(logoScene_t812, ___levelName_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* logoScene_t812_FieldInfos[] =
{
	&logoScene_t812____fadeTime_2_FieldInfo,
	&logoScene_t812____fadeOut_3_FieldInfo,
	&logoScene_t812____fadeIn_4_FieldInfo,
	&logoScene_t812____levelName_5_FieldInfo,
	NULL
};
static const Il2CppType* logoScene_t812_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CwaitU3Ec__Iterator10_t813_0_0_0,
};
static Il2CppMethodReference logoScene_t812_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool logoScene_t812_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType logoScene_t812_1_0_0;
struct logoScene_t812;
const Il2CppTypeDefinitionMetadata logoScene_t812_DefinitionMetadata = 
{
	NULL/* declaringType */
	, logoScene_t812_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, logoScene_t812_VTable/* vtableMethods */
	, logoScene_t812_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo logoScene_t812_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "logoScene"/* name */
	, ""/* namespaze */
	, logoScene_t812_MethodInfos/* methods */
	, NULL/* properties */
	, logoScene_t812_FieldInfos/* fields */
	, NULL/* events */
	, &logoScene_t812_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &logoScene_t812_0_0_0/* byval_arg */
	, &logoScene_t812_1_0_0/* this_arg */
	, &logoScene_t812_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (logoScene_t812)/* instance_size */
	, sizeof (logoScene_t812)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// menu
#include "AssemblyU2DCSharp_menu.h"
// Metadata Definition menu
extern TypeInfo menu_t814_il2cpp_TypeInfo;
// menu
#include "AssemblyU2DCSharp_menuMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void menu::.ctor()
MethodInfo menu__ctor_m3565_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&menu__ctor_m3565/* method */
	, &menu_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void menu::Start()
MethodInfo menu_Start_m3566_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&menu_Start_m3566/* method */
	, &menu_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void menu::OnGUI()
MethodInfo menu_OnGUI_m3567_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&menu_OnGUI_m3567/* method */
	, &menu_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void menu::OnApplicationQuit()
MethodInfo menu_OnApplicationQuit_m3568_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&menu_OnApplicationQuit_m3568/* method */
	, &menu_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* menu_t814_MethodInfos[] =
{
	&menu__ctor_m3565_MethodInfo,
	&menu_Start_m3566_MethodInfo,
	&menu_OnGUI_m3567_MethodInfo,
	&menu_OnApplicationQuit_m3568_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo menu_t814____controller_2_FieldInfo = 
{
	"controller"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___controller_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo menu_t814____marqueeSt_3_FieldInfo = 
{
	"marqueeSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___marqueeSt_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo menu_t814____startSt_4_FieldInfo = 
{
	"startSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___startSt_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo menu_t814____offset_5_FieldInfo = 
{
	"offset"/* name */
	, &Single_t202_0_0_6/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___offset_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo menu_t814____messageRect_6_FieldInfo = 
{
	"messageRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___messageRect_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo menu_t814____t_7_FieldInfo = 
{
	"t"/* name */
	, &Int32_t189_0_0_1/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___t_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo menu_t814____vChild_8_FieldInfo = 
{
	"vChild"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___vChild_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo menu_t814____v_9_FieldInfo = 
{
	"v"/* name */
	, &Single_t202_0_0_1/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___v_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo menu_t814____yU_10_FieldInfo = 
{
	"yU"/* name */
	, &Single_t202_0_0_1/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___yU_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture_t736_0_0_6;
FieldInfo menu_t814____splash_11_FieldInfo = 
{
	"splash"/* name */
	, &Texture_t736_0_0_6/* type */
	, &menu_t814_il2cpp_TypeInfo/* parent */
	, offsetof(menu_t814, ___splash_11)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* menu_t814_FieldInfos[] =
{
	&menu_t814____controller_2_FieldInfo,
	&menu_t814____marqueeSt_3_FieldInfo,
	&menu_t814____startSt_4_FieldInfo,
	&menu_t814____offset_5_FieldInfo,
	&menu_t814____messageRect_6_FieldInfo,
	&menu_t814____t_7_FieldInfo,
	&menu_t814____vChild_8_FieldInfo,
	&menu_t814____v_9_FieldInfo,
	&menu_t814____yU_10_FieldInfo,
	&menu_t814____splash_11_FieldInfo,
	NULL
};
static Il2CppMethodReference menu_t814_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool menu_t814_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType menu_t814_0_0_0;
extern Il2CppType menu_t814_1_0_0;
struct menu_t814;
const Il2CppTypeDefinitionMetadata menu_t814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, menu_t814_VTable/* vtableMethods */
	, menu_t814_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo menu_t814_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "menu"/* name */
	, ""/* namespaze */
	, menu_t814_MethodInfos/* methods */
	, NULL/* properties */
	, menu_t814_FieldInfos/* fields */
	, NULL/* events */
	, &menu_t814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &menu_t814_0_0_0/* byval_arg */
	, &menu_t814_1_0_0/* this_arg */
	, &menu_t814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (menu_t814)/* instance_size */
	, sizeof (menu_t814)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// movementProxy
#include "AssemblyU2DCSharp_movementProxy.h"
// Metadata Definition movementProxy
extern TypeInfo movementProxy_t815_il2cpp_TypeInfo;
// movementProxy
#include "AssemblyU2DCSharp_movementProxyMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movementProxy::.ctor()
MethodInfo movementProxy__ctor_m3569_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&movementProxy__ctor_m3569/* method */
	, &movementProxy_t815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movementProxy::rotateShipDirection()
MethodInfo movementProxy_rotateShipDirection_m3570_MethodInfo = 
{
	"rotateShipDirection"/* name */
	, (methodPointerType)&movementProxy_rotateShipDirection_m3570/* method */
	, &movementProxy_t815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movementProxy::rotateInactive()
MethodInfo movementProxy_rotateInactive_m3571_MethodInfo = 
{
	"rotateInactive"/* name */
	, (methodPointerType)&movementProxy_rotateInactive_m3571/* method */
	, &movementProxy_t815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo movementProxy_t815_movementProxy_calculateAngle_m3572_ParameterInfos[] = 
{
	{"target", 0, 134220496, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Single movementProxy::calculateAngle(UnityEngine.Vector2)
MethodInfo movementProxy_calculateAngle_m3572_MethodInfo = 
{
	"calculateAngle"/* name */
	, (methodPointerType)&movementProxy_calculateAngle_m3572/* method */
	, &movementProxy_t815_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Vector2_t739/* invoker_method */
	, movementProxy_t815_movementProxy_calculateAngle_m3572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* movementProxy_t815_MethodInfos[] =
{
	&movementProxy__ctor_m3569_MethodInfo,
	&movementProxy_rotateShipDirection_m3570_MethodInfo,
	&movementProxy_rotateInactive_m3571_MethodInfo,
	&movementProxy_calculateAngle_m3572_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo movementProxy_t815____enemyController_2_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &movementProxy_t815_il2cpp_TypeInfo/* parent */
	, offsetof(movementProxy_t815, ___enemyController_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyController_t785_0_0_1;
FieldInfo movementProxy_t815_____enemyController_3_FieldInfo = 
{
	"_enemyController"/* name */
	, &enemyController_t785_0_0_1/* type */
	, &movementProxy_t815_il2cpp_TypeInfo/* parent */
	, offsetof(movementProxy_t815, ____enemyController_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* movementProxy_t815_FieldInfos[] =
{
	&movementProxy_t815____enemyController_2_FieldInfo,
	&movementProxy_t815_____enemyController_3_FieldInfo,
	NULL
};
static Il2CppMethodReference movementProxy_t815_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool movementProxy_t815_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType movementProxy_t815_0_0_0;
extern Il2CppType movementProxy_t815_1_0_0;
struct movementProxy_t815;
const Il2CppTypeDefinitionMetadata movementProxy_t815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, movementProxy_t815_VTable/* vtableMethods */
	, movementProxy_t815_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo movementProxy_t815_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "movementProxy"/* name */
	, ""/* namespaze */
	, movementProxy_t815_MethodInfos/* methods */
	, NULL/* properties */
	, movementProxy_t815_FieldInfos/* fields */
	, NULL/* events */
	, &movementProxy_t815_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &movementProxy_t815_0_0_0/* byval_arg */
	, &movementProxy_t815_1_0_0/* this_arg */
	, &movementProxy_t815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (movementProxy_t815)/* instance_size */
	, sizeof (movementProxy_t815)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// movingTextFeedback
#include "AssemblyU2DCSharp_movingTextFeedback.h"
// Metadata Definition movingTextFeedback
extern TypeInfo movingTextFeedback_t816_il2cpp_TypeInfo;
// movingTextFeedback
#include "AssemblyU2DCSharp_movingTextFeedbackMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextFeedback::.ctor()
MethodInfo movingTextFeedback__ctor_m3573_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&movingTextFeedback__ctor_m3573/* method */
	, &movingTextFeedback_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextFeedback::Start()
MethodInfo movingTextFeedback_Start_m3574_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&movingTextFeedback_Start_m3574/* method */
	, &movingTextFeedback_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextFeedback::Update()
MethodInfo movingTextFeedback_Update_m3575_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&movingTextFeedback_Update_m3575/* method */
	, &movingTextFeedback_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextFeedback::OnGUI()
MethodInfo movingTextFeedback_OnGUI_m3576_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&movingTextFeedback_OnGUI_m3576/* method */
	, &movingTextFeedback_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* movingTextFeedback_t816_MethodInfos[] =
{
	&movingTextFeedback__ctor_m3573_MethodInfo,
	&movingTextFeedback_Start_m3574_MethodInfo,
	&movingTextFeedback_Update_m3575_MethodInfo,
	&movingTextFeedback_OnGUI_m3576_MethodInfo,
	NULL
};
static Il2CppMethodReference movingTextFeedback_t816_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool movingTextFeedback_t816_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType movingTextFeedback_t816_0_0_0;
extern Il2CppType movingTextFeedback_t816_1_0_0;
struct movingTextFeedback_t816;
const Il2CppTypeDefinitionMetadata movingTextFeedback_t816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, movingTextFeedback_t816_VTable/* vtableMethods */
	, movingTextFeedback_t816_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo movingTextFeedback_t816_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "movingTextFeedback"/* name */
	, ""/* namespaze */
	, movingTextFeedback_t816_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &movingTextFeedback_t816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &movingTextFeedback_t816_0_0_0/* byval_arg */
	, &movingTextFeedback_t816_1_0_0/* this_arg */
	, &movingTextFeedback_t816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (movingTextFeedback_t816)/* instance_size */
	, sizeof (movingTextFeedback_t816)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// movingTextKillsScr
#include "AssemblyU2DCSharp_movingTextKillsScr.h"
// Metadata Definition movingTextKillsScr
extern TypeInfo movingTextKillsScr_t817_il2cpp_TypeInfo;
// movingTextKillsScr
#include "AssemblyU2DCSharp_movingTextKillsScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextKillsScr::.ctor()
MethodInfo movingTextKillsScr__ctor_m3577_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&movingTextKillsScr__ctor_m3577/* method */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextKillsScr::Start()
MethodInfo movingTextKillsScr_Start_m3578_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&movingTextKillsScr_Start_m3578/* method */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextKillsScr::Update()
MethodInfo movingTextKillsScr_Update_m3579_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&movingTextKillsScr_Update_m3579/* method */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextKillsScr::OnGUI()
MethodInfo movingTextKillsScr_OnGUI_m3580_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&movingTextKillsScr_OnGUI_m3580/* method */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* movingTextKillsScr_t817_MethodInfos[] =
{
	&movingTextKillsScr__ctor_m3577_MethodInfo,
	&movingTextKillsScr_Start_m3578_MethodInfo,
	&movingTextKillsScr_Update_m3579_MethodInfo,
	&movingTextKillsScr_OnGUI_m3580_MethodInfo,
	NULL
};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo movingTextKillsScr_t817____st_2_FieldInfo = 
{
	"st"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* parent */
	, offsetof(movingTextKillsScr_t817, ___st_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* movingTextKillsScr_t817_FieldInfos[] =
{
	&movingTextKillsScr_t817____st_2_FieldInfo,
	NULL
};
static Il2CppMethodReference movingTextKillsScr_t817_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool movingTextKillsScr_t817_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType movingTextKillsScr_t817_0_0_0;
extern Il2CppType movingTextKillsScr_t817_1_0_0;
struct movingTextKillsScr_t817;
const Il2CppTypeDefinitionMetadata movingTextKillsScr_t817_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, movingTextKillsScr_t817_VTable/* vtableMethods */
	, movingTextKillsScr_t817_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo movingTextKillsScr_t817_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "movingTextKillsScr"/* name */
	, ""/* namespaze */
	, movingTextKillsScr_t817_MethodInfos/* methods */
	, NULL/* properties */
	, movingTextKillsScr_t817_FieldInfos/* fields */
	, NULL/* events */
	, &movingTextKillsScr_t817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &movingTextKillsScr_t817_0_0_0/* byval_arg */
	, &movingTextKillsScr_t817_1_0_0/* this_arg */
	, &movingTextKillsScr_t817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (movingTextKillsScr_t817)/* instance_size */
	, sizeof (movingTextKillsScr_t817)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// movingTextScr
#include "AssemblyU2DCSharp_movingTextScr.h"
// Metadata Definition movingTextScr
extern TypeInfo movingTextScr_t818_il2cpp_TypeInfo;
// movingTextScr
#include "AssemblyU2DCSharp_movingTextScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextScr::.ctor()
MethodInfo movingTextScr__ctor_m3581_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&movingTextScr__ctor_m3581/* method */
	, &movingTextScr_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextScr::Update()
MethodInfo movingTextScr_Update_m3582_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&movingTextScr_Update_m3582/* method */
	, &movingTextScr_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void movingTextScr::OnGUI()
MethodInfo movingTextScr_OnGUI_m3583_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&movingTextScr_OnGUI_m3583/* method */
	, &movingTextScr_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* movingTextScr_t818_MethodInfos[] =
{
	&movingTextScr__ctor_m3581_MethodInfo,
	&movingTextScr_Update_m3582_MethodInfo,
	&movingTextScr_OnGUI_m3583_MethodInfo,
	NULL
};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo movingTextScr_t818____st_2_FieldInfo = 
{
	"st"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &movingTextScr_t818_il2cpp_TypeInfo/* parent */
	, offsetof(movingTextScr_t818, ___st_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo movingTextScr_t818____bulletType_3_FieldInfo = 
{
	"bulletType"/* name */
	, &Int32_t189_0_0_6/* type */
	, &movingTextScr_t818_il2cpp_TypeInfo/* parent */
	, offsetof(movingTextScr_t818, ___bulletType_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* movingTextScr_t818_FieldInfos[] =
{
	&movingTextScr_t818____st_2_FieldInfo,
	&movingTextScr_t818____bulletType_3_FieldInfo,
	NULL
};
static Il2CppMethodReference movingTextScr_t818_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool movingTextScr_t818_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType movingTextScr_t818_0_0_0;
extern Il2CppType movingTextScr_t818_1_0_0;
struct movingTextScr_t818;
const Il2CppTypeDefinitionMetadata movingTextScr_t818_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, movingTextScr_t818_VTable/* vtableMethods */
	, movingTextScr_t818_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo movingTextScr_t818_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "movingTextScr"/* name */
	, ""/* namespaze */
	, movingTextScr_t818_MethodInfos/* methods */
	, NULL/* properties */
	, movingTextScr_t818_FieldInfos/* fields */
	, NULL/* events */
	, &movingTextScr_t818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &movingTextScr_t818_0_0_0/* byval_arg */
	, &movingTextScr_t818_1_0_0/* this_arg */
	, &movingTextScr_t818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (movingTextScr_t818)/* instance_size */
	, sizeof (movingTextScr_t818)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// musicToggle
#include "AssemblyU2DCSharp_musicToggle.h"
// Metadata Definition musicToggle
extern TypeInfo musicToggle_t819_il2cpp_TypeInfo;
// musicToggle
#include "AssemblyU2DCSharp_musicToggleMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void musicToggle::.ctor()
MethodInfo musicToggle__ctor_m3584_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&musicToggle__ctor_m3584/* method */
	, &musicToggle_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void musicToggle::Start()
MethodInfo musicToggle_Start_m3585_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&musicToggle_Start_m3585/* method */
	, &musicToggle_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo musicToggle_t819_musicToggle_U3CStartU3Em__A6_m3586_ParameterInfos[] = 
{
	{"", 0, 134217728, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void musicToggle::<Start>m__A6(System.Boolean)
MethodInfo musicToggle_U3CStartU3Em__A6_m3586_MethodInfo = 
{
	"<Start>m__A6"/* name */
	, (methodPointerType)&musicToggle_U3CStartU3Em__A6_m3586/* method */
	, &musicToggle_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, musicToggle_t819_musicToggle_U3CStartU3Em__A6_m3586_ParameterInfos/* parameters */
	, 320/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* musicToggle_t819_MethodInfos[] =
{
	&musicToggle__ctor_m3584_MethodInfo,
	&musicToggle_Start_m3585_MethodInfo,
	&musicToggle_U3CStartU3Em__A6_m3586_MethodInfo,
	NULL
};
extern Il2CppType Toggle_t720_0_0_1;
FieldInfo musicToggle_t819____tog_2_FieldInfo = 
{
	"tog"/* name */
	, &Toggle_t720_0_0_1/* type */
	, &musicToggle_t819_il2cpp_TypeInfo/* parent */
	, offsetof(musicToggle_t819, ___tog_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t721_0_0_17;
FieldInfo musicToggle_t819____U3CU3Ef__amU24cache1_3_FieldInfo = 
{
	"<>f__am$cache1"/* name */
	, &UnityAction_1_t721_0_0_17/* type */
	, &musicToggle_t819_il2cpp_TypeInfo/* parent */
	, offsetof(musicToggle_t819_StaticFields, ___U3CU3Ef__amU24cache1_3)/* offset */
	, 319/* custom_attributes_cache */

};
static FieldInfo* musicToggle_t819_FieldInfos[] =
{
	&musicToggle_t819____tog_2_FieldInfo,
	&musicToggle_t819____U3CU3Ef__amU24cache1_3_FieldInfo,
	NULL
};
static Il2CppMethodReference musicToggle_t819_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool musicToggle_t819_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType musicToggle_t819_0_0_0;
extern Il2CppType musicToggle_t819_1_0_0;
struct musicToggle_t819;
const Il2CppTypeDefinitionMetadata musicToggle_t819_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, musicToggle_t819_VTable/* vtableMethods */
	, musicToggle_t819_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo musicToggle_t819_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "musicToggle"/* name */
	, ""/* namespaze */
	, musicToggle_t819_MethodInfos/* methods */
	, NULL/* properties */
	, musicToggle_t819_FieldInfos/* fields */
	, NULL/* events */
	, &musicToggle_t819_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &musicToggle_t819_0_0_0/* byval_arg */
	, &musicToggle_t819_1_0_0/* this_arg */
	, &musicToggle_t819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (musicToggle_t819)/* instance_size */
	, sizeof (musicToggle_t819)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(musicToggle_t819_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// naveProxyController
#include "AssemblyU2DCSharp_naveProxyController.h"
// Metadata Definition naveProxyController
extern TypeInfo naveProxyController_t820_il2cpp_TypeInfo;
// naveProxyController
#include "AssemblyU2DCSharp_naveProxyControllerMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void naveProxyController::.ctor()
MethodInfo naveProxyController__ctor_m3587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&naveProxyController__ctor_m3587/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void naveProxyController::.cctor()
MethodInfo naveProxyController__cctor_m3588_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&naveProxyController__cctor_m3588/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void naveProxyController::Start()
MethodInfo naveProxyController_Start_m3589_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&naveProxyController_Start_m3589/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void naveProxyController::Update()
MethodInfo naveProxyController_Update_m3590_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&naveProxyController_Update_m3590/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean naveProxyController::isWorking()
MethodInfo naveProxyController_isWorking_m3591_MethodInfo = 
{
	"isWorking"/* name */
	, (methodPointerType)&naveProxyController_isWorking_m3591/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void naveProxyController::setWorking()
MethodInfo naveProxyController_setWorking_m3592_MethodInfo = 
{
	"setWorking"/* name */
	, (methodPointerType)&naveProxyController_setWorking_m3592/* method */
	, &naveProxyController_t820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* naveProxyController_t820_MethodInfos[] =
{
	&naveProxyController__ctor_m3587_MethodInfo,
	&naveProxyController__cctor_m3588_MethodInfo,
	&naveProxyController_Start_m3589_MethodInfo,
	&naveProxyController_Update_m3590_MethodInfo,
	&naveProxyController_isWorking_m3591_MethodInfo,
	&naveProxyController_setWorking_m3592_MethodInfo,
	NULL
};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo naveProxyController_t820____working_2_FieldInfo = 
{
	"working"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &naveProxyController_t820_il2cpp_TypeInfo/* parent */
	, offsetof(naveProxyController_t820, ___working_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo naveProxyController_t820____gizmosReady_3_FieldInfo = 
{
	"gizmosReady"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &naveProxyController_t820_il2cpp_TypeInfo/* parent */
	, offsetof(naveProxyController_t820_StaticFields, ___gizmosReady_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* naveProxyController_t820_FieldInfos[] =
{
	&naveProxyController_t820____working_2_FieldInfo,
	&naveProxyController_t820____gizmosReady_3_FieldInfo,
	NULL
};
static Il2CppMethodReference naveProxyController_t820_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool naveProxyController_t820_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType naveProxyController_t820_0_0_0;
extern Il2CppType naveProxyController_t820_1_0_0;
struct naveProxyController_t820;
const Il2CppTypeDefinitionMetadata naveProxyController_t820_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, naveProxyController_t820_VTable/* vtableMethods */
	, naveProxyController_t820_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo naveProxyController_t820_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "naveProxyController"/* name */
	, ""/* namespaze */
	, naveProxyController_t820_MethodInfos/* methods */
	, NULL/* properties */
	, naveProxyController_t820_FieldInfos/* fields */
	, NULL/* events */
	, &naveProxyController_t820_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &naveProxyController_t820_0_0_0/* byval_arg */
	, &naveProxyController_t820_1_0_0/* this_arg */
	, &naveProxyController_t820_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (naveProxyController_t820)/* instance_size */
	, sizeof (naveProxyController_t820)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(naveProxyController_t820_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// newWeaponRoomSrc/<animSpin>c__Iterator11
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CanimSpinU3Ec__Iterator.h"
// Metadata Definition newWeaponRoomSrc/<animSpin>c__Iterator11
extern TypeInfo U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo;
// newWeaponRoomSrc/<animSpin>c__Iterator11
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CanimSpinU3Ec__IteratorMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::.ctor()
MethodInfo U3CanimSpinU3Ec__Iterator11__ctor_m3593_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11__ctor_m3593/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 326/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object newWeaponRoomSrc/<animSpin>c__Iterator11::System.Collections.IEnumerator.get_Current()
MethodInfo U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 327/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean newWeaponRoomSrc/<animSpin>c__Iterator11::MoveNext()
MethodInfo U3CanimSpinU3Ec__Iterator11_MoveNext_m3596_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11_MoveNext_m3596/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Dispose()
MethodInfo U3CanimSpinU3Ec__Iterator11_Dispose_m3597_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11_Dispose_m3597/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 328/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<animSpin>c__Iterator11::Reset()
MethodInfo U3CanimSpinU3Ec__Iterator11_Reset_m3598_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CanimSpinU3Ec__Iterator11_Reset_m3598/* method */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 329/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CanimSpinU3Ec__Iterator11_t822_MethodInfos[] =
{
	&U3CanimSpinU3Ec__Iterator11__ctor_m3593_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_MoveNext_m3596_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_Dispose_m3597_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_Reset_m3598_MethodInfo,
	NULL
};
extern Il2CppType Vector3_t758_0_0_3;
FieldInfo U3CanimSpinU3Ec__Iterator11_t822____U3CzU3E__0_0_FieldInfo = 
{
	"<z>__0"/* name */
	, &Vector3_t758_0_0_3/* type */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, offsetof(U3CanimSpinU3Ec__Iterator11_t822, ___U3CzU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CanimSpinU3Ec__Iterator11_t822____U24PC_1_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, offsetof(U3CanimSpinU3Ec__Iterator11_t822, ___U24PC_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CanimSpinU3Ec__Iterator11_t822____U24current_2_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, offsetof(U3CanimSpinU3Ec__Iterator11_t822, ___U24current_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType newWeaponRoomSrc_t821_0_0_3;
FieldInfo U3CanimSpinU3Ec__Iterator11_t822____U3CU3Ef__this_3_FieldInfo = 
{
	"<>f__this"/* name */
	, &newWeaponRoomSrc_t821_0_0_3/* type */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, offsetof(U3CanimSpinU3Ec__Iterator11_t822, ___U3CU3Ef__this_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CanimSpinU3Ec__Iterator11_t822_FieldInfos[] =
{
	&U3CanimSpinU3Ec__Iterator11_t822____U3CzU3E__0_0_FieldInfo,
	&U3CanimSpinU3Ec__Iterator11_t822____U24PC_1_FieldInfo,
	&U3CanimSpinU3Ec__Iterator11_t822____U24current_2_FieldInfo,
	&U3CanimSpinU3Ec__Iterator11_t822____U3CU3Ef__this_3_FieldInfo,
	NULL
};
extern MethodInfo U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594_MethodInfo;
static PropertyInfo U3CanimSpinU3Ec__Iterator11_t822____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595_MethodInfo;
static PropertyInfo U3CanimSpinU3Ec__Iterator11_t822____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CanimSpinU3Ec__Iterator11_t822_PropertyInfos[] =
{
	&U3CanimSpinU3Ec__Iterator11_t822____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CanimSpinU3Ec__Iterator11_t822____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CanimSpinU3Ec__Iterator11_Dispose_m3597_MethodInfo;
extern MethodInfo U3CanimSpinU3Ec__Iterator11_MoveNext_m3596_MethodInfo;
extern MethodInfo U3CanimSpinU3Ec__Iterator11_Reset_m3598_MethodInfo;
static Il2CppMethodReference U3CanimSpinU3Ec__Iterator11_t822_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_Dispose_m3597_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3595_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_MoveNext_m3596_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3594_MethodInfo,
	&U3CanimSpinU3Ec__Iterator11_Reset_m3598_MethodInfo,
};
static bool U3CanimSpinU3Ec__Iterator11_t822_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CanimSpinU3Ec__Iterator11_t822_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CanimSpinU3Ec__Iterator11_t822_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CanimSpinU3Ec__Iterator11_t822_0_0_0;
extern Il2CppType U3CanimSpinU3Ec__Iterator11_t822_1_0_0;
extern TypeInfo newWeaponRoomSrc_t821_il2cpp_TypeInfo;
extern Il2CppType newWeaponRoomSrc_t821_0_0_0;
struct U3CanimSpinU3Ec__Iterator11_t822;
const Il2CppTypeDefinitionMetadata U3CanimSpinU3Ec__Iterator11_t822_DefinitionMetadata = 
{
	&newWeaponRoomSrc_t821_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CanimSpinU3Ec__Iterator11_t822_InterfacesTypeInfos/* implementedInterfaces */
	, U3CanimSpinU3Ec__Iterator11_t822_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CanimSpinU3Ec__Iterator11_t822_VTable/* vtableMethods */
	, U3CanimSpinU3Ec__Iterator11_t822_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<animSpin>c__Iterator11"/* name */
	, ""/* namespaze */
	, U3CanimSpinU3Ec__Iterator11_t822_MethodInfos/* methods */
	, U3CanimSpinU3Ec__Iterator11_t822_PropertyInfos/* properties */
	, U3CanimSpinU3Ec__Iterator11_t822_FieldInfos/* fields */
	, NULL/* events */
	, &U3CanimSpinU3Ec__Iterator11_t822_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 325/* custom_attributes_cache */
	, &U3CanimSpinU3Ec__Iterator11_t822_0_0_0/* byval_arg */
	, &U3CanimSpinU3Ec__Iterator11_t822_1_0_0/* this_arg */
	, &U3CanimSpinU3Ec__Iterator11_t822_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CanimSpinU3Ec__Iterator11_t822)/* instance_size */
	, sizeof (U3CanimSpinU3Ec__Iterator11_t822)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// newWeaponRoomSrc/<waiting>c__Iterator12
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CwaitingU3Ec__Iterator1.h"
// Metadata Definition newWeaponRoomSrc/<waiting>c__Iterator12
extern TypeInfo U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo;
// newWeaponRoomSrc/<waiting>c__Iterator12
#include "AssemblyU2DCSharp_newWeaponRoomSrc_U3CwaitingU3Ec__Iterator1MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::.ctor()
MethodInfo U3CwaitingU3Ec__Iterator12__ctor_m3599_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12__ctor_m3599/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 331/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.IEnumerator.get_Current()
MethodInfo U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 332/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean newWeaponRoomSrc/<waiting>c__Iterator12::MoveNext()
MethodInfo U3CwaitingU3Ec__Iterator12_MoveNext_m3602_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12_MoveNext_m3602/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Dispose()
MethodInfo U3CwaitingU3Ec__Iterator12_Dispose_m3603_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12_Dispose_m3603/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 333/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Reset()
MethodInfo U3CwaitingU3Ec__Iterator12_Reset_m3604_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CwaitingU3Ec__Iterator12_Reset_m3604/* method */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 334/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CwaitingU3Ec__Iterator12_t823_MethodInfos[] =
{
	&U3CwaitingU3Ec__Iterator12__ctor_m3599_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_MoveNext_m3602_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_Dispose_m3603_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_Reset_m3604_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CwaitingU3Ec__Iterator12_t823____U24PC_0_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitingU3Ec__Iterator12_t823, ___U24PC_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CwaitingU3Ec__Iterator12_t823____U24current_1_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* parent */
	, offsetof(U3CwaitingU3Ec__Iterator12_t823, ___U24current_1)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CwaitingU3Ec__Iterator12_t823_FieldInfos[] =
{
	&U3CwaitingU3Ec__Iterator12_t823____U24PC_0_FieldInfo,
	&U3CwaitingU3Ec__Iterator12_t823____U24current_1_FieldInfo,
	NULL
};
extern MethodInfo U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600_MethodInfo;
static PropertyInfo U3CwaitingU3Ec__Iterator12_t823____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601_MethodInfo;
static PropertyInfo U3CwaitingU3Ec__Iterator12_t823____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CwaitingU3Ec__Iterator12_t823_PropertyInfos[] =
{
	&U3CwaitingU3Ec__Iterator12_t823____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CwaitingU3Ec__Iterator12_t823____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CwaitingU3Ec__Iterator12_Dispose_m3603_MethodInfo;
extern MethodInfo U3CwaitingU3Ec__Iterator12_MoveNext_m3602_MethodInfo;
extern MethodInfo U3CwaitingU3Ec__Iterator12_Reset_m3604_MethodInfo;
static Il2CppMethodReference U3CwaitingU3Ec__Iterator12_t823_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_Dispose_m3603_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_MoveNext_m3602_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600_MethodInfo,
	&U3CwaitingU3Ec__Iterator12_Reset_m3604_MethodInfo,
};
static bool U3CwaitingU3Ec__Iterator12_t823_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CwaitingU3Ec__Iterator12_t823_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CwaitingU3Ec__Iterator12_t823_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CwaitingU3Ec__Iterator12_t823_0_0_0;
extern Il2CppType U3CwaitingU3Ec__Iterator12_t823_1_0_0;
struct U3CwaitingU3Ec__Iterator12_t823;
const Il2CppTypeDefinitionMetadata U3CwaitingU3Ec__Iterator12_t823_DefinitionMetadata = 
{
	&newWeaponRoomSrc_t821_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CwaitingU3Ec__Iterator12_t823_InterfacesTypeInfos/* implementedInterfaces */
	, U3CwaitingU3Ec__Iterator12_t823_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CwaitingU3Ec__Iterator12_t823_VTable/* vtableMethods */
	, U3CwaitingU3Ec__Iterator12_t823_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<waiting>c__Iterator12"/* name */
	, ""/* namespaze */
	, U3CwaitingU3Ec__Iterator12_t823_MethodInfos/* methods */
	, U3CwaitingU3Ec__Iterator12_t823_PropertyInfos/* properties */
	, U3CwaitingU3Ec__Iterator12_t823_FieldInfos/* fields */
	, NULL/* events */
	, &U3CwaitingU3Ec__Iterator12_t823_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 330/* custom_attributes_cache */
	, &U3CwaitingU3Ec__Iterator12_t823_0_0_0/* byval_arg */
	, &U3CwaitingU3Ec__Iterator12_t823_1_0_0/* this_arg */
	, &U3CwaitingU3Ec__Iterator12_t823_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CwaitingU3Ec__Iterator12_t823)/* instance_size */
	, sizeof (U3CwaitingU3Ec__Iterator12_t823)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// newWeaponRoomSrc
#include "AssemblyU2DCSharp_newWeaponRoomSrc.h"
// Metadata Definition newWeaponRoomSrc
// newWeaponRoomSrc
#include "AssemblyU2DCSharp_newWeaponRoomSrcMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::.ctor()
MethodInfo newWeaponRoomSrc__ctor_m3605_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&newWeaponRoomSrc__ctor_m3605/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::Start()
MethodInfo newWeaponRoomSrc_Start_m3606_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&newWeaponRoomSrc_Start_m3606/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::Update()
MethodInfo newWeaponRoomSrc_Update_m3607_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&newWeaponRoomSrc_Update_m3607/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator newWeaponRoomSrc::animSpin()
MethodInfo newWeaponRoomSrc_animSpin_m3608_MethodInfo = 
{
	"animSpin"/* name */
	, (methodPointerType)&newWeaponRoomSrc_animSpin_m3608/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 323/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator newWeaponRoomSrc::waiting()
MethodInfo newWeaponRoomSrc_waiting_m3609_MethodInfo = 
{
	"waiting"/* name */
	, (methodPointerType)&newWeaponRoomSrc_waiting_m3609/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 324/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean newWeaponRoomSrc::isReady()
MethodInfo newWeaponRoomSrc_isReady_m3610_MethodInfo = 
{
	"isReady"/* name */
	, (methodPointerType)&newWeaponRoomSrc_isReady_m3610/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo newWeaponRoomSrc_t821_newWeaponRoomSrc_setReady_m3611_ParameterInfos[] = 
{
	{"readyState", 0, 134220497, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::setReady(System.Boolean)
MethodInfo newWeaponRoomSrc_setReady_m3611_MethodInfo = 
{
	"setReady"/* name */
	, (methodPointerType)&newWeaponRoomSrc_setReady_m3611/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_SByte_t236/* invoker_method */
	, newWeaponRoomSrc_t821_newWeaponRoomSrc_setReady_m3611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::OnGUI()
MethodInfo newWeaponRoomSrc_OnGUI_m3612_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&newWeaponRoomSrc_OnGUI_m3612/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void newWeaponRoomSrc::OnApplicationQuit()
MethodInfo newWeaponRoomSrc_OnApplicationQuit_m3613_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&newWeaponRoomSrc_OnApplicationQuit_m3613/* method */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* newWeaponRoomSrc_t821_MethodInfos[] =
{
	&newWeaponRoomSrc__ctor_m3605_MethodInfo,
	&newWeaponRoomSrc_Start_m3606_MethodInfo,
	&newWeaponRoomSrc_Update_m3607_MethodInfo,
	&newWeaponRoomSrc_animSpin_m3608_MethodInfo,
	&newWeaponRoomSrc_waiting_m3609_MethodInfo,
	&newWeaponRoomSrc_isReady_m3610_MethodInfo,
	&newWeaponRoomSrc_setReady_m3611_MethodInfo,
	&newWeaponRoomSrc_OnGUI_m3612_MethodInfo,
	&newWeaponRoomSrc_OnApplicationQuit_m3613_MethodInfo,
	NULL
};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo newWeaponRoomSrc_t821____newWeapon_2_FieldInfo = 
{
	"newWeapon"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821, ___newWeapon_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo newWeaponRoomSrc_t821____messageRect_3_FieldInfo = 
{
	"messageRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821, ___messageRect_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType SpriteU5BU5D_t824_0_0_1;
FieldInfo newWeaponRoomSrc_t821____weaponsSprites_4_FieldInfo = 
{
	"weaponsSprites"/* name */
	, &SpriteU5BU5D_t824_0_0_1/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821, ___weaponsSprites_4)/* offset */
	, 321/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_22;
FieldInfo newWeaponRoomSrc_t821____isNewWeaponReady_5_FieldInfo = 
{
	"isNewWeaponReady"/* name */
	, &Boolean_t203_0_0_22/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821_StaticFields, ___isNewWeaponReady_5)/* offset */
	, 322/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_6;
FieldInfo newWeaponRoomSrc_t821____halfScreen_6_FieldInfo = 
{
	"halfScreen"/* name */
	, &Rect_t738_0_0_6/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821, ___halfScreen_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo newWeaponRoomSrc_t821____v_7_FieldInfo = 
{
	"v"/* name */
	, &Single_t202_0_0_1/* type */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* parent */
	, offsetof(newWeaponRoomSrc_t821, ___v_7)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* newWeaponRoomSrc_t821_FieldInfos[] =
{
	&newWeaponRoomSrc_t821____newWeapon_2_FieldInfo,
	&newWeaponRoomSrc_t821____messageRect_3_FieldInfo,
	&newWeaponRoomSrc_t821____weaponsSprites_4_FieldInfo,
	&newWeaponRoomSrc_t821____isNewWeaponReady_5_FieldInfo,
	&newWeaponRoomSrc_t821____halfScreen_6_FieldInfo,
	&newWeaponRoomSrc_t821____v_7_FieldInfo,
	NULL
};
static const Il2CppType* newWeaponRoomSrc_t821_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CanimSpinU3Ec__Iterator11_t822_0_0_0,
	&U3CwaitingU3Ec__Iterator12_t823_0_0_0,
};
static Il2CppMethodReference newWeaponRoomSrc_t821_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool newWeaponRoomSrc_t821_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType newWeaponRoomSrc_t821_1_0_0;
struct newWeaponRoomSrc_t821;
const Il2CppTypeDefinitionMetadata newWeaponRoomSrc_t821_DefinitionMetadata = 
{
	NULL/* declaringType */
	, newWeaponRoomSrc_t821_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, newWeaponRoomSrc_t821_VTable/* vtableMethods */
	, newWeaponRoomSrc_t821_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo newWeaponRoomSrc_t821_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "newWeaponRoomSrc"/* name */
	, ""/* namespaze */
	, newWeaponRoomSrc_t821_MethodInfos/* methods */
	, NULL/* properties */
	, newWeaponRoomSrc_t821_FieldInfos/* fields */
	, NULL/* events */
	, &newWeaponRoomSrc_t821_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &newWeaponRoomSrc_t821_0_0_0/* byval_arg */
	, &newWeaponRoomSrc_t821_1_0_0/* this_arg */
	, &newWeaponRoomSrc_t821_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (newWeaponRoomSrc_t821)/* instance_size */
	, sizeof (newWeaponRoomSrc_t821)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(newWeaponRoomSrc_t821_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// number
#include "AssemblyU2DCSharp_number.h"
// Metadata Definition number
extern TypeInfo number_t825_il2cpp_TypeInfo;
// number
#include "AssemblyU2DCSharp_numberMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void number::.ctor()
MethodInfo number__ctor_m3614_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&number__ctor_m3614/* method */
	, &number_t825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* number_t825_MethodInfos[] =
{
	&number__ctor_m3614_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo number_t825____n_2_FieldInfo = 
{
	"n"/* name */
	, &Int32_t189_0_0_6/* type */
	, &number_t825_il2cpp_TypeInfo/* parent */
	, offsetof(number_t825, ___n_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo number_t825____isCleared_3_FieldInfo = 
{
	"isCleared"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &number_t825_il2cpp_TypeInfo/* parent */
	, offsetof(number_t825, ___isCleared_3)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* number_t825_FieldInfos[] =
{
	&number_t825____n_2_FieldInfo,
	&number_t825____isCleared_3_FieldInfo,
	NULL
};
static Il2CppMethodReference number_t825_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool number_t825_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType number_t825_0_0_0;
extern Il2CppType number_t825_1_0_0;
struct number_t825;
const Il2CppTypeDefinitionMetadata number_t825_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, number_t825_VTable/* vtableMethods */
	, number_t825_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo number_t825_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "number"/* name */
	, ""/* namespaze */
	, number_t825_MethodInfos/* methods */
	, NULL/* properties */
	, number_t825_FieldInfos/* fields */
	, NULL/* events */
	, &number_t825_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &number_t825_0_0_0/* byval_arg */
	, &number_t825_1_0_0/* this_arg */
	, &number_t825_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (number_t825)/* instance_size */
	, sizeof (number_t825)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// orlaScr
#include "AssemblyU2DCSharp_orlaScr.h"
// Metadata Definition orlaScr
extern TypeInfo orlaScr_t826_il2cpp_TypeInfo;
// orlaScr
#include "AssemblyU2DCSharp_orlaScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void orlaScr::.ctor()
MethodInfo orlaScr__ctor_m3615_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&orlaScr__ctor_m3615/* method */
	, &orlaScr_t826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void orlaScr::Start()
MethodInfo orlaScr_Start_m3616_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&orlaScr_Start_m3616/* method */
	, &orlaScr_t826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void orlaScr::Update()
MethodInfo orlaScr_Update_m3617_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&orlaScr_Update_m3617/* method */
	, &orlaScr_t826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void orlaScr::OnGUI()
MethodInfo orlaScr_OnGUI_m3618_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&orlaScr_OnGUI_m3618/* method */
	, &orlaScr_t826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* orlaScr_t826_MethodInfos[] =
{
	&orlaScr__ctor_m3615_MethodInfo,
	&orlaScr_Start_m3616_MethodInfo,
	&orlaScr_Update_m3617_MethodInfo,
	&orlaScr_OnGUI_m3618_MethodInfo,
	NULL
};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo orlaScr_t826____orlitaSt_2_FieldInfo = 
{
	"orlitaSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &orlaScr_t826_il2cpp_TypeInfo/* parent */
	, offsetof(orlaScr_t826, ___orlitaSt_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* orlaScr_t826_FieldInfos[] =
{
	&orlaScr_t826____orlitaSt_2_FieldInfo,
	NULL
};
static Il2CppMethodReference orlaScr_t826_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool orlaScr_t826_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType orlaScr_t826_0_0_0;
extern Il2CppType orlaScr_t826_1_0_0;
struct orlaScr_t826;
const Il2CppTypeDefinitionMetadata orlaScr_t826_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, orlaScr_t826_VTable/* vtableMethods */
	, orlaScr_t826_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo orlaScr_t826_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "orlaScr"/* name */
	, ""/* namespaze */
	, orlaScr_t826_MethodInfos/* methods */
	, NULL/* properties */
	, orlaScr_t826_FieldInfos/* fields */
	, NULL/* events */
	, &orlaScr_t826_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &orlaScr_t826_0_0_0/* byval_arg */
	, &orlaScr_t826_1_0_0/* this_arg */
	, &orlaScr_t826_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (orlaScr_t826)/* instance_size */
	, sizeof (orlaScr_t826)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// paintBlue
#include "AssemblyU2DCSharp_paintBlue.h"
// Metadata Definition paintBlue
extern TypeInfo paintBlue_t827_il2cpp_TypeInfo;
// paintBlue
#include "AssemblyU2DCSharp_paintBlueMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paintBlue::.ctor()
MethodInfo paintBlue__ctor_m3619_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&paintBlue__ctor_m3619/* method */
	, &paintBlue_t827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paintBlue::Start()
MethodInfo paintBlue_Start_m3620_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&paintBlue_Start_m3620/* method */
	, &paintBlue_t827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* paintBlue_t827_MethodInfos[] =
{
	&paintBlue__ctor_m3619_MethodInfo,
	&paintBlue_Start_m3620_MethodInfo,
	NULL
};
static Il2CppMethodReference paintBlue_t827_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool paintBlue_t827_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType paintBlue_t827_0_0_0;
extern Il2CppType paintBlue_t827_1_0_0;
struct paintBlue_t827;
const Il2CppTypeDefinitionMetadata paintBlue_t827_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, paintBlue_t827_VTable/* vtableMethods */
	, paintBlue_t827_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo paintBlue_t827_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "paintBlue"/* name */
	, ""/* namespaze */
	, paintBlue_t827_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &paintBlue_t827_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &paintBlue_t827_0_0_0/* byval_arg */
	, &paintBlue_t827_1_0_0/* this_arg */
	, &paintBlue_t827_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (paintBlue_t827)/* instance_size */
	, sizeof (paintBlue_t827)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// paqueteScr
#include "AssemblyU2DCSharp_paqueteScr.h"
// Metadata Definition paqueteScr
extern TypeInfo paqueteScr_t828_il2cpp_TypeInfo;
// paqueteScr
#include "AssemblyU2DCSharp_paqueteScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paqueteScr::.ctor()
MethodInfo paqueteScr__ctor_m3621_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&paqueteScr__ctor_m3621/* method */
	, &paqueteScr_t828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paqueteScr::Start()
MethodInfo paqueteScr_Start_m3622_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&paqueteScr_Start_m3622/* method */
	, &paqueteScr_t828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paqueteScr::Update()
MethodInfo paqueteScr_Update_m3623_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&paqueteScr_Update_m3623/* method */
	, &paqueteScr_t828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paqueteScr::giveBullets()
MethodInfo paqueteScr_giveBullets_m3624_MethodInfo = 
{
	"giveBullets"/* name */
	, (methodPointerType)&paqueteScr_giveBullets_m3624/* method */
	, &paqueteScr_t828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void paqueteScr::triggerAnim()
MethodInfo paqueteScr_triggerAnim_m3625_MethodInfo = 
{
	"triggerAnim"/* name */
	, (methodPointerType)&paqueteScr_triggerAnim_m3625/* method */
	, &paqueteScr_t828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* paqueteScr_t828_MethodInfos[] =
{
	&paqueteScr__ctor_m3621_MethodInfo,
	&paqueteScr_Start_m3622_MethodInfo,
	&paqueteScr_Update_m3623_MethodInfo,
	&paqueteScr_giveBullets_m3624_MethodInfo,
	&paqueteScr_triggerAnim_m3625_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____wBlockBonus_2_FieldInfo = 
{
	"wBlockBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___wBlockBonus_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____laserBonus_3_FieldInfo = 
{
	"laserBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___laserBonus_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____TWayBonus_4_FieldInfo = 
{
	"TWayBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___TWayBonus_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____circularBonus_5_FieldInfo = 
{
	"circularBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___circularBonus_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____triBonus_6_FieldInfo = 
{
	"triBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___triBonus_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____moireBonus_7_FieldInfo = 
{
	"moireBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___moireBonus_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____bombaBonus_8_FieldInfo = 
{
	"bombaBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___bombaBonus_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____rayoBonus_9_FieldInfo = 
{
	"rayoBonus"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___rayoBonus_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo paqueteScr_t828____aumento_10_FieldInfo = 
{
	"aumento"/* name */
	, &Single_t202_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___aumento_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo paqueteScr_t828____animPaquete_11_FieldInfo = 
{
	"animPaquete"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___animPaquete_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____bulletType_12_FieldInfo = 
{
	"bulletType"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___bulletType_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo paqueteScr_t828____lifeTime_13_FieldInfo = 
{
	"lifeTime"/* name */
	, &Int32_t189_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___lifeTime_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo paqueteScr_t828____lift_14_FieldInfo = 
{
	"lift"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &paqueteScr_t828_il2cpp_TypeInfo/* parent */
	, offsetof(paqueteScr_t828, ___lift_14)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* paqueteScr_t828_FieldInfos[] =
{
	&paqueteScr_t828____wBlockBonus_2_FieldInfo,
	&paqueteScr_t828____laserBonus_3_FieldInfo,
	&paqueteScr_t828____TWayBonus_4_FieldInfo,
	&paqueteScr_t828____circularBonus_5_FieldInfo,
	&paqueteScr_t828____triBonus_6_FieldInfo,
	&paqueteScr_t828____moireBonus_7_FieldInfo,
	&paqueteScr_t828____bombaBonus_8_FieldInfo,
	&paqueteScr_t828____rayoBonus_9_FieldInfo,
	&paqueteScr_t828____aumento_10_FieldInfo,
	&paqueteScr_t828____animPaquete_11_FieldInfo,
	&paqueteScr_t828____bulletType_12_FieldInfo,
	&paqueteScr_t828____lifeTime_13_FieldInfo,
	&paqueteScr_t828____lift_14_FieldInfo,
	NULL
};
static Il2CppMethodReference paqueteScr_t828_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool paqueteScr_t828_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType paqueteScr_t828_0_0_0;
extern Il2CppType paqueteScr_t828_1_0_0;
struct paqueteScr_t828;
const Il2CppTypeDefinitionMetadata paqueteScr_t828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, paqueteScr_t828_VTable/* vtableMethods */
	, paqueteScr_t828_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo paqueteScr_t828_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "paqueteScr"/* name */
	, ""/* namespaze */
	, paqueteScr_t828_MethodInfos/* methods */
	, NULL/* properties */
	, paqueteScr_t828_FieldInfos/* fields */
	, NULL/* events */
	, &paqueteScr_t828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &paqueteScr_t828_0_0_0/* byval_arg */
	, &paqueteScr_t828_1_0_0/* this_arg */
	, &paqueteScr_t828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (paqueteScr_t828)/* instance_size */
	, sizeof (paqueteScr_t828)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// playerMovement/<moveCoin>c__Iterator13
#include "AssemblyU2DCSharp_playerMovement_U3CmoveCoinU3Ec__Iterator13.h"
// Metadata Definition playerMovement/<moveCoin>c__Iterator13
extern TypeInfo U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo;
// playerMovement/<moveCoin>c__Iterator13
#include "AssemblyU2DCSharp_playerMovement_U3CmoveCoinU3Ec__Iterator13MethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement/<moveCoin>c__Iterator13::.ctor()
MethodInfo U3CmoveCoinU3Ec__Iterator13__ctor_m3626_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13__ctor_m3626/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
MethodInfo U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 337/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.IEnumerator.get_Current()
MethodInfo U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 338/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean playerMovement/<moveCoin>c__Iterator13::MoveNext()
MethodInfo U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement/<moveCoin>c__Iterator13::Dispose()
MethodInfo U3CmoveCoinU3Ec__Iterator13_Dispose_m3630_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13_Dispose_m3630/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 339/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement/<moveCoin>c__Iterator13::Reset()
MethodInfo U3CmoveCoinU3Ec__Iterator13_Reset_m3631_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CmoveCoinU3Ec__Iterator13_Reset_m3631/* method */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 340/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CmoveCoinU3Ec__Iterator13_t829_MethodInfos[] =
{
	&U3CmoveCoinU3Ec__Iterator13__ctor_m3626_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_Dispose_m3630_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_Reset_m3631_MethodInfo,
	NULL
};
extern Il2CppType Vector2_t739_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____U3CtargetU3E__0_0_FieldInfo = 
{
	"<target>__0"/* name */
	, &Vector2_t739_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___U3CtargetU3E__0_0)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____coin_1_FieldInfo = 
{
	"coin"/* name */
	, &GameObject_t144_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___coin_1)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____U3CmovU3E__1_2_FieldInfo = 
{
	"<mov>__1"/* name */
	, &Vector2_t739_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___U3CmovU3E__1_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____U24PC_3_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t189_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___U24PC_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____U24current_4_FieldInfo = 
{
	"$current"/* name */
	, &Object_t_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___U24current_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_3;
FieldInfo U3CmoveCoinU3Ec__Iterator13_t829____U3CU24U3Ecoin_5_FieldInfo = 
{
	"<$>coin"/* name */
	, &GameObject_t144_0_0_3/* type */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, offsetof(U3CmoveCoinU3Ec__Iterator13_t829, ___U3CU24U3Ecoin_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* U3CmoveCoinU3Ec__Iterator13_t829_FieldInfos[] =
{
	&U3CmoveCoinU3Ec__Iterator13_t829____U3CtargetU3E__0_0_FieldInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____coin_1_FieldInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____U3CmovU3E__1_2_FieldInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____U24PC_3_FieldInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____U24current_4_FieldInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____U3CU24U3Ecoin_5_FieldInfo,
	NULL
};
extern MethodInfo U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627_MethodInfo;
static PropertyInfo U3CmoveCoinU3Ec__Iterator13_t829____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern MethodInfo U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628_MethodInfo;
static PropertyInfo U3CmoveCoinU3Ec__Iterator13_t829____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* U3CmoveCoinU3Ec__Iterator13_t829_PropertyInfos[] =
{
	&U3CmoveCoinU3Ec__Iterator13_t829____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CmoveCoinU3Ec__Iterator13_t829____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern MethodInfo U3CmoveCoinU3Ec__Iterator13_Dispose_m3630_MethodInfo;
extern MethodInfo U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629_MethodInfo;
extern MethodInfo U3CmoveCoinU3Ec__Iterator13_Reset_m3631_MethodInfo;
static Il2CppMethodReference U3CmoveCoinU3Ec__Iterator13_t829_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_Dispose_m3630_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627_MethodInfo,
	&U3CmoveCoinU3Ec__Iterator13_Reset_m3631_MethodInfo,
};
static bool U3CmoveCoinU3Ec__Iterator13_t829_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CmoveCoinU3Ec__Iterator13_t829_InterfacesTypeInfos[] = 
{
	&IDisposable_t191_0_0_0,
	&IEnumerator_t37_0_0_0,
	&IEnumerator_1_t166_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CmoveCoinU3Ec__Iterator13_t829_InterfacesOffsets[] = 
{
	{ &IDisposable_t191_0_0_0, 4},
	{ &IEnumerator_t37_0_0_0, 5},
	{ &IEnumerator_1_t166_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CmoveCoinU3Ec__Iterator13_t829_0_0_0;
extern Il2CppType U3CmoveCoinU3Ec__Iterator13_t829_1_0_0;
extern TypeInfo playerMovement_t830_il2cpp_TypeInfo;
extern Il2CppType playerMovement_t830_0_0_0;
struct U3CmoveCoinU3Ec__Iterator13_t829;
const Il2CppTypeDefinitionMetadata U3CmoveCoinU3Ec__Iterator13_t829_DefinitionMetadata = 
{
	&playerMovement_t830_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CmoveCoinU3Ec__Iterator13_t829_InterfacesTypeInfos/* implementedInterfaces */
	, U3CmoveCoinU3Ec__Iterator13_t829_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CmoveCoinU3Ec__Iterator13_t829_VTable/* vtableMethods */
	, U3CmoveCoinU3Ec__Iterator13_t829_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<moveCoin>c__Iterator13"/* name */
	, ""/* namespaze */
	, U3CmoveCoinU3Ec__Iterator13_t829_MethodInfos/* methods */
	, U3CmoveCoinU3Ec__Iterator13_t829_PropertyInfos/* properties */
	, U3CmoveCoinU3Ec__Iterator13_t829_FieldInfos/* fields */
	, NULL/* events */
	, &U3CmoveCoinU3Ec__Iterator13_t829_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 336/* custom_attributes_cache */
	, &U3CmoveCoinU3Ec__Iterator13_t829_0_0_0/* byval_arg */
	, &U3CmoveCoinU3Ec__Iterator13_t829_1_0_0/* this_arg */
	, &U3CmoveCoinU3Ec__Iterator13_t829_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CmoveCoinU3Ec__Iterator13_t829)/* instance_size */
	, sizeof (U3CmoveCoinU3Ec__Iterator13_t829)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// playerMovement
#include "AssemblyU2DCSharp_playerMovement.h"
// Metadata Definition playerMovement
// playerMovement
#include "AssemblyU2DCSharp_playerMovementMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement::.ctor()
MethodInfo playerMovement__ctor_m3632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&playerMovement__ctor_m3632/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement::move()
MethodInfo playerMovement_move_m3633_MethodInfo = 
{
	"move"/* name */
	, (methodPointerType)&playerMovement_move_m3633/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType enemyController_t785_0_0_0;
static ParameterInfo playerMovement_t830_playerMovement_rotateShipDirection_m3634_ParameterInfos[] = 
{
	{"_enemyController", 0, 134220498, 0, &enemyController_t785_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement::rotateShipDirection(enemyController)
MethodInfo playerMovement_rotateShipDirection_m3634_MethodInfo = 
{
	"rotateShipDirection"/* name */
	, (methodPointerType)&playerMovement_rotateShipDirection_m3634/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, playerMovement_t830_playerMovement_rotateShipDirection_m3634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Collider_t900_0_0_0;
static ParameterInfo playerMovement_t830_playerMovement_OnTriggerEnter_m3635_ParameterInfos[] = 
{
	{"other", 0, 134220499, 0, &Collider_t900_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void playerMovement::OnTriggerEnter(UnityEngine.Collider)
MethodInfo playerMovement_OnTriggerEnter_m3635_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&playerMovement_OnTriggerEnter_m3635/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, playerMovement_t830_playerMovement_OnTriggerEnter_m3635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t144_0_0_0;
static ParameterInfo playerMovement_t830_playerMovement_moveCoin_m3636_ParameterInfos[] = 
{
	{"coin", 0, 134220500, 0, &GameObject_t144_0_0_0},
};
extern Il2CppType IEnumerator_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator playerMovement::moveCoin(UnityEngine.GameObject)
MethodInfo playerMovement_moveCoin_m3636_MethodInfo = 
{
	"moveCoin"/* name */
	, (methodPointerType)&playerMovement_moveCoin_m3636/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, playerMovement_t830_playerMovement_moveCoin_m3636_ParameterInfos/* parameters */
	, 335/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t739_0_0_0;
static ParameterInfo playerMovement_t830_playerMovement_calculateAngle_m3637_ParameterInfos[] = 
{
	{"target", 0, 134220501, 0, &Vector2_t739_0_0_0},
};
extern Il2CppType Single_t202_0_0_0;
extern void* RuntimeInvoker_Single_t202_Vector2_t739 (MethodInfo* method, void* obj, void** args);
// System.Single playerMovement::calculateAngle(UnityEngine.Vector2)
MethodInfo playerMovement_calculateAngle_m3637_MethodInfo = 
{
	"calculateAngle"/* name */
	, (methodPointerType)&playerMovement_calculateAngle_m3637/* method */
	, &playerMovement_t830_il2cpp_TypeInfo/* declaring_type */
	, &Single_t202_0_0_0/* return_type */
	, RuntimeInvoker_Single_t202_Vector2_t739/* invoker_method */
	, playerMovement_t830_playerMovement_calculateAngle_m3637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* playerMovement_t830_MethodInfos[] =
{
	&playerMovement__ctor_m3632_MethodInfo,
	&playerMovement_move_m3633_MethodInfo,
	&playerMovement_rotateShipDirection_m3634_MethodInfo,
	&playerMovement_OnTriggerEnter_m3635_MethodInfo,
	&playerMovement_moveCoin_m3636_MethodInfo,
	&playerMovement_calculateAngle_m3637_MethodInfo,
	NULL
};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____speed_2_FieldInfo = 
{
	"speed"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___speed_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo playerMovement_t830____isDead_3_FieldInfo = 
{
	"isDead"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___isDead_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo playerMovement_t830____isMoving_4_FieldInfo = 
{
	"isMoving"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___isMoving_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____enemyController_5_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___enemyController_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____movingText_6_FieldInfo = 
{
	"movingText"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___movingText_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____feedback_7_FieldInfo = 
{
	"feedback"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___feedback_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____closest_8_FieldInfo = 
{
	"closest"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___closest_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____rayo_9_FieldInfo = 
{
	"rayo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___rayo_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____dust_10_FieldInfo = 
{
	"dust"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___dust_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____currentDust_11_FieldInfo = 
{
	"currentDust"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___currentDust_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____sizeDust_12_FieldInfo = 
{
	"sizeDust"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___sizeDust_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo playerMovement_t830____bulletFeedbackObj_13_FieldInfo = 
{
	"bulletFeedbackObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___bulletFeedbackObj_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____accX_14_FieldInfo = 
{
	"accX"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___accX_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____accY_15_FieldInfo = 
{
	"accY"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___accY_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____speedX_16_FieldInfo = 
{
	"speedX"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___speedX_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____speedY_17_FieldInfo = 
{
	"speedY"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___speedY_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____decc_18_FieldInfo = 
{
	"decc"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___decc_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo playerMovement_t830____acceleartionTouch_19_FieldInfo = 
{
	"acceleartionTouch"/* name */
	, &Single_t202_0_0_6/* type */
	, &playerMovement_t830_il2cpp_TypeInfo/* parent */
	, offsetof(playerMovement_t830, ___acceleartionTouch_19)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* playerMovement_t830_FieldInfos[] =
{
	&playerMovement_t830____speed_2_FieldInfo,
	&playerMovement_t830____isDead_3_FieldInfo,
	&playerMovement_t830____isMoving_4_FieldInfo,
	&playerMovement_t830____enemyController_5_FieldInfo,
	&playerMovement_t830____movingText_6_FieldInfo,
	&playerMovement_t830____feedback_7_FieldInfo,
	&playerMovement_t830____closest_8_FieldInfo,
	&playerMovement_t830____rayo_9_FieldInfo,
	&playerMovement_t830____dust_10_FieldInfo,
	&playerMovement_t830____currentDust_11_FieldInfo,
	&playerMovement_t830____sizeDust_12_FieldInfo,
	&playerMovement_t830____bulletFeedbackObj_13_FieldInfo,
	&playerMovement_t830____accX_14_FieldInfo,
	&playerMovement_t830____accY_15_FieldInfo,
	&playerMovement_t830____speedX_16_FieldInfo,
	&playerMovement_t830____speedY_17_FieldInfo,
	&playerMovement_t830____decc_18_FieldInfo,
	&playerMovement_t830____acceleartionTouch_19_FieldInfo,
	NULL
};
static const Il2CppType* playerMovement_t830_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CmoveCoinU3Ec__Iterator13_t829_0_0_0,
};
static Il2CppMethodReference playerMovement_t830_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool playerMovement_t830_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType playerMovement_t830_1_0_0;
struct playerMovement_t830;
const Il2CppTypeDefinitionMetadata playerMovement_t830_DefinitionMetadata = 
{
	NULL/* declaringType */
	, playerMovement_t830_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, playerMovement_t830_VTable/* vtableMethods */
	, playerMovement_t830_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo playerMovement_t830_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "playerMovement"/* name */
	, ""/* namespaze */
	, playerMovement_t830_MethodInfos/* methods */
	, NULL/* properties */
	, playerMovement_t830_FieldInfos/* fields */
	, NULL/* events */
	, &playerMovement_t830_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &playerMovement_t830_0_0_0/* byval_arg */
	, &playerMovement_t830_1_0_0/* this_arg */
	, &playerMovement_t830_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (playerMovement_t830)/* instance_size */
	, sizeof (playerMovement_t830)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// playerState
#include "AssemblyU2DCSharp_playerState.h"
// Metadata Definition playerState
extern TypeInfo playerState_t831_il2cpp_TypeInfo;
// playerState
#include "AssemblyU2DCSharp_playerStateMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerState::.ctor()
MethodInfo playerState__ctor_m3638_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&playerState__ctor_m3638/* method */
	, &playerState_t831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerState::Start()
MethodInfo playerState_Start_m3639_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&playerState_Start_m3639/* method */
	, &playerState_t831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void playerState::Update()
MethodInfo playerState_Update_m3640_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&playerState_Update_m3640/* method */
	, &playerState_t831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* playerState_t831_MethodInfos[] =
{
	&playerState__ctor_m3638_MethodInfo,
	&playerState_Start_m3639_MethodInfo,
	&playerState_Update_m3640_MethodInfo,
	NULL
};
static Il2CppMethodReference playerState_t831_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool playerState_t831_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType playerState_t831_0_0_0;
extern Il2CppType playerState_t831_1_0_0;
struct playerState_t831;
const Il2CppTypeDefinitionMetadata playerState_t831_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, playerState_t831_VTable/* vtableMethods */
	, playerState_t831_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo playerState_t831_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "playerState"/* name */
	, ""/* namespaze */
	, playerState_t831_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &playerState_t831_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &playerState_t831_0_0_0/* byval_arg */
	, &playerState_t831_1_0_0/* this_arg */
	, &playerState_t831_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (playerState_t831)/* instance_size */
	, sizeof (playerState_t831)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// randomColorScr
#include "AssemblyU2DCSharp_randomColorScr.h"
// Metadata Definition randomColorScr
extern TypeInfo randomColorScr_t832_il2cpp_TypeInfo;
// randomColorScr
#include "AssemblyU2DCSharp_randomColorScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void randomColorScr::.ctor()
MethodInfo randomColorScr__ctor_m3641_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&randomColorScr__ctor_m3641/* method */
	, &randomColorScr_t832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void randomColorScr::Start()
MethodInfo randomColorScr_Start_m3642_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&randomColorScr_Start_m3642/* method */
	, &randomColorScr_t832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void randomColorScr::Update()
MethodInfo randomColorScr_Update_m3643_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&randomColorScr_Update_m3643/* method */
	, &randomColorScr_t832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* randomColorScr_t832_MethodInfos[] =
{
	&randomColorScr__ctor_m3641_MethodInfo,
	&randomColorScr_Start_m3642_MethodInfo,
	&randomColorScr_Update_m3643_MethodInfo,
	NULL
};
extern Il2CppType Color_t747_0_0_1;
FieldInfo randomColorScr_t832____c_2_FieldInfo = 
{
	"c"/* name */
	, &Color_t747_0_0_1/* type */
	, &randomColorScr_t832_il2cpp_TypeInfo/* parent */
	, offsetof(randomColorScr_t832, ___c_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* randomColorScr_t832_FieldInfos[] =
{
	&randomColorScr_t832____c_2_FieldInfo,
	NULL
};
static Il2CppMethodReference randomColorScr_t832_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool randomColorScr_t832_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType randomColorScr_t832_0_0_0;
extern Il2CppType randomColorScr_t832_1_0_0;
struct randomColorScr_t832;
const Il2CppTypeDefinitionMetadata randomColorScr_t832_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, randomColorScr_t832_VTable/* vtableMethods */
	, randomColorScr_t832_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo randomColorScr_t832_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "randomColorScr"/* name */
	, ""/* namespaze */
	, randomColorScr_t832_MethodInfos/* methods */
	, NULL/* properties */
	, randomColorScr_t832_FieldInfos/* fields */
	, NULL/* events */
	, &randomColorScr_t832_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &randomColorScr_t832_0_0_0/* byval_arg */
	, &randomColorScr_t832_1_0_0/* this_arg */
	, &randomColorScr_t832_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (randomColorScr_t832)/* instance_size */
	, sizeof (randomColorScr_t832)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// rotateItself
#include "AssemblyU2DCSharp_rotateItself.h"
// Metadata Definition rotateItself
extern TypeInfo rotateItself_t833_il2cpp_TypeInfo;
// rotateItself
#include "AssemblyU2DCSharp_rotateItselfMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void rotateItself::.ctor()
MethodInfo rotateItself__ctor_m3644_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&rotateItself__ctor_m3644/* method */
	, &rotateItself_t833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void rotateItself::Start()
MethodInfo rotateItself_Start_m3645_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&rotateItself_Start_m3645/* method */
	, &rotateItself_t833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void rotateItself::Update()
MethodInfo rotateItself_Update_m3646_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&rotateItself_Update_m3646/* method */
	, &rotateItself_t833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* rotateItself_t833_MethodInfos[] =
{
	&rotateItself__ctor_m3644_MethodInfo,
	&rotateItself_Start_m3645_MethodInfo,
	&rotateItself_Update_m3646_MethodInfo,
	NULL
};
extern Il2CppType Vector3_t758_0_0_6;
FieldInfo rotateItself_t833____dir_2_FieldInfo = 
{
	"dir"/* name */
	, &Vector3_t758_0_0_6/* type */
	, &rotateItself_t833_il2cpp_TypeInfo/* parent */
	, offsetof(rotateItself_t833, ___dir_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* rotateItself_t833_FieldInfos[] =
{
	&rotateItself_t833____dir_2_FieldInfo,
	NULL
};
static Il2CppMethodReference rotateItself_t833_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool rotateItself_t833_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType rotateItself_t833_0_0_0;
extern Il2CppType rotateItself_t833_1_0_0;
struct rotateItself_t833;
const Il2CppTypeDefinitionMetadata rotateItself_t833_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, rotateItself_t833_VTable/* vtableMethods */
	, rotateItself_t833_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo rotateItself_t833_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "rotateItself"/* name */
	, ""/* namespaze */
	, rotateItself_t833_MethodInfos/* methods */
	, NULL/* properties */
	, rotateItself_t833_FieldInfos/* fields */
	, NULL/* events */
	, &rotateItself_t833_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &rotateItself_t833_0_0_0/* byval_arg */
	, &rotateItself_t833_1_0_0/* this_arg */
	, &rotateItself_t833_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (rotateItself_t833)/* instance_size */
	, sizeof (rotateItself_t833)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// songsBatch
#include "AssemblyU2DCSharp_songsBatch.h"
// Metadata Definition songsBatch
extern TypeInfo songsBatch_t834_il2cpp_TypeInfo;
// songsBatch
#include "AssemblyU2DCSharp_songsBatchMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void songsBatch::.ctor()
MethodInfo songsBatch__ctor_m3647_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&songsBatch__ctor_m3647/* method */
	, &songsBatch_t834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void songsBatch::Awake()
MethodInfo songsBatch_Awake_m3648_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&songsBatch_Awake_m3648/* method */
	, &songsBatch_t834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void songsBatch::selectSong()
MethodInfo songsBatch_selectSong_m3649_MethodInfo = 
{
	"selectSong"/* name */
	, (methodPointerType)&songsBatch_selectSong_m3649/* method */
	, &songsBatch_t834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void songsBatch::switcherButton()
MethodInfo songsBatch_switcherButton_m3650_MethodInfo = 
{
	"switcherButton"/* name */
	, (methodPointerType)&songsBatch_switcherButton_m3650/* method */
	, &songsBatch_t834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* songsBatch_t834_MethodInfos[] =
{
	&songsBatch__ctor_m3647_MethodInfo,
	&songsBatch_Awake_m3648_MethodInfo,
	&songsBatch_selectSong_m3649_MethodInfo,
	&songsBatch_switcherButton_m3650_MethodInfo,
	NULL
};
extern Il2CppType ArrayList_t737_0_0_6;
FieldInfo songsBatch_t834____songs_2_FieldInfo = 
{
	"songs"/* name */
	, &ArrayList_t737_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___songs_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____switcherSound_3_FieldInfo = 
{
	"switcherSound"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___switcherSound_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____track_1_4_FieldInfo = 
{
	"track_1"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___track_1_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____track_2_5_FieldInfo = 
{
	"track_2"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___track_2_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____track_3_6_FieldInfo = 
{
	"track_3"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___track_3_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____track_4_7_FieldInfo = 
{
	"track_4"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___track_4_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioClip_t753_0_0_6;
FieldInfo songsBatch_t834____track_5_8_FieldInfo = 
{
	"track_5"/* name */
	, &AudioClip_t753_0_0_6/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___track_5_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType AudioSource_t755_0_0_1;
FieldInfo songsBatch_t834____musicPlayer_9_FieldInfo = 
{
	"musicPlayer"/* name */
	, &AudioSource_t755_0_0_1/* type */
	, &songsBatch_t834_il2cpp_TypeInfo/* parent */
	, offsetof(songsBatch_t834, ___musicPlayer_9)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* songsBatch_t834_FieldInfos[] =
{
	&songsBatch_t834____songs_2_FieldInfo,
	&songsBatch_t834____switcherSound_3_FieldInfo,
	&songsBatch_t834____track_1_4_FieldInfo,
	&songsBatch_t834____track_2_5_FieldInfo,
	&songsBatch_t834____track_3_6_FieldInfo,
	&songsBatch_t834____track_4_7_FieldInfo,
	&songsBatch_t834____track_5_8_FieldInfo,
	&songsBatch_t834____musicPlayer_9_FieldInfo,
	NULL
};
static Il2CppMethodReference songsBatch_t834_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool songsBatch_t834_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType songsBatch_t834_0_0_0;
extern Il2CppType songsBatch_t834_1_0_0;
struct songsBatch_t834;
const Il2CppTypeDefinitionMetadata songsBatch_t834_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, songsBatch_t834_VTable/* vtableMethods */
	, songsBatch_t834_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo songsBatch_t834_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "songsBatch"/* name */
	, ""/* namespaze */
	, songsBatch_t834_MethodInfos/* methods */
	, NULL/* properties */
	, songsBatch_t834_FieldInfos/* fields */
	, NULL/* events */
	, &songsBatch_t834_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &songsBatch_t834_0_0_0/* byval_arg */
	, &songsBatch_t834_1_0_0/* this_arg */
	, &songsBatch_t834_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (songsBatch_t834)/* instance_size */
	, sizeof (songsBatch_t834)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// tilePrefabScr
#include "AssemblyU2DCSharp_tilePrefabScr.h"
// Metadata Definition tilePrefabScr
extern TypeInfo tilePrefabScr_t835_il2cpp_TypeInfo;
// tilePrefabScr
#include "AssemblyU2DCSharp_tilePrefabScrMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tilePrefabScr::.ctor()
MethodInfo tilePrefabScr__ctor_m3651_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&tilePrefabScr__ctor_m3651/* method */
	, &tilePrefabScr_t835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tilePrefabScr::Awake()
MethodInfo tilePrefabScr_Awake_m3652_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&tilePrefabScr_Awake_m3652/* method */
	, &tilePrefabScr_t835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tilePrefabScr::Update()
MethodInfo tilePrefabScr_Update_m3653_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&tilePrefabScr_Update_m3653/* method */
	, &tilePrefabScr_t835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* tilePrefabScr_t835_MethodInfos[] =
{
	&tilePrefabScr__ctor_m3651_MethodInfo,
	&tilePrefabScr_Awake_m3652_MethodInfo,
	&tilePrefabScr_Update_m3653_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_1;
FieldInfo tilePrefabScr_t835____t_2_FieldInfo = 
{
	"t"/* name */
	, &Int32_t189_0_0_1/* type */
	, &tilePrefabScr_t835_il2cpp_TypeInfo/* parent */
	, offsetof(tilePrefabScr_t835, ___t_2)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* tilePrefabScr_t835_FieldInfos[] =
{
	&tilePrefabScr_t835____t_2_FieldInfo,
	NULL
};
static Il2CppMethodReference tilePrefabScr_t835_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool tilePrefabScr_t835_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType tilePrefabScr_t835_0_0_0;
extern Il2CppType tilePrefabScr_t835_1_0_0;
struct tilePrefabScr_t835;
const Il2CppTypeDefinitionMetadata tilePrefabScr_t835_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, tilePrefabScr_t835_VTable/* vtableMethods */
	, tilePrefabScr_t835_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo tilePrefabScr_t835_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "tilePrefabScr"/* name */
	, ""/* namespaze */
	, tilePrefabScr_t835_MethodInfos/* methods */
	, NULL/* properties */
	, tilePrefabScr_t835_FieldInfos/* fields */
	, NULL/* events */
	, &tilePrefabScr_t835_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &tilePrefabScr_t835_0_0_0/* byval_arg */
	, &tilePrefabScr_t835_1_0_0/* this_arg */
	, &tilePrefabScr_t835_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (tilePrefabScr_t835)/* instance_size */
	, sizeof (tilePrefabScr_t835)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// triggerSplashBomb
#include "AssemblyU2DCSharp_triggerSplashBomb.h"
// Metadata Definition triggerSplashBomb
extern TypeInfo triggerSplashBomb_t836_il2cpp_TypeInfo;
// triggerSplashBomb
#include "AssemblyU2DCSharp_triggerSplashBombMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void triggerSplashBomb::.ctor()
MethodInfo triggerSplashBomb__ctor_m3654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&triggerSplashBomb__ctor_m3654/* method */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void triggerSplashBomb::triggerSplash()
MethodInfo triggerSplashBomb_triggerSplash_m3655_MethodInfo = 
{
	"triggerSplash"/* name */
	, (methodPointerType)&triggerSplashBomb_triggerSplash_m3655/* method */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void triggerSplashBomb::rayobomba()
MethodInfo triggerSplashBomb_rayobomba_m3656_MethodInfo = 
{
	"rayobomba"/* name */
	, (methodPointerType)&triggerSplashBomb_rayobomba_m3656/* method */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* triggerSplashBomb_t836_MethodInfos[] =
{
	&triggerSplashBomb__ctor_m3654_MethodInfo,
	&triggerSplashBomb_triggerSplash_m3655_MethodInfo,
	&triggerSplashBomb_rayobomba_m3656_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo triggerSplashBomb_t836____bombaSplash_2_FieldInfo = 
{
	"bombaSplash"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* parent */
	, offsetof(triggerSplashBomb_t836, ___bombaSplash_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo triggerSplashBomb_t836____rayosbomba_3_FieldInfo = 
{
	"rayosbomba"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* parent */
	, offsetof(triggerSplashBomb_t836, ___rayosbomba_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo triggerSplashBomb_t836____rayo_4_FieldInfo = 
{
	"rayo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* parent */
	, offsetof(triggerSplashBomb_t836, ___rayo_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo triggerSplashBomb_t836____enemyController_5_FieldInfo = 
{
	"enemyController"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* parent */
	, offsetof(triggerSplashBomb_t836, ___enemyController_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo triggerSplashBomb_t836____timerRays_6_FieldInfo = 
{
	"timerRays"/* name */
	, &Int32_t189_0_0_6/* type */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* parent */
	, offsetof(triggerSplashBomb_t836, ___timerRays_6)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* triggerSplashBomb_t836_FieldInfos[] =
{
	&triggerSplashBomb_t836____bombaSplash_2_FieldInfo,
	&triggerSplashBomb_t836____rayosbomba_3_FieldInfo,
	&triggerSplashBomb_t836____rayo_4_FieldInfo,
	&triggerSplashBomb_t836____enemyController_5_FieldInfo,
	&triggerSplashBomb_t836____timerRays_6_FieldInfo,
	NULL
};
static Il2CppMethodReference triggerSplashBomb_t836_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool triggerSplashBomb_t836_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType triggerSplashBomb_t836_0_0_0;
extern Il2CppType triggerSplashBomb_t836_1_0_0;
struct triggerSplashBomb_t836;
const Il2CppTypeDefinitionMetadata triggerSplashBomb_t836_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, triggerSplashBomb_t836_VTable/* vtableMethods */
	, triggerSplashBomb_t836_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo triggerSplashBomb_t836_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "triggerSplashBomb"/* name */
	, ""/* namespaze */
	, triggerSplashBomb_t836_MethodInfos/* methods */
	, NULL/* properties */
	, triggerSplashBomb_t836_FieldInfos/* fields */
	, NULL/* events */
	, &triggerSplashBomb_t836_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &triggerSplashBomb_t836_0_0_0/* byval_arg */
	, &triggerSplashBomb_t836_1_0_0/* this_arg */
	, &triggerSplashBomb_t836_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (triggerSplashBomb_t836)/* instance_size */
	, sizeof (triggerSplashBomb_t836)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// tutorialController
#include "AssemblyU2DCSharp_tutorialController.h"
// Metadata Definition tutorialController
extern TypeInfo tutorialController_t837_il2cpp_TypeInfo;
// tutorialController
#include "AssemblyU2DCSharp_tutorialControllerMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tutorialController::.ctor()
MethodInfo tutorialController__ctor_m3657_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&tutorialController__ctor_m3657/* method */
	, &tutorialController_t837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tutorialController::Start()
MethodInfo tutorialController_Start_m3658_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&tutorialController_Start_m3658/* method */
	, &tutorialController_t837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tutorialController::Update()
MethodInfo tutorialController_Update_m3659_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&tutorialController_Update_m3659/* method */
	, &tutorialController_t837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* tutorialController_t837_MethodInfos[] =
{
	&tutorialController__ctor_m3657_MethodInfo,
	&tutorialController_Start_m3658_MethodInfo,
	&tutorialController_Update_m3659_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____pod_2_FieldInfo = 
{
	"pod"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___pod_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo tutorialController_t837____podPos_3_FieldInfo = 
{
	"podPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___podPos_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____hole_4_FieldInfo = 
{
	"hole"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___hole_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____bomba_5_FieldInfo = 
{
	"bomba"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___bomba_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____coin_6_FieldInfo = 
{
	"coin"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___coin_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____enemyControllerObj_7_FieldInfo = 
{
	"enemyControllerObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___enemyControllerObj_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType enemyController_t785_0_0_1;
FieldInfo tutorialController_t837_____enemyController_8_FieldInfo = 
{
	"_enemyController"/* name */
	, &enemyController_t785_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ____enemyController_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____player_9_FieldInfo = 
{
	"player"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___player_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____bocadilloObj_10_FieldInfo = 
{
	"bocadilloObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___bocadilloObj_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Vector2_t739_0_0_1;
FieldInfo tutorialController_t837____bocaPos_11_FieldInfo = 
{
	"bocaPos"/* name */
	, &Vector2_t739_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___bocaPos_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_1;
FieldInfo tutorialController_t837____size_12_FieldInfo = 
{
	"size"/* name */
	, &Single_t202_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___size_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo tutorialController_t837____created_13_FieldInfo = 
{
	"created"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___created_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo tutorialController_t837____endTutorial_14_FieldInfo = 
{
	"endTutorial"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___endTutorial_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo tutorialController_t837____failedTutorial_15_FieldInfo = 
{
	"failedTutorial"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___failedTutorial_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialController_t837____bocadillo_16_FieldInfo = 
{
	"bocadillo"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialController_t837_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialController_t837, ___bocadillo_16)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* tutorialController_t837_FieldInfos[] =
{
	&tutorialController_t837____pod_2_FieldInfo,
	&tutorialController_t837____podPos_3_FieldInfo,
	&tutorialController_t837____hole_4_FieldInfo,
	&tutorialController_t837____bomba_5_FieldInfo,
	&tutorialController_t837____coin_6_FieldInfo,
	&tutorialController_t837____enemyControllerObj_7_FieldInfo,
	&tutorialController_t837_____enemyController_8_FieldInfo,
	&tutorialController_t837____player_9_FieldInfo,
	&tutorialController_t837____bocadilloObj_10_FieldInfo,
	&tutorialController_t837____bocaPos_11_FieldInfo,
	&tutorialController_t837____size_12_FieldInfo,
	&tutorialController_t837____created_13_FieldInfo,
	&tutorialController_t837____endTutorial_14_FieldInfo,
	&tutorialController_t837____failedTutorial_15_FieldInfo,
	&tutorialController_t837____bocadillo_16_FieldInfo,
	NULL
};
static Il2CppMethodReference tutorialController_t837_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool tutorialController_t837_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType tutorialController_t837_0_0_0;
extern Il2CppType tutorialController_t837_1_0_0;
struct tutorialController_t837;
const Il2CppTypeDefinitionMetadata tutorialController_t837_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, tutorialController_t837_VTable/* vtableMethods */
	, tutorialController_t837_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo tutorialController_t837_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "tutorialController"/* name */
	, ""/* namespaze */
	, tutorialController_t837_MethodInfos/* methods */
	, NULL/* properties */
	, tutorialController_t837_FieldInfos/* fields */
	, NULL/* events */
	, &tutorialController_t837_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &tutorialController_t837_0_0_0/* byval_arg */
	, &tutorialController_t837_1_0_0/* this_arg */
	, &tutorialController_t837_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (tutorialController_t837)/* instance_size */
	, sizeof (tutorialController_t837)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// tutorialPod
#include "AssemblyU2DCSharp_tutorialPod.h"
// Metadata Definition tutorialPod
extern TypeInfo tutorialPod_t838_il2cpp_TypeInfo;
// tutorialPod
#include "AssemblyU2DCSharp_tutorialPodMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tutorialPod::.ctor()
MethodInfo tutorialPod__ctor_m3660_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&tutorialPod__ctor_m3660/* method */
	, &tutorialPod_t838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void tutorialPod::Start()
MethodInfo tutorialPod_Start_m3661_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&tutorialPod_Start_m3661/* method */
	, &tutorialPod_t838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Collider_t900_0_0_0;
static ParameterInfo tutorialPod_t838_tutorialPod_OnTriggerEnter_m3662_ParameterInfos[] = 
{
	{"other", 0, 134220502, 0, &Collider_t900_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void tutorialPod::OnTriggerEnter(UnityEngine.Collider)
MethodInfo tutorialPod_OnTriggerEnter_m3662_MethodInfo = 
{
	"OnTriggerEnter"/* name */
	, (methodPointerType)&tutorialPod_OnTriggerEnter_m3662/* method */
	, &tutorialPod_t838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, tutorialPod_t838_tutorialPod_OnTriggerEnter_m3662_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* tutorialPod_t838_MethodInfos[] =
{
	&tutorialPod__ctor_m3660_MethodInfo,
	&tutorialPod_Start_m3661_MethodInfo,
	&tutorialPod_OnTriggerEnter_m3662_MethodInfo,
	NULL
};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo tutorialPod_t838____idPod_2_FieldInfo = 
{
	"idPod"/* name */
	, &Int32_t189_0_0_6/* type */
	, &tutorialPod_t838_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialPod_t838, ___idPod_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_6;
FieldInfo tutorialPod_t838____touched_3_FieldInfo = 
{
	"touched"/* name */
	, &Boolean_t203_0_0_6/* type */
	, &tutorialPod_t838_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialPod_t838, ___touched_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo tutorialPod_t838____growF_4_FieldInfo = 
{
	"growF"/* name */
	, &Single_t202_0_0_6/* type */
	, &tutorialPod_t838_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialPod_t838, ___growF_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo tutorialPod_t838____gameControlObj_5_FieldInfo = 
{
	"gameControlObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &tutorialPod_t838_il2cpp_TypeInfo/* parent */
	, offsetof(tutorialPod_t838, ___gameControlObj_5)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* tutorialPod_t838_FieldInfos[] =
{
	&tutorialPod_t838____idPod_2_FieldInfo,
	&tutorialPod_t838____touched_3_FieldInfo,
	&tutorialPod_t838____growF_4_FieldInfo,
	&tutorialPod_t838____gameControlObj_5_FieldInfo,
	NULL
};
static Il2CppMethodReference tutorialPod_t838_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool tutorialPod_t838_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType tutorialPod_t838_0_0_0;
extern Il2CppType tutorialPod_t838_1_0_0;
struct tutorialPod_t838;
const Il2CppTypeDefinitionMetadata tutorialPod_t838_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, tutorialPod_t838_VTable/* vtableMethods */
	, tutorialPod_t838_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo tutorialPod_t838_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "tutorialPod"/* name */
	, ""/* namespaze */
	, tutorialPod_t838_MethodInfos/* methods */
	, NULL/* properties */
	, tutorialPod_t838_FieldInfos/* fields */
	, NULL/* events */
	, &tutorialPod_t838_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &tutorialPod_t838_0_0_0/* byval_arg */
	, &tutorialPod_t838_1_0_0/* this_arg */
	, &tutorialPod_t838_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (tutorialPod_t838)/* instance_size */
	, sizeof (tutorialPod_t838)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// weaponIcon
#include "AssemblyU2DCSharp_weaponIcon.h"
// Metadata Definition weaponIcon
extern TypeInfo weaponIcon_t839_il2cpp_TypeInfo;
// weaponIcon
#include "AssemblyU2DCSharp_weaponIconMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponIcon::.ctor()
MethodInfo weaponIcon__ctor_m3663_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&weaponIcon__ctor_m3663/* method */
	, &weaponIcon_t839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponIcon::Start()
MethodInfo weaponIcon_Start_m3664_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&weaponIcon_Start_m3664/* method */
	, &weaponIcon_t839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponIcon::Update()
MethodInfo weaponIcon_Update_m3665_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&weaponIcon_Update_m3665/* method */
	, &weaponIcon_t839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* weaponIcon_t839_MethodInfos[] =
{
	&weaponIcon__ctor_m3663_MethodInfo,
	&weaponIcon_Start_m3664_MethodInfo,
	&weaponIcon_Update_m3665_MethodInfo,
	NULL
};
static Il2CppMethodReference weaponIcon_t839_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool weaponIcon_t839_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType weaponIcon_t839_0_0_0;
extern Il2CppType weaponIcon_t839_1_0_0;
struct weaponIcon_t839;
const Il2CppTypeDefinitionMetadata weaponIcon_t839_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, weaponIcon_t839_VTable/* vtableMethods */
	, weaponIcon_t839_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo weaponIcon_t839_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "weaponIcon"/* name */
	, ""/* namespaze */
	, weaponIcon_t839_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &weaponIcon_t839_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &weaponIcon_t839_0_0_0/* byval_arg */
	, &weaponIcon_t839_1_0_0/* this_arg */
	, &weaponIcon_t839_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (weaponIcon_t839)/* instance_size */
	, sizeof (weaponIcon_t839)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// weaponRoom
#include "AssemblyU2DCSharp_weaponRoom.h"
// Metadata Definition weaponRoom
extern TypeInfo weaponRoom_t840_il2cpp_TypeInfo;
// weaponRoom
#include "AssemblyU2DCSharp_weaponRoomMethodDeclarations.h"
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::.ctor()
MethodInfo weaponRoom__ctor_m3666_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&weaponRoom__ctor_m3666/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::Start()
MethodInfo weaponRoom_Start_m3667_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&weaponRoom_Start_m3667/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::Update()
MethodInfo weaponRoom_Update_m3668_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&weaponRoom_Update_m3668/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::setSizes()
MethodInfo weaponRoom_setSizes_m3669_MethodInfo = 
{
	"setSizes"/* name */
	, (methodPointerType)&weaponRoom_setSizes_m3669/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::OnGUI()
MethodInfo weaponRoom_OnGUI_m3670_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&weaponRoom_OnGUI_m3670/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args);
// System.Void weaponRoom::OnApplicationQuit()
MethodInfo weaponRoom_OnApplicationQuit_m3671_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&weaponRoom_OnApplicationQuit_m3671/* method */
	, &weaponRoom_t840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* weaponRoom_t840_MethodInfos[] =
{
	&weaponRoom__ctor_m3666_MethodInfo,
	&weaponRoom_Start_m3667_MethodInfo,
	&weaponRoom_Update_m3668_MethodInfo,
	&weaponRoom_setSizes_m3669_MethodInfo,
	&weaponRoom_OnGUI_m3670_MethodInfo,
	&weaponRoom_OnApplicationQuit_m3671_MethodInfo,
	NULL
};
extern Il2CppType GameObject_t144_0_0_6;
FieldInfo weaponRoom_t840____gameControlObj_2_FieldInfo = 
{
	"gameControlObj"/* name */
	, &GameObject_t144_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___gameControlObj_2)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo weaponRoom_t840____offsetWeaponIcon_3_FieldInfo = 
{
	"offsetWeaponIcon"/* name */
	, &Single_t202_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___offsetWeaponIcon_3)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo weaponRoom_t840____offsetForStripe_4_FieldInfo = 
{
	"offsetForStripe"/* name */
	, &Single_t202_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___offsetForStripe_4)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Single_t202_0_0_6;
FieldInfo weaponRoom_t840____offsetLetterShadow_5_FieldInfo = 
{
	"offsetLetterShadow"/* name */
	, &Single_t202_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___offsetLetterShadow_5)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo weaponRoom_t840____marginLeft_6_FieldInfo = 
{
	"marginLeft"/* name */
	, &Int32_t189_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___marginLeft_6)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Int32_t189_0_0_6;
FieldInfo weaponRoom_t840____marginTop_7_FieldInfo = 
{
	"marginTop"/* name */
	, &Int32_t189_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___marginTop_7)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo weaponRoom_t840____boxRect_8_FieldInfo = 
{
	"boxRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___boxRect_8)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_1;
FieldInfo weaponRoom_t840____messageRect_9_FieldInfo = 
{
	"messageRect"/* name */
	, &Rect_t738_0_0_1/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___messageRect_9)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____gearWordSt_10_FieldInfo = 
{
	"gearWordSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___gearWordSt_10)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____weaponsRoomSt_11_FieldInfo = 
{
	"weaponsRoomSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___weaponsRoomSt_11)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____lowBarButtonSt_12_FieldInfo = 
{
	"lowBarButtonSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___lowBarButtonSt_12)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____lowBarPlaySt_13_FieldInfo = 
{
	"lowBarPlaySt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___lowBarPlaySt_13)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____levelSt_14_FieldInfo = 
{
	"levelSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___levelSt_14)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t724_0_0_6;
FieldInfo weaponRoom_t840____titleSt_15_FieldInfo = 
{
	"titleSt"/* name */
	, &GUIStyle_t724_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___titleSt_15)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Texture_t736_0_0_6;
FieldInfo weaponRoom_t840____keyPic_16_FieldInfo = 
{
	"keyPic"/* name */
	, &Texture_t736_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___keyPic_16)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType ArrayList_t737_0_0_6;
FieldInfo weaponRoom_t840____iconBombaGfx_17_FieldInfo = 
{
	"iconBombaGfx"/* name */
	, &ArrayList_t737_0_0_6/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___iconBombaGfx_17)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_22;
FieldInfo weaponRoom_t840____weaponSelectHUD_18_FieldInfo = 
{
	"weaponSelectHUD"/* name */
	, &Rect_t738_0_0_22/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840_StaticFields, ___weaponSelectHUD_18)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Rect_t738_0_0_22;
FieldInfo weaponRoom_t840____levelSelectHUD_19_FieldInfo = 
{
	"levelSelectHUD"/* name */
	, &Rect_t738_0_0_22/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840_StaticFields, ___levelSelectHUD_19)/* offset */
	, 0/* custom_attributes_cache */

};
extern Il2CppType Boolean_t203_0_0_1;
FieldInfo weaponRoom_t840____screenPos_20_FieldInfo = 
{
	"screenPos"/* name */
	, &Boolean_t203_0_0_1/* type */
	, &weaponRoom_t840_il2cpp_TypeInfo/* parent */
	, offsetof(weaponRoom_t840, ___screenPos_20)/* offset */
	, 0/* custom_attributes_cache */

};
static FieldInfo* weaponRoom_t840_FieldInfos[] =
{
	&weaponRoom_t840____gameControlObj_2_FieldInfo,
	&weaponRoom_t840____offsetWeaponIcon_3_FieldInfo,
	&weaponRoom_t840____offsetForStripe_4_FieldInfo,
	&weaponRoom_t840____offsetLetterShadow_5_FieldInfo,
	&weaponRoom_t840____marginLeft_6_FieldInfo,
	&weaponRoom_t840____marginTop_7_FieldInfo,
	&weaponRoom_t840____boxRect_8_FieldInfo,
	&weaponRoom_t840____messageRect_9_FieldInfo,
	&weaponRoom_t840____gearWordSt_10_FieldInfo,
	&weaponRoom_t840____weaponsRoomSt_11_FieldInfo,
	&weaponRoom_t840____lowBarButtonSt_12_FieldInfo,
	&weaponRoom_t840____lowBarPlaySt_13_FieldInfo,
	&weaponRoom_t840____levelSt_14_FieldInfo,
	&weaponRoom_t840____titleSt_15_FieldInfo,
	&weaponRoom_t840____keyPic_16_FieldInfo,
	&weaponRoom_t840____iconBombaGfx_17_FieldInfo,
	&weaponRoom_t840____weaponSelectHUD_18_FieldInfo,
	&weaponRoom_t840____levelSelectHUD_19_FieldInfo,
	&weaponRoom_t840____screenPos_20_FieldInfo,
	NULL
};
static Il2CppMethodReference weaponRoom_t840_VTable[] =
{
	&Object_Equals_m1199_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1200_MethodInfo,
	&Object_ToString_m1201_MethodInfo,
};
static bool weaponRoom_t840_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType weaponRoom_t840_0_0_0;
extern Il2CppType weaponRoom_t840_1_0_0;
struct weaponRoom_t840;
const Il2CppTypeDefinitionMetadata weaponRoom_t840_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t26_0_0_0/* parent */
	, weaponRoom_t840_VTable/* vtableMethods */
	, weaponRoom_t840_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo weaponRoom_t840_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "weaponRoom"/* name */
	, ""/* namespaze */
	, weaponRoom_t840_MethodInfos/* methods */
	, NULL/* properties */
	, weaponRoom_t840_FieldInfos/* fields */
	, NULL/* events */
	, &weaponRoom_t840_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &weaponRoom_t840_0_0_0/* byval_arg */
	, &weaponRoom_t840_1_0_0/* this_arg */
	, &weaponRoom_t840_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (weaponRoom_t840)/* instance_size */
	, sizeof (weaponRoom_t840)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(weaponRoom_t840_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceive.h"
// Metadata Definition GooglePlayGames.BasicApi.InvitationReceivedDelegate
extern TypeInfo InvitationReceivedDelegate_t363_il2cpp_TypeInfo;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceiveMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo InvitationReceivedDelegate_t363_InvitationReceivedDelegate__ctor_m3672_ParameterInfos[] = 
{
	{"object", 0, 134220503, 0, &Object_t_0_0_0},
	{"method", 1, 134220504, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::.ctor(System.Object,System.IntPtr)
MethodInfo InvitationReceivedDelegate__ctor_m3672_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvitationReceivedDelegate__ctor_m3672/* method */
	, &InvitationReceivedDelegate_t363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, InvitationReceivedDelegate_t363_InvitationReceivedDelegate__ctor_m3672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Invitation_t341_0_0_0;
extern Il2CppType Invitation_t341_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo InvitationReceivedDelegate_t363_InvitationReceivedDelegate_Invoke_m3673_ParameterInfos[] = 
{
	{"invitation", 0, 134220505, 0, &Invitation_t341_0_0_0},
	{"shouldAutoAccept", 1, 134220506, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
MethodInfo InvitationReceivedDelegate_Invoke_m3673_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvitationReceivedDelegate_Invoke_m3673/* method */
	, &InvitationReceivedDelegate_t363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, InvitationReceivedDelegate_t363_InvitationReceivedDelegate_Invoke_m3673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Invitation_t341_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo InvitationReceivedDelegate_t363_InvitationReceivedDelegate_BeginInvoke_m3674_ParameterInfos[] = 
{
	{"invitation", 0, 134220507, 0, &Invitation_t341_0_0_0},
	{"shouldAutoAccept", 1, 134220508, 0, &Boolean_t203_0_0_0},
	{"callback", 2, 134220509, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134220510, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult GooglePlayGames.BasicApi.InvitationReceivedDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean,System.AsyncCallback,System.Object)
MethodInfo InvitationReceivedDelegate_BeginInvoke_m3674_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&InvitationReceivedDelegate_BeginInvoke_m3674/* method */
	, &InvitationReceivedDelegate_t363_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, InvitationReceivedDelegate_t363_InvitationReceivedDelegate_BeginInvoke_m3674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo InvitationReceivedDelegate_t363_InvitationReceivedDelegate_EndInvoke_m3675_ParameterInfos[] = 
{
	{"result", 0, 134220511, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::EndInvoke(System.IAsyncResult)
MethodInfo InvitationReceivedDelegate_EndInvoke_m3675_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&InvitationReceivedDelegate_EndInvoke_m3675/* method */
	, &InvitationReceivedDelegate_t363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, InvitationReceivedDelegate_t363_InvitationReceivedDelegate_EndInvoke_m3675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvitationReceivedDelegate_t363_MethodInfos[] =
{
	&InvitationReceivedDelegate__ctor_m3672_MethodInfo,
	&InvitationReceivedDelegate_Invoke_m3673_MethodInfo,
	&InvitationReceivedDelegate_BeginInvoke_m3674_MethodInfo,
	&InvitationReceivedDelegate_EndInvoke_m3675_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m1272_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m1273_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m1274_MethodInfo;
extern MethodInfo Delegate_Clone_m1275_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m1276_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m1277_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m1278_MethodInfo;
extern MethodInfo InvitationReceivedDelegate_Invoke_m3673_MethodInfo;
extern MethodInfo InvitationReceivedDelegate_BeginInvoke_m3674_MethodInfo;
extern MethodInfo InvitationReceivedDelegate_EndInvoke_m3675_MethodInfo;
static Il2CppMethodReference InvitationReceivedDelegate_t363_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&InvitationReceivedDelegate_Invoke_m3673_MethodInfo,
	&InvitationReceivedDelegate_BeginInvoke_m3674_MethodInfo,
	&InvitationReceivedDelegate_EndInvoke_m3675_MethodInfo,
};
static bool InvitationReceivedDelegate_t363_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppType ICloneable_t309_0_0_0;
extern Il2CppType ISerializable_t310_0_0_0;
static Il2CppInterfaceOffsetPair InvitationReceivedDelegate_t363_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType InvitationReceivedDelegate_t363_0_0_0;
extern Il2CppType InvitationReceivedDelegate_t363_1_0_0;
extern Il2CppType MulticastDelegate_t22_0_0_0;
struct InvitationReceivedDelegate_t363;
const Il2CppTypeDefinitionMetadata InvitationReceivedDelegate_t363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvitationReceivedDelegate_t363_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, InvitationReceivedDelegate_t363_VTable/* vtableMethods */
	, InvitationReceivedDelegate_t363_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo InvitationReceivedDelegate_t363_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvitationReceivedDelegate"/* name */
	, "GooglePlayGames.BasicApi"/* namespaze */
	, InvitationReceivedDelegate_t363_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &InvitationReceivedDelegate_t363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvitationReceivedDelegate_t363_0_0_0/* byval_arg */
	, &InvitationReceivedDelegate_t363_1_0_0/* this_arg */
	, &InvitationReceivedDelegate_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_InvitationReceivedDelegate_t363/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvitationReceivedDelegate_t363)/* instance_size */
	, sizeof (InvitationReceivedDelegate_t363)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1.h"
// Metadata Definition GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
extern TypeInfo MatchDelegate_t364_il2cpp_TypeInfo;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1MethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo MatchDelegate_t364_MatchDelegate__ctor_m3676_ParameterInfos[] = 
{
	{"object", 0, 134220512, 0, &Object_t_0_0_0},
	{"method", 1, 134220513, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::.ctor(System.Object,System.IntPtr)
MethodInfo MatchDelegate__ctor_m3676_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchDelegate__ctor_m3676/* method */
	, &MatchDelegate_t364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, MatchDelegate_t364_MatchDelegate__ctor_m3676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TurnBasedMatch_t353_0_0_0;
extern Il2CppType TurnBasedMatch_t353_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
static ParameterInfo MatchDelegate_t364_MatchDelegate_Invoke_m3677_ParameterInfos[] = 
{
	{"match", 0, 134220514, 0, &TurnBasedMatch_t353_0_0_0},
	{"shouldAutoLaunch", 1, 134220515, 0, &Boolean_t203_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
MethodInfo MatchDelegate_Invoke_m3677_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MatchDelegate_Invoke_m3677/* method */
	, &MatchDelegate_t364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_SByte_t236/* invoker_method */
	, MatchDelegate_t364_MatchDelegate_Invoke_m3677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TurnBasedMatch_t353_0_0_0;
extern Il2CppType Boolean_t203_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo MatchDelegate_t364_MatchDelegate_BeginInvoke_m3678_ParameterInfos[] = 
{
	{"match", 0, 134220516, 0, &TurnBasedMatch_t353_0_0_0},
	{"shouldAutoLaunch", 1, 134220517, 0, &Boolean_t203_0_0_0},
	{"callback", 2, 134220518, 0, &AsyncCallback_t20_0_0_0},
	{"object", 3, 134220519, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean,System.AsyncCallback,System.Object)
MethodInfo MatchDelegate_BeginInvoke_m3678_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MatchDelegate_BeginInvoke_m3678/* method */
	, &MatchDelegate_t364_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t/* invoker_method */
	, MatchDelegate_t364_MatchDelegate_BeginInvoke_m3678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo MatchDelegate_t364_MatchDelegate_EndInvoke_m3679_ParameterInfos[] = 
{
	{"result", 0, 134220520, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::EndInvoke(System.IAsyncResult)
MethodInfo MatchDelegate_EndInvoke_m3679_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MatchDelegate_EndInvoke_m3679/* method */
	, &MatchDelegate_t364_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, MatchDelegate_t364_MatchDelegate_EndInvoke_m3679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MatchDelegate_t364_MethodInfos[] =
{
	&MatchDelegate__ctor_m3676_MethodInfo,
	&MatchDelegate_Invoke_m3677_MethodInfo,
	&MatchDelegate_BeginInvoke_m3678_MethodInfo,
	&MatchDelegate_EndInvoke_m3679_MethodInfo,
	NULL
};
extern MethodInfo MatchDelegate_Invoke_m3677_MethodInfo;
extern MethodInfo MatchDelegate_BeginInvoke_m3678_MethodInfo;
extern MethodInfo MatchDelegate_EndInvoke_m3679_MethodInfo;
static Il2CppMethodReference MatchDelegate_t364_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&MatchDelegate_Invoke_m3677_MethodInfo,
	&MatchDelegate_BeginInvoke_m3678_MethodInfo,
	&MatchDelegate_EndInvoke_m3679_MethodInfo,
};
static bool MatchDelegate_t364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MatchDelegate_t364_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType MatchDelegate_t364_0_0_0;
extern Il2CppType MatchDelegate_t364_1_0_0;
struct MatchDelegate_t364;
const Il2CppTypeDefinitionMetadata MatchDelegate_t364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchDelegate_t364_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, MatchDelegate_t364_VTable/* vtableMethods */
	, MatchDelegate_t364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MatchDelegate_t364_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDelegate"/* name */
	, "GooglePlayGames.BasicApi.Multiplayer"/* namespaze */
	, MatchDelegate_t364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MatchDelegate_t364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDelegate_t364_0_0_0/* byval_arg */
	, &MatchDelegate_t364_1_0_0/* this_arg */
	, &MatchDelegate_t364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchDelegate_t364/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDelegate_t364)/* instance_size */
	, sizeof (MatchDelegate_t364)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0.h"
// Metadata Definition GooglePlayGames.BasicApi.SavedGame.ConflictCallback
extern TypeInfo ConflictCallback_t621_il2cpp_TypeInfo;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic_0MethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo ConflictCallback_t621_ConflictCallback__ctor_m3680_ParameterInfos[] = 
{
	{"object", 0, 134220521, 0, &Object_t_0_0_0},
	{"method", 1, 134220522, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::.ctor(System.Object,System.IntPtr)
MethodInfo ConflictCallback__ctor_m3680_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConflictCallback__ctor_m3680/* method */
	, &ConflictCallback_t621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, ConflictCallback_t621_ConflictCallback__ctor_m3680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConflictResolver_t617_0_0_0;
extern Il2CppType IConflictResolver_t617_0_0_0;
extern Il2CppType ISavedGameMetadata_t618_0_0_0;
extern Il2CppType ISavedGameMetadata_t618_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType ISavedGameMetadata_t618_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
static ParameterInfo ConflictCallback_t621_ConflictCallback_Invoke_m3681_ParameterInfos[] = 
{
	{"resolver", 0, 134220523, 0, &IConflictResolver_t617_0_0_0},
	{"original", 1, 134220524, 0, &ISavedGameMetadata_t618_0_0_0},
	{"originalData", 2, 134220525, 0, &ByteU5BU5D_t350_0_0_0},
	{"unmerged", 3, 134220526, 0, &ISavedGameMetadata_t618_0_0_0},
	{"unmergedData", 4, 134220527, 0, &ByteU5BU5D_t350_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
MethodInfo ConflictCallback_Invoke_m3681_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConflictCallback_Invoke_m3681/* method */
	, &ConflictCallback_t621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ConflictCallback_t621_ConflictCallback_Invoke_m3681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IConflictResolver_t617_0_0_0;
extern Il2CppType ISavedGameMetadata_t618_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType ISavedGameMetadata_t618_0_0_0;
extern Il2CppType ByteU5BU5D_t350_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ConflictCallback_t621_ConflictCallback_BeginInvoke_m3682_ParameterInfos[] = 
{
	{"resolver", 0, 134220528, 0, &IConflictResolver_t617_0_0_0},
	{"original", 1, 134220529, 0, &ISavedGameMetadata_t618_0_0_0},
	{"originalData", 2, 134220530, 0, &ByteU5BU5D_t350_0_0_0},
	{"unmerged", 3, 134220531, 0, &ISavedGameMetadata_t618_0_0_0},
	{"unmergedData", 4, 134220532, 0, &ByteU5BU5D_t350_0_0_0},
	{"callback", 5, 134220533, 0, &AsyncCallback_t20_0_0_0},
	{"object", 6, 134220534, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult GooglePlayGames.BasicApi.SavedGame.ConflictCallback::BeginInvoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],System.AsyncCallback,System.Object)
MethodInfo ConflictCallback_BeginInvoke_m3682_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ConflictCallback_BeginInvoke_m3682/* method */
	, &ConflictCallback_t621_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ConflictCallback_t621_ConflictCallback_BeginInvoke_m3682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo ConflictCallback_t621_ConflictCallback_EndInvoke_m3683_ParameterInfos[] = 
{
	{"result", 0, 134220535, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::EndInvoke(System.IAsyncResult)
MethodInfo ConflictCallback_EndInvoke_m3683_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ConflictCallback_EndInvoke_m3683/* method */
	, &ConflictCallback_t621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ConflictCallback_t621_ConflictCallback_EndInvoke_m3683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ConflictCallback_t621_MethodInfos[] =
{
	&ConflictCallback__ctor_m3680_MethodInfo,
	&ConflictCallback_Invoke_m3681_MethodInfo,
	&ConflictCallback_BeginInvoke_m3682_MethodInfo,
	&ConflictCallback_EndInvoke_m3683_MethodInfo,
	NULL
};
extern MethodInfo ConflictCallback_Invoke_m3681_MethodInfo;
extern MethodInfo ConflictCallback_BeginInvoke_m3682_MethodInfo;
extern MethodInfo ConflictCallback_EndInvoke_m3683_MethodInfo;
static Il2CppMethodReference ConflictCallback_t621_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&ConflictCallback_Invoke_m3681_MethodInfo,
	&ConflictCallback_BeginInvoke_m3682_MethodInfo,
	&ConflictCallback_EndInvoke_m3683_MethodInfo,
};
static bool ConflictCallback_t621_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ConflictCallback_t621_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ConflictCallback_t621_0_0_0;
extern Il2CppType ConflictCallback_t621_1_0_0;
struct ConflictCallback_t621;
const Il2CppTypeDefinitionMetadata ConflictCallback_t621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConflictCallback_t621_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, ConflictCallback_t621_VTable/* vtableMethods */
	, ConflictCallback_t621_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ConflictCallback_t621_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConflictCallback"/* name */
	, "GooglePlayGames.BasicApi.SavedGame"/* namespaze */
	, ConflictCallback_t621_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ConflictCallback_t621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConflictCallback_t621_0_0_0/* byval_arg */
	, &ConflictCallback_t621_1_0_0/* this_arg */
	, &ConflictCallback_t621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConflictCallback_t621/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConflictCallback_t621)/* instance_size */
	, sizeof (ConflictCallback_t621)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgress.h"
// Metadata Definition GooglePlayGames.ReportProgress
extern TypeInfo ReportProgress_t380_il2cpp_TypeInfo;
// GooglePlayGames.ReportProgress
#include "AssemblyU2DCSharp_GooglePlayGames_ReportProgressMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t_0_0_0;
static ParameterInfo ReportProgress_t380_ReportProgress__ctor_m3684_ParameterInfos[] = 
{
	{"object", 0, 134220536, 0, &Object_t_0_0_0},
	{"method", 1, 134220537, 0, &IntPtr_t_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.ReportProgress::.ctor(System.Object,System.IntPtr)
MethodInfo ReportProgress__ctor_m3684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReportProgress__ctor_m3684/* method */
	, &ReportProgress_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_IntPtr_t/* invoker_method */
	, ReportProgress_t380_ReportProgress__ctor_m3684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
static ParameterInfo ReportProgress_t380_ReportProgress_Invoke_m3685_ParameterInfos[] = 
{
	{"id", 0, 134220538, 0, &String_t_0_0_0},
	{"progress", 1, 134220539, 0, &Double_t234_0_0_0},
	{"callback", 2, 134220540, 0, &Action_1_t98_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>)
MethodInfo ReportProgress_Invoke_m3685_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ReportProgress_Invoke_m3685/* method */
	, &ReportProgress_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t/* invoker_method */
	, ReportProgress_t380_ReportProgress_Invoke_m3685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Double_t234_0_0_0;
extern Il2CppType Action_1_t98_0_0_0;
extern Il2CppType AsyncCallback_t20_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ReportProgress_t380_ReportProgress_BeginInvoke_m3686_ParameterInfos[] = 
{
	{"id", 0, 134220541, 0, &String_t_0_0_0},
	{"progress", 1, 134220542, 0, &Double_t234_0_0_0},
	{"callback", 2, 134220543, 0, &Action_1_t98_0_0_0},
	{"_callback", 3, 134220544, 0, &AsyncCallback_t20_0_0_0},
	{"object", 4, 134220545, 0, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t19_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Double_t234_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult GooglePlayGames.ReportProgress::BeginInvoke(System.String,System.Double,System.Action`1<System.Boolean>,System.AsyncCallback,System.Object)
MethodInfo ReportProgress_BeginInvoke_m3686_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ReportProgress_BeginInvoke_m3686/* method */
	, &ReportProgress_t380_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t19_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Double_t234_Object_t_Object_t_Object_t/* invoker_method */
	, ReportProgress_t380_ReportProgress_BeginInvoke_m3686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t19_0_0_0;
static ParameterInfo ReportProgress_t380_ReportProgress_EndInvoke_m3687_ParameterInfos[] = 
{
	{"result", 0, 134220546, 0, &IAsyncResult_t19_0_0_0},
};
extern Il2CppType Void_t260_0_0_0;
extern void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void GooglePlayGames.ReportProgress::EndInvoke(System.IAsyncResult)
MethodInfo ReportProgress_EndInvoke_m3687_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ReportProgress_EndInvoke_m3687/* method */
	, &ReportProgress_t380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t260_0_0_0/* return_type */
	, RuntimeInvoker_Void_t260_Object_t/* invoker_method */
	, ReportProgress_t380_ReportProgress_EndInvoke_m3687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ReportProgress_t380_MethodInfos[] =
{
	&ReportProgress__ctor_m3684_MethodInfo,
	&ReportProgress_Invoke_m3685_MethodInfo,
	&ReportProgress_BeginInvoke_m3686_MethodInfo,
	&ReportProgress_EndInvoke_m3687_MethodInfo,
	NULL
};
extern MethodInfo ReportProgress_Invoke_m3685_MethodInfo;
extern MethodInfo ReportProgress_BeginInvoke_m3686_MethodInfo;
extern MethodInfo ReportProgress_EndInvoke_m3687_MethodInfo;
static Il2CppMethodReference ReportProgress_t380_VTable[] =
{
	&MulticastDelegate_Equals_m1272_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&MulticastDelegate_GetHashCode_m1273_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&Delegate_Clone_m1275_MethodInfo,
	&MulticastDelegate_GetObjectData_m1274_MethodInfo,
	&MulticastDelegate_GetInvocationList_m1276_MethodInfo,
	&MulticastDelegate_CombineImpl_m1277_MethodInfo,
	&MulticastDelegate_RemoveImpl_m1278_MethodInfo,
	&ReportProgress_Invoke_m3685_MethodInfo,
	&ReportProgress_BeginInvoke_m3686_MethodInfo,
	&ReportProgress_EndInvoke_m3687_MethodInfo,
};
static bool ReportProgress_t380_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReportProgress_t380_InterfacesOffsets[] = 
{
	{ &ICloneable_t309_0_0_0, 4},
	{ &ISerializable_t310_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ReportProgress_t380_0_0_0;
extern Il2CppType ReportProgress_t380_1_0_0;
struct ReportProgress_t380;
const Il2CppTypeDefinitionMetadata ReportProgress_t380_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReportProgress_t380_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t22_0_0_0/* parent */
	, ReportProgress_t380_VTable/* vtableMethods */
	, ReportProgress_t380_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo ReportProgress_t380_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReportProgress"/* name */
	, "GooglePlayGames"/* namespaze */
	, ReportProgress_t380_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ReportProgress_t380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReportProgress_t380_0_0_0/* byval_arg */
	, &ReportProgress_t380_1_0_0/* this_arg */
	, &ReportProgress_t380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReportProgress_t380)/* instance_size */
	, sizeof (ReportProgress_t380)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
