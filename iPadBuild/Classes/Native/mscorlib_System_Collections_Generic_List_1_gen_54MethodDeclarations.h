﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.IntPtr>
struct List_1_t3807;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.IntPtr>
struct IEnumerable_1_t944;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t3784;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<System.IntPtr>
struct ICollection_1_t4396;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.IntPtr>
struct ReadOnlyCollection_1_t3810;
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.Predicate`1<System.IntPtr>
struct Predicate_1_t3814;
// System.Action`1<System.IntPtr>
struct Action_1_t940;
// System.Comparison`1<System.IntPtr>
struct Comparison_1_t3817;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.List`1/Enumerator<System.IntPtr>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"

// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor()
extern "C" void List_1__ctor_m20727_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1__ctor_m20727(__this, method) (( void (*) (List_1_t3807 *, MethodInfo*))List_1__ctor_m20727_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m20728_gshared (List_1_t3807 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m20728(__this, ___collection, method) (( void (*) (List_1_t3807 *, Object_t*, MethodInfo*))List_1__ctor_m20728_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.ctor(System.Int32)
extern "C" void List_1__ctor_m20729_gshared (List_1_t3807 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m20729(__this, ___capacity, method) (( void (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1__ctor_m20729_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::.cctor()
extern "C" void List_1__cctor_m20730_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m20730(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m20730_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.IntPtr>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20731_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20731(__this, method) (( Object_t* (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20731_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20732_gshared (List_1_t3807 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m20732(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3807 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m20732_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m20733_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20733(__this, method) (( Object_t * (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m20733_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m20734_gshared (List_1_t3807 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m20734(__this, ___item, method) (( int32_t (*) (List_1_t3807 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m20734_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m20735_gshared (List_1_t3807 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m20735(__this, ___item, method) (( bool (*) (List_1_t3807 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m20735_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m20736_gshared (List_1_t3807 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m20736(__this, ___item, method) (( int32_t (*) (List_1_t3807 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m20736_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m20737_gshared (List_1_t3807 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m20737(__this, ___index, ___item, method) (( void (*) (List_1_t3807 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m20737_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m20738_gshared (List_1_t3807 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m20738(__this, ___item, method) (( void (*) (List_1_t3807 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m20738_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20739_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20739(__this, method) (( bool (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m20740_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20740(__this, method) (( bool (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m20740_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.IntPtr>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m20741_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m20741(__this, method) (( Object_t * (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m20741_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m20742_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m20742(__this, method) (( bool (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m20742_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m20743_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m20743(__this, method) (( bool (*) (List_1_t3807 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m20743_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m20744_gshared (List_1_t3807 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m20744(__this, ___index, method) (( Object_t * (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m20744_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m20745_gshared (List_1_t3807 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m20745(__this, ___index, ___value, method) (( void (*) (List_1_t3807 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m20745_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Add(T)
extern "C" void List_1_Add_m20746_gshared (List_1_t3807 * __this, IntPtr_t ___item, MethodInfo* method);
#define List_1_Add_m20746(__this, ___item, method) (( void (*) (List_1_t3807 *, IntPtr_t, MethodInfo*))List_1_Add_m20746_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m20747_gshared (List_1_t3807 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m20747(__this, ___newCount, method) (( void (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m20747_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m20748_gshared (List_1_t3807 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m20748(__this, ___collection, method) (( void (*) (List_1_t3807 *, Object_t*, MethodInfo*))List_1_AddCollection_m20748_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m20749_gshared (List_1_t3807 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m20749(__this, ___enumerable, method) (( void (*) (List_1_t3807 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m20749_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m20750_gshared (List_1_t3807 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m20750(__this, ___collection, method) (( void (*) (List_1_t3807 *, Object_t*, MethodInfo*))List_1_AddRange_m20750_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.IntPtr>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3810 * List_1_AsReadOnly_m20751_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m20751(__this, method) (( ReadOnlyCollection_1_t3810 * (*) (List_1_t3807 *, MethodInfo*))List_1_AsReadOnly_m20751_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Clear()
extern "C" void List_1_Clear_m20752_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_Clear_m20752(__this, method) (( void (*) (List_1_t3807 *, MethodInfo*))List_1_Clear_m20752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::Contains(T)
extern "C" bool List_1_Contains_m20753_gshared (List_1_t3807 * __this, IntPtr_t ___item, MethodInfo* method);
#define List_1_Contains_m20753(__this, ___item, method) (( bool (*) (List_1_t3807 *, IntPtr_t, MethodInfo*))List_1_Contains_m20753_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m20754_gshared (List_1_t3807 * __this, IntPtrU5BU5D_t859* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m20754(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3807 *, IntPtrU5BU5D_t859*, int32_t, MethodInfo*))List_1_CopyTo_m20754_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.IntPtr>::Find(System.Predicate`1<T>)
extern "C" IntPtr_t List_1_Find_m20755_gshared (List_1_t3807 * __this, Predicate_1_t3814 * ___match, MethodInfo* method);
#define List_1_Find_m20755(__this, ___match, method) (( IntPtr_t (*) (List_1_t3807 *, Predicate_1_t3814 *, MethodInfo*))List_1_Find_m20755_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m20756_gshared (Object_t * __this /* static, unused */, Predicate_1_t3814 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m20756(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3814 *, MethodInfo*))List_1_CheckMatch_m20756_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m20757_gshared (List_1_t3807 * __this, Predicate_1_t3814 * ___match, MethodInfo* method);
#define List_1_FindIndex_m20757(__this, ___match, method) (( int32_t (*) (List_1_t3807 *, Predicate_1_t3814 *, MethodInfo*))List_1_FindIndex_m20757_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m20758_gshared (List_1_t3807 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3814 * ___match, MethodInfo* method);
#define List_1_GetIndex_m20758(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3807 *, int32_t, int32_t, Predicate_1_t3814 *, MethodInfo*))List_1_GetIndex_m20758_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m20759_gshared (List_1_t3807 * __this, Action_1_t940 * ___action, MethodInfo* method);
#define List_1_ForEach_m20759(__this, ___action, method) (( void (*) (List_1_t3807 *, Action_1_t940 *, MethodInfo*))List_1_ForEach_m20759_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.IntPtr>::GetEnumerator()
extern "C" Enumerator_t3808  List_1_GetEnumerator_m20760_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m20760(__this, method) (( Enumerator_t3808  (*) (List_1_t3807 *, MethodInfo*))List_1_GetEnumerator_m20760_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m20761_gshared (List_1_t3807 * __this, IntPtr_t ___item, MethodInfo* method);
#define List_1_IndexOf_m20761(__this, ___item, method) (( int32_t (*) (List_1_t3807 *, IntPtr_t, MethodInfo*))List_1_IndexOf_m20761_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m20762_gshared (List_1_t3807 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m20762(__this, ___start, ___delta, method) (( void (*) (List_1_t3807 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m20762_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m20763_gshared (List_1_t3807 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m20763(__this, ___index, method) (( void (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_CheckIndex_m20763_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m20764_gshared (List_1_t3807 * __this, int32_t ___index, IntPtr_t ___item, MethodInfo* method);
#define List_1_Insert_m20764(__this, ___index, ___item, method) (( void (*) (List_1_t3807 *, int32_t, IntPtr_t, MethodInfo*))List_1_Insert_m20764_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m20765_gshared (List_1_t3807 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m20765(__this, ___collection, method) (( void (*) (List_1_t3807 *, Object_t*, MethodInfo*))List_1_CheckCollection_m20765_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.IntPtr>::Remove(T)
extern "C" bool List_1_Remove_m20766_gshared (List_1_t3807 * __this, IntPtr_t ___item, MethodInfo* method);
#define List_1_Remove_m20766(__this, ___item, method) (( bool (*) (List_1_t3807 *, IntPtr_t, MethodInfo*))List_1_Remove_m20766_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m20767_gshared (List_1_t3807 * __this, Predicate_1_t3814 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m20767(__this, ___match, method) (( int32_t (*) (List_1_t3807 *, Predicate_1_t3814 *, MethodInfo*))List_1_RemoveAll_m20767_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m20768_gshared (List_1_t3807 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m20768(__this, ___index, method) (( void (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_RemoveAt_m20768_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Reverse()
extern "C" void List_1_Reverse_m20769_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_Reverse_m20769(__this, method) (( void (*) (List_1_t3807 *, MethodInfo*))List_1_Reverse_m20769_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Sort()
extern "C" void List_1_Sort_m20770_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_Sort_m20770(__this, method) (( void (*) (List_1_t3807 *, MethodInfo*))List_1_Sort_m20770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m20771_gshared (List_1_t3807 * __this, Comparison_1_t3817 * ___comparison, MethodInfo* method);
#define List_1_Sort_m20771(__this, ___comparison, method) (( void (*) (List_1_t3807 *, Comparison_1_t3817 *, MethodInfo*))List_1_Sort_m20771_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.IntPtr>::ToArray()
extern "C" IntPtrU5BU5D_t859* List_1_ToArray_m20772_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_ToArray_m20772(__this, method) (( IntPtrU5BU5D_t859* (*) (List_1_t3807 *, MethodInfo*))List_1_ToArray_m20772_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::TrimExcess()
extern "C" void List_1_TrimExcess_m20773_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_TrimExcess_m20773(__this, method) (( void (*) (List_1_t3807 *, MethodInfo*))List_1_TrimExcess_m20773_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m20774_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_get_Capacity_m20774(__this, method) (( int32_t (*) (List_1_t3807 *, MethodInfo*))List_1_get_Capacity_m20774_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m20775_gshared (List_1_t3807 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m20775(__this, ___value, method) (( void (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_set_Capacity_m20775_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.IntPtr>::get_Count()
extern "C" int32_t List_1_get_Count_m20776_gshared (List_1_t3807 * __this, MethodInfo* method);
#define List_1_get_Count_m20776(__this, method) (( int32_t (*) (List_1_t3807 *, MethodInfo*))List_1_get_Count_m20776_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.IntPtr>::get_Item(System.Int32)
extern "C" IntPtr_t List_1_get_Item_m20777_gshared (List_1_t3807 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m20777(__this, ___index, method) (( IntPtr_t (*) (List_1_t3807 *, int32_t, MethodInfo*))List_1_get_Item_m20777_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.IntPtr>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m20778_gshared (List_1_t3807 * __this, int32_t ___index, IntPtr_t ___value, MethodInfo* method);
#define List_1_set_Item_m20778(__this, ___index, ___value, method) (( void (*) (List_1_t3807 *, int32_t, IntPtr_t, MethodInfo*))List_1_set_Item_m20778_gshared)(__this, ___index, ___value, method)
