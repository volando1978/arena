﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration
struct NearbyConnectionConfiguration_t362;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t361;

// System.Void GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::.ctor(System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>,System.Int64)
extern "C" void NearbyConnectionConfiguration__ctor_m1462 (NearbyConnectionConfiguration_t362 * __this, Action_1_t361 * ___callback, int64_t ___localClientId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_LocalClientId()
extern "C" int64_t NearbyConnectionConfiguration_get_LocalClientId_m1463 (NearbyConnectionConfiguration_t362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus> GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration::get_InitializationCallback()
extern "C" Action_1_t361 * NearbyConnectionConfiguration_get_InitializationCallback_m1464 (NearbyConnectionConfiguration_t362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
