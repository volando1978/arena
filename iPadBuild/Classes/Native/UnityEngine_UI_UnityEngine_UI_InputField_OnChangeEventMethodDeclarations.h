﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t1256;

// System.Void UnityEngine.UI.InputField/OnChangeEvent::.ctor()
extern "C" void OnChangeEvent__ctor_m5039 (OnChangeEvent_t1256 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
