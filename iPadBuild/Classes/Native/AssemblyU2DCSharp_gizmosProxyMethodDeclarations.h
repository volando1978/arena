﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gizmosProxy
struct gizmosProxy_t804;

// System.Void gizmosProxy::.ctor()
extern "C" void gizmosProxy__ctor_m3509 (gizmosProxy_t804 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gizmosProxy::createGizmos()
extern "C" void gizmosProxy_createGizmos_m3510 (gizmosProxy_t804 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
