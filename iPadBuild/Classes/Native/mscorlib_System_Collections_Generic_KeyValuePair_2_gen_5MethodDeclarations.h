﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct KeyValuePair_2_t3531;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m17099_gshared (KeyValuePair_2_t3531 * __this, Object_t * ___key, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m17099(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3531 *, Object_t *, int32_t, MethodInfo*))KeyValuePair_2__ctor_m17099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m17100_gshared (KeyValuePair_2_t3531 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m17100(__this, method) (( Object_t * (*) (KeyValuePair_2_t3531 *, MethodInfo*))KeyValuePair_2_get_Key_m17100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17101_gshared (KeyValuePair_2_t3531 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m17101(__this, ___value, method) (( void (*) (KeyValuePair_2_t3531 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Key_m17101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m17102_gshared (KeyValuePair_2_t3531 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m17102(__this, method) (( int32_t (*) (KeyValuePair_2_t3531 *, MethodInfo*))KeyValuePair_2_get_Value_m17102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17103_gshared (KeyValuePair_2_t3531 * __this, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m17103(__this, ___value, method) (( void (*) (KeyValuePair_2_t3531 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m17103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m17104_gshared (KeyValuePair_2_t3531 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m17104(__this, method) (( String_t* (*) (KeyValuePair_2_t3531 *, MethodInfo*))KeyValuePair_2_ToString_m17104_gshared)(__this, method)
