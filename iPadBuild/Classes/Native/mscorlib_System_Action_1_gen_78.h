﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t230;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Component>
struct  Action_1_t3885  : public MulticastDelegate_t22
{
};
