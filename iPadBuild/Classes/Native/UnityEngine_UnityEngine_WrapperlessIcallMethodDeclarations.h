﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t1606;

// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m7301 (WrapperlessIcall_t1606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
