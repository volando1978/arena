﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "replacements_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t2183_il2cpp_TypeInfo;
// <Module>
#include "replacements_U3CModuleU3EMethodDeclarations.h"
static MethodInfo* U3CModuleU3E_t2183_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType U3CModuleU3E_t2183_0_0_0;
extern Il2CppType U3CModuleU3E_t2183_1_0_0;
struct U3CModuleU3E_t2183;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t2183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo U3CModuleU3E_t2183_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t2183_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &U3CModuleU3E_t2183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t2183_0_0_0/* byval_arg */
	, &U3CModuleU3E_t2183_1_0_0/* this_arg */
	, &U3CModuleU3E_t2183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t2183)/* instance_size */
	, sizeof (U3CModuleU3E_t2183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTable.h"
// Metadata Definition Replacements.MSCompatUnicodeTable
extern TypeInfo MSCompatUnicodeTable_t2184_il2cpp_TypeInfo;
// Replacements.MSCompatUnicodeTable
#include "replacements_Replacements_MSCompatUnicodeTableMethodDeclarations.h"
extern Il2CppType Boolean_t203_0_0_0;
extern void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args);
// System.Boolean Replacements.MSCompatUnicodeTable::get_IsReady()
MethodInfo MSCompatUnicodeTable_get_IsReady_m8988_MethodInfo = 
{
	"get_IsReady"/* name */
	, (methodPointerType)&MSCompatUnicodeTable_get_IsReady_m8988/* method */
	, &MSCompatUnicodeTable_t2184_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t203_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t203/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MSCompatUnicodeTable_t2184_MethodInfos[] =
{
	&MSCompatUnicodeTable_get_IsReady_m8988_MethodInfo,
	NULL
};
extern MethodInfo MSCompatUnicodeTable_get_IsReady_m8988_MethodInfo;
static PropertyInfo MSCompatUnicodeTable_t2184____IsReady_PropertyInfo = 
{
	&MSCompatUnicodeTable_t2184_il2cpp_TypeInfo/* parent */
	, "IsReady"/* name */
	, &MSCompatUnicodeTable_get_IsReady_m8988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static PropertyInfo* MSCompatUnicodeTable_t2184_PropertyInfos[] =
{
	&MSCompatUnicodeTable_t2184____IsReady_PropertyInfo,
	NULL
};
extern MethodInfo Object_Equals_m1176_MethodInfo;
extern MethodInfo Object_Finalize_m1177_MethodInfo;
extern MethodInfo Object_GetHashCode_m1178_MethodInfo;
extern MethodInfo Object_ToString_m1179_MethodInfo;
static Il2CppMethodReference MSCompatUnicodeTable_t2184_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool MSCompatUnicodeTable_t2184_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType MSCompatUnicodeTable_t2184_0_0_0;
extern Il2CppType MSCompatUnicodeTable_t2184_1_0_0;
extern Il2CppType Object_t_0_0_0;
struct MSCompatUnicodeTable_t2184;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTable_t2184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTable_t2184_VTable/* vtableMethods */
	, MSCompatUnicodeTable_t2184_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo MSCompatUnicodeTable_t2184_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTable"/* name */
	, "Replacements"/* namespaze */
	, MSCompatUnicodeTable_t2184_MethodInfos/* methods */
	, MSCompatUnicodeTable_t2184_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MSCompatUnicodeTable_t2184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTable_t2184_0_0_0/* byval_arg */
	, &MSCompatUnicodeTable_t2184_1_0_0/* this_arg */
	, &MSCompatUnicodeTable_t2184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTable_t2184)/* instance_size */
	, sizeof (MSCompatUnicodeTable_t2184)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElement.h"
// Metadata Definition Replacements.SecurityElement
extern TypeInfo SecurityElement_t2185_il2cpp_TypeInfo;
// Replacements.SecurityElement
#include "replacements_Replacements_SecurityElementMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
static ParameterInfo SecurityElement_t2185_SecurityElement_ToString_m8989_ParameterInfos[] = 
{
	{"__this", 0, 134217729, 0, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Replacements.SecurityElement::ToString(System.Object)
MethodInfo SecurityElement_ToString_m8989_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&SecurityElement_ToString_m8989/* method */
	, &SecurityElement_t2185_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, SecurityElement_t2185_SecurityElement_ToString_m8989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SecurityElement_t2185_MethodInfos[] =
{
	&SecurityElement_ToString_m8989_MethodInfo,
	NULL
};
static Il2CppMethodReference SecurityElement_t2185_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool SecurityElement_t2185_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType SecurityElement_t2185_0_0_0;
extern Il2CppType SecurityElement_t2185_1_0_0;
struct SecurityElement_t2185;
const Il2CppTypeDefinitionMetadata SecurityElement_t2185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityElement_t2185_VTable/* vtableMethods */
	, SecurityElement_t2185_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo SecurityElement_t2185_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityElement"/* name */
	, "Replacements"/* namespaze */
	, SecurityElement_t2185_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SecurityElement_t2185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityElement_t2185_0_0_0/* byval_arg */
	, &SecurityElement_t2185_1_0_0/* this_arg */
	, &SecurityElement_t2185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityElement_t2185)/* instance_size */
	, sizeof (SecurityElement_t2185)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServices.h"
// Metadata Definition Replacements.RemotingServices
extern TypeInfo RemotingServices_t2186_il2cpp_TypeInfo;
// Replacements.RemotingServices
#include "replacements_Replacements_RemotingServicesMethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_1_0_2;
extern Il2CppType String_t_1_0_0;
static ParameterInfo RemotingServices_t2186_RemotingServices_GetClientChannelSinkChain_m8990_ParameterInfos[] = 
{
	{"url", 0, 134217730, 0, &String_t_0_0_0},
	{"channelData", 1, 134217731, 0, &Object_t_0_0_0},
	{"objectUri", 2, 134217732, 0, &String_t_1_0_2},
};
extern Il2CppType IMessageSink_t2187_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t319 (MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink Replacements.RemotingServices::GetClientChannelSinkChain(System.String,System.Object,System.String&)
MethodInfo RemotingServices_GetClientChannelSinkChain_m8990_MethodInfo = 
{
	"GetClientChannelSinkChain"/* name */
	, (methodPointerType)&RemotingServices_GetClientChannelSinkChain_m8990/* method */
	, &RemotingServices_t2186_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t2187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t319/* invoker_method */
	, RemotingServices_t2186_RemotingServices_GetClientChannelSinkChain_m8990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
extern Il2CppType ObjectU5BU5D_t208_0_0_0;
static ParameterInfo RemotingServices_t2186_RemotingServices_CreateClientProxy_m8991_ParameterInfos[] = 
{
	{"objectType", 0, 134217733, 0, &Type_t_0_0_0},
	{"url", 1, 134217734, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134217735, 0, &ObjectU5BU5D_t208_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object Replacements.RemotingServices::CreateClientProxy(System.Type,System.String,System.Object[])
MethodInfo RemotingServices_CreateClientProxy_m8991_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m8991/* method */
	, &RemotingServices_t2186_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t2186_RemotingServices_CreateClientProxy_m8991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RemotingServices_t2186_MethodInfos[] =
{
	&RemotingServices_GetClientChannelSinkChain_m8990_MethodInfo,
	&RemotingServices_CreateClientProxy_m8991_MethodInfo,
	NULL
};
static Il2CppMethodReference RemotingServices_t2186_VTable[] =
{
	&Object_Equals_m1176_MethodInfo,
	&Object_Finalize_m1177_MethodInfo,
	&Object_GetHashCode_m1178_MethodInfo,
	&Object_ToString_m1179_MethodInfo,
};
static bool RemotingServices_t2186_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_replacements_dll_Image;
extern Il2CppType RemotingServices_t2186_0_0_0;
extern Il2CppType RemotingServices_t2186_1_0_0;
struct RemotingServices_t2186;
const Il2CppTypeDefinitionMetadata RemotingServices_t2186_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t2186_VTable/* vtableMethods */
	, RemotingServices_t2186_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */

};
TypeInfo RemotingServices_t2186_il2cpp_TypeInfo = 
{
	&g_replacements_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "Replacements"/* namespaze */
	, RemotingServices_t2186_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &RemotingServices_t2186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingServices_t2186_0_0_0/* byval_arg */
	, &RemotingServices_t2186_1_0_0/* this_arg */
	, &RemotingServices_t2186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t2186)/* instance_size */
	, sizeof (RemotingServices_t2186)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
