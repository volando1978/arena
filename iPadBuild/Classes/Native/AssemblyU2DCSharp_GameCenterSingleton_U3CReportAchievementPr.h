﻿#pragma once
#include <stdint.h>
// GameCenterSingleton
struct GameCenterSingleton_t730;
// System.Object
#include "mscorlib_System_Object.h"
// GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65
struct  U3CReportAchievementProgressU3Ec__AnonStorey65_t731  : public Object_t
{
	// System.Boolean GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65::success
	bool ___success_0;
	// GameCenterSingleton GameCenterSingleton/<ReportAchievementProgress>c__AnonStorey65::<>f__this
	GameCenterSingleton_t730 * ___U3CU3Ef__this_1;
};
