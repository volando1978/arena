﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$136
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU24136_t1804 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24136_t1804__padding[136];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$136
#pragma pack(push, tp, 1)
struct U24ArrayTypeU24136_t1804_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24136_t1804__padding[136];
	};
};
#pragma pack(pop, tp)
