﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t3488;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t920;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m16162_gshared (ValueCollection_t3488 * __this, Dictionary_2_t920 * ___dictionary, MethodInfo* method);
#define ValueCollection__ctor_m16162(__this, ___dictionary, method) (( void (*) (ValueCollection_t3488 *, Dictionary_2_t920 *, MethodInfo*))ValueCollection__ctor_m16162_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared (ValueCollection_t3488 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164(__this, ___item, method) (( void (*) (ValueCollection_t3488 *, Object_t *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166(__this, method) (( void (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared (ValueCollection_t3488 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168(__this, ___item, method) (( bool (*) (ValueCollection_t3488 *, Object_t *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared (ValueCollection_t3488 * __this, Object_t * ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170(__this, ___item, method) (( bool (*) (ValueCollection_t3488 *, Object_t *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172(__this, method) (( Object_t* (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared (ValueCollection_t3488 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m16174(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3488 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176(__this, method) (( Object_t * (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178(__this, method) (( bool (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180(__this, method) (( bool (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182(__this, method) (( Object_t * (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m16184_gshared (ValueCollection_t3488 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_CopyTo_m16184(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3488 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))ValueCollection_CopyTo_m16184_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::GetEnumerator()
extern "C" Enumerator_t3489  ValueCollection_GetEnumerator_m16186_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_GetEnumerator_m16186(__this, method) (( Enumerator_t3489  (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_GetEnumerator_m16186_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m16188_gshared (ValueCollection_t3488 * __this, MethodInfo* method);
#define ValueCollection_get_Count_m16188(__this, method) (( int32_t (*) (ValueCollection_t3488 *, MethodInfo*))ValueCollection_get_Count_m16188_gshared)(__this, method)
