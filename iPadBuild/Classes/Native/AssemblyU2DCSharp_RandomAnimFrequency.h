﻿#pragma once
#include <stdint.h>
// UnityEngine.Animator
struct Animator_t749;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RandomAnimFrequency
struct  RandomAnimFrequency_t750  : public MonoBehaviour_t26
{
	// UnityEngine.Animator RandomAnimFrequency::anim
	Animator_t749 * ___anim_2;
};
