﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PluginVersion
struct PluginVersion_t716;

// System.Void GooglePlayGames.PluginVersion::.ctor()
extern "C" void PluginVersion__ctor_m3134 (PluginVersion_t716 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
