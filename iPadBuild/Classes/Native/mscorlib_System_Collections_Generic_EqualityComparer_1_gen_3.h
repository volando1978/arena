﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.UInt32>
struct EqualityComparer_1_t3668;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.UInt32>
struct  EqualityComparer_1_t3668  : public Object_t
{
};
struct EqualityComparer_1_t3668_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt32>::_default
	EqualityComparer_1_t3668 * ____default_0;
};
