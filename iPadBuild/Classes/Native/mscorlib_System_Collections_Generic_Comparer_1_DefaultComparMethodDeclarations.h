﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t3424;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::.ctor()
extern "C" void DefaultComparer__ctor_m15519_gshared (DefaultComparer_t3424 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m15519(__this, method) (( void (*) (DefaultComparer_t3424 *, MethodInfo*))DefaultComparer__ctor_m15519_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15520_gshared (DefaultComparer_t3424 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define DefaultComparer_Compare_m15520(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3424 *, Object_t *, Object_t *, MethodInfo*))DefaultComparer_Compare_m15520_gshared)(__this, ___x, ___y, method)
