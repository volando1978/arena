﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t720;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t721;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// AudioToggle
struct  AudioToggle_t722  : public MonoBehaviour_t26
{
	// UnityEngine.UI.Toggle AudioToggle::tog
	Toggle_t720 * ___tog_2;
};
struct AudioToggle_t722_StaticFields{
	// UnityEngine.Events.UnityAction`1<System.Boolean> AudioToggle::<>f__am$cache1
	UnityAction_1_t721 * ___U3CU3Ef__amU24cache1_3;
};
