﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Enumerator_t4070;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t1520;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_t1521;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20MethodDeclarations.h"
#define Enumerator__ctor_m24312(__this, ___dictionary, method) (( void (*) (Enumerator_t4070 *, Dictionary_2_t1521 *, MethodInfo*))Enumerator__ctor_m22333_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24313(__this, method) (( Object_t * (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24314(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24315(__this, method) (( Object_t * (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24316(__this, method) (( Object_t * (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m24317(__this, method) (( bool (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_MoveNext_m22338_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m24318(__this, method) (( KeyValuePair_2_t4067  (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_get_Current_m22339_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24319(__this, method) (( int32_t (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_get_CurrentKey_m22340_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24320(__this, method) (( LayoutCache_t1520 * (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_get_CurrentValue_m22341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m24321(__this, method) (( void (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_VerifyState_m22342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24322(__this, method) (( void (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_VerifyCurrent_m22343_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m24323(__this, method) (( void (*) (Enumerator_t4070 *, MethodInfo*))Enumerator_Dispose_m22344_gshared)(__this, method)
