﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCurrencyStorageAndroid
struct VirtualCurrencyStorageAndroid_t68;

// System.Void Soomla.Store.VirtualCurrencyStorageAndroid::.ctor()
extern "C" void VirtualCurrencyStorageAndroid__ctor_m257 (VirtualCurrencyStorageAndroid_t68 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
