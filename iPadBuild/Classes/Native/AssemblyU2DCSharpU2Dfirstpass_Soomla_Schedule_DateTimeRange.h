﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// Soomla.Schedule/DateTimeRange
struct  DateTimeRange_t47  : public Object_t
{
	// System.DateTime Soomla.Schedule/DateTimeRange::Start
	DateTime_t48  ___Start_0;
	// System.DateTime Soomla.Schedule/DateTimeRange::End
	DateTime_t48  ___End_1;
};
