﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant[]
struct MultiplayerParticipantU5BU5D_t3739;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct  List_1_t891  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::_items
	MultiplayerParticipantU5BU5D_t3739* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::_version
	int32_t ____version_3;
};
struct List_1_t891_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::EmptyArray
	MultiplayerParticipantU5BU5D_t3739* ___EmptyArray_4;
};
