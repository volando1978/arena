﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E
struct U3CHelperForSessionU3Ec__AnonStorey2E_t596;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::.ctor()
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E__ctor_m2452 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__24(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__24_m2453 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__25(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__25_m2454 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__26(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__26_m2455 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::<>m__27(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void U3CHelperForSessionU3Ec__AnonStorey2E_U3CU3Em__27_m2456 (U3CHelperForSessionU3Ec__AnonStorey2E_t596 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
