﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23
struct U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533;
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;

// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23::.ctor()
extern "C" void U3CRegisterInvitationDelegateU3Ec__AnonStorey23__ctor_m2253 (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23::<>m__1B(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void U3CRegisterInvitationDelegateU3Ec__AnonStorey23_U3CU3Em__1B_m2254 (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * __this, Invitation_t341 * ___invitation, bool ___autoAccept, MethodInfo* method) IL2CPP_METHOD_ATTR;
