﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>
struct DefaultComparer_t3816;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>::.ctor()
extern "C" void DefaultComparer__ctor_m20867_gshared (DefaultComparer_t3816 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m20867(__this, method) (( void (*) (DefaultComparer_t3816 *, MethodInfo*))DefaultComparer__ctor_m20867_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.IntPtr>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m20868_gshared (DefaultComparer_t3816 * __this, IntPtr_t ___x, IntPtr_t ___y, MethodInfo* method);
#define DefaultComparer_Compare_m20868(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3816 *, IntPtr_t, IntPtr_t, MethodInfo*))DefaultComparer_Compare_m20868_gshared)(__this, ___x, ___y, method)
