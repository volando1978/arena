﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>
struct Enumerator_t3717;
// System.Object
struct Object_t;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct Dictionary_2_t542;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m19716(__this, ___dictionary, method) (( void (*) (Enumerator_t3717 *, Dictionary_2_t542 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19717(__this, method) (( Object_t * (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m19718(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m19719(__this, method) (( Object_t * (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m19720(__this, method) (( Object_t * (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::MoveNext()
#define Enumerator_MoveNext_m19721(__this, method) (( bool (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::get_Current()
#define Enumerator_get_Current_m19722(__this, method) (( KeyValuePair_2_t3714  (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m19723(__this, method) (( String_t* (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19724(__this, method) (( Achievement_t333 * (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::VerifyState()
#define Enumerator_VerifyState_m19725(__this, method) (( void (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m19726(__this, method) (( void (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>::Dispose()
#define Enumerator_Dispose_m19727(__this, method) (( void (*) (Enumerator_t3717 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
