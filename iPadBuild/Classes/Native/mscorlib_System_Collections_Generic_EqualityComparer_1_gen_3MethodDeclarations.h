﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.UInt32>
struct EqualityComparer_1_t3668;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt32>::.ctor()
extern "C" void EqualityComparer_1__ctor_m19040_gshared (EqualityComparer_1_t3668 * __this, MethodInfo* method);
#define EqualityComparer_1__ctor_m19040(__this, method) (( void (*) (EqualityComparer_1_t3668 *, MethodInfo*))EqualityComparer_1__ctor_m19040_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt32>::.cctor()
extern "C" void EqualityComparer_1__cctor_m19041_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1__cctor_m19041(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1__cctor_m19041_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19042_gshared (EqualityComparer_1_t3668 * __this, Object_t * ___obj, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19042(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3668 *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19042_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19043_gshared (EqualityComparer_1_t3668 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19043(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3668 *, Object_t *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19043_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt32>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt32>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt32>::get_Default()
extern "C" EqualityComparer_1_t3668 * EqualityComparer_1_get_Default_m19044_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1_get_Default_m19044(__this /* static, unused */, method) (( EqualityComparer_1_t3668 * (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1_get_Default_m19044_gshared)(__this /* static, unused */, method)
