﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// HUD/<runOut>c__Iterator2
struct U3CrunOutU3Ec__Iterator2_t735;
// System.Object
struct Object_t;

// System.Void HUD/<runOut>c__Iterator2::.ctor()
extern "C" void U3CrunOutU3Ec__Iterator2__ctor_m3184 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUD/<runOut>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CrunOutU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3185 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HUD/<runOut>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CrunOutU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3186 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HUD/<runOut>c__Iterator2::MoveNext()
extern "C" bool U3CrunOutU3Ec__Iterator2_MoveNext_m3187 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD/<runOut>c__Iterator2::Dispose()
extern "C" void U3CrunOutU3Ec__Iterator2_Dispose_m3188 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD/<runOut>c__Iterator2::Reset()
extern "C" void U3CrunOutU3Ec__Iterator2_Reset_m3189 (U3CrunOutU3Ec__Iterator2_t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
