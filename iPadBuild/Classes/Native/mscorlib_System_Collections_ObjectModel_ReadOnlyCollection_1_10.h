﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Soomla.Store.UpgradeVG>
struct IList_1_t3585;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.UpgradeVG>
struct  ReadOnlyCollection_1_t3586  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.UpgradeVG>::list
	Object_t* ___list_0;
};
