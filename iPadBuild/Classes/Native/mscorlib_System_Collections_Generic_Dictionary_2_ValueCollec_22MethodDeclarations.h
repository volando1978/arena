﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>
struct ValueCollection_t3663;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t4359;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.UInt32[]
struct UInt32U5BU5D_t2194;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_23.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m19003_gshared (ValueCollection_t3663 * __this, Dictionary_2_t3655 * ___dictionary, MethodInfo* method);
#define ValueCollection__ctor_m19003(__this, ___dictionary, method) (( void (*) (ValueCollection_t3663 *, Dictionary_2_t3655 *, MethodInfo*))ValueCollection__ctor_m19003_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004_gshared (ValueCollection_t3663 * __this, uint32_t ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004(__this, ___item, method) (( void (*) (ValueCollection_t3663 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m19004_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005(__this, method) (( void (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m19005_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006_gshared (ValueCollection_t3663 * __this, uint32_t ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006(__this, ___item, method) (( bool (*) (ValueCollection_t3663 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m19006_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007_gshared (ValueCollection_t3663 * __this, uint32_t ___item, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007(__this, ___item, method) (( bool (*) (ValueCollection_t3663 *, uint32_t, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m19007_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008(__this, method) (( Object_t* (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m19008_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m19009_gshared (ValueCollection_t3663 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m19009(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3663 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m19009_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010(__this, method) (( Object_t * (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m19010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011(__this, method) (( bool (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m19011_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012(__this, method) (( bool (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m19012_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013(__this, method) (( Object_t * (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m19013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m19014_gshared (ValueCollection_t3663 * __this, UInt32U5BU5D_t2194* ___array, int32_t ___index, MethodInfo* method);
#define ValueCollection_CopyTo_m19014(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3663 *, UInt32U5BU5D_t2194*, int32_t, MethodInfo*))ValueCollection_CopyTo_m19014_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::GetEnumerator()
extern "C" Enumerator_t3664  ValueCollection_GetEnumerator_m19015_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_GetEnumerator_m19015(__this, method) (( Enumerator_t3664  (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_GetEnumerator_m19015_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m19016_gshared (ValueCollection_t3663 * __this, MethodInfo* method);
#define ValueCollection_get_Count_m19016(__this, method) (( int32_t (*) (ValueCollection_t3663 *, MethodInfo*))ValueCollection_get_Count_m19016_gshared)(__this, method)
