﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Type>
struct  Comparison_1_t4128  : public MulticastDelegate_t22
{
};
