﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t621;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t617;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ConflictCallback__ctor_m3680 (ConflictCallback_t621 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::Invoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C" void ConflictCallback_Invoke_m3681 (ConflictCallback_t621 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method) IL2CPP_METHOD_ATTR;
#include "mscorlib_ArrayTypes.h"
// System.Byte
#include "mscorlib_System_Byte.h"
extern "C" void pinvoke_delegate_wrapper_ConflictCallback_t621(Il2CppObject* delegate, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData);
// System.IAsyncResult GooglePlayGames.BasicApi.SavedGame.ConflictCallback::BeginInvoke(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],System.AsyncCallback,System.Object)
extern "C" Object_t * ConflictCallback_BeginInvoke_m3682 (ConflictCallback_t621 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.SavedGame.ConflictCallback::EndInvoke(System.IAsyncResult)
extern "C" void ConflictCallback_EndInvoke_m3683 (ConflictCallback_t621 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
