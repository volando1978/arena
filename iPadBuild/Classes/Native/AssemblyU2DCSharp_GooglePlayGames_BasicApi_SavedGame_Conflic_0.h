﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t617;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct  ConflictCallback_t621  : public MulticastDelegate_t22
{
};
