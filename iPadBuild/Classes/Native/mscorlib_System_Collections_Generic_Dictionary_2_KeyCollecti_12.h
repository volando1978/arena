﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct Dictionary_2_t111;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>
struct  KeyCollection_t3608  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>::dictionary
	Dictionary_2_t111 * ___dictionary_0;
};
