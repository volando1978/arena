﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.Invitation
struct Invitation_t341;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>
struct  Action_2_t541  : public MulticastDelegate_t22
{
};
