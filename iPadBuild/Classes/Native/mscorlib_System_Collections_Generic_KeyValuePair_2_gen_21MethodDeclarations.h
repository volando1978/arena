﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct KeyValuePair_2_t3959;
// UnityEngine.Font
struct Font_t1222;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t1382;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22837(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, Font_t1222 *, List_1_t1382 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m22838(__this, method) (( Font_t1222 * (*) (KeyValuePair_2_t3959 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22839(__this, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, Font_t1222 *, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m22840(__this, method) (( List_1_t1382 * (*) (KeyValuePair_2_t3959 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22841(__this, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, List_1_t1382 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m22842(__this, method) (( String_t* (*) (KeyValuePair_2_t3959 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
