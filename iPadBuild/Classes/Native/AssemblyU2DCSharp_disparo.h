﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// disparo
struct  disparo_t784  : public MonoBehaviour_t26
{
	// System.Int32 disparo::shootTime
	int32_t ___shootTime_2;
	// System.Int32 disparo::shotTimeNormal
	int32_t ___shotTimeNormal_3;
	// System.Int32 disparo::shotTimewBock
	int32_t ___shotTimewBock_4;
	// System.Int32 disparo::shotTimeLaser
	int32_t ___shotTimeLaser_5;
	// System.Int32 disparo::shotTimeMoire
	int32_t ___shotTimeMoire_6;
	// System.Int32 disparo::shotTimeTWay
	int32_t ___shotTimeTWay_7;
	// System.Int32 disparo::shotTimeTri
	int32_t ___shotTimeTri_8;
	// System.Int32 disparo::shotTimeCircular
	int32_t ___shotTimeCircular_9;
	// System.Int32 disparo::largo
	int32_t ___largo_10;
	// System.Int32 disparo::tLaser
	int32_t ___tLaser_11;
	// System.Boolean disparo::isLaserState
	bool ___isLaserState_12;
	// System.Single disparo::shotForce
	float ___shotForce_13;
	// UnityEngine.GameObject disparo::rayo
	GameObject_t144 * ___rayo_14;
};
