﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B
struct U3CRematchU3Ec__AnonStorey5B_t650;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::.ctor()
extern "C" void U3CRematchU3Ec__AnonStorey5B__ctor_m2576 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::<>m__65(System.Boolean)
extern "C" void U3CRematchU3Ec__AnonStorey5B_U3CU3Em__65_m2577 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, bool ___failed, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::<>m__66(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CRematchU3Ec__AnonStorey5B_U3CU3Em__66_m2578 (U3CRematchU3Ec__AnonStorey5B_t650 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
