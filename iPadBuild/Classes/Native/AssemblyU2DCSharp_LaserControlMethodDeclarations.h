﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LaserControl
struct LaserControl_t745;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void LaserControl::.ctor()
extern "C" void LaserControl__ctor_m3235 (LaserControl_t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserControl::Update()
extern "C" void LaserControl_Update_m3236 (LaserControl_t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaserControl::showLaser()
extern "C" Object_t * LaserControl_showLaser_m3237 (LaserControl_t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserControl::resetLaser()
extern "C" void LaserControl_resetLaser_m3238 (LaserControl_t745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
