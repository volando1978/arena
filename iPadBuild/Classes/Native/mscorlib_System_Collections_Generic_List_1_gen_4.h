﻿#pragma once
#include <stdint.h>
// Soomla.Schedule/DateTimeRange[]
struct DateTimeRangeU5BU5D_t3509;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>
struct  List_1_t49  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::_items
	DateTimeRangeU5BU5D_t3509* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::_version
	int32_t ____version_3;
};
struct List_1_t49_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Schedule/DateTimeRange>::EmptyArray
	DateTimeRangeU5BU5D_t3509* ___EmptyArray_4;
};
