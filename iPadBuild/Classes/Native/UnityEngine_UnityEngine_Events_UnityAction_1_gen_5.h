﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t1368;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct  UnityAction_1_t1331  : public MulticastDelegate_t22
{
};
