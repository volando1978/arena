﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualItem[]
struct VirtualItemU5BU5D_t3564;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.VirtualItem>
struct  List_1_t179  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::_items
	VirtualItemU5BU5D_t3564* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::_version
	int32_t ____version_3;
};
struct List_1_t179_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualItem>::EmptyArray
	VirtualItemU5BU5D_t3564* ___EmptyArray_4;
};
