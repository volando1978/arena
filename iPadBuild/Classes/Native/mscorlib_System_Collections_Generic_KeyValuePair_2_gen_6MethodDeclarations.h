﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct KeyValuePair_2_t3545;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17192(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3545 *, String_t*, int32_t, MethodInfo*))KeyValuePair_2__ctor_m17099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m17193(__this, method) (( String_t* (*) (KeyValuePair_2_t3545 *, MethodInfo*))KeyValuePair_2_get_Key_m17100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17194(__this, ___value, method) (( void (*) (KeyValuePair_2_t3545 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m17101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m17195(__this, method) (( int32_t (*) (KeyValuePair_2_t3545 *, MethodInfo*))KeyValuePair_2_get_Value_m17102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17196(__this, ___value, method) (( void (*) (KeyValuePair_2_t3545 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m17103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m17197(__this, method) (( String_t* (*) (KeyValuePair_2_t3545 *, MethodInfo*))KeyValuePair_2_ToString_m17104_gshared)(__this, method)
