﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct List_1_t841;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>
struct  Enumerator_t3638 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::l
	List_1_t841 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::current
	Achievement_t333 * ___current_3;
};
