﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<System.Object,System.IntPtr>
struct Action_2_t3778;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_2__ctor_m20410_gshared (Action_2_t3778 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_2__ctor_m20410(__this, ___object, ___method, method) (( void (*) (Action_2_t3778 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m20410_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<System.Object,System.IntPtr>::Invoke(T1,T2)
extern "C" void Action_2_Invoke_m20411_gshared (Action_2_t3778 * __this, Object_t * ___arg1, IntPtr_t ___arg2, MethodInfo* method);
#define Action_2_Invoke_m20411(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t3778 *, Object_t *, IntPtr_t, MethodInfo*))Action_2_Invoke_m20411_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<System.Object,System.IntPtr>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_2_BeginInvoke_m20412_gshared (Action_2_t3778 * __this, Object_t * ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_2_BeginInvoke_m20412(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t3778 *, Object_t *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m20412_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C" void Action_2_EndInvoke_m20413_gshared (Action_2_t3778 * __this, Object_t * ___result, MethodInfo* method);
#define Action_2_EndInvoke_m20413(__this, ___result, method) (( void (*) (Action_2_t3778 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m20413_gshared)(__this, ___result, method)
