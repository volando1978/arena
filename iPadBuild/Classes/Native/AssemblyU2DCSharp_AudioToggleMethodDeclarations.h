﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioToggle
struct AudioToggle_t722;

// System.Void AudioToggle::.ctor()
extern "C" void AudioToggle__ctor_m3140 (AudioToggle_t722 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioToggle::Start()
extern "C" void AudioToggle_Start_m3141 (AudioToggle_t722 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioToggle::<Start>m__A2(System.Boolean)
extern "C" void AudioToggle_U3CStartU3Em__A2_m3142 (Object_t * __this /* static, unused */, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
