﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>
struct Enumerator_t217;
// System.Object
struct Object_t;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.Collections.Generic.List`1<Soomla.Store.VirtualGood>
struct List_1_t116;

// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m17810(__this, ___l, method) (( void (*) (Enumerator_t217 *, List_1_t116 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17811(__this, method) (( Object_t * (*) (Enumerator_t217 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::Dispose()
#define Enumerator_Dispose_m17812(__this, method) (( void (*) (Enumerator_t217 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::VerifyState()
#define Enumerator_VerifyState_m17813(__this, method) (( void (*) (Enumerator_t217 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::MoveNext()
#define Enumerator_MoveNext_m990(__this, method) (( bool (*) (Enumerator_t217 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.VirtualGood>::get_Current()
#define Enumerator_get_Current_m988(__this, method) (( VirtualGood_t79 * (*) (Enumerator_t217 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
