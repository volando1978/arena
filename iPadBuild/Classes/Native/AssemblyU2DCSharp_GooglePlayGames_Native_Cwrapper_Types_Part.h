﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
struct  ParticipantStatus_t513 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus::value__
	int32_t ___value___1;
};
