﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// culobichoScr
struct culobichoScr_t782;

// System.Void culobichoScr::.ctor()
extern "C" void culobichoScr__ctor_m3365 (culobichoScr_t782 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void culobichoScr::Start()
extern "C" void culobichoScr_Start_m3366 (culobichoScr_t782 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void culobichoScr::Update()
extern "C" void culobichoScr_Update_m3367 (culobichoScr_t782 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
