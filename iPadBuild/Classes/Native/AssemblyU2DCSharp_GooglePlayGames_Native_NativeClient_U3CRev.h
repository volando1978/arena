﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E
struct  U3CRevealAchievementU3Ec__AnonStorey1E_t527  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::achId
	String_t* ___achId_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::<>f__this
	NativeClient_t524 * ___U3CU3Ef__this_1;
};
