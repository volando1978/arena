﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ExitGUIException
struct ExitGUIException_t212;

// System.Void UnityEngine.ExitGUIException::.ctor()
extern "C" void ExitGUIException__ctor_m963 (ExitGUIException_t212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
