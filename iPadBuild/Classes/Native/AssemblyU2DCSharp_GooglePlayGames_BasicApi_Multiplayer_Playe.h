﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
struct  Player_t347  : public Object_t
{
	// System.String GooglePlayGames.BasicApi.Multiplayer.Player::mDisplayName
	String_t* ___mDisplayName_0;
	// System.String GooglePlayGames.BasicApi.Multiplayer.Player::mPlayerId
	String_t* ___mPlayerId_1;
	// System.String GooglePlayGames.BasicApi.Multiplayer.Player::mAvatarUrl
	String_t* ___mAvatarUrl_2;
};
