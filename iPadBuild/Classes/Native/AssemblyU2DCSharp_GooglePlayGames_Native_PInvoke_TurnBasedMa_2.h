﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback
struct  TurnBasedMatchCallback_t709  : public MulticastDelegate_t22
{
};
