﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState
struct  ShutdownState_t587  : public State_t565
{
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::mSession
	RoomSession_t566 * ___mSession_0;
};
