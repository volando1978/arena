﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t1420_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t1427_il2cpp_TypeInfo_var;
void g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2342);
		AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2343);
		AssemblyConfigurationAttribute_t1420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2344);
		AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2345);
		AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2346);
		AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2347);
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2348);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AssemblyTrademarkAttribute_t1427_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2351);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 11;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyTitleAttribute_t1418 * tmp;
		tmp = (AssemblyTitleAttribute_t1418 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m6174(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t1419 * tmp;
		tmp = (AssemblyDescriptionAttribute_t1419 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m6175(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t1420 * tmp;
		tmp = (AssemblyConfigurationAttribute_t1420 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t1420_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m6176(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t1421 * tmp;
		tmp = (AssemblyCompanyAttribute_t1421 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m6177(tmp, il2cpp_codegen_string_new_wrapper("Microsoft"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t1422 * tmp;
		tmp = (AssemblyProductAttribute_t1422 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m6178(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t1423 * tmp;
		tmp = (AssemblyCopyrightAttribute_t1423 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m6179(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Microsoft 2013"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t1424 * tmp;
		tmp = (AssemblyFileVersionAttribute_t1424 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m6180(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("d4f464c7-9b15-460d-b4bc-2cacd1c1df73"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t1427 * tmp;
		tmp = (AssemblyTrademarkAttribute_t1427 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t1427_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m6183(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void EventHandle_t1150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Event System"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_m_FirstSelected(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Selected"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_m_sendNavigationEvents(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_m_DragThreshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_EventSystem_get_current_m4556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void EventSystem_t1155_CustomAttributesCacheGenerator_EventSystem_set_current_m4557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void EventTrigger_t1161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Event Trigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ExecuteEvents_t1182_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ExecuteEvents_t1182_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m4639(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m4663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m4664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m4665(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m4666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m4675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m4676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m4677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m4678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m4679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m4680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m4681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m4682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m4683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m4684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m4685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m4686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m4687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m4688(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m4689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m4690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_position_m4691(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_position_m4692(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_delta_m4693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_delta_m4694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m4695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m4696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m4697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m4698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m4699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m4700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m4701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m4702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m4703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m4704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m4705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m4706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m4707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m4708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m4709(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m4710(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_button_m4711(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_button_m4712(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
extern const Il2CppType* EventSystem_t1155_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void BaseInputModule_t1152_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventSystem_t1155_0_0_0_var = il2cpp_codegen_type_from_index(2150);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(EventSystem_t1155_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Standalone Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_HorizontalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_VerticalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_SubmitButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_CancelButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_InputActionsPerSecond(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void StandaloneInputModule_t1200_CustomAttributesCacheGenerator_StandaloneInputModule_t1200____inputMode_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void InputMode_t1199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void TouchInputModule_t1201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Touch Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void TouchInputModule_t1201_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void BaseRaycaster_t1186_CustomAttributesCacheGenerator_BaseRaycaster_t1186____priority_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("Please use sortOrderPriority and renderOrderPriority"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t978_0_0_0_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void Physics2DRaycaster_t1202_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t978_0_0_0_var = il2cpp_codegen_type_from_index(1015);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics 2D Raycaster"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(Camera_t978_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t978_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t1203_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t978_0_0_0_var = il2cpp_codegen_type_from_index(1015);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(Camera_t978_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_m_EventMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m4821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void TweenRunner_1_t1434_CustomAttributesCacheGenerator_TweenRunner_1_Start_m6224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m6228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6229(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m6231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m6232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_NormalTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("normalTrigger"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_HighlightedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("highlightedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedTrigger"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_PressedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("pressedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_DisabledTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("disabledTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Button_t1213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Button"), 30, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Button_t1213_CustomAttributesCacheGenerator_m_OnClick(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("onClick"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Button_t1213_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m4860(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4849(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m4852(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m4853(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m4877(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m4878(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_NormalColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("normalColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_HighlightedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("highlightedColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedColor"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_PressedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("pressedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_DisabledColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("disabledColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_ColorMultiplier(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 1.0f, 5.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void ColorBlock_t1221_CustomAttributesCacheGenerator_m_FadeDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("fadeDuration"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("font"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_FontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("fontSize"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_FontStyle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("fontStyle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_BestFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_MinSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_MaxSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_Alignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("alignment"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_RichText(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("richText"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_HorizontalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_VerticalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void FontData_t1223_CustomAttributesCacheGenerator_m_LineSpacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern const Il2CppType* CanvasRenderer_t1228_0_0_0_var;
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasRenderer_t1228_0_0_0_var = il2cpp_codegen_type_from_index(2240);
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2356);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_t1228_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t1437 * tmp;
		tmp = (DisallowMultipleComponent_t1437 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m6238(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_m_Material(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Mat"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m4966(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Graphic_t1233_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m4967(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern const Il2CppType* Canvas_t1229_0_0_0_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t1229_0_0_0_var = il2cpp_codegen_type_from_index(2238);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6201(tmp, il2cpp_codegen_string_new_wrapper("Event/Graphic Raycaster"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(Canvas_t1229_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("ignoreReversedGraphics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_BlockingObjects(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("blockingObjects"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_BlockingMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void GraphicRaycaster_t1237_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m4982(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Image"), 10, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Frame"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_PreserveAspect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_FillCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_FillMethod(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_FillAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_FillClockwise(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Image_t1248_CustomAttributesCacheGenerator_m_FillOrigin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Input Field"), 31, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_TextComponent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("text"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_Placeholder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_ContentType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_InputType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("inputType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_AsteriskChar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("asteriskChar"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_KeyboardType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("keyboardType"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_LineType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_HideMobileInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("hideMobileInput"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_CharacterValidation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("validation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_CharacterLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("characterLimit"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_EndEdit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_OnSubmit"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("onSubmit"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_OnValueChange(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("onValueChange"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_OnValidateInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("onValidateInput"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("selectionColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("mValue"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_m_CaretBlinkRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 8.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_InputField_CaretBlink_m5102(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m5119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void InputField_t1259_CustomAttributesCacheGenerator_InputField_t1259_InputField_SetToCustomIfContentTypeIsNot_m5170_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m5046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m5048(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m5049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m5052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m5054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m5055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Navigation_t1272_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnUp(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("selectOnUp"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnDown(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("selectOnDown"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnLeft(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("selectOnLeft"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnRight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("selectOnRight"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void Mode_t1271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void RawImage_t1273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Raw Image"), 12, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void RawImage_t1273_CustomAttributesCacheGenerator_m_Texture(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Tex"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void RawImage_t1273_CustomAttributesCacheGenerator_m_UVRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Scrollbar"), 32, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_Size(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_NumberOfSteps(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 11.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern TypeInfo* SpaceAttribute_t1440_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t1440_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2359);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t1440 * tmp;
		tmp = (SpaceAttribute_t1440 *)il2cpp_codegen_object_new (SpaceAttribute_t1440_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m6244(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void Scrollbar_t1278_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m5245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5211(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m5212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m5214(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m5215(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2360);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t1441 * tmp;
		tmp = (SelectionBaseAttribute_t1441 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m6245(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Scroll Rect"), 33, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_Horizontal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_Vertical(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_MovementType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_Elasticity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_Inertia(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_DecelerationRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_ScrollSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_HorizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_VerticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ScrollRect_t1285_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2356);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2360);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DisallowMultipleComponent_t1437 * tmp;
		tmp = (DisallowMultipleComponent_t1437 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t1437_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m6238(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t1441 * tmp;
		tmp = (SelectionBaseAttribute_t1441 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t1441_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m6245(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Selectable"), 70, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_Navigation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("navigation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_Transition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("transition"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("colors"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_SpriteState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("spriteState"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_AnimationTriggers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("animationTriggers"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_Interactable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("Can the Selectable be interacted with?"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_m_TargetGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_HighlightGraphic"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("highlightGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m5330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m5331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m5332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m5333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m5334(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m5335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Selectable_t1215_CustomAttributesCacheGenerator_Selectable_IsPressed_m5361(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m6216(tmp, il2cpp_codegen_string_new_wrapper("Is Pressed no longer requires eventData"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Slider"), 34, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_FillRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* SpaceAttribute_t1440_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		SpaceAttribute_t1440_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2359);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SpaceAttribute_t1440 * tmp;
		tmp = (SpaceAttribute_t1440 *)il2cpp_codegen_object_new (SpaceAttribute_t1440_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m6244(tmp, 6.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_WholeNumbers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SpaceAttribute_t1440_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Slider_t1295_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t1440_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2359);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t1440 * tmp;
		tmp = (SpaceAttribute_t1440 *)il2cpp_codegen_object_new (SpaceAttribute_t1440_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m6244(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void SpriteState_t1290_CustomAttributesCacheGenerator_m_HighlightedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("highlightedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void SpriteState_t1290_CustomAttributesCacheGenerator_m_PressedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("pressedSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void SpriteState_t1290_CustomAttributesCacheGenerator_m_DisabledSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("disabledSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Text_t1263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Text"), 11, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Text_t1263_CustomAttributesCacheGenerator_m_FontData(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern TypeInfo* TextAreaAttribute_t1443_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Text_t1263_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextAreaAttribute_t1443_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2362);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TextAreaAttribute_t1443 * tmp;
		tmp = (TextAreaAttribute_t1443 *)il2cpp_codegen_object_new (TextAreaAttribute_t1443_il2cpp_TypeInfo_var);
		TextAreaAttribute__ctor_m6249(tmp, 3, 10, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void Toggle_t720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle"), 35, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Toggle_t720_CustomAttributesCacheGenerator_m_Group(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Toggle_t720_CustomAttributesCacheGenerator_m_IsOn(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("Is the toggle currently on or off?"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_IsActive"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle Group"), 36, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator_m_AllowSwitchOff(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ToggleGroup_t1301_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m5504(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void AspectRatioFitter_t1306_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Aspect Ratio Fitter"), 142, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void AspectRatioFitter_t1306_CustomAttributesCacheGenerator_m_AspectMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void AspectRatioFitter_t1306_CustomAttributesCacheGenerator_m_AspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Canvas_t1229_0_0_0_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t1229_0_0_0_var = il2cpp_codegen_type_from_index(2238);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(Canvas_t1229_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Canvas Scaler"), 101, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_UiScaleMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("Determines how UI elements in the Canvas are scaled."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("Scales all UI elements in the Canvas by this factor."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ReferenceResolution(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ScreenMatchMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t1436_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_MatchWidthOrHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		RangeAttribute_t1436_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2355);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("Determines if the scaling is using the width or height as reference, or a mix in between."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t1436 * tmp;
		tmp = (RangeAttribute_t1436 *)il2cpp_codegen_object_new (RangeAttribute_t1436_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m6237(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_PhysicalUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("The physical unit to specify positions and sizes in."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_FallbackScreenDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("The DPI to assume if the screen DPI is not known."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_DefaultSpriteDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t1442_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void CanvasScaler_t1310_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t1442_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2361);
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t1442 * tmp;
		tmp = (TooltipAttribute_t1442 *)il2cpp_codegen_object_new (TooltipAttribute_t1442_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m6246(tmp, il2cpp_codegen_string_new_wrapper("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void ContentSizeFitter_t1312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Content Size Fitter"), 141, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ContentSizeFitter_t1312_CustomAttributesCacheGenerator_m_HorizontalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void ContentSizeFitter_t1312_CustomAttributesCacheGenerator_m_VerticalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Grid Layout Group"), 152, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void HorizontalLayoutGroup_t1318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Horizontal Layout Group"), 150, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_ChildForceExpandWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_ChildForceExpandHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Layout Element"), 140, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_IgnoreLayout(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_MinWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_PreferredWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_PreferredHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_FlexibleWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutElement_t1320_CustomAttributesCacheGenerator_m_FlexibleHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t1227_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t1432_il2cpp_TypeInfo_var;
void LayoutGroup_t1317_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t1227_0_0_0_var = il2cpp_codegen_type_from_index(2237);
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		RequireComponent_t1432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2354);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t1432 * tmp;
		tmp = (RequireComponent_t1432 *)il2cpp_codegen_object_new (RequireComponent_t1432_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m6214(tmp, il2cpp_codegen_type_get_object(RectTransform_t1227_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void LayoutGroup_t1317_CustomAttributesCacheGenerator_m_Padding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void LayoutGroup_t1317_CustomAttributesCacheGenerator_m_ChildAlignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_Alignment"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m5664(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m5665(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m5666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m5667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m5680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m5684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void VerticalLayoutGroup_t1328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("Layout/Vertical Layout Group"), 151, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Mask_t1329_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Mask"), 13, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var;
void Mask_t1329_CustomAttributesCacheGenerator_m_ShowMaskGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2353);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t1430 * tmp;
		tmp = (FormerlySerializedAsAttribute_t1430 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t1430_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m6202(tmp, il2cpp_codegen_string_new_wrapper("m_ShowGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void IndexedSet_1_t1444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasListPool_t1332_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CanvasListPool_t1332_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ComponentListPool_t1335_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ComponentListPool_t1335_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectPool_1_t1445_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectPool_1_t1445_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m6285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectPool_1_t1445_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m6286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t1438_il2cpp_TypeInfo_var;
void BaseVertexEffect_t1336_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t1438_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2357);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t1438 * tmp;
		tmp = (ExecuteInEditMode_t1438 *)il2cpp_codegen_object_new (ExecuteInEditMode_t1438_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m6239(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Outline_t1337_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Outline"), 15, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void PositionAsUV1_t1339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Position As UV1"), 16, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t1429_il2cpp_TypeInfo_var;
void Shadow_t1338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t1429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2352);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t1429 * tmp;
		tmp = (AddComponentMenu_t1429 *)il2cpp_codegen_object_new (AddComponentMenu_t1429_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m6233(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Shadow"), 14, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Shadow_t1338_CustomAttributesCacheGenerator_m_EffectColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Shadow_t1338_CustomAttributesCacheGenerator_m_EffectDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void Shadow_t1338_CustomAttributesCacheGenerator_m_UseGraphicAlpha(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_UI_Assembly_AttributeGenerators[344] = 
{
	NULL,
	g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator,
	EventHandle_t1150_CustomAttributesCacheGenerator,
	EventSystem_t1155_CustomAttributesCacheGenerator,
	EventSystem_t1155_CustomAttributesCacheGenerator_m_FirstSelected,
	EventSystem_t1155_CustomAttributesCacheGenerator_m_sendNavigationEvents,
	EventSystem_t1155_CustomAttributesCacheGenerator_m_DragThreshold,
	EventSystem_t1155_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField,
	EventSystem_t1155_CustomAttributesCacheGenerator_EventSystem_get_current_m4556,
	EventSystem_t1155_CustomAttributesCacheGenerator_EventSystem_set_current_m4557,
	EventTrigger_t1161_CustomAttributesCacheGenerator,
	ExecuteEvents_t1182_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13,
	ExecuteEvents_t1182_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m4639,
	AxisEventData_t1188_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField,
	AxisEventData_t1188_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField,
	AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m4663,
	AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m4664,
	AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m4665,
	AxisEventData_t1188_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m4666,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m4675,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m4676,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m4677,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m4678,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m4679,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m4680,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m4681,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m4682,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m4683,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m4684,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m4685,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m4686,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m4687,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m4688,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m4689,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m4690,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_position_m4691,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_position_m4692,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_delta_m4693,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_delta_m4694,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m4695,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m4696,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m4697,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m4698,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m4699,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m4700,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m4701,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m4702,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m4703,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m4704,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m4705,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m4706,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m4707,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m4708,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m4709,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m4710,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_get_button_m4711,
	PointerEventData_t1191_CustomAttributesCacheGenerator_PointerEventData_set_button_m4712,
	BaseInputModule_t1152_CustomAttributesCacheGenerator,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_HorizontalAxis,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_VerticalAxis,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_SubmitButton,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_CancelButton,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_InputActionsPerSecond,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice,
	StandaloneInputModule_t1200_CustomAttributesCacheGenerator_StandaloneInputModule_t1200____inputMode_PropertyInfo,
	InputMode_t1199_CustomAttributesCacheGenerator,
	TouchInputModule_t1201_CustomAttributesCacheGenerator,
	TouchInputModule_t1201_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone,
	BaseRaycaster_t1186_CustomAttributesCacheGenerator_BaseRaycaster_t1186____priority_PropertyInfo,
	Physics2DRaycaster_t1202_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t1203_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_m_EventMask,
	PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	PhysicsRaycaster_t1203_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m4821,
	TweenRunner_1_t1434_CustomAttributesCacheGenerator_TweenRunner_1_Start_m6224,
	U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m6228,
	U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m6229,
	U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m6231,
	U3CStartU3Ec__Iterator0_t1435_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m6232,
	AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_NormalTrigger,
	AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_HighlightedTrigger,
	AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_PressedTrigger,
	AnimationTriggers_t1210_CustomAttributesCacheGenerator_m_DisabledTrigger,
	Button_t1213_CustomAttributesCacheGenerator,
	Button_t1213_CustomAttributesCacheGenerator_m_OnClick,
	Button_t1213_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m4860,
	U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator,
	U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4849,
	U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4850,
	U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m4852,
	U3COnFinishSubmitU3Ec__Iterator1_t1214_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m4853,
	CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m4877,
	CanvasUpdateRegistry_t1217_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m4878,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_NormalColor,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_HighlightedColor,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_PressedColor,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_DisabledColor,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_ColorMultiplier,
	ColorBlock_t1221_CustomAttributesCacheGenerator_m_FadeDuration,
	FontData_t1223_CustomAttributesCacheGenerator_m_Font,
	FontData_t1223_CustomAttributesCacheGenerator_m_FontSize,
	FontData_t1223_CustomAttributesCacheGenerator_m_FontStyle,
	FontData_t1223_CustomAttributesCacheGenerator_m_BestFit,
	FontData_t1223_CustomAttributesCacheGenerator_m_MinSize,
	FontData_t1223_CustomAttributesCacheGenerator_m_MaxSize,
	FontData_t1223_CustomAttributesCacheGenerator_m_Alignment,
	FontData_t1223_CustomAttributesCacheGenerator_m_RichText,
	FontData_t1223_CustomAttributesCacheGenerator_m_HorizontalOverflow,
	FontData_t1223_CustomAttributesCacheGenerator_m_VerticalOverflow,
	FontData_t1223_CustomAttributesCacheGenerator_m_LineSpacing,
	Graphic_t1233_CustomAttributesCacheGenerator,
	Graphic_t1233_CustomAttributesCacheGenerator_m_Material,
	Graphic_t1233_CustomAttributesCacheGenerator_m_Color,
	Graphic_t1233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE,
	Graphic_t1233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF,
	Graphic_t1233_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m4966,
	Graphic_t1233_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m4967,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_BlockingObjects,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator_m_BlockingMask,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	GraphicRaycaster_t1237_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m4982,
	Image_t1248_CustomAttributesCacheGenerator,
	Image_t1248_CustomAttributesCacheGenerator_m_Sprite,
	Image_t1248_CustomAttributesCacheGenerator_m_Type,
	Image_t1248_CustomAttributesCacheGenerator_m_PreserveAspect,
	Image_t1248_CustomAttributesCacheGenerator_m_FillCenter,
	Image_t1248_CustomAttributesCacheGenerator_m_FillMethod,
	Image_t1248_CustomAttributesCacheGenerator_m_FillAmount,
	Image_t1248_CustomAttributesCacheGenerator_m_FillClockwise,
	Image_t1248_CustomAttributesCacheGenerator_m_FillOrigin,
	InputField_t1259_CustomAttributesCacheGenerator,
	InputField_t1259_CustomAttributesCacheGenerator_m_TextComponent,
	InputField_t1259_CustomAttributesCacheGenerator_m_Placeholder,
	InputField_t1259_CustomAttributesCacheGenerator_m_ContentType,
	InputField_t1259_CustomAttributesCacheGenerator_m_InputType,
	InputField_t1259_CustomAttributesCacheGenerator_m_AsteriskChar,
	InputField_t1259_CustomAttributesCacheGenerator_m_KeyboardType,
	InputField_t1259_CustomAttributesCacheGenerator_m_LineType,
	InputField_t1259_CustomAttributesCacheGenerator_m_HideMobileInput,
	InputField_t1259_CustomAttributesCacheGenerator_m_CharacterValidation,
	InputField_t1259_CustomAttributesCacheGenerator_m_CharacterLimit,
	InputField_t1259_CustomAttributesCacheGenerator_m_EndEdit,
	InputField_t1259_CustomAttributesCacheGenerator_m_OnValueChange,
	InputField_t1259_CustomAttributesCacheGenerator_m_OnValidateInput,
	InputField_t1259_CustomAttributesCacheGenerator_m_SelectionColor,
	InputField_t1259_CustomAttributesCacheGenerator_m_Text,
	InputField_t1259_CustomAttributesCacheGenerator_m_CaretBlinkRate,
	InputField_t1259_CustomAttributesCacheGenerator_InputField_CaretBlink_m5102,
	InputField_t1259_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m5119,
	InputField_t1259_CustomAttributesCacheGenerator_InputField_t1259_InputField_SetToCustomIfContentTypeIsNot_m5170_Arg0_ParameterInfo,
	U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator,
	U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5045,
	U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m5046,
	U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m5048,
	U3CCaretBlinkU3Ec__Iterator2_t1260_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m5049,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5051,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m5052,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m5054,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t1261_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m5055,
	Navigation_t1272_CustomAttributesCacheGenerator_m_Mode,
	Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnUp,
	Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnDown,
	Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnLeft,
	Navigation_t1272_CustomAttributesCacheGenerator_m_SelectOnRight,
	Mode_t1271_CustomAttributesCacheGenerator,
	RawImage_t1273_CustomAttributesCacheGenerator,
	RawImage_t1273_CustomAttributesCacheGenerator_m_Texture,
	RawImage_t1273_CustomAttributesCacheGenerator_m_UVRect,
	Scrollbar_t1278_CustomAttributesCacheGenerator,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_HandleRect,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_Direction,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_Value,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_Size,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_NumberOfSteps,
	Scrollbar_t1278_CustomAttributesCacheGenerator_m_OnValueChanged,
	Scrollbar_t1278_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m5245,
	U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator,
	U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m5211,
	U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m5212,
	U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m5214,
	U3CClickRepeatU3Ec__Iterator4_t1279_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m5215,
	ScrollRect_t1285_CustomAttributesCacheGenerator,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_Content,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_Horizontal,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_Vertical,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_MovementType,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_Elasticity,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_Inertia,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_DecelerationRate,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_ScrollSensitivity,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_HorizontalScrollbar,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_VerticalScrollbar,
	ScrollRect_t1285_CustomAttributesCacheGenerator_m_OnValueChanged,
	Selectable_t1215_CustomAttributesCacheGenerator,
	Selectable_t1215_CustomAttributesCacheGenerator_m_Navigation,
	Selectable_t1215_CustomAttributesCacheGenerator_m_Transition,
	Selectable_t1215_CustomAttributesCacheGenerator_m_Colors,
	Selectable_t1215_CustomAttributesCacheGenerator_m_SpriteState,
	Selectable_t1215_CustomAttributesCacheGenerator_m_AnimationTriggers,
	Selectable_t1215_CustomAttributesCacheGenerator_m_Interactable,
	Selectable_t1215_CustomAttributesCacheGenerator_m_TargetGraphic,
	Selectable_t1215_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField,
	Selectable_t1215_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField,
	Selectable_t1215_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m5330,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m5331,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m5332,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m5333,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m5334,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m5335,
	Selectable_t1215_CustomAttributesCacheGenerator_Selectable_IsPressed_m5361,
	Slider_t1295_CustomAttributesCacheGenerator,
	Slider_t1295_CustomAttributesCacheGenerator_m_FillRect,
	Slider_t1295_CustomAttributesCacheGenerator_m_HandleRect,
	Slider_t1295_CustomAttributesCacheGenerator_m_Direction,
	Slider_t1295_CustomAttributesCacheGenerator_m_MinValue,
	Slider_t1295_CustomAttributesCacheGenerator_m_MaxValue,
	Slider_t1295_CustomAttributesCacheGenerator_m_WholeNumbers,
	Slider_t1295_CustomAttributesCacheGenerator_m_Value,
	Slider_t1295_CustomAttributesCacheGenerator_m_OnValueChanged,
	SpriteState_t1290_CustomAttributesCacheGenerator_m_HighlightedSprite,
	SpriteState_t1290_CustomAttributesCacheGenerator_m_PressedSprite,
	SpriteState_t1290_CustomAttributesCacheGenerator_m_DisabledSprite,
	Text_t1263_CustomAttributesCacheGenerator,
	Text_t1263_CustomAttributesCacheGenerator_m_FontData,
	Text_t1263_CustomAttributesCacheGenerator_m_Text,
	Toggle_t720_CustomAttributesCacheGenerator,
	Toggle_t720_CustomAttributesCacheGenerator_m_Group,
	Toggle_t720_CustomAttributesCacheGenerator_m_IsOn,
	ToggleGroup_t1301_CustomAttributesCacheGenerator,
	ToggleGroup_t1301_CustomAttributesCacheGenerator_m_AllowSwitchOff,
	ToggleGroup_t1301_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ToggleGroup_t1301_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	ToggleGroup_t1301_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m5503,
	ToggleGroup_t1301_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m5504,
	AspectRatioFitter_t1306_CustomAttributesCacheGenerator,
	AspectRatioFitter_t1306_CustomAttributesCacheGenerator_m_AspectMode,
	AspectRatioFitter_t1306_CustomAttributesCacheGenerator_m_AspectRatio,
	CanvasScaler_t1310_CustomAttributesCacheGenerator,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_UiScaleMode,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ScaleFactor,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ReferenceResolution,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_ScreenMatchMode,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_MatchWidthOrHeight,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_PhysicalUnit,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_FallbackScreenDPI,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_DefaultSpriteDPI,
	CanvasScaler_t1310_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit,
	ContentSizeFitter_t1312_CustomAttributesCacheGenerator,
	ContentSizeFitter_t1312_CustomAttributesCacheGenerator_m_HorizontalFit,
	ContentSizeFitter_t1312_CustomAttributesCacheGenerator_m_VerticalFit,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_StartCorner,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_StartAxis,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_CellSize,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_Spacing,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_Constraint,
	GridLayoutGroup_t1316_CustomAttributesCacheGenerator_m_ConstraintCount,
	HorizontalLayoutGroup_t1318_CustomAttributesCacheGenerator,
	HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_Spacing,
	HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_ChildForceExpandWidth,
	HorizontalOrVerticalLayoutGroup_t1319_CustomAttributesCacheGenerator_m_ChildForceExpandHeight,
	LayoutElement_t1320_CustomAttributesCacheGenerator,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_IgnoreLayout,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_MinWidth,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_MinHeight,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_PreferredWidth,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_PreferredHeight,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_FlexibleWidth,
	LayoutElement_t1320_CustomAttributesCacheGenerator_m_FlexibleHeight,
	LayoutGroup_t1317_CustomAttributesCacheGenerator,
	LayoutGroup_t1317_CustomAttributesCacheGenerator_m_Padding,
	LayoutGroup_t1317_CustomAttributesCacheGenerator_m_ChildAlignment,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m5664,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m5665,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m5666,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m5667,
	LayoutRebuilder_t1325_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m5668,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m5680,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m5681,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m5682,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m5683,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m5684,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m5685,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m5686,
	LayoutUtility_t1327_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m5687,
	VerticalLayoutGroup_t1328_CustomAttributesCacheGenerator,
	Mask_t1329_CustomAttributesCacheGenerator,
	Mask_t1329_CustomAttributesCacheGenerator_m_ShowMaskGraphic,
	IndexedSet_1_t1444_CustomAttributesCacheGenerator,
	CanvasListPool_t1332_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	CanvasListPool_t1332_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m5709,
	ComponentListPool_t1335_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ComponentListPool_t1335_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m5713,
	ObjectPool_1_t1445_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField,
	ObjectPool_1_t1445_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m6285,
	ObjectPool_1_t1445_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m6286,
	BaseVertexEffect_t1336_CustomAttributesCacheGenerator,
	Outline_t1337_CustomAttributesCacheGenerator,
	PositionAsUV1_t1339_CustomAttributesCacheGenerator,
	Shadow_t1338_CustomAttributesCacheGenerator,
	Shadow_t1338_CustomAttributesCacheGenerator_m_EffectColor,
	Shadow_t1338_CustomAttributesCacheGenerator_m_EffectDistance,
	Shadow_t1338_CustomAttributesCacheGenerator_m_UseGraphicAlpha,
};
