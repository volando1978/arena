﻿#pragma once
#include <stdint.h>
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t530;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22
struct  U3CShowLeaderboardUIU3Ec__AnonStorey22_t532  : public Object_t
{
	// System.Action`1<GooglePlayGames.BasicApi.UIStatus> GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22::cb
	Action_1_t530 * ___cb_0;
};
