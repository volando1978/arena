﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// movingTextFeedback
struct movingTextFeedback_t816;

// System.Void movingTextFeedback::.ctor()
extern "C" void movingTextFeedback__ctor_m3573 (movingTextFeedback_t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextFeedback::Start()
extern "C" void movingTextFeedback_Start_m3574 (movingTextFeedback_t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextFeedback::Update()
extern "C" void movingTextFeedback_Update_m3575 (movingTextFeedback_t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void movingTextFeedback::OnGUI()
extern "C" void movingTextFeedback_OnGUI_m3576 (movingTextFeedback_t816 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
