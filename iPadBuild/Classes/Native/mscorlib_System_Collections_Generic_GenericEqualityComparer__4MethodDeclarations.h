﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t3543;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m17182_gshared (GenericEqualityComparer_1_t3543 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m17182(__this, method) (( void (*) (GenericEqualityComparer_1_t3543 *, MethodInfo*))GenericEqualityComparer_1__ctor_m17182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m17183_gshared (GenericEqualityComparer_1_t3543 * __this, int32_t ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m17183(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3543 *, int32_t, MethodInfo*))GenericEqualityComparer_1_GetHashCode_m17183_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m17184_gshared (GenericEqualityComparer_1_t3543 * __this, int32_t ___x, int32_t ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m17184(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3543 *, int32_t, int32_t, MethodInfo*))GenericEqualityComparer_1_Equals_m17184_gshared)(__this, ___x, ___y, method)
