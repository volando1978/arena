﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNodeCollection
struct DictionaryNodeCollection_t1970;
// System.Object
struct Object_t;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1964;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::.ctor(System.Collections.Specialized.ListDictionary,System.Boolean)
extern "C" void DictionaryNodeCollection__ctor_m7884 (DictionaryNodeCollection_t1970 * __this, ListDictionary_t1964 * ___dict, bool ___isKeyList, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_Count()
extern "C" int32_t DictionaryNodeCollection_get_Count_m7885 (DictionaryNodeCollection_t1970 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_IsSynchronized()
extern "C" bool DictionaryNodeCollection_get_IsSynchronized_m7886 (DictionaryNodeCollection_t1970 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::get_SyncRoot()
extern "C" Object_t * DictionaryNodeCollection_get_SyncRoot_m7887 (DictionaryNodeCollection_t1970 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::CopyTo(System.Array,System.Int32)
extern "C" void DictionaryNodeCollection_CopyTo_m7888 (DictionaryNodeCollection_t1970 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.ListDictionary/DictionaryNodeCollection::GetEnumerator()
extern "C" Object_t * DictionaryNodeCollection_GetEnumerator_m7889 (DictionaryNodeCollection_t1970 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
