﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct List_1_t841;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.BasicApi.Achievement>
struct IEnumerable_1_t4348;
// System.Collections.Generic.IEnumerator`1<GooglePlayGames.BasicApi.Achievement>
struct IEnumerator_1_t4349;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<GooglePlayGames.BasicApi.Achievement>
struct ICollection_1_t4350;
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Achievement>
struct ReadOnlyCollection_1_t3637;
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3635;
// System.Predicate`1<GooglePlayGames.BasicApi.Achievement>
struct Predicate_1_t543;
// System.Action`1<GooglePlayGames.BasicApi.Achievement>
struct Action_1_t860;
// System.Comparison`1<GooglePlayGames.BasicApi.Achievement>
struct Comparison_1_t3639;
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"

// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3690(__this, method) (( void (*) (List_1_t841 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18546(__this, ___collection, method) (( void (*) (List_1_t841 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::.ctor(System.Int32)
#define List_1__ctor_m18547(__this, ___capacity, method) (( void (*) (List_1_t841 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::.cctor()
#define List_1__cctor_m18548(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18549(__this, method) (( Object_t* (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18550(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t841 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18551(__this, method) (( Object_t * (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18552(__this, ___item, method) (( int32_t (*) (List_1_t841 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18553(__this, ___item, method) (( bool (*) (List_1_t841 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18554(__this, ___item, method) (( int32_t (*) (List_1_t841 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18555(__this, ___index, ___item, method) (( void (*) (List_1_t841 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18556(__this, ___item, method) (( void (*) (List_1_t841 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18557(__this, method) (( bool (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18558(__this, method) (( bool (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18559(__this, method) (( Object_t * (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18560(__this, method) (( bool (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18561(__this, method) (( bool (*) (List_1_t841 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18562(__this, ___index, method) (( Object_t * (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18563(__this, ___index, ___value, method) (( void (*) (List_1_t841 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Add(T)
#define List_1_Add_m18564(__this, ___item, method) (( void (*) (List_1_t841 *, Achievement_t333 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18565(__this, ___newCount, method) (( void (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18566(__this, ___collection, method) (( void (*) (List_1_t841 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18567(__this, ___enumerable, method) (( void (*) (List_1_t841 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18568(__this, ___collection, method) (( void (*) (List_1_t841 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::AsReadOnly()
#define List_1_AsReadOnly_m18569(__this, method) (( ReadOnlyCollection_1_t3637 * (*) (List_1_t841 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Clear()
#define List_1_Clear_m18570(__this, method) (( void (*) (List_1_t841 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Contains(T)
#define List_1_Contains_m18571(__this, ___item, method) (( bool (*) (List_1_t841 *, Achievement_t333 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18572(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t841 *, AchievementU5BU5D_t3635*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Find(System.Predicate`1<T>)
#define List_1_Find_m18573(__this, ___match, method) (( Achievement_t333 * (*) (List_1_t841 *, Predicate_1_t543 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18574(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t543 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m18575(__this, ___match, method) (( int32_t (*) (List_1_t841 *, Predicate_1_t543 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18576(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t841 *, int32_t, int32_t, Predicate_1_t543 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m18577(__this, ___action, method) (( void (*) (List_1_t841 *, Action_1_t860 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::GetEnumerator()
#define List_1_GetEnumerator_m18578(__this, method) (( Enumerator_t3638  (*) (List_1_t841 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::IndexOf(T)
#define List_1_IndexOf_m18579(__this, ___item, method) (( int32_t (*) (List_1_t841 *, Achievement_t333 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18580(__this, ___start, ___delta, method) (( void (*) (List_1_t841 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18581(__this, ___index, method) (( void (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Insert(System.Int32,T)
#define List_1_Insert_m18582(__this, ___index, ___item, method) (( void (*) (List_1_t841 *, int32_t, Achievement_t333 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18583(__this, ___collection, method) (( void (*) (List_1_t841 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Remove(T)
#define List_1_Remove_m18584(__this, ___item, method) (( bool (*) (List_1_t841 *, Achievement_t333 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18585(__this, ___match, method) (( int32_t (*) (List_1_t841 *, Predicate_1_t543 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18586(__this, ___index, method) (( void (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Reverse()
#define List_1_Reverse_m18587(__this, method) (( void (*) (List_1_t841 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Sort()
#define List_1_Sort_m18588(__this, method) (( void (*) (List_1_t841 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18589(__this, ___comparison, method) (( void (*) (List_1_t841 *, Comparison_1_t3639 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::ToArray()
#define List_1_ToArray_m18590(__this, method) (( AchievementU5BU5D_t3635* (*) (List_1_t841 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::TrimExcess()
#define List_1_TrimExcess_m18591(__this, method) (( void (*) (List_1_t841 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::get_Capacity()
#define List_1_get_Capacity_m18592(__this, method) (( int32_t (*) (List_1_t841 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18593(__this, ___value, method) (( void (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::get_Count()
#define List_1_get_Count_m18594(__this, method) (( int32_t (*) (List_1_t841 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::get_Item(System.Int32)
#define List_1_get_Item_m18595(__this, ___index, method) (( Achievement_t333 * (*) (List_1_t841 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>::set_Item(System.Int32,T)
#define List_1_set_Item_m18596(__this, ___index, ___value, method) (( void (*) (List_1_t841 *, int32_t, Achievement_t333 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
