﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_t3407;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m15308_gshared (KeyValuePair_2_t3407 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m15308(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, Object_t *, Object_t *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m15309_gshared (KeyValuePair_2_t3407 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m15309(__this, method) (( Object_t * (*) (KeyValuePair_2_t3407 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m15310_gshared (KeyValuePair_2_t3407 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m15310(__this, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m15311_gshared (KeyValuePair_2_t3407 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m15311(__this, method) (( Object_t * (*) (KeyValuePair_2_t3407 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m15312_gshared (KeyValuePair_2_t3407 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m15312(__this, ___value, method) (( void (*) (KeyValuePair_2_t3407 *, Object_t *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m15313_gshared (KeyValuePair_2_t3407 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m15313(__this, method) (( String_t* (*) (KeyValuePair_2_t3407 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
