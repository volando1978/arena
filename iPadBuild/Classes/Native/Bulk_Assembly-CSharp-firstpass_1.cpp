﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Advertisements.UnityAds
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// UnityEngine.Advertisements.UnityAds
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_UniMethodDeclarations.h"

// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// System.String
#include "mscorlib_System_String.h"
// System.Action`1<UnityEngine.Advertisements.ShowResult>
#include "mscorlib_System_Action_1_gen_5.h"
// UnityEngine.Advertisements.Advertisement
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv_0.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// UnityEngine.NetworkReachability
#include "UnityEngine_UnityEngine_NetworkReachability.h"
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.ShowOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho.h"
// UnityEngine.Advertisements.Optional.ShowOptionsExtended
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Opt.h"
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// UnityEngine.Advertisements.Advertisement
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv_0MethodDeclarations.h"
// UnityEngine.Advertisements.Utils
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_UtiMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsExternal
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_0MethodDeclarations.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.Advertisements.ShowOptions
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_ShoMethodDeclarations.h"
// UnityEngine.Advertisements.Optional.ShowOptionsExtended
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_OptMethodDeclarations.h"
// System.Action`1<UnityEngine.Advertisements.ShowResult>
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
struct GameObject_t144;
struct UnityAds_t154;
struct GameObject_t144;
struct Component_t230;
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Component>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Component>()
extern "C" Component_t230 * GameObject_AddComponent_TisComponent_t230_m1035_gshared (GameObject_t144 * __this, MethodInfo* method);
#define GameObject_AddComponent_TisComponent_t230_m1035(__this, method) (( Component_t230 * (*) (GameObject_t144 *, MethodInfo*))GameObject_AddComponent_TisComponent_t230_m1035_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Advertisements.UnityAds>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Advertisements.UnityAds>()
#define GameObject_AddComponent_TisUnityAds_t154_m1057(__this, method) (( UnityAds_t154 * (*) (GameObject_t144 *, MethodInfo*))GameObject_AddComponent_TisComponent_t230_m1035_gshared)(__this, method)

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Advertisements.UnityAds::.ctor()
extern "C" void UnityAds__ctor_m739 (UnityAds_t154 * __this, MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m850(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::.cctor()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Advertisement_t143_il2cpp_TypeInfo_var;
extern "C" void UnityAds__cctor_m740 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Advertisement_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2 = 0;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isInitialized_3 = 0;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___allowPrecache_4 = 1;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___initCalled_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemNameKey_7 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemPictureKey_8 = L_1;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____resultDelivered_9 = 0;
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___resultCallback_10 = (Action_1_t155 *)NULL;
		String_t* L_2 = Application_get_unityVersion_m1058(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t143_il2cpp_TypeInfo_var);
		String_t* L_3 = ((Advertisement_t143_StaticFields*)Advertisement_t143_il2cpp_TypeInfo_var->static_fields)->___version_0;
		String_t* L_4 = String_Concat_m912(NULL /*static, unused*/, L_2, (String_t*) &_stringLiteral313, L_3, /*hidden argument*/NULL);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____versionString_11 = L_4;
		return;
	}
}
// UnityEngine.Advertisements.UnityAds UnityEngine.Advertisements.UnityAds::get_SharedInstance()
extern const Il2CppType* UnityAds_t154_0_0_0_var;
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t144_il2cpp_TypeInfo_var;
extern MethodInfo* GameObject_AddComponent_TisUnityAds_t154_m1057_MethodInfo_var;
extern "C" UnityAds_t154 * UnityAds_get_SharedInstance_m741 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_0_0_0_var = il2cpp_codegen_type_from_index(155);
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		GameObject_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		GameObject_AddComponent_TisUnityAds_t154_m1057_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483811);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t144 * V_0 = {0};
	GameObject_t144 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_t154 * L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___sharedInstance_6;
		bool L_1 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m991(NULL /*static, unused*/, LoadTypeToken(UnityAds_t154_0_0_0_var), /*hidden argument*/NULL);
		Object_t187 * L_3 = Object_FindObjectOfType_m1060(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___sharedInstance_6 = ((UnityAds_t154 *)Castclass(L_3, UnityAds_t154_il2cpp_TypeInfo_var));
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_t154 * L_4 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___sharedInstance_6;
		bool L_5 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0062;
		}
	}
	{
		GameObject_t144 * L_6 = (GameObject_t144 *)il2cpp_codegen_object_new (GameObject_t144_il2cpp_TypeInfo_var);
		GameObject__ctor_m1061(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t144 * L_7 = V_1;
		NullCheck(L_7);
		Object_set_hideFlags_m1037(L_7, 3, /*hidden argument*/NULL);
		GameObject_t144 * L_8 = V_1;
		V_0 = L_8;
		GameObject_t144 * L_9 = V_0;
		NullCheck(L_9);
		UnityAds_t154 * L_10 = GameObject_AddComponent_TisUnityAds_t154_m1057(L_9, /*hidden argument*/GameObject_AddComponent_TisUnityAds_t154_m1057_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___sharedInstance_6 = L_10;
		GameObject_t144 * L_11 = V_0;
		NullCheck(L_11);
		Object_set_name_m1062(L_11, (String_t*) &_stringLiteral312, /*hidden argument*/NULL);
		GameObject_t144 * L_12 = V_0;
		Object_DontDestroyOnLoad_m854(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_t154 * L_13 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___sharedInstance_6;
		return L_13;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::Init(System.String,System.Boolean)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAds_Init_m742 (UnityAds_t154 * __this, String_t* ___gameId, bool ___testModeEnabled, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		Exception_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t135 * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___initCalled_5;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___initCalled_5 = 1;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = Application_get_internetReachability_m1063(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (L_1)
			{
				goto IL_002a;
			}
		}

IL_001b:
		{
			Utils_LogError_m735(NULL /*static, unused*/, (String_t*) &_stringLiteral314, /*hidden argument*/NULL);
			goto IL_0065;
		}

IL_002a:
		{
			goto IL_004a;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t135 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t135_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002f;
		throw e;
	}

CATCH_002f:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t135 *)__exception_local);
		Exception_t135 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral315, L_3, /*hidden argument*/NULL);
		Utils_LogDebug_m732(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_004a;
	} // end catch (depth: 1)

IL_004a:
	{
		String_t* L_5 = ___gameId;
		bool L_6 = ___testModeEnabled;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_t154 * L_7 = UnityAds_get_SharedInstance_m741(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_t144 * L_8 = Component_get_gameObject_m853(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m1064(L_8, /*hidden argument*/NULL);
		String_t* L_10 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____versionString_11;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsExternal_init_m773(NULL /*static, unused*/, L_5, L_6, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::Awake()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" void UnityAds_Awake_m743 (UnityAds_t154 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t144 * L_0 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_t154 * L_1 = UnityAds_get_SharedInstance_m741(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t144 * L_2 = Component_get_gameObject_m853(L_1, /*hidden argument*/NULL);
		bool L_3 = Object_op_Equality_m848(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		GameObject_t144 * L_4 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m854(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_002a:
	{
		GameObject_t144 * L_5 = Component_get_gameObject_m853(__this, /*hidden argument*/NULL);
		Object_Destroy_m855(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::isSupported()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_isSupported_m744 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_0 = UnityAdsExternal_isSupported_m776(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::getSDKVersion()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_getSDKVersion_m745 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_0 = UnityAdsExternal_getSDKVersion_m777(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAds_setLogLevel_m746 (Object_t * __this /* static, unused */, int32_t ___logLevel, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logLevel;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsExternal_setLogLevel_m787(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::canShowZone(System.String)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_canShowZone_m747 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isInitialized_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_1 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2;
		if (!L_1)
		{
			goto IL_0016;
		}
	}

IL_0014:
	{
		return 0;
	}

IL_0016:
	{
		String_t* L_2 = ___zone;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_3 = UnityAdsExternal_canShowZone_m778(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::hasMultipleRewardItems()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_hasMultipleRewardItems_m748 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_0 = UnityAdsExternal_hasMultipleRewardItems_m779(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.List`1<System.String> UnityEngine.Advertisements.UnityAds::getRewardItemKeys()
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t41_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m883_MethodInfo_var;
extern MethodInfo* List_1__ctor_m1066_MethodInfo_var;
extern "C" List_1_t43 * UnityAds_getRewardItemKeys_m749 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		CharU5BU5D_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		List_1__ctor_m883_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		List_1__ctor_m1066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t43 * V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_0 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m883(L_0, /*hidden argument*/List_1__ctor_m883_MethodInfo_var);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_1 = UnityAdsExternal_getRewardItemKeys_m780(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = V_1;
		CharU5BU5D_t41* L_3 = ((CharU5BU5D_t41*)SZArrayNew(CharU5BU5D_t41_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0)) = (uint16_t)((int32_t)59);
		NullCheck(L_2);
		StringU5BU5D_t169* L_4 = String_Split_m1065(L_2, L_3, /*hidden argument*/NULL);
		List_1_t43 * L_5 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m1066(L_5, (Object_t*)(Object_t*)L_4, /*hidden argument*/List_1__ctor_m1066_MethodInfo_var);
		V_0 = L_5;
		List_1_t43 * L_6 = V_0;
		return L_6;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::getDefaultRewardItemKey()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_getDefaultRewardItemKey_m750 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_0 = UnityAdsExternal_getDefaultRewardItemKey_m781(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::getCurrentRewardItemKey()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_getCurrentRewardItemKey_m751 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_0 = UnityAdsExternal_getCurrentRewardItemKey_m782(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::setRewardItemKey(System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_setRewardItemKey_m752 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___rewardItemKey;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_1 = UnityAdsExternal_setRewardItemKey_m783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::setDefaultRewardItemAsRewardItem()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAds_setDefaultRewardItemAsRewardItem_m753 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemNameKey()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_getRewardItemNameKey_m754 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemNameKey_7;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_1 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemNameKey_7;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m902(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_fillRewardItemKeyData_m763(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_3 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemNameKey_7;
		return L_3;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::getRewardItemPictureKey()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_getRewardItemPictureKey_m755 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemPictureKey_8;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_1 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemPictureKey_8;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m902(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_fillRewardItemKeyData_m763(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_3 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemPictureKey_8;
		return L_3;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Advertisements.UnityAds::getRewardItemDetailsWithKey(System.String)
extern TypeInfo* Dictionary_2_t165_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t41_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2__ctor_m911_MethodInfo_var;
extern MethodInfo* List_1__ctor_m1066_MethodInfo_var;
extern MethodInfo* List_1_ToArray_m1067_MethodInfo_var;
extern "C" Dictionary_2_t165 * UnityAds_getRewardItemDetailsWithKey_m756 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		CharU5BU5D_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		Dictionary_2__ctor_m911_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		List_1__ctor_m1066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		List_1_ToArray_m1067_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t165 * V_0 = {0};
	String_t* V_1 = {0};
	List_1_t43 * V_2 = {0};
	{
		Dictionary_2_t165 * L_0 = (Dictionary_2_t165 *)il2cpp_codegen_object_new (Dictionary_2_t165_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m911(L_0, /*hidden argument*/Dictionary_2__ctor_m911_MethodInfo_var);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		String_t* L_2 = ___rewardItemKey;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_3 = UnityAdsExternal_getRewardItemDetailsWithKey_m785(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		if (!L_4)
		{
			goto IL_0084;
		}
	}
	{
		String_t* L_5 = V_1;
		CharU5BU5D_t41* L_6 = ((CharU5BU5D_t41*)SZArrayNew(CharU5BU5D_t41_il2cpp_TypeInfo_var, 1));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_6, 0)) = (uint16_t)((int32_t)59);
		NullCheck(L_5);
		StringU5BU5D_t169* L_7 = String_Split_m1065(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_8 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m1066(L_8, (Object_t*)(Object_t*)L_7, /*hidden argument*/List_1__ctor_m1066_MethodInfo_var);
		V_2 = L_8;
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral316, L_9, /*hidden argument*/NULL);
		Utils_LogDebug_m732(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		List_1_t43 * L_11 = V_2;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_11);
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0084;
		}
	}
	{
		Dictionary_2_t165 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_14 = UnityAds_getRewardItemNameKey_m754(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t43 * L_15 = V_2;
		NullCheck(L_15);
		StringU5BU5D_t169* L_16 = List_1_ToArray_m1067(L_15, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_16);
		Object_t * L_17 = Array_GetValue_m1068(L_16, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_13, L_14, L_18);
		Dictionary_2_t165 * L_19 = V_0;
		String_t* L_20 = UnityAds_getRewardItemPictureKey_m755(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t43 * L_21 = V_2;
		NullCheck(L_21);
		StringU5BU5D_t169* L_22 = List_1_ToArray_m1067(L_21, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_22);
		Object_t * L_23 = Array_GetValue_m1068(L_22, 1, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_23);
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_19, L_20, L_24);
	}

IL_0084:
	{
		Dictionary_2_t165 * L_25 = V_0;
		return L_25;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* ShowOptionsExtended_t152_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t165_il2cpp_TypeInfo_var;
extern MethodInfo* Dictionary_2__ctor_m911_MethodInfo_var;
extern "C" void UnityAds_Show_m757 (UnityAds_t154 * __this, String_t* ___zoneId, ShowOptions_t153 * ___options, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		ShowOptionsExtended_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(178);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Dictionary_2_t165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Dictionary_2__ctor_m911_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	ShowOptionsExtended_t152 * V_1 = {0};
	Dictionary_2_t165 * V_2 = {0};
	{
		V_0 = (String_t*)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____resultDelivered_9 = 0;
		ShowOptions_t153 * L_0 = ___options;
		if (!L_0)
		{
			goto IL_0060;
		}
	}
	{
		ShowOptions_t153 * L_1 = ___options;
		NullCheck(L_1);
		Action_1_t155 * L_2 = ShowOptions_get_resultCallback_m825(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		ShowOptions_t153 * L_3 = ___options;
		NullCheck(L_3);
		Action_1_t155 * L_4 = ShowOptions_get_resultCallback_m825(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___resultCallback_10 = L_4;
	}

IL_0024:
	{
		ShowOptions_t153 * L_5 = ___options;
		V_1 = ((ShowOptionsExtended_t152 *)IsInst(L_5, ShowOptionsExtended_t152_il2cpp_TypeInfo_var));
		ShowOptionsExtended_t152 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0059;
		}
	}
	{
		ShowOptionsExtended_t152 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = ShowOptionsExtended_get_gamerSid_m737(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		ShowOptionsExtended_t152 * L_9 = V_1;
		NullCheck(L_9);
		String_t* L_10 = ShowOptionsExtended_get_gamerSid_m737(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m902(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		ShowOptionsExtended_t152 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = ShowOptionsExtended_get_gamerSid_m737(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0060;
	}

IL_0059:
	{
		ShowOptions_t153 * L_14 = ___options;
		NullCheck(L_14);
		String_t* L_15 = ShowOptions_get_gamerSid_m827(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_16 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isInitialized_3;
		if (!L_16)
		{
			goto IL_0074;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_17 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2;
		if (!L_17)
		{
			goto IL_007b;
		}
	}

IL_0074:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}

IL_007b:
	{
		String_t* L_18 = V_0;
		if (!L_18)
		{
			goto IL_00af;
		}
	}
	{
		String_t* L_19 = ___zoneId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Dictionary_2_t165 * L_21 = (Dictionary_2_t165 *)il2cpp_codegen_object_new (Dictionary_2_t165_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m911(L_21, /*hidden argument*/Dictionary_2__ctor_m911_MethodInfo_var);
		V_2 = L_21;
		Dictionary_2_t165 * L_22 = V_2;
		String_t* L_23 = V_0;
		NullCheck(L_22);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_22, (String_t*) &_stringLiteral317, L_23);
		Dictionary_2_t165 * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_25 = UnityAds_show_m760(NULL /*static, unused*/, L_19, L_20, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00aa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		goto IL_00c0;
	}

IL_00af:
	{
		String_t* L_26 = ___zoneId;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_27 = UnityAds_show_m758(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00c0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_show_m758 (Object_t * __this /* static, unused */, String_t* ___zoneId, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___zoneId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_2 = UnityAds_show_m760(NULL /*static, unused*/, L_0, L_1, (Dictionary_2_t165 *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_show_m759 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = ___rewardItemKey;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_2 = UnityAds_show_m760(NULL /*static, unused*/, L_0, L_1, (Dictionary_2_t165 *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAds::show(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAds_show_m760 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, Dictionary_2_t165 * ___options, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2;
		if (L_0)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2 = 1;
		UnityAds_t154 * L_1 = UnityAds_get_SharedInstance_m741(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Object_op_Implicit_m1059(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		Dictionary_2_t165 * L_3 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		String_t* L_4 = UnityAds_parseOptionsDictionary_m764(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___zoneId;
		String_t* L_6 = ___rewardItemKey;
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_8 = UnityAdsExternal_show_m774(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		return 1;
	}

IL_0035:
	{
		return 0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::deliverCallback(UnityEngine.Advertisements.ShowResult)
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" void UnityAds_deliverCallback_m761 (Object_t * __this /* static, unused */, int32_t ___result, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2 = 0;
		Action_1_t155 * L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___resultCallback_10;
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_1 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____resultDelivered_9;
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____resultDelivered_9 = 1;
		Action_1_t155 * L_2 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___resultCallback_10;
		int32_t L_3 = ___result;
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::Invoke(!0) */, L_2, L_3);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___resultCallback_10 = (Action_1_t155 *)NULL;
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::hide()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAds_hide_m762 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsExternal_hide_m775(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::fillRewardItemKeyData()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t41_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m1066_MethodInfo_var;
extern MethodInfo* List_1_ToArray_m1067_MethodInfo_var;
extern "C" void UnityAds_fillRewardItemKeyData_m763 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		CharU5BU5D_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		List_1__ctor_m1066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		List_1_ToArray_m1067_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	List_1_t43 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		String_t* L_0 = UnityAdsExternal_getRewardItemDetailsKeys_m786(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m902(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)2)))
		{
			goto IL_005b;
		}
	}
	{
		String_t* L_4 = V_0;
		CharU5BU5D_t41* L_5 = ((CharU5BU5D_t41*)SZArrayNew(CharU5BU5D_t41_il2cpp_TypeInfo_var, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_5, 0)) = (uint16_t)((int32_t)59);
		NullCheck(L_4);
		StringU5BU5D_t169* L_6 = String_Split_m1065(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_7 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m1066(L_7, (Object_t*)(Object_t*)L_6, /*hidden argument*/List_1__ctor_m1066_MethodInfo_var);
		V_1 = L_7;
		List_1_t43 * L_8 = V_1;
		NullCheck(L_8);
		StringU5BU5D_t169* L_9 = List_1_ToArray_m1067(L_8, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_9);
		Object_t * L_10 = Array_GetValue_m1068(L_9, 0, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemNameKey_7 = L_11;
		List_1_t43 * L_12 = V_1;
		NullCheck(L_12);
		StringU5BU5D_t169* L_13 = List_1_ToArray_m1067(L_12, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_13);
		Object_t * L_14 = Array_GetValue_m1068(L_13, 1, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->____rewardItemPictureKey_8 = L_15;
	}

IL_005b:
	{
		return;
	}
}
// System.String UnityEngine.Advertisements.UnityAds::parseOptionsDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAds_parseOptionsDictionary_m764 (Object_t * __this /* static, unused */, Dictionary_2_t165 * ___options, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	String_t* G_B4_0 = {0};
	String_t* G_B3_0 = {0};
	String_t* G_B5_0 = {0};
	String_t* G_B5_1 = {0};
	String_t* G_B9_0 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B10_0 = {0};
	String_t* G_B10_1 = {0};
	String_t* G_B14_0 = {0};
	String_t* G_B13_0 = {0};
	String_t* G_B15_0 = {0};
	String_t* G_B15_1 = {0};
	String_t* G_B19_0 = {0};
	String_t* G_B18_0 = {0};
	String_t* G_B20_0 = {0};
	String_t* G_B20_1 = {0};
	String_t* G_B24_0 = {0};
	String_t* G_B23_0 = {0};
	String_t* G_B25_0 = {0};
	String_t* G_B25_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		Dictionary_2_t165 * L_1 = ___options;
		if (!L_1)
		{
			goto IL_0144;
		}
	}
	{
		V_1 = 0;
		Dictionary_2_t165 * L_2 = ___options;
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_2, (String_t*) &_stringLiteral318);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_4 = V_0;
		bool L_5 = V_1;
		G_B3_0 = L_4;
		if (!L_5)
		{
			G_B4_0 = L_4;
			goto IL_002f;
		}
	}
	{
		G_B5_0 = (String_t*) &_stringLiteral41;
		G_B5_1 = G_B3_0;
		goto IL_0034;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B5_0 = L_6;
		G_B5_1 = G_B4_0;
	}

IL_0034:
	{
		Dictionary_2_t165 * L_7 = ___options;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_7, (String_t*) &_stringLiteral318);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1025(NULL /*static, unused*/, G_B5_1, G_B5_0, (String_t*) &_stringLiteral319, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		V_1 = 1;
	}

IL_004c:
	{
		Dictionary_2_t165 * L_10 = ___options;
		NullCheck(L_10);
		bool L_11 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_10, (String_t*) &_stringLiteral320);
		if (!L_11)
		{
			goto IL_008a;
		}
	}
	{
		String_t* L_12 = V_0;
		bool L_13 = V_1;
		G_B8_0 = L_12;
		if (!L_13)
		{
			G_B9_0 = L_12;
			goto IL_006d;
		}
	}
	{
		G_B10_0 = (String_t*) &_stringLiteral41;
		G_B10_1 = G_B8_0;
		goto IL_0072;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B10_0 = L_14;
		G_B10_1 = G_B9_0;
	}

IL_0072:
	{
		Dictionary_2_t165 * L_15 = ___options;
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_15, (String_t*) &_stringLiteral320);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m1025(NULL /*static, unused*/, G_B10_1, G_B10_0, (String_t*) &_stringLiteral321, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		V_1 = 1;
	}

IL_008a:
	{
		Dictionary_2_t165 * L_18 = ___options;
		NullCheck(L_18);
		bool L_19 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_18, (String_t*) &_stringLiteral317);
		if (!L_19)
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_20 = V_0;
		bool L_21 = V_1;
		G_B13_0 = L_20;
		if (!L_21)
		{
			G_B14_0 = L_20;
			goto IL_00ab;
		}
	}
	{
		G_B15_0 = (String_t*) &_stringLiteral41;
		G_B15_1 = G_B13_0;
		goto IL_00b0;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B15_0 = L_22;
		G_B15_1 = G_B14_0;
	}

IL_00b0:
	{
		Dictionary_2_t165 * L_23 = ___options;
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_23, (String_t*) &_stringLiteral317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m1025(NULL /*static, unused*/, G_B15_1, G_B15_0, (String_t*) &_stringLiteral322, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		V_1 = 1;
	}

IL_00c8:
	{
		Dictionary_2_t165 * L_26 = ___options;
		NullCheck(L_26);
		bool L_27 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_26, (String_t*) &_stringLiteral323);
		if (!L_27)
		{
			goto IL_0106;
		}
	}
	{
		String_t* L_28 = V_0;
		bool L_29 = V_1;
		G_B18_0 = L_28;
		if (!L_29)
		{
			G_B19_0 = L_28;
			goto IL_00e9;
		}
	}
	{
		G_B20_0 = (String_t*) &_stringLiteral41;
		G_B20_1 = G_B18_0;
		goto IL_00ee;
	}

IL_00e9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B20_0 = L_30;
		G_B20_1 = G_B19_0;
	}

IL_00ee:
	{
		Dictionary_2_t165 * L_31 = ___options;
		NullCheck(L_31);
		String_t* L_32 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_31, (String_t*) &_stringLiteral323);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1025(NULL /*static, unused*/, G_B20_1, G_B20_0, (String_t*) &_stringLiteral324, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		V_1 = 1;
	}

IL_0106:
	{
		Dictionary_2_t165 * L_34 = ___options;
		NullCheck(L_34);
		bool L_35 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_34, (String_t*) &_stringLiteral325);
		if (!L_35)
		{
			goto IL_0144;
		}
	}
	{
		String_t* L_36 = V_0;
		bool L_37 = V_1;
		G_B23_0 = L_36;
		if (!L_37)
		{
			G_B24_0 = L_36;
			goto IL_0127;
		}
	}
	{
		G_B25_0 = (String_t*) &_stringLiteral41;
		G_B25_1 = G_B23_0;
		goto IL_012c;
	}

IL_0127:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B25_0 = L_38;
		G_B25_1 = G_B24_0;
	}

IL_012c:
	{
		Dictionary_2_t165 * L_39 = ___options;
		NullCheck(L_39);
		String_t* L_40 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_39, (String_t*) &_stringLiteral325);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1025(NULL /*static, unused*/, G_B25_1, G_B25_0, (String_t*) &_stringLiteral326, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		V_1 = 1;
	}

IL_0144:
	{
		String_t* L_42 = V_0;
		return L_42;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onHide()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" void UnityAds_onHide_m765 (UnityAds_t154 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isShowing_2 = 0;
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Utils_LogDebug_m732(NULL /*static, unused*/, (String_t*) &_stringLiteral327, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onShow()
extern "C" void UnityAds_onShow_m766 (UnityAds_t154 * __this, MethodInfo* method)
{
	{
		Utils_LogDebug_m732(NULL /*static, unused*/, (String_t*) &_stringLiteral328, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onVideoStarted()
extern "C" void UnityAds_onVideoStarted_m767 (UnityAds_t154 * __this, MethodInfo* method)
{
	{
		Utils_LogDebug_m732(NULL /*static, unused*/, (String_t*) &_stringLiteral329, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onVideoCompleted(System.String)
extern TypeInfo* CharU5BU5D_t41_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t43_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m1066_MethodInfo_var;
extern MethodInfo* List_1_ToArray_m1067_MethodInfo_var;
extern "C" void UnityAds_onVideoCompleted_m768 (UnityAds_t154 * __this, String_t* ___parameters, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		List_1_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		List_1__ctor_m1066_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		List_1_ToArray_m1067_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t43 * V_0 = {0};
	String_t* V_1 = {0};
	bool V_2 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___parameters;
		if (!L_0)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_1 = ___parameters;
		CharU5BU5D_t41* L_2 = ((CharU5BU5D_t41*)SZArrayNew(CharU5BU5D_t41_il2cpp_TypeInfo_var, 1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0)) = (uint16_t)((int32_t)59);
		NullCheck(L_1);
		StringU5BU5D_t169* L_3 = String_Split_m1065(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t43_il2cpp_TypeInfo_var);
		List_1_t43 * L_4 = (List_1_t43 *)il2cpp_codegen_object_new (List_1_t43_il2cpp_TypeInfo_var);
		List_1__ctor_m1066(L_4, (Object_t*)(Object_t*)L_3, /*hidden argument*/List_1__ctor_m1066_MethodInfo_var);
		V_0 = L_4;
		List_1_t43 * L_5 = V_0;
		NullCheck(L_5);
		StringU5BU5D_t169* L_6 = List_1_ToArray_m1067(L_5, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_6);
		Object_t * L_7 = Array_GetValue_m1068(L_6, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		V_1 = L_8;
		List_1_t43 * L_9 = V_0;
		NullCheck(L_9);
		StringU5BU5D_t169* L_10 = List_1_ToArray_m1067(L_9, /*hidden argument*/List_1_ToArray_m1067_MethodInfo_var);
		NullCheck(L_10);
		Object_t * L_11 = Array_GetValue_m1068(L_10, 1, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m835(NULL /*static, unused*/, L_12, (String_t*) &_stringLiteral27, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0055;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0056;
	}

IL_0055:
	{
		G_B4_0 = 0;
	}

IL_0056:
	{
		V_2 = G_B4_0;
		ObjectU5BU5D_t208* L_14 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, (String_t*) &_stringLiteral330);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 0)) = (Object_t *)(String_t*) &_stringLiteral330;
		ObjectU5BU5D_t208* L_15 = L_14;
		String_t* L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		ArrayElementTypeCheck (L_15, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 1)) = (Object_t *)L_16;
		ObjectU5BU5D_t208* L_17 = L_15;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, (String_t*) &_stringLiteral331);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 2)) = (Object_t *)(String_t*) &_stringLiteral331;
		ObjectU5BU5D_t208* L_18 = L_17;
		bool L_19 = V_2;
		bool L_20 = L_19;
		Object_t * L_21 = Box(Boolean_t203_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 3)) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m976(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Utils_LogDebug_m732(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		bool L_23 = V_2;
		if (!L_23)
		{
			goto IL_0095;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_0095:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		UnityAds_deliverCallback_m761(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onFetchCompleted()
extern TypeInfo* UnityAds_t154_il2cpp_TypeInfo_var;
extern "C" void UnityAds_onFetchCompleted_m769 (UnityAds_t154 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAds_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAds_t154_il2cpp_TypeInfo_var);
		((UnityAds_t154_StaticFields*)UnityAds_t154_il2cpp_TypeInfo_var->static_fields)->___isInitialized_3 = 1;
		Utils_LogDebug_m732(NULL /*static, unused*/, (String_t*) &_stringLiteral332, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAds::onFetchFailed()
extern "C" void UnityAds_onFetchFailed_m770 (UnityAds_t154 * __this, MethodInfo* method)
{
	{
		Utils_LogDebug_m732(NULL /*static, unused*/, (String_t*) &_stringLiteral333, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Advertisements.UnityAdsExternal
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_0.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Advertisements.UnityAdsPlatform
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_2.h"
// UnityEngine.Advertisements.UnityAdsIos
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_1.h"
// UnityEngine.Advertisements.UnityAdsIos
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_1MethodDeclarations.h"
// UnityEngine.Advertisements.UnityAdsPlatform
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_2MethodDeclarations.h"


// System.Void UnityEngine.Advertisements.UnityAdsExternal::.cctor()
extern "C" void UnityAdsExternal__cctor_m771 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Advertisements.UnityAdsPlatform UnityEngine.Advertisements.UnityAdsExternal::getImpl()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsIos_t158_il2cpp_TypeInfo_var;
extern "C" UnityAdsPlatform_t156 * UnityAdsExternal_getImpl_m772 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		UnityAdsIos_t158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(179);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		bool L_0 = ((UnityAdsExternal_t157_StaticFields*)UnityAdsExternal_t157_il2cpp_TypeInfo_var->static_fields)->___initialized_1;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		((UnityAdsExternal_t157_StaticFields*)UnityAdsExternal_t157_il2cpp_TypeInfo_var->static_fields)->___initialized_1 = 1;
		UnityAdsIos_t158 * L_1 = (UnityAdsIos_t158 *)il2cpp_codegen_object_new (UnityAdsIos_t158_il2cpp_TypeInfo_var);
		UnityAdsIos__ctor_m788(L_1, /*hidden argument*/NULL);
		((UnityAdsExternal_t157_StaticFields*)UnityAdsExternal_t157_il2cpp_TypeInfo_var->static_fields)->___impl_0 = L_1;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_2 = ((UnityAdsExternal_t157_StaticFields*)UnityAdsExternal_t157_il2cpp_TypeInfo_var->static_fields)->___impl_0;
		return L_2;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsExternal::init(System.String,System.Boolean,System.String,System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAdsExternal_init_m773 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___gameId;
		bool L_2 = ___testModeEnabled;
		String_t* L_3 = ___gameObjectName;
		String_t* L_4 = ___unityVersion;
		NullCheck(L_0);
		VirtActionInvoker4< String_t*, bool, String_t*, String_t* >::Invoke(4 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::init(System.String,System.Boolean,System.String,System.String) */, L_0, L_1, L_2, L_3, L_4);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::show(System.String,System.String,System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsExternal_show_m774 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___zoneId;
		String_t* L_2 = ___rewardItemKey;
		String_t* L_3 = ___options;
		NullCheck(L_0);
		bool L_4 = (bool)VirtFuncInvoker3< bool, String_t*, String_t*, String_t* >::Invoke(5 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::show(System.String,System.String,System.String) */, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsExternal::hide()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAdsExternal_hide_m775 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::hide() */, L_0);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::isSupported()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsExternal_isSupported_m776 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::isSupported() */, L_0);
		return L_1;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getSDKVersion()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getSDKVersion_m777 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getSDKVersion() */, L_0);
		return L_1;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::canShowZone(System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsExternal_canShowZone_m778 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___zone;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(9 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::canShowZone(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::hasMultipleRewardItems()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsExternal_hasMultipleRewardItems_m779 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::hasMultipleRewardItems() */, L_0);
		return L_1;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemKeys()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getRewardItemKeys_m780 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemKeys() */, L_0);
		return L_1;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getDefaultRewardItemKey()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getDefaultRewardItemKey_m781 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getDefaultRewardItemKey() */, L_0);
		return L_1;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getCurrentRewardItemKey()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getCurrentRewardItemKey_m782 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getCurrentRewardItemKey() */, L_0);
		return L_1;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::setRewardItemKey(System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsExternal_setRewardItemKey_m783 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___rewardItemKey;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(14 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::setRewardItemKey(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setDefaultRewardItemAsRewardItem()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(15 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::setDefaultRewardItemAsRewardItem() */, L_0);
		return;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsWithKey(System.String)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getRewardItemDetailsWithKey_m785 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___rewardItemKey;
		NullCheck(L_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(16 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsWithKey(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsKeys()
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" String_t* UnityAdsExternal_getRewardItemDetailsKeys_m786 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(17 /* System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsKeys() */, L_0);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern TypeInfo* UnityAdsExternal_t157_il2cpp_TypeInfo_var;
extern "C" void UnityAdsExternal_setLogLevel_m787 (Object_t * __this /* static, unused */, int32_t ___logLevel, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsExternal_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityAdsExternal_t157_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t156 * L_0 = UnityAdsExternal_getImpl_m772(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___logLevel;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(18 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel) */, L_0, L_1);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Advertisements.UnityAdsIosBridge
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_3MethodDeclarations.h"


// System.Void UnityEngine.Advertisements.UnityAdsIos::.ctor()
extern "C" void UnityAdsIos__ctor_m788 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		UnityAdsPlatform__ctor_m821(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsIos::init(System.String,System.Boolean,System.String,System.String)
extern TypeInfo* Advertisement_t143_il2cpp_TypeInfo_var;
extern "C" void UnityAdsIos_init_m789 (UnityAdsIos_t158 * __this, String_t* ___gameId, bool ___testModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Advertisement_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B4_0 = false;
	String_t* G_B4_1 = {0};
	bool G_B3_0 = false;
	String_t* G_B3_1 = {0};
	int32_t G_B5_0 = 0;
	bool G_B5_1 = false;
	String_t* G_B5_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t143_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_get_UnityDeveloperInternalTestMode_m701(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		String_t* L_1 = ___gameId;
		bool L_2 = ___testModeEnabled;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t143_il2cpp_TypeInfo_var);
		int32_t L_3 = Advertisement_get_debugLevel_m690(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = L_1;
		if (!((int32_t)((int32_t)L_3&(int32_t)8)))
		{
			G_B4_0 = L_2;
			G_B4_1 = L_1;
			goto IL_0023;
		}
	}
	{
		G_B5_0 = 1;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0024;
	}

IL_0023:
	{
		G_B5_0 = 0;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0024:
	{
		String_t* L_4 = ___gameObjectName;
		String_t* L_5 = ___unityVersion;
		UnityAdsIosBridge_UnityAdsInit_m804(NULL /*static, unused*/, G_B5_2, G_B5_1, G_B5_0, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::show(System.String,System.String,System.String)
extern "C" bool UnityAdsIos_show_m790 (UnityAdsIos_t158 * __this, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method)
{
	{
		String_t* L_0 = ___zoneId;
		String_t* L_1 = ___rewardItemKey;
		String_t* L_2 = ___options;
		bool L_3 = UnityAdsIosBridge_UnityAdsShow_m805(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsIos::hide()
extern "C" void UnityAdsIos_hide_m791 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		UnityAdsIosBridge_UnityAdsHide_m806(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::isSupported()
extern "C" bool UnityAdsIos_isSupported_m792 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		bool L_0 = UnityAdsIosBridge_UnityAdsIsSupported_m807(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getSDKVersion()
extern "C" String_t* UnityAdsIos_getSDKVersion_m793 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = UnityAdsIosBridge_UnityAdsGetSDKVersion_m808(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::canShowZone(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool UnityAdsIos_canShowZone_m794 (UnityAdsIos_t158 * __this, String_t* ___zone, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___zone;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m899(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___zone;
		bool L_3 = UnityAdsIosBridge_UnityAdsCanShowZone_m810(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		bool L_4 = UnityAdsIosBridge_UnityAdsCanShow_m809(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::hasMultipleRewardItems()
extern "C" bool UnityAdsIos_hasMultipleRewardItems_m795 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		bool L_0 = UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemKeys()
extern "C" String_t* UnityAdsIos_getRewardItemKeys_m796 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getDefaultRewardItemKey()
extern "C" String_t* UnityAdsIos_getDefaultRewardItemKey_m797 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getCurrentRewardItemKey()
extern "C" String_t* UnityAdsIos_getCurrentRewardItemKey_m798 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIos::setRewardItemKey(System.String)
extern "C" bool UnityAdsIos_setRewardItemKey_m799 (UnityAdsIos_t158 * __this, String_t* ___rewardItemKey, MethodInfo* method)
{
	{
		String_t* L_0 = ___rewardItemKey;
		bool L_1 = UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsIos::setDefaultRewardItemAsRewardItem()
extern "C" void UnityAdsIos_setDefaultRewardItemAsRewardItem_m800 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsWithKey(System.String)
extern "C" String_t* UnityAdsIos_getRewardItemDetailsWithKey_m801 (UnityAdsIos_t158 * __this, String_t* ___rewardItemKey, MethodInfo* method)
{
	{
		String_t* L_0 = ___rewardItemKey;
		String_t* L_1 = UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String UnityEngine.Advertisements.UnityAdsIos::getRewardItemDetailsKeys()
extern "C" String_t* UnityAdsIos_getRewardItemDetailsKeys_m802 (UnityAdsIos_t158 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsIos::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern TypeInfo* Advertisement_t143_il2cpp_TypeInfo_var;
extern "C" void UnityAdsIos_setLogLevel_m803 (UnityAdsIos_t158 * __this, int32_t ___logLevel, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Advertisement_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t143_il2cpp_TypeInfo_var);
		int32_t L_0 = Advertisement_get_debugLevel_m690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)L_0&(int32_t)8)))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		UnityAdsIosBridge_UnityAdsSetDebugMode_m819(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Advertisements.UnityAdsIosBridge
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Uni_3.h"
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsInit(System.String,System.Boolean,System.Boolean,System.String,System.String)
extern "C" {void DEFAULT_CALL UnityAdsInit(char*, int32_t, int32_t, char*, char*);}
extern "C" void UnityAdsIosBridge_UnityAdsInit_m804 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, bool ___debugModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsInit;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsInit'"));
		}
	}

	// Marshaling of parameter '___gameId' to native representation
	char* ____gameId_marshaled = { 0 };
	____gameId_marshaled = il2cpp_codegen_marshal_string(___gameId);

	// Marshaling of parameter '___testModeEnabled' to native representation

	// Marshaling of parameter '___debugModeEnabled' to native representation

	// Marshaling of parameter '___gameObjectName' to native representation
	char* ____gameObjectName_marshaled = { 0 };
	____gameObjectName_marshaled = il2cpp_codegen_marshal_string(___gameObjectName);

	// Marshaling of parameter '___unityVersion' to native representation
	char* ____unityVersion_marshaled = { 0 };
	____unityVersion_marshaled = il2cpp_codegen_marshal_string(___unityVersion);

	// Native function invocation
	_il2cpp_pinvoke_func(____gameId_marshaled, ___testModeEnabled, ___debugModeEnabled, ____gameObjectName_marshaled, ____unityVersion_marshaled);

	// Marshaling cleanup of parameter '___gameId' native representation
	il2cpp_codegen_marshal_free(____gameId_marshaled);
	____gameId_marshaled = NULL;

	// Marshaling cleanup of parameter '___testModeEnabled' native representation

	// Marshaling cleanup of parameter '___debugModeEnabled' native representation

	// Marshaling cleanup of parameter '___gameObjectName' native representation
	il2cpp_codegen_marshal_free(____gameObjectName_marshaled);
	____gameObjectName_marshaled = NULL;

	// Marshaling cleanup of parameter '___unityVersion' native representation
	il2cpp_codegen_marshal_free(____unityVersion_marshaled);
	____unityVersion_marshaled = NULL;

}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsShow(System.String,System.String,System.String)
extern "C" {int32_t DEFAULT_CALL UnityAdsShow(char*, char*, char*);}
extern "C" bool UnityAdsIosBridge_UnityAdsShow_m805 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsShow;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsShow'"));
		}
	}

	// Marshaling of parameter '___zoneId' to native representation
	char* ____zoneId_marshaled = { 0 };
	____zoneId_marshaled = il2cpp_codegen_marshal_string(___zoneId);

	// Marshaling of parameter '___rewardItemKey' to native representation
	char* ____rewardItemKey_marshaled = { 0 };
	____rewardItemKey_marshaled = il2cpp_codegen_marshal_string(___rewardItemKey);

	// Marshaling of parameter '___options' to native representation
	char* ____options_marshaled = { 0 };
	____options_marshaled = il2cpp_codegen_marshal_string(___options);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zoneId_marshaled, ____rewardItemKey_marshaled, ____options_marshaled);

	// Marshaling cleanup of parameter '___zoneId' native representation
	il2cpp_codegen_marshal_free(____zoneId_marshaled);
	____zoneId_marshaled = NULL;

	// Marshaling cleanup of parameter '___rewardItemKey' native representation
	il2cpp_codegen_marshal_free(____rewardItemKey_marshaled);
	____rewardItemKey_marshaled = NULL;

	// Marshaling cleanup of parameter '___options' native representation
	il2cpp_codegen_marshal_free(____options_marshaled);
	____options_marshaled = NULL;

	return _return_value;
}
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHide()
extern "C" {void DEFAULT_CALL UnityAdsHide();}
extern "C" void UnityAdsIosBridge_UnityAdsHide_m806 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsHide;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsHide'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsIsSupported()
extern "C" {int32_t DEFAULT_CALL UnityAdsIsSupported();}
extern "C" bool UnityAdsIosBridge_UnityAdsIsSupported_m807 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsIsSupported;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsIsSupported'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetSDKVersion()
extern "C" {char* DEFAULT_CALL UnityAdsGetSDKVersion();}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetSDKVersion_m808 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetSDKVersion;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetSDKVersion'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShow()
extern "C" {int32_t DEFAULT_CALL UnityAdsCanShow();}
extern "C" bool UnityAdsIosBridge_UnityAdsCanShow_m809 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsCanShow;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsCanShow'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsCanShowZone(System.String)
extern "C" {int32_t DEFAULT_CALL UnityAdsCanShowZone(char*);}
extern "C" bool UnityAdsIosBridge_UnityAdsCanShowZone_m810 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsCanShowZone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsCanShowZone'"));
		}
	}

	// Marshaling of parameter '___zone' to native representation
	char* ____zone_marshaled = { 0 };
	____zone_marshaled = il2cpp_codegen_marshal_string(___zone);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____zone_marshaled);

	// Marshaling cleanup of parameter '___zone' native representation
	il2cpp_codegen_marshal_free(____zone_marshaled);
	____zone_marshaled = NULL;

	return _return_value;
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsHasMultipleRewardItems()
extern "C" {int32_t DEFAULT_CALL UnityAdsHasMultipleRewardItems();}
extern "C" bool UnityAdsIosBridge_UnityAdsHasMultipleRewardItems_m811 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsHasMultipleRewardItems;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsHasMultipleRewardItems'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemKeys()
extern "C" {char* DEFAULT_CALL UnityAdsGetRewardItemKeys();}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemKeys_m812 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetRewardItemKeys;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetRewardItemKeys'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetDefaultRewardItemKey()
extern "C" {char* DEFAULT_CALL UnityAdsGetDefaultRewardItemKey();}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetDefaultRewardItemKey_m813 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetDefaultRewardItemKey;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetDefaultRewardItemKey'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetCurrentRewardItemKey()
extern "C" {char* DEFAULT_CALL UnityAdsGetCurrentRewardItemKey();}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetCurrentRewardItemKey_m814 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetCurrentRewardItemKey;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetCurrentRewardItemKey'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Boolean UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetRewardItemKey(System.String)
extern "C" {int32_t DEFAULT_CALL UnityAdsSetRewardItemKey(char*);}
extern "C" bool UnityAdsIosBridge_UnityAdsSetRewardItemKey_m815 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsSetRewardItemKey;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsSetRewardItemKey'"));
		}
	}

	// Marshaling of parameter '___rewardItemKey' to native representation
	char* ____rewardItemKey_marshaled = { 0 };
	____rewardItemKey_marshaled = il2cpp_codegen_marshal_string(___rewardItemKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____rewardItemKey_marshaled);

	// Marshaling cleanup of parameter '___rewardItemKey' native representation
	il2cpp_codegen_marshal_free(____rewardItemKey_marshaled);
	____rewardItemKey_marshaled = NULL;

	return _return_value;
}
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDefaultRewardItemAsRewardItem()
extern "C" {void DEFAULT_CALL UnityAdsSetDefaultRewardItemAsRewardItem();}
extern "C" void UnityAdsIosBridge_UnityAdsSetDefaultRewardItemAsRewardItem_m816 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsSetDefaultRewardItemAsRewardItem;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsSetDefaultRewardItemAsRewardItem'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsWithKey(System.String)
extern "C" {char* DEFAULT_CALL UnityAdsGetRewardItemDetailsWithKey(char*);}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemDetailsWithKey_m817 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetRewardItemDetailsWithKey;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetRewardItemDetailsWithKey'"));
		}
	}

	// Marshaling of parameter '___rewardItemKey' to native representation
	char* ____rewardItemKey_marshaled = { 0 };
	____rewardItemKey_marshaled = il2cpp_codegen_marshal_string(___rewardItemKey);

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func(____rewardItemKey_marshaled);
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	// Marshaling cleanup of parameter '___rewardItemKey' native representation
	il2cpp_codegen_marshal_free(____rewardItemKey_marshaled);
	____rewardItemKey_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.String UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsGetRewardItemDetailsKeys()
extern "C" {char* DEFAULT_CALL UnityAdsGetRewardItemDetailsKeys();}
extern "C" String_t* UnityAdsIosBridge_UnityAdsGetRewardItemDetailsKeys_m818 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsGetRewardItemDetailsKeys;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsGetRewardItemDetailsKeys'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsSetDebugMode(System.Boolean)
extern "C" {void DEFAULT_CALL UnityAdsSetDebugMode(int32_t);}
extern "C" void UnityAdsIosBridge_UnityAdsSetDebugMode_m819 (Object_t * __this /* static, unused */, bool ___debugMode, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsSetDebugMode;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsSetDebugMode'"));
		}
	}

	// Marshaling of parameter '___debugMode' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___debugMode);

	// Marshaling cleanup of parameter '___debugMode' native representation

}
// System.Void UnityEngine.Advertisements.UnityAdsIosBridge::UnityAdsEnableUnityDeveloperInternalTestMode()
extern "C" {void DEFAULT_CALL UnityAdsEnableUnityDeveloperInternalTestMode();}
extern "C" void UnityAdsIosBridge_UnityAdsEnableUnityDeveloperInternalTestMode_m820 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)UnityAdsEnableUnityDeveloperInternalTestMode;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'UnityAdsEnableUnityDeveloperInternalTestMode'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Advertisements.UnityAdsPlatform::.ctor()
extern "C" void UnityAdsPlatform__ctor_m821 (UnityAdsPlatform_t156 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::init(System.String,System.Boolean,System.String,System.String)
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::show(System.String,System.String,System.String)
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::hide()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::isSupported()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getSDKVersion()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::canShowZone(System.String)
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::hasMultipleRewardItems()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemKeys()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getDefaultRewardItemKey()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getCurrentRewardItemKey()
// System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::setRewardItemKey(System.String)
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setDefaultRewardItemAsRewardItem()
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsWithKey(System.String)
// System.String UnityEngine.Advertisements.UnityAdsPlatform::getRewardItemDetailsKeys()
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C" void ShowOptions__ctor_m822 (ShowOptions_t153 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.ShowOptions::get_pause()
extern "C" bool ShowOptions_get_pause_m823 (ShowOptions_t153 * __this, MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CpauseU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::set_pause(System.Boolean)
extern "C" void ShowOptions_set_pause_m824 (ShowOptions_t153 * __this, bool ___value, MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CpauseU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern "C" Action_1_t155 * ShowOptions_get_resultCallback_m825 (ShowOptions_t153 * __this, MethodInfo* method)
{
	{
		Action_1_t155 * L_0 = (__this->___U3CresultCallbackU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C" void ShowOptions_set_resultCallback_m826 (ShowOptions_t153 * __this, Action_1_t155 * ___value, MethodInfo* method)
{
	{
		Action_1_t155 * L_0 = ___value;
		__this->___U3CresultCallbackU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern "C" String_t* ShowOptions_get_gamerSid_m827 (ShowOptions_t153 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CgamerSidU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern "C" void ShowOptions_set_gamerSid_m828 (ShowOptions_t153 * __this, String_t* ___value, MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CgamerSidU3Ek__BackingField_2 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Advertisements.ShowResult
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Sho_0MethodDeclarations.h"



// <PrivateImplementationDetails>/$ArrayType$8
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$8
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetailMethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
void U24ArrayTypeU248_t161_marshal(const U24ArrayTypeU248_t161& unmarshaled, U24ArrayTypeU248_t161_marshaled& marshaled)
{
}
void U24ArrayTypeU248_t161_marshal_back(const U24ArrayTypeU248_t161_marshaled& marshaled, U24ArrayTypeU248_t161& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
void U24ArrayTypeU248_t161_marshal_cleanup(U24ArrayTypeU248_t161_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementationDetail_0MethodDeclarations.h"



// System.Void <PrivateImplementationDetails>::.ctor()
extern "C" void U3CPrivateImplementationDetailsU3E__ctor_m829 (U3CPrivateImplementationDetailsU3E_t162 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
