﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
struct Enumerator_t4175;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t4170;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25647_gshared (Enumerator_t4175 * __this, Dictionary_2_t4170 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m25647(__this, ___dictionary, method) (( void (*) (Enumerator_t4175 *, Dictionary_2_t4170 *, MethodInfo*))Enumerator__ctor_m25647_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25648_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25648(__this, method) (( Object_t * (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25648_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650(__this, method) (( Object_t * (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25650_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651(__this, method) (( Object_t * (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25651_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25652_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m25652(__this, method) (( bool (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_MoveNext_m25652_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" KeyValuePair_2_t4171  Enumerator_get_Current_m25653_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_get_Current_m25653(__this, method) (( KeyValuePair_2_t4171  (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_get_Current_m25653_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m25654_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m25654(__this, method) (( Object_t * (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_get_CurrentKey_m25654_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentValue()
extern "C" uint8_t Enumerator_get_CurrentValue_m25655_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m25655(__this, method) (( uint8_t (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_get_CurrentValue_m25655_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m25656_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m25656(__this, method) (( void (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_VerifyState_m25656_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m25657_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m25657(__this, method) (( void (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_VerifyCurrent_m25657_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m25658_gshared (Enumerator_t4175 * __this, MethodInfo* method);
#define Enumerator_Dispose_m25658(__this, method) (( void (*) (Enumerator_t4175 *, MethodInfo*))Enumerator_Dispose_m25658_gshared)(__this, method)
