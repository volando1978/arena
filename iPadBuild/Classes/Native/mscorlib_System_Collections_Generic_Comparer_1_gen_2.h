﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>
struct Comparer_1_t3840;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>
struct  Comparer_1_t3840  : public Object_t
{
};
struct Comparer_1_t3840_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Matrix4x4>::_default
	Comparer_1_t3840 * ____default_0;
};
