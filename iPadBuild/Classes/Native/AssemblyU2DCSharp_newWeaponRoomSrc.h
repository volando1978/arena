﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t824;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// newWeaponRoomSrc
struct  newWeaponRoomSrc_t821  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle newWeaponRoomSrc::newWeapon
	GUIStyle_t724 * ___newWeapon_2;
	// UnityEngine.Rect newWeaponRoomSrc::messageRect
	Rect_t738  ___messageRect_3;
	// UnityEngine.Sprite[] newWeaponRoomSrc::weaponsSprites
	SpriteU5BU5D_t824* ___weaponsSprites_4;
	// UnityEngine.Rect newWeaponRoomSrc::halfScreen
	Rect_t738  ___halfScreen_6;
	// System.Single newWeaponRoomSrc::v
	float ___v_7;
};
struct newWeaponRoomSrc_t821_StaticFields{
	// System.Boolean newWeaponRoomSrc::isNewWeaponReady
	bool ___isNewWeaponReady_5;
};
