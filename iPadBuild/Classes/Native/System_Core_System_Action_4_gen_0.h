﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Void
#include "mscorlib_System_Void.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`4<System.Object,System.Object,System.Object,System.Byte>
struct  Action_4_t3738  : public MulticastDelegate_t22
{
};
