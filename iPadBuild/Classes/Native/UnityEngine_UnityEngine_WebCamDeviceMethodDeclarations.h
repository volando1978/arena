﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamDevice
struct WebCamDevice_t1585;
struct WebCamDevice_t1585_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m7174 (WebCamDevice_t1585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m7175 (WebCamDevice_t1585 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void WebCamDevice_t1585_marshal(const WebCamDevice_t1585& unmarshaled, WebCamDevice_t1585_marshaled& marshaled);
void WebCamDevice_t1585_marshal_back(const WebCamDevice_t1585_marshaled& marshaled, WebCamDevice_t1585& unmarshaled);
void WebCamDevice_t1585_marshal_cleanup(WebCamDevice_t1585_marshaled& marshaled);
