﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// enemyController
struct enemyController_t785;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// tutorialController
struct  tutorialController_t837  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject tutorialController::pod
	GameObject_t144 * ___pod_2;
	// UnityEngine.Vector2 tutorialController::podPos
	Vector2_t739  ___podPos_3;
	// UnityEngine.GameObject tutorialController::hole
	GameObject_t144 * ___hole_4;
	// UnityEngine.GameObject tutorialController::bomba
	GameObject_t144 * ___bomba_5;
	// UnityEngine.GameObject tutorialController::coin
	GameObject_t144 * ___coin_6;
	// UnityEngine.GameObject tutorialController::enemyControllerObj
	GameObject_t144 * ___enemyControllerObj_7;
	// enemyController tutorialController::_enemyController
	enemyController_t785 * ____enemyController_8;
	// UnityEngine.GameObject tutorialController::player
	GameObject_t144 * ___player_9;
	// UnityEngine.GameObject tutorialController::bocadilloObj
	GameObject_t144 * ___bocadilloObj_10;
	// UnityEngine.Vector2 tutorialController::bocaPos
	Vector2_t739  ___bocaPos_11;
	// System.Single tutorialController::size
	float ___size_12;
	// System.Boolean tutorialController::created
	bool ___created_13;
	// System.Boolean tutorialController::endTutorial
	bool ___endTutorial_14;
	// System.Boolean tutorialController::failedTutorial
	bool ___failedTutorial_15;
	// UnityEngine.GameObject tutorialController::bocadillo
	GameObject_t144 * ___bocadillo_16;
};
