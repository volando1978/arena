﻿#pragma once
#include <stdint.h>
// System.Func`2<System.UIntPtr,System.Object>
struct Func_2_t935;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>
struct  U3CToEnumerableU3Ec__Iterator0_1_t3776  : public Object_t
{
	// System.UInt64 GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::<i>__0
	uint64_t ___U3CiU3E__0_0;
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::size
	UIntPtr_t  ___size_1;
	// System.Func`2<System.UIntPtr,T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::getElement
	Func_2_t935 * ___getElement_2;
	// System.Int32 GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::$PC
	int32_t ___U24PC_3;
	// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::$current
	Object_t * ___U24current_4;
	// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::<$>size
	UIntPtr_t  ___U3CU24U3Esize_5;
	// System.Func`2<System.UIntPtr,T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::<$>getElement
	Func_2_t935 * ___U3CU24U3EgetElement_6;
};
