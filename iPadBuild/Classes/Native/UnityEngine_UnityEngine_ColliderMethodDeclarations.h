﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider
struct Collider_t900;
// UnityEngine.Rigidbody
struct Rigidbody_t994;

// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C" Rigidbody_t994 * Collider_get_attachedRigidbody_m7148 (Collider_t900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
