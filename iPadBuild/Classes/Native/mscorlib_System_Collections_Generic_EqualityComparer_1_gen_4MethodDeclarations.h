﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.IntPtr>
struct EqualityComparer_1_t3812;
// System.Object
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.ctor()
extern "C" void EqualityComparer_1__ctor_m20851_gshared (EqualityComparer_1_t3812 * __this, MethodInfo* method);
#define EqualityComparer_1__ctor_m20851(__this, method) (( void (*) (EqualityComparer_1_t3812 *, MethodInfo*))EqualityComparer_1__ctor_m20851_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.IntPtr>::.cctor()
extern "C" void EqualityComparer_1__cctor_m20852_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1__cctor_m20852(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1__cctor_m20852_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20853_gshared (EqualityComparer_1_t3812 * __this, Object_t * ___obj, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20853(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3812 *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20853_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20854_gshared (EqualityComparer_1_t3812 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20854(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3812 *, Object_t *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20854_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.IntPtr>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.IntPtr>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.IntPtr>::get_Default()
extern "C" EqualityComparer_1_t3812 * EqualityComparer_1_get_Default_m20855_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1_get_Default_m20855(__this /* static, unused */, method) (( EqualityComparer_1_t3812 * (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1_get_Default_m20855_gshared)(__this /* static, unused */, method)
