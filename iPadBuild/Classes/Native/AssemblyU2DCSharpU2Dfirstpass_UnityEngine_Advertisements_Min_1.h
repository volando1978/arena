﻿#pragma once
#include <stdint.h>
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.MiniJSON.Json/Serializer
struct  Serializer_t149  : public Object_t
{
	// System.Text.StringBuilder UnityEngine.Advertisements.MiniJSON.Json/Serializer::builder
	StringBuilder_t36 * ___builder_0;
};
