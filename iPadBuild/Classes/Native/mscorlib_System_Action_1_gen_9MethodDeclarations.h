﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
struct Action_1_t847;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_1.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m19222_gshared (Action_1_t847 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m19222(__this, ___object, ___method, method) (( void (*) (Action_1_t847 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m19222_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::Invoke(T)
extern "C" void Action_1_Invoke_m19223_gshared (Action_1_t847 * __this, ConnectionResponse_t358  ___obj, MethodInfo* method);
#define Action_1_Invoke_m19223(__this, ___obj, method) (( void (*) (Action_1_t847 *, ConnectionResponse_t358 , MethodInfo*))Action_1_Invoke_m19223_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m19224_gshared (Action_1_t847 * __this, ConnectionResponse_t358  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m19224(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t847 *, ConnectionResponse_t358 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m19224_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m19225_gshared (Action_1_t847 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m19225(__this, ___result, method) (( void (*) (Action_1_t847 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m19225_gshared)(__this, ___result, method)
