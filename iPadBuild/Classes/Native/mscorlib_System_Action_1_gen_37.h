﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t703;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse>
struct  Action_1_t896  : public MulticastDelegate_t22
{
};
