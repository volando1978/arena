﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t1226;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t1232;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1267;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m5869(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t1226 *, UnityAction_1_t1232 *, UnityAction_1_t1232 *, MethodInfo*))ObjectPool_1__ctor_m21655_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m22886(__this, method) (( int32_t (*) (ObjectPool_1_t1226 *, MethodInfo*))ObjectPool_1_get_countAll_m21657_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m22887(__this, ___value, method) (( void (*) (ObjectPool_1_t1226 *, int32_t, MethodInfo*))ObjectPool_1_set_countAll_m21659_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m22888(__this, method) (( int32_t (*) (ObjectPool_1_t1226 *, MethodInfo*))ObjectPool_1_get_countActive_m21661_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m22889(__this, method) (( int32_t (*) (ObjectPool_1_t1226 *, MethodInfo*))ObjectPool_1_get_countInactive_m21663_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m5875(__this, method) (( List_1_t1267 * (*) (ObjectPool_1_t1226 *, MethodInfo*))ObjectPool_1_Get_m21665_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m5878(__this, ___element, method) (( void (*) (ObjectPool_1_t1226 *, List_1_t1267 *, MethodInfo*))ObjectPool_1_Release_m21667_gshared)(__this, ___element, method)
