﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse
struct CommitResponse_t703;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::.ctor(System.IntPtr)
extern "C" void CommitResponse__ctor_m3027 (CommitResponse_t703 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::ResponseStatus()
extern "C" int32_t CommitResponse_ResponseStatus_m3028 (CommitResponse_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::RequestSucceeded()
extern "C" bool CommitResponse_RequestSucceeded_m3029 (CommitResponse_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::Data()
extern "C" NativeSnapshotMetadata_t611 * CommitResponse_Data_m3030 (CommitResponse_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void CommitResponse_CallDispose_m3031 (CommitResponse_t703 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse GooglePlayGames.Native.PInvoke.SnapshotManager/CommitResponse::FromPointer(System.IntPtr)
extern "C" CommitResponse_t703 * CommitResponse_FromPointer_m3032 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
