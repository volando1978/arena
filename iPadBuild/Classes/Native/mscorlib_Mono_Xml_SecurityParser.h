﻿#pragma once
#include <stdint.h>
// System.Security.SecurityElement
struct SecurityElement_t2419;
// System.Collections.Stack
struct Stack_t1661;
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Mono.Xml.SecurityParser
struct  SecurityParser_t2420  : public SmallXmlParser_t2421
{
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t2419 * ___root_13;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t2419 * ___current_14;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t1661 * ___stack_15;
};
