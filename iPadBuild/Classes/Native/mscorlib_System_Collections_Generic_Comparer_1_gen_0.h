﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparer_1_t3449;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  Comparer_1_t3449  : public Object_t
{
};
struct Comparer_1_t3449_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::_default
	Comparer_1_t3449 * ____default_0;
};
