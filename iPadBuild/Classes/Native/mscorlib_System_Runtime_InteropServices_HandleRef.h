﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t657 
{
	// System.Object System.Runtime.InteropServices.HandleRef::wrapper
	Object_t * ___wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::handle
	IntPtr_t ___handle_1;
};
