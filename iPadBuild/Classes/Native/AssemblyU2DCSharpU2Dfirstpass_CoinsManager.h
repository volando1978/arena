﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CoinsManager
struct  CoinsManager_t72  : public MonoBehaviour_t26
{
	// System.Int32 CoinsManager::t
	int32_t ___t_6;
};
struct CoinsManager_t72_StaticFields{
	// System.Int32 CoinsManager::coins
	int32_t ___coins_2;
	// System.Int32 CoinsManager::earnedCoins
	int32_t ___earnedCoins_3;
	// System.Boolean CoinsManager::isPurchasesRestored
	bool ___isPurchasesRestored_4;
	// System.Boolean CoinsManager::isAddingCoins
	bool ___isAddingCoins_5;
};
