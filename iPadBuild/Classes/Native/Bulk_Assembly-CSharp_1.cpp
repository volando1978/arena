﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_2.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_2MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ClaimMilestoneCallback__ctor_m1881 (ClaimMilestoneCallback_t447 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void ClaimMilestoneCallback_Invoke_m1882 (ClaimMilestoneCallback_t447 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ClaimMilestoneCallback_Invoke_m1882((ClaimMilestoneCallback_t447 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ClaimMilestoneCallback_t447(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ClaimMilestoneCallback_BeginInvoke_m1883 (ClaimMilestoneCallback_t447 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::EndInvoke(System.IAsyncResult)
extern "C" void ClaimMilestoneCallback_EndInvoke_m1884 (ClaimMilestoneCallback_t447 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void QuestUICallback__ctor_m1885 (QuestUICallback_t448 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void QuestUICallback_Invoke_m1886 (QuestUICallback_t448 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		QuestUICallback_Invoke_m1886((QuestUICallback_t448 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_QuestUICallback_t448(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * QuestUICallback_BeginInvoke_m1887 (QuestUICallback_t448 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback::EndInvoke(System.IAsyncResult)
extern "C" void QuestUICallback_EndInvoke_m1888 (QuestUICallback_t448 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.QuestManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_4MethodDeclarations.h"

// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_0.h"
// GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag_1.h"
// System.String
#include "mscorlib_System_String.h"
// GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestManag.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"


// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchList(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.Int32,GooglePlayGames.Native.Cwrapper.QuestManager/FetchListCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_FetchList(void*, int32_t, int32_t, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_FetchList_m1889 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, int32_t ___fetch_flags, FetchListCallback_t445 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchList;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchList'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___fetch_flags' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ___fetch_flags, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___fetch_flags' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_Accept(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/AcceptCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_Accept(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_Accept_m1890 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___quest, AcceptCallback_t446 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_Accept;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_Accept'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___quest' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___quest, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___quest' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_ShowAllUI(void*, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_ShowAllUI_m1891 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, QuestUICallback_t448 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ShowAllUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ShowAllUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ShowUI(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/QuestUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_ShowUI(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_ShowUI_m1892 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___quest, QuestUICallback_t448 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ShowUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ShowUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___quest' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___quest, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___quest' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestone(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_ClaimMilestone(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_ClaimMilestone_m1893 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___milestone, ClaimMilestoneCallback_t447 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ClaimMilestone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ClaimMilestone'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___milestone' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___milestone, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___milestone' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.QuestManager/FetchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL QuestManager_Fetch(void*, int32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void QuestManager_QuestManager_Fetch_m1894 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___quest_id, FetchCallback_t444 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_Fetch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_Fetch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___quest_id' to native representation
	char* ____quest_id_marshaled = { 0 };
	____quest_id_marshaled = il2cpp_codegen_marshal_string(___quest_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____quest_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___quest_id' native representation
	il2cpp_codegen_marshal_free(____quest_id_marshaled);
	____quest_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestManager_FetchResponse_Dispose(void*);}
extern "C" void QuestManager_QuestManager_FetchResponse_Dispose_m1895 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestManager_FetchResponse_GetStatus(void*);}
extern "C" int32_t QuestManager_QuestManager_FetchResponse_GetStatus_m1896 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_FetchResponse_GetData(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_FetchResponse_GetData_m1897 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestManager_FetchListResponse_Dispose(void*);}
extern "C" void QuestManager_QuestManager_FetchListResponse_Dispose_m1898 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchListResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchListResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestManager_FetchListResponse_GetStatus(void*);}
extern "C" int32_t QuestManager_QuestManager_FetchListResponse_GetStatus_m1899 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchListResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchListResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL QuestManager_FetchListResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  QuestManager_QuestManager_FetchListResponse_GetData_Length_m1900 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchListResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchListResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_FetchListResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_FetchListResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t QuestManager_QuestManager_FetchListResponse_GetData_GetElement_m1901 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_FetchListResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_FetchListResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestManager_AcceptResponse_Dispose(void*);}
extern "C" void QuestManager_QuestManager_AcceptResponse_Dispose_m1902 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_AcceptResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_AcceptResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestManager_AcceptResponse_GetStatus(void*);}
extern "C" int32_t QuestManager_QuestManager_AcceptResponse_GetStatus_m1903 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_AcceptResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_AcceptResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_AcceptResponse_GetAcceptedQuest(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_AcceptResponse_GetAcceptedQuest(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_AcceptResponse_GetAcceptedQuest_m1904 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_AcceptResponse_GetAcceptedQuest;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_AcceptResponse_GetAcceptedQuest'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestManager_ClaimMilestoneResponse_Dispose(void*);}
extern "C" void QuestManager_QuestManager_ClaimMilestoneResponse_Dispose_m1905 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ClaimMilestoneResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ClaimMilestoneResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetStatus(void*);}
extern "C" int32_t QuestManager_QuestManager_ClaimMilestoneResponse_GetStatus_m1906 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ClaimMilestoneResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ClaimMilestoneResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetClaimedMilestone(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_ClaimMilestoneResponse_GetClaimedMilestone_m1907 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ClaimMilestoneResponse_GetClaimedMilestone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ClaimMilestoneResponse_GetClaimedMilestone'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_ClaimMilestoneResponse_GetQuest(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_ClaimMilestoneResponse_GetQuest(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_ClaimMilestoneResponse_GetQuest_m1908 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_ClaimMilestoneResponse_GetQuest;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_ClaimMilestoneResponse_GetQuest'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestManager_QuestUIResponse_Dispose(void*);}
extern "C" void QuestManager_QuestManager_QuestUIResponse_Dispose_m1909 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_QuestUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_QuestUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestManager_QuestUIResponse_GetStatus(void*);}
extern "C" int32_t QuestManager_QuestManager_QuestUIResponse_GetStatus_m1910 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_QuestUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_QuestUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetAcceptedQuest(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_QuestUIResponse_GetAcceptedQuest(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_QuestUIResponse_GetAcceptedQuest_m1911 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_QuestUIResponse_GetAcceptedQuest;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_QuestUIResponse_GetAcceptedQuest'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestManager::QuestManager_QuestUIResponse_GetMilestoneToClaim(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestManager_QuestUIResponse_GetMilestoneToClaim(void*);}
extern "C" IntPtr_t QuestManager_QuestManager_QuestUIResponse_GetMilestoneToClaim_m1912 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestManager_QuestUIResponse_GetMilestoneToClaim;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestManager_QuestUIResponse_GetMilestoneToClaim'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.QuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestMiles.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.QuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_QuestMilesMethodDeclarations.h"

// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
#include "mscorlib_ArrayTypes.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"


// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_EventId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL QuestMilestone_EventId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_EventId_m1913 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_EventId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_EventId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CurrentCount(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL QuestMilestone_CurrentCount(void*);}
extern "C" uint64_t QuestMilestone_QuestMilestone_CurrentCount_m1914 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_CurrentCount;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_CurrentCount'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Copy(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL QuestMilestone_Copy(void*);}
extern "C" IntPtr_t QuestMilestone_QuestMilestone_Copy_m1915 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_Copy;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_Copy'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL QuestMilestone_Dispose(void*);}
extern "C" void QuestMilestone_QuestMilestone_Dispose_m1916 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UInt64 GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_TargetCount(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL QuestMilestone_TargetCount(void*);}
extern "C" uint64_t QuestMilestone_QuestMilestone_TargetCount_m1917 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_TargetCount;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_TargetCount'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_QuestId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL QuestMilestone_QuestId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_QuestId_m1918 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_QuestId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_QuestId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_CompletionRewardData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL QuestMilestone_CompletionRewardData(void*, uint8_t*, UIntPtr_t );}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_CompletionRewardData_m1919 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_CompletionRewardData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_CompletionRewardData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	uint8_t* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	___out_arg = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, ____out_arg_marshaled, 1);

	// Marshaling cleanup of parameter '___out_arg' native representation

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_State(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL QuestMilestone_State(void*);}
extern "C" int32_t QuestMilestone_QuestMilestone_State_m1920 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_State;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_State'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL QuestMilestone_Valid(void*);}
extern "C" bool QuestMilestone_QuestMilestone_Valid_m1921 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.QuestMilestone::QuestMilestone_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL QuestMilestone_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  QuestMilestone_QuestMilestone_Id_m1922 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)QuestMilestone_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'QuestMilestone_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEvMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnRoomStatusChangedCallback__ctor_m1923 (OnRoomStatusChangedCallback_t451 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OnRoomStatusChangedCallback_Invoke_m1924 (OnRoomStatusChangedCallback_t451 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnRoomStatusChangedCallback_Invoke_m1924((OnRoomStatusChangedCallback_t451 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnRoomStatusChangedCallback_t451(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnRoomStatusChangedCallback_BeginInvoke_m1925 (OnRoomStatusChangedCallback_t451 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnRoomStatusChangedCallback_EndInvoke_m1926 (OnRoomStatusChangedCallback_t451 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnRoomConnectedSetChangedCallback__ctor_m1927 (OnRoomConnectedSetChangedCallback_t452 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OnRoomConnectedSetChangedCallback_Invoke_m1928 (OnRoomConnectedSetChangedCallback_t452 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnRoomConnectedSetChangedCallback_Invoke_m1928((OnRoomConnectedSetChangedCallback_t452 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnRoomConnectedSetChangedCallback_t452(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnRoomConnectedSetChangedCallback_BeginInvoke_m1929 (OnRoomConnectedSetChangedCallback_t452 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnRoomConnectedSetChangedCallback_EndInvoke_m1930 (OnRoomConnectedSetChangedCallback_t452 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnP2PConnectedCallback__ctor_m1931 (OnP2PConnectedCallback_t453 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void OnP2PConnectedCallback_Invoke_m1932 (OnP2PConnectedCallback_t453 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnP2PConnectedCallback_Invoke_m1932((OnP2PConnectedCallback_t453 *)__this->___prev_9,___arg0, ___arg1, ___arg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnP2PConnectedCallback_t453(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Marshaling of parameter '___arg2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1, ___arg2);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

	// Marshaling cleanup of parameter '___arg2' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnP2PConnectedCallback_BeginInvoke_m1933 (OnP2PConnectedCallback_t453 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnP2PConnectedCallback_EndInvoke_m1934 (OnP2PConnectedCallback_t453 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnP2PDisconnectedCallback__ctor_m1935 (OnP2PDisconnectedCallback_t454 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void OnP2PDisconnectedCallback_Invoke_m1936 (OnP2PDisconnectedCallback_t454 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnP2PDisconnectedCallback_Invoke_m1936((OnP2PDisconnectedCallback_t454 *)__this->___prev_9,___arg0, ___arg1, ___arg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnP2PDisconnectedCallback_t454(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Marshaling of parameter '___arg2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1, ___arg2);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

	// Marshaling cleanup of parameter '___arg2' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnP2PDisconnectedCallback_BeginInvoke_m1937 (OnP2PDisconnectedCallback_t454 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnP2PDisconnectedCallback_EndInvoke_m1938 (OnP2PDisconnectedCallback_t454 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnParticipantStatusChangedCallback__ctor_m1939 (OnParticipantStatusChangedCallback_t455 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void OnParticipantStatusChangedCallback_Invoke_m1940 (OnParticipantStatusChangedCallback_t455 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnParticipantStatusChangedCallback_Invoke_m1940((OnParticipantStatusChangedCallback_t455 *)__this->___prev_9,___arg0, ___arg1, ___arg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnParticipantStatusChangedCallback_t455(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Marshaling of parameter '___arg2' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1, ___arg2);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

	// Marshaling cleanup of parameter '___arg2' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OnParticipantStatusChangedCallback_BeginInvoke_m1941 (OnParticipantStatusChangedCallback_t455 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnParticipantStatusChangedCallback_EndInvoke_m1942 (OnParticipantStatusChangedCallback_t455 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_4MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnDataReceivedCallback__ctor_m1943 (OnDataReceivedCallback_t456 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr)
extern "C" void OnDataReceivedCallback_Invoke_m1944 (OnDataReceivedCallback_t456 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnDataReceivedCallback_Invoke_m1944((OnDataReceivedCallback_t456 *)__this->___prev_9,___arg0, ___arg1, ___arg2, ___arg3, ___arg4, ___arg5, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3, ___arg4, ___arg5,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1, ___arg2, ___arg3, ___arg4, ___arg5,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnDataReceivedCallback_t456(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t, IntPtr_t, UIntPtr_t , int8_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Marshaling of parameter '___arg2' to native representation

	// Marshaling of parameter '___arg3' to native representation

	// Marshaling of parameter '___arg4' to native representation

	// Marshaling of parameter '___arg5' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1, ___arg2, ___arg3, ___arg4, ___arg5);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

	// Marshaling cleanup of parameter '___arg2' native representation

	// Marshaling cleanup of parameter '___arg3' native representation

	// Marshaling cleanup of parameter '___arg4' native representation

	// Marshaling cleanup of parameter '___arg5' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* UIntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t203_il2cpp_TypeInfo_var;
extern "C" Object_t * OnDataReceivedCallback_BeginInvoke_m1945 (OnDataReceivedCallback_t456 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		UIntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		Boolean_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[7] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	__d_args[2] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg2);
	__d_args[3] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___arg3);
	__d_args[4] = Box(Boolean_t203_il2cpp_TypeInfo_var, &___arg4);
	__d_args[5] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg5);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnDataReceivedCallback_EndInvoke_m1946 (OnDataReceivedCallback_t456 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeEv_5MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnParticipantStatusChangedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback_m1947 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnParticipantStatusChangedCallback_t455 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnParticipantStatusChangedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Construct()
extern "C" {IntPtr_t DEFAULT_CALL RealTimeEventListenerHelper_Construct();}
extern "C" IntPtr_t RealTimeEventListenerHelper_RealTimeEventListenerHelper_Construct_m1948 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PDisconnectedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback_m1949 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnP2PDisconnectedCallback_t454 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnP2PDisconnectedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnDataReceivedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnDataReceivedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnDataReceivedCallback_m1950 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnDataReceivedCallback_t456 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnDataReceivedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnDataReceivedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomStatusChangedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback_m1951 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnRoomStatusChangedCallback_t451 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnRoomStatusChangedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnP2PConnectedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnP2PConnectedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnP2PConnectedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnP2PConnectedCallback_m1952 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnP2PConnectedCallback_t453 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnP2PConnectedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnP2PConnectedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnRoomConnectedSetChangedCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback_m1953 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, OnRoomConnectedSetChangedCallback_t452 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_SetOnRoomConnectedSetChangedCallback'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper::RealTimeEventListenerHelper_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeEventListenerHelper_Dispose(void*);}
extern "C" void RealTimeEventListenerHelper_RealTimeEventListenerHelper_Dispose_m1954 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeEventListenerHelper_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeEventListenerHelper_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMuMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C" void RealTimeRoomCallback__ctor_m1955 (RealTimeRoomCallback_t458 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void RealTimeRoomCallback_Invoke_m1956 (RealTimeRoomCallback_t458 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RealTimeRoomCallback_Invoke_m1956((RealTimeRoomCallback_t458 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_RealTimeRoomCallback_t458(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * RealTimeRoomCallback_BeginInvoke_m1957 (RealTimeRoomCallback_t458 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback::EndInvoke(System.IAsyncResult)
extern "C" void RealTimeRoomCallback_EndInvoke_m1958 (RealTimeRoomCallback_t458 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LeaveRoomCallback__ctor_m1959 (LeaveRoomCallback_t459 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr)
extern "C" void LeaveRoomCallback_Invoke_m1960 (LeaveRoomCallback_t459 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LeaveRoomCallback_Invoke_m1960((LeaveRoomCallback_t459 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LeaveRoomCallback_t459(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * LeaveRoomCallback_BeginInvoke_m1961 (LeaveRoomCallback_t459 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback::EndInvoke(System.IAsyncResult)
extern "C" void LeaveRoomCallback_EndInvoke_m1962 (LeaveRoomCallback_t459 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_1MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"


// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::.ctor(System.Object,System.IntPtr)
extern "C" void SendReliableMessageCallback__ctor_m1963 (SendReliableMessageCallback_t460 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern "C" void SendReliableMessageCallback_Invoke_m1964 (SendReliableMessageCallback_t460 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SendReliableMessageCallback_Invoke_m1964((SendReliableMessageCallback_t460 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SendReliableMessageCallback_t460(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* MultiplayerStatus_t413_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * SendReliableMessageCallback_BeginInvoke_m1965 (SendReliableMessageCallback_t460 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerStatus_t413_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(MultiplayerStatus_t413_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback::EndInvoke(System.IAsyncResult)
extern "C" void SendReliableMessageCallback_EndInvoke_m1966 (SendReliableMessageCallback_t460 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void RoomInboxUICallback__ctor_m1967 (RoomInboxUICallback_t461 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void RoomInboxUICallback_Invoke_m1968 (RoomInboxUICallback_t461 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		RoomInboxUICallback_Invoke_m1968((RoomInboxUICallback_t461 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_RoomInboxUICallback_t461(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * RoomInboxUICallback_BeginInvoke_m1969 (RoomInboxUICallback_t461 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback::EndInvoke(System.IAsyncResult)
extern "C" void RoomInboxUICallback_EndInvoke_m1970 (RoomInboxUICallback_t461 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void PlayerSelectUICallback__ctor_m1971 (PlayerSelectUICallback_t462 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void PlayerSelectUICallback_Invoke_m1972 (PlayerSelectUICallback_t462 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PlayerSelectUICallback_Invoke_m1972((PlayerSelectUICallback_t462 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PlayerSelectUICallback_t462(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayerSelectUICallback_BeginInvoke_m1973 (PlayerSelectUICallback_t462 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C" void PlayerSelectUICallback_EndInvoke_m1974 (PlayerSelectUICallback_t462 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_4MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void WaitingRoomUICallback__ctor_m1975 (WaitingRoomUICallback_t463 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void WaitingRoomUICallback_Invoke_m1976 (WaitingRoomUICallback_t463 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WaitingRoomUICallback_Invoke_m1976((WaitingRoomUICallback_t463 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WaitingRoomUICallback_t463(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * WaitingRoomUICallback_BeginInvoke_m1977 (WaitingRoomUICallback_t463 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback::EndInvoke(System.IAsyncResult)
extern "C" void WaitingRoomUICallback_EndInvoke_m1978 (WaitingRoomUICallback_t463 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_5MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchInvitationsCallback__ctor_m1979 (FetchInvitationsCallback_t464 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchInvitationsCallback_Invoke_m1980 (FetchInvitationsCallback_t464 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchInvitationsCallback_Invoke_m1980((FetchInvitationsCallback_t464 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchInvitationsCallback_t464(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchInvitationsCallback_BeginInvoke_m1981 (FetchInvitationsCallback_t464 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchInvitationsCallback_EndInvoke_m1982 (FetchInvitationsCallback_t464 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeMu_6MethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"


// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_CreateRealTimeRoom(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_CreateRealTimeRoom(void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_CreateRealTimeRoom_m1983 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___config, IntPtr_t ___helper, RealTimeRoomCallback_t458 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_CreateRealTimeRoom;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_CreateRealTimeRoom'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___config' to native representation

	// Marshaling of parameter '___helper' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___config, ___helper, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___config' native representation

	// Marshaling cleanup of parameter '___helper' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_LeaveRoom(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/LeaveRoomCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_LeaveRoom(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_LeaveRoom_m1984 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___room, LeaveRoomCallback_t459 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_LeaveRoom;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_LeaveRoom'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___room' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___room, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___room' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendUnreliableMessage(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr[],System.UIntPtr,System.Byte[],System.UIntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_SendUnreliableMessage(void*, IntPtr_t, IntPtr_t*, UIntPtr_t , uint8_t*, UIntPtr_t );}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendUnreliableMessage_m1985 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___room, IntPtrU5BU5D_t859* ___participants, UIntPtr_t  ___participants_size, ByteU5BU5D_t350* ___data, UIntPtr_t  ___data_size, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t*, UIntPtr_t , uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_SendUnreliableMessage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_SendUnreliableMessage'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___room' to native representation

	// Marshaling of parameter '___participants' to native representation
	IntPtr_t* ____participants_marshaled = { 0 };
	____participants_marshaled = il2cpp_codegen_marshal_array<IntPtr_t>((Il2CppCodeGenArray*)___participants);

	// Marshaling of parameter '___participants_size' to native representation

	// Marshaling of parameter '___data' to native representation
	uint8_t* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data);

	// Marshaling of parameter '___data_size' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___room, ____participants_marshaled, ___participants_size, ____data_marshaled, ___data_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___room' native representation

	// Marshaling cleanup of parameter '___participants' native representation

	// Marshaling cleanup of parameter '___participants_size' native representation

	// Marshaling cleanup of parameter '___data' native representation

	// Marshaling cleanup of parameter '___data_size' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowWaitingRoomUI(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.UInt32,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/WaitingRoomUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_ShowWaitingRoomUI(void*, IntPtr_t, uint32_t, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowWaitingRoomUI_m1986 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___room, uint32_t ___min_participants_to_start, WaitingRoomUICallback_t463 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, uint32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_ShowWaitingRoomUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_ShowWaitingRoomUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___room' to native representation

	// Marshaling of parameter '___min_participants_to_start' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___room, ___min_participants_to_start, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___room' native representation

	// Marshaling cleanup of parameter '___min_participants_to_start' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowPlayerSelectUI(System.Runtime.InteropServices.HandleRef,System.UInt32,System.UInt32,System.Boolean,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/PlayerSelectUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_ShowPlayerSelectUI(void*, uint32_t, uint32_t, int8_t, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowPlayerSelectUI_m1987 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_players, uint32_t ___maximum_players, bool ___allow_automatch, PlayerSelectUICallback_t462 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t, uint32_t, int8_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_ShowPlayerSelectUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_ShowPlayerSelectUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___minimum_players' to native representation

	// Marshaling of parameter '___maximum_players' to native representation

	// Marshaling of parameter '___allow_automatch' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___minimum_players, ___maximum_players, ___allow_automatch, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___minimum_players' native representation

	// Marshaling cleanup of parameter '___maximum_players' native representation

	// Marshaling cleanup of parameter '___allow_automatch' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_DismissInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_DismissInvitation(void*, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_DismissInvitation_m1988 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_DismissInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_DismissInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_DeclineInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_DeclineInvitation(void*, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_DeclineInvitation_m1989 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_DeclineInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_DeclineInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendReliableMessage(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.Byte[],System.UIntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/SendReliableMessageCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_SendReliableMessage(void*, IntPtr_t, IntPtr_t, uint8_t*, UIntPtr_t , methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendReliableMessage_m1990 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___room, IntPtr_t ___participant, ByteU5BU5D_t350* ___data, UIntPtr_t  ___data_size, SendReliableMessageCallback_t460 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, uint8_t*, UIntPtr_t , methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_SendReliableMessage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_SendReliableMessage'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___room' to native representation

	// Marshaling of parameter '___participant' to native representation

	// Marshaling of parameter '___data' to native representation
	uint8_t* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data);

	// Marshaling of parameter '___data_size' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___room, ___participant, ____data_marshaled, ___data_size, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___room' native representation

	// Marshaling cleanup of parameter '___participant' native representation

	// Marshaling cleanup of parameter '___data' native representation

	// Marshaling cleanup of parameter '___data_size' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_AcceptInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RealTimeRoomCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_AcceptInvitation(void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_AcceptInvitation_m1991 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, IntPtr_t ___helper, RealTimeRoomCallback_t458 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_AcceptInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_AcceptInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Marshaling of parameter '___helper' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation, ___helper, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

	// Marshaling cleanup of parameter '___helper' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitations(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/FetchInvitationsCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitations(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitations_m1992 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, FetchInvitationsCallback_t464 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_FetchInvitations;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_FetchInvitations'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_SendUnreliableMessageToOthers(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_SendUnreliableMessageToOthers(void*, IntPtr_t, uint8_t*, UIntPtr_t );}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_SendUnreliableMessageToOthers_m1993 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___room, ByteU5BU5D_t350* ___data, UIntPtr_t  ___data_size, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_SendUnreliableMessageToOthers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_SendUnreliableMessageToOthers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___room' to native representation

	// Marshaling of parameter '___data' to native representation
	uint8_t* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data);

	// Marshaling of parameter '___data_size' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___room, ____data_marshaled, ___data_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___room' native representation

	// Marshaling cleanup of parameter '___data' native representation

	// Marshaling cleanup of parameter '___data_size' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_ShowRoomInboxUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager/RoomInboxUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_ShowRoomInboxUI(void*, methodPointerType, IntPtr_t);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_ShowRoomInboxUI_m1994 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, RoomInboxUICallback_t461 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_ShowRoomInboxUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_ShowRoomInboxUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose(void*);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose_m1995 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RealTimeRoomResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus(void*);}
extern "C" int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus_m1996 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RealTimeRoomResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom(void*);}
extern "C" IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom_m1997 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RealTimeRoomResponse_GetRoom'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose(void*);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose_m1998 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RoomInboxUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus(void*);}
extern "C" int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus_m1999 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RoomInboxUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation(void*);}
extern "C" IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation_m2000 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_RoomInboxUIResponse_GetInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose(void*);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose_m2001 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_WaitingRoomUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus(void*);}
extern "C" int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus_m2002 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_WaitingRoomUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom(void*);}
extern "C" IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom_m2003 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_WaitingRoomUIResponse_GetRoom'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose(void*);}
extern "C" void RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose_m2004 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_FetchInvitationsResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus(void*);}
extern "C" int32_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus_m2005 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_FetchInvitationsResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length(void*);}
extern "C" UIntPtr_t  RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length_m2006 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeMultiplayerManager::RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t RealTimeMultiplayerManager_RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement_m2007 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeMultiplayerManager_FetchInvitationsResponse_GetInvitations_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.RealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRo.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRoMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"


// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Status(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL RealTimeRoom_Status(void*);}
extern "C" int32_t RealTimeRoom_RealTimeRoom_Status_m2008 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Status;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Status'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeRoom_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Description_m2009 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL RealTimeRoom_Variant(void*);}
extern "C" uint32_t RealTimeRoom_RealTimeRoom_Variant_m2010 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Variant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Variant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL RealTimeRoom_CreationTime(void*);}
extern "C" uint64_t RealTimeRoom_RealTimeRoom_CreationTime_m2011 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_CreationTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_CreationTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeRoom_Participants_Length(void*);}
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Participants_Length_m2012 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Participants_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Participants_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeRoom_Participants_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t RealTimeRoom_RealTimeRoom_Participants_GetElement_m2013 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Participants_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Participants_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL RealTimeRoom_Valid(void*);}
extern "C" bool RealTimeRoom_RealTimeRoom_Valid_m2014 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_RemainingAutomatchingSlots(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL RealTimeRoom_RemainingAutomatchingSlots(void*);}
extern "C" uint32_t RealTimeRoom_RealTimeRoom_RemainingAutomatchingSlots_m2015 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_RemainingAutomatchingSlots;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_RemainingAutomatchingSlots'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_AutomatchWaitEstimate(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL RealTimeRoom_AutomatchWaitEstimate(void*);}
extern "C" uint64_t RealTimeRoom_RealTimeRoom_AutomatchWaitEstimate_m2016 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_AutomatchWaitEstimate;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_AutomatchWaitEstimate'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeRoom_CreatingParticipant(void*);}
extern "C" IntPtr_t RealTimeRoom_RealTimeRoom_CreatingParticipant_m2017 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_CreatingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_CreatingParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeRoom_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  RealTimeRoom_RealTimeRoom_Id_m2018 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoom::RealTimeRoom_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeRoom_Dispose(void*);}
extern "C" void RealTimeRoom_RealTimeRoom_Dispose_m2019 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoom_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoom_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRo_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRo_0MethodDeclarations.h"

// System.Int64
#include "mscorlib_System_Int64.h"


// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeRoomConfig_PlayerIdsToInvite_Length(void*);}
extern "C" UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_Length_m2020 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_PlayerIdsToInvite_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_PlayerIdsToInvite_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL RealTimeRoomConfig_PlayerIdsToInvite_GetElement(void*, UIntPtr_t , char*, UIntPtr_t );}
extern "C" UIntPtr_t  RealTimeRoomConfig_RealTimeRoomConfig_PlayerIdsToInvite_GetElement_m2021 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t , char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_PlayerIdsToInvite_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_PlayerIdsToInvite_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL RealTimeRoomConfig_Variant(void*);}
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_Variant_m2022 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Variant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Variant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL RealTimeRoomConfig_ExclusiveBitMask(void*);}
extern "C" int64_t RealTimeRoomConfig_RealTimeRoomConfig_ExclusiveBitMask_m2023 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_ExclusiveBitMask;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_ExclusiveBitMask'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL RealTimeRoomConfig_Valid(void*);}
extern "C" bool RealTimeRoomConfig_RealTimeRoomConfig_Valid_m2024 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL RealTimeRoomConfig_MaximumAutomatchingPlayers(void*);}
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MaximumAutomatchingPlayers_m2025 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_MaximumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_MaximumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL RealTimeRoomConfig_MinimumAutomatchingPlayers(void*);}
extern "C" uint32_t RealTimeRoomConfig_RealTimeRoomConfig_MinimumAutomatchingPlayers_m2026 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_MinimumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_MinimumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfig::RealTimeRoomConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Dispose(void*);}
extern "C" void RealTimeRoomConfig_RealTimeRoomConfig_Dispose_m2027 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRo_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_RealTimeRo_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse(void*, IntPtr_t);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse_m2028 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___response, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_PopulateFromPlayerSelectUIResponse'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___response' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___response);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___response' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_SetVariant(void*, uint32_t);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetVariant_m2029 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___variant, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_SetVariant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_SetVariant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___variant' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___variant);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___variant' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_AddPlayerToInvite(void*, char*);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_AddPlayerToInvite_m2030 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___player_id, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_AddPlayerToInvite;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_AddPlayerToInvite'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___player_id' to native representation
	char* ____player_id_marshaled = { 0 };
	____player_id_marshaled = il2cpp_codegen_marshal_string(___player_id);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____player_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___player_id' native representation
	il2cpp_codegen_marshal_free(____player_id_marshaled);
	____player_id_marshaled = NULL;

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Construct()
extern "C" {IntPtr_t DEFAULT_CALL RealTimeRoomConfig_Builder_Construct();}
extern "C" IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Construct_m2031 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_SetExclusiveBitMask(void*, uint64_t);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetExclusiveBitMask_m2032 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint64_t ___exclusive_bit_mask, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_SetExclusiveBitMask;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_SetExclusiveBitMask'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___exclusive_bit_mask' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___exclusive_bit_mask);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___exclusive_bit_mask' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers(void*, uint32_t);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers_m2033 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___maximum_automatching_players, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_SetMaximumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___maximum_automatching_players' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___maximum_automatching_players);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___maximum_automatching_players' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL RealTimeRoomConfig_Builder_Create(void*);}
extern "C" IntPtr_t RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Create_m2034 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_Create;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_Create'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers(void*, uint32_t);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers_m2035 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_automatching_players, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_SetMinimumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___minimum_automatching_players' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___minimum_automatching_players);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___minimum_automatching_players' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeRoomConfigBuilder::RealTimeRoomConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL RealTimeRoomConfig_Builder_Dispose(void*);}
extern "C" void RealTimeRoomConfigBuilder_RealTimeRoomConfig_Builder_Dispose_m2036 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)RealTimeRoomConfig_Builder_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'RealTimeRoomConfig_Builder_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.Score
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Score.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Score
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ScoreMethodDeclarations.h"



// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Value(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Score_Value(void*);}
extern "C" uint64_t Score_Score_Value_m2037 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Score_Value;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Score_Value'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.Score::Score_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL Score_Valid(void*);}
extern "C" bool Score_Score_Valid_m2038 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Score_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Score_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.Score::Score_Rank(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL Score_Rank(void*);}
extern "C" uint64_t Score_Score_Rank_m2039 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Score_Rank;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Score_Rank'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.Score::Score_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL Score_Dispose(void*);}
extern "C" void Score_Score_Dispose_m2040 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Score_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Score_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.Score::Score_Metadata(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL Score_Metadata(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  Score_Score_Metadata_m2041 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Score_Metadata;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Score_Metadata'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.ScorePage
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ScorePage.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.ScorePage
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ScorePageMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"


// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL ScorePage_Dispose(void*);}
extern "C" void ScorePage_ScorePage_Dispose_m2042 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL ScorePage_TimeSpan(void*);}
extern "C" int32_t ScorePage_ScorePage_TimeSpan_m2043 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_TimeSpan;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_TimeSpan'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL ScorePage_LeaderboardId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  ScorePage_ScorePage_LeaderboardId_m2044 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_LeaderboardId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_LeaderboardId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Collection(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL ScorePage_Collection(void*);}
extern "C" int32_t ScorePage_ScorePage_Collection_m2045 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Collection;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Collection'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Start(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL ScorePage_Start(void*);}
extern "C" int32_t ScorePage_ScorePage_Start_m2046 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Start;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Start'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScorePage_Valid(void*);}
extern "C" bool ScorePage_ScorePage_Valid_m2047 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasPreviousScorePage(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScorePage_HasPreviousScorePage(void*);}
extern "C" bool ScorePage_ScorePage_HasPreviousScorePage_m2048 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_HasPreviousScorePage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_HasPreviousScorePage'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasNextScorePage(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScorePage_HasNextScorePage(void*);}
extern "C" bool ScorePage_ScorePage_HasNextScorePage_m2049 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_HasNextScorePage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_HasNextScorePage'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_PreviousScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL ScorePage_PreviousScorePageToken(void*);}
extern "C" IntPtr_t ScorePage_ScorePage_PreviousScorePageToken_m2050 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_PreviousScorePageToken;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_PreviousScorePageToken'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_NextScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL ScorePage_NextScorePageToken(void*);}
extern "C" IntPtr_t ScorePage_ScorePage_NextScorePageToken_m2051 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_NextScorePageToken;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_NextScorePageToken'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL ScorePage_Entries_Length(void*);}
extern "C" UIntPtr_t  ScorePage_ScorePage_Entries_Length_m2052 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entries_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entries_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL ScorePage_Entries_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t ScorePage_ScorePage_Entries_GetElement_m2053 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entries_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entries_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL ScorePage_Entry_Dispose(void*);}
extern "C" void ScorePage_ScorePage_Entry_Dispose_m2054 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_PlayerId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL ScorePage_Entry_PlayerId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  ScorePage_ScorePage_Entry_PlayerId_m2055 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_PlayerId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_PlayerId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModified(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL ScorePage_Entry_LastModified(void*);}
extern "C" uint64_t ScorePage_ScorePage_Entry_LastModified_m2056 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_LastModified;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_LastModified'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Score(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL ScorePage_Entry_Score(void*);}
extern "C" IntPtr_t ScorePage_ScorePage_Entry_Score_m2057 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_Score;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_Score'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScorePage_Entry_Valid(void*);}
extern "C" bool ScorePage_ScorePage_Entry_Valid_m2058 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL ScorePage_Entry_LastModifiedTime(void*);}
extern "C" uint64_t ScorePage_ScorePage_Entry_LastModifiedTime_m2059 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_Entry_LastModifiedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_Entry_LastModifiedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScorePage_ScorePageToken_Valid(void*);}
extern "C" bool ScorePage_ScorePage_ScorePageToken_Valid_m2060 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_ScorePageToken_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_ScorePageToken_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL ScorePage_ScorePageToken_Dispose(void*);}
extern "C" void ScorePage_ScorePage_ScorePageToken_Dispose_m2061 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScorePage_ScorePageToken_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScorePage_ScorePageToken_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.ScoreSummary
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ScoreSumma.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.ScoreSummary
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_ScoreSummaMethodDeclarations.h"



// System.UInt64 GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_ApproximateNumberOfScores(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL ScoreSummary_ApproximateNumberOfScores(void*);}
extern "C" uint64_t ScoreSummary_ScoreSummary_ApproximateNumberOfScores_m2062 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_ApproximateNumberOfScores;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_ApproximateNumberOfScores'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL ScoreSummary_TimeSpan(void*);}
extern "C" int32_t ScoreSummary_ScoreSummary_TimeSpan_m2063 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_TimeSpan;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_TimeSpan'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL ScoreSummary_LeaderboardId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  ScoreSummary_ScoreSummary_LeaderboardId_m2064 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_LeaderboardId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_LeaderboardId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Collection(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL ScoreSummary_Collection(void*);}
extern "C" int32_t ScoreSummary_ScoreSummary_Collection_m2065 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_Collection;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_Collection'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL ScoreSummary_Valid(void*);}
extern "C" bool ScoreSummary_ScoreSummary_Valid_m2066 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_CurrentPlayerScore(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL ScoreSummary_CurrentPlayerScore(void*);}
extern "C" IntPtr_t ScoreSummary_ScoreSummary_CurrentPlayerScore_m2067 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_CurrentPlayerScore;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_CurrentPlayerScore'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.ScoreSummary::ScoreSummary_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL ScoreSummary_Dispose(void*);}
extern "C" void ScoreSummary_ScoreSummary_Dispose_m2068 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ScoreSummary_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ScoreSummary_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.Sentinels
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Sentinels.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Sentinels
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SentinelsMethodDeclarations.h"



// System.IntPtr GooglePlayGames.Native.Cwrapper.Sentinels::Sentinels_AutomatchingParticipant()
extern "C" {IntPtr_t DEFAULT_CALL Sentinels_AutomatchingParticipant();}
extern "C" IntPtr_t Sentinels_Sentinels_AutomatchingParticipant_m2069 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Sentinels_AutomatchingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Sentinels_AutomatchingParticipant'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMaMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FetchAllCallback__ctor_m2070 (FetchAllCallback_t473 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void FetchAllCallback_Invoke_m2071 (FetchAllCallback_t473 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FetchAllCallback_Invoke_m2071((FetchAllCallback_t473 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FetchAllCallback_t473(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * FetchAllCallback_BeginInvoke_m2072 (FetchAllCallback_t473 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback::EndInvoke(System.IAsyncResult)
extern "C" void FetchAllCallback_EndInvoke_m2073 (FetchAllCallback_t473 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OpenCallback__ctor_m2074 (OpenCallback_t474 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void OpenCallback_Invoke_m2075 (OpenCallback_t474 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OpenCallback_Invoke_m2075((OpenCallback_t474 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OpenCallback_t474(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * OpenCallback_BeginInvoke_m2076 (OpenCallback_t474 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback::EndInvoke(System.IAsyncResult)
extern "C" void OpenCallback_EndInvoke_m2077 (OpenCallback_t474 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::.ctor(System.Object,System.IntPtr)
extern "C" void CommitCallback__ctor_m2078 (CommitCallback_t475 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void CommitCallback_Invoke_m2079 (CommitCallback_t475 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CommitCallback_Invoke_m2079((CommitCallback_t475 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CommitCallback_t475(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * CommitCallback_BeginInvoke_m2080 (CommitCallback_t475 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback::EndInvoke(System.IAsyncResult)
extern "C" void CommitCallback_EndInvoke_m2081 (CommitCallback_t475 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ReadCallback__ctor_m2082 (ReadCallback_t476 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void ReadCallback_Invoke_m2083 (ReadCallback_t476 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReadCallback_Invoke_m2083((ReadCallback_t476 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReadCallback_t476(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * ReadCallback_BeginInvoke_m2084 (ReadCallback_t476 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::EndInvoke(System.IAsyncResult)
extern "C" void ReadCallback_EndInvoke_m2085 (ReadCallback_t476 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void SnapshotSelectUICallback__ctor_m2086 (SnapshotSelectUICallback_t477 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void SnapshotSelectUICallback_Invoke_m2087 (SnapshotSelectUICallback_t477 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SnapshotSelectUICallback_Invoke_m2087((SnapshotSelectUICallback_t477 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SnapshotSelectUICallback_t477(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * SnapshotSelectUICallback_BeginInvoke_m2088 (SnapshotSelectUICallback_t477 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C" void SnapshotSelectUICallback_EndInvoke_m2089 (SnapshotSelectUICallback_t477 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMa_4MethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"


// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.SnapshotManager/FetchAllCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_FetchAll(void*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_FetchAll_m2090 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t473 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_FetchAll;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_FetchAll'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ShowSelectUIOperation(System.Runtime.InteropServices.HandleRef,System.Boolean,System.Boolean,System.UInt32,System.String,GooglePlayGames.Native.Cwrapper.SnapshotManager/SnapshotSelectUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_ShowSelectUIOperation(void*, int8_t, int8_t, uint32_t, char*, methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_ShowSelectUIOperation_m2091 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, bool ___allow_create, bool ___allow_delete, uint32_t ___max_snapshots, String_t* ___title, SnapshotSelectUICallback_t477 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int8_t, int8_t, uint32_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_ShowSelectUIOperation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_ShowSelectUIOperation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___allow_create' to native representation

	// Marshaling of parameter '___allow_delete' to native representation

	// Marshaling of parameter '___max_snapshots' to native representation

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = { 0 };
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___allow_create, ___allow_delete, ___max_snapshots, ____title_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___allow_create' native representation

	// Marshaling cleanup of parameter '___allow_delete' native representation

	// Marshaling cleanup of parameter '___max_snapshots' native representation

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Read(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_Read(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_Read_m2092 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___snapshot_metadata, ReadCallback_t476 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_Read;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_Read'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___snapshot_metadata' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___snapshot_metadata, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___snapshot_metadata' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Commit(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.Byte[],System.UIntPtr,GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_Commit(void*, IntPtr_t, IntPtr_t, uint8_t*, UIntPtr_t , methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_Commit_m2093 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___snapshot_metadata, IntPtr_t ___metadata_change, ByteU5BU5D_t350* ___data, UIntPtr_t  ___data_size, CommitCallback_t475 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, uint8_t*, UIntPtr_t , methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_Commit;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_Commit'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___snapshot_metadata' to native representation

	// Marshaling of parameter '___metadata_change' to native representation

	// Marshaling of parameter '___data' to native representation
	uint8_t* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___data);

	// Marshaling of parameter '___data_size' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___snapshot_metadata, ___metadata_change, ____data_marshaled, ___data_size, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___snapshot_metadata' native representation

	// Marshaling cleanup of parameter '___metadata_change' native representation

	// Marshaling cleanup of parameter '___data' native representation

	// Marshaling cleanup of parameter '___data_size' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Open(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy,GooglePlayGames.Native.Cwrapper.SnapshotManager/OpenCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_Open(void*, int32_t, char*, int32_t, methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_Open_m2094 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___file_name, int32_t ___conflict_policy, OpenCallback_t474 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, char*, int32_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_Open;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_Open'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___data_source' to native representation

	// Marshaling of parameter '___file_name' to native representation
	char* ____file_name_marshaled = { 0 };
	____file_name_marshaled = il2cpp_codegen_marshal_string(___file_name);

	// Marshaling of parameter '___conflict_policy' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___data_source, ____file_name_marshaled, ___conflict_policy, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___data_source' native representation

	// Marshaling cleanup of parameter '___file_name' native representation
	il2cpp_codegen_marshal_free(____file_name_marshaled);
	____file_name_marshaled = NULL;

	// Marshaling cleanup of parameter '___conflict_policy' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ResolveConflict(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,System.String,GooglePlayGames.Native.Cwrapper.SnapshotManager/CommitCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_ResolveConflict(void*, IntPtr_t, IntPtr_t, char*, methodPointerType, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_ResolveConflict_m2095 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___snapshot_metadata, IntPtr_t ___metadata_change, String_t* ___conflict_id, CommitCallback_t475 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_ResolveConflict;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_ResolveConflict'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___snapshot_metadata' to native representation

	// Marshaling of parameter '___metadata_change' to native representation

	// Marshaling of parameter '___conflict_id' to native representation
	char* ____conflict_id_marshaled = { 0 };
	____conflict_id_marshaled = il2cpp_codegen_marshal_string(___conflict_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___snapshot_metadata, ___metadata_change, ____conflict_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___snapshot_metadata' native representation

	// Marshaling cleanup of parameter '___metadata_change' native representation

	// Marshaling cleanup of parameter '___conflict_id' native representation
	il2cpp_codegen_marshal_free(____conflict_id_marshaled);
	____conflict_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_Delete(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL SnapshotManager_Delete(void*, IntPtr_t);}
extern "C" void SnapshotManager_SnapshotManager_Delete_m2096 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___snapshot_metadata, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_Delete;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_Delete'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___snapshot_metadata' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___snapshot_metadata);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___snapshot_metadata' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotManager_FetchAllResponse_Dispose(void*);}
extern "C" void SnapshotManager_SnapshotManager_FetchAllResponse_Dispose_m2097 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_FetchAllResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_FetchAllResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL SnapshotManager_FetchAllResponse_GetStatus(void*);}
extern "C" int32_t SnapshotManager_SnapshotManager_FetchAllResponse_GetStatus_m2098 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_FetchAllResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_FetchAllResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotManager_FetchAllResponse_GetData_Length(void*);}
extern "C" UIntPtr_t  SnapshotManager_SnapshotManager_FetchAllResponse_GetData_Length_m2099 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_FetchAllResponse_GetData_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_FetchAllResponse_GetData_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_FetchAllResponse_GetData_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_FetchAllResponse_GetData_GetElement_m2100 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_FetchAllResponse_GetData_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_FetchAllResponse_GetData_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotManager_OpenResponse_Dispose(void*);}
extern "C" void SnapshotManager_SnapshotManager_OpenResponse_Dispose_m2101 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL SnapshotManager_OpenResponse_GetStatus(void*);}
extern "C" int32_t SnapshotManager_SnapshotManager_OpenResponse_GetStatus_m2102 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetData(void*);}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetData_m2103 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  SnapshotManager_SnapshotManager_OpenResponse_GetConflictId_m2104 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_GetConflictId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_GetConflictId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictOriginal(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictOriginal(void*);}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetConflictOriginal_m2105 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_GetConflictOriginal;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_GetConflictOriginal'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_OpenResponse_GetConflictUnmerged(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_OpenResponse_GetConflictUnmerged(void*);}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_OpenResponse_GetConflictUnmerged_m2106 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_OpenResponse_GetConflictUnmerged;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_OpenResponse_GetConflictUnmerged'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotManager_CommitResponse_Dispose(void*);}
extern "C" void SnapshotManager_SnapshotManager_CommitResponse_Dispose_m2107 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_CommitResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_CommitResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL SnapshotManager_CommitResponse_GetStatus(void*);}
extern "C" int32_t SnapshotManager_SnapshotManager_CommitResponse_GetStatus_m2108 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_CommitResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_CommitResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_CommitResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_CommitResponse_GetData(void*);}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_CommitResponse_GetData_m2109 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_CommitResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_CommitResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotManager_ReadResponse_Dispose(void*);}
extern "C" void SnapshotManager_SnapshotManager_ReadResponse_Dispose_m2110 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_ReadResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_ReadResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL SnapshotManager_ReadResponse_GetStatus(void*);}
extern "C" int32_t SnapshotManager_SnapshotManager_ReadResponse_GetStatus_m2111 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_ReadResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_ReadResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_ReadResponse_GetData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotManager_ReadResponse_GetData(void*, uint8_t*, UIntPtr_t );}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
extern "C" UIntPtr_t  SnapshotManager_SnapshotManager_ReadResponse_GetData_m2112 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_ReadResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_ReadResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	uint8_t* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	___out_arg = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, ____out_arg_marshaled, 1);

	// Marshaling cleanup of parameter '___out_arg' native representation

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_Dispose(void*);}
extern "C" void SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_Dispose_m2113 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_SnapshotSelectUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_SnapshotSelectUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_GetStatus(void*);}
extern "C" int32_t SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetStatus_m2114 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_SnapshotSelectUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_SnapshotSelectUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotManager::SnapshotManager_SnapshotSelectUIResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotManager_SnapshotSelectUIResponse_GetData(void*);}
extern "C" IntPtr_t SnapshotManager_SnapshotManager_SnapshotSelectUIResponse_GetData_m2115 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotManager_SnapshotSelectUIResponse_GetData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotManager_SnapshotSelectUIResponse_GetData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.SnapshotMetadata
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotMetadata
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMeMethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotMetadata_Dispose(void*);}
extern "C" void SnapshotMetadata_SnapshotMetadata_Dispose_m2116 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_CoverImageURL(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotMetadata_CoverImageURL(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_CoverImageURL_m2117 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_CoverImageURL;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_CoverImageURL'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotMetadata_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_Description_m2118 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_IsOpen(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadata_IsOpen(void*);}
extern "C" bool SnapshotMetadata_SnapshotMetadata_IsOpen_m2119 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_IsOpen;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_IsOpen'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_FileName(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotMetadata_FileName(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  SnapshotMetadata_SnapshotMetadata_FileName_m2120 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_FileName;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_FileName'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadata_Valid(void*);}
extern "C" bool SnapshotMetadata_SnapshotMetadata_Valid_m2121 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL SnapshotMetadata_PlayedTime(void*);}
extern "C" int64_t SnapshotMetadata_SnapshotMetadata_PlayedTime_m2122 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_PlayedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_PlayedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.SnapshotMetadata::SnapshotMetadata_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL SnapshotMetadata_LastModifiedTime(void*);}
extern "C" int64_t SnapshotMetadata_SnapshotMetadata_LastModifiedTime_m2123 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadata_LastModifiedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadata_LastModifiedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_0MethodDeclarations.h"



// System.UIntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL SnapshotMetadataChange_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  SnapshotMetadataChange_SnapshotMetadataChange_Description_m2124 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Image(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotMetadataChange_Image(void*);}
extern "C" IntPtr_t SnapshotMetadataChange_SnapshotMetadataChange_Image_m2125 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Image;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Image'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTimeIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadataChange_PlayedTimeIsChanged(void*);}
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_PlayedTimeIsChanged_m2126 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_PlayedTimeIsChanged;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_PlayedTimeIsChanged'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadataChange_Valid(void*);}
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_Valid_m2127 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_PlayedTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL SnapshotMetadataChange_PlayedTime(void*);}
extern "C" uint64_t SnapshotMetadataChange_SnapshotMetadataChange_PlayedTime_m2128 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_PlayedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_PlayedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotMetadataChange_Dispose(void*);}
extern "C" void SnapshotMetadataChange_SnapshotMetadataChange_Dispose_m2129 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_ImageIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadataChange_ImageIsChanged(void*);}
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_ImageIsChanged_m2130 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_ImageIsChanged;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_ImageIsChanged'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.SnapshotMetadataChange::SnapshotMetadataChange_DescriptionIsChanged(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL SnapshotMetadataChange_DescriptionIsChanged(void*);}
extern "C" bool SnapshotMetadataChange_SnapshotMetadataChange_DescriptionIsChanged_m2131 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_DescriptionIsChanged;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_DescriptionIsChanged'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SnapshotMe_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetDescription(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL SnapshotMetadataChange_Builder_SetDescription(void*, char*);}
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetDescription_m2132 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___description, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_SetDescription;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_SetDescription'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___description' to native representation
	char* ____description_marshaled = { 0 };
	____description_marshaled = il2cpp_codegen_marshal_string(___description);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____description_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___description' native representation
	il2cpp_codegen_marshal_free(____description_marshaled);
	____description_marshaled = NULL;

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Construct()
extern "C" {IntPtr_t DEFAULT_CALL SnapshotMetadataChange_Builder_Construct();}
extern "C" IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Construct_m2133 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetPlayedTime(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C" {void DEFAULT_CALL SnapshotMetadataChange_Builder_SetPlayedTime(void*, uint64_t);}
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetPlayedTime_m2134 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint64_t ___played_time, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_SetPlayedTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_SetPlayedTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___played_time' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___played_time);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___played_time' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_SetCoverImageFromPngData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" {void DEFAULT_CALL SnapshotMetadataChange_Builder_SetCoverImageFromPngData(void*, uint8_t*, UIntPtr_t );}
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_SetCoverImageFromPngData_m2135 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___png_data, UIntPtr_t  ___png_data_size, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_SetCoverImageFromPngData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_SetCoverImageFromPngData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___png_data' to native representation
	uint8_t* ____png_data_marshaled = { 0 };
	____png_data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___png_data);

	// Marshaling of parameter '___png_data_size' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____png_data_marshaled, ___png_data_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___png_data' native representation

	// Marshaling cleanup of parameter '___png_data_size' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL SnapshotMetadataChange_Builder_Create(void*);}
extern "C" IntPtr_t SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Create_m2136 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_Create;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_Create'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotMetadataChangeBuilder::SnapshotMetadataChange_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL SnapshotMetadataChange_Builder_Dispose(void*);}
extern "C" void SnapshotMetadataChangeBuilder_SnapshotMetadataChange_Builder_Dispose_m2137 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SnapshotMetadataChange_Builder_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SnapshotMetadataChange_Builder_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.Status/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Res.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_ResMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/FlushStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Flu.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/FlushStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_FluMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Aut.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_AutMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_UIS.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_UISMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Mul.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_MulMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Que.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_QueMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Que_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Que_0MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/CommonErrorStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Com.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/CommonErrorStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_ComMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Sna.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_SnaMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Status
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Status
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_StatusMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.SymbolLocation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SymbolLoca.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.SymbolLocation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_SymbolLocaMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedMMethodDeclarations.h"

// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0.h"


// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_AutomatchingSlotsAvailable(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatch_AutomatchingSlotsAvailable(void*);}
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_AutomatchingSlotsAvailable_m2138 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_AutomatchingSlotsAvailable;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_AutomatchingSlotsAvailable'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreationTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL TurnBasedMatch_CreationTime(void*);}
extern "C" uint64_t TurnBasedMatch_TurnBasedMatch_CreationTime_m2139 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_CreationTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_CreationTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_Participants_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Participants_Length_m2140 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Participants_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Participants_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Participants_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_Participants_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_Participants_GetElement_m2141 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Participants_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Participants_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Version(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatch_Version(void*);}
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Version_m2142 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Version;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Version'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_ParticipantResults(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_ParticipantResults(void*);}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_ParticipantResults_m2143 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_ParticipantResults;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_ParticipantResults'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Status(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL TurnBasedMatch_Status(void*);}
extern "C" int32_t TurnBasedMatch_TurnBasedMatch_Status_m2144 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Status;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Status'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Description(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_Description(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Description_m2145 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Description;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Description'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PendingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_PendingParticipant(void*);}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_PendingParticipant_m2146 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_PendingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_PendingParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatch_Variant(void*);}
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Variant_m2147 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Variant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Variant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasPreviousMatchData(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL TurnBasedMatch_HasPreviousMatchData(void*);}
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasPreviousMatchData_m2148 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_HasPreviousMatchData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_HasPreviousMatchData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Data(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_Data(void*, uint8_t*, UIntPtr_t );}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Data_m2149 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Data;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Data'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	uint8_t* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	___out_arg = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, ____out_arg_marshaled, 1);

	// Marshaling cleanup of parameter '___out_arg' native representation

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_LastUpdatingParticipant(void*);}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_LastUpdatingParticipant_m2150 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_LastUpdatingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_LastUpdatingParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasData(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL TurnBasedMatch_HasData(void*);}
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasData_m2151 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_HasData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_HasData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_SuggestedNextParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_SuggestedNextParticipant(void*);}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_SuggestedNextParticipant_m2152 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_SuggestedNextParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_SuggestedNextParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_PreviousMatchData(System.Runtime.InteropServices.HandleRef,System.Byte[],System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_PreviousMatchData(void*, uint8_t*, UIntPtr_t );}
extern TypeInfo* Byte_t237_il2cpp_TypeInfo_var;
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_PreviousMatchData_m2153 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ByteU5BU5D_t350* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Byte_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, uint8_t*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_PreviousMatchData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_PreviousMatchData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	uint8_t* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	___out_arg = (ByteU5BU5D_t350*)il2cpp_codegen_marshal_array_result(Byte_t237_il2cpp_TypeInfo_var, ____out_arg_marshaled, 1);

	// Marshaling cleanup of parameter '___out_arg' native representation

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt64 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_LastUpdateTime(System.Runtime.InteropServices.HandleRef)
extern "C" {uint64_t DEFAULT_CALL TurnBasedMatch_LastUpdateTime(void*);}
extern "C" uint64_t TurnBasedMatch_TurnBasedMatch_LastUpdateTime_m2154 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_LastUpdateTime;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_LastUpdateTime'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_RematchId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_RematchId(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_RematchId_m2155 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_RematchId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_RematchId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Number(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatch_Number(void*);}
extern "C" uint32_t TurnBasedMatch_TurnBasedMatch_Number_m2156 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Number;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Number'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_HasRematchId(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL TurnBasedMatch_HasRematchId(void*);}
extern "C" bool TurnBasedMatch_TurnBasedMatch_HasRematchId_m2157 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_HasRematchId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_HasRematchId'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL TurnBasedMatch_Valid(void*);}
extern "C" bool TurnBasedMatch_TurnBasedMatch_Valid_m2158 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_CreatingParticipant(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatch_CreatingParticipant(void*);}
extern "C" IntPtr_t TurnBasedMatch_TurnBasedMatch_CreatingParticipant_m2159 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_CreatingParticipant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_CreatingParticipant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Id(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatch_Id(void*, char*, UIntPtr_t );}
extern "C" UIntPtr_t  TurnBasedMatch_TurnBasedMatch_Id_m2160 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Id;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Id'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatch::TurnBasedMatch_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMatch_Dispose(void*);}
extern "C" void TurnBasedMatch_TurnBasedMatch_Dispose_m2161 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatch_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatch_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_0MethodDeclarations.h"



// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatchConfig_PlayerIdsToInvite_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_Length_m2162 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_PlayerIdsToInvite_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_PlayerIdsToInvite_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(void*, UIntPtr_t , char*, UIntPtr_t );}
extern "C" UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_GetElement_m2163 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t , char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_PlayerIdsToInvite_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_PlayerIdsToInvite_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatchConfig_Variant(void*);}
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_Variant_m2164 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Variant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Variant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Int64 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C" {int64_t DEFAULT_CALL TurnBasedMatchConfig_ExclusiveBitMask(void*);}
extern "C" int64_t TurnBasedMatchConfig_TurnBasedMatchConfig_ExclusiveBitMask_m2165 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int64_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_ExclusiveBitMask;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_ExclusiveBitMask'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int64_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" {int8_t DEFAULT_CALL TurnBasedMatchConfig_Valid(void*);}
extern "C" bool TurnBasedMatchConfig_TurnBasedMatchConfig_Valid_m2166 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int8_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Valid;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Valid'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int8_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatchConfig_MaximumAutomatchingPlayers(void*);}
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MaximumAutomatchingPlayers_m2167 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_MaximumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_MaximumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMatchConfig_MinimumAutomatchingPlayers(void*);}
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MinimumAutomatchingPlayers_m2168 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_MinimumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_MinimumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Dispose(void*);}
extern "C" void TurnBasedMatchConfig_TurnBasedMatchConfig_Dispose_m2169 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse(void*, IntPtr_t);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse_m2170 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___response, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_PopulateFromPlayerSelectUIResponse'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___response' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___response);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___response' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetVariant(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetVariant(void*, uint32_t);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetVariant_m2171 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___variant, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_SetVariant;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_SetVariant'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___variant' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___variant);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___variant' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_AddPlayerToInvite(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_AddPlayerToInvite(void*, char*);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_AddPlayerToInvite_m2172 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___player_id, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_AddPlayerToInvite;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_AddPlayerToInvite'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___player_id' to native representation
	char* ____player_id_marshaled = { 0 };
	____player_id_marshaled = il2cpp_codegen_marshal_string(___player_id);

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____player_id_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___player_id' native representation
	il2cpp_codegen_marshal_free(____player_id_marshaled);
	____player_id_marshaled = NULL;

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Construct()
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatchConfig_Builder_Construct();}
extern "C" IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Construct_m2173 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_Construct;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_Construct'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetExclusiveBitMask(System.Runtime.InteropServices.HandleRef,System.UInt64)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetExclusiveBitMask(void*, uint64_t);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetExclusiveBitMask_m2174 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint64_t ___exclusive_bit_mask, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint64_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_SetExclusiveBitMask;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_SetExclusiveBitMask'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___exclusive_bit_mask' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___exclusive_bit_mask);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___exclusive_bit_mask' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers(void*, uint32_t);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers_m2175 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___maximum_automatching_players, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_SetMaximumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___maximum_automatching_players' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___maximum_automatching_players);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___maximum_automatching_players' native representation

}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Create(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMatchConfig_Builder_Create(void*);}
extern "C" IntPtr_t TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Create_m2176 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_Create;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_Create'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef,System.UInt32)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers(void*, uint32_t);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers_m2177 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_automatching_players, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_SetMinimumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___minimum_automatching_players' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___minimum_automatching_players);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___minimum_automatching_players' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfigBuilder::TurnBasedMatchConfig_Builder_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMatchConfig_Builder_Dispose(void*);}
extern "C" void TurnBasedMatchConfigBuilder_TurnBasedMatchConfig_Builder_Dispose_m2178 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMatchConfig_Builder_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMatchConfig_Builder_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TurnBasedMatchCallback__ctor_m2179 (TurnBasedMatchCallback_t496 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedMatchCallback_Invoke_m2180 (TurnBasedMatchCallback_t496 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TurnBasedMatchCallback_Invoke_m2180((TurnBasedMatchCallback_t496 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_TurnBasedMatchCallback_t496(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * TurnBasedMatchCallback_BeginInvoke_m2181 (TurnBasedMatchCallback_t496 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::EndInvoke(System.IAsyncResult)
extern "C" void TurnBasedMatchCallback_EndInvoke_m2182 (TurnBasedMatchCallback_t496 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback::.ctor(System.Object,System.IntPtr)
extern "C" void MultiplayerStatusCallback__ctor_m2183 (MultiplayerStatusCallback_t497 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern "C" void MultiplayerStatusCallback_Invoke_m2184 (MultiplayerStatusCallback_t497 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MultiplayerStatusCallback_Invoke_m2184((MultiplayerStatusCallback_t497 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_MultiplayerStatusCallback_t497(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* MultiplayerStatus_t413_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * MultiplayerStatusCallback_BeginInvoke_m2185 (MultiplayerStatusCallback_t497 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiplayerStatus_t413_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(MultiplayerStatus_t413_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback::EndInvoke(System.IAsyncResult)
extern "C" void MultiplayerStatusCallback_EndInvoke_m2186 (MultiplayerStatusCallback_t497 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_4.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_4MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TurnBasedMatchesCallback__ctor_m2187 (TurnBasedMatchesCallback_t498 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedMatchesCallback_Invoke_m2188 (TurnBasedMatchesCallback_t498 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TurnBasedMatchesCallback_Invoke_m2188((TurnBasedMatchesCallback_t498 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_TurnBasedMatchesCallback_t498(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * TurnBasedMatchesCallback_BeginInvoke_m2189 (TurnBasedMatchesCallback_t498 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback::EndInvoke(System.IAsyncResult)
extern "C" void TurnBasedMatchesCallback_EndInvoke_m2190 (TurnBasedMatchesCallback_t498 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_5.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_5MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void MatchInboxUICallback__ctor_m2191 (MatchInboxUICallback_t499 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void MatchInboxUICallback_Invoke_m2192 (MatchInboxUICallback_t499 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		MatchInboxUICallback_Invoke_m2192((MatchInboxUICallback_t499 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_MatchInboxUICallback_t499(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * MatchInboxUICallback_BeginInvoke_m2193 (MatchInboxUICallback_t499 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback::EndInvoke(System.IAsyncResult)
extern "C" void MatchInboxUICallback_EndInvoke_m2194 (MatchInboxUICallback_t499 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_6.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_6MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void PlayerSelectUICallback__ctor_m2195 (PlayerSelectUICallback_t500 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void PlayerSelectUICallback_Invoke_m2196 (PlayerSelectUICallback_t500 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PlayerSelectUICallback_Invoke_m2196((PlayerSelectUICallback_t500 *)__this->___prev_9,___arg0, ___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0, ___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PlayerSelectUICallback_t500(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1)
{
	typedef void (STDCALL *native_function_ptr_type)(IntPtr_t, IntPtr_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___arg0' to native representation

	// Marshaling of parameter '___arg1' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___arg0, ___arg1);

	// Marshaling cleanup of parameter '___arg0' native representation

	// Marshaling cleanup of parameter '___arg1' native representation

}
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayerSelectUICallback_BeginInvoke_m2197 (PlayerSelectUICallback_t500 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg0);
	__d_args[1] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___arg1);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback::EndInvoke(System.IAsyncResult)
extern "C" void PlayerSelectUICallback_EndInvoke_m2198 (PlayerSelectUICallback_t500 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_7.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TurnBasedM_7MethodDeclarations.h"



// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowPlayerSelectUI(System.Runtime.InteropServices.HandleRef,System.UInt32,System.UInt32,System.Boolean,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/PlayerSelectUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_ShowPlayerSelectUI(void*, uint32_t, uint32_t, int8_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowPlayerSelectUI_m2199 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, uint32_t ___minimum_players, uint32_t ___maximum_players, bool ___allow_automatch, PlayerSelectUICallback_t500 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, uint32_t, uint32_t, int8_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_ShowPlayerSelectUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_ShowPlayerSelectUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___minimum_players' to native representation

	// Marshaling of parameter '___maximum_players' to native representation

	// Marshaling of parameter '___allow_automatch' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___minimum_players, ___maximum_players, ___allow_automatch, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___minimum_players' native representation

	// Marshaling cleanup of parameter '___maximum_players' native representation

	// Marshaling cleanup of parameter '___allow_automatch' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CancelMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_CancelMatch(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CancelMatch_m2200 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_CancelMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_CancelMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_DismissMatch(void*, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissMatch_m2201 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_DismissMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_DismissMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ShowMatchInboxUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MatchInboxUICallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_ShowMatchInboxUI(void*, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ShowMatchInboxUI_m2202 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MatchInboxUICallback_t499 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_ShowMatchInboxUI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_ShowMatchInboxUI'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_SynchronizeData(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_SynchronizeData(void*);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_SynchronizeData_m2203 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_SynchronizeData;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_SynchronizeData'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_Rematch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_Rematch(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_Rematch_m2204 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_Rematch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_Rematch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DismissInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_DismissInvitation(void*, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DismissInvitation_m2205 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_DismissInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_DismissInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatch(System.Runtime.InteropServices.HandleRef,System.String,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_FetchMatch(void*, char*, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatch_m2206 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___match_id, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_FetchMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_FetchMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match_id' to native representation
	char* ____match_id_marshaled = { 0 };
	____match_id_marshaled = il2cpp_codegen_marshal_string(___match_id);

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____match_id_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match_id' native representation
	il2cpp_codegen_marshal_free(____match_id_marshaled);
	____match_id_marshaled = NULL;

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_DeclineInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_DeclineInvitation(void*, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_DeclineInvitation_m2207 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_DeclineInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_DeclineInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_FinishMatchDuringMyTurn(void*, IntPtr_t, uint8_t*, UIntPtr_t , IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FinishMatchDuringMyTurn_m2208 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, ByteU5BU5D_t350* ___match_data, UIntPtr_t  ___match_data_size, IntPtr_t ___results, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, uint8_t*, UIntPtr_t , IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_FinishMatchDuringMyTurn;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_FinishMatchDuringMyTurn'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___match_data' to native representation
	uint8_t* ____match_data_marshaled = { 0 };
	____match_data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___match_data);

	// Marshaling of parameter '___match_data_size' to native representation

	// Marshaling of parameter '___results' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____match_data_marshaled, ___match_data_size, ___results, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___match_data' native representation

	// Marshaling cleanup of parameter '___match_data_size' native representation

	// Marshaling cleanup of parameter '___results' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_FetchMatches(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchesCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_FetchMatches(void*, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_FetchMatches_m2209 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, TurnBasedMatchesCallback_t498 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_FetchMatches;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_FetchMatches'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_CreateTurnBasedMatch(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_CreateTurnBasedMatch(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_CreateTurnBasedMatch_m2210 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___config, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_CreateTurnBasedMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_CreateTurnBasedMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___config' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___config, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___config' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_AcceptInvitation(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_AcceptInvitation(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_AcceptInvitation_m2211 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___invitation, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_AcceptInvitation;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_AcceptInvitation'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___invitation' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___invitation, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___invitation' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TakeMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.Byte[],System.UIntPtr,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_TakeMyTurn(void*, IntPtr_t, uint8_t*, UIntPtr_t , IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TakeMyTurn_m2212 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, ByteU5BU5D_t350* ___match_data, UIntPtr_t  ___match_data_size, IntPtr_t ___results, IntPtr_t ___next_participant, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, uint8_t*, UIntPtr_t , IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TakeMyTurn;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TakeMyTurn'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___match_data' to native representation
	uint8_t* ____match_data_marshaled = { 0 };
	____match_data_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___match_data);

	// Marshaling of parameter '___match_data_size' to native representation

	// Marshaling of parameter '___results' to native representation

	// Marshaling of parameter '___next_participant' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____match_data_marshaled, ___match_data_size, ___results, ___next_participant, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___match_data' native representation

	// Marshaling cleanup of parameter '___match_data_size' native representation

	// Marshaling cleanup of parameter '___results' native representation

	// Marshaling cleanup of parameter '___next_participant' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_ConfirmPendingCompletion(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_ConfirmPendingCompletion(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_ConfirmPendingCompletion_m2213 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, TurnBasedMatchCallback_t496 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_ConfirmPendingCompletion;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_ConfirmPendingCompletion'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn(void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn_m2214 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, IntPtr_t ___next_participant, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_LeaveMatchDuringMyTurn'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___next_participant' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ___next_participant, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___next_participant' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(System.Runtime.InteropServices.HandleRef,System.IntPtr,GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/MultiplayerStatusCallback,System.IntPtr)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn(void*, IntPtr_t, methodPointerType, IntPtr_t);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn_m2215 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtr_t ___match, MultiplayerStatusCallback_t497 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, IntPtr_t, methodPointerType, IntPtr_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_LeaveMatchDuringTheirTurn'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___match' to native representation

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Marshaling of parameter '___callback_arg' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled, ___match, ____callback_marshaled, ___callback_arg);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___match' native representation

	// Marshaling cleanup of parameter '___callback' native representation

	// Marshaling cleanup of parameter '___callback_arg' native representation

}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose(void*);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose_m2216 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus(void*);}
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus_m2217 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch(void*);}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch_m2218 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchResponse_GetMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose(void*);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose_m2219 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus(void*);}
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus_m2220 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length_m2221 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement_m2222 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetInvitations_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length_m2223 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement_m2224 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetMyTurnMatches_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length_m2225 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement_m2226 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetTheirTurnMatches_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length_m2227 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement(void*, UIntPtr_t );}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement_m2228 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_TurnBasedMatchesResponse_GetCompletedMatches_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose(void*);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose_m2229 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_MatchInboxUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus(void*);}
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus_m2230 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_MatchInboxUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.IntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(System.Runtime.InteropServices.HandleRef)
extern "C" {IntPtr_t DEFAULT_CALL TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch(void*);}
extern "C" IntPtr_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch_m2231 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef IntPtr_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_MatchInboxUIResponse_GetMatch'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	IntPtr_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" {void DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose(void*);}
extern "C" void TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose_m2232 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_Dispose'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation
	_il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

}
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" {int32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus(void*);}
extern "C" int32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus_m2233 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetStatus'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(System.Runtime.InteropServices.HandleRef)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length(void*);}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length_m2234 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_Length'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" {UIntPtr_t  DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement(void*, UIntPtr_t , char*, UIntPtr_t );}
extern "C" UIntPtr_t  TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement_m2235 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method)
{
	typedef UIntPtr_t  (DEFAULT_CALL *PInvokeFunc) (void*, UIntPtr_t , char*, UIntPtr_t );
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetPlayerIds_GetElement'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Marshaling of parameter '___index' to native representation

	// Marshaling of parameter '___out_arg' to native representation
	char* ____out_arg_marshaled = { 0 };
	____out_arg_marshaled = il2cpp_codegen_marshal_string_builder(___out_arg);

	// Marshaling of parameter '___out_size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	UIntPtr_t  _return_value = _il2cpp_pinvoke_func(____self_marshaled, ___index, ____out_arg_marshaled, ___out_size);

	// Marshaling cleanup of parameter '___self' native representation

	// Marshaling cleanup of parameter '___index' native representation

	// Marshaling of parameter '___out_arg' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___out_arg, ____out_arg_marshaled);

	// Marshaling cleanup of parameter '___out_arg' native representation
	il2cpp_codegen_marshal_free(____out_arg_marshaled);
	____out_arg_marshaled = NULL;

	// Marshaling cleanup of parameter '___out_size' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers(void*);}
extern "C" uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers_m2236 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMinimumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager::TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" {uint32_t DEFAULT_CALL TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers(void*);}
extern "C" uint32_t TurnBasedMultiplayerManager_TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers_m2237 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'TurnBasedMultiplayerManager_PlayerSelectUIResponse_GetMaximumAutomatchingPlayers'"));
		}
	}

	// Marshaling of parameter '___self' to native representation
	void* ____self_marshaled = { 0 };
	____self_marshaled = ___self.___handle_1.___m_value_0;

	// Native function invocation and marshaling of return value back from native representation
	uint32_t _return_value = _il2cpp_pinvoke_func(____self_marshaled);

	// Marshaling cleanup of parameter '___self' native representation

	return _return_value;
}
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_DataMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/LogLevel
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_LogL.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/LogLevel
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_LogLMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_AuthMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Imag.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/ImageResolution
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_ImagMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_AchiMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Even.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_EvenMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_LeadMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_PartMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_MatcMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_QuesMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0MethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_MultMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0MethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_RealMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_SnapMethodDeclarations.h"



// GooglePlayGames.Native.Cwrapper.Types
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.Cwrapper.Types
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_TypesMethodDeclarations.h"



// GooglePlayGames.Native.NativeClient/AuthState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_AuthSt.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/AuthState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_AuthStMethodDeclarations.h"



// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CHan.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CHanMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_0.h"
// GooglePlayGames.Native.NativeClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient.h"
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager_Fetch.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// GooglePlayGames.Native.NativeClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClientMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::.ctor()
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C__ctor_m2238 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::<>m__11(GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse)
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__11_m2239 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, FetchAllResponse_t655 * ___results, MethodInfo* method)
{
	{
		NativeClient_t524 * L_0 = (__this->___U3CU3Ef__this_1);
		uint32_t L_1 = (__this->___currentAuthGeneration_0);
		FetchAllResponse_t655 * L_2 = ___results;
		NullCheck(L_0);
		NativeClient_PopulateAchievements_m2263(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::<>m__12(GooglePlayGames.Native.PlayerManager/FetchSelfResponse)
extern "C" void U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__12_m2240 (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * __this, FetchSelfResponse_t684 * ___results, MethodInfo* method)
{
	{
		NativeClient_t524 * L_0 = (__this->___U3CU3Ef__this_1);
		uint32_t L_1 = (__this->___currentAuthGeneration_0);
		FetchSelfResponse_t684 * L_2 = ___results;
		NullCheck(L_0);
		NativeClient_PopulateUser_m2265(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CUnl.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CUnlMethodDeclarations.h"

// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achievement.h"
// GooglePlayGames.Native.PInvoke.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService.h"
// GooglePlayGames.Native.PInvoke.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_1.h"
// GooglePlayGames.BasicApi.Achievement
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_AchievementMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.GameServices
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameServiceMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.AchievementManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_1MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::.ctor()
extern "C" void U3CUnlockAchievementU3Ec__AnonStorey1D__ctor_m2241 (U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<UnlockAchievement>c__AnonStorey1D::<>m__14(GooglePlayGames.BasicApi.Achievement)
extern "C" void U3CUnlockAchievementU3Ec__AnonStorey1D_U3CU3Em__14_m2242 (U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * __this, Achievement_t333 * ___a, MethodInfo* method)
{
	{
		Achievement_t333 * L_0 = ___a;
		NullCheck(L_0);
		Achievement_set_IsUnlocked_m1336(L_0, 1, /*hidden argument*/NULL);
		NativeClient_t524 * L_1 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_1);
		GameServices_t534 * L_2 = NativeClient_GameServices_m2256(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		AchievementManager_t656 * L_3 = GameServices_AchievementManager_m2665(L_2, /*hidden argument*/NULL);
		String_t* L_4 = (__this->___achId_0);
		NullCheck(L_3);
		AchievementManager_Unlock_m2623(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CRev.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CRevMethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::.ctor()
extern "C" void U3CRevealAchievementU3Ec__AnonStorey1E__ctor_m2243 (U3CRevealAchievementU3Ec__AnonStorey1E_t527 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<RevealAchievement>c__AnonStorey1E::<>m__16(GooglePlayGames.BasicApi.Achievement)
extern "C" void U3CRevealAchievementU3Ec__AnonStorey1E_U3CU3Em__16_m2244 (U3CRevealAchievementU3Ec__AnonStorey1E_t527 * __this, Achievement_t333 * ___a, MethodInfo* method)
{
	{
		Achievement_t333 * L_0 = ___a;
		NullCheck(L_0);
		Achievement_set_IsRevealed_m1338(L_0, 1, /*hidden argument*/NULL);
		NativeClient_t524 * L_1 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_1);
		GameServices_t534 * L_2 = NativeClient_GameServices_m2256(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		AchievementManager_t656 * L_3 = GameServices_AchievementManager_m2665(L_2, /*hidden argument*/NULL);
		String_t* L_4 = (__this->___achId_0);
		NullCheck(L_3);
		AchievementManager_Reveal_m2622(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CUpd.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CUpdMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11.h"
// GooglePlayGames.Native.NativeAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeAchievement.h"
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_AchievementMethodDeclarations.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11MethodDeclarations.h"
// GooglePlayGames.Native.NativeAchievement
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeAchievementMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// GooglePlayGames.OurUtils.Logger
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_LoggerMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::.ctor()
extern "C" void U3CUpdateAchievementU3Ec__AnonStorey1F__ctor_m2245 (U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<UpdateAchievement>c__AnonStorey1F::<>m__17(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CUpdateAchievementU3Ec__AnonStorey1F_U3CU3Em__17_m2246 (U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * __this, FetchResponse_t653 * ___rsp, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		FetchResponse_t653 * L_0 = ___rsp;
		NullCheck(L_0);
		int32_t L_1 = FetchResponse_Status_m2602(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		Action_1_t98 * L_2 = (__this->___callback_0);
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 1);
		NativeClient_t524 * L_3 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_3);
		Dictionary_2_t542 * L_4 = (L_3->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_5 = (__this->___achId_1);
		NullCheck(L_4);
		VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Remove(!0) */, L_4, L_5);
		NativeClient_t524 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		Dictionary_2_t542 * L_7 = (L_6->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_8 = (__this->___achId_1);
		FetchResponse_t653 * L_9 = ___rsp;
		NullCheck(L_9);
		NativeAchievement_t673 * L_10 = FetchResponse_Achievement_m2603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Achievement_t333 * L_11 = NativeAchievement_AsAchievement_m2734(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, Achievement_t333 * >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Add(!0,!1) */, L_7, L_8, L_11);
		goto IL_009c;
	}

IL_0059:
	{
		ObjectU5BU5D_t208* L_12 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral479);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 0)) = (Object_t *)(String_t*) &_stringLiteral479;
		ObjectU5BU5D_t208* L_13 = L_12;
		String_t* L_14 = (__this->___achId_1);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1)) = (Object_t *)L_14;
		ObjectU5BU5D_t208* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, (String_t*) &_stringLiteral480);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2)) = (Object_t *)(String_t*) &_stringLiteral480;
		ObjectU5BU5D_t208* L_16 = L_15;
		FetchResponse_t653 * L_17 = ___rsp;
		NullCheck(L_17);
		int32_t L_18 = FetchResponse_Status_m2602(L_17, /*hidden argument*/NULL);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 3)) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m976(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Action_1_t98 * L_22 = (__this->___callback_0);
		NullCheck(L_22);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_22, 0);
	}

IL_009c:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CInc.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CIncMethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::.ctor()
extern "C" void U3CIncrementAchievementU3Ec__AnonStorey20__ctor_m2247 (U3CIncrementAchievementU3Ec__AnonStorey20_t529 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::<>m__18(GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CIncrementAchievementU3Ec__AnonStorey20_U3CU3Em__18_m2248 (U3CIncrementAchievementU3Ec__AnonStorey20_t529 * __this, FetchResponse_t653 * ___rsp, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		FetchResponse_t653 * L_0 = ___rsp;
		NullCheck(L_0);
		int32_t L_1 = FetchResponse_Status_m2602(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		Action_1_t98 * L_2 = (__this->___callback_0);
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_2, 1);
		NativeClient_t524 * L_3 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_3);
		Dictionary_2_t542 * L_4 = (L_3->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_5 = (__this->___achId_1);
		NullCheck(L_4);
		VirtFuncInvoker1< bool, String_t* >::Invoke(18 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Remove(!0) */, L_4, L_5);
		NativeClient_t524 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		Dictionary_2_t542 * L_7 = (L_6->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_8 = (__this->___achId_1);
		FetchResponse_t653 * L_9 = ___rsp;
		NullCheck(L_9);
		NativeAchievement_t673 * L_10 = FetchResponse_Achievement_m2603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Achievement_t333 * L_11 = NativeAchievement_AsAchievement_m2734(L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, Achievement_t333 * >::Invoke(28 /* System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::Add(!0,!1) */, L_7, L_8, L_11);
		goto IL_009c;
	}

IL_0059:
	{
		ObjectU5BU5D_t208* L_12 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral479);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 0)) = (Object_t *)(String_t*) &_stringLiteral479;
		ObjectU5BU5D_t208* L_13 = L_12;
		String_t* L_14 = (__this->___achId_1);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1)) = (Object_t *)L_14;
		ObjectU5BU5D_t208* L_15 = L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, (String_t*) &_stringLiteral480);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2)) = (Object_t *)(String_t*) &_stringLiteral480;
		ObjectU5BU5D_t208* L_16 = L_15;
		FetchResponse_t653 * L_17 = ___rsp;
		NullCheck(L_17);
		int32_t L_18 = FetchResponse_Status_m2602(L_17, /*hidden argument*/NULL);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 3)) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m976(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		Action_1_t98 * L_22 = (__this->___callback_0);
		NullCheck(L_22);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_22, 0);
	}

IL_009c:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CSho.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CShoMethodDeclarations.h"

// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
#include "mscorlib_System_Action_1_gen_6.h"
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21::.ctor()
extern "C" void U3CShowAchievementsUIU3Ec__AnonStorey21__ctor_m2249 (U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<ShowAchievementsUI>c__AnonStorey21::<>m__19(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" void U3CShowAchievementsUIU3Ec__AnonStorey21_U3CU3Em__19_m2250 (U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * __this, int32_t ___result, MethodInfo* method)
{
	{
		Action_1_t530 * L_0 = (__this->___cb_0);
		int32_t L_1 = ___result;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(!0) */, L_0, L_1);
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CSho_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CSho_0MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22::.ctor()
extern "C" void U3CShowLeaderboardUIU3Ec__AnonStorey22__ctor_m2251 (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22::<>m__1A(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" void U3CShowLeaderboardUIU3Ec__AnonStorey22_U3CU3Em__1A_m2252 (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * __this, int32_t ___result, MethodInfo* method)
{
	{
		Action_1_t530 * L_0 = (__this->___cb_0);
		int32_t L_1 = ___result;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(!0) */, L_0, L_1);
		return;
	}
}
// GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CReg.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeClient_U3CRegMethodDeclarations.h"

// GooglePlayGames.BasicApi.Multiplayer.Invitation
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit_0.h"
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceive.h"
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_InvitationReceiveMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23::.ctor()
extern "C" void U3CRegisterInvitationDelegateU3Ec__AnonStorey23__ctor_m2253 (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient/<RegisterInvitationDelegate>c__AnonStorey23::<>m__1B(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean)
extern "C" void U3CRegisterInvitationDelegateU3Ec__AnonStorey23_U3CU3Em__1B_m2254 (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * __this, Invitation_t341 * ___invitation, bool ___autoAccept, MethodInfo* method)
{
	{
		InvitationReceivedDelegate_t363 * L_0 = (__this->___invitationDelegate_0);
		Invitation_t341 * L_1 = ___invitation;
		bool L_2 = ___autoAccept;
		NullCheck(L_0);
		VirtActionInvoker2< Invitation_t341 *, bool >::Invoke(10 /* System.Void GooglePlayGames.BasicApi.InvitationReceivedDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean) */, L_0, L_1, L_2);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// GooglePlayGames.Native.PInvoke.GameServicesBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_2.h"
// GooglePlayGames.Native.PInvoke.PlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_PlatformCon.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_0.h"
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeTurnB.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_3_gen_2.h"
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Multiplayer.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "System_Core_System_Action_3_gen_3.h"
// GooglePlayGames.Native.PInvoke.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_1.h"
// GooglePlayGames.Native.NativeEventClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClient.h"
// GooglePlayGames.Native.PInvoke.QuestManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_4.h"
// GooglePlayGames.Native.NativeQuestClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_3.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_14.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match_1.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_3.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_34.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_4.h"
// GooglePlayGames.Native.NativeSavedGameClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_10.h"
// GooglePlayGames.Native.UnsupportedSavedGamesClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_UnsupportedSavedGam.h"
// GooglePlayGames.Native.UnsupportedAppStateClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_UnsupportedAppState.h"
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>
#include "System_Core_System_Action_2_gen_2.h"
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_IosPlatform.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Playe.h"
// GooglePlayGames.Native.NativePlayer
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativePlayer.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_17.h"
// GooglePlayGames.Native.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager.h"
// System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
#include "mscorlib_System_Action_1_gen_18.h"
// System.Predicate`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Predicate_1_gen_0.h"
// System.Action`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Action_1_gen_19.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_20.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
#include "mscorlib_System_Action_1_gen_21.h"
// GooglePlayGames.Native.PInvoke.Callbacks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Callbacks.h"
// GooglePlayGames.Native.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_LeaderboardManager.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// GooglePlayGames.OurUtils.PlayGamesHelperObject
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGamesHelperObMethodDeclarations.h"
// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_MiscMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.GameServicesBuilder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_2MethodDeclarations.h"
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_GameService_0MethodDeclarations.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
#include "System_Core_System_Action_3_gen_2MethodDeclarations.h"
// System.Action`3<GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
#include "System_Core_System_Action_3_gen_3MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.EventManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_1MethodDeclarations.h"
// GooglePlayGames.Native.NativeEventClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClientMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.QuestManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_4MethodDeclarations.h"
// GooglePlayGames.Native.NativeQuestClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClientMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.TurnBasedManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_TurnBasedMa_3MethodDeclarations.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeTurnBasedMult_14MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan_3MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_34MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.SnapshotManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_SnapshotMan_4MethodDeclarations.h"
// GooglePlayGames.Native.NativeSavedGameClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeSavedGameClie_10MethodDeclarations.h"
// GooglePlayGames.Native.UnsupportedSavedGamesClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_UnsupportedSavedGamMethodDeclarations.h"
// GooglePlayGames.Native.UnsupportedAppStateClient
#include "AssemblyU2DCSharp_GooglePlayGames_Native_UnsupportedAppStateMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_MultiplayerMethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>
#include "System_Core_System_Action_2_gen_2MethodDeclarations.h"
// GooglePlayGames.GameInfo
#include "AssemblyU2DCSharp_GooglePlayGames_GameInfoMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.IosPlatformConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_IosPlatformMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Achievement_0MethodDeclarations.h"
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManager_FetchMethodDeclarations.h"
// GooglePlayGames.Native.NativePlayer
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativePlayerMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_17MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PlayerManager/FetchSelfResponse>
#include "mscorlib_System_Action_1_gen_18MethodDeclarations.h"
// GooglePlayGames.Native.PlayerManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PlayerManagerMethodDeclarations.h"
// GooglePlayGames.BasicApi.Multiplayer.Player
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_PlayeMethodDeclarations.h"
// System.Predicate`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Predicate_1_gen_0MethodDeclarations.h"
// System.Action`1<GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Action_1_gen_19MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.AchievementManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_20MethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.Callbacks
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_CallbacksMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>
#include "mscorlib_System_Action_1_gen_21MethodDeclarations.h"
// GooglePlayGames.Native.LeaderboardManager
#include "AssemblyU2DCSharp_GooglePlayGames_Native_LeaderboardManagerMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
struct Misc_t391;
// GooglePlayGames.OurUtils.Misc
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc.h"
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.PlayGamesClientConfiguration>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.PlayGamesClientConfiguration>(!!0)
extern "C" PlayGamesClientConfiguration_t366  Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_gshared (Object_t * __this /* static, unused */, PlayGamesClientConfiguration_t366  p0, MethodInfo* method);
#define Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739(__this /* static, unused */, p0, method) (( PlayGamesClientConfiguration_t366  (*) (Object_t * /* static, unused */, PlayGamesClientConfiguration_t366 , MethodInfo*))Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_gshared)(__this /* static, unused */, p0, method)
struct NativeClient_t524;
struct Action_1_t98;
struct NativeClient_t524;
struct Action_1_t910;
// Declaration System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Byte>(System.Action`1<!!0>,!!0)
// System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Byte>(System.Action`1<!!0>,!!0)
extern "C" void NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741_gshared (Object_t * __this /* static, unused */, Action_1_t910 * p0, uint8_t p1, MethodInfo* method);
#define NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, Action_1_t910 *, uint8_t, MethodInfo*))NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Boolean>(System.Action`1<!!0>,!!0)
// System.Void GooglePlayGames.Native.NativeClient::InvokeCallbackOnGameThread<System.Boolean>(System.Action`1<!!0>,!!0)
#define NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, Action_1_t98 *, bool, MethodInfo*))NativeClient_InvokeCallbackOnGameThread_TisByte_t237_m3741_gshared)(__this /* static, unused */, p0, p1, method)
struct NativeClient_t524;
struct Action_1_t98;
struct NativeClient_t524;
struct Action_1_t910;
// Declaration System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Byte>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Byte>(System.Action`1<!!0>)
extern "C" Action_1_t910 * NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743_gshared (Object_t * __this /* static, unused */, Action_1_t910 * p0, MethodInfo* method);
#define NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743(__this /* static, unused */, p0, method) (( Action_1_t910 * (*) (Object_t * /* static, unused */, Action_1_t910 *, MethodInfo*))NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Boolean>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Boolean>(System.Action`1<!!0>)
#define NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742(__this /* static, unused */, p0, method) (( Action_1_t98 * (*) (Object_t * /* static, unused */, Action_1_t98 *, MethodInfo*))NativeClient_AsOnGameThreadCallback_TisByte_t237_m3743_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct String_t;
struct Misc_t391;
struct Object_t;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Object>(!!0)
extern "C" Object_t * Misc_CheckNotNull_TisObject_t_m3706_gshared (Object_t * __this /* static, unused */, Object_t * p0, MethodInfo* method);
#define Misc_CheckNotNull_TisObject_t_m3706(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.String>(!!0)
#define Misc_CheckNotNull_TisString_t_m3705(__this /* static, unused */, p0, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct NativeClient_t524;
struct Action_1_t660;
struct NativeClient_t524;
struct Action_1_t911;
// Declaration System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Int32>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<System.Int32>(System.Action`1<!!0>)
extern "C" Action_1_t911 * NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745_gshared (Object_t * __this /* static, unused */, Action_1_t911 * p0, MethodInfo* method);
#define NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745(__this /* static, unused */, p0, method) (( Action_1_t911 * (*) (Object_t * /* static, unused */, Action_1_t911 *, MethodInfo*))NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>(System.Action`1<!!0>)
// System.Action`1<!!0> GooglePlayGames.Native.NativeClient::AsOnGameThreadCallback<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus>(System.Action`1<!!0>)
#define NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744(__this /* static, unused */, p0, method) (( Action_1_t660 * (*) (Object_t * /* static, unused */, Action_1_t660 *, MethodInfo*))NativeClient_AsOnGameThreadCallback_TisInt32_t189_m3745_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct OnStateLoadedListener_t842;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.OnStateLoadedListener>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.OnStateLoadedListener>(!!0)
#define Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Callbacks_t661;
struct Action_2_t541;
struct Callbacks_t661;
struct Action_2_t912;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Object,System.Byte>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<System.Object,System.Byte>(System.Action`2<!!0,!!1>)
extern "C" Action_2_t912 * Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared (Object_t * __this /* static, unused */, Action_2_t912 * p0, MethodInfo* method);
#define Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748(__this /* static, unused */, p0, method) (( Action_2_t912 * (*) (Object_t * /* static, unused */, Action_2_t912 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.PInvoke.Callbacks::AsOnGameThreadCallback<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>(System.Action`2<!!0,!!1>)
#define Callbacks_AsOnGameThreadCallback_TisInvitation_t341_TisBoolean_t203_m3747(__this /* static, unused */, p0, method) (( Action_2_t541 * (*) (Object_t * /* static, unused */, Action_2_t541 *, MethodInfo*))Callbacks_AsOnGameThreadCallback_TisObject_t_TisByte_t237_m3748_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeClient::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* PlayGamesHelperObject_t392_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_MethodInfo_var;
extern "C" void NativeClient__ctor_m2255 (NativeClient_t524 * __this, PlayGamesClientConfiguration_t366  ___configuration, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		PlayGamesHelperObject_t392_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483841);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m830(L_0, /*hidden argument*/NULL);
		__this->___GameServicesLock_3 = L_0;
		Object_t * L_1 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m830(L_1, /*hidden argument*/NULL);
		__this->___AuthStateLock_4 = L_1;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayGamesHelperObject_t392_il2cpp_TypeInfo_var);
		PlayGamesHelperObject_CreateObject_m1600(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayGamesClientConfiguration_t366  L_2 = ___configuration;
		PlayGamesClientConfiguration_t366  L_3 = Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisPlayGamesClientConfiguration_t366_m3739_MethodInfo_var);
		__this->___mConfiguration_5 = L_3;
		return;
	}
}
// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.NativeClient::GameServices()
extern "C" GameServices_t534 * NativeClient_GameServices_m2256 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	GameServices_t534 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			GameServices_t534 * L_2 = (__this->___mServices_6);
			V_1 = L_2;
			IL2CPP_LEAVE(0x25, FINALLY_001e);
		}

IL_0019:
		{
			; // IL_0019: leave IL_0025
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0025:
	{
		GameServices_t534 * L_4 = V_1;
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern TypeInfo* Action_1_t98_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var;
extern "C" void NativeClient_Authenticate_m2257 (NativeClient_t524 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___AuthStateLock_4);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = (__this->___mAuthState_18);
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_2) == ((uint32_t)1))))
			{
				goto IL_0027;
			}
		}

IL_001b:
		{
			Action_1_t98 * L_3 = ___callback;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_3, 1, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
			IL2CPP_LEAVE(0xB0, FINALLY_0092);
		}

IL_0027:
		{
			bool L_4 = (__this->___mSilentAuthFailed_20);
			il2cpp_codegen_memory_barrier();
			if (!L_4)
			{
				goto IL_0046;
			}
		}

IL_0034:
		{
			bool L_5 = ___silent;
			if (!L_5)
			{
				goto IL_0046;
			}
		}

IL_003a:
		{
			Action_1_t98 * L_6 = ___callback;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_6, 0, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
			IL2CPP_LEAVE(0xB0, FINALLY_0092);
		}

IL_0046:
		{
			Action_1_t98 * L_7 = ___callback;
			if (!L_7)
			{
				goto IL_008d;
			}
		}

IL_004c:
		{
			bool L_8 = ___silent;
			if (!L_8)
			{
				goto IL_0072;
			}
		}

IL_0052:
		{
			Action_1_t98 * L_9 = (__this->___mSilentAuthCallbacks_17);
			il2cpp_codegen_memory_barrier();
			Action_1_t98 * L_10 = ___callback;
			Delegate_t211 * L_11 = Delegate_Combine_m952(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			il2cpp_codegen_memory_barrier();
			__this->___mSilentAuthCallbacks_17 = ((Action_1_t98 *)Castclass(L_11, Action_1_t98_il2cpp_TypeInfo_var));
			goto IL_008d;
		}

IL_0072:
		{
			Action_1_t98 * L_12 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			Action_1_t98 * L_13 = ___callback;
			Delegate_t211 * L_14 = Delegate_Combine_m952(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = ((Action_1_t98 *)Castclass(L_14, Action_1_t98_il2cpp_TypeInfo_var));
		}

IL_008d:
		{
			IL2CPP_LEAVE(0x99, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		Object_t * L_15 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(146)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_JUMP_TBL(0x99, IL_0099)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0099:
	{
		NativeClient_InitializeGameServices_m2258(__this, /*hidden argument*/NULL);
		bool L_16 = ___silent;
		if (L_16)
		{
			goto IL_00b0;
		}
	}
	{
		GameServices_t534 * L_17 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameServices_StartAuthorizationUI_m2664(L_17, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::InitializeGameServices()
extern TypeInfo* AuthFinishedCallback_t665_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t871_il2cpp_TypeInfo_var;
extern TypeInfo* Action_3_t872_il2cpp_TypeInfo_var;
extern TypeInfo* EventManager_t548_il2cpp_TypeInfo_var;
extern TypeInfo* NativeEventClient_t549_il2cpp_TypeInfo_var;
extern TypeInfo* QuestManager_t560_il2cpp_TypeInfo_var;
extern TypeInfo* NativeQuestClient_t561_il2cpp_TypeInfo_var;
extern TypeInfo* TurnBasedManager_t651_il2cpp_TypeInfo_var;
extern TypeInfo* NativeTurnBasedMultiplayerClient_t535_il2cpp_TypeInfo_var;
extern TypeInfo* RealtimeManager_t564_il2cpp_TypeInfo_var;
extern TypeInfo* NativeRealtimeMultiplayerClient_t536_il2cpp_TypeInfo_var;
extern TypeInfo* SnapshotManager_t610_il2cpp_TypeInfo_var;
extern TypeInfo* NativeSavedGameClient_t624_il2cpp_TypeInfo_var;
extern TypeInfo* UnsupportedSavedGamesClient_t714_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_HandleAuthTransition_m2266_MethodInfo_var;
extern MethodInfo* NativeClient_U3CInitializeGameServicesU3Em__10_m2288_MethodInfo_var;
extern MethodInfo* Action_3__ctor_m3749_MethodInfo_var;
extern MethodInfo* NativeClient_HandleInvitation_m2260_MethodInfo_var;
extern MethodInfo* Action_3__ctor_m3750_MethodInfo_var;
extern "C" void NativeClient_InitializeGameServices_m2258 (NativeClient_t524 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AuthFinishedCallback_t665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		Action_3_t871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(656);
		Action_3_t872_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		EventManager_t548_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(658);
		NativeEventClient_t549_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(659);
		QuestManager_t560_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		NativeQuestClient_t561_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		TurnBasedManager_t651_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		NativeTurnBasedMultiplayerClient_t535_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(663);
		RealtimeManager_t564_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		NativeRealtimeMultiplayerClient_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		SnapshotManager_t610_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(666);
		NativeSavedGameClient_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		UnsupportedSavedGamesClient_t714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		NativeClient_HandleAuthTransition_m2266_MethodInfo_var = il2cpp_codegen_method_info_from_index(195);
		NativeClient_U3CInitializeGameServicesU3Em__10_m2288_MethodInfo_var = il2cpp_codegen_method_info_from_index(196);
		Action_3__ctor_m3749_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483845);
		NativeClient_HandleInvitation_m2260_MethodInfo_var = il2cpp_codegen_method_info_from_index(198);
		Action_3__ctor_m3750_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483847);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	GameServicesBuilder_t667 * V_1 = {0};
	PlatformConfiguration_t669 * V_2 = {0};
	PlayGamesClientConfiguration_t366  V_3 = {0};
	PlayGamesClientConfiguration_t366  V_4 = {0};
	PlayGamesClientConfiguration_t366  V_5 = {0};
	PlayGamesClientConfiguration_t366  V_6 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			GameServices_t534 * L_2 = (__this->___mServices_6);
			if (!L_2)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			IL2CPP_LEAVE(0x1A2, FINALLY_019b);
		}

IL_001d:
		{
			GameServicesBuilder_t667 * L_3 = GameServicesBuilder_Create_m2690(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_3;
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			{
				PlatformConfiguration_t669 * L_4 = NativeClient_CreatePlatformConfiguration_m2261(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_2 = L_4;
			}

IL_0029:
			try
			{ // begin try (depth: 3)
				{
					PlayGamesClientConfiguration_t366  L_5 = (__this->___mConfiguration_5);
					V_3 = L_5;
					InvitationReceivedDelegate_t363 * L_6 = PlayGamesClientConfiguration_get_InvitationDelegate_m1481((&V_3), /*hidden argument*/NULL);
					VirtActionInvoker1< InvitationReceivedDelegate_t363 * >::Invoke(24 /* System.Void GooglePlayGames.Native.NativeClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate) */, __this, L_6);
					GameServicesBuilder_t667 * L_7 = V_1;
					IntPtr_t L_8 = { NativeClient_HandleAuthTransition_m2266_MethodInfo_var };
					AuthFinishedCallback_t665 * L_9 = (AuthFinishedCallback_t665 *)il2cpp_codegen_object_new (AuthFinishedCallback_t665_il2cpp_TypeInfo_var);
					AuthFinishedCallback__ctor_m2670(L_9, __this, L_8, /*hidden argument*/NULL);
					NullCheck(L_7);
					GameServicesBuilder_SetOnAuthFinishedCallback_m2679(L_7, L_9, /*hidden argument*/NULL);
					GameServicesBuilder_t667 * L_10 = V_1;
					IntPtr_t L_11 = { NativeClient_U3CInitializeGameServicesU3Em__10_m2288_MethodInfo_var };
					Action_3_t871 * L_12 = (Action_3_t871 *)il2cpp_codegen_object_new (Action_3_t871_il2cpp_TypeInfo_var);
					Action_3__ctor_m3749(L_12, __this, L_11, /*hidden argument*/Action_3__ctor_m3749_MethodInfo_var);
					NullCheck(L_10);
					GameServicesBuilder_SetOnTurnBasedMatchEventCallback_m2686(L_10, L_12, /*hidden argument*/NULL);
					GameServicesBuilder_t667 * L_13 = V_1;
					IntPtr_t L_14 = { NativeClient_HandleInvitation_m2260_MethodInfo_var };
					Action_3_t872 * L_15 = (Action_3_t872 *)il2cpp_codegen_object_new (Action_3_t872_il2cpp_TypeInfo_var);
					Action_3__ctor_m3750(L_15, __this, L_14, /*hidden argument*/Action_3__ctor_m3750_MethodInfo_var);
					NullCheck(L_13);
					GameServicesBuilder_SetOnMultiplayerInvitationEventCallback_m2688(L_13, L_15, /*hidden argument*/NULL);
					PlayGamesClientConfiguration_t366  L_16 = (__this->___mConfiguration_5);
					V_4 = L_16;
					bool L_17 = PlayGamesClientConfiguration_get_EnableSavedGames_m1479((&V_4), /*hidden argument*/NULL);
					if (!L_17)
					{
						goto IL_008d;
					}
				}

IL_0087:
				{
					GameServicesBuilder_t667 * L_18 = V_1;
					NullCheck(L_18);
					GameServicesBuilder_EnableSnapshots_m2680(L_18, /*hidden argument*/NULL);
				}

IL_008d:
				{
					GameServicesBuilder_t667 * L_19 = V_1;
					PlatformConfiguration_t669 * L_20 = V_2;
					NullCheck(L_19);
					GameServices_t534 * L_21 = GameServicesBuilder_Build_m2689(L_19, L_20, /*hidden argument*/NULL);
					__this->___mServices_6 = L_21;
					GameServices_t534 * L_22 = (__this->___mServices_6);
					EventManager_t548 * L_23 = (EventManager_t548 *)il2cpp_codegen_object_new (EventManager_t548_il2cpp_TypeInfo_var);
					EventManager__ctor_m2655(L_23, L_22, /*hidden argument*/NULL);
					NativeEventClient_t549 * L_24 = (NativeEventClient_t549 *)il2cpp_codegen_object_new (NativeEventClient_t549_il2cpp_TypeInfo_var);
					NativeEventClient__ctor_m2295(L_24, L_23, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mEventsClient_10 = L_24;
					GameServices_t534 * L_25 = (__this->___mServices_6);
					QuestManager_t560 * L_26 = (QuestManager_t560 *)il2cpp_codegen_object_new (QuestManager_t560_il2cpp_TypeInfo_var);
					QuestManager__ctor_m2922(L_26, L_25, /*hidden argument*/NULL);
					NativeQuestClient_t561 * L_27 = (NativeQuestClient_t561 *)il2cpp_codegen_object_new (NativeQuestClient_t561_il2cpp_TypeInfo_var);
					NativeQuestClient__ctor_m2309(L_27, L_26, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mQuestsClient_11 = L_27;
					GameServices_t534 * L_28 = (__this->___mServices_6);
					TurnBasedManager_t651 * L_29 = (TurnBasedManager_t651 *)il2cpp_codegen_object_new (TurnBasedManager_t651_il2cpp_TypeInfo_var);
					TurnBasedManager__ctor_m3080(L_29, L_28, /*hidden argument*/NULL);
					NativeTurnBasedMultiplayerClient_t535 * L_30 = (NativeTurnBasedMultiplayerClient_t535 *)il2cpp_codegen_object_new (NativeTurnBasedMultiplayerClient_t535_il2cpp_TypeInfo_var);
					NativeTurnBasedMultiplayerClient__ctor_m2579(L_30, __this, L_29, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mTurnBasedClient_7 = L_30;
					NativeTurnBasedMultiplayerClient_t535 * L_31 = (__this->___mTurnBasedClient_7);
					il2cpp_codegen_memory_barrier();
					PlayGamesClientConfiguration_t366  L_32 = (__this->___mConfiguration_5);
					V_5 = L_32;
					MatchDelegate_t364 * L_33 = PlayGamesClientConfiguration_get_MatchDelegate_m1482((&V_5), /*hidden argument*/NULL);
					NullCheck(L_31);
					VirtActionInvoker1< MatchDelegate_t364 * >::Invoke(8 /* System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::RegisterMatchDelegate(GooglePlayGames.BasicApi.Multiplayer.MatchDelegate) */, L_31, L_33);
					GameServices_t534 * L_34 = (__this->___mServices_6);
					RealtimeManager_t564 * L_35 = (RealtimeManager_t564 *)il2cpp_codegen_object_new (RealtimeManager_t564_il2cpp_TypeInfo_var);
					RealtimeManager__ctor_m2976(L_35, L_34, /*hidden argument*/NULL);
					NativeRealtimeMultiplayerClient_t536 * L_36 = (NativeRealtimeMultiplayerClient_t536 *)il2cpp_codegen_object_new (NativeRealtimeMultiplayerClient_t536_il2cpp_TypeInfo_var);
					NativeRealtimeMultiplayerClient__ctor_m2477(L_36, __this, L_35, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mRealTimeClient_8 = L_36;
					PlayGamesClientConfiguration_t366  L_37 = (__this->___mConfiguration_5);
					V_6 = L_37;
					bool L_38 = PlayGamesClientConfiguration_get_EnableSavedGames_m1479((&V_6), /*hidden argument*/NULL);
					if (!L_38)
					{
						goto IL_0149;
					}
				}

IL_012c:
				{
					GameServices_t534 * L_39 = (__this->___mServices_6);
					SnapshotManager_t610 * L_40 = (SnapshotManager_t610 *)il2cpp_codegen_object_new (SnapshotManager_t610_il2cpp_TypeInfo_var);
					SnapshotManager__ctor_m3046(L_40, L_39, /*hidden argument*/NULL);
					IL2CPP_RUNTIME_CLASS_INIT(NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
					NativeSavedGameClient_t624 * L_41 = (NativeSavedGameClient_t624 *)il2cpp_codegen_object_new (NativeSavedGameClient_t624_il2cpp_TypeInfo_var);
					NativeSavedGameClient__ctor_m2524(L_41, L_40, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mSavedGameClient_9 = L_41;
					goto IL_015b;
				}

IL_0149:
				{
					UnsupportedSavedGamesClient_t714 * L_42 = (UnsupportedSavedGamesClient_t714 *)il2cpp_codegen_object_new (UnsupportedSavedGamesClient_t714_il2cpp_TypeInfo_var);
					UnsupportedSavedGamesClient__ctor_m3124(L_42, (String_t*) &_stringLiteral438, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mSavedGameClient_9 = L_42;
				}

IL_015b:
				{
					Object_t * L_43 = NativeClient_CreateAppStateClient_m2259(__this, /*hidden argument*/NULL);
					il2cpp_codegen_memory_barrier();
					__this->___mAppStateClient_12 = L_43;
					il2cpp_codegen_memory_barrier();
					__this->___mAuthState_18 = 2;
					IL2CPP_LEAVE(0x184, FINALLY_0177);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_0177;
			}

FINALLY_0177:
			{ // begin finally (depth: 3)
				{
					PlatformConfiguration_t669 * L_44 = V_2;
					if (!L_44)
					{
						goto IL_0183;
					}
				}

IL_017d:
				{
					PlatformConfiguration_t669 * L_45 = V_2;
					NullCheck(L_45);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_45);
				}

IL_0183:
				{
					IL2CPP_END_FINALLY(375)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(375)
			{
				IL2CPP_JUMP_TBL(0x184, IL_0184)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_0184:
			{
				IL2CPP_LEAVE(0x196, FINALLY_0189);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_0189;
		}

FINALLY_0189:
		{ // begin finally (depth: 2)
			{
				GameServicesBuilder_t667 * L_46 = V_1;
				if (!L_46)
				{
					goto IL_0195;
				}
			}

IL_018f:
			{
				GameServicesBuilder_t667 * L_47 = V_1;
				NullCheck(L_47);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_47);
			}

IL_0195:
			{
				IL2CPP_END_FINALLY(393)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(393)
		{
			IL2CPP_JUMP_TBL(0x196, IL_0196)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_0196:
		{
			IL2CPP_LEAVE(0x1A2, FINALLY_019b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_019b;
	}

FINALLY_019b:
	{ // begin finally (depth: 1)
		Object_t * L_48 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(411)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(411)
	{
		IL2CPP_JUMP_TBL(0x1A2, IL_01a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_01a2:
	{
		return;
	}
}
// GooglePlayGames.Native.AppStateClient GooglePlayGames.Native.NativeClient::CreateAppStateClient()
extern TypeInfo* UnsupportedAppStateClient_t713_il2cpp_TypeInfo_var;
extern "C" Object_t * NativeClient_CreateAppStateClient_m2259 (NativeClient_t524 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnsupportedAppStateClient_t713_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(669);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnsupportedAppStateClient_t713 * L_0 = (UnsupportedAppStateClient_t713 *)il2cpp_codegen_object_new (UnsupportedAppStateClient_t713_il2cpp_TypeInfo_var);
		UnsupportedAppStateClient__ctor_m3121(L_0, (String_t*) &_stringLiteral439, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::HandleInvitation(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* MultiplayerEvent_t518_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void NativeClient_HandleInvitation_m2260 (NativeClient_t524 * __this, int32_t ___eventType, String_t* ___invitationId, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		MultiplayerEvent_t518_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	Action_2_t541 * V_0 = {0};
	bool V_1 = false;
	{
		Action_2_t541 * L_0 = (__this->___mInvitationDelegate_13);
		il2cpp_codegen_memory_barrier();
		V_0 = L_0;
		Action_2_t541 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t208* L_2 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 5));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, (String_t*) &_stringLiteral440);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0)) = (Object_t *)(String_t*) &_stringLiteral440;
		ObjectU5BU5D_t208* L_3 = L_2;
		int32_t L_4 = ___eventType;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(MultiplayerEvent_t518_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t208* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral441);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)(String_t*) &_stringLiteral441;
		ObjectU5BU5D_t208* L_8 = L_7;
		String_t* L_9 = ___invitationId;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_9;
		ObjectU5BU5D_t208* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, (String_t*) &_stringLiteral442);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4)) = (Object_t *)(String_t*) &_stringLiteral442;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m976(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0045:
	{
		int32_t L_12 = ___eventType;
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_13 = ___invitationId;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral443, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}

IL_005d:
	{
		int32_t L_15 = ___eventType;
		V_1 = ((((int32_t)L_15) == ((int32_t)2))? 1 : 0);
		Action_2_t541 * L_16 = V_0;
		MultiplayerInvitation_t601 * L_17 = ___invitation;
		NullCheck(L_17);
		Invitation_t341 * L_18 = MultiplayerInvitation_AsInvitation_m2708(L_17, /*hidden argument*/NULL);
		bool L_19 = V_1;
		NullCheck(L_16);
		VirtActionInvoker2< Invitation_t341 *, bool >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.Multiplayer.Invitation,System.Boolean>::Invoke(!0,!1) */, L_16, L_18, L_19);
		return;
	}
}
// GooglePlayGames.Native.PInvoke.PlatformConfiguration GooglePlayGames.Native.NativeClient::CreatePlatformConfiguration()
extern TypeInfo* InvalidOperationException_t905_il2cpp_TypeInfo_var;
extern "C" PlatformConfiguration_t669 * NativeClient_CreatePlatformConfiguration_m2261 (Object_t * __this /* static, unused */, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	IosPlatformConfiguration_t668 * V_0 = {0};
	{
		bool L_0 = GameInfo_IosClientIdInitialized_m1495(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t905 * L_1 = (InvalidOperationException_t905 *)il2cpp_codegen_object_new (InvalidOperationException_t905_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m3712(L_1, (String_t*) &_stringLiteral444, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		IosPlatformConfiguration_t668 * L_2 = IosPlatformConfiguration_Create_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		IosPlatformConfiguration_t668 * L_3 = V_0;
		NullCheck(L_3);
		IosPlatformConfiguration_SetClientId_m2692(L_3, (String_t*) &_stringLiteral366, /*hidden argument*/NULL);
		IosPlatformConfiguration_t668 * L_4 = V_0;
		return L_4;
	}
}
// System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated()
extern "C" bool NativeClient_IsAuthenticated_m2262 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	bool V_1 = false;
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___AuthStateLock_4);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = (__this->___mAuthState_18);
			il2cpp_codegen_memory_barrier();
			V_1 = ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}

IL_001e:
		{
			; // IL_001e: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_002a:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::PopulateAchievements(System.UInt32,GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* ResponseStatus_t409_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t542_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t865_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t191_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t189_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var;
extern MethodInfo* Dictionary_2__ctor_m3751_MethodInfo_var;
extern "C" void NativeClient_PopulateAchievements_m2263 (NativeClient_t524 * __this, uint32_t ___authGeneration, FetchAllResponse_t655 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		ResponseStatus_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Dictionary_2_t542_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(670);
		IEnumerator_1_t865_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(671);
		IDisposable_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		Int32_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		Dictionary_2__ctor_m3751_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483848);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Action_1_t98 * V_1 = {0};
	Dictionary_2_t542 * V_2 = {0};
	NativeAchievement_t673 * V_3 = {0};
	Object_t* V_4 = {0};
	NativeAchievement_t673 * V_5 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		uint32_t L_0 = ___authGeneration;
		uint32_t L_1 = (__this->___mAuthGeneration_19);
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral445, /*hidden argument*/NULL);
		return;
	}

IL_0019:
	{
		FetchAllResponse_t655 * L_2 = ___response;
		NullCheck(L_2);
		int32_t L_3 = FetchAllResponse_Status_m2608(L_2, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(ResponseStatus_t409_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral446, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Object_t * L_7 = (__this->___AuthStateLock_4);
		V_0 = L_7;
		Object_t * L_8 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		{
			FetchAllResponse_t655 * L_9 = ___response;
			NullCheck(L_9);
			int32_t L_10 = FetchAllResponse_Status_m2608(L_9, /*hidden argument*/NULL);
			if ((((int32_t)L_10) == ((int32_t)1)))
			{
				goto IL_008c;
			}
		}

IL_004c:
		{
			FetchAllResponse_t655 * L_11 = ___response;
			NullCheck(L_11);
			int32_t L_12 = FetchAllResponse_Status_m2608(L_11, /*hidden argument*/NULL);
			if ((((int32_t)L_12) == ((int32_t)2)))
			{
				goto IL_008c;
			}
		}

IL_0058:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral447, /*hidden argument*/NULL);
			Action_1_t98 * L_13 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			V_1 = L_13;
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = (Action_1_t98 *)NULL;
			Action_1_t98 * L_14 = V_1;
			if (!L_14)
			{
				goto IL_0081;
			}
		}

IL_007a:
		{
			Action_1_t98 * L_15 = V_1;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_15, 0, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
		}

IL_0081:
		{
			VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeClient::SignOut() */, __this);
			IL2CPP_LEAVE(0x132, FINALLY_011b);
		}

IL_008c:
		{
			Dictionary_2_t542 * L_16 = (Dictionary_2_t542 *)il2cpp_codegen_object_new (Dictionary_2_t542_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m3751(L_16, /*hidden argument*/Dictionary_2__ctor_m3751_MethodInfo_var);
			V_2 = L_16;
			FetchAllResponse_t655 * L_17 = ___response;
			NullCheck(L_17);
			Object_t* L_18 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(7 /* System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement> GooglePlayGames.Native.PInvoke.AchievementManager/FetchAllResponse::GetEnumerator() */, L_17);
			V_4 = L_18;
		}

IL_009a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00d0;
			}

IL_009f:
			{
				Object_t* L_19 = V_4;
				NullCheck(L_19);
				NativeAchievement_t673 * L_20 = (NativeAchievement_t673 *)InterfaceFuncInvoker0< NativeAchievement_t673 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<GooglePlayGames.Native.NativeAchievement>::get_Current() */, IEnumerator_1_t865_il2cpp_TypeInfo_var, L_19);
				V_3 = L_20;
				NativeAchievement_t673 * L_21 = V_3;
				V_5 = L_21;
			}

IL_00aa:
			try
			{ // begin try (depth: 3)
				Dictionary_2_t542 * L_22 = V_2;
				NativeAchievement_t673 * L_23 = V_3;
				NullCheck(L_23);
				String_t* L_24 = NativeAchievement_Id_m2728(L_23, /*hidden argument*/NULL);
				NativeAchievement_t673 * L_25 = V_3;
				NullCheck(L_25);
				Achievement_t333 * L_26 = NativeAchievement_AsAchievement_m2734(L_25, /*hidden argument*/NULL);
				NullCheck(L_22);
				VirtActionInvoker2< String_t*, Achievement_t333 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::set_Item(!0,!1) */, L_22, L_24, L_26);
				IL2CPP_LEAVE(0xD0, FINALLY_00c1);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t135 *)e.ex;
				goto FINALLY_00c1;
			}

FINALLY_00c1:
			{ // begin finally (depth: 3)
				{
					NativeAchievement_t673 * L_27 = V_5;
					if (!L_27)
					{
						goto IL_00cf;
					}
				}

IL_00c8:
				{
					NativeAchievement_t673 * L_28 = V_5;
					NullCheck(L_28);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_28);
				}

IL_00cf:
				{
					IL2CPP_END_FINALLY(193)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(193)
			{
				IL2CPP_JUMP_TBL(0xD0, IL_00d0)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
			}

IL_00d0:
			{
				Object_t* L_29 = V_4;
				NullCheck(L_29);
				bool L_30 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t37_il2cpp_TypeInfo_var, L_29);
				if (L_30)
				{
					goto IL_009f;
				}
			}

IL_00dc:
			{
				IL2CPP_LEAVE(0xEE, FINALLY_00e1);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t135 *)e.ex;
			goto FINALLY_00e1;
		}

FINALLY_00e1:
		{ // begin finally (depth: 2)
			{
				Object_t* L_31 = V_4;
				if (L_31)
				{
					goto IL_00e6;
				}
			}

IL_00e5:
			{
				IL2CPP_END_FINALLY(225)
			}

IL_00e6:
			{
				Object_t* L_32 = V_4;
				NullCheck(L_32);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t191_il2cpp_TypeInfo_var, L_32);
				IL2CPP_END_FINALLY(225)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(225)
		{
			IL2CPP_JUMP_TBL(0xEE, IL_00ee)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
		}

IL_00ee:
		{
			Dictionary_2_t542 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Count() */, L_33);
			int32_t L_35 = L_34;
			Object_t * L_36 = Box(Int32_t189_il2cpp_TypeInfo_var, &L_35);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_37 = String_Concat_m995(NULL /*static, unused*/, (String_t*) &_stringLiteral448, L_36, (String_t*) &_stringLiteral449, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			Dictionary_2_t542 * L_38 = V_2;
			il2cpp_codegen_memory_barrier();
			__this->___mAchievements_14 = L_38;
			IL2CPP_LEAVE(0x122, FINALLY_011b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_011b;
	}

FINALLY_011b:
	{ // begin finally (depth: 1)
		Object_t * L_39 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(283)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(283)
	{
		IL2CPP_JUMP_TBL(0x132, IL_0132)
		IL2CPP_JUMP_TBL(0x122, IL_0122)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral450, /*hidden argument*/NULL);
		NativeClient_MaybeFinishAuthentication_m2264(__this, /*hidden argument*/NULL);
	}

IL_0132:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::MaybeFinishAuthentication()
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var;
extern "C" void NativeClient_MaybeFinishAuthentication_m2264 (NativeClient_t524 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t98 * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Action_1_t98 *)NULL;
		Object_t * L_0 = (__this->___AuthStateLock_4);
		V_1 = L_0;
		Object_t * L_1 = V_1;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			Player_t347 * L_2 = (__this->___mUser_15);
			il2cpp_codegen_memory_barrier();
			if (!L_2)
			{
				goto IL_0029;
			}
		}

IL_001c:
		{
			Dictionary_2_t542 * L_3 = (__this->___mAchievements_14);
			il2cpp_codegen_memory_barrier();
			if (L_3)
			{
				goto IL_0064;
			}
		}

IL_0029:
		{
			ObjectU5BU5D_t208* L_4 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
			ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral451);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0)) = (Object_t *)(String_t*) &_stringLiteral451;
			ObjectU5BU5D_t208* L_5 = L_4;
			Player_t347 * L_6 = (__this->___mUser_15);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
			ArrayElementTypeCheck (L_5, L_6);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1)) = (Object_t *)L_6;
			ObjectU5BU5D_t208* L_7 = L_5;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
			ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral452);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)(String_t*) &_stringLiteral452;
			ObjectU5BU5D_t208* L_8 = L_7;
			Dictionary_2_t542 * L_9 = (__this->___mAchievements_14);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
			ArrayElementTypeCheck (L_8, L_9);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m976(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xB2, FINALLY_008e);
		}

IL_0064:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral453, /*hidden argument*/NULL);
			Action_1_t98 * L_11 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			V_0 = L_11;
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = (Action_1_t98 *)NULL;
			il2cpp_codegen_memory_barrier();
			__this->___mAuthState_18 = 1;
			IL2CPP_LEAVE(0x95, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		Object_t * L_12 = V_1;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0095:
	{
		Action_1_t98 * L_13 = V_0;
		if (!L_13)
		{
			goto IL_00b2;
		}
	}
	{
		Action_1_t98 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral454, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Action_1_t98 * L_16 = V_0;
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_16, 1, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
	}

IL_00b2:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::PopulateUser(System.UInt32,GooglePlayGames.Native.PlayerManager/FetchSelfResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var;
extern "C" void NativeClient_PopulateUser_m2265 (NativeClient_t524 * __this, uint32_t ___authGeneration, FetchSelfResponse_t684 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Action_1_t98 * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral455, /*hidden argument*/NULL);
		uint32_t L_0 = ___authGeneration;
		uint32_t L_1 = (__this->___mAuthGeneration_19);
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral456, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		Object_t * L_2 = (__this->___AuthStateLock_4);
		V_0 = L_2;
		Object_t * L_3 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0030:
	try
	{ // begin try (depth: 1)
		{
			FetchSelfResponse_t684 * L_4 = ___response;
			NullCheck(L_4);
			int32_t L_5 = FetchSelfResponse_Status_m2871(L_4, /*hidden argument*/NULL);
			if ((((int32_t)L_5) == ((int32_t)1)))
			{
				goto IL_007c;
			}
		}

IL_003c:
		{
			FetchSelfResponse_t684 * L_6 = ___response;
			NullCheck(L_6);
			int32_t L_7 = FetchSelfResponse_Status_m2871(L_6, /*hidden argument*/NULL);
			if ((((int32_t)L_7) == ((int32_t)2)))
			{
				goto IL_007c;
			}
		}

IL_0048:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral457, /*hidden argument*/NULL);
			Action_1_t98 * L_8 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			V_1 = L_8;
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = (Action_1_t98 *)NULL;
			Action_1_t98 * L_9 = V_1;
			if (!L_9)
			{
				goto IL_0071;
			}
		}

IL_006a:
		{
			Action_1_t98 * L_10 = V_1;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_10, 0, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
		}

IL_0071:
		{
			VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeClient::SignOut() */, __this);
			IL2CPP_LEAVE(0xC2, FINALLY_0094);
		}

IL_007c:
		{
			FetchSelfResponse_t684 * L_11 = ___response;
			NullCheck(L_11);
			NativePlayer_t675 * L_12 = FetchSelfResponse_Self_m2872(L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			Player_t347 * L_13 = NativePlayer_AsPlayer_m2756(L_12, /*hidden argument*/NULL);
			il2cpp_codegen_memory_barrier();
			__this->___mUser_15 = L_13;
			IL2CPP_LEAVE(0x9B, FINALLY_0094);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0094;
	}

FINALLY_0094:
	{ // begin finally (depth: 1)
		Object_t * L_14 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(148)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(148)
	{
		IL2CPP_JUMP_TBL(0xC2, IL_00c2)
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_009b:
	{
		Player_t347 * L_15 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral458, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Logger_d_m1591(NULL /*static, unused*/, (String_t*) &_stringLiteral459, /*hidden argument*/NULL);
		NativeClient_MaybeFinishAuthentication_m2264(__this, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::HandleAuthTransition(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus)
extern TypeInfo* ObjectU5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* AuthOperation_t504_il2cpp_TypeInfo_var;
extern TypeInfo* AuthStatus_t411_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t98_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t866_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t875_il2cpp_TypeInfo_var;
extern MethodInfo* U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__11_m2239_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3752_MethodInfo_var;
extern MethodInfo* U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__12_m2240_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3753_MethodInfo_var;
extern MethodInfo* NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var;
extern "C" void NativeClient_HandleAuthTransition_m2266 (NativeClient_t524 * __this, int32_t ___operation, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		AuthOperation_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		AuthStatus_t411_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(675);
		Action_1_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		Action_1_t866_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(676);
		Action_1_t875_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(677);
		U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__11_m2239_MethodInfo_var = il2cpp_codegen_method_info_from_index(201);
		Action_1__ctor_m3752_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483850);
		U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__12_m2240_MethodInfo_var = il2cpp_codegen_method_info_from_index(203);
		Action_1__ctor_m3753_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483852);
		NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483842);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Action_1_t98 * V_1 = {0};
	Action_1_t98 * V_2 = {0};
	int32_t V_3 = {0};
	U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * V_4 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectU5BU5D_t208* L_0 = ((ObjectU5BU5D_t208*)SZArrayNew(ObjectU5BU5D_t208_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral460);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral460;
		ObjectU5BU5D_t208* L_1 = L_0;
		int32_t L_2 = ___operation;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t208* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, (String_t*) &_stringLiteral461);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)(String_t*) &_stringLiteral461;
		ObjectU5BU5D_t208* L_6 = L_5;
		int32_t L_7 = ___status;
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(AuthStatus_t411_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m976(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Object_t * L_11 = (__this->___AuthStateLock_4);
		V_0 = L_11;
		Object_t * L_12 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_13 = ___operation;
			V_3 = L_13;
			int32_t L_14 = V_3;
			if ((((int32_t)L_14) == ((int32_t)1)))
			{
				goto IL_0054;
			}
		}

IL_0048:
		{
			int32_t L_15 = V_3;
			if ((((int32_t)L_15) == ((int32_t)2)))
			{
				goto IL_0164;
			}
		}

IL_004f:
		{
			goto IL_0172;
		}

IL_0054:
		{
			int32_t L_16 = ___status;
			if ((!(((uint32_t)L_16) == ((uint32_t)1))))
			{
				goto IL_00f0;
			}
		}

IL_005b:
		{
			U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * L_17 = (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 *)il2cpp_codegen_object_new (U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525_il2cpp_TypeInfo_var);
			U3CHandleAuthTransitionU3Ec__AnonStorey1C__ctor_m2238(L_17, /*hidden argument*/NULL);
			V_4 = L_17;
			U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * L_18 = V_4;
			NullCheck(L_18);
			L_18->___U3CU3Ef__this_1 = __this;
			Action_1_t98 * L_19 = (__this->___mSilentAuthCallbacks_17);
			il2cpp_codegen_memory_barrier();
			if (!L_19)
			{
				goto IL_00a2;
			}
		}

IL_0077:
		{
			Action_1_t98 * L_20 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			Action_1_t98 * L_21 = (__this->___mSilentAuthCallbacks_17);
			il2cpp_codegen_memory_barrier();
			Delegate_t211 * L_22 = Delegate_Combine_m952(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = ((Action_1_t98 *)Castclass(L_22, Action_1_t98_il2cpp_TypeInfo_var));
			il2cpp_codegen_memory_barrier();
			__this->___mSilentAuthCallbacks_17 = (Action_1_t98 *)NULL;
		}

IL_00a2:
		{
			U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * L_23 = V_4;
			uint32_t L_24 = (__this->___mAuthGeneration_19);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_23);
			L_23->___currentAuthGeneration_0 = L_24;
			GameServices_t534 * L_25 = (__this->___mServices_6);
			NullCheck(L_25);
			AchievementManager_t656 * L_26 = GameServices_AchievementManager_m2665(L_25, /*hidden argument*/NULL);
			U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * L_27 = V_4;
			IntPtr_t L_28 = { U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__11_m2239_MethodInfo_var };
			Action_1_t866 * L_29 = (Action_1_t866 *)il2cpp_codegen_object_new (Action_1_t866_il2cpp_TypeInfo_var);
			Action_1__ctor_m3752(L_29, L_27, L_28, /*hidden argument*/Action_1__ctor_m3752_MethodInfo_var);
			NullCheck(L_26);
			AchievementManager_FetchAll_m2617(L_26, L_29, /*hidden argument*/NULL);
			GameServices_t534 * L_30 = (__this->___mServices_6);
			NullCheck(L_30);
			PlayerManager_t685 * L_31 = GameServices_PlayerManager_m2667(L_30, /*hidden argument*/NULL);
			U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525 * L_32 = V_4;
			IntPtr_t L_33 = { U3CHandleAuthTransitionU3Ec__AnonStorey1C_U3CU3Em__12_m2240_MethodInfo_var };
			Action_1_t875 * L_34 = (Action_1_t875 *)il2cpp_codegen_object_new (Action_1_t875_il2cpp_TypeInfo_var);
			Action_1__ctor_m3753(L_34, L_32, L_33, /*hidden argument*/Action_1__ctor_m3753_MethodInfo_var);
			NullCheck(L_31);
			PlayerManager_FetchSelf_m2876(L_31, L_34, /*hidden argument*/NULL);
			goto IL_015f;
		}

IL_00f0:
		{
			int32_t L_35 = (__this->___mAuthState_18);
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_35) == ((uint32_t)2))))
			{
				goto IL_0146;
			}
		}

IL_00fe:
		{
			il2cpp_codegen_memory_barrier();
			__this->___mSilentAuthFailed_20 = 1;
			il2cpp_codegen_memory_barrier();
			__this->___mAuthState_18 = 0;
			Action_1_t98 * L_36 = (__this->___mSilentAuthCallbacks_17);
			il2cpp_codegen_memory_barrier();
			V_1 = L_36;
			il2cpp_codegen_memory_barrier();
			__this->___mSilentAuthCallbacks_17 = (Action_1_t98 *)NULL;
			Action_1_t98 * L_37 = V_1;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_37, 0, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
			Action_1_t98 * L_38 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			if (!L_38)
			{
				goto IL_0141;
			}
		}

IL_0136:
		{
			GameServices_t534 * L_39 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
			NullCheck(L_39);
			GameServices_StartAuthorizationUI_m2664(L_39, /*hidden argument*/NULL);
		}

IL_0141:
		{
			goto IL_015f;
		}

IL_0146:
		{
			Action_1_t98 * L_40 = (__this->___mPendingAuthCallbacks_16);
			il2cpp_codegen_memory_barrier();
			V_2 = L_40;
			il2cpp_codegen_memory_barrier();
			__this->___mPendingAuthCallbacks_16 = (Action_1_t98 *)NULL;
			Action_1_t98 * L_41 = V_2;
			NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740(NULL /*static, unused*/, L_41, 0, /*hidden argument*/NativeClient_InvokeCallbackOnGameThread_TisBoolean_t203_m3740_MethodInfo_var);
		}

IL_015f:
		{
			goto IL_018c;
		}

IL_0164:
		{
			il2cpp_codegen_memory_barrier();
			__this->___mAuthState_18 = 0;
			goto IL_018c;
		}

IL_0172:
		{
			int32_t L_42 = ___operation;
			int32_t L_43 = L_42;
			Object_t * L_44 = Box(AuthOperation_t504_il2cpp_TypeInfo_var, &L_43);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_45 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral462, L_44, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
			goto IL_018c;
		}

IL_018c:
		{
			IL2CPP_LEAVE(0x198, FINALLY_0191);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0191;
	}

FINALLY_0191:
	{ // begin finally (depth: 1)
		Object_t * L_46 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(401)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(401)
	{
		IL2CPP_JUMP_TBL(0x198, IL_0198)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0198:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::ToUnauthenticated()
extern "C" void NativeClient_ToUnauthenticated_m2267 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___AuthStateLock_4);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		il2cpp_codegen_memory_barrier();
		__this->___mUser_15 = (Player_t347 *)NULL;
		il2cpp_codegen_memory_barrier();
		__this->___mAchievements_14 = (Dictionary_2_t542 *)NULL;
		il2cpp_codegen_memory_barrier();
		__this->___mAuthState_18 = 0;
		uint32_t L_2 = (__this->___mAuthGeneration_19);
		il2cpp_codegen_memory_barrier();
		il2cpp_codegen_memory_barrier();
		__this->___mAuthGeneration_19 = ((int32_t)((int32_t)L_2+(int32_t)1));
		IL2CPP_LEAVE(0x46, FINALLY_003f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0046:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::SignOut()
extern "C" void NativeClient_SignOut_m2268 (NativeClient_t524 * __this, MethodInfo* method)
{
	{
		NativeClient_ToUnauthenticated_m2267(__this, /*hidden argument*/NULL);
		GameServices_t534 * L_0 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		GameServices_t534 * L_1 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameServices_SignOut_m2663(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String GooglePlayGames.Native.NativeClient::GetUserId()
extern "C" String_t* NativeClient_GetUserId_m2269 (NativeClient_t524 * __this, MethodInfo* method)
{
	{
		Player_t347 * L_0 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000f:
	{
		Player_t347 * L_1 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		String_t* L_2 = Player_get_PlayerId_m1398(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeClient::GetUserDisplayName()
extern "C" String_t* NativeClient_GetUserDisplayName_m2270 (NativeClient_t524 * __this, MethodInfo* method)
{
	{
		Player_t347 * L_0 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000f:
	{
		Player_t347 * L_1 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		String_t* L_2 = Player_get_DisplayName_m1397(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String GooglePlayGames.Native.NativeClient::GetUserImageUrl()
extern "C" String_t* NativeClient_GetUserImageUrl_m2271 (NativeClient_t524 * __this, MethodInfo* method)
{
	{
		Player_t347 * L_0 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000f:
	{
		Player_t347 * L_1 = (__this->___mUser_15);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		String_t* L_2 = Player_get_AvatarURL_m1399(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeClient::GetAchievement(System.String)
extern "C" Achievement_t333 * NativeClient_GetAchievement_m2272 (NativeClient_t524 * __this, String_t* ___achId, MethodInfo* method)
{
	{
		Dictionary_2_t542 * L_0 = (__this->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Dictionary_2_t542 * L_1 = (__this->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_2 = ___achId;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(29 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::ContainsKey(!0) */, L_1, L_2);
		if (L_3)
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (Achievement_t333 *)NULL;
	}

IL_0022:
	{
		Dictionary_2_t542 * L_4 = (__this->___mAchievements_14);
		il2cpp_codegen_memory_barrier();
		String_t* L_5 = ___achId;
		NullCheck(L_4);
		Achievement_t333 * L_6 = (Achievement_t333 *)VirtFuncInvoker1< Achievement_t333 *, String_t* >::Invoke(26 /* !1 System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>::get_Item(!0) */, L_4, L_5);
		return L_6;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::UnlockAchievement(System.String,System.Action`1<System.Boolean>)
extern TypeInfo* U3CUnlockAchievementU3Ec__AnonStorey1D_t526_il2cpp_TypeInfo_var;
extern TypeInfo* NativeClient_t524_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t543_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t860_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_U3CUnlockAchievementU3Em__13_m2289_MethodInfo_var;
extern MethodInfo* Predicate_1__ctor_m3754_MethodInfo_var;
extern MethodInfo* U3CUnlockAchievementU3Ec__AnonStorey1D_U3CU3Em__14_m2242_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3755_MethodInfo_var;
extern "C" void NativeClient_UnlockAchievement_m2273 (NativeClient_t524 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(678);
		NativeClient_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(679);
		Predicate_1_t543_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(680);
		Action_1_t860_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(681);
		NativeClient_U3CUnlockAchievementU3Em__13_m2289_MethodInfo_var = il2cpp_codegen_method_info_from_index(205);
		Predicate_1__ctor_m3754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483854);
		U3CUnlockAchievementU3Ec__AnonStorey1D_U3CU3Em__14_m2242_MethodInfo_var = il2cpp_codegen_method_info_from_index(207);
		Action_1__ctor_m3755_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483856);
		s_Il2CppMethodIntialized = true;
	}
	U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * V_0 = {0};
	Action_1_t98 * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B2_2 = {0};
	NativeClient_t524 * G_B2_3 = {0};
	Action_1_t98 * G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	String_t* G_B1_2 = {0};
	NativeClient_t524 * G_B1_3 = {0};
	{
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * L_0 = (U3CUnlockAchievementU3Ec__AnonStorey1D_t526 *)il2cpp_codegen_object_new (U3CUnlockAchievementU3Ec__AnonStorey1D_t526_il2cpp_TypeInfo_var);
		U3CUnlockAchievementU3Ec__AnonStorey1D__ctor_m2241(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * L_1 = V_0;
		String_t* L_2 = ___achId;
		NullCheck(L_1);
		L_1->___achId_0 = L_2;
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = (L_4->___achId_0);
		Action_1_t98 * L_6 = ___callback;
		Predicate_1_t543 * L_7 = ((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache12_21;
		G_B1_0 = L_6;
		G_B1_1 = L_5;
		G_B1_2 = (String_t*) &_stringLiteral463;
		G_B1_3 = __this;
		if (L_7)
		{
			G_B2_0 = L_6;
			G_B2_1 = L_5;
			G_B2_2 = (String_t*) &_stringLiteral463;
			G_B2_3 = __this;
			goto IL_0039;
		}
	}
	{
		IntPtr_t L_8 = { NativeClient_U3CUnlockAchievementU3Em__13_m2289_MethodInfo_var };
		Predicate_1_t543 * L_9 = (Predicate_1_t543 *)il2cpp_codegen_object_new (Predicate_1_t543_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3754(L_9, NULL, L_8, /*hidden argument*/Predicate_1__ctor_m3754_MethodInfo_var);
		((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache12_21 = L_9;
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0039:
	{
		Predicate_1_t543 * L_10 = ((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache12_21;
		U3CUnlockAchievementU3Ec__AnonStorey1D_t526 * L_11 = V_0;
		IntPtr_t L_12 = { U3CUnlockAchievementU3Ec__AnonStorey1D_U3CU3Em__14_m2242_MethodInfo_var };
		Action_1_t860 * L_13 = (Action_1_t860 *)il2cpp_codegen_object_new (Action_1_t860_il2cpp_TypeInfo_var);
		Action_1__ctor_m3755(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3755_MethodInfo_var);
		NullCheck(G_B2_3);
		NativeClient_UpdateAchievement_m2275(G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::RevealAchievement(System.String,System.Action`1<System.Boolean>)
extern TypeInfo* U3CRevealAchievementU3Ec__AnonStorey1E_t527_il2cpp_TypeInfo_var;
extern TypeInfo* NativeClient_t524_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t543_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t860_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_U3CRevealAchievementU3Em__15_m2290_MethodInfo_var;
extern MethodInfo* Predicate_1__ctor_m3754_MethodInfo_var;
extern MethodInfo* U3CRevealAchievementU3Ec__AnonStorey1E_U3CU3Em__16_m2244_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3755_MethodInfo_var;
extern "C" void NativeClient_RevealAchievement_m2274 (NativeClient_t524 * __this, String_t* ___achId, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRevealAchievementU3Ec__AnonStorey1E_t527_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(682);
		NativeClient_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(679);
		Predicate_1_t543_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(680);
		Action_1_t860_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(681);
		NativeClient_U3CRevealAchievementU3Em__15_m2290_MethodInfo_var = il2cpp_codegen_method_info_from_index(209);
		Predicate_1__ctor_m3754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483854);
		U3CRevealAchievementU3Ec__AnonStorey1E_U3CU3Em__16_m2244_MethodInfo_var = il2cpp_codegen_method_info_from_index(210);
		Action_1__ctor_m3755_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483856);
		s_Il2CppMethodIntialized = true;
	}
	U3CRevealAchievementU3Ec__AnonStorey1E_t527 * V_0 = {0};
	Action_1_t98 * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B2_2 = {0};
	NativeClient_t524 * G_B2_3 = {0};
	Action_1_t98 * G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	String_t* G_B1_2 = {0};
	NativeClient_t524 * G_B1_3 = {0};
	{
		U3CRevealAchievementU3Ec__AnonStorey1E_t527 * L_0 = (U3CRevealAchievementU3Ec__AnonStorey1E_t527 *)il2cpp_codegen_object_new (U3CRevealAchievementU3Ec__AnonStorey1E_t527_il2cpp_TypeInfo_var);
		U3CRevealAchievementU3Ec__AnonStorey1E__ctor_m2243(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRevealAchievementU3Ec__AnonStorey1E_t527 * L_1 = V_0;
		String_t* L_2 = ___achId;
		NullCheck(L_1);
		L_1->___achId_0 = L_2;
		U3CRevealAchievementU3Ec__AnonStorey1E_t527 * L_3 = V_0;
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_1 = __this;
		U3CRevealAchievementU3Ec__AnonStorey1E_t527 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = (L_4->___achId_0);
		Action_1_t98 * L_6 = ___callback;
		Predicate_1_t543 * L_7 = ((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache13_22;
		G_B1_0 = L_6;
		G_B1_1 = L_5;
		G_B1_2 = (String_t*) &_stringLiteral464;
		G_B1_3 = __this;
		if (L_7)
		{
			G_B2_0 = L_6;
			G_B2_1 = L_5;
			G_B2_2 = (String_t*) &_stringLiteral464;
			G_B2_3 = __this;
			goto IL_0039;
		}
	}
	{
		IntPtr_t L_8 = { NativeClient_U3CRevealAchievementU3Em__15_m2290_MethodInfo_var };
		Predicate_1_t543 * L_9 = (Predicate_1_t543 *)il2cpp_codegen_object_new (Predicate_1_t543_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3754(L_9, NULL, L_8, /*hidden argument*/Predicate_1__ctor_m3754_MethodInfo_var);
		((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache13_22 = L_9;
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0039:
	{
		Predicate_1_t543 * L_10 = ((NativeClient_t524_StaticFields*)NativeClient_t524_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache13_22;
		U3CRevealAchievementU3Ec__AnonStorey1E_t527 * L_11 = V_0;
		IntPtr_t L_12 = { U3CRevealAchievementU3Ec__AnonStorey1E_U3CU3Em__16_m2244_MethodInfo_var };
		Action_1_t860 * L_13 = (Action_1_t860 *)il2cpp_codegen_object_new (Action_1_t860_il2cpp_TypeInfo_var);
		Action_1__ctor_m3755(L_13, L_11, L_12, /*hidden argument*/Action_1__ctor_m3755_MethodInfo_var);
		NullCheck(G_B2_3);
		NativeClient_UpdateAchievement_m2275(G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::UpdateAchievement(System.String,System.String,System.Action`1<System.Boolean>,System.Predicate`1<GooglePlayGames.BasicApi.Achievement>,System.Action`1<GooglePlayGames.BasicApi.Achievement>)
extern TypeInfo* U3CUpdateAchievementU3Ec__AnonStorey1F_t528_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t867_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* U3CUpdateAchievementU3Ec__AnonStorey1F_U3CU3Em__17_m2246_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3756_MethodInfo_var;
extern "C" void NativeClient_UpdateAchievement_m2275 (NativeClient_t524 * __this, String_t* ___updateType, String_t* ___achId, Action_1_t98 * ___callback, Predicate_1_t543 * ___alreadyDone, Action_1_t860 * ___updateAchievment, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(684);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t867_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483859);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		U3CUpdateAchievementU3Ec__AnonStorey1F_U3CU3Em__17_m2246_MethodInfo_var = il2cpp_codegen_method_info_from_index(212);
		Action_1__ctor_m3756_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483861);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t333 * V_0 = {0};
	U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * V_1 = {0};
	{
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_0 = (U3CUpdateAchievementU3Ec__AnonStorey1F_t528 *)il2cpp_codegen_object_new (U3CUpdateAchievementU3Ec__AnonStorey1F_t528_il2cpp_TypeInfo_var);
		U3CUpdateAchievementU3Ec__AnonStorey1F__ctor_m2245(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_1 = V_1;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_3 = V_1;
		String_t* L_4 = ___achId;
		NullCheck(L_3);
		L_3->___achId_1 = L_4;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_5 = V_1;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_2 = __this;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_6 = V_1;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_7 = V_1;
		NullCheck(L_7);
		Action_1_t98 * L_8 = (L_7->___callback_0);
		Action_1_t98 * L_9 = NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742(NULL /*static, unused*/, L_8, /*hidden argument*/NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_0 = L_9;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = (L_10->___achId_1);
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_11, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		NativeClient_InitializeGameServices_m2258(__this, /*hidden argument*/NULL);
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = (L_12->___achId_1);
		Achievement_t333 * L_14 = (Achievement_t333 *)VirtFuncInvoker1< Achievement_t333 *, String_t* >::Invoke(10 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeClient::GetAchievement(System.String) */, __this, L_13);
		V_0 = L_14;
		Achievement_t333 * L_15 = V_0;
		if (L_15)
		{
			goto IL_0079;
		}
	}
	{
		String_t* L_16 = ___updateType;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_17 = V_1;
		NullCheck(L_17);
		String_t* L_18 = (L_17->___achId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1025(NULL /*static, unused*/, (String_t*) &_stringLiteral465, L_16, (String_t*) &_stringLiteral466, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_20 = V_1;
		NullCheck(L_20);
		Action_1_t98 * L_21 = (L_20->___callback_0);
		NullCheck(L_21);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_21, 0);
		return;
	}

IL_0079:
	{
		Predicate_1_t543 * L_22 = ___alreadyDone;
		Achievement_t333 * L_23 = V_0;
		NullCheck(L_22);
		bool L_24 = (bool)VirtFuncInvoker1< bool, Achievement_t333 * >::Invoke(10 /* System.Boolean System.Predicate`1<GooglePlayGames.BasicApi.Achievement>::Invoke(!0) */, L_22, L_23);
		if (!L_24)
		{
			goto IL_00ae;
		}
	}
	{
		String_t* L_25 = ___updateType;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_26 = V_1;
		NullCheck(L_26);
		String_t* L_27 = (L_26->___achId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m1025(NULL /*static, unused*/, (String_t*) &_stringLiteral467, L_25, (String_t*) &_stringLiteral468, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_29 = V_1;
		NullCheck(L_29);
		Action_1_t98 * L_30 = (L_29->___callback_0);
		NullCheck(L_30);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_30, 1);
		return;
	}

IL_00ae:
	{
		String_t* L_31 = ___updateType;
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_32 = V_1;
		NullCheck(L_32);
		String_t* L_33 = (L_32->___achId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1025(NULL /*static, unused*/, (String_t*) &_stringLiteral469, L_31, (String_t*) &_stringLiteral470, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		Action_1_t860 * L_35 = ___updateAchievment;
		Achievement_t333 * L_36 = V_0;
		NullCheck(L_35);
		VirtActionInvoker1< Achievement_t333 * >::Invoke(10 /* System.Void System.Action`1<GooglePlayGames.BasicApi.Achievement>::Invoke(!0) */, L_35, L_36);
		GameServices_t534 * L_37 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		AchievementManager_t656 * L_38 = GameServices_AchievementManager_m2665(L_37, /*hidden argument*/NULL);
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_39 = V_1;
		NullCheck(L_39);
		String_t* L_40 = (L_39->___achId_1);
		U3CUpdateAchievementU3Ec__AnonStorey1F_t528 * L_41 = V_1;
		IntPtr_t L_42 = { U3CUpdateAchievementU3Ec__AnonStorey1F_U3CU3Em__17_m2246_MethodInfo_var };
		Action_1_t867 * L_43 = (Action_1_t867 *)il2cpp_codegen_object_new (Action_1_t867_il2cpp_TypeInfo_var);
		Action_1__ctor_m3756(L_43, L_41, L_42, /*hidden argument*/Action_1__ctor_m3756_MethodInfo_var);
		NullCheck(L_38);
		AchievementManager_Fetch_m2619(L_38, L_40, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::IncrementAchievement(System.String,System.Int32,System.Action`1<System.Boolean>)
extern TypeInfo* U3CIncrementAchievementU3Ec__AnonStorey20_t529_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t184_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t867_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var;
extern MethodInfo* U3CIncrementAchievementU3Ec__AnonStorey20_U3CU3Em__18_m2248_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3756_MethodInfo_var;
extern "C" void NativeClient_IncrementAchievement_m2276 (NativeClient_t524 * __this, String_t* ___achId, int32_t ___steps, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CIncrementAchievementU3Ec__AnonStorey20_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Convert_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Action_1_t867_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483859);
		U3CIncrementAchievementU3Ec__AnonStorey20_U3CU3Em__18_m2248_MethodInfo_var = il2cpp_codegen_method_info_from_index(214);
		Action_1__ctor_m3756_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483861);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t333 * V_0 = {0};
	U3CIncrementAchievementU3Ec__AnonStorey20_t529 * V_1 = {0};
	{
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_0 = (U3CIncrementAchievementU3Ec__AnonStorey20_t529 *)il2cpp_codegen_object_new (U3CIncrementAchievementU3Ec__AnonStorey20_t529_il2cpp_TypeInfo_var);
		U3CIncrementAchievementU3Ec__AnonStorey20__ctor_m2247(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_1 = V_1;
		Action_1_t98 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_3 = V_1;
		String_t* L_4 = ___achId;
		NullCheck(L_3);
		L_3->___achId_1 = L_4;
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_5 = V_1;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_2 = __this;
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___achId_1);
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_7, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_8 = V_1;
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_9 = V_1;
		NullCheck(L_9);
		Action_1_t98 * L_10 = (L_9->___callback_0);
		Action_1_t98 * L_11 = NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742(NULL /*static, unused*/, L_10, /*hidden argument*/NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var);
		NullCheck(L_8);
		L_8->___callback_0 = L_11;
		NativeClient_InitializeGameServices_m2258(__this, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = (L_12->___achId_1);
		Achievement_t333 * L_14 = (Achievement_t333 *)VirtFuncInvoker1< Achievement_t333 *, String_t* >::Invoke(10 /* GooglePlayGames.BasicApi.Achievement GooglePlayGames.Native.NativeClient::GetAchievement(System.String) */, __this, L_13);
		V_0 = L_14;
		Achievement_t333 * L_15 = V_0;
		if (L_15)
		{
			goto IL_0073;
		}
	}
	{
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = (L_16->___achId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral471, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_19 = V_1;
		NullCheck(L_19);
		Action_1_t98 * L_20 = (L_19->___callback_0);
		NullCheck(L_20);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_20, 0);
		return;
	}

IL_0073:
	{
		Achievement_t333 * L_21 = V_0;
		NullCheck(L_21);
		bool L_22 = Achievement_get_IsIncremental_m1329(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00a5;
		}
	}
	{
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_23 = V_1;
		NullCheck(L_23);
		String_t* L_24 = (L_23->___achId_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m912(NULL /*static, unused*/, (String_t*) &_stringLiteral472, L_24, (String_t*) &_stringLiteral473, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_26 = V_1;
		NullCheck(L_26);
		Action_1_t98 * L_27 = (L_26->___callback_0);
		NullCheck(L_27);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_27, 0);
		return;
	}

IL_00a5:
	{
		int32_t L_28 = ___steps;
		if ((((int32_t)L_28) >= ((int32_t)0)))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral474, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_29 = V_1;
		NullCheck(L_29);
		Action_1_t98 * L_30 = (L_29->___callback_0);
		NullCheck(L_30);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_30, 0);
		return;
	}

IL_00c3:
	{
		GameServices_t534 * L_31 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		AchievementManager_t656 * L_32 = GameServices_AchievementManager_m2665(L_31, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_33 = V_1;
		NullCheck(L_33);
		String_t* L_34 = (L_33->___achId_1);
		int32_t L_35 = ___steps;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t184_il2cpp_TypeInfo_var);
		uint32_t L_36 = Convert_ToUInt32_m3757(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_32);
		AchievementManager_Increment_m2621(L_32, L_34, L_36, /*hidden argument*/NULL);
		GameServices_t534 * L_37 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		AchievementManager_t656 * L_38 = GameServices_AchievementManager_m2665(L_37, /*hidden argument*/NULL);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_39 = V_1;
		NullCheck(L_39);
		String_t* L_40 = (L_39->___achId_1);
		U3CIncrementAchievementU3Ec__AnonStorey20_t529 * L_41 = V_1;
		IntPtr_t L_42 = { U3CIncrementAchievementU3Ec__AnonStorey20_U3CU3Em__18_m2248_MethodInfo_var };
		Action_1_t867 * L_43 = (Action_1_t867 *)il2cpp_codegen_object_new (Action_1_t867_il2cpp_TypeInfo_var);
		Action_1__ctor_m3756(L_43, L_41, L_42, /*hidden argument*/Action_1__ctor_m3756_MethodInfo_var);
		NullCheck(L_38);
		AchievementManager_Fetch_m2619(L_38, L_40, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::ShowAchievementsUI(System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern TypeInfo* U3CShowAchievementsUIU3Ec__AnonStorey21_t531_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t660_il2cpp_TypeInfo_var;
extern MethodInfo* U3CShowAchievementsUIU3Ec__AnonStorey21_U3CU3Em__19_m2250_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3758_MethodInfo_var;
extern MethodInfo* NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var;
extern "C" void NativeClient_ShowAchievementsUI_m2277 (NativeClient_t524 * __this, Action_1_t530 * ___cb, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShowAchievementsUIU3Ec__AnonStorey21_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		U3CShowAchievementsUIU3Ec__AnonStorey21_U3CU3Em__19_m2250_MethodInfo_var = il2cpp_codegen_method_info_from_index(215);
		Action_1__ctor_m3758_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483864);
		NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483865);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t660 * V_0 = {0};
	U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * V_1 = {0};
	{
		U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * L_0 = (U3CShowAchievementsUIU3Ec__AnonStorey21_t531 *)il2cpp_codegen_object_new (U3CShowAchievementsUIU3Ec__AnonStorey21_t531_il2cpp_TypeInfo_var);
		U3CShowAchievementsUIU3Ec__AnonStorey21__ctor_m2249(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * L_1 = V_1;
		Action_1_t530 * L_2 = ___cb;
		NullCheck(L_1);
		L_1->___cb_0 = L_2;
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated() */, __this);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t660 * L_4 = ((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___NoopUICallback_0;
		V_0 = L_4;
		U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * L_5 = V_1;
		NullCheck(L_5);
		Action_1_t530 * L_6 = (L_5->___cb_0);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		U3CShowAchievementsUIU3Ec__AnonStorey21_t531 * L_7 = V_1;
		IntPtr_t L_8 = { U3CShowAchievementsUIU3Ec__AnonStorey21_U3CU3Em__19_m2250_MethodInfo_var };
		Action_1_t660 * L_9 = (Action_1_t660 *)il2cpp_codegen_object_new (Action_1_t660_il2cpp_TypeInfo_var);
		Action_1__ctor_m3758(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m3758_MethodInfo_var);
		V_0 = L_9;
	}

IL_0037:
	{
		Action_1_t660 * L_10 = V_0;
		Action_1_t660 * L_11 = NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744(NULL /*static, unused*/, L_10, /*hidden argument*/NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var);
		V_0 = L_11;
		GameServices_t534 * L_12 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		AchievementManager_t656 * L_13 = GameServices_AchievementManager_m2665(L_12, /*hidden argument*/NULL);
		Action_1_t660 * L_14 = V_0;
		NullCheck(L_13);
		AchievementManager_ShowAllUI_m2616(L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::ShowLeaderboardUI(System.String,System.Action`1<GooglePlayGames.BasicApi.UIStatus>)
extern TypeInfo* U3CShowLeaderboardUIU3Ec__AnonStorey22_t532_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t660_il2cpp_TypeInfo_var;
extern MethodInfo* U3CShowLeaderboardUIU3Ec__AnonStorey22_U3CU3Em__1A_m2252_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3758_MethodInfo_var;
extern MethodInfo* NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var;
extern "C" void NativeClient_ShowLeaderboardUI_m2278 (NativeClient_t524 * __this, String_t* ___leaderboardId, Action_1_t530 * ___cb, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CShowLeaderboardUIU3Ec__AnonStorey22_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(690);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		Action_1_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		U3CShowLeaderboardUIU3Ec__AnonStorey22_U3CU3Em__1A_m2252_MethodInfo_var = il2cpp_codegen_method_info_from_index(218);
		Action_1__ctor_m3758_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483864);
		NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483865);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t660 * V_0 = {0};
	U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * V_1 = {0};
	{
		U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * L_0 = (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 *)il2cpp_codegen_object_new (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532_il2cpp_TypeInfo_var);
		U3CShowLeaderboardUIU3Ec__AnonStorey22__ctor_m2251(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * L_1 = V_1;
		Action_1_t530 * L_2 = ___cb;
		NullCheck(L_1);
		L_1->___cb_0 = L_2;
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated() */, __this);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_1_t660 * L_4 = ((Callbacks_t661_StaticFields*)Callbacks_t661_il2cpp_TypeInfo_var->static_fields)->___NoopUICallback_0;
		V_0 = L_4;
		U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * L_5 = V_1;
		NullCheck(L_5);
		Action_1_t530 * L_6 = (L_5->___cb_0);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * L_7 = V_1;
		IntPtr_t L_8 = { U3CShowLeaderboardUIU3Ec__AnonStorey22_U3CU3Em__1A_m2252_MethodInfo_var };
		Action_1_t660 * L_9 = (Action_1_t660 *)il2cpp_codegen_object_new (Action_1_t660_il2cpp_TypeInfo_var);
		Action_1__ctor_m3758(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m3758_MethodInfo_var);
		V_0 = L_9;
	}

IL_0037:
	{
		Action_1_t660 * L_10 = V_0;
		Action_1_t660 * L_11 = NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744(NULL /*static, unused*/, L_10, /*hidden argument*/NativeClient_AsOnGameThreadCallback_TisUIStatus_t412_m3744_MethodInfo_var);
		V_0 = L_11;
		String_t* L_12 = ___leaderboardId;
		if (L_12)
		{
			goto IL_005a;
		}
	}
	{
		GameServices_t534 * L_13 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		LeaderboardManager_t670 * L_14 = GameServices_LeaderboardManager_m2666(L_13, /*hidden argument*/NULL);
		Action_1_t660 * L_15 = V_0;
		NullCheck(L_14);
		LeaderboardManager_ShowAllUI_m2697(L_14, L_15, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_005a:
	{
		GameServices_t534 * L_16 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		LeaderboardManager_t670 * L_17 = GameServices_LeaderboardManager_m2666(L_16, /*hidden argument*/NULL);
		String_t* L_18 = ___leaderboardId;
		Action_1_t660 * L_19 = V_0;
		NullCheck(L_17);
		LeaderboardManager_ShowUI_m2698(L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::SubmitScore(System.String,System.Int64,System.Action`1<System.Boolean>)
extern TypeInfo* ArgumentNullException_t908_il2cpp_TypeInfo_var;
extern MethodInfo* NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var;
extern "C" void NativeClient_SubmitScore_m2279 (NativeClient_t524 * __this, String_t* ___leaderboardId, int64_t ___score, Action_1_t98 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t908_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483859);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t98 * L_0 = ___callback;
		Action_1_t98 * L_1 = NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742(NULL /*static, unused*/, L_0, /*hidden argument*/NativeClient_AsOnGameThreadCallback_TisBoolean_t203_m3742_MethodInfo_var);
		___callback = L_1;
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated() */, __this);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t98 * L_3 = ___callback;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_3, 0);
	}

IL_001a:
	{
		NativeClient_InitializeGameServices_m2258(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___leaderboardId;
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		ArgumentNullException_t908 * L_5 = (ArgumentNullException_t908 *)il2cpp_codegen_object_new (ArgumentNullException_t908_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3728(L_5, (String_t*) &_stringLiteral475, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0031:
	{
		GameServices_t534 * L_6 = NativeClient_GameServices_m2256(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		LeaderboardManager_t670 * L_7 = GameServices_LeaderboardManager_m2666(L_6, /*hidden argument*/NULL);
		String_t* L_8 = ___leaderboardId;
		int64_t L_9 = ___score;
		NullCheck(L_7);
		LeaderboardManager_SubmitScore_m2696(L_7, L_8, L_9, /*hidden argument*/NULL);
		Action_1_t98 * L_10 = ___callback;
		NullCheck(L_10);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void System.Action`1<System.Boolean>::Invoke(!0) */, L_10, 1);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* OnStateLoadedListener_t842_il2cpp_TypeInfo_var;
extern TypeInfo* AppStateClient_t540_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var;
extern "C" void NativeClient_LoadState_m2280 (NativeClient_t524 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		OnStateLoadedListener_t842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		AppStateClient_t540_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(691);
		Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483867);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___listener;
		Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var);
		Object_t * L_1 = (__this->___GameServicesLock_3);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_3 = (__this->___mAuthState_18);
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_0035;
			}
		}

IL_0022:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral476, /*hidden argument*/NULL);
			Object_t * L_4 = ___listener;
			int32_t L_5 = ___slot;
			NullCheck(L_4);
			InterfaceActionInvoker3< bool, int32_t, ByteU5BU5D_t350* >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.OnStateLoadedListener::OnStateLoaded(System.Boolean,System.Int32,System.Byte[]) */, OnStateLoadedListener_t842_il2cpp_TypeInfo_var, L_4, 0, L_5, (ByteU5BU5D_t350*)(ByteU5BU5D_t350*)NULL);
		}

IL_0035:
		{
			Object_t * L_6 = (__this->___mAppStateClient_12);
			il2cpp_codegen_memory_barrier();
			int32_t L_7 = ___slot;
			Object_t * L_8 = ___listener;
			NullCheck(L_6);
			InterfaceActionInvoker2< int32_t, Object_t * >::Invoke(0 /* System.Void GooglePlayGames.Native.AppStateClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener) */, AppStateClient_t540_il2cpp_TypeInfo_var, L_6, L_7, L_8);
			IL2CPP_LEAVE(0x50, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Object_t * L_9 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0050:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* OnStateLoadedListener_t842_il2cpp_TypeInfo_var;
extern TypeInfo* AppStateClient_t540_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var;
extern "C" void NativeClient_UpdateState_m2281 (NativeClient_t524 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		OnStateLoadedListener_t842_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		AppStateClient_t540_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(691);
		Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483867);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___listener;
		Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisOnStateLoadedListener_t842_m3746_MethodInfo_var);
		Object_t * L_1 = (__this->___GameServicesLock_3);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_3 = (__this->___mAuthState_18);
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_3) == ((int32_t)1)))
			{
				goto IL_0034;
			}
		}

IL_0022:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral477, /*hidden argument*/NULL);
			Object_t * L_4 = ___listener;
			int32_t L_5 = ___slot;
			NullCheck(L_4);
			InterfaceActionInvoker2< bool, int32_t >::Invoke(2 /* System.Void GooglePlayGames.BasicApi.OnStateLoadedListener::OnStateSaved(System.Boolean,System.Int32) */, OnStateLoadedListener_t842_il2cpp_TypeInfo_var, L_4, 0, L_5);
		}

IL_0034:
		{
			Object_t * L_6 = (__this->___mAppStateClient_12);
			il2cpp_codegen_memory_barrier();
			int32_t L_7 = ___slot;
			ByteU5BU5D_t350* L_8 = ___data;
			Object_t * L_9 = ___listener;
			NullCheck(L_6);
			InterfaceActionInvoker3< int32_t, ByteU5BU5D_t350*, Object_t * >::Invoke(1 /* System.Void GooglePlayGames.Native.AppStateClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener) */, AppStateClient_t540_il2cpp_TypeInfo_var, L_6, L_7, L_8, L_9);
			IL2CPP_LEAVE(0x50, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Object_t * L_10 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0050:
	{
		return;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.IRealTimeMultiplayerClient GooglePlayGames.Native.NativeClient::GetRtmpClient()
extern "C" Object_t * NativeClient_GetRtmpClient_m2282 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeClient::IsAuthenticated() */, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (Object_t *)NULL;
	}

IL_000d:
	{
		Object_t * L_1 = (__this->___GameServicesLock_3);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			NativeRealtimeMultiplayerClient_t536 * L_3 = (__this->___mRealTimeClient_8);
			il2cpp_codegen_memory_barrier();
			V_1 = L_3;
			IL2CPP_LEAVE(0x34, FINALLY_002d);
		}

IL_0028:
		{
			; // IL_0028: leave IL_0034
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Object_t * L_4 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0034:
	{
		Object_t * L_5 = V_1;
		return L_5;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.ITurnBasedMultiplayerClient GooglePlayGames.Native.NativeClient::GetTbmpClient()
extern "C" Object_t * NativeClient_GetTbmpClient_m2283 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NativeTurnBasedMultiplayerClient_t535 * L_2 = (__this->___mTurnBasedClient_7);
			il2cpp_codegen_memory_barrier();
			V_1 = L_2;
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}

IL_001b:
		{
			; // IL_001b: leave IL_0027
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		Object_t * L_4 = V_1;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.SavedGame.ISavedGameClient GooglePlayGames.Native.NativeClient::GetSavedGameClient()
extern "C" Object_t * NativeClient_GetSavedGameClient_m2284 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_2 = (__this->___mSavedGameClient_9);
			il2cpp_codegen_memory_barrier();
			V_1 = L_2;
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}

IL_001b:
		{
			; // IL_001b: leave IL_0027
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		Object_t * L_4 = V_1;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Events.IEventsClient GooglePlayGames.Native.NativeClient::GetEventsClient()
extern "C" Object_t * NativeClient_GetEventsClient_m2285 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_2 = (__this->___mEventsClient_10);
			il2cpp_codegen_memory_barrier();
			V_1 = L_2;
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}

IL_001b:
		{
			; // IL_001b: leave IL_0027
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		Object_t * L_4 = V_1;
		return L_4;
	}
}
// GooglePlayGames.BasicApi.Quests.IQuestsClient GooglePlayGames.Native.NativeClient::GetQuestsClient()
extern "C" Object_t * NativeClient_GetQuestsClient_m2286 (NativeClient_t524 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___GameServicesLock_3);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_2 = (__this->___mQuestsClient_11);
			il2cpp_codegen_memory_barrier();
			V_1 = L_2;
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}

IL_001b:
		{
			; // IL_001b: leave IL_0027
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		Object_t * L_4 = V_1;
		return L_4;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::RegisterInvitationDelegate(GooglePlayGames.BasicApi.InvitationReceivedDelegate)
extern TypeInfo* U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533_il2cpp_TypeInfo_var;
extern TypeInfo* Action_2_t541_il2cpp_TypeInfo_var;
extern TypeInfo* Callbacks_t661_il2cpp_TypeInfo_var;
extern MethodInfo* U3CRegisterInvitationDelegateU3Ec__AnonStorey23_U3CU3Em__1B_m2254_MethodInfo_var;
extern MethodInfo* Action_2__ctor_m3759_MethodInfo_var;
extern MethodInfo* Callbacks_AsOnGameThreadCallback_TisInvitation_t341_TisBoolean_t203_m3747_MethodInfo_var;
extern "C" void NativeClient_RegisterInvitationDelegate_m2287 (NativeClient_t524 * __this, InvitationReceivedDelegate_t363 * ___invitationDelegate, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(693);
		Action_2_t541_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(694);
		Callbacks_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_U3CU3Em__1B_m2254_MethodInfo_var = il2cpp_codegen_method_info_from_index(220);
		Action_2__ctor_m3759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483869);
		Callbacks_AsOnGameThreadCallback_TisInvitation_t341_TisBoolean_t203_m3747_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483870);
		s_Il2CppMethodIntialized = true;
	}
	U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * V_0 = {0};
	{
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * L_0 = (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 *)il2cpp_codegen_object_new (U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533_il2cpp_TypeInfo_var);
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23__ctor_m2253(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * L_1 = V_0;
		InvitationReceivedDelegate_t363 * L_2 = ___invitationDelegate;
		NullCheck(L_1);
		L_1->___invitationDelegate_0 = L_2;
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * L_3 = V_0;
		NullCheck(L_3);
		InvitationReceivedDelegate_t363 * L_4 = (L_3->___invitationDelegate_0);
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		il2cpp_codegen_memory_barrier();
		__this->___mInvitationDelegate_13 = (Action_2_t541 *)NULL;
		goto IL_003f;
	}

IL_0026:
	{
		U3CRegisterInvitationDelegateU3Ec__AnonStorey23_t533 * L_5 = V_0;
		IntPtr_t L_6 = { U3CRegisterInvitationDelegateU3Ec__AnonStorey23_U3CU3Em__1B_m2254_MethodInfo_var };
		Action_2_t541 * L_7 = (Action_2_t541 *)il2cpp_codegen_object_new (Action_2_t541_il2cpp_TypeInfo_var);
		Action_2__ctor_m3759(L_7, L_5, L_6, /*hidden argument*/Action_2__ctor_m3759_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Callbacks_t661_il2cpp_TypeInfo_var);
		Action_2_t541 * L_8 = Callbacks_AsOnGameThreadCallback_TisInvitation_t341_TisBoolean_t203_m3747(NULL /*static, unused*/, L_7, /*hidden argument*/Callbacks_AsOnGameThreadCallback_TisInvitation_t341_TisBoolean_t203_m3747_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		__this->___mInvitationDelegate_13 = L_8;
	}

IL_003f:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeClient::<InitializeGameServices>m__10(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void NativeClient_U3CInitializeGameServicesU3Em__10_m2288 (NativeClient_t524 * __this, int32_t ___eventType, String_t* ___matchId, NativeTurnBasedMatch_t680 * ___match, MethodInfo* method)
{
	{
		NativeTurnBasedMultiplayerClient_t535 * L_0 = (__this->___mTurnBasedClient_7);
		il2cpp_codegen_memory_barrier();
		int32_t L_1 = ___eventType;
		String_t* L_2 = ___matchId;
		NativeTurnBasedMatch_t680 * L_3 = ___match;
		NullCheck(L_0);
		NativeTurnBasedMultiplayerClient_HandleMatchEvent_m2587(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GooglePlayGames.Native.NativeClient::<UnlockAchievement>m__13(GooglePlayGames.BasicApi.Achievement)
extern "C" bool NativeClient_U3CUnlockAchievementU3Em__13_m2289 (Object_t * __this /* static, unused */, Achievement_t333 * ___a, MethodInfo* method)
{
	{
		Achievement_t333 * L_0 = ___a;
		NullCheck(L_0);
		bool L_1 = Achievement_get_IsUnlocked_m1335(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean GooglePlayGames.Native.NativeClient::<RevealAchievement>m__15(GooglePlayGames.BasicApi.Achievement)
extern "C" bool NativeClient_U3CRevealAchievementU3Em__15_m2290 (Object_t * __this /* static, unused */, Achievement_t333 * ___a, MethodInfo* method)
{
	{
		Achievement_t333 * L_0 = ___a;
		NullCheck(L_0);
		bool L_1 = Achievement_get_IsRevealed_m1337(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClient_U.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClient_UMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_0.h"
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>
#include "System_Core_System_Action_2_gen_3.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
#include "mscorlib_System_Collections_Generic_List_1_gen_16.h"
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
#include "mscorlib_System_Collections_Generic_List_1_gen_17.h"
// GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage_0MethodDeclarations.h"
// GooglePlayGames.Native.ConversionUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_ConversionUtilsMethodDeclarations.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>
#include "mscorlib_System_Collections_Generic_List_1_gen_16MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>
#include "System_Core_System_Action_2_gen_3MethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t914;
struct IEnumerable_t38;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t219;
struct IEnumerable_1_t221;
struct IEnumerable_t38;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C" Object_t* Enumerable_Cast_TisObject_t_m3761_gshared (Object_t * __this /* static, unused */, Object_t * p0, MethodInfo* method);
#define Enumerable_Cast_TisObject_t_m3761(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Enumerable_Cast_TisObject_t_m3761_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.Events.IEvent>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.Events.IEvent>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisIEvent_t913_m3760(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Enumerable_Cast_TisObject_t_m3761_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct List_1_t915;
struct IEnumerable_1_t914;
struct Enumerable_t219;
struct List_1_t181;
struct IEnumerable_1_t221;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t181 * Enumerable_ToList_TisObject_t_m3763_gshared (Object_t * __this /* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m3763(__this /* static, unused */, p0, method) (( List_1_t181 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Events.IEvent>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Events.IEvent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisIEvent_t913_m3762(__this /* static, unused */, p0, method) (( List_1_t915 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24::.ctor()
extern "C" void U3CFetchAllEventsU3Ec__AnonStorey24__ctor_m2291 (U3CFetchAllEventsU3Ec__AnonStorey24_t545 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24::<>m__1C(GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse)
extern TypeInfo* List_1_t915_il2cpp_TypeInfo_var;
extern MethodInfo* List_1__ctor_m3764_MethodInfo_var;
extern MethodInfo* Enumerable_Cast_TisIEvent_t913_m3760_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisIEvent_t913_m3762_MethodInfo_var;
extern "C" void U3CFetchAllEventsU3Ec__AnonStorey24_U3CU3Em__1C_m2292 (U3CFetchAllEventsU3Ec__AnonStorey24_t545 * __this, FetchAllResponse_t664 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t915_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(696);
		List_1__ctor_m3764_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483871);
		Enumerable_Cast_TisIEvent_t913_m3760_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483872);
		Enumerable_ToList_TisIEvent_t913_m3762_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483873);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		FetchAllResponse_t664 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = FetchAllResponse_ResponseStatus_m2648(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ConversionUtils_ConvertResponseStatus_m1610(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FetchAllResponse_t664 * L_3 = ___response;
		NullCheck(L_3);
		bool L_4 = FetchAllResponse_RequestSucceeded_m2650(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		Action_2_t544 * L_5 = (__this->___callback_0);
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t915_il2cpp_TypeInfo_var);
		List_1_t915 * L_7 = (List_1_t915 *)il2cpp_codegen_object_new (List_1_t915_il2cpp_TypeInfo_var);
		List_1__ctor_m3764(L_7, /*hidden argument*/List_1__ctor_m3764_MethodInfo_var);
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, List_1_t915 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::Invoke(!0,!1) */, L_5, L_6, L_7);
		goto IL_0049;
	}

IL_002d:
	{
		Action_2_t544 * L_8 = (__this->___callback_0);
		int32_t L_9 = V_0;
		FetchAllResponse_t664 * L_10 = ___response;
		NullCheck(L_10);
		List_1_t868 * L_11 = FetchAllResponse_Data_m2649(L_10, /*hidden argument*/NULL);
		Object_t* L_12 = Enumerable_Cast_TisIEvent_t913_m3760(NULL /*static, unused*/, L_11, /*hidden argument*/Enumerable_Cast_TisIEvent_t913_m3760_MethodInfo_var);
		List_1_t915 * L_13 = Enumerable_ToList_TisIEvent_t913_m3762(NULL /*static, unused*/, L_12, /*hidden argument*/Enumerable_ToList_TisIEvent_t913_m3762_MethodInfo_var);
		NullCheck(L_8);
		VirtActionInvoker2< int32_t, List_1_t915 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>::Invoke(!0,!1) */, L_8, L_9, L_13);
	}

IL_0049:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClient_U_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEventClient_U_0MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManage.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>
#include "System_Core_System_Action_2_gen_4.h"
// GooglePlayGames.Native.NativeEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeEvent.h"
// GooglePlayGames.Native.PInvoke.EventManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_EventManageMethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>
#include "System_Core_System_Action_2_gen_4MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25::.ctor()
extern "C" void U3CFetchEventU3Ec__AnonStorey25__ctor_m2293 (U3CFetchEventU3Ec__AnonStorey25_t547 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeEventClient/<FetchEvent>c__AnonStorey25::<>m__1D(GooglePlayGames.Native.PInvoke.EventManager/FetchResponse)
extern "C" void U3CFetchEventU3Ec__AnonStorey25_U3CU3Em__1D_m2294 (U3CFetchEventU3Ec__AnonStorey25_t547 * __this, FetchResponse_t662 * ___response, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		FetchResponse_t662 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = FetchResponse_ResponseStatus_m2642(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ConversionUtils_ConvertResponseStatus_m1610(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FetchResponse_t662 * L_3 = ___response;
		NullCheck(L_3);
		bool L_4 = FetchResponse_RequestSucceeded_m2643(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		Action_2_t546 * L_5 = (__this->___callback_0);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>::Invoke(!0,!1) */, L_5, L_6, (Object_t *)NULL);
		goto IL_003b;
	}

IL_0029:
	{
		Action_2_t546 * L_7 = (__this->___callback_0);
		int32_t L_8 = V_0;
		FetchResponse_t662 * L_9 = ___response;
		NullCheck(L_9);
		NativeEvent_t674 * L_10 = FetchResponse_Data_m2644(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>::Invoke(!0,!1) */, L_7, L_8, L_10);
	}

IL_003b:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_22.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_23.h"
// GooglePlayGames.Native.CallbackUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUtilsMethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchAllResponse>
#include "mscorlib_System_Action_1_gen_22MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.EventManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_23MethodDeclarations.h"
struct Misc_t391;
struct EventManager_t548;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.EventManager>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.EventManager>(!!0)
#define Misc_CheckNotNull_TisEventManager_t548_m3765(__this /* static, unused */, p0, method) (( EventManager_t548 * (*) (Object_t * /* static, unused */, EventManager_t548 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t544;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t544_m3766(__this /* static, unused */, p0, method) (( Action_2_t544 * (*) (Object_t * /* static, unused */, Action_2_t544 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_2_t544;
// GooglePlayGames.Native.CallbackUtils
#include "AssemblyU2DCSharp_GooglePlayGames_Native_CallbackUtils.h"
struct CallbackUtils_t395;
struct Action_2_t916;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<System.Int32,System.Object>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<System.Int32,System.Object>(System.Action`2<!!0,!!1>)
extern "C" Action_2_t916 * CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared (Object_t * __this /* static, unused */, Action_2_t916 * p0, MethodInfo* method);
#define CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768(__this /* static, unused */, p0, method) (( Action_2_t916 * (*) (Object_t * /* static, unused */, Action_2_t916 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>(System.Action`2<!!0,!!1>)
#define CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t915_m3767(__this /* static, unused */, p0, method) (( Action_2_t544 * (*) (Object_t * /* static, unused */, Action_2_t544 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t546;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t546_m3769(__this /* static, unused */, p0, method) (( Action_2_t546 * (*) (Object_t * /* static, unused */, Action_2_t546 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeEventClient::.ctor(GooglePlayGames.Native.PInvoke.EventManager)
extern MethodInfo* Misc_CheckNotNull_TisEventManager_t548_m3765_MethodInfo_var;
extern "C" void NativeEventClient__ctor_m2295 (NativeEventClient_t549 * __this, EventManager_t548 * ___manager, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisEventManager_t548_m3765_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483874);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		EventManager_t548 * L_0 = ___manager;
		EventManager_t548 * L_1 = Misc_CheckNotNull_TisEventManager_t548_m3765(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisEventManager_t548_m3765_MethodInfo_var);
		__this->___mEventManager_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeEventClient::FetchAllEvents(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>)
extern TypeInfo* U3CFetchAllEventsU3Ec__AnonStorey24_t545_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t869_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t544_m3766_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t915_m3767_MethodInfo_var;
extern MethodInfo* U3CFetchAllEventsU3Ec__AnonStorey24_U3CU3Em__1C_m2292_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3770_MethodInfo_var;
extern "C" void NativeEventClient_FetchAllEvents_m2296 (NativeEventClient_t549 * __this, int32_t ___source, Action_2_t544 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFetchAllEventsU3Ec__AnonStorey24_t545_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(700);
		Action_1_t869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(701);
		Misc_CheckNotNull_TisAction_2_t544_m3766_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483875);
		CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t915_m3767_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483876);
		U3CFetchAllEventsU3Ec__AnonStorey24_U3CU3Em__1C_m2292_MethodInfo_var = il2cpp_codegen_method_info_from_index(229);
		Action_1__ctor_m3770_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483878);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchAllEventsU3Ec__AnonStorey24_t545 * V_0 = {0};
	{
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_0 = (U3CFetchAllEventsU3Ec__AnonStorey24_t545 *)il2cpp_codegen_object_new (U3CFetchAllEventsU3Ec__AnonStorey24_t545_il2cpp_TypeInfo_var);
		U3CFetchAllEventsU3Ec__AnonStorey24__ctor_m2291(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_1 = V_0;
		Action_2_t544 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_3 = V_0;
		NullCheck(L_3);
		Action_2_t544 * L_4 = (L_3->___callback_0);
		Misc_CheckNotNull_TisAction_2_t544_m3766(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t544_m3766_MethodInfo_var);
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_5 = V_0;
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_6 = V_0;
		NullCheck(L_6);
		Action_2_t544 * L_7 = (L_6->___callback_0);
		Action_2_t544 * L_8 = CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t915_m3767(NULL /*static, unused*/, L_7, /*hidden argument*/CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t915_m3767_MethodInfo_var);
		NullCheck(L_5);
		L_5->___callback_0 = L_8;
		EventManager_t548 * L_9 = (__this->___mEventManager_0);
		int32_t L_10 = ___source;
		int32_t L_11 = ConversionUtils_AsDataSource_m1612(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		U3CFetchAllEventsU3Ec__AnonStorey24_t545 * L_12 = V_0;
		IntPtr_t L_13 = { U3CFetchAllEventsU3Ec__AnonStorey24_U3CU3Em__1C_m2292_MethodInfo_var };
		Action_1_t869 * L_14 = (Action_1_t869 *)il2cpp_codegen_object_new (Action_1_t869_il2cpp_TypeInfo_var);
		Action_1__ctor_m3770(L_14, L_12, L_13, /*hidden argument*/Action_1__ctor_m3770_MethodInfo_var);
		NullCheck(L_9);
		EventManager_FetchAll_m2656(L_9, L_11, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeEventClient::FetchEvent(GooglePlayGames.BasicApi.DataSource,System.String,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>)
extern TypeInfo* U3CFetchEventU3Ec__AnonStorey25_t547_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t870_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t546_m3769_MethodInfo_var;
extern MethodInfo* U3CFetchEventU3Ec__AnonStorey25_U3CU3Em__1D_m2294_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3771_MethodInfo_var;
extern "C" void NativeEventClient_FetchEvent_m2297 (NativeEventClient_t549 * __this, int32_t ___source, String_t* ___eventId, Action_2_t546 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFetchEventU3Ec__AnonStorey25_t547_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(704);
		Action_1_t870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(705);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisAction_2_t546_m3769_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483879);
		U3CFetchEventU3Ec__AnonStorey25_U3CU3Em__1D_m2294_MethodInfo_var = il2cpp_codegen_method_info_from_index(232);
		Action_1__ctor_m3771_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483881);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchEventU3Ec__AnonStorey25_t547 * V_0 = {0};
	{
		U3CFetchEventU3Ec__AnonStorey25_t547 * L_0 = (U3CFetchEventU3Ec__AnonStorey25_t547 *)il2cpp_codegen_object_new (U3CFetchEventU3Ec__AnonStorey25_t547_il2cpp_TypeInfo_var);
		U3CFetchEventU3Ec__AnonStorey25__ctor_m2293(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchEventU3Ec__AnonStorey25_t547 * L_1 = V_0;
		Action_2_t546 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		String_t* L_3 = ___eventId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		U3CFetchEventU3Ec__AnonStorey25_t547 * L_4 = V_0;
		NullCheck(L_4);
		Action_2_t546 * L_5 = (L_4->___callback_0);
		Misc_CheckNotNull_TisAction_2_t546_m3769(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t546_m3769_MethodInfo_var);
		EventManager_t548 * L_6 = (__this->___mEventManager_0);
		int32_t L_7 = ___source;
		int32_t L_8 = ConversionUtils_AsDataSource_m1612(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_9 = ___eventId;
		U3CFetchEventU3Ec__AnonStorey25_t547 * L_10 = V_0;
		IntPtr_t L_11 = { U3CFetchEventU3Ec__AnonStorey25_U3CU3Em__1D_m2294_MethodInfo_var };
		Action_1_t870 * L_12 = (Action_1_t870 *)il2cpp_codegen_object_new (Action_1_t870_il2cpp_TypeInfo_var);
		Action_1__ctor_m3771(L_12, L_10, L_11, /*hidden argument*/Action_1__ctor_m3771_MethodInfo_var);
		NullCheck(L_6);
		EventManager_Fetch_m2658(L_6, L_8, L_9, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeEventClient::IncrementEvent(System.String,System.UInt32)
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void NativeEventClient_IncrementEvent_m2298 (NativeEventClient_t549 * __this, String_t* ___eventId, uint32_t ___stepsToIncrement, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___eventId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		EventManager_t548 * L_1 = (__this->___mEventManager_0);
		String_t* L_2 = ___eventId;
		uint32_t L_3 = ___stepsToIncrement;
		NullCheck(L_1);
		EventManager_Increment_m2660(L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_UMethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>
#include "System_Core_System_Action_2_gen_5.h"
// GooglePlayGames.Native.NativeQuest
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuest.h"
// GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManageMethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>
#include "System_Core_System_Action_2_gen_5MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26::.ctor()
extern "C" void U3CFetchU3Ec__AnonStorey26__ctor_m2299 (U3CFetchU3Ec__AnonStorey26_t551 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient/<Fetch>c__AnonStorey26::<>m__1E(GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse)
extern "C" void U3CFetchU3Ec__AnonStorey26_U3CU3Em__1E_m2300 (U3CFetchU3Ec__AnonStorey26_t551 * __this, FetchResponse_t688 * ___response, MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		FetchResponse_t688 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = FetchResponse_ResponseStatus_m2890(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ConversionUtils_ConvertResponseStatus_m1610(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FetchResponse_t688 * L_3 = ___response;
		NullCheck(L_3);
		bool L_4 = FetchResponse_RequestSucceeded_m2892(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		Action_2_t550 * L_5 = (__this->___callback_0);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(!0,!1) */, L_5, L_6, (Object_t *)NULL);
		goto IL_003b;
	}

IL_0029:
	{
		Action_2_t550 * L_7 = (__this->___callback_0);
		int32_t L_8 = V_0;
		FetchResponse_t688 * L_9 = ___response;
		NullCheck(L_9);
		NativeQuest_t677 * L_10 = FetchResponse_Data_m2891(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(!0,!1) */, L_7, L_8, L_10);
	}

IL_003b:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_0MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_0.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>
#include "System_Core_System_Action_2_gen_6.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_0MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>
#include "System_Core_System_Action_2_gen_6MethodDeclarations.h"
struct Enumerable_t219;
struct IEnumerable_1_t917;
struct IEnumerable_t38;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.Quests.IQuest>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<GooglePlayGames.BasicApi.Quests.IQuest>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisIQuest_t861_m3772(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Enumerable_Cast_TisObject_t_m3761_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t219;
struct List_1_t918;
struct IEnumerable_1_t917;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Quests.IQuest>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<GooglePlayGames.BasicApi.Quests.IQuest>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisIQuest_t861_m3773(__this /* static, unused */, p0, method) (( List_1_t918 * (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Enumerable_ToList_TisObject_t_m3763_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27::.ctor()
extern "C" void U3CFetchMatchingStateU3Ec__AnonStorey27__ctor_m2301 (U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27::<>m__1F(GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse)
extern MethodInfo* Enumerable_Cast_TisIQuest_t861_m3772_MethodInfo_var;
extern MethodInfo* Enumerable_ToList_TisIQuest_t861_m3773_MethodInfo_var;
extern "C" void U3CFetchMatchingStateU3Ec__AnonStorey27_U3CU3Em__1F_m2302 (U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * __this, FetchListResponse_t689 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerable_Cast_TisIQuest_t861_m3772_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483882);
		Enumerable_ToList_TisIQuest_t861_m3773_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483883);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		FetchListResponse_t689 * L_0 = ___response;
		NullCheck(L_0);
		int32_t L_1 = FetchListResponse_ResponseStatus_m2896(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ConversionUtils_ConvertResponseStatus_m1610(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FetchListResponse_t689 * L_3 = ___response;
		NullCheck(L_3);
		bool L_4 = FetchListResponse_RequestSucceeded_m2897(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		Action_2_t552 * L_5 = (__this->___callback_0);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, List_1_t918 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::Invoke(!0,!1) */, L_5, L_6, (List_1_t918 *)NULL);
		goto IL_0045;
	}

IL_0029:
	{
		Action_2_t552 * L_7 = (__this->___callback_0);
		int32_t L_8 = V_0;
		FetchListResponse_t689 * L_9 = ___response;
		NullCheck(L_9);
		Object_t* L_10 = FetchListResponse_Data_m2898(L_9, /*hidden argument*/NULL);
		Object_t* L_11 = Enumerable_Cast_TisIQuest_t861_m3772(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_Cast_TisIQuest_t861_m3772_MethodInfo_var);
		List_1_t918 * L_12 = Enumerable_ToList_TisIQuest_t861_m3773(NULL /*static, unused*/, L_11, /*hidden argument*/Enumerable_ToList_TisIQuest_t861_m3773_MethodInfo_var);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, List_1_t918 * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>::Invoke(!0,!1) */, L_7, L_8, L_12);
	}

IL_0045:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_1MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_3.h"
// GooglePlayGames.Native.NativeQuestMilestone
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestMileston.h"
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
#include "System_Core_System_Action_3_gen_4.h"
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferen.h"
// GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_3MethodDeclarations.h"
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
#include "System_Core_System_Action_3_gen_4MethodDeclarations.h"
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_BaseReferenMethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28::.ctor()
extern "C" void U3CFromQuestUICallbackU3Ec__AnonStorey28__ctor_m2303 (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient/<FromQuestUICallback>c__AnonStorey28::<>m__20(GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" void U3CFromQuestUICallbackU3Ec__AnonStorey28_U3CU3Em__20_m2304 (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * __this, QuestUIResponse_t692 * ___response, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuest_t677 * V_0 = {0};
	NativeQuestMilestone_t676 * V_1 = {0};
	{
		QuestUIResponse_t692 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = QuestUIResponse_RequestSucceeded_m2917(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0024;
		}
	}
	{
		Action_3_t554 * L_2 = (__this->___callback_0);
		QuestUIResponse_t692 * L_3 = ___response;
		NullCheck(L_3);
		int32_t L_4 = QuestUIResponse_RequestStatus_m2916(L_3, /*hidden argument*/NULL);
		int32_t L_5 = NativeQuestClient_UiErrorToQuestUiResult_m2314(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_2, L_5, (Object_t *)NULL, (Object_t *)NULL);
		return;
	}

IL_0024:
	{
		QuestUIResponse_t692 * L_6 = ___response;
		NullCheck(L_6);
		NativeQuest_t677 * L_7 = QuestUIResponse_AcceptedQuest_m2918(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		QuestUIResponse_t692 * L_8 = ___response;
		NullCheck(L_8);
		NativeQuestMilestone_t676 * L_9 = QuestUIResponse_MilestoneToClaim_m2919(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		NativeQuest_t677 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0051;
		}
	}
	{
		Action_3_t554 * L_11 = (__this->___callback_0);
		NativeQuest_t677 * L_12 = V_0;
		NullCheck(L_11);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_11, 0, L_12, (Object_t *)NULL);
		NativeQuestMilestone_t676 * L_13 = V_1;
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_13);
		goto IL_0099;
	}

IL_0051:
	{
		NativeQuestMilestone_t676 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0075;
		}
	}
	{
		Action_3_t554 * L_15 = (__this->___callback_0);
		QuestUIResponse_t692 * L_16 = ___response;
		NullCheck(L_16);
		NativeQuestMilestone_t676 * L_17 = QuestUIResponse_MilestoneToClaim_m2919(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_15, 1, (Object_t *)NULL, L_17);
		NativeQuest_t677 * L_18 = V_0;
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_18);
		goto IL_0099;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral485, /*hidden argument*/NULL);
		NativeQuest_t677 * L_19 = V_0;
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_19);
		NativeQuestMilestone_t676 * L_20 = V_1;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(4 /* System.Void GooglePlayGames.Native.PInvoke.BaseReferenceHolder::Dispose() */, L_20);
		Action_3_t554 * L_21 = (__this->___callback_0);
		NullCheck(L_21);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_21, 3, (Object_t *)NULL, (Object_t *)NULL);
	}

IL_0099:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_2MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_2.h"
// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>
#include "System_Core_System_Action_2_gen_7.h"
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccep.h"
// GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_2MethodDeclarations.h"
// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>
#include "System_Core_System_Action_2_gen_7MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29::.ctor()
extern "C" void U3CAcceptU3Ec__AnonStorey29__ctor_m2305 (U3CAcceptU3Ec__AnonStorey29_t557 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29::<>m__21(GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse)
extern "C" void U3CAcceptU3Ec__AnonStorey29_U3CU3Em__21_m2306 (U3CAcceptU3Ec__AnonStorey29_t557 * __this, AcceptResponse_t691 * ___response, MethodInfo* method)
{
	{
		AcceptResponse_t691 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = AcceptResponse_RequestSucceeded_m2912(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Action_2_t556 * L_2 = (__this->___callback_0);
		AcceptResponse_t691 * L_3 = ___response;
		NullCheck(L_3);
		NativeQuest_t677 * L_4 = AcceptResponse_AcceptedQuest_m2911(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(!0,!1) */, L_2, 0, L_4);
		goto IL_0039;
	}

IL_0022:
	{
		Action_2_t556 * L_5 = (__this->___callback_0);
		AcceptResponse_t691 * L_6 = ___response;
		NullCheck(L_6);
		int32_t L_7 = AcceptResponse_ResponseStatus_m2910(L_6, /*hidden argument*/NULL);
		int32_t L_8 = NativeQuestClient_FromAcceptStatus_m2317(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(!0,!1) */, L_5, L_8, (Object_t *)NULL);
	}

IL_0039:
	{
		return;
	}
}
// GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeQuestClient_U_3MethodDeclarations.h"

// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_1.h"
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
#include "System_Core_System_Action_3_gen_5.h"
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaim.h"
// GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_QuestManage_1MethodDeclarations.h"
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
#include "System_Core_System_Action_3_gen_5MethodDeclarations.h"


// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A::.ctor()
extern "C" void U3CClaimMilestoneU3Ec__AnonStorey2A__ctor_m2307 (U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient/<ClaimMilestone>c__AnonStorey2A::<>m__22(GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse)
extern "C" void U3CClaimMilestoneU3Ec__AnonStorey2A_U3CU3Em__22_m2308 (U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * __this, ClaimMilestoneResponse_t690 * ___response, MethodInfo* method)
{
	{
		ClaimMilestoneResponse_t690 * L_0 = ___response;
		NullCheck(L_0);
		bool L_1 = ClaimMilestoneResponse_RequestSucceeded_m2906(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Action_3_t558 * L_2 = (__this->___callback_0);
		ClaimMilestoneResponse_t690 * L_3 = ___response;
		NullCheck(L_3);
		NativeQuest_t677 * L_4 = ClaimMilestoneResponse_Quest_m2904(L_3, /*hidden argument*/NULL);
		ClaimMilestoneResponse_t690 * L_5 = ___response;
		NullCheck(L_5);
		NativeQuestMilestone_t676 * L_6 = ClaimMilestoneResponse_ClaimedMilestone_m2905(L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_2, 0, L_4, L_6);
		goto IL_0040;
	}

IL_0028:
	{
		Action_3_t558 * L_7 = (__this->___callback_0);
		ClaimMilestoneResponse_t690 * L_8 = ___response;
		NullCheck(L_8);
		int32_t L_9 = ClaimMilestoneResponse_ResponseStatus_m2903(L_8, /*hidden argument*/NULL);
		int32_t L_10 = NativeQuestClient_FromClaimStatus_m2319(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_7, L_10, (Object_t *)NULL, (Object_t *)NULL);
	}

IL_0040:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_24.h"
// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestFetch.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>
#include "mscorlib_System_Action_1_gen_25.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>
#include "mscorlib_System_Action_1_gen_26.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse>
#include "mscorlib_System_Action_1_gen_27.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>
#include "mscorlib_System_Action_1_gen_28.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchResponse>
#include "mscorlib_System_Action_1_gen_24MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse>
#include "mscorlib_System_Action_1_gen_25MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>
#include "mscorlib_System_Action_1_gen_26MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/AcceptResponse>
#include "mscorlib_System_Action_1_gen_27MethodDeclarations.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/ClaimMilestoneResponse>
#include "mscorlib_System_Action_1_gen_28MethodDeclarations.h"
struct Misc_t391;
struct QuestManager_t560;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.QuestManager>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.QuestManager>(!!0)
#define Misc_CheckNotNull_TisQuestManager_t560_m3774(__this /* static, unused */, p0, method) (( QuestManager_t560 * (*) (Object_t * /* static, unused */, QuestManager_t560 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t550;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t550_m3775(__this /* static, unused */, p0, method) (( Action_2_t550 * (*) (Object_t * /* static, unused */, Action_2_t550 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_2_t550;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>(System.Action`2<!!0,!!1>)
#define CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisIQuest_t861_m3776(__this /* static, unused */, p0, method) (( Action_2_t550 * (*) (Object_t * /* static, unused */, Action_2_t550 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t552;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t552_m3777(__this /* static, unused */, p0, method) (( Action_2_t552 * (*) (Object_t * /* static, unused */, Action_2_t552 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_2_t552;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>(System.Action`2<!!0,!!1>)
#define CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t918_m3778(__this /* static, unused */, p0, method) (( Action_2_t552 * (*) (Object_t * /* static, unused */, Action_2_t552 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_3_t554;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>>(!!0)
#define Misc_CheckNotNull_TisAction_3_t554_m3779(__this /* static, unused */, p0, method) (( Action_3_t554 * (*) (Object_t * /* static, unused */, Action_3_t554 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_3_t554;
struct CallbackUtils_t395;
struct Action_3_t919;
// Declaration System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<System.Int32,System.Object,System.Object>(System.Action`3<!!0,!!1,!!2>)
// System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<System.Int32,System.Object,System.Object>(System.Action`3<!!0,!!1,!!2>)
extern "C" Action_3_t919 * CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared (Object_t * __this /* static, unused */, Action_3_t919 * p0, MethodInfo* method);
#define CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781(__this /* static, unused */, p0, method) (( Action_3_t919 * (*) (Object_t * /* static, unused */, Action_3_t919 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>(System.Action`3<!!0,!!1,!!2>)
// System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>(System.Action`3<!!0,!!1,!!2>)
#define CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780(__this /* static, unused */, p0, method) (( Action_3_t554 * (*) (Object_t * /* static, unused */, Action_3_t554 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct IQuest_t861;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Quests.IQuest>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Quests.IQuest>(!!0)
#define Misc_CheckNotNull_TisIQuest_t861_m3782(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_2_t556;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>>(!!0)
#define Misc_CheckNotNull_TisAction_2_t556_m3783(__this /* static, unused */, p0, method) (( Action_2_t556 * (*) (Object_t * /* static, unused */, Action_2_t556 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_2_t556;
// Declaration System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>(System.Action`2<!!0,!!1>)
// System.Action`2<!!0,!!1> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>(System.Action`2<!!0,!!1>)
#define CallbackUtils_ToOnGameThread_TisQuestAcceptStatus_t370_TisIQuest_t861_m3784(__this /* static, unused */, p0, method) (( Action_2_t556 * (*) (Object_t * /* static, unused */, Action_2_t556 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_m3768_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct IQuestMilestone_t863;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Quests.IQuestMilestone>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.BasicApi.Quests.IQuestMilestone>(!!0)
#define Misc_CheckNotNull_TisIQuestMilestone_t863_m3785(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct Action_3_t558;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>>(!!0)
#define Misc_CheckNotNull_TisAction_3_t558_m3786(__this /* static, unused */, p0, method) (( Action_3_t558 * (*) (Object_t * /* static, unused */, Action_3_t558 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct CallbackUtils_t395;
struct Action_3_t558;
// Declaration System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>(System.Action`3<!!0,!!1,!!2>)
// System.Action`3<!!0,!!1,!!2> GooglePlayGames.Native.CallbackUtils::ToOnGameThread<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>(System.Action`3<!!0,!!1,!!2>)
#define CallbackUtils_ToOnGameThread_TisQuestClaimMilestoneStatus_t371_TisIQuest_t861_TisIQuestMilestone_t863_m3787(__this /* static, unused */, p0, method) (( Action_3_t558 * (*) (Object_t * /* static, unused */, Action_3_t558 *, MethodInfo*))CallbackUtils_ToOnGameThread_TisInt32_t189_TisObject_t_TisObject_t_m3781_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeQuestClient::.ctor(GooglePlayGames.Native.PInvoke.QuestManager)
extern MethodInfo* Misc_CheckNotNull_TisQuestManager_t560_m3774_MethodInfo_var;
extern "C" void NativeQuestClient__ctor_m2309 (NativeQuestClient_t561 * __this, QuestManager_t560 * ___manager, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisQuestManager_t560_m3774_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483884);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		QuestManager_t560 * L_0 = ___manager;
		QuestManager_t560 * L_1 = Misc_CheckNotNull_TisQuestManager_t560_m3774(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisQuestManager_t560_m3774_MethodInfo_var);
		__this->___mManager_0 = L_1;
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::Fetch(GooglePlayGames.BasicApi.DataSource,System.String,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>)
extern TypeInfo* U3CFetchU3Ec__AnonStorey26_t551_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t877_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t550_m3775_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisIQuest_t861_m3776_MethodInfo_var;
extern MethodInfo* U3CFetchU3Ec__AnonStorey26_U3CU3Em__1E_m2300_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3788_MethodInfo_var;
extern "C" void NativeQuestClient_Fetch_m2310 (NativeQuestClient_t561 * __this, int32_t ___source, String_t* ___questId, Action_2_t550 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFetchU3Ec__AnonStorey26_t551_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(709);
		Action_1_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(710);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		Misc_CheckNotNull_TisAction_2_t550_m3775_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483885);
		CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisIQuest_t861_m3776_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483886);
		U3CFetchU3Ec__AnonStorey26_U3CU3Em__1E_m2300_MethodInfo_var = il2cpp_codegen_method_info_from_index(239);
		Action_1__ctor_m3788_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483888);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchU3Ec__AnonStorey26_t551 * V_0 = {0};
	{
		U3CFetchU3Ec__AnonStorey26_t551 * L_0 = (U3CFetchU3Ec__AnonStorey26_t551 *)il2cpp_codegen_object_new (U3CFetchU3Ec__AnonStorey26_t551_il2cpp_TypeInfo_var);
		U3CFetchU3Ec__AnonStorey26__ctor_m2299(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchU3Ec__AnonStorey26_t551 * L_1 = V_0;
		Action_2_t550 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		String_t* L_3 = ___questId;
		Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
		U3CFetchU3Ec__AnonStorey26_t551 * L_4 = V_0;
		NullCheck(L_4);
		Action_2_t550 * L_5 = (L_4->___callback_0);
		Misc_CheckNotNull_TisAction_2_t550_m3775(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t550_m3775_MethodInfo_var);
		U3CFetchU3Ec__AnonStorey26_t551 * L_6 = V_0;
		U3CFetchU3Ec__AnonStorey26_t551 * L_7 = V_0;
		NullCheck(L_7);
		Action_2_t550 * L_8 = (L_7->___callback_0);
		Action_2_t550 * L_9 = CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisIQuest_t861_m3776(NULL /*static, unused*/, L_8, /*hidden argument*/CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisIQuest_t861_m3776_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_0 = L_9;
		QuestManager_t560 * L_10 = (__this->___mManager_0);
		int32_t L_11 = ___source;
		int32_t L_12 = ConversionUtils_AsDataSource_m1612(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		String_t* L_13 = ___questId;
		U3CFetchU3Ec__AnonStorey26_t551 * L_14 = V_0;
		IntPtr_t L_15 = { U3CFetchU3Ec__AnonStorey26_U3CU3Em__1E_m2300_MethodInfo_var };
		Action_1_t877 * L_16 = (Action_1_t877 *)il2cpp_codegen_object_new (Action_1_t877_il2cpp_TypeInfo_var);
		Action_1__ctor_m3788(L_16, L_14, L_15, /*hidden argument*/Action_1__ctor_m3788_MethodInfo_var);
		NullCheck(L_10);
		QuestManager_Fetch_m2923(L_10, L_12, L_13, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::FetchMatchingState(GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.Quests.QuestFetchFlags,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>)
extern TypeInfo* U3CFetchMatchingStateU3Ec__AnonStorey27_t553_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t878_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t552_m3777_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t918_m3778_MethodInfo_var;
extern MethodInfo* U3CFetchMatchingStateU3Ec__AnonStorey27_U3CU3Em__1F_m2302_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3789_MethodInfo_var;
extern "C" void NativeQuestClient_FetchMatchingState_m2311 (NativeQuestClient_t561 * __this, int32_t ___source, int32_t ___flags, Action_2_t552 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(714);
		Action_1_t878_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(715);
		Misc_CheckNotNull_TisAction_2_t552_m3777_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483889);
		CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t918_m3778_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483890);
		U3CFetchMatchingStateU3Ec__AnonStorey27_U3CU3Em__1F_m2302_MethodInfo_var = il2cpp_codegen_method_info_from_index(243);
		Action_1__ctor_m3789_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483892);
		s_Il2CppMethodIntialized = true;
	}
	U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * V_0 = {0};
	{
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_0 = (U3CFetchMatchingStateU3Ec__AnonStorey27_t553 *)il2cpp_codegen_object_new (U3CFetchMatchingStateU3Ec__AnonStorey27_t553_il2cpp_TypeInfo_var);
		U3CFetchMatchingStateU3Ec__AnonStorey27__ctor_m2301(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_1 = V_0;
		Action_2_t552 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_3 = V_0;
		NullCheck(L_3);
		Action_2_t552 * L_4 = (L_3->___callback_0);
		Misc_CheckNotNull_TisAction_2_t552_m3777(NULL /*static, unused*/, L_4, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t552_m3777_MethodInfo_var);
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_5 = V_0;
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_6 = V_0;
		NullCheck(L_6);
		Action_2_t552 * L_7 = (L_6->___callback_0);
		Action_2_t552 * L_8 = CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t918_m3778(NULL /*static, unused*/, L_7, /*hidden argument*/CallbackUtils_ToOnGameThread_TisResponseStatus_t335_TisList_1_t918_m3778_MethodInfo_var);
		NullCheck(L_5);
		L_5->___callback_0 = L_8;
		QuestManager_t560 * L_9 = (__this->___mManager_0);
		int32_t L_10 = ___source;
		int32_t L_11 = ConversionUtils_AsDataSource_m1612(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___flags;
		U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * L_13 = V_0;
		IntPtr_t L_14 = { U3CFetchMatchingStateU3Ec__AnonStorey27_U3CU3Em__1F_m2302_MethodInfo_var };
		Action_1_t878 * L_15 = (Action_1_t878 *)il2cpp_codegen_object_new (Action_1_t878_il2cpp_TypeInfo_var);
		Action_1__ctor_m3789(L_15, L_13, L_14, /*hidden argument*/Action_1__ctor_m3789_MethodInfo_var);
		NullCheck(L_9);
		QuestManager_FetchList_m2925(L_9, L_11, L_12, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::ShowAllQuestsUI(System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern MethodInfo* Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var;
extern "C" void NativeQuestClient_ShowAllQuestsUI_m2312 (NativeQuestClient_t561 * __this, Action_3_t554 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483893);
		CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483894);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_3_t554 * L_0 = ___callback;
		Misc_CheckNotNull_TisAction_3_t554_m3779(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var);
		Action_3_t554 * L_1 = ___callback;
		Action_3_t554 * L_2 = CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780(NULL /*static, unused*/, L_1, /*hidden argument*/CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var);
		___callback = L_2;
		QuestManager_t560 * L_3 = (__this->___mManager_0);
		Action_3_t554 * L_4 = ___callback;
		Action_1_t862 * L_5 = NativeQuestClient_FromQuestUICallback_m2315(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		QuestManager_ShowAllQuestUI_m2927(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::ShowSpecificQuestUI(GooglePlayGames.BasicApi.Quests.IQuest,System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var;
extern "C" void NativeQuestClient_ShowSpecificQuestUI_m2313 (NativeQuestClient_t561 * __this, Object_t * ___quest, Action_3_t554 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483895);
		Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483893);
		CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483894);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuest_t677 * V_0 = {0};
	{
		Object_t * L_0 = ___quest;
		Misc_CheckNotNull_TisIQuest_t861_m3782(NULL /*static, unused*/, L_0, /*hidden argument*/Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var);
		Action_3_t554 * L_1 = ___callback;
		Misc_CheckNotNull_TisAction_3_t554_m3779(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisAction_3_t554_m3779_MethodInfo_var);
		Action_3_t554 * L_2 = ___callback;
		Action_3_t554 * L_3 = CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780(NULL /*static, unused*/, L_2, /*hidden argument*/CallbackUtils_ToOnGameThread_TisQuestUiResult_t372_TisIQuest_t861_TisIQuestMilestone_t863_m3780_MethodInfo_var);
		___callback = L_3;
		Object_t * L_4 = ___quest;
		V_0 = ((NativeQuest_t677 *)IsInst(L_4, NativeQuest_t677_il2cpp_TypeInfo_var));
		NativeQuest_t677 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral481, /*hidden argument*/NULL);
		Action_3_t554 * L_6 = ___callback;
		NullCheck(L_6);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_6, 2, (Object_t *)NULL, (Object_t *)NULL);
		return;
	}

IL_0037:
	{
		QuestManager_t560 * L_7 = (__this->___mManager_0);
		NativeQuest_t677 * L_8 = V_0;
		Action_3_t554 * L_9 = ___callback;
		Action_1_t862 * L_10 = NativeQuestClient_FromQuestUICallback_m2315(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		QuestManager_ShowQuestUI_m2928(L_7, L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Quests.QuestUiResult GooglePlayGames.Native.NativeQuestClient::UiErrorToQuestUiResult(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern TypeInfo* UIStatus_t412_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeQuestClient_UiErrorToQuestUiResult_m2314 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIStatus_t412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 0)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 1)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 2)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 3)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 4)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 5)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 6)
		{
			goto IL_0040;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 7)
		{
			goto IL_0044;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 8)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 9)
		{
			goto IL_003e;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)((int32_t)12))) == 10)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_0048;
	}

IL_003c:
	{
		return (int32_t)(3);
	}

IL_003e:
	{
		return (int32_t)(5);
	}

IL_0040:
	{
		return (int32_t)(4);
	}

IL_0042:
	{
		return (int32_t)(6);
	}

IL_0044:
	{
		return (int32_t)(7);
	}

IL_0046:
	{
		return (int32_t)(8);
	}

IL_0048:
	{
		int32_t L_2 = ___status;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(UIStatus_t412_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral482, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (int32_t)(3);
	}
}
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse> GooglePlayGames.Native.NativeQuestClient::FromQuestUICallback(System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern TypeInfo* U3CFromQuestUICallbackU3Ec__AnonStorey28_t555_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t862_il2cpp_TypeInfo_var;
extern MethodInfo* U3CFromQuestUICallbackU3Ec__AnonStorey28_U3CU3Em__20_m2304_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3790_MethodInfo_var;
extern "C" Action_1_t862 * NativeQuestClient_FromQuestUICallback_m2315 (Object_t * __this /* static, unused */, Action_3_t554 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFromQuestUICallbackU3Ec__AnonStorey28_t555_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(721);
		Action_1_t862_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(722);
		U3CFromQuestUICallbackU3Ec__AnonStorey28_U3CU3Em__20_m2304_MethodInfo_var = il2cpp_codegen_method_info_from_index(248);
		Action_1__ctor_m3790_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483897);
		s_Il2CppMethodIntialized = true;
	}
	U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * V_0 = {0};
	{
		U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * L_0 = (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 *)il2cpp_codegen_object_new (U3CFromQuestUICallbackU3Ec__AnonStorey28_t555_il2cpp_TypeInfo_var);
		U3CFromQuestUICallbackU3Ec__AnonStorey28__ctor_m2303(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * L_1 = V_0;
		Action_3_t554 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		U3CFromQuestUICallbackU3Ec__AnonStorey28_t555 * L_3 = V_0;
		IntPtr_t L_4 = { U3CFromQuestUICallbackU3Ec__AnonStorey28_U3CU3Em__20_m2304_MethodInfo_var };
		Action_1_t862 * L_5 = (Action_1_t862 *)il2cpp_codegen_object_new (Action_1_t862_il2cpp_TypeInfo_var);
		Action_1__ctor_m3790(L_5, L_3, L_4, /*hidden argument*/Action_1__ctor_m3790_MethodInfo_var);
		return L_5;
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::Accept(GooglePlayGames.BasicApi.Quests.IQuest,System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>)
extern TypeInfo* U3CAcceptU3Ec__AnonStorey29_t557_il2cpp_TypeInfo_var;
extern TypeInfo* NativeQuest_t677_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t879_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_2_t556_m3783_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisQuestAcceptStatus_t370_TisIQuest_t861_m3784_MethodInfo_var;
extern MethodInfo* U3CAcceptU3Ec__AnonStorey29_U3CU3Em__21_m2306_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3791_MethodInfo_var;
extern "C" void NativeQuestClient_Accept_m2316 (NativeQuestClient_t561 * __this, Object_t * ___quest, Action_2_t556 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAcceptU3Ec__AnonStorey29_t557_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(726);
		NativeQuest_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(719);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(727);
		Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483895);
		Misc_CheckNotNull_TisAction_2_t556_m3783_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483898);
		CallbackUtils_ToOnGameThread_TisQuestAcceptStatus_t370_TisIQuest_t861_m3784_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483899);
		U3CAcceptU3Ec__AnonStorey29_U3CU3Em__21_m2306_MethodInfo_var = il2cpp_codegen_method_info_from_index(252);
		Action_1__ctor_m3791_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483901);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuest_t677 * V_0 = {0};
	U3CAcceptU3Ec__AnonStorey29_t557 * V_1 = {0};
	{
		U3CAcceptU3Ec__AnonStorey29_t557 * L_0 = (U3CAcceptU3Ec__AnonStorey29_t557 *)il2cpp_codegen_object_new (U3CAcceptU3Ec__AnonStorey29_t557_il2cpp_TypeInfo_var);
		U3CAcceptU3Ec__AnonStorey29__ctor_m2305(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CAcceptU3Ec__AnonStorey29_t557 * L_1 = V_1;
		Action_2_t556 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		Object_t * L_3 = ___quest;
		Misc_CheckNotNull_TisIQuest_t861_m3782(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisIQuest_t861_m3782_MethodInfo_var);
		U3CAcceptU3Ec__AnonStorey29_t557 * L_4 = V_1;
		NullCheck(L_4);
		Action_2_t556 * L_5 = (L_4->___callback_0);
		Misc_CheckNotNull_TisAction_2_t556_m3783(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_2_t556_m3783_MethodInfo_var);
		U3CAcceptU3Ec__AnonStorey29_t557 * L_6 = V_1;
		U3CAcceptU3Ec__AnonStorey29_t557 * L_7 = V_1;
		NullCheck(L_7);
		Action_2_t556 * L_8 = (L_7->___callback_0);
		Action_2_t556 * L_9 = CallbackUtils_ToOnGameThread_TisQuestAcceptStatus_t370_TisIQuest_t861_m3784(NULL /*static, unused*/, L_8, /*hidden argument*/CallbackUtils_ToOnGameThread_TisQuestAcceptStatus_t370_TisIQuest_t861_m3784_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_0 = L_9;
		Object_t * L_10 = ___quest;
		V_0 = ((NativeQuest_t677 *)IsInst(L_10, NativeQuest_t677_il2cpp_TypeInfo_var));
		NativeQuest_t677 * L_11 = V_0;
		if (L_11)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral481, /*hidden argument*/NULL);
		U3CAcceptU3Ec__AnonStorey29_t557 * L_12 = V_1;
		NullCheck(L_12);
		Action_2_t556 * L_13 = (L_12->___callback_0);
		NullCheck(L_13);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(10 /* System.Void System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>::Invoke(!0,!1) */, L_13, 1, (Object_t *)NULL);
		return;
	}

IL_0056:
	{
		QuestManager_t560 * L_14 = (__this->___mManager_0);
		NativeQuest_t677 * L_15 = V_0;
		U3CAcceptU3Ec__AnonStorey29_t557 * L_16 = V_1;
		IntPtr_t L_17 = { U3CAcceptU3Ec__AnonStorey29_U3CU3Em__21_m2306_MethodInfo_var };
		Action_1_t879 * L_18 = (Action_1_t879 *)il2cpp_codegen_object_new (Action_1_t879_il2cpp_TypeInfo_var);
		Action_1__ctor_m3791(L_18, L_16, L_17, /*hidden argument*/Action_1__ctor_m3791_MethodInfo_var);
		NullCheck(L_14);
		QuestManager_Accept_m2930(L_14, L_15, L_18, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus GooglePlayGames.Native.NativeQuestClient::FromAcceptStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus)
extern TypeInfo* QuestAcceptStatus_t414_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeQuestClient_FromAcceptStatus_m2317 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuestAcceptStatus_t414_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(728);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 0)
		{
			goto IL_0043;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 1)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 2)
		{
			goto IL_003d;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 3)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 4)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 5)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 6)
		{
			goto IL_0045;
		}
	}

IL_0026:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)-14))))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)-13))))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0047;
	}

IL_003b:
	{
		return (int32_t)(2);
	}

IL_003d:
	{
		return (int32_t)(3);
	}

IL_003f:
	{
		return (int32_t)(6);
	}

IL_0041:
	{
		return (int32_t)(5);
	}

IL_0043:
	{
		return (int32_t)(4);
	}

IL_0045:
	{
		return (int32_t)(0);
	}

IL_0047:
	{
		int32_t L_4 = ___status;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(QuestAcceptStatus_t414_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral483, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return (int32_t)(2);
	}
}
// System.Void GooglePlayGames.Native.NativeQuestClient::ClaimMilestone(GooglePlayGames.BasicApi.Quests.IQuestMilestone,System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern TypeInfo* U3CClaimMilestoneU3Ec__AnonStorey2A_t559_il2cpp_TypeInfo_var;
extern TypeInfo* NativeQuestMilestone_t676_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t880_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisIQuestMilestone_t863_m3785_MethodInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisAction_3_t558_m3786_MethodInfo_var;
extern MethodInfo* CallbackUtils_ToOnGameThread_TisQuestClaimMilestoneStatus_t371_TisIQuest_t861_TisIQuestMilestone_t863_m3787_MethodInfo_var;
extern MethodInfo* U3CClaimMilestoneU3Ec__AnonStorey2A_U3CU3Em__22_m2308_MethodInfo_var;
extern MethodInfo* Action_1__ctor_m3792_MethodInfo_var;
extern "C" void NativeQuestClient_ClaimMilestone_m2318 (NativeQuestClient_t561 * __this, Object_t * ___milestone, Action_3_t558 * ___callback, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(732);
		NativeQuestMilestone_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(733);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Action_1_t880_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(734);
		Misc_CheckNotNull_TisIQuestMilestone_t863_m3785_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483902);
		Misc_CheckNotNull_TisAction_3_t558_m3786_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483903);
		CallbackUtils_ToOnGameThread_TisQuestClaimMilestoneStatus_t371_TisIQuest_t861_TisIQuestMilestone_t863_m3787_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483904);
		U3CClaimMilestoneU3Ec__AnonStorey2A_U3CU3Em__22_m2308_MethodInfo_var = il2cpp_codegen_method_info_from_index(257);
		Action_1__ctor_m3792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483906);
		s_Il2CppMethodIntialized = true;
	}
	NativeQuestMilestone_t676 * V_0 = {0};
	U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * V_1 = {0};
	{
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_0 = (U3CClaimMilestoneU3Ec__AnonStorey2A_t559 *)il2cpp_codegen_object_new (U3CClaimMilestoneU3Ec__AnonStorey2A_t559_il2cpp_TypeInfo_var);
		U3CClaimMilestoneU3Ec__AnonStorey2A__ctor_m2307(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_1 = V_1;
		Action_3_t558 * L_2 = ___callback;
		NullCheck(L_1);
		L_1->___callback_0 = L_2;
		Object_t * L_3 = ___milestone;
		Misc_CheckNotNull_TisIQuestMilestone_t863_m3785(NULL /*static, unused*/, L_3, /*hidden argument*/Misc_CheckNotNull_TisIQuestMilestone_t863_m3785_MethodInfo_var);
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_4 = V_1;
		NullCheck(L_4);
		Action_3_t558 * L_5 = (L_4->___callback_0);
		Misc_CheckNotNull_TisAction_3_t558_m3786(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisAction_3_t558_m3786_MethodInfo_var);
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_6 = V_1;
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_7 = V_1;
		NullCheck(L_7);
		Action_3_t558 * L_8 = (L_7->___callback_0);
		Action_3_t558 * L_9 = CallbackUtils_ToOnGameThread_TisQuestClaimMilestoneStatus_t371_TisIQuest_t861_TisIQuestMilestone_t863_m3787(NULL /*static, unused*/, L_8, /*hidden argument*/CallbackUtils_ToOnGameThread_TisQuestClaimMilestoneStatus_t371_TisIQuest_t861_TisIQuestMilestone_t863_m3787_MethodInfo_var);
		NullCheck(L_6);
		L_6->___callback_0 = L_9;
		Object_t * L_10 = ___milestone;
		V_0 = ((NativeQuestMilestone_t676 *)IsInst(L_10, NativeQuestMilestone_t676_il2cpp_TypeInfo_var));
		NativeQuestMilestone_t676 * L_11 = V_0;
		if (L_11)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral484, /*hidden argument*/NULL);
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_12 = V_1;
		NullCheck(L_12);
		Action_3_t558 * L_13 = (L_12->___callback_0);
		NullCheck(L_13);
		VirtActionInvoker3< int32_t, Object_t *, Object_t * >::Invoke(10 /* System.Void System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>::Invoke(!0,!1,!2) */, L_13, 1, (Object_t *)NULL, (Object_t *)NULL);
		return;
	}

IL_0057:
	{
		QuestManager_t560 * L_14 = (__this->___mManager_0);
		NativeQuestMilestone_t676 * L_15 = V_0;
		U3CClaimMilestoneU3Ec__AnonStorey2A_t559 * L_16 = V_1;
		IntPtr_t L_17 = { U3CClaimMilestoneU3Ec__AnonStorey2A_U3CU3Em__22_m2308_MethodInfo_var };
		Action_1_t880 * L_18 = (Action_1_t880 *)il2cpp_codegen_object_new (Action_1_t880_il2cpp_TypeInfo_var);
		Action_1__ctor_m3792(L_18, L_16, L_17, /*hidden argument*/Action_1__ctor_m3792_MethodInfo_var);
		NullCheck(L_14);
		QuestManager_ClaimMilestone_m2932(L_14, L_15, L_18, /*hidden argument*/NULL);
		return;
	}
}
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus GooglePlayGames.Native.NativeQuestClient::FromClaimStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus)
extern TypeInfo* QuestClaimMilestoneStatus_t415_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern "C" int32_t NativeQuestClient_FromClaimStatus_m2319 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuestClaimMilestoneStatus_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(735);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___status;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 0)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 1)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 2)
		{
			goto IL_0043;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 3)
		{
			goto IL_003d;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 4)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 5)
		{
			goto IL_0026;
		}
		if (((int32_t)((int32_t)L_1+(int32_t)5)) == 6)
		{
			goto IL_003b;
		}
	}

IL_0026:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)-16))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)-15))))
		{
			goto IL_003f;
		}
	}
	{
		goto IL_0047;
	}

IL_003b:
	{
		return (int32_t)(0);
	}

IL_003d:
	{
		return (int32_t)(2);
	}

IL_003f:
	{
		return (int32_t)(5);
	}

IL_0041:
	{
		return (int32_t)(6);
	}

IL_0043:
	{
		return (int32_t)(3);
	}

IL_0045:
	{
		return (int32_t)(4);
	}

IL_0047:
	{
		int32_t L_4 = ___status;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(QuestClaimMilestoneStatus_t415_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m909(NULL /*static, unused*/, (String_t*) &_stringLiteral483, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_e_m1593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return (int32_t)(2);
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMultiMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// GooglePlayGames.BasicApi.Multiplayer.Participant
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti_0.h"


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::.ctor()
extern "C" void NoopListener__ctor_m2320 (NoopListener_t562 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRoomSetupProgress(System.Single)
extern "C" void NoopListener_OnRoomSetupProgress_m2321 (NoopListener_t562 * __this, float ___percent, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRoomConnected(System.Boolean)
extern "C" void NoopListener_OnRoomConnected_m2322 (NoopListener_t562 * __this, bool ___success, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnLeftRoom()
extern "C" void NoopListener_OnLeftRoom_m2323 (NoopListener_t562 * __this, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnParticipantLeft(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" void NoopListener_OnParticipantLeft_m2324 (NoopListener_t562 * __this, Participant_t340 * ___participant, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnPeersConnected(System.String[])
extern "C" void NoopListener_OnPeersConnected_m2325 (NoopListener_t562 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnPeersDisconnected(System.String[])
extern "C" void NoopListener_OnPeersDisconnected_m2326 (NoopListener_t562 * __this, StringU5BU5D_t169* ___participantIds, MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/NoopListener::OnRealTimeMessageReceived(System.Boolean,System.String,System.Byte[])
extern "C" void NoopListener_OnRealTimeMessageReceived_m2327 (NoopListener_t562 * __this, bool ___isReliable, String_t* ___senderId, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	{
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_0.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_0MethodDeclarations.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_7.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_10.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Action
#include "System_Core_System_Action.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_11.h"
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_NativeRealT.h"
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_Multiplayer_0.h"
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_RealtimeMan.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_7MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/BeforeRoomCreateStartedState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_10MethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_8MethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomCreationPendingState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_11MethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
struct Misc_t391;
struct RealtimeManager_t564;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.RealtimeManager>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.PInvoke.RealtimeManager>(!!0)
#define Misc_CheckNotNull_TisRealtimeManager_t564_m3793(__this /* static, unused */, p0, method) (( RealtimeManager_t564 * (*) (Object_t * /* static, unused */, RealtimeManager_t564 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)
struct Misc_t391;
struct State_t565;
// Declaration !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State>(!!0)
// !!0 GooglePlayGames.OurUtils.Misc::CheckNotNull<GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State>(!!0)
#define Misc_CheckNotNull_TisState_t565_m3794(__this /* static, unused */, p0, method) (( State_t565 * (*) (Object_t * /* static, unused */, State_t565 *, MethodInfo*))Misc_CheckNotNull_TisObject_t_m3706_gshared)(__this /* static, unused */, p0, method)


// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::.ctor(GooglePlayGames.Native.PInvoke.RealtimeManager,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* OnGameThreadForwardingListener_t563_il2cpp_TypeInfo_var;
extern TypeInfo* BeforeRoomCreateStartedState_t581_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var;
extern "C" void RoomSession__ctor_m2328 (RoomSession_t566 * __this, RealtimeManager_t564 * ___manager, Object_t * ___listener, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		OnGameThreadForwardingListener_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(736);
		BeforeRoomCreateStartedState_t581_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(737);
		Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483907);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m830(L_0, /*hidden argument*/NULL);
		__this->___mLifecycleLock_0 = L_0;
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		RealtimeManager_t564 * L_1 = ___manager;
		RealtimeManager_t564 * L_2 = Misc_CheckNotNull_TisRealtimeManager_t564_m3793(NULL /*static, unused*/, L_1, /*hidden argument*/Misc_CheckNotNull_TisRealtimeManager_t564_m3793_MethodInfo_var);
		__this->___mManager_2 = L_2;
		Object_t * L_3 = ___listener;
		OnGameThreadForwardingListener_t563 * L_4 = (OnGameThreadForwardingListener_t563 *)il2cpp_codegen_object_new (OnGameThreadForwardingListener_t563_il2cpp_TypeInfo_var);
		OnGameThreadForwardingListener__ctor_m2364(L_4, L_3, /*hidden argument*/NULL);
		__this->___mListener_1 = L_4;
		BeforeRoomCreateStartedState_t581 * L_5 = (BeforeRoomCreateStartedState_t581 *)il2cpp_codegen_object_new (BeforeRoomCreateStartedState_t581_il2cpp_TypeInfo_var);
		BeforeRoomCreateStartedState__ctor_m2405(L_5, __this, /*hidden argument*/NULL);
		RoomSession_EnterState_m2335(__this, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->___mStillPreRoomCreation_5 = 1;
		return;
	}
}
// System.UInt32 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::get_MinPlayersToStart()
extern "C" uint32_t RoomSession_get_MinPlayersToStart_m2329 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___mMinPlayersToStart_6);
		return L_0;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::set_MinPlayersToStart(System.UInt32)
extern "C" void RoomSession_set_MinPlayersToStart_m2330 (RoomSession_t566 * __this, uint32_t ___value, MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		__this->___mMinPlayersToStart_6 = L_0;
		return;
	}
}
// GooglePlayGames.Native.PInvoke.RealtimeManager GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::Manager()
extern "C" RealtimeManager_t564 * RoomSession_Manager_m2331 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		RealtimeManager_t564 * L_0 = (__this->___mManager_2);
		return L_0;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::IsActive()
extern "C" bool RoomSession_IsActive_m2332 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsActive() */, L_0);
		return L_1;
	}
}
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SelfPlayerId()
extern "C" String_t* RoomSession_SelfPlayerId_m2333 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mCurrentPlayerId_3);
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnGameThreadListener()
extern "C" OnGameThreadForwardingListener_t563 * RoomSession_OnGameThreadListener_m2334 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___mListener_1);
		return L_0;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::EnterState(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisState_t565_m3794_MethodInfo_var;
extern "C" void RoomSession_EnterState_m2335 (RoomSession_t566 * __this, State_t565 * ___handler, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		Misc_CheckNotNull_TisState_t565_m3794_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483908);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = ___handler;
		State_t565 * L_3 = Misc_CheckNotNull_TisState_t565_m3794(NULL /*static, unused*/, L_2, /*hidden argument*/Misc_CheckNotNull_TisState_t565_m3794_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		__this->___mState_4 = L_3;
		State_t565 * L_4 = ___handler;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m932(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m856(NULL /*static, unused*/, (String_t*) &_stringLiteral489, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
		Logger_d_m1591(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		State_t565 * L_8 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(8 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnStateEntered() */, L_8);
		IL2CPP_LEAVE(0x4E, FINALLY_0047);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Object_t * L_9 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::LeaveRoom()
extern "C" void RoomSession_LeaveRoom_m2336 (RoomSession_t566 * __this, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(6 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::LeaveRoom() */, L_2);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Object_t * L_3 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::ShowWaitingRoomUI()
extern "C" void RoomSession_ShowWaitingRoomUI_m2337 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		uint32_t L_1 = RoomSession_get_MinPlayersToStart_m2329(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< uint32_t >::Invoke(7 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::ShowWaitingRoomUI(System.UInt32) */, L_0, L_1);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::StartRoomCreation(System.String,System.Action)
extern TypeInfo* Logger_t390_il2cpp_TypeInfo_var;
extern TypeInfo* RoomCreationPendingState_t582_il2cpp_TypeInfo_var;
extern MethodInfo* Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var;
extern "C" void RoomSession_StartRoomCreation_m2338 (RoomSession_t566 * __this, String_t* ___currentPlayerId, Action_t588 * ___createRoom, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Logger_t390_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		RoomCreationPendingState_t582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(739);
		Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483825);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (__this->___mStillPreRoomCreation_5);
			il2cpp_codegen_memory_barrier();
			if (L_2)
			{
				goto IL_0029;
			}
		}

IL_001a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_e_m1593(NULL /*static, unused*/, (String_t*) &_stringLiteral490, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x7F, FINALLY_0078);
		}

IL_0029:
		{
			State_t565 * L_3 = (__this->___mState_4);
			il2cpp_codegen_memory_barrier();
			NullCheck(L_3);
			bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsActive() */, L_3);
			if (L_4)
			{
				goto IL_004a;
			}
		}

IL_003b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Logger_t390_il2cpp_TypeInfo_var);
			Logger_w_m1592(NULL /*static, unused*/, (String_t*) &_stringLiteral491, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x7F, FINALLY_0078);
		}

IL_004a:
		{
			String_t* L_5 = ___currentPlayerId;
			String_t* L_6 = Misc_CheckNotNull_TisString_t_m3705(NULL /*static, unused*/, L_5, /*hidden argument*/Misc_CheckNotNull_TisString_t_m3705_MethodInfo_var);
			il2cpp_codegen_memory_barrier();
			__this->___mCurrentPlayerId_3 = L_6;
			il2cpp_codegen_memory_barrier();
			__this->___mStillPreRoomCreation_5 = 0;
			RoomCreationPendingState_t582 * L_7 = (RoomCreationPendingState_t582 *)il2cpp_codegen_object_new (RoomCreationPendingState_t582_il2cpp_TypeInfo_var);
			RoomCreationPendingState__ctor_m2407(L_7, __this, /*hidden argument*/NULL);
			RoomSession_EnterState_m2335(__this, L_7, /*hidden argument*/NULL);
			Action_t588 * L_8 = ___createRoom;
			NullCheck(L_8);
			VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_8);
			IL2CPP_LEAVE(0x7F, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		Object_t * L_9 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(120)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_007f:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void RoomSession_OnRoomStatusChanged_m2339 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NativeRealTimeRoom_t574 * L_3 = ___room;
		NullCheck(L_2);
		VirtActionInvoker1< NativeRealTimeRoom_t574 * >::Invoke(9 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom) */, L_2, L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_4 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void RoomSession_OnConnectedSetChanged_m2340 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NativeRealTimeRoom_t574 * L_3 = ___room;
		NullCheck(L_2);
		VirtActionInvoker1< NativeRealTimeRoom_t574 * >::Invoke(10 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom) */, L_2, L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_4 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void RoomSession_OnParticipantStatusChanged_m2341 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NativeRealTimeRoom_t574 * L_3 = ___room;
		MultiplayerParticipant_t672 * L_4 = ___participant;
		NullCheck(L_2);
		VirtActionInvoker2< NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 * >::Invoke(11 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant) */, L_2, L_3, L_4);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		Object_t * L_5 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void RoomSession_HandleRoomResponse_m2342 (RoomSession_t566 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method)
{
	Object_t * V_0 = {0};
	Exception_t135 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t135 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___mLifecycleLock_0);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m3734(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		State_t565 * L_2 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		RealTimeRoomResponse_t695 * L_3 = ___response;
		NullCheck(L_2);
		VirtActionInvoker1< RealTimeRoomResponse_t695 * >::Invoke(4 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse) */, L_2, L_3);
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t135 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_t * L_4 = V_0;
		Monitor_Exit_m3735(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t135 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void RoomSession_OnDataReceived_m2343 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NativeRealTimeRoom_t574 * L_1 = ___room;
		MultiplayerParticipant_t672 * L_2 = ___sender;
		ByteU5BU5D_t350* L_3 = ___data;
		bool L_4 = ___isReliable;
		NullCheck(L_0);
		VirtActionInvoker4< NativeRealTimeRoom_t574 *, MultiplayerParticipant_t672 *, ByteU5BU5D_t350*, bool >::Invoke(12 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean) */, L_0, L_1, L_2, L_3, L_4);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessageToAll(System.Boolean,System.Byte[])
extern "C" void RoomSession_SendMessageToAll_m2344 (RoomSession_t566 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	{
		bool L_0 = ___reliable;
		ByteU5BU5D_t350* L_1 = ___data;
		ByteU5BU5D_t350* L_2 = ___data;
		NullCheck(L_2);
		RoomSession_SendMessageToAll_m2345(__this, L_0, L_1, 0, (((int32_t)(((Array_t *)L_2)->max_length))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessageToAll(System.Boolean,System.Byte[],System.Int32,System.Int32)
extern "C" void RoomSession_SendMessageToAll_m2345 (RoomSession_t566 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		ByteU5BU5D_t350* L_1 = ___data;
		int32_t L_2 = ___offset;
		int32_t L_3 = ___length;
		bool L_4 = ___reliable;
		NullCheck(L_0);
		VirtActionInvoker4< ByteU5BU5D_t350*, int32_t, int32_t, bool >::Invoke(14 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToAll(System.Byte[],System.Int32,System.Int32,System.Boolean) */, L_0, L_1, L_2, L_3, L_4);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessage(System.Boolean,System.String,System.Byte[])
extern "C" void RoomSession_SendMessage_m2346 (RoomSession_t566 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, MethodInfo* method)
{
	{
		bool L_0 = ___reliable;
		String_t* L_1 = ___participantId;
		ByteU5BU5D_t350* L_2 = ___data;
		ByteU5BU5D_t350* L_3 = ___data;
		NullCheck(L_3);
		RoomSession_SendMessage_m2347(__this, L_0, L_1, L_2, 0, (((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessage(System.Boolean,System.String,System.Byte[],System.Int32,System.Int32)
extern "C" void RoomSession_SendMessage_m2347 (RoomSession_t566 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = ___participantId;
		ByteU5BU5D_t350* L_2 = ___data;
		int32_t L_3 = ___offset;
		int32_t L_4 = ___length;
		bool L_5 = ___reliable;
		NullCheck(L_0);
		VirtActionInvoker5< String_t*, ByteU5BU5D_t350*, int32_t, int32_t, bool >::Invoke(13 /* System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::SendToSpecificRecipient(System.String,System.Byte[],System.Int32,System.Int32,System.Boolean) */, L_0, L_1, L_2, L_3, L_4, L_5);
		return;
	}
}
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetConnectedParticipants()
extern "C" List_1_t351 * RoomSession_GetConnectedParticipants_m2348 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		List_1_t351 * L_1 = (List_1_t351 *)VirtFuncInvoker0< List_1_t351 * >::Invoke(15 /* System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetConnectedParticipants() */, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetSelf()
extern "C" Participant_t340 * RoomSession_GetSelf_m2349 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		Participant_t340 * L_1 = (Participant_t340 *)VirtFuncInvoker0< Participant_t340 * >::Invoke(16 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetSelf() */, L_0);
		return L_1;
	}
}
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetParticipant(System.String)
extern "C" Participant_t340 * RoomSession_GetParticipant_m2350 (RoomSession_t566 * __this, String_t* ___participantId, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = ___participantId;
		NullCheck(L_0);
		Participant_t340 * L_2 = (Participant_t340 *)VirtFuncInvoker1< Participant_t340 *, String_t* >::Invoke(17 /* GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::GetParticipant(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::IsRoomConnected()
extern "C" bool RoomSession_IsRoomConnected_m2351 (RoomSession_t566 * __this, MethodInfo* method)
{
	{
		State_t565 * L_0 = (__this->___mState_4);
		il2cpp_codegen_memory_barrier();
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State::IsRoomConnected() */, L_0);
		return L_1;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_1.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_1MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::.ctor()
extern "C" void U3CRoomSetupProgressU3Ec__AnonStorey3B__ctor_m2352 (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::<>m__30()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CRoomSetupProgressU3Ec__AnonStorey3B_U3CU3Em__30_m2353 (U3CRoomSetupProgressU3Ec__AnonStorey3B_t567 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		float L_2 = (__this->___percent_0);
		NullCheck(L_1);
		InterfaceActionInvoker1< float >::Invoke(0 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnRoomSetupProgress(System.Single) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_2.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_2MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C::.ctor()
extern "C" void U3CRoomConnectedU3Ec__AnonStorey3C__ctor_m2354 (U3CRoomConnectedU3Ec__AnonStorey3C_t568 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomConnected>c__AnonStorey3C::<>m__31()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CRoomConnectedU3Ec__AnonStorey3C_U3CU3Em__31_m2355 (U3CRoomConnectedU3Ec__AnonStorey3C_t568 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		bool L_2 = (__this->___success_0);
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(1 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnRoomConnected(System.Boolean) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_3.h"
#ifndef _MSC_VER
#else
#endif
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D
#include "AssemblyU2DCSharp_GooglePlayGames_Native_NativeRealtimeMulti_3MethodDeclarations.h"



// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::.ctor()
extern "C" void U3CPeersConnectedU3Ec__AnonStorey3D__ctor_m2356 (U3CPeersConnectedU3Ec__AnonStorey3D_t569 * __this, MethodInfo* method)
{
	{
		Object__ctor_m830(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersConnected>c__AnonStorey3D::<>m__33()
extern TypeInfo* RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var;
extern "C" void U3CPeersConnectedU3Ec__AnonStorey3D_U3CU3Em__33_m2357 (U3CPeersConnectedU3Ec__AnonStorey3D_t569 * __this, MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGameThreadForwardingListener_t563 * L_0 = (__this->___U3CU3Ef__this_1);
		NullCheck(L_0);
		Object_t * L_1 = (L_0->___mListener_0);
		StringU5BU5D_t169* L_2 = (__this->___participantIds_0);
		NullCheck(L_1);
		InterfaceActionInvoker1< StringU5BU5D_t169* >::Invoke(4 /* System.Void GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener::OnPeersConnected(System.String[]) */, RealTimeMultiplayerListener_t573_il2cpp_TypeInfo_var, L_1, L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
