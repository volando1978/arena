﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Advertisements.UnityAdsExternal
struct UnityAdsExternal_t157;
// UnityEngine.Advertisements.UnityAdsPlatform
struct UnityAdsPlatform_t156;
// System.String
struct String_t;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"

// System.Void UnityEngine.Advertisements.UnityAdsExternal::.cctor()
extern "C" void UnityAdsExternal__cctor_m771 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Advertisements.UnityAdsPlatform UnityEngine.Advertisements.UnityAdsExternal::getImpl()
extern "C" UnityAdsPlatform_t156 * UnityAdsExternal_getImpl_m772 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsExternal::init(System.String,System.Boolean,System.String,System.String)
extern "C" void UnityAdsExternal_init_m773 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, String_t* ___gameObjectName, String_t* ___unityVersion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::show(System.String,System.String,System.String)
extern "C" bool UnityAdsExternal_show_m774 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsExternal::hide()
extern "C" void UnityAdsExternal_hide_m775 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::isSupported()
extern "C" bool UnityAdsExternal_isSupported_m776 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getSDKVersion()
extern "C" String_t* UnityAdsExternal_getSDKVersion_m777 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::canShowZone(System.String)
extern "C" bool UnityAdsExternal_canShowZone_m778 (Object_t * __this /* static, unused */, String_t* ___zone, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::hasMultipleRewardItems()
extern "C" bool UnityAdsExternal_hasMultipleRewardItems_m779 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemKeys()
extern "C" String_t* UnityAdsExternal_getRewardItemKeys_m780 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getDefaultRewardItemKey()
extern "C" String_t* UnityAdsExternal_getDefaultRewardItemKey_m781 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getCurrentRewardItemKey()
extern "C" String_t* UnityAdsExternal_getCurrentRewardItemKey_m782 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsExternal::setRewardItemKey(System.String)
extern "C" bool UnityAdsExternal_setRewardItemKey_m783 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setDefaultRewardItemAsRewardItem()
extern "C" void UnityAdsExternal_setDefaultRewardItemAsRewardItem_m784 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsWithKey(System.String)
extern "C" String_t* UnityAdsExternal_getRewardItemDetailsWithKey_m785 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.UnityAdsExternal::getRewardItemDetailsKeys()
extern "C" String_t* UnityAdsExternal_getRewardItemDetailsKeys_m786 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsExternal::setLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C" void UnityAdsExternal_setLogLevel_m787 (Object_t * __this /* static, unused */, int32_t ___logLevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
