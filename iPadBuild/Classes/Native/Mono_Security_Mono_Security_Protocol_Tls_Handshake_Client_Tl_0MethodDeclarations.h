﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
struct TlsClientCertificateVerify_t2286;
// Mono.Security.Protocol.Tls.Context
struct Context_t2240;
// System.Security.Cryptography.RSA
struct RSA_t2130;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::.ctor(Mono.Security.Protocol.Tls.Context)
extern "C" void TlsClientCertificateVerify__ctor_m9742 (TlsClientCertificateVerify_t2286 * __this, Context_t2240 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::Update()
extern "C" void TlsClientCertificateVerify_Update_m9743 (TlsClientCertificateVerify_t2286 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsSsl3()
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m9744 (TlsClientCertificateVerify_t2286 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsTls1()
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m9745 (TlsClientCertificateVerify_t2286 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getClientCertRSA(System.Security.Cryptography.RSA)
extern "C" RSA_t2130 * TlsClientCertificateVerify_getClientCertRSA_m9746 (TlsClientCertificateVerify_t2286 * __this, RSA_t2130 * ___privKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t350* TlsClientCertificateVerify_getUnsignedBigInteger_m9747 (TlsClientCertificateVerify_t2286 * __this, ByteU5BU5D_t350* ___integer, MethodInfo* method) IL2CPP_METHOD_ATTR;
