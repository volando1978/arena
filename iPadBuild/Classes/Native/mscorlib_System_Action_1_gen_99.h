﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1650;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Events.PersistentCall>
struct  Action_1_t4156  : public MulticastDelegate_t22
{
};
