﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameCenterSingleton
struct GameCenterSingleton_t730;
// System.String
struct String_t;
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t732;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t856;

// System.Void GameCenterSingleton::.ctor()
extern "C" void GameCenterSingleton__ctor_m3161 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameCenterSingleton GameCenterSingleton::get_Instance()
extern "C" GameCenterSingleton_t730 * GameCenterSingleton_get_Instance_m3162 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::Initialize()
extern "C" void GameCenterSingleton_Initialize_m3163 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterSingleton::IsUserAuthenticated()
extern "C" bool GameCenterSingleton_IsUserAuthenticated_m3164 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ShowAchievementUI()
extern "C" void GameCenterSingleton_ShowAchievementUI_m3165 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ShowLeaderboardUI()
extern "C" void GameCenterSingleton_ShowLeaderboardUI_m3166 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ReportScore(System.Int64,System.String)
extern "C" void GameCenterSingleton_ReportScore_m3167 (GameCenterSingleton_t730 * __this, int64_t ___score, String_t* ___leaderBoardID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterSingleton::AddAchievementProgress(System.String,System.Single)
extern "C" bool GameCenterSingleton_AddAchievementProgress_m3168 (GameCenterSingleton_t730 * __this, String_t* ___achievementID, float ___percentageToAdd, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterSingleton::ReportAchievementProgress(System.String,System.Single)
extern "C" bool GameCenterSingleton_ReportAchievementProgress_m3169 (GameCenterSingleton_t730 * __this, String_t* ___achievementID, float ___progressCompleted, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ResetAchievements()
extern "C" void GameCenterSingleton_ResetAchievements_m3170 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::LoadAchievements()
extern "C" void GameCenterSingleton_LoadAchievements_m3171 (GameCenterSingleton_t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ProcessAuthentication(System.Boolean)
extern "C" void GameCenterSingleton_ProcessAuthentication_m3172 (GameCenterSingleton_t730 * __this, bool ___success, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ProcessLoadedAchievements(UnityEngine.SocialPlatforms.IAchievement[])
extern "C" void GameCenterSingleton_ProcessLoadedAchievements_m3173 (GameCenterSingleton_t730 * __this, IAchievementU5BU5D_t732* ___achievements, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameCenterSingleton::IsAchievementComplete(System.String)
extern "C" bool GameCenterSingleton_IsAchievementComplete_m3174 (GameCenterSingleton_t730 * __this, String_t* ___achievementID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.IAchievement GameCenterSingleton::GetAchievement(System.String)
extern "C" Object_t * GameCenterSingleton_GetAchievement_m3175 (GameCenterSingleton_t730 * __this, String_t* ___achievementID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::ResetAchievementsHandler(System.Boolean)
extern "C" void GameCenterSingleton_ResetAchievementsHandler_m3176 (GameCenterSingleton_t730 * __this, bool ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameCenterSingleton::<ReportScore>m__A3(System.Boolean)
extern "C" void GameCenterSingleton_U3CReportScoreU3Em__A3_m3177 (Object_t * __this /* static, unused */, bool ___success, MethodInfo* method) IL2CPP_METHOD_ATTR;
