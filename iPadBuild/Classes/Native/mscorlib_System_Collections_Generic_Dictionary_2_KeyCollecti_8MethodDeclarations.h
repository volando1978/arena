﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
struct Enumerator_t3534;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3530;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17119_gshared (Enumerator_t3534 * __this, Dictionary_2_t3530 * ___host, MethodInfo* method);
#define Enumerator__ctor_m17119(__this, ___host, method) (( void (*) (Enumerator_t3534 *, Dictionary_2_t3530 *, MethodInfo*))Enumerator__ctor_m17119_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17120_gshared (Enumerator_t3534 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17120(__this, method) (( Object_t * (*) (Enumerator_t3534 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17120_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m17121_gshared (Enumerator_t3534 * __this, MethodInfo* method);
#define Enumerator_Dispose_m17121(__this, method) (( void (*) (Enumerator_t3534 *, MethodInfo*))Enumerator_Dispose_m17121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17122_gshared (Enumerator_t3534 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m17122(__this, method) (( bool (*) (Enumerator_t3534 *, MethodInfo*))Enumerator_MoveNext_m17122_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m17123_gshared (Enumerator_t3534 * __this, MethodInfo* method);
#define Enumerator_get_Current_m17123(__this, method) (( Object_t * (*) (Enumerator_t3534 *, MethodInfo*))Enumerator_get_Current_m17123_gshared)(__this, method)
