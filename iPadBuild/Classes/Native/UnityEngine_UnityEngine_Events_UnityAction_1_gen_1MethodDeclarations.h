﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t1359;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction_1__ctor_m5888_gshared (UnityAction_1_t1359 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define UnityAction_1__ctor_m5888(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t1359 *, Object_t *, IntPtr_t, MethodInfo*))UnityAction_1__ctor_m5888_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityAction_1_Invoke_m22547_gshared (UnityAction_1_t1359 * __this, Color_t747  ___arg0, MethodInfo* method);
#define UnityAction_1_Invoke_m22547(__this, ___arg0, method) (( void (*) (UnityAction_1_t1359 *, Color_t747 , MethodInfo*))UnityAction_1_Invoke_m22547_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAction_1_BeginInvoke_m22548_gshared (UnityAction_1_t1359 * __this, Color_t747  ___arg0, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define UnityAction_1_BeginInvoke_m22548(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t1359 *, Color_t747 , AsyncCallback_t20 *, Object_t *, MethodInfo*))UnityAction_1_BeginInvoke_m22548_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_1_EndInvoke_m22549_gshared (UnityAction_1_t1359 * __this, Object_t * ___result, MethodInfo* method);
#define UnityAction_1_EndInvoke_m22549(__this, ___result, method) (( void (*) (UnityAction_1_t1359 *, Object_t *, MethodInfo*))UnityAction_1_EndInvoke_m22549_gshared)(__this, ___result, method)
