﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>
struct Dictionary_2_t111;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.PurchasableVirtualItem>
struct KeyCollection_t3608;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.PurchasableVirtualItem>
struct ValueCollection_t3609;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>[]
struct KeyValuePair_2U5BU5D_t4329;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>>
struct IEnumerator_1_t4330;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.PurchasableVirtualItem>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.PurchasableVirtualItem>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m1006(__this, method) (( void (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18043(__this, ___comparer, method) (( void (*) (Dictionary_2_t111 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(System.Int32)
#define Dictionary_2__ctor_m18044(__this, ___capacity, method) (( void (*) (Dictionary_2_t111 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m18045(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t111 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18046(__this, method) (( Object_t * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18047(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t111 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18048(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t111 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m18049(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t111 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m18050(__this, ___key, method) (( void (*) (Dictionary_2_t111 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18051(__this, method) (( bool (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18052(__this, method) (( Object_t * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18053(__this, method) (( bool (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18054(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t111 *, KeyValuePair_2_t3607 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18055(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t111 *, KeyValuePair_2_t3607 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18056(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t111 *, KeyValuePair_2U5BU5D_t4329*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18057(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t111 *, KeyValuePair_2_t3607 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18058(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t111 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18059(__this, method) (( Object_t * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18060(__this, method) (( Object_t* (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18061(__this, method) (( Object_t * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Count()
#define Dictionary_2_get_Count_m18062(__this, method) (( int32_t (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Item(TKey)
#define Dictionary_2_get_Item_m18063(__this, ___key, method) (( PurchasableVirtualItem_t124 * (*) (Dictionary_2_t111 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m18064(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t111 *, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m18065(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t111 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m18066(__this, ___size, method) (( void (*) (Dictionary_2_t111 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m18067(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t111 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m18068(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3607  (*) (Object_t * /* static, unused */, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m18069(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m18070(__this /* static, unused */, ___key, ___value, method) (( PurchasableVirtualItem_t124 * (*) (Object_t * /* static, unused */, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m18071(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t111 *, KeyValuePair_2U5BU5D_t4329*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::Resize()
#define Dictionary_2_Resize_m18072(__this, method) (( void (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::Add(TKey,TValue)
#define Dictionary_2_Add_m18073(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t111 *, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::Clear()
#define Dictionary_2_Clear_m18074(__this, method) (( void (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m18075(__this, ___key, method) (( bool (*) (Dictionary_2_t111 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m18076(__this, ___value, method) (( bool (*) (Dictionary_2_t111 *, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m18077(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t111 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m18078(__this, ___sender, method) (( void (*) (Dictionary_2_t111 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::Remove(TKey)
#define Dictionary_2_Remove_m18079(__this, ___key, method) (( bool (*) (Dictionary_2_t111 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m18080(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t111 *, String_t*, PurchasableVirtualItem_t124 **, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Keys()
#define Dictionary_2_get_Keys_m18081(__this, method) (( KeyCollection_t3608 * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::get_Values()
#define Dictionary_2_get_Values_m18082(__this, method) (( ValueCollection_t3609 * (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m18083(__this, ___key, method) (( String_t* (*) (Dictionary_2_t111 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m18084(__this, ___value, method) (( PurchasableVirtualItem_t124 * (*) (Dictionary_2_t111 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m18085(__this, ___pair, method) (( bool (*) (Dictionary_2_t111 *, KeyValuePair_2_t3607 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m18086(__this, method) (( Enumerator_t3610  (*) (Dictionary_2_t111 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.PurchasableVirtualItem>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m18087(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, PurchasableVirtualItem_t124 *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
