﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
struct Action_2_t632;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D
struct  U3CCreateWithInvitationScreenU3Ec__AnonStorey4D_t633  : public Object_t
{
	// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::callback
	Action_2_t632 * ___callback_0;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey4D::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_1;
};
