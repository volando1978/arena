﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>
struct Collection_1_t3835;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t3830;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Matrix4x4>
struct IEnumerator_1_t4401;
// System.Collections.Generic.IList`1<UnityEngine.Matrix4x4>
struct IList_1_t3833;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::.ctor()
extern "C" void Collection_1__ctor_m21122_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1__ctor_m21122(__this, method) (( void (*) (Collection_1_t3835 *, MethodInfo*))Collection_1__ctor_m21122_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21123_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21123(__this, method) (( bool (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21123_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m21124_gshared (Collection_1_t3835 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m21124(__this, ___array, ___index, method) (( void (*) (Collection_1_t3835 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m21124_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m21125_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m21125(__this, method) (( Object_t * (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m21125_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m21126_gshared (Collection_1_t3835 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m21126(__this, ___value, method) (( int32_t (*) (Collection_1_t3835 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m21126_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m21127_gshared (Collection_1_t3835 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m21127(__this, ___value, method) (( bool (*) (Collection_1_t3835 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m21127_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m21128_gshared (Collection_1_t3835 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m21128(__this, ___value, method) (( int32_t (*) (Collection_1_t3835 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m21128_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m21129_gshared (Collection_1_t3835 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m21129(__this, ___index, ___value, method) (( void (*) (Collection_1_t3835 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m21129_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m21130_gshared (Collection_1_t3835 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m21130(__this, ___value, method) (( void (*) (Collection_1_t3835 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m21130_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m21131_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m21131(__this, method) (( bool (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m21131_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m21132_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m21132(__this, method) (( Object_t * (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m21132_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m21133_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m21133(__this, method) (( bool (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m21133_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m21134_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m21134(__this, method) (( bool (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m21134_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m21135_gshared (Collection_1_t3835 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m21135(__this, ___index, method) (( Object_t * (*) (Collection_1_t3835 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m21135_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m21136_gshared (Collection_1_t3835 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m21136(__this, ___index, ___value, method) (( void (*) (Collection_1_t3835 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m21136_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::Add(T)
extern "C" void Collection_1_Add_m21137_gshared (Collection_1_t3835 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_Add_m21137(__this, ___item, method) (( void (*) (Collection_1_t3835 *, Matrix4x4_t997 , MethodInfo*))Collection_1_Add_m21137_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::Clear()
extern "C" void Collection_1_Clear_m21138_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_Clear_m21138(__this, method) (( void (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_Clear_m21138_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::ClearItems()
extern "C" void Collection_1_ClearItems_m21139_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m21139(__this, method) (( void (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_ClearItems_m21139_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::Contains(T)
extern "C" bool Collection_1_Contains_m21140_gshared (Collection_1_t3835 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_Contains_m21140(__this, ___item, method) (( bool (*) (Collection_1_t3835 *, Matrix4x4_t997 , MethodInfo*))Collection_1_Contains_m21140_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m21141_gshared (Collection_1_t3835 * __this, Matrix4x4U5BU5D_t3830* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m21141(__this, ___array, ___index, method) (( void (*) (Collection_1_t3835 *, Matrix4x4U5BU5D_t3830*, int32_t, MethodInfo*))Collection_1_CopyTo_m21141_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m21142_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m21142(__this, method) (( Object_t* (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_GetEnumerator_m21142_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m21143_gshared (Collection_1_t3835 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m21143(__this, ___item, method) (( int32_t (*) (Collection_1_t3835 *, Matrix4x4_t997 , MethodInfo*))Collection_1_IndexOf_m21143_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m21144_gshared (Collection_1_t3835 * __this, int32_t ___index, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_Insert_m21144(__this, ___index, ___item, method) (( void (*) (Collection_1_t3835 *, int32_t, Matrix4x4_t997 , MethodInfo*))Collection_1_Insert_m21144_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m21145_gshared (Collection_1_t3835 * __this, int32_t ___index, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m21145(__this, ___index, ___item, method) (( void (*) (Collection_1_t3835 *, int32_t, Matrix4x4_t997 , MethodInfo*))Collection_1_InsertItem_m21145_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::Remove(T)
extern "C" bool Collection_1_Remove_m21146_gshared (Collection_1_t3835 * __this, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_Remove_m21146(__this, ___item, method) (( bool (*) (Collection_1_t3835 *, Matrix4x4_t997 , MethodInfo*))Collection_1_Remove_m21146_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m21147_gshared (Collection_1_t3835 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m21147(__this, ___index, method) (( void (*) (Collection_1_t3835 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m21147_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m21148_gshared (Collection_1_t3835 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m21148(__this, ___index, method) (( void (*) (Collection_1_t3835 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m21148_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m21149_gshared (Collection_1_t3835 * __this, MethodInfo* method);
#define Collection_1_get_Count_m21149(__this, method) (( int32_t (*) (Collection_1_t3835 *, MethodInfo*))Collection_1_get_Count_m21149_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::get_Item(System.Int32)
extern "C" Matrix4x4_t997  Collection_1_get_Item_m21150_gshared (Collection_1_t3835 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m21150(__this, ___index, method) (( Matrix4x4_t997  (*) (Collection_1_t3835 *, int32_t, MethodInfo*))Collection_1_get_Item_m21150_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m21151_gshared (Collection_1_t3835 * __this, int32_t ___index, Matrix4x4_t997  ___value, MethodInfo* method);
#define Collection_1_set_Item_m21151(__this, ___index, ___value, method) (( void (*) (Collection_1_t3835 *, int32_t, Matrix4x4_t997 , MethodInfo*))Collection_1_set_Item_m21151_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m21152_gshared (Collection_1_t3835 * __this, int32_t ___index, Matrix4x4_t997  ___item, MethodInfo* method);
#define Collection_1_SetItem_m21152(__this, ___index, ___item, method) (( void (*) (Collection_1_t3835 *, int32_t, Matrix4x4_t997 , MethodInfo*))Collection_1_SetItem_m21152_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m21153_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m21153(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m21153_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::ConvertItem(System.Object)
extern "C" Matrix4x4_t997  Collection_1_ConvertItem_m21154_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m21154(__this /* static, unused */, ___item, method) (( Matrix4x4_t997  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m21154_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m21155_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m21155(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m21155_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m21156_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m21156(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m21156_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Matrix4x4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m21157_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m21157(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m21157_gshared)(__this /* static, unused */, ___list, method)
