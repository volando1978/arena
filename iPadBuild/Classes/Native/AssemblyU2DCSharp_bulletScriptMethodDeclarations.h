﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// bulletScript
struct bulletScript_t767;
// UnityEngine.Collider
struct Collider_t900;

// System.Void bulletScript::.ctor()
extern "C" void bulletScript__ctor_m3316 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::OnExplosion()
extern "C" void bulletScript_OnExplosion_m3317 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::Start()
extern "C" void bulletScript_Start_m3318 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::OnTriggerEnter(UnityEngine.Collider)
extern "C" void bulletScript_OnTriggerEnter_m3319 (bulletScript_t767 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::Update()
extern "C" void bulletScript_Update_m3320 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::leavePow()
extern "C" void bulletScript_leavePow_m3321 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScript::setBulletType()
extern "C" void bulletScript_setBulletType_m3322 (bulletScript_t767 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
