﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void MatchDelegate__ctor_m3676 (MatchDelegate_t364 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::Invoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean)
extern "C" void MatchDelegate_Invoke_m3677 (MatchDelegate_t364 * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_MatchDelegate_t364(Il2CppObject* delegate, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch);
// System.IAsyncResult GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::BeginInvoke(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean,System.AsyncCallback,System.Object)
extern "C" Object_t * MatchDelegate_BeginInvoke_m3678 (MatchDelegate_t364 * __this, TurnBasedMatch_t353 * ___match, bool ___shouldAutoLaunch, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.Multiplayer.MatchDelegate::EndInvoke(System.IAsyncResult)
extern "C" void MatchDelegate_EndInvoke_m3679 (MatchDelegate_t364 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
