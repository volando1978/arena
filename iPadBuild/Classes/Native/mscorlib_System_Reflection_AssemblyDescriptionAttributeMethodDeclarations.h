﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_t1419;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
extern "C" void AssemblyDescriptionAttribute__ctor_m6175 (AssemblyDescriptionAttribute_t1419 * __this, String_t* ___description, MethodInfo* method) IL2CPP_METHOD_ATTR;
