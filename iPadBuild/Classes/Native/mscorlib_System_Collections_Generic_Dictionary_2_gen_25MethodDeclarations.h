﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt32>
struct KeyCollection_t3659;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt32>
struct ValueCollection_t3663;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3482;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>[]
struct KeyValuePair_2U5BU5D_t4357;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>
struct IEnumerator_1_t4358;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::.ctor()
extern "C" void Dictionary_2__ctor_m18861_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2__ctor_m18861(__this, method) (( void (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2__ctor_m18861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m18863_gshared (Dictionary_2_t3655 * __this, Object_t* ___comparer, MethodInfo* method);
#define Dictionary_2__ctor_m18863(__this, ___comparer, method) (( void (*) (Dictionary_2_t3655 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m18863_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m18865_gshared (Dictionary_2_t3655 * __this, int32_t ___capacity, MethodInfo* method);
#define Dictionary_2__ctor_m18865(__this, ___capacity, method) (( void (*) (Dictionary_2_t3655 *, int32_t, MethodInfo*))Dictionary_2__ctor_m18865_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m18867_gshared (Dictionary_2_t3655 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2__ctor_m18867(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3655 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m18867_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m18869_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18869(__this, method) (( Object_t * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m18869_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m18871_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18871(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m18871_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18873_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18873(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m18873_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18875_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m18875(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m18875_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18877_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m18877(__this, ___key, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m18877_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879(__this, method) (( bool (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18879_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881(__this, method) (( Object_t * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18881_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883(__this, method) (( bool (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18883_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2_t3656  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3655 *, KeyValuePair_2_t3656 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18885_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2_t3656  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3655 *, KeyValuePair_2_t3656 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18887_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2U5BU5D_t4357* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3655 *, KeyValuePair_2U5BU5D_t4357*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18889_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2_t3656  ___keyValuePair, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3655 *, KeyValuePair_2_t3656 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18891_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18893_gshared (Dictionary_2_t3655 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18893(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3655 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m18893_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895(__this, method) (( Object_t * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18895_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897(__this, method) (( Object_t* (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18897_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899(__this, method) (( Object_t * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18899_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m18901_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_get_Count_m18901(__this, method) (( int32_t (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_get_Count_m18901_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::get_Item(TKey)
extern "C" uint32_t Dictionary_2_get_Item_m18903_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_get_Item_m18903(__this, ___key, method) (( uint32_t (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_get_Item_m18903_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m18905_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_set_Item_m18905(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, uint32_t, MethodInfo*))Dictionary_2_set_Item_m18905_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m18907_gshared (Dictionary_2_t3655 * __this, int32_t ___capacity, Object_t* ___hcp, MethodInfo* method);
#define Dictionary_2_Init_m18907(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3655 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m18907_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m18909_gshared (Dictionary_2_t3655 * __this, int32_t ___size, MethodInfo* method);
#define Dictionary_2_InitArrays_m18909(__this, ___size, method) (( void (*) (Dictionary_2_t3655 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m18909_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m18911_gshared (Dictionary_2_t3655 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyToCheck_m18911(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3655 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m18911_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3656  Dictionary_2_make_pair_m18913_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_make_pair_m18913(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3656  (*) (Object_t * /* static, unused */, Object_t *, uint32_t, MethodInfo*))Dictionary_2_make_pair_m18913_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m18915_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_pick_key_m18915(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint32_t, MethodInfo*))Dictionary_2_pick_key_m18915_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::pick_value(TKey,TValue)
extern "C" uint32_t Dictionary_2_pick_value_m18917_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_pick_value_m18917(__this /* static, unused */, ___key, ___value, method) (( uint32_t (*) (Object_t * /* static, unused */, Object_t *, uint32_t, MethodInfo*))Dictionary_2_pick_value_m18917_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m18919_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2U5BU5D_t4357* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyTo_m18919(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3655 *, KeyValuePair_2U5BU5D_t4357*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m18919_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::Resize()
extern "C" void Dictionary_2_Resize_m18921_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_Resize_m18921(__this, method) (( void (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_Resize_m18921_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m18923_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_Add_m18923(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, uint32_t, MethodInfo*))Dictionary_2_Add_m18923_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::Clear()
extern "C" void Dictionary_2_Clear_m18925_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_Clear_m18925(__this, method) (( void (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_Clear_m18925_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m18927_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ContainsKey_m18927(__this, ___key, method) (( bool (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_ContainsKey_m18927_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m18929_gshared (Dictionary_2_t3655 * __this, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_ContainsValue_m18929(__this, ___value, method) (( bool (*) (Dictionary_2_t3655 *, uint32_t, MethodInfo*))Dictionary_2_ContainsValue_m18929_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m18931_gshared (Dictionary_2_t3655 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define Dictionary_2_GetObjectData_m18931(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3655 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m18931_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m18933_gshared (Dictionary_2_t3655 * __this, Object_t * ___sender, MethodInfo* method);
#define Dictionary_2_OnDeserialization_m18933(__this, ___sender, method) (( void (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m18933_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m18935_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_Remove_m18935(__this, ___key, method) (( bool (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_Remove_m18935_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m18937_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, uint32_t* ___value, MethodInfo* method);
#define Dictionary_2_TryGetValue_m18937(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3655 *, Object_t *, uint32_t*, MethodInfo*))Dictionary_2_TryGetValue_m18937_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::get_Keys()
extern "C" KeyCollection_t3659 * Dictionary_2_get_Keys_m18939_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_get_Keys_m18939(__this, method) (( KeyCollection_t3659 * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_get_Keys_m18939_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::get_Values()
extern "C" ValueCollection_t3663 * Dictionary_2_get_Values_m18941_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_get_Values_m18941(__this, method) (( ValueCollection_t3663 * (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_get_Values_m18941_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m18943_gshared (Dictionary_2_t3655 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ToTKey_m18943(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m18943_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::ToTValue(System.Object)
extern "C" uint32_t Dictionary_2_ToTValue_m18945_gshared (Dictionary_2_t3655 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ToTValue_m18945(__this, ___value, method) (( uint32_t (*) (Dictionary_2_t3655 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m18945_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m18947_gshared (Dictionary_2_t3655 * __this, KeyValuePair_2_t3656  ___pair, MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m18947(__this, ___pair, method) (( bool (*) (Dictionary_2_t3655 *, KeyValuePair_2_t3656 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m18947_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::GetEnumerator()
extern "C" Enumerator_t3661  Dictionary_2_GetEnumerator_m18949_gshared (Dictionary_2_t3655 * __this, MethodInfo* method);
#define Dictionary_2_GetEnumerator_m18949(__this, method) (( Enumerator_t3661  (*) (Dictionary_2_t3655 *, MethodInfo*))Dictionary_2_GetEnumerator_m18949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2128  Dictionary_2_U3CCopyToU3Em__0_m18951_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint32_t ___value, MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m18951(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, Object_t *, uint32_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m18951_gshared)(__this /* static, unused */, ___key, ___value, method)
