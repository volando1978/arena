﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.PlayerManager
struct PlayerManager_t442;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback
struct FetchListCallback_t441;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback
struct FetchCallback_t440;
// GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback
struct FetchSelfCallback_t439;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchInvitable(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" void PlayerManager_PlayerManager_FetchInvitable_m1841 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchConnected(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" void PlayerManager_PlayerManager_FetchConnected_m1842 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchCallback,System.IntPtr)
extern "C" void PlayerManager_PlayerManager_Fetch_m1843 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___player_id, FetchCallback_t440 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchRecentlyPlayed(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchListCallback,System.IntPtr)
extern "C" void PlayerManager_PlayerManager_FetchRecentlyPlayed_m1844 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchListCallback_t441 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelf(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.PlayerManager/FetchSelfCallback,System.IntPtr)
extern "C" void PlayerManager_PlayerManager_FetchSelf_m1845 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchSelfCallback_t439 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void PlayerManager_PlayerManager_FetchSelfResponse_Dispose_m1846 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t PlayerManager_PlayerManager_FetchSelfResponse_GetStatus_m1847 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchSelfResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchSelfResponse_GetData_m1848 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void PlayerManager_PlayerManager_FetchResponse_Dispose_m1849 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t PlayerManager_PlayerManager_FetchResponse_GetStatus_m1850 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchResponse_GetData_m1851 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void PlayerManager_PlayerManager_FetchListResponse_Dispose_m1852 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t PlayerManager_PlayerManager_FetchListResponse_GetStatus_m1853 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  PlayerManager_PlayerManager_FetchListResponse_GetData_Length_m1854 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.PlayerManager::PlayerManager_FetchListResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t PlayerManager_PlayerManager_FetchListResponse_GetData_GetElement_m1855 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
