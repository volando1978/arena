﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// backgroundDetails
struct  backgroundDetails_t763  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject backgroundDetails::dust
	GameObject_t144 * ___dust_2;
	// System.Int32 backgroundDetails::t
	int32_t ___t_3;
	// System.Int32 backgroundDetails::t2
	int32_t ___t2_4;
};
