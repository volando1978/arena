﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback
struct TurnBasedMatchCallback_t709;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TurnBasedMatchCallback__ctor_m3076 (TurnBasedMatchCallback_t709 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::Invoke(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void TurnBasedMatchCallback_Invoke_m3077 (TurnBasedMatchCallback_t709 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_TurnBasedMatchCallback_t709(Il2CppObject* delegate, TurnBasedMatchResponse_t707 * ___response);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::BeginInvoke(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse,System.AsyncCallback,System.Object)
extern "C" Object_t * TurnBasedMatchCallback_BeginInvoke_m3078 (TurnBasedMatchCallback_t709 * __this, TurnBasedMatchResponse_t707 * ___response, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchCallback::EndInvoke(System.IAsyncResult)
extern "C" void TurnBasedMatchCallback_EndInvoke_m3079 (TurnBasedMatchCallback_t709 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
