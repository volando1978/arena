﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t3859;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m21430_gshared (DefaultComparer_t3859 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21430(__this, method) (( void (*) (DefaultComparer_t3859 *, MethodInfo*))DefaultComparer__ctor_m21430_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector3>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m21431_gshared (DefaultComparer_t3859 * __this, Vector3_t758  ___x, Vector3_t758  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m21431(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3859 *, Vector3_t758 , Vector3_t758 , MethodInfo*))DefaultComparer_Compare_m21431_gshared)(__this, ___x, ___y, method)
