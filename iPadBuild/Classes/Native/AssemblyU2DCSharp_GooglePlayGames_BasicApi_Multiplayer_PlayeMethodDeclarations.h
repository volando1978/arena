﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t347;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.BasicApi.Multiplayer.Player::.ctor(System.String,System.String,System.String)
extern "C" void Player__ctor_m1396 (Player_t347 * __this, String_t* ___displayName, String_t* ___playerId, String_t* ___avatarUrl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_DisplayName()
extern "C" String_t* Player_get_DisplayName_m1397 (Player_t347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_PlayerId()
extern "C" String_t* Player_get_PlayerId_m1398 (Player_t347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::get_AvatarURL()
extern "C" String_t* Player_get_AvatarURL_m1399 (Player_t347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.BasicApi.Multiplayer.Player::ToString()
extern "C" String_t* Player_ToString_m1400 (Player_t347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.Multiplayer.Player::Equals(System.Object)
extern "C" bool Player_Equals_m1401 (Player_t347 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GooglePlayGames.BasicApi.Multiplayer.Player::GetHashCode()
extern "C" int32_t Player_GetHashCode_m1402 (Player_t347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
