﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Byte>
struct UnityAction_1_t3818;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Byte>
struct  InvokableCall_1_t3821  : public BaseInvokableCall_t1647
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Byte>::Delegate
	UnityAction_1_t3818 * ___Delegate_0;
};
