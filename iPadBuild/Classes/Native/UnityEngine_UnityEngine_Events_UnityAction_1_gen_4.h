﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t230;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct  UnityAction_1_t1323  : public MulticastDelegate_t22
{
};
