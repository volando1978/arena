﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t1524;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1523;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutEntry>
struct IEnumerable_1_t4500;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutEntry>
struct IEnumerator_1_t4501;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutEntry>
struct ICollection_1_t4502;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>
struct ReadOnlyCollection_1_t4073;
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t4071;
// System.Predicate`1<UnityEngine.GUILayoutEntry>
struct Predicate_1_t4074;
// System.Action`1<UnityEngine.GUILayoutEntry>
struct Action_1_t4075;
// System.Comparison`1<UnityEngine.GUILayoutEntry>
struct Comparison_1_t4076;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m7488(__this, method) (( void (*) (List_1_t1524 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24328(__this, ___collection, method) (( void (*) (List_1_t1524 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor(System.Int32)
#define List_1__ctor_m24329(__this, ___capacity, method) (( void (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.cctor()
#define List_1__cctor_m24330(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24331(__this, method) (( Object_t* (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24332(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1524 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24333(__this, method) (( Object_t * (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24334(__this, ___item, method) (( int32_t (*) (List_1_t1524 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24335(__this, ___item, method) (( bool (*) (List_1_t1524 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24336(__this, ___item, method) (( int32_t (*) (List_1_t1524 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24337(__this, ___index, ___item, method) (( void (*) (List_1_t1524 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24338(__this, ___item, method) (( void (*) (List_1_t1524 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24339(__this, method) (( bool (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24340(__this, method) (( bool (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24341(__this, method) (( Object_t * (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24342(__this, method) (( bool (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24343(__this, method) (( bool (*) (List_1_t1524 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24344(__this, ___index, method) (( Object_t * (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24345(__this, ___index, ___value, method) (( void (*) (List_1_t1524 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(T)
#define List_1_Add_m24346(__this, ___item, method) (( void (*) (List_1_t1524 *, GUILayoutEntry_t1523 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24347(__this, ___newCount, method) (( void (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24348(__this, ___collection, method) (( void (*) (List_1_t1524 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24349(__this, ___enumerable, method) (( void (*) (List_1_t1524 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m24350(__this, ___collection, method) (( void (*) (List_1_t1524 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AsReadOnly()
#define List_1_AsReadOnly_m24351(__this, method) (( ReadOnlyCollection_1_t4073 * (*) (List_1_t1524 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Clear()
#define List_1_Clear_m24352(__this, method) (( void (*) (List_1_t1524 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Contains(T)
#define List_1_Contains_m24353(__this, ___item, method) (( bool (*) (List_1_t1524 *, GUILayoutEntry_t1523 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24354(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1524 *, GUILayoutEntryU5BU5D_t4071*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Find(System.Predicate`1<T>)
#define List_1_Find_m24355(__this, ___match, method) (( GUILayoutEntry_t1523 * (*) (List_1_t1524 *, Predicate_1_t4074 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24356(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4074 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m24357(__this, ___match, method) (( int32_t (*) (List_1_t1524 *, Predicate_1_t4074 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24358(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1524 *, int32_t, int32_t, Predicate_1_t4074 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m24359(__this, ___action, method) (( void (*) (List_1_t1524 *, Action_1_t4075 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GetEnumerator()
#define List_1_GetEnumerator_m7485(__this, method) (( Enumerator_t1679  (*) (List_1_t1524 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::IndexOf(T)
#define List_1_IndexOf_m24360(__this, ___item, method) (( int32_t (*) (List_1_t1524 *, GUILayoutEntry_t1523 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24361(__this, ___start, ___delta, method) (( void (*) (List_1_t1524 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24362(__this, ___index, method) (( void (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Insert(System.Int32,T)
#define List_1_Insert_m24363(__this, ___index, ___item, method) (( void (*) (List_1_t1524 *, int32_t, GUILayoutEntry_t1523 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24364(__this, ___collection, method) (( void (*) (List_1_t1524 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Remove(T)
#define List_1_Remove_m24365(__this, ___item, method) (( bool (*) (List_1_t1524 *, GUILayoutEntry_t1523 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m24366(__this, ___match, method) (( int32_t (*) (List_1_t1524 *, Predicate_1_t4074 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24367(__this, ___index, method) (( void (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Reverse()
#define List_1_Reverse_m24368(__this, method) (( void (*) (List_1_t1524 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Sort()
#define List_1_Sort_m24369(__this, method) (( void (*) (List_1_t1524 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24370(__this, ___comparison, method) (( void (*) (List_1_t1524 *, Comparison_1_t4076 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::ToArray()
#define List_1_ToArray_m24371(__this, method) (( GUILayoutEntryU5BU5D_t4071* (*) (List_1_t1524 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::TrimExcess()
#define List_1_TrimExcess_m24372(__this, method) (( void (*) (List_1_t1524 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Capacity()
#define List_1_get_Capacity_m24373(__this, method) (( int32_t (*) (List_1_t1524 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24374(__this, ___value, method) (( void (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count()
#define List_1_get_Count_m24375(__this, method) (( int32_t (*) (List_1_t1524 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32)
#define List_1_get_Item_m24376(__this, ___index, method) (( GUILayoutEntry_t1523 * (*) (List_1_t1524 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m24377(__this, ___index, ___value, method) (( void (*) (List_1_t1524 *, int32_t, GUILayoutEntry_t1523 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
