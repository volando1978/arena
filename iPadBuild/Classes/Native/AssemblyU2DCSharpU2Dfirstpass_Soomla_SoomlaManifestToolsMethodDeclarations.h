﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaManifestTools
struct SoomlaManifestTools_t18;

// System.Void Soomla.SoomlaManifestTools::.ctor()
extern "C" void SoomlaManifestTools__ctor_m28 (SoomlaManifestTools_t18 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
