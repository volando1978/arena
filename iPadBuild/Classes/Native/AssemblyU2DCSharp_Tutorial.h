﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Texture
struct Texture_t736;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Tutorial
struct  Tutorial_t759  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle Tutorial::tutSt
	GUIStyle_t724 * ___tutSt_2;
	// UnityEngine.GUIStyle Tutorial::boxSt
	GUIStyle_t724 * ___boxSt_3;
	// UnityEngine.Texture Tutorial::boxTexture
	Texture_t736 * ___boxTexture_4;
};
