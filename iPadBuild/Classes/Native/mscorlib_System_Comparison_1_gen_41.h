﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t1215;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Selectable>
struct  Comparison_1_t4019  : public MulticastDelegate_t22
{
};
