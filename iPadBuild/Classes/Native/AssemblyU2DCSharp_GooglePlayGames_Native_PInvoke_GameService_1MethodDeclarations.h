﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback
struct AuthStartedCallback_t666;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"

// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AuthStartedCallback__ctor_m2674 (AuthStartedCallback_t666 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation)
extern "C" void AuthStartedCallback_Invoke_m2675 (AuthStartedCallback_t666 * __this, int32_t ___operation, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AuthStartedCallback_t666(Il2CppObject* delegate, int32_t ___operation);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,System.AsyncCallback,System.Object)
extern "C" Object_t * AuthStartedCallback_BeginInvoke_m2676 (AuthStartedCallback_t666 * __this, int32_t ___operation, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthStartedCallback::EndInvoke(System.IAsyncResult)
extern "C" void AuthStartedCallback_EndInvoke_m2677 (AuthStartedCallback_t666 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
