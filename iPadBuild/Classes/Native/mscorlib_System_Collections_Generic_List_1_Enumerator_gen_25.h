﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct List_1_t891;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct  Enumerator_t3753 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::l
	List_1_t891 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::current
	MultiplayerParticipant_t672 * ___current_3;
};
