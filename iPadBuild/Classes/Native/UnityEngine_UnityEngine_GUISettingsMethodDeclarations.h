﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUISettings
struct GUISettings_t1531;

// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m6581 (GUISettings_t1531 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
