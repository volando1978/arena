﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<cabezaScr/Frame>
struct Predicate_1_t3826;
// System.Object
struct Object_t;
// cabezaScr/Frame
struct Frame_t770;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<cabezaScr/Frame>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_5MethodDeclarations.h"
#define Predicate_1__ctor_m21012(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3826 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m15507_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<cabezaScr/Frame>::Invoke(T)
#define Predicate_1_Invoke_m21013(__this, ___obj, method) (( bool (*) (Predicate_1_t3826 *, Frame_t770 *, MethodInfo*))Predicate_1_Invoke_m15508_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<cabezaScr/Frame>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m21014(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3826 *, Frame_t770 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m15509_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<cabezaScr/Frame>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m21015(__this, ___result, method) (( bool (*) (Predicate_1_t3826 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m15510_gshared)(__this, ___result, method)
