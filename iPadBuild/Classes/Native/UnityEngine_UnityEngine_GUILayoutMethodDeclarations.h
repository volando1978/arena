﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayout
struct GUILayout_t1518;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t1527;

// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern "C" GUILayoutOption_t1527 * GUILayout_Width_m6516 (Object_t * __this /* static, unused */, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern "C" GUILayoutOption_t1527 * GUILayout_Height_m6517 (Object_t * __this /* static, unused */, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
