﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Action_2_t643;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55
struct  U3CFindEqualVersionMatchWithParticipantU3Ec__AnonStorey55_t644  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::participantId
	String_t* ___participantId_0;
	// System.Action`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::onFoundParticipantAndMatch
	Action_2_t643 * ___onFoundParticipantAndMatch_1;
	// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::match
	TurnBasedMatch_t353 * ___match_2;
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatchWithParticipant>c__AnonStorey55::onFailure
	Action_1_t98 * ___onFailure_3;
};
