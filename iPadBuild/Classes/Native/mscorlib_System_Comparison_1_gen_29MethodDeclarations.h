﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Matrix4x4>
struct Comparison_1_t3842;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Comparison`1<UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m21180_gshared (Comparison_1_t3842 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m21180(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3842 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m21180_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Matrix4x4>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m21181_gshared (Comparison_1_t3842 * __this, Matrix4x4_t997  ___x, Matrix4x4_t997  ___y, MethodInfo* method);
#define Comparison_1_Invoke_m21181(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3842 *, Matrix4x4_t997 , Matrix4x4_t997 , MethodInfo*))Comparison_1_Invoke_m21181_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Matrix4x4>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m21182_gshared (Comparison_1_t3842 * __this, Matrix4x4_t997  ___x, Matrix4x4_t997  ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m21182(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3842 *, Matrix4x4_t997 , Matrix4x4_t997 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m21182_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Matrix4x4>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m21183_gshared (Comparison_1_t3842 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m21183(__this, ___result, method) (( int32_t (*) (Comparison_1_t3842 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m21183_gshared)(__this, ___result, method)
