﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// BackgroundScript
struct  BackgroundScript_t723  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject BackgroundScript::tilePrefab
	GameObject_t144 * ___tilePrefab_2;
};
