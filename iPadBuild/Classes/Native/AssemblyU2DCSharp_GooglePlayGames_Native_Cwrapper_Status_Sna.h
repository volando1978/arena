﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Status/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Status_Sna.h"
// GooglePlayGames.Native.Cwrapper.Status/SnapshotOpenStatus
struct  SnapshotOpenStatus_t490 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Status/SnapshotOpenStatus::value__
	int32_t ___value___1;
};
