﻿#pragma once
#include <stdint.h>
// GooglePlayGames.ReportProgress
struct ReportProgress_t380;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// GooglePlayGames.PlayGamesAchievement
struct  PlayGamesAchievement_t381  : public Object_t
{
	// GooglePlayGames.ReportProgress GooglePlayGames.PlayGamesAchievement::mProgressCallback
	ReportProgress_t380 * ___mProgressCallback_0;
	// System.String GooglePlayGames.PlayGamesAchievement::mId
	String_t* ___mId_1;
	// System.Double GooglePlayGames.PlayGamesAchievement::mPercentComplete
	double ___mPercentComplete_2;
};
struct PlayGamesAchievement_t381_StaticFields{
	// System.DateTime GooglePlayGames.PlayGamesAchievement::Thesentinel
	DateTime_t48  ___Thesentinel_3;
};
