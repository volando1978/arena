﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.GameInfo
struct GameInfo_t379;
// System.String
struct String_t;

// System.Boolean GooglePlayGames.GameInfo::ApplicationIdInitialized()
extern "C" bool GameInfo_ApplicationIdInitialized_m1494 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.GameInfo::IosClientIdInitialized()
extern "C" bool GameInfo_IosClientIdInitialized_m1495 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.GameInfo::ToEscapedToken(System.String)
extern "C" String_t* GameInfo_ToEscapedToken_m1496 (Object_t * __this /* static, unused */, String_t* ___token, MethodInfo* method) IL2CPP_METHOD_ATTR;
