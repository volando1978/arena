﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1289;
// UnityEngine.CanvasGroup
struct CanvasGroup_t1387;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
struct  Enumerator_t4025 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::l
	List_1_t1289 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::current
	CanvasGroup_t1387 * ___current_3;
};
