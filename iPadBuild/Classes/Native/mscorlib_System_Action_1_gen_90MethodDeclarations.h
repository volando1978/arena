﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.UI.Toggle>
struct Action_1_t4037;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t720;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.UI.Toggle>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m23936(__this, ___object, ___method, method) (( void (*) (Action_1_t4037 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.UI.Toggle>::Invoke(T)
#define Action_1_Invoke_m23937(__this, ___obj, method) (( void (*) (Action_1_t4037 *, Toggle_t720 *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.UI.Toggle>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m23938(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t4037 *, Toggle_t720 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.UI.Toggle>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m23939(__this, ___result, method) (( void (*) (Action_1_t4037 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
