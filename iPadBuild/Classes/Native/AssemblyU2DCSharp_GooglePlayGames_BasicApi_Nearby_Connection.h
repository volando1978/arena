﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_EndpointDe.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct  ConnectionRequest_t355 
{
	// GooglePlayGames.BasicApi.Nearby.EndpointDetails GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mRemoteEndpoint
	EndpointDetails_t356  ___mRemoteEndpoint_0;
	// System.Byte[] GooglePlayGames.BasicApi.Nearby.ConnectionRequest::mPayload
	ByteU5BU5D_t350* ___mPayload_1;
};
// Native definition for marshalling of: GooglePlayGames.BasicApi.Nearby.ConnectionRequest
struct ConnectionRequest_t355_marshaled
{
	EndpointDetails_t356_marshaled ___mRemoteEndpoint_0;
	uint8_t* ___mPayload_1;
};
