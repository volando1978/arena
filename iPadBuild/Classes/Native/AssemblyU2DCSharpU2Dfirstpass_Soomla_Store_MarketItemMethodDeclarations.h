﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// Soomla.Store.MarketItem/Consumable
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_MarketItem_Consum.h"

// System.Void Soomla.Store.MarketItem::.ctor(System.String,Soomla.Store.MarketItem/Consumable,System.Double)
extern "C" void MarketItem__ctor_m582 (MarketItem_t122 * __this, String_t* ___productId, int32_t ___consumable, double ___price, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.MarketItem::.ctor(JSONObject)
extern "C" void MarketItem__ctor_m583 (MarketItem_t122 * __this, JSONObject_t30 * ___jsonObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.MarketItem::toJSONObject()
extern "C" JSONObject_t30 * MarketItem_toJSONObject_m584 (MarketItem_t122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
