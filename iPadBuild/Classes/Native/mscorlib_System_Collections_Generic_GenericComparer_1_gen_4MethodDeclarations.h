﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Object>
struct GenericComparer_1_t4217;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.GenericComparer`1<System.Object>::.ctor()
extern "C" void GenericComparer_1__ctor_m25895_gshared (GenericComparer_1_t4217 * __this, MethodInfo* method);
#define GenericComparer_1__ctor_m25895(__this, method) (( void (*) (GenericComparer_1_t4217 *, MethodInfo*))GenericComparer_1__ctor_m25895_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Object>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m25896_gshared (GenericComparer_1_t4217 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define GenericComparer_1_Compare_m25896(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t4217 *, Object_t *, Object_t *, MethodInfo*))GenericComparer_1_Compare_m25896_gshared)(__this, ___x, ___y, method)
