﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t3416;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3415;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m15428_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t* ___list, MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m15428(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3416 *, Object_t*, MethodInfo*))ReadOnlyCollection_1__ctor_m15428_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15429_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430(__this, method) (( void (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15430_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15431_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___item, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15432_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15433_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3416 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15434_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15435_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436(__this, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15436_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared (ReadOnlyCollection_1_t3416 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3416 *, Array_t *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15437_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15438_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m15439(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15439_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m15440(__this, method) (( void (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15440_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m15441(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15441_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15442_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m15443(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15443_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m15444(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15444_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15445_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446(__this, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15447_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448(__this, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15448_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449(__this, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15449_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3416 *, int32_t, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15450_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3416 *, int32_t, Object_t *, MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15451_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m15452_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m15452(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_Contains_m15452_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m15453_gshared (ReadOnlyCollection_1_t3416 * __this, ObjectU5BU5D_t208* ___array, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m15453(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3416 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))ReadOnlyCollection_1_CopyTo_m15453_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m15454_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m15454(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15454_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m15455_gshared (ReadOnlyCollection_1_t3416 * __this, Object_t * ___value, MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m15455(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3416 *, Object_t *, MethodInfo*))ReadOnlyCollection_1_IndexOf_m15455_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m15456_gshared (ReadOnlyCollection_1_t3416 * __this, MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m15456(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3416 *, MethodInfo*))ReadOnlyCollection_1_get_Count_m15456_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_get_Item_m15457_gshared (ReadOnlyCollection_1_t3416 * __this, int32_t ___index, MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m15457(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3416 *, int32_t, MethodInfo*))ReadOnlyCollection_1_get_Item_m15457_gshared)(__this, ___index, method)
