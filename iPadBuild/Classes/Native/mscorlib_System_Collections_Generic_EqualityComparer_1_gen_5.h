﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>
struct EqualityComparer_1_t3836;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>
struct  EqualityComparer_1_t3836  : public Object_t
{
};
struct EqualityComparer_1_t3836_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Matrix4x4>::_default
	EqualityComparer_1_t3836 * ____default_0;
};
