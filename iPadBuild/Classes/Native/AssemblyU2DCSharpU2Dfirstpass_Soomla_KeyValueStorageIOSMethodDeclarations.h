﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.KeyValueStorageIOS
struct KeyValueStorageIOS_t27;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Soomla.KeyValueStorageIOS::.ctor()
extern "C" void KeyValueStorageIOS__ctor_m44 (KeyValueStorageIOS_t27 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.KeyValueStorageIOS::keyValStorage_GetValue(System.String,System.IntPtr&)
extern "C" void KeyValueStorageIOS_keyValStorage_GetValue_m45 (Object_t * __this /* static, unused */, String_t* ___key, IntPtr_t* ___outResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.KeyValueStorageIOS::keyValStorage_SetValue(System.String,System.String)
extern "C" void KeyValueStorageIOS_keyValStorage_SetValue_m46 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.KeyValueStorageIOS::keyValStorage_DeleteKeyValue(System.String)
extern "C" void KeyValueStorageIOS_keyValStorage_DeleteKeyValue_m47 (Object_t * __this /* static, unused */, String_t* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.KeyValueStorageIOS::_getValue(System.String)
extern "C" String_t* KeyValueStorageIOS__getValue_m48 (KeyValueStorageIOS_t27 * __this, String_t* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.KeyValueStorageIOS::_setValue(System.String,System.String)
extern "C" void KeyValueStorageIOS__setValue_m49 (KeyValueStorageIOS_t27 * __this, String_t* ___key, String_t* ___val, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.KeyValueStorageIOS::_deleteKeyValue(System.String)
extern "C" void KeyValueStorageIOS__deleteKeyValue_m50 (KeyValueStorageIOS_t27 * __this, String_t* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
