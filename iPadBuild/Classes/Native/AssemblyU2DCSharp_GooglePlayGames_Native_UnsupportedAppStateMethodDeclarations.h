﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.UnsupportedAppStateClient
struct UnsupportedAppStateClient_t713;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.OnStateLoadedListener
struct OnStateLoadedListener_t842;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::.ctor(System.String)
extern "C" void UnsupportedAppStateClient__ctor_m3121 (UnsupportedAppStateClient_t713 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::LoadState(System.Int32,GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void UnsupportedAppStateClient_LoadState_m3122 (UnsupportedAppStateClient_t713 * __this, int32_t ___slot, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedAppStateClient::UpdateState(System.Int32,System.Byte[],GooglePlayGames.BasicApi.OnStateLoadedListener)
extern "C" void UnsupportedAppStateClient_UpdateState_m3123 (UnsupportedAppStateClient_t713 * __this, int32_t ___slot, ByteU5BU5D_t350* ___data, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
