﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_t1615;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t1624;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t1497;

// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern "C" UserProfile_t1624 * GcUserProfileData_ToUserProfile_m7312 (GcUserProfileData_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern "C" void GcUserProfileData_AddToArray_m7313 (GcUserProfileData_t1615 * __this, UserProfileU5BU5D_t1497** ___array, int32_t ___number, MethodInfo* method) IL2CPP_METHOD_ATTR;
