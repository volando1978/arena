﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Func_2_t578;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_40MethodDeclarations.h"
#define Func_2__ctor_m3809(__this, ___object, ___method, method) (( void (*) (Func_2_t578 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m19196_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::Invoke(T)
#define Func_2_Invoke_m20109(__this, ___arg1, method) (( Participant_t340 * (*) (Func_2_t578 *, MultiplayerParticipant_t672 *, MethodInfo*))Func_2_Invoke_m19198_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20110(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t578 *, MultiplayerParticipant_t672 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m19200_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,GooglePlayGames.BasicApi.Multiplayer.Participant>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20111(__this, ___result, method) (( Participant_t340 * (*) (Func_2_t578 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m19202_gshared)(__this, ___result, method)
