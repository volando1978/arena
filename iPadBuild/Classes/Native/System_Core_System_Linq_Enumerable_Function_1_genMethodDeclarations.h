﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/Function`1<System.Object>
struct Function_1_t3747;
// System.Object
struct Object_t;

// System.Void System.Linq.Enumerable/Function`1<System.Object>::.cctor()
extern "C" void Function_1__cctor_m20115_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define Function_1__cctor_m20115(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))Function_1__cctor_m20115_gshared)(__this /* static, unused */, method)
// T System.Linq.Enumerable/Function`1<System.Object>::<Identity>m__77(T)
extern "C" Object_t * Function_1_U3CIdentityU3Em__77_m20116_gshared (Object_t * __this /* static, unused */, Object_t * ___t, MethodInfo* method);
#define Function_1_U3CIdentityU3Em__77_m20116(__this /* static, unused */, ___t, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Function_1_U3CIdentityU3Em__77_m20116_gshared)(__this /* static, unused */, ___t, method)
