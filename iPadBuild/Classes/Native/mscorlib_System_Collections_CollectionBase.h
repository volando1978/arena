﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t737;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CollectionBase
struct  CollectionBase_t2030  : public Object_t
{
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t737 * ___list_0;
};
