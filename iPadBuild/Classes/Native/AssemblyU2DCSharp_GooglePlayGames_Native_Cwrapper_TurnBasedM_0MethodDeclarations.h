﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t494;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_Length_m2162 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_PlayerIdsToInvite_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  TurnBasedMatchConfig_TurnBasedMatchConfig_PlayerIdsToInvite_GetElement_m2163 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Variant(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_Variant_m2164 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_ExclusiveBitMask(System.Runtime.InteropServices.HandleRef)
extern "C" int64_t TurnBasedMatchConfig_TurnBasedMatchConfig_ExclusiveBitMask_m2165 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool TurnBasedMatchConfig_TurnBasedMatchConfig_Valid_m2166 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MaximumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MaximumAutomatchingPlayers_m2167 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_MinimumAutomatchingPlayers(System.Runtime.InteropServices.HandleRef)
extern "C" uint32_t TurnBasedMatchConfig_TurnBasedMatchConfig_MinimumAutomatchingPlayers_m2168 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMatchConfig::TurnBasedMatchConfig_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchConfig_TurnBasedMatchConfig_Dispose_m2169 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
