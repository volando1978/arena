﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback
struct ReadCallback_t476;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ReadCallback__ctor_m2082 (ReadCallback_t476 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void ReadCallback_Invoke_m2083 (ReadCallback_t476 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReadCallback_t476(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * ReadCallback_BeginInvoke_m2084 (ReadCallback_t476 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.SnapshotManager/ReadCallback::EndInvoke(System.IAsyncResult)
extern "C" void ReadCallback_EndInvoke_m2085 (ReadCallback_t476 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
