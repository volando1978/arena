﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Enumerator_t923;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct Dictionary_2_t575;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_3MethodDeclarations.h"
#define Enumerator__ctor_m20226(__this, ___host, method) (( void (*) (Enumerator_t923 *, Dictionary_2_t575 *, MethodInfo*))Enumerator__ctor_m16301_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20227(__this, method) (( Object_t * (*) (Enumerator_t923 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16302_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::Dispose()
#define Enumerator_Dispose_m20228(__this, method) (( void (*) (Enumerator_t923 *, MethodInfo*))Enumerator_Dispose_m16303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::MoveNext()
#define Enumerator_MoveNext_m3816(__this, method) (( bool (*) (Enumerator_t923 *, MethodInfo*))Enumerator_MoveNext_m16304_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.Native.PInvoke.MultiplayerParticipant>::get_Current()
#define Enumerator_get_Current_m3815(__this, method) (( String_t* (*) (Enumerator_t923 *, MethodInfo*))Enumerator_get_Current_m16305_gshared)(__this, method)
