﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32
struct U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32::.ctor()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey32__ctor_m2459 (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateWithInvitationScreen>c__AnonStorey2F/<CreateWithInvitationScreen>c__AnonStorey32::<>m__2C()
extern "C" void U3CCreateWithInvitationScreenU3Ec__AnonStorey32_U3CU3Em__2C_m2460 (U3CCreateWithInvitationScreenU3Ec__AnonStorey32_t600 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
