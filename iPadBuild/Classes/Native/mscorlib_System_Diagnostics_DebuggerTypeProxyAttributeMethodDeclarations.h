﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t2454;
// System.Type
struct Type_t;

// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
extern "C" void DebuggerTypeProxyAttribute__ctor_m11481 (DebuggerTypeProxyAttribute_t2454 * __this, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
