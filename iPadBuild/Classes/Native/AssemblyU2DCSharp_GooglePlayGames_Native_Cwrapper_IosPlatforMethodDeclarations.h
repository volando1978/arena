﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration
struct IosPlatformConfiguration_t425;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Construct()
extern "C" IntPtr_t IosPlatformConfiguration_IosPlatformConfiguration_Construct_m1726 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void IosPlatformConfiguration_IosPlatformConfiguration_Dispose_m1727 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool IosPlatformConfiguration_IosPlatformConfiguration_Valid_m1728 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.IosPlatformConfiguration::IosPlatformConfiguration_SetClientID(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" void IosPlatformConfiguration_IosPlatformConfiguration_SetClientID_m1729 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___client_id, MethodInfo* method) IL2CPP_METHOD_ATTR;
