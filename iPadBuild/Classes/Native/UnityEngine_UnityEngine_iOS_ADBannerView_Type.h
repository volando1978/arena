﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.iOS.ADBannerView/Type
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Type.h"
// UnityEngine.iOS.ADBannerView/Type
struct  Type_t1570 
{
	// System.Int32 UnityEngine.iOS.ADBannerView/Type::value__
	int32_t ___value___1;
};
