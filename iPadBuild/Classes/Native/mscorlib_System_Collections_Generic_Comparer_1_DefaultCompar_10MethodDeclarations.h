﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t4248;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m26124_gshared (DefaultComparer_t4248 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26124(__this, method) (( void (*) (DefaultComparer_t4248 *, MethodInfo*))DefaultComparer__ctor_m26124_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m26125_gshared (DefaultComparer_t4248 * __this, DateTimeOffset_t2793  ___x, DateTimeOffset_t2793  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m26125(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4248 *, DateTimeOffset_t2793 , DateTimeOffset_t2793 , MethodInfo*))DefaultComparer_Compare_m26125_gshared)(__this, ___x, ___y, method)
