﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t22;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t2875;
// System.Delegate
struct Delegate_t211;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m1274 (MulticastDelegate_t22 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m1272 (MulticastDelegate_t22 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m1273 (MulticastDelegate_t22 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t2875* MulticastDelegate_GetInvocationList_m1276 (MulticastDelegate_t22 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t211 * MulticastDelegate_CombineImpl_m1277 (MulticastDelegate_t22 * __this, Delegate_t211 * ___follow, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m10489 (MulticastDelegate_t22 * __this, MulticastDelegate_t22 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t22 * MulticastDelegate_KPM_m10490 (Object_t * __this /* static, unused */, MulticastDelegate_t22 * ___needle, MulticastDelegate_t22 * ___haystack, MulticastDelegate_t22 ** ___tail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t211 * MulticastDelegate_RemoveImpl_m1278 (MulticastDelegate_t22 * __this, Delegate_t211 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
