﻿#pragma once
#include <stdint.h>
// Soomla.Reward[]
struct RewardU5BU5D_t3519;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Reward>
struct  List_1_t56  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Reward>::_items
	RewardU5BU5D_t3519* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Reward>::_version
	int32_t ____version_3;
};
struct List_1_t56_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Reward>::EmptyArray
	RewardU5BU5D_t3519* ___EmptyArray_4;
};
