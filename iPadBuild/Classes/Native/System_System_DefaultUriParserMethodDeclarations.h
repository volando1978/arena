﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DefaultUriParser
struct DefaultUriParser_t2115;
// System.String
struct String_t;

// System.Void System.DefaultUriParser::.ctor()
extern "C" void DefaultUriParser__ctor_m8666 (DefaultUriParser_t2115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DefaultUriParser::.ctor(System.String)
extern "C" void DefaultUriParser__ctor_m8667 (DefaultUriParser_t2115 * __this, String_t* ___scheme, MethodInfo* method) IL2CPP_METHOD_ATTR;
