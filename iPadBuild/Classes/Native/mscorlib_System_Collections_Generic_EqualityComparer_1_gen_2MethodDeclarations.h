﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t3542;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.ctor()
extern "C" void EqualityComparer_1__ctor_m17177_gshared (EqualityComparer_1_t3542 * __this, MethodInfo* method);
#define EqualityComparer_1__ctor_m17177(__this, method) (( void (*) (EqualityComparer_1_t3542 *, MethodInfo*))EqualityComparer_1__ctor_m17177_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.cctor()
extern "C" void EqualityComparer_1__cctor_m17178_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1__cctor_m17178(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1__cctor_m17178_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17179_gshared (EqualityComparer_1_t3542 * __this, Object_t * ___obj, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17179(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3542 *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17179_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17180_gshared (EqualityComparer_1_t3542 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17180(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3542 *, Object_t *, Object_t *, MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17180_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::get_Default()
extern "C" EqualityComparer_1_t3542 * EqualityComparer_1_get_Default_m17181_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define EqualityComparer_1_get_Default_m17181(__this /* static, unused */, method) (( EqualityComparer_1_t3542 * (*) (Object_t * /* static, unused */, MethodInfo*))EqualityComparer_1_get_Default_m17181_gshared)(__this /* static, unused */, method)
