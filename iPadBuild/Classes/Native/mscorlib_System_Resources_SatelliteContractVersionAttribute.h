﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1997;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1807  : public Attribute_t1546
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1997 * ___ver_0;
};
