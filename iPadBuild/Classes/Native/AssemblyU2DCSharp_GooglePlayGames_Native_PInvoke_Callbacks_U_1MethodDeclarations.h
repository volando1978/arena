﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Byte,System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Byte,System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20342_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20342(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2__ctor_m20342_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Byte,System.Object>::<>m__76(T1,T2)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20343_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771 * __this, uint8_t ___result1, Object_t * ___result2, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20343(__this, ___result1, ___result2, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3771 *, uint8_t, Object_t *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_U3CU3Em__76_m20343_gshared)(__this, ___result1, ___result2, method)
