﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.PurchasableVirtualItem
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchasableVirtua.h"
// Soomla.Store.VirtualCurrencyPack
struct  VirtualCurrencyPack_t78  : public PurchasableVirtualItem_t124
{
	// System.Int32 Soomla.Store.VirtualCurrencyPack::CurrencyAmount
	int32_t ___CurrencyAmount_8;
	// System.String Soomla.Store.VirtualCurrencyPack::CurrencyItemId
	String_t* ___CurrencyItemId_9;
};
struct VirtualCurrencyPack_t78_StaticFields{
	// System.String Soomla.Store.VirtualCurrencyPack::TAG
	String_t* ___TAG_7;
};
