﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t1302;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t1303;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t1304;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.ToggleGroup
struct  ToggleGroup_t1301  : public UIBehaviour_t1156
{
	// System.Boolean UnityEngine.UI.ToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::m_Toggles
	List_1_t1302 * ___m_Toggles_3;
};
struct ToggleGroup_t1301_StaticFields{
	// System.Predicate`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::<>f__am$cache2
	Predicate_1_t1303 * ___U3CU3Ef__amU24cache2_4;
	// System.Func`2<UnityEngine.UI.Toggle,System.Boolean> UnityEngine.UI.ToggleGroup::<>f__am$cache3
	Func_2_t1304 * ___U3CU3Ef__amU24cache3_5;
};
