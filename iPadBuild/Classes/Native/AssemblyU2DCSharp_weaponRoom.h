﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.Texture
struct Texture_t736;
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// weaponRoom
struct  weaponRoom_t840  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject weaponRoom::gameControlObj
	GameObject_t144 * ___gameControlObj_2;
	// System.Single weaponRoom::offsetWeaponIcon
	float ___offsetWeaponIcon_3;
	// System.Single weaponRoom::offsetForStripe
	float ___offsetForStripe_4;
	// System.Single weaponRoom::offsetLetterShadow
	float ___offsetLetterShadow_5;
	// System.Int32 weaponRoom::marginLeft
	int32_t ___marginLeft_6;
	// System.Int32 weaponRoom::marginTop
	int32_t ___marginTop_7;
	// UnityEngine.Rect weaponRoom::boxRect
	Rect_t738  ___boxRect_8;
	// UnityEngine.Rect weaponRoom::messageRect
	Rect_t738  ___messageRect_9;
	// UnityEngine.GUIStyle weaponRoom::gearWordSt
	GUIStyle_t724 * ___gearWordSt_10;
	// UnityEngine.GUIStyle weaponRoom::weaponsRoomSt
	GUIStyle_t724 * ___weaponsRoomSt_11;
	// UnityEngine.GUIStyle weaponRoom::lowBarButtonSt
	GUIStyle_t724 * ___lowBarButtonSt_12;
	// UnityEngine.GUIStyle weaponRoom::lowBarPlaySt
	GUIStyle_t724 * ___lowBarPlaySt_13;
	// UnityEngine.GUIStyle weaponRoom::levelSt
	GUIStyle_t724 * ___levelSt_14;
	// UnityEngine.GUIStyle weaponRoom::titleSt
	GUIStyle_t724 * ___titleSt_15;
	// UnityEngine.Texture weaponRoom::keyPic
	Texture_t736 * ___keyPic_16;
	// System.Collections.ArrayList weaponRoom::iconBombaGfx
	ArrayList_t737 * ___iconBombaGfx_17;
	// System.Boolean weaponRoom::screenPos
	bool ___screenPos_20;
};
struct weaponRoom_t840_StaticFields{
	// UnityEngine.Rect weaponRoom::weaponSelectHUD
	Rect_t738  ___weaponSelectHUD_18;
	// UnityEngine.Rect weaponRoom::levelSelectHUD
	Rect_t738  ___levelSelectHUD_19;
};
