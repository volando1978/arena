﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse
struct FetchAllResponse_t702;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/FetchAllResponse>
struct  Action_1_t893  : public MulticastDelegate_t22
{
};
