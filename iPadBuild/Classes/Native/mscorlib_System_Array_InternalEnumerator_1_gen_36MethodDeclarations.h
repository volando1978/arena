﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
struct InternalEnumerator_1_t4007;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23520_gshared (InternalEnumerator_1_t4007 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m23520(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4007 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m23520_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23521_gshared (InternalEnumerator_1_t4007 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23521(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4007 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23521_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23522_gshared (InternalEnumerator_1_t4007 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23522(__this, method) (( void (*) (InternalEnumerator_1_t4007 *, MethodInfo*))InternalEnumerator_1_Dispose_m23522_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23523_gshared (InternalEnumerator_1_t4007 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23523(__this, method) (( bool (*) (InternalEnumerator_1_t4007 *, MethodInfo*))InternalEnumerator_1_MoveNext_m23523_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t1398  InternalEnumerator_1_get_Current_m23524_gshared (InternalEnumerator_1_t4007 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23524(__this, method) (( UILineInfo_t1398  (*) (InternalEnumerator_1_t4007 *, MethodInfo*))InternalEnumerator_1_get_Current_m23524_gshared)(__this, method)
