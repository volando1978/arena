﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoEnumInfo/IntComparer
struct IntComparer_t2804;
// System.Object
struct Object_t;

// System.Void System.MonoEnumInfo/IntComparer::.ctor()
extern "C" void IntComparer__ctor_m13983 (IntComparer_t2804 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Object,System.Object)
extern "C" int32_t IntComparer_Compare_m13984 (IntComparer_t2804 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Int32,System.Int32)
extern "C" int32_t IntComparer_Compare_m13985 (IntComparer_t2804 * __this, int32_t ___ix, int32_t ___iy, MethodInfo* method) IL2CPP_METHOD_ATTR;
