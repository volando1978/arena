﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t588;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Action>
struct  Comparison_1_t3705  : public MulticastDelegate_t22
{
};
