﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey15_1__ctor_m19611_gshared (U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey15_1__ctor_m19611(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey15_1__ctor_m19611_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1/<ToOnGameThread>c__AnonStorey15`1<System.Object>::<>m__A()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey15_1_U3CU3Em__A_m19612_gshared (U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey15_1_U3CU3Em__A_m19612(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey15_1_t3707 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey15_1_U3CU3Em__A_m19612_gshared)(__this, method)
