﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1<System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1<System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey14_1__ctor_m19609_gshared (U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey14_1__ctor_m19609(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey14_1__ctor_m19609_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey14`1<System.Object>::<>m__5(T)
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey14_1_U3CU3Em__5_m19610_gshared (U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706 * __this, Object_t * ___val, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey14_1_U3CU3Em__5_m19610(__this, ___val, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey14_1_t3706 *, Object_t *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey14_1_U3CU3Em__5_m19610_gshared)(__this, ___val, method)
