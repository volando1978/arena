﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// enemyController
struct enemyController_t785;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// movementProxy
struct  movementProxy_t815  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject movementProxy::enemyController
	GameObject_t144 * ___enemyController_2;
	// enemyController movementProxy::_enemyController
	enemyController_t785 * ____enemyController_3;
};
