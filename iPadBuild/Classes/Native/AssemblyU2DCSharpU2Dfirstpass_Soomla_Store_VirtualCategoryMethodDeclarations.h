﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.VirtualCategory::.ctor(System.String,System.Collections.Generic.List`1<System.String>)
extern "C" void VirtualCategory__ctor_m590 (VirtualCategory_t126 * __this, String_t* ___name, List_1_t43 * ___goodItemIds, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualCategory::.ctor(JSONObject)
extern "C" void VirtualCategory__ctor_m591 (VirtualCategory_t126 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.VirtualCategory::toJSONObject()
extern "C" JSONObject_t30 * VirtualCategory_toJSONObject_m592 (VirtualCategory_t126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
