﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Byte>
struct Action_1_t910;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>
struct  U3CInvokeCallbackOnGameThreadU3Ec__AnonStorey1B_1_t3718  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>::callback
	Action_1_t910 * ___callback_0;
	// T GooglePlayGames.Native.NativeClient/<InvokeCallbackOnGameThread>c__AnonStorey1B`1<System.Byte>::data
	uint8_t ___data_1;
};
