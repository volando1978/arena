﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct U24ArrayTypeU2496_t2865;
struct U24ArrayTypeU2496_t2865_marshaled;

void U24ArrayTypeU2496_t2865_marshal(const U24ArrayTypeU2496_t2865& unmarshaled, U24ArrayTypeU2496_t2865_marshaled& marshaled);
void U24ArrayTypeU2496_t2865_marshal_back(const U24ArrayTypeU2496_t2865_marshaled& marshaled, U24ArrayTypeU2496_t2865& unmarshaled);
void U24ArrayTypeU2496_t2865_marshal_cleanup(U24ArrayTypeU2496_t2865_marshaled& marshaled);
