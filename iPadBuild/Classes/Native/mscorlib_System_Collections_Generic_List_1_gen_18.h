﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Quests.IQuest[]
struct IQuestU5BU5D_t3682;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>
struct  List_1_t918  : public Object_t
{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::_items
	IQuestU5BU5D_t3682* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::_version
	int32_t ____version_3;
};
struct List_1_t918_StaticFields{
	// T[] System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>::EmptyArray
	IQuestU5BU5D_t3682* ___EmptyArray_4;
};
