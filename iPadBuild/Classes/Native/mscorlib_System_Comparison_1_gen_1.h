﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t1360;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct  Comparison_1_t1219  : public MulticastDelegate_t22
{
};
