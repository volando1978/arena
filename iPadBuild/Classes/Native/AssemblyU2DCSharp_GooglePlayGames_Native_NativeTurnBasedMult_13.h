﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch>
struct Action_2_t632;
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct NativeTurnBasedMultiplayerClient_t535;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B
struct  U3CRematchU3Ec__AnonStorey5B_t650  : public Object_t
{
	// System.Action`2<System.Boolean,GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::callback
	Action_2_t632 * ___callback_0;
	// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Rematch>c__AnonStorey5B::<>f__this
	NativeTurnBasedMultiplayerClient_t535 * ___U3CU3Ef__this_1;
};
