﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Object,System.Object>
struct Action_2_t3481;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Object>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey60_2_t3782  : public Object_t
{
	// System.Action`2<T1,T2> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey60`2<System.Object,System.Object>::toInvokeOnGameThread
	Action_2_t3481 * ___toInvokeOnGameThread_0;
};
