﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t1488;
// UnityEngine.Object
struct Object_t187;
struct Object_t187_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1662;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m6351 (AssetBundleRequest_t1488 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t187 * AssetBundleRequest_get_asset_m6352 (AssetBundleRequest_t1488 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t1662* AssetBundleRequest_get_allAssets_m6353 (AssetBundleRequest_t1488 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
