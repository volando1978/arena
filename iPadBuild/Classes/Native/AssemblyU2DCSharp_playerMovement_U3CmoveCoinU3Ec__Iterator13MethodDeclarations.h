﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// playerMovement/<moveCoin>c__Iterator13
struct U3CmoveCoinU3Ec__Iterator13_t829;
// System.Object
struct Object_t;

// System.Void playerMovement/<moveCoin>c__Iterator13::.ctor()
extern "C" void U3CmoveCoinU3Ec__Iterator13__ctor_m3626 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CmoveCoinU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3627 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object playerMovement/<moveCoin>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CmoveCoinU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m3628 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean playerMovement/<moveCoin>c__Iterator13::MoveNext()
extern "C" bool U3CmoveCoinU3Ec__Iterator13_MoveNext_m3629 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerMovement/<moveCoin>c__Iterator13::Dispose()
extern "C" void U3CmoveCoinU3Ec__Iterator13_Dispose_m3630 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void playerMovement/<moveCoin>c__Iterator13::Reset()
extern "C" void U3CmoveCoinU3Ec__Iterator13_Reset_m3631 (U3CmoveCoinU3Ec__Iterator13_t829 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
