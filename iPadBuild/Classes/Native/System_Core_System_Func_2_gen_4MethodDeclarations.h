﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.String,System.Boolean>
struct Func_2_t926;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.String,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Byte>
#include "System_Core_System_Func_2_gen_38MethodDeclarations.h"
#define Func_2__ctor_m3831(__this, ___object, ___method, method) (( void (*) (Func_2_t926 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m18520_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.String,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m20304(__this, ___arg1, method) (( bool (*) (Func_2_t926 *, String_t*, MethodInfo*))Func_2_Invoke_m18522_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.String,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20305(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t926 *, String_t*, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m18524_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.String,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20306(__this, ___result, method) (( bool (*) (Func_2_t926 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m18526_gshared)(__this, ___result, method)
