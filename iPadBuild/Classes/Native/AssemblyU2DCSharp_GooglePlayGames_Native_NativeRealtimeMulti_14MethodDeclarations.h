﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState
struct ActiveState_t586;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.String
struct String_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::.ctor(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void ActiveState__ctor_m2425 (ActiveState_t586 * __this, NativeRealTimeRoom_t574 * ___room, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::OnStateEntered()
extern "C" void ActiveState_OnStateEntered_m2426 (ActiveState_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::IsRoomConnected()
extern "C" bool ActiveState_IsRoomConnected_m2427 (ActiveState_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetParticipant(System.String)
extern "C" Participant_t340 * ActiveState_GetParticipant_m2428 (ActiveState_t586 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::GetSelf()
extern "C" Participant_t340 * ActiveState_GetSelf_m2429 (ActiveState_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::HandleConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void ActiveState_HandleConnectedSetChanged_m2430 (ActiveState_t586 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::LeaveRoom()
extern "C" void ActiveState_LeaveRoom_m2431 (ActiveState_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3D(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__3D_m2432 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3E(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" Participant_t340 * ActiveState_U3CHandleConnectedSetChangedU3Em__3E_m2433 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__3F(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__3F_m2434 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<HandleConnectedSetChanged>m__40(GooglePlayGames.BasicApi.Multiplayer.Participant)
extern "C" String_t* ActiveState_U3CHandleConnectedSetChangedU3Em__40_m2435 (Object_t * __this /* static, unused */, Participant_t340 * ___p, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState::<LeaveRoom>m__45()
extern "C" void ActiveState_U3CLeaveRoomU3Em__45_m2436 (ActiveState_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
