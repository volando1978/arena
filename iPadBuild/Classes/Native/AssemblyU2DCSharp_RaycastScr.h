﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t75;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RaycastScr
struct  RaycastScr_t751  : public MonoBehaviour_t26
{
	// System.Int32 RaycastScr::timer
	int32_t ___timer_2;
	// System.Single RaycastScr::length
	float ___length_3;
	// UnityEngine.GameObject RaycastScr::enemyController
	GameObject_t144 * ___enemyController_4;
	// UnityEngine.GameObject RaycastScr::explosion
	GameObject_t144 * ___explosion_5;
	// UnityEngine.GameObject RaycastScr::paquete
	GameObject_t144 * ___paquete_6;
	// UnityEngine.GameObject RaycastScr::coin
	GameObject_t144 * ___coin_7;
	// UnityEngine.GameObject RaycastScr::movingText
	GameObject_t144 * ___movingText_8;
	// UnityEngine.GameObject RaycastScr::movingTextKills
	GameObject_t144 * ___movingTextKills_9;
	// UnityEngine.GameObject RaycastScr::dust
	GameObject_t144 * ___dust_10;
	// System.Boolean RaycastScr::killed
	bool ___killed_11;
};
struct RaycastScr_t751_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> RaycastScr::<>f__switch$map0
	Dictionary_2_t75 * ___U3CU3Ef__switchU24map0_12;
};
