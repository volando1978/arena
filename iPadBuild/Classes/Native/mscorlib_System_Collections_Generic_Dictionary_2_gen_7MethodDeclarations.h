﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct Dictionary_2_t113;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct KeyCollection_t3619;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct ValueCollection_t3620;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>[]
struct KeyValuePair_2U5BU5D_t4339;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>>
struct IEnumerator_1_t4340;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__11.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m1008(__this, method) (( void (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18233(__this, ___comparer, method) (( void (*) (Dictionary_2_t113 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m18234(__this, ___capacity, method) (( void (*) (Dictionary_2_t113 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m18235(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t113 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m18236(__this, method) (( Object_t * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18237(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t113 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18238(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t113 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m18239(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t113 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m18240(__this, ___key, method) (( void (*) (Dictionary_2_t113 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18241(__this, method) (( bool (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18242(__this, method) (( Object_t * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18243(__this, method) (( bool (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18244(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t113 *, KeyValuePair_2_t3618 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18245(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t113 *, KeyValuePair_2_t3618 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18246(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t113 *, KeyValuePair_2U5BU5D_t4339*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18247(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t113 *, KeyValuePair_2_t3618 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18248(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t113 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18249(__this, method) (( Object_t * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18250(__this, method) (( Object_t* (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18251(__this, method) (( Object_t * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Count()
#define Dictionary_2_get_Count_m18252(__this, method) (( int32_t (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Item(TKey)
#define Dictionary_2_get_Item_m18253(__this, ___key, method) (( List_1_t178 * (*) (Dictionary_2_t113 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m18254(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t113 *, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m18255(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t113 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m18256(__this, ___size, method) (( void (*) (Dictionary_2_t113 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m18257(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t113 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m18258(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3618  (*) (Object_t * /* static, unused */, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m18259(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m18260(__this /* static, unused */, ___key, ___value, method) (( List_1_t178 * (*) (Object_t * /* static, unused */, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m18261(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t113 *, KeyValuePair_2U5BU5D_t4339*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::Resize()
#define Dictionary_2_Resize_m18262(__this, method) (( void (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::Add(TKey,TValue)
#define Dictionary_2_Add_m18263(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t113 *, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::Clear()
#define Dictionary_2_Clear_m18264(__this, method) (( void (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m18265(__this, ___key, method) (( bool (*) (Dictionary_2_t113 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m18266(__this, ___value, method) (( bool (*) (Dictionary_2_t113 *, List_1_t178 *, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m18267(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t113 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m18268(__this, ___sender, method) (( void (*) (Dictionary_2_t113 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::Remove(TKey)
#define Dictionary_2_Remove_m18269(__this, ___key, method) (( bool (*) (Dictionary_2_t113 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m18270(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t113 *, String_t*, List_1_t178 **, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Keys()
#define Dictionary_2_get_Keys_m18271(__this, method) (( KeyCollection_t3619 * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::get_Values()
#define Dictionary_2_get_Values_m18272(__this, method) (( ValueCollection_t3620 * (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m18273(__this, ___key, method) (( String_t* (*) (Dictionary_2_t113 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m18274(__this, ___value, method) (( List_1_t178 * (*) (Dictionary_2_t113 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m18275(__this, ___pair, method) (( bool (*) (Dictionary_2_t113 *, KeyValuePair_2_t3618 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m18276(__this, method) (( Enumerator_t3621  (*) (Dictionary_2_t113 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m18277(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, List_1_t178 *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
