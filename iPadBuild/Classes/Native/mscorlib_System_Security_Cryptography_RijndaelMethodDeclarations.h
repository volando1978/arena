﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Rijndael
struct Rijndael_t2324;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.Rijndael::.ctor()
extern "C" void Rijndael__ctor_m13095 (Rijndael_t2324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create()
extern "C" Rijndael_t2324 * Rijndael_Create_m9868 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Rijndael System.Security.Cryptography.Rijndael::Create(System.String)
extern "C" Rijndael_t2324 * Rijndael_Create_m13096 (Object_t * __this /* static, unused */, String_t* ___algName, MethodInfo* method) IL2CPP_METHOD_ATTR;
