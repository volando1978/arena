﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// menu
struct menu_t814;

// System.Void menu::.ctor()
extern "C" void menu__ctor_m3565 (menu_t814 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void menu::Start()
extern "C" void menu_Start_m3566 (menu_t814 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void menu::OnGUI()
extern "C" void menu_OnGUI_m3567 (menu_t814 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void menu::OnApplicationQuit()
extern "C" void menu_OnApplicationQuit_m3568 (menu_t814 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
