﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
// UnityEngine.Advertisements.Advertisement/DebugLevel
struct  DebugLevel_t142 
{
	// System.Int32 UnityEngine.Advertisements.Advertisement/DebugLevel::value__
	int32_t ___value___1;
};
