﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameOverSrc/<run>c__IteratorD
struct U3CrunU3Ec__IteratorD_t802;
// System.Object
struct Object_t;

// System.Void gameOverSrc/<run>c__IteratorD::.ctor()
extern "C" void U3CrunU3Ec__IteratorD__ctor_m3488 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameOverSrc/<run>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CrunU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameOverSrc/<run>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CrunU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3490 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameOverSrc/<run>c__IteratorD::MoveNext()
extern "C" bool U3CrunU3Ec__IteratorD_MoveNext_m3491 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc/<run>c__IteratorD::Dispose()
extern "C" void U3CrunU3Ec__IteratorD_Dispose_m3492 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameOverSrc/<run>c__IteratorD::Reset()
extern "C" void U3CrunU3Ec__IteratorD_Reset_m3493 (U3CrunU3Ec__IteratorD_t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
