﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>
struct PrimeHelper_t3765;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>::.cctor()
extern "C" void PrimeHelper__cctor_m20284_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define PrimeHelper__cctor_m20284(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))PrimeHelper__cctor_m20284_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m20285_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_TestPrime_m20285(__this /* static, unused */, ___x, method) (( bool (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_TestPrime_m20285_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>::CalcPrime(System.Int32)
extern "C" int32_t PrimeHelper_CalcPrime_m20286_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_CalcPrime_m20286(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_CalcPrime_m20286_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>::ToPrime(System.Int32)
extern "C" int32_t PrimeHelper_ToPrime_m20287_gshared (Object_t * __this /* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_ToPrime_m20287(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, MethodInfo*))PrimeHelper_ToPrime_m20287_gshared)(__this /* static, unused */, ___x, method)
