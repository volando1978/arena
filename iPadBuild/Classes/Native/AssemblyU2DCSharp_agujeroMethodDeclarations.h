﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// agujero
struct agujero_t761;

// System.Void agujero::.ctor()
extern "C" void agujero__ctor_m3302 (agujero_t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void agujero::Start()
extern "C" void agujero_Start_m3303 (agujero_t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void agujero::Update()
extern "C" void agujero_Update_m3304 (agujero_t761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
