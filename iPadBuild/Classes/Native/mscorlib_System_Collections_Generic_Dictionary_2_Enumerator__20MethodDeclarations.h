﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct Enumerator_t3925;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3920;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22333_gshared (Enumerator_t3925 * __this, Dictionary_2_t3920 * ___dictionary, MethodInfo* method);
#define Enumerator__ctor_m22333(__this, ___dictionary, method) (( void (*) (Enumerator_t3925 *, Dictionary_2_t3920 *, MethodInfo*))Enumerator__ctor_m22333_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22334(__this, method) (( Object_t * (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22334_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2128  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336(__this, method) (( Object_t * (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337(__this, method) (( Object_t * (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22337_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22338_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m22338(__this, method) (( bool (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_MoveNext_m22338_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3921  Enumerator_get_Current_m22339_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_get_Current_m22339(__this, method) (( KeyValuePair_2_t3921  (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_get_Current_m22339_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22340_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_get_CurrentKey_m22340(__this, method) (( int32_t (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_get_CurrentKey_m22340_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m22341_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_get_CurrentValue_m22341(__this, method) (( Object_t * (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_get_CurrentValue_m22341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m22342_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m22342(__this, method) (( void (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_VerifyState_m22342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22343_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_VerifyCurrent_m22343(__this, method) (( void (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_VerifyCurrent_m22343_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m22344_gshared (Enumerator_t3925 * __this, MethodInfo* method);
#define Enumerator_Dispose_m22344(__this, method) (( void (*) (Enumerator_t3925 *, MethodInfo*))Enumerator_Dispose_m22344_gshared)(__this, method)
