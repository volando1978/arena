﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t737;
// UnityEngine.AudioClip
struct AudioClip_t753;
// UnityEngine.AudioSource
struct AudioSource_t755;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// songsBatch
struct  songsBatch_t834  : public MonoBehaviour_t26
{
	// System.Collections.ArrayList songsBatch::songs
	ArrayList_t737 * ___songs_2;
	// UnityEngine.AudioClip songsBatch::switcherSound
	AudioClip_t753 * ___switcherSound_3;
	// UnityEngine.AudioClip songsBatch::track_1
	AudioClip_t753 * ___track_1_4;
	// UnityEngine.AudioClip songsBatch::track_2
	AudioClip_t753 * ___track_2_5;
	// UnityEngine.AudioClip songsBatch::track_3
	AudioClip_t753 * ___track_3_6;
	// UnityEngine.AudioClip songsBatch::track_4
	AudioClip_t753 * ___track_4_7;
	// UnityEngine.AudioClip songsBatch::track_5
	AudioClip_t753 * ___track_5_8;
	// UnityEngine.AudioSource songsBatch::musicPlayer
	AudioSource_t755 * ___musicPlayer_9;
};
