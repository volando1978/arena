﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualGoodsStorage
struct VirtualGoodsStorage_t71;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.VirtualGoodsStorage::.ctor()
extern "C" void VirtualGoodsStorage__ctor_m550 (VirtualGoodsStorage_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::.cctor()
extern "C" void VirtualGoodsStorage__cctor_m551 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualGoodsStorage Soomla.Store.VirtualGoodsStorage::get_instance()
extern "C" VirtualGoodsStorage_t71 * VirtualGoodsStorage_get_instance_m552 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::RemoveUpgrades(Soomla.Store.VirtualGood)
extern "C" void VirtualGoodsStorage_RemoveUpgrades_m553 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::RemoveUpgrades(Soomla.Store.VirtualGood,System.Boolean)
extern "C" void VirtualGoodsStorage_RemoveUpgrades_m554 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::AssignCurrentUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG)
extern "C" void VirtualGoodsStorage_AssignCurrentUpgrade_m555 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___upgradeVG, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::AssignCurrentUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG,System.Boolean)
extern "C" void VirtualGoodsStorage_AssignCurrentUpgrade_m556 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___upgradeVG, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.UpgradeVG Soomla.Store.VirtualGoodsStorage::GetCurrentUpgrade(Soomla.Store.VirtualGood)
extern "C" UpgradeVG_t133 * VirtualGoodsStorage_GetCurrentUpgrade_m557 (Object_t * __this /* static, unused */, VirtualGood_t79 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.VirtualGoodsStorage::IsEquipped(Soomla.Store.EquippableVG)
extern "C" bool VirtualGoodsStorage_IsEquipped_m558 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::Equip(Soomla.Store.EquippableVG)
extern "C" void VirtualGoodsStorage_Equip_m559 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::Equip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorage_Equip_m560 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::UnEquip(Soomla.Store.EquippableVG)
extern "C" void VirtualGoodsStorage_UnEquip_m561 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::UnEquip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorage_UnEquip_m562 (Object_t * __this /* static, unused */, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::GetBalance(Soomla.Store.VirtualItem)
extern "C" int32_t VirtualGoodsStorage_GetBalance_m563 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::SetBalance(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualGoodsStorage_SetBalance_m564 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___balance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::SetBalance(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorage_SetBalance_m565 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::Add(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualGoodsStorage_Add_m566 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::Add(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorage_Add_m567 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::Remove(Soomla.Store.VirtualItem,System.Int32)
extern "C" int32_t VirtualGoodsStorage_Remove_m568 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGoodsStorage::Remove(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualGoodsStorage_Remove_m569 (Object_t * __this /* static, unused */, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::_removeUpgrades(Soomla.Store.VirtualGood,System.Boolean)
extern "C" void VirtualGoodsStorage__removeUpgrades_m570 (VirtualGoodsStorage_t71 * __this, VirtualGood_t79 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::_assignCurrentUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG,System.Boolean)
extern "C" void VirtualGoodsStorage__assignCurrentUpgrade_m571 (VirtualGoodsStorage_t71 * __this, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___upgradeVG, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.UpgradeVG Soomla.Store.VirtualGoodsStorage::_getCurrentUpgrade(Soomla.Store.VirtualGood)
extern "C" UpgradeVG_t133 * VirtualGoodsStorage__getCurrentUpgrade_m572 (VirtualGoodsStorage_t71 * __this, VirtualGood_t79 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.VirtualGoodsStorage::_isEquipped(Soomla.Store.EquippableVG)
extern "C" bool VirtualGoodsStorage__isEquipped_m573 (VirtualGoodsStorage_t71 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::_equip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorage__equip_m574 (VirtualGoodsStorage_t71 * __this, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGoodsStorage::_unequip(Soomla.Store.EquippableVG,System.Boolean)
extern "C" void VirtualGoodsStorage__unequip_m575 (VirtualGoodsStorage_t71 * __this, EquippableVG_t129 * ___good, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
