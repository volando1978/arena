﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
struct Func_2_t967;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t705;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.IntPtr,System.Object>
#include "System_Core_System_Func_2_gen_41MethodDeclarations.h"
#define Func_2__ctor_m3996(__this, ___object, ___method, method) (( void (*) (Func_2_t967 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20368_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::Invoke(T)
#define Func_2_Invoke_m20882(__this, ___arg1, method) (( SnapshotSelectUIResponse_t705 * (*) (Func_2_t967 *, IntPtr_t, MethodInfo*))Func_2_Invoke_m20370_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20883(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t967 *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20372_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20884(__this, ___result, method) (( SnapshotSelectUIResponse_t705 * (*) (Func_2_t967 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20374_gshared)(__this, ___result, method)
