﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t850  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t906  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t732  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t907  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t977  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t1662  : public Array_t
{
};
// UnityEngine.Matrix4x4[]
// UnityEngine.Matrix4x4[]
struct  Matrix4x4U5BU5D_t3830  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t3843  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t1284  : public Array_t
{
};
// UnityEngine.Sprite[]
// UnityEngine.Sprite[]
struct  SpriteU5BU5D_t824  : public Array_t
{
};
// UnityEngine.Touch[]
// UnityEngine.Touch[]
struct  TouchU5BU5D_t998  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t3882  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t1376  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t1380  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t3948  : public Array_t
{
};
struct FontU5BU5D_t3948_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t1264  : public Array_t
{
};
struct UIVertexU5BU5D_t1264_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t3976  : public Array_t
{
};
struct CanvasU5BU5D_t3976_StaticFields{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t1247  : public Array_t
{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t1670  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t1669  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t4020  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t4040  : public Array_t
{
};
struct RectTransformU5BU5D_t4040_StaticFields{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t1496  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t1497  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t4054  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t1663  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t1676  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t1664  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t1677  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t1665  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t4065  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t4071  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t4071_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t1533  : public Array_t
{
};
struct GUIStyleU5BU5D_t1533_StaticFields{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t1631  : public Array_t
{
};
struct CameraU5BU5D_t1631_StaticFields{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t4607  : public Array_t
{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t1560  : public Array_t
{
};
struct DisplayU5BU5D_t1560_StaticFields{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t4090  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t1668  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t1608  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t1609  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t1610  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t1630  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t4132  : public Array_t
{
};
struct EventU5BU5D_t4132_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t4133  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t4152  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t4158  : public Array_t
{
};
