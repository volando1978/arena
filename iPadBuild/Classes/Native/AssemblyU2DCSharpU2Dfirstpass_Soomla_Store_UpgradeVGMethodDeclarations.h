﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.UpgradeVG::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void UpgradeVG__ctor_m653 (UpgradeVG_t133 * __this, String_t* ___goodItemId, String_t* ___nextItemId, String_t* ___prevItemId, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.UpgradeVG::.ctor(JSONObject)
extern "C" void UpgradeVG__ctor_m654 (UpgradeVG_t133 * __this, JSONObject_t30 * ___jsonItem, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.UpgradeVG::.cctor()
extern "C" void UpgradeVG__cctor_m655 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.UpgradeVG::toJSONObject()
extern "C" JSONObject_t30 * UpgradeVG_toJSONObject_m656 (UpgradeVG_t133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.UpgradeVG::canBuy()
extern "C" bool UpgradeVG_canBuy_m657 (UpgradeVG_t133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.UpgradeVG::Give(System.Int32,System.Boolean)
extern "C" int32_t UpgradeVG_Give_m658 (UpgradeVG_t133 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.UpgradeVG::Take(System.Int32,System.Boolean)
extern "C" int32_t UpgradeVG_Take_m659 (UpgradeVG_t133 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
