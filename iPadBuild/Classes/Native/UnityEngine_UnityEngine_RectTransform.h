﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1413;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.RectTransform
struct  RectTransform_t1227  : public Transform_t809
{
};
struct RectTransform_t1227_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1413 * ___reapplyDrivenProperties_2;
};
