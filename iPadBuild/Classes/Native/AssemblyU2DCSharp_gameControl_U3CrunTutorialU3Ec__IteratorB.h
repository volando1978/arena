﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// gameControl
struct gameControl_t794;
// System.Object
#include "mscorlib_System_Object.h"
// gameControl/<runTutorial>c__IteratorB
struct  U3CrunTutorialU3Ec__IteratorB_t799  : public Object_t
{
	// System.Int32 gameControl/<runTutorial>c__IteratorB::$PC
	int32_t ___U24PC_0;
	// System.Object gameControl/<runTutorial>c__IteratorB::$current
	Object_t * ___U24current_1;
	// gameControl gameControl/<runTutorial>c__IteratorB::<>f__this
	gameControl_t794 * ___U3CU3Ef__this_2;
};
