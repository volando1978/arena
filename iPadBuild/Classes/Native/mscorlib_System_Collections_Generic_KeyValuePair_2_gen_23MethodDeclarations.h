﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
struct KeyValuePair_2_t3997;
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m23414(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3997 *, Graphic_t1233 *, int32_t, MethodInfo*))KeyValuePair_2__ctor_m17099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m23415(__this, method) (( Graphic_t1233 * (*) (KeyValuePair_2_t3997 *, MethodInfo*))KeyValuePair_2_get_Key_m17100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m23416(__this, ___value, method) (( void (*) (KeyValuePair_2_t3997 *, Graphic_t1233 *, MethodInfo*))KeyValuePair_2_set_Key_m17101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m23417(__this, method) (( int32_t (*) (KeyValuePair_2_t3997 *, MethodInfo*))KeyValuePair_2_get_Value_m17102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m23418(__this, ___value, method) (( void (*) (KeyValuePair_2_t3997 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m17103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m23419(__this, method) (( String_t* (*) (KeyValuePair_2_t3997 *, MethodInfo*))KeyValuePair_2_ToString_m17104_gshared)(__this, method)
