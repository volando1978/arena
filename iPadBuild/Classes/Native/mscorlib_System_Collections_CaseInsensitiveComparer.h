﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t2138;
// System.Globalization.CultureInfo
struct CultureInfo_t232;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t2138  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t232 * ___culture_2;
};
struct CaseInsensitiveComparer_t2138_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t2138 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t2138 * ___defaultInvariantComparer_1;
};
