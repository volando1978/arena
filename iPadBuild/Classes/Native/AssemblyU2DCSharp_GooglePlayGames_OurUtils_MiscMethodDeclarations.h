﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.OurUtils.Misc
struct Misc_t391;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Boolean GooglePlayGames.OurUtils.Misc::BuffersAreIdentical(System.Byte[],System.Byte[])
extern "C" bool Misc_BuffersAreIdentical_m1596 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___a, ByteU5BU5D_t350* ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.OurUtils.Misc::GetSubsetBytes(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t350* Misc_GetSubsetBytes_m1597 (Object_t * __this /* static, unused */, ByteU5BU5D_t350* ___array, int32_t ___offset, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
