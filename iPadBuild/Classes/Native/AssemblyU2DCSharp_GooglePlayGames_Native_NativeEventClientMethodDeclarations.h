﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeEventClient
struct NativeEventClient_t549;
// GooglePlayGames.Native.PInvoke.EventManager
struct EventManager_t548;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>
struct Action_2_t544;
// System.String
struct String_t;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>
struct Action_2_t546;
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"

// System.Void GooglePlayGames.Native.NativeEventClient::.ctor(GooglePlayGames.Native.PInvoke.EventManager)
extern "C" void NativeEventClient__ctor_m2295 (NativeEventClient_t549 * __this, EventManager_t548 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient::FetchAllEvents(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>)
extern "C" void NativeEventClient_FetchAllEvents_m2296 (NativeEventClient_t549 * __this, int32_t ___source, Action_2_t544 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient::FetchEvent(GooglePlayGames.BasicApi.DataSource,System.String,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Events.IEvent>)
extern "C" void NativeEventClient_FetchEvent_m2297 (NativeEventClient_t549 * __this, int32_t ___source, String_t* ___eventId, Action_2_t546 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEventClient::IncrementEvent(System.String,System.UInt32)
extern "C" void NativeEventClient_IncrementEvent_m2298 (NativeEventClient_t549 * __this, String_t* ___eventId, uint32_t ___stepsToIncrement, MethodInfo* method) IL2CPP_METHOD_ATTR;
