﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
struct Func_2_t972;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t706;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.IntPtr,System.Object>
#include "System_Core_System_Func_2_gen_41MethodDeclarations.h"
#define Func_2__ctor_m4005(__this, ___object, ___method, method) (( void (*) (Func_2_t972 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20368_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>::Invoke(T)
#define Func_2_Invoke_m20900(__this, ___arg1, method) (( MatchInboxUIResponse_t706 * (*) (Func_2_t972 *, IntPtr_t, MethodInfo*))Func_2_Invoke_m20370_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20901(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t972 *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20372_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20902(__this, ___result, method) (( MatchInboxUIResponse_t706 * (*) (Func_2_t972 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20374_gshared)(__this, ___result, method)
