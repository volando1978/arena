﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.LifetimeVG
struct LifetimeVG_t130;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.LifetimeVG::.ctor(System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void LifetimeVG__ctor_m631 (LifetimeVG_t130 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.LifetimeVG::.ctor(JSONObject)
extern "C" void LifetimeVG__ctor_m632 (LifetimeVG_t130 * __this, JSONObject_t30 * ___jsonVg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.LifetimeVG::.cctor()
extern "C" void LifetimeVG__cctor_m633 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.LifetimeVG::toJSONObject()
extern "C" JSONObject_t30 * LifetimeVG_toJSONObject_m634 (LifetimeVG_t130 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.LifetimeVG::Give(System.Int32,System.Boolean)
extern "C" int32_t LifetimeVG_Give_m635 (LifetimeVG_t130 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.LifetimeVG::Take(System.Int32,System.Boolean)
extern "C" int32_t LifetimeVG_Take_m636 (LifetimeVG_t130 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.LifetimeVG::canBuy()
extern "C" bool LifetimeVG_canBuy_m637 (LifetimeVG_t130 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
