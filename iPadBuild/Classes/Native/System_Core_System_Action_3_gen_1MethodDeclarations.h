﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`3<Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_3_t97;
// System.Object
struct Object_t;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`3<Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
// System.Action`3<System.Object,System.Object,System.Object>
#include "System_Core_System_Action_3_gen_7MethodDeclarations.h"
#define Action_3__ctor_m966(__this, ___object, ___method, method) (( void (*) (Action_3_t97 *, Object_t *, IntPtr_t, MethodInfo*))Action_3__ctor_m17301_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`3<Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m17302(__this, ___arg1, ___arg2, ___arg3, method) (( void (*) (Action_3_t97 *, PurchasableVirtualItem_t124 *, String_t*, Dictionary_2_t165 *, MethodInfo*))Action_3_Invoke_m17303_gshared)(__this, ___arg1, ___arg2, ___arg3, method)
// System.IAsyncResult System.Action`3<Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m17304(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method) (( Object_t * (*) (Action_3_t97 *, PurchasableVirtualItem_t124 *, String_t*, Dictionary_2_t165 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_3_BeginInvoke_m17305_gshared)(__this, ___arg1, ___arg2, ___arg3, ___callback, ___object, method)
// System.Void System.Action`3<Soomla.Store.PurchasableVirtualItem,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m17306(__this, ___result, method) (( void (*) (Action_3_t97 *, Object_t *, MethodInfo*))Action_3_EndInvoke_m17307_gshared)(__this, ___result, method)
