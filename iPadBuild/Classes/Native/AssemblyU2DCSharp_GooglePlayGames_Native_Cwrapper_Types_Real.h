﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
struct  RealTimeRoomStatus_t520 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus::value__
	int32_t ___value___1;
};
