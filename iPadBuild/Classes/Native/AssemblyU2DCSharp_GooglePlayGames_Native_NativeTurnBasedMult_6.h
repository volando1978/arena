﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch
struct TurnBasedMatch_t353;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch>
struct Action_1_t641;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54
struct  U3CFindEqualVersionMatchU3Ec__AnonStorey54_t642  : public Object_t
{
	// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::match
	TurnBasedMatch_t353 * ___match_0;
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::onFailure
	Action_1_t98 * ___onFailure_1;
	// System.Action`1<GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch> GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<FindEqualVersionMatch>c__AnonStorey54::onVersionMatch
	Action_1_t641 * ___onVersionMatch_2;
};
