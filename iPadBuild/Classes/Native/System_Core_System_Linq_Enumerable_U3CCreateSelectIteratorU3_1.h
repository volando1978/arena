﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t221;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Object
struct Object_t;
// System.Func`2<System.Object,System.IntPtr>
struct Func_2_t963;
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>
struct  U3CCreateSelectIteratorU3Ec__Iterator10_2_t3806  : public Object_t
{
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::source
	Object_t* ___source_0;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::<$s_70>__0
	Object_t* ___U3CU24s_70U3E__0_1;
	// TSource System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::<element>__1
	Object_t * ___U3CelementU3E__1_2;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::selector
	Func_2_t963 * ___selector_3;
	// System.Int32 System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::$PC
	int32_t ___U24PC_4;
	// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::$current
	IntPtr_t ___U24current_5;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::<$>source
	Object_t* ___U3CU24U3Esource_6;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.IntPtr>::<$>selector
	Func_2_t963 * ___U3CU24U3Eselector_7;
};
