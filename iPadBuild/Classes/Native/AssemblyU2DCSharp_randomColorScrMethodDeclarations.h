﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// randomColorScr
struct randomColorScr_t832;

// System.Void randomColorScr::.ctor()
extern "C" void randomColorScr__ctor_m3641 (randomColorScr_t832 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void randomColorScr::Start()
extern "C" void randomColorScr_Start_m3642 (randomColorScr_t832 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void randomColorScr::Update()
extern "C" void randomColorScr_Update_m3643 (randomColorScr_t832 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
