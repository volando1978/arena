﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>
struct InternalEnumerator_1_t4190;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25766_gshared (InternalEnumerator_1_t4190 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25766(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4190 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25766_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25767_gshared (InternalEnumerator_1_t4190 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25767(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4190 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25767_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25768_gshared (InternalEnumerator_1_t4190 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25768(__this, method) (( void (*) (InternalEnumerator_1_t4190 *, MethodInfo*))InternalEnumerator_1_Dispose_m25768_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25769_gshared (InternalEnumerator_1_t4190 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25769(__this, method) (( bool (*) (InternalEnumerator_1_t4190 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25769_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C" X509ChainStatus_t2034  InternalEnumerator_1_get_Current_m25770_gshared (InternalEnumerator_1_t4190 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25770(__this, method) (( X509ChainStatus_t2034  (*) (InternalEnumerator_1_t4190 *, MethodInfo*))InternalEnumerator_1_get_Current_m25770_gshared)(__this, method)
