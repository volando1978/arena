﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t1263;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Text>
struct  Comparison_1_t3958  : public MulticastDelegate_t22
{
};
