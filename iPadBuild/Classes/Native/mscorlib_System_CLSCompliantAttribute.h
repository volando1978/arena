﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_t1813  : public Attribute_t1546
{
	// System.Boolean System.CLSCompliantAttribute::is_compliant
	bool ___is_compliant_0;
};
