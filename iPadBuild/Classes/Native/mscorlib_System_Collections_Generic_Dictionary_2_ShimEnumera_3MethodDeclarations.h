﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t3931;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3920;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22380_gshared (ShimEnumerator_t3931 * __this, Dictionary_2_t3920 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m22380(__this, ___host, method) (( void (*) (ShimEnumerator_t3931 *, Dictionary_2_t3920 *, MethodInfo*))ShimEnumerator__ctor_m22380_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22381_gshared (ShimEnumerator_t3931 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m22381(__this, method) (( bool (*) (ShimEnumerator_t3931 *, MethodInfo*))ShimEnumerator_MoveNext_m22381_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m22382_gshared (ShimEnumerator_t3931 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m22382(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t3931 *, MethodInfo*))ShimEnumerator_get_Entry_m22382_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22383_gshared (ShimEnumerator_t3931 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m22383(__this, method) (( Object_t * (*) (ShimEnumerator_t3931 *, MethodInfo*))ShimEnumerator_get_Key_m22383_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22384_gshared (ShimEnumerator_t3931 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m22384(__this, method) (( Object_t * (*) (ShimEnumerator_t3931 *, MethodInfo*))ShimEnumerator_get_Value_m22384_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22385_gshared (ShimEnumerator_t3931 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m22385(__this, method) (( Object_t * (*) (ShimEnumerator_t3931 *, MethodInfo*))ShimEnumerator_get_Current_m22385_gshared)(__this, method)
