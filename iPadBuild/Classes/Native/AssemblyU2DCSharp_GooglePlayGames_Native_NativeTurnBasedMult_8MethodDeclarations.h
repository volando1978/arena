﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56
struct U3CFinishU3Ec__AnonStorey56_t645;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::.ctor()
extern "C" void U3CFinishU3Ec__AnonStorey56__ctor_m2561 (U3CFinishU3Ec__AnonStorey56_t645 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::<>m__60(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CFinishU3Ec__AnonStorey56_U3CU3Em__60_m2562 (U3CFinishU3Ec__AnonStorey56_t645 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Finish>c__AnonStorey56::<>m__69(GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse)
extern "C" void U3CFinishU3Ec__AnonStorey56_U3CU3Em__69_m2563 (U3CFinishU3Ec__AnonStorey56_t645 * __this, TurnBasedMatchResponse_t707 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
