﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// newWeaponRoomSrc/<waiting>c__Iterator12
struct U3CwaitingU3Ec__Iterator12_t823;
// System.Object
struct Object_t;

// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::.ctor()
extern "C" void U3CwaitingU3Ec__Iterator12__ctor_m3599 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CwaitingU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3600 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object newWeaponRoomSrc/<waiting>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CwaitingU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m3601 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean newWeaponRoomSrc/<waiting>c__Iterator12::MoveNext()
extern "C" bool U3CwaitingU3Ec__Iterator12_MoveNext_m3602 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Dispose()
extern "C" void U3CwaitingU3Ec__Iterator12_Dispose_m3603 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void newWeaponRoomSrc/<waiting>c__Iterator12::Reset()
extern "C" void U3CwaitingU3Ec__Iterator12_Reset_m3604 (U3CwaitingU3Ec__Iterator12_t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
