﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t705;
// GooglePlayGames.Native.NativeSnapshotMetadata
struct NativeSnapshotMetadata_t611;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::.ctor(System.IntPtr)
extern "C" void SnapshotSelectUIResponse__ctor_m3040 (SnapshotSelectUIResponse_t705 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestStatus()
extern "C" int32_t SnapshotSelectUIResponse_RequestStatus_m3041 (SnapshotSelectUIResponse_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::RequestSucceeded()
extern "C" bool SnapshotSelectUIResponse_RequestSucceeded_m3042 (SnapshotSelectUIResponse_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeSnapshotMetadata GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::Data()
extern "C" NativeSnapshotMetadata_t611 * SnapshotSelectUIResponse_Data_m3043 (SnapshotSelectUIResponse_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void SnapshotSelectUIResponse_CallDispose_m3044 (SnapshotSelectUIResponse_t705 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse::FromPointer(System.IntPtr)
extern "C" SnapshotSelectUIResponse_t705 * SnapshotSelectUIResponse_FromPointer_m3045 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
