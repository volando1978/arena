﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t4103;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m24869_gshared (DefaultComparer_t4103 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m24869(__this, method) (( void (*) (DefaultComparer_t4103 *, MethodInfo*))DefaultComparer__ctor_m24869_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m24870_gshared (DefaultComparer_t4103 * __this, UICharInfo_t1400  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m24870(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4103 *, UICharInfo_t1400 , MethodInfo*))DefaultComparer_GetHashCode_m24870_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m24871_gshared (DefaultComparer_t4103 * __this, UICharInfo_t1400  ___x, UICharInfo_t1400  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m24871(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4103 *, UICharInfo_t1400 , UICharInfo_t1400 , MethodInfo*))DefaultComparer_Equals_m24871_gshared)(__this, ___x, ___y, method)
