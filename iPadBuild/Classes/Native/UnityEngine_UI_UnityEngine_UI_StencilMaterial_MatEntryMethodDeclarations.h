﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t1296;

// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern "C" void MatEntry__ctor_m5424 (MatEntry_t1296 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
