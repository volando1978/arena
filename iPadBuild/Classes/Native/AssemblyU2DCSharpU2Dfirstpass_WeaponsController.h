﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Boolean[]
struct BooleanU5BU5D_t108;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WeaponsController/WEAPONS
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsController_WEAPONS.h"
// WeaponsController
struct  WeaponsController_t109  : public MonoBehaviour_t26
{
};
struct WeaponsController_t109_StaticFields{
	// System.Int32[] WeaponsController::bullets
	Int32U5BU5D_t107* ___bullets_2;
	// System.Boolean[] WeaponsController::weaponPurchased
	BooleanU5BU5D_t108* ___weaponPurchased_3;
	// System.Boolean[] WeaponsController::weaponActivated
	BooleanU5BU5D_t108* ___weaponActivated_4;
	// System.Boolean[] WeaponsController::bombaPurchased
	BooleanU5BU5D_t108* ___bombaPurchased_5;
	// System.Boolean[] WeaponsController::bombaActivated
	BooleanU5BU5D_t108* ___bombaActivated_6;
	// System.Boolean WeaponsController::allWeaponsUnlocked
	bool ___allWeaponsUnlocked_7;
	// WeaponsController/WEAPONS WeaponsController::currentWeapon
	int32_t ___currentWeapon_8;
	// WeaponsController/WEAPONS WeaponsController::unlockedWeapons
	int32_t ___unlockedWeapons_9;
};
