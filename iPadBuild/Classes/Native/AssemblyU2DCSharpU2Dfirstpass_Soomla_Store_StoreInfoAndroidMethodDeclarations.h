﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreInfoAndroid
struct StoreInfoAndroid_t66;

// System.Void Soomla.Store.StoreInfoAndroid::.ctor()
extern "C" void StoreInfoAndroid__ctor_m256 (StoreInfoAndroid_t66 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
