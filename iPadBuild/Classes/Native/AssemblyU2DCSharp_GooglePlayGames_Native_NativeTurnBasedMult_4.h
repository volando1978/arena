﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52
struct  U3CRegisterMatchDelegateU3Ec__AnonStorey52_t639  : public Object_t
{
	// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<RegisterMatchDelegate>c__AnonStorey52::del
	MatchDelegate_t364 * ___del_0;
};
