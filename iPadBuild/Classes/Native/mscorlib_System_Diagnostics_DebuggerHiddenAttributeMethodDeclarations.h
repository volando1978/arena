﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t257;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C" void DebuggerHiddenAttribute__ctor_m1132 (DebuggerHiddenAttribute_t257 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
