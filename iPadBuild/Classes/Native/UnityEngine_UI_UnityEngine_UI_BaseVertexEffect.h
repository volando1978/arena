﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t1233;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1336  : public UIBehaviour_t1156
{
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::m_Graphic
	Graphic_t1233 * ___m_Graphic_2;
};
