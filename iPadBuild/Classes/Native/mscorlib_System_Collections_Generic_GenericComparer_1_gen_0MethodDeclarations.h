﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2913;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m14425_gshared (GenericComparer_1_t2913 * __this, MethodInfo* method);
#define GenericComparer_1__ctor_m14425(__this, method) (( void (*) (GenericComparer_1_t2913 *, MethodInfo*))GenericComparer_1__ctor_m14425_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m26119_gshared (GenericComparer_1_t2913 * __this, DateTimeOffset_t2793  ___x, DateTimeOffset_t2793  ___y, MethodInfo* method);
#define GenericComparer_1_Compare_m26119(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2913 *, DateTimeOffset_t2793 , DateTimeOffset_t2793 , MethodInfo*))GenericComparer_1_Compare_m26119_gshared)(__this, ___x, ___y, method)
