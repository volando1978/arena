﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.IntPtr,System.Object>
struct Func_2_t945;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.IntPtr,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_2__ctor_m20368_gshared (Func_2_t945 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Func_2__ctor_m20368(__this, ___object, ___method, method) (( void (*) (Func_2_t945 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20368_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.IntPtr,System.Object>::Invoke(T)
extern "C" Object_t * Func_2_Invoke_m20370_gshared (Func_2_t945 * __this, IntPtr_t ___arg1, MethodInfo* method);
#define Func_2_Invoke_m20370(__this, ___arg1, method) (( Object_t * (*) (Func_2_t945 *, IntPtr_t, MethodInfo*))Func_2_Invoke_m20370_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.IntPtr,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Func_2_BeginInvoke_m20372_gshared (Func_2_t945 * __this, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Func_2_BeginInvoke_m20372(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t945 *, IntPtr_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20372_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.IntPtr,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Func_2_EndInvoke_m20374_gshared (Func_2_t945 * __this, Object_t * ___result, MethodInfo* method);
#define Func_2_EndInvoke_m20374(__this, ___result, method) (( Object_t * (*) (Func_2_t945 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20374_gshared)(__this, ___result, method)
