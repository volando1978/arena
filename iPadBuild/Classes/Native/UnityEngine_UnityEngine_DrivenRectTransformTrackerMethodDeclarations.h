﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t1280;
// UnityEngine.Object
struct Object_t187;
struct Object_t187_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t1227;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m6051 (DrivenRectTransformTracker_t1280 * __this, Object_t187 * ___driver, RectTransform_t1227 * ___rectTransform, int32_t ___drivenProperties, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m6049 (DrivenRectTransformTracker_t1280 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
