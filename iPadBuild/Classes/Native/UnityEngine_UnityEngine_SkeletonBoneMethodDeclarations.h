﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t1597;
struct SkeletonBone_t1597_marshaled;

void SkeletonBone_t1597_marshal(const SkeletonBone_t1597& unmarshaled, SkeletonBone_t1597_marshaled& marshaled);
void SkeletonBone_t1597_marshal_back(const SkeletonBone_t1597_marshaled& marshaled, SkeletonBone_t1597& unmarshaled);
void SkeletonBone_t1597_marshal_cleanup(SkeletonBone_t1597_marshaled& marshaled);
