﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>
struct OutMethod_1_t942;
// System.Object
struct Object_t;
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C" void OutMethod_1__ctor_m3916_gshared (OutMethod_1_t942 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define OutMethod_1__ctor_m3916(__this, ___object, ___method, method) (( void (*) (OutMethod_1_t942 *, Object_t *, IntPtr_t, MethodInfo*))OutMethod_1__ctor_m3916_gshared)(__this, ___object, ___method, method)
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::Invoke(T[],System.UIntPtr)
extern "C" UIntPtr_t  OutMethod_1_Invoke_m20433_gshared (OutMethod_1_t942 * __this, IntPtrU5BU5D_t859* ___out_bytes, UIntPtr_t  ___out_size, MethodInfo* method);
#define OutMethod_1_Invoke_m20433(__this, ___out_bytes, ___out_size, method) (( UIntPtr_t  (*) (OutMethod_1_t942 *, IntPtrU5BU5D_t859*, UIntPtr_t , MethodInfo*))OutMethod_1_Invoke_m20433_gshared)(__this, ___out_bytes, ___out_size, method)
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OutMethod_1_BeginInvoke_m20434_gshared (OutMethod_1_t942 * __this, IntPtrU5BU5D_t859* ___out_bytes, UIntPtr_t  ___out_size, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define OutMethod_1_BeginInvoke_m20434(__this, ___out_bytes, ___out_size, ___callback, ___object, method) (( Object_t * (*) (OutMethod_1_t942 *, IntPtrU5BU5D_t859*, UIntPtr_t , AsyncCallback_t20 *, Object_t *, MethodInfo*))OutMethod_1_BeginInvoke_m20434_gshared)(__this, ___out_bytes, ___out_size, ___callback, ___object, method)
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C" UIntPtr_t  OutMethod_1_EndInvoke_m20435_gshared (OutMethod_1_t942 * __this, Object_t * ___result, MethodInfo* method);
#define OutMethod_1_EndInvoke_m20435(__this, ___result, method) (( UIntPtr_t  (*) (OutMethod_1_t942 *, Object_t *, MethodInfo*))OutMethod_1_EndInvoke_m20435_gshared)(__this, ___result, method)
