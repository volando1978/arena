﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// GooglePlayGames.BasicApi.Nearby.InitializationStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Initializa.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct  Action_1_t361  : public MulticastDelegate_t22
{
};
