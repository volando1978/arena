﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ObsoleteAttribute
struct ObsoleteAttribute_t259;
// System.String
struct String_t;

// System.Void System.ObsoleteAttribute::.ctor()
extern "C" void ObsoleteAttribute__ctor_m8946 (ObsoleteAttribute_t259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
extern "C" void ObsoleteAttribute__ctor_m1159 (ObsoleteAttribute_t259 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
extern "C" void ObsoleteAttribute__ctor_m6216 (ObsoleteAttribute_t259 * __this, String_t* ___message, bool ___error, MethodInfo* method) IL2CPP_METHOD_ATTR;
