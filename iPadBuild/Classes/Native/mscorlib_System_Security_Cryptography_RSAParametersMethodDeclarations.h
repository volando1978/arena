﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t2147;
struct RSAParameters_t2147_marshaled;

void RSAParameters_t2147_marshal(const RSAParameters_t2147& unmarshaled, RSAParameters_t2147_marshaled& marshaled);
void RSAParameters_t2147_marshal_back(const RSAParameters_t2147_marshaled& marshaled, RSAParameters_t2147& unmarshaled);
void RSAParameters_t2147_marshal_cleanup(RSAParameters_t2147_marshaled& marshaled);
