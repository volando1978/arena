﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_2_t25;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Object,System.Object>
#include "System_Core_System_Action_2_gen_17MethodDeclarations.h"
#define Action_2__ctor_m852(__this, ___object, ___method, method) (( void (*) (Action_2_t25 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m16118_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::Invoke(T1,T2)
#define Action_2_Invoke_m16119(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t25 *, String_t*, Dictionary_2_t165 *, MethodInfo*))Action_2_Invoke_m16120_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m16121(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t25 *, String_t*, Dictionary_2_t165 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m16122_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m16123(__this, ___result, method) (( void (*) (Action_2_t25 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m16124_gshared)(__this, ___result, method)
