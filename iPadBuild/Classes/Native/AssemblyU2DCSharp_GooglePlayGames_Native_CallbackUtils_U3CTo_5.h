﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Int32,System.Object>
struct Action_2_t916;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Int32,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey16_2_t3726  : public Object_t
{
	// System.Action`2<T1,T2> GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Int32,System.Object>::toConvert
	Action_2_t916 * ___toConvert_0;
};
