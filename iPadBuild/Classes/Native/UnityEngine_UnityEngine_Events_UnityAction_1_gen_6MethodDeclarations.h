﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t1334;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t1366;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_8MethodDeclarations.h"
#define UnityAction_1__ctor_m6170(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t1334 *, Object_t *, IntPtr_t, MethodInfo*))UnityAction_1__ctor_m21684_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Invoke(T0)
#define UnityAction_1_Invoke_m24095(__this, ___arg0, method) (( void (*) (UnityAction_1_t1334 *, List_1_t1366 *, MethodInfo*))UnityAction_1_Invoke_m21685_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m24096(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t1334 *, List_1_t1366 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))UnityAction_1_BeginInvoke_m21686_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m24097(__this, ___result, method) (( void (*) (UnityAction_1_t1334 *, Object_t *, MethodInfo*))UnityAction_1_EndInvoke_m21687_gshared)(__this, ___result, method)
