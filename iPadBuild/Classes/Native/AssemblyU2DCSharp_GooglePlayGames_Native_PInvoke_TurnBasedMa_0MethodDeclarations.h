﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse
struct TurnBasedMatchResponse_t707;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::.ctor(System.IntPtr)
extern "C" void TurnBasedMatchResponse__ctor_m3064 (TurnBasedMatchResponse_t707 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::ResponseStatus()
extern "C" int32_t TurnBasedMatchResponse_ResponseStatus_m3065 (TurnBasedMatchResponse_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::RequestSucceeded()
extern "C" bool TurnBasedMatchResponse_RequestSucceeded_m3066 (TurnBasedMatchResponse_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::Match()
extern "C" NativeTurnBasedMatch_t680 * TurnBasedMatchResponse_Match_m3067 (TurnBasedMatchResponse_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void TurnBasedMatchResponse_CallDispose_m3068 (TurnBasedMatchResponse_t707 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse::FromPointer(System.IntPtr)
extern "C" TurnBasedMatchResponse_t707 * TurnBasedMatchResponse_FromPointer_m3069 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
