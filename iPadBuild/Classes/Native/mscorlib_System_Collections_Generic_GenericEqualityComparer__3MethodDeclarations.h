﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>
struct GenericEqualityComparer_1_t3507;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m16625_gshared (GenericEqualityComparer_1_t3507 * __this, MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m16625(__this, method) (( void (*) (GenericEqualityComparer_1_t3507 *, MethodInfo*))GenericEqualityComparer_1__ctor_m16625_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m16626_gshared (GenericEqualityComparer_1_t3507 * __this, uint16_t ___obj, MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m16626(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3507 *, uint16_t, MethodInfo*))GenericEqualityComparer_1_GetHashCode_m16626_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt16>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m16627_gshared (GenericEqualityComparer_1_t3507 * __this, uint16_t ___x, uint16_t ___y, MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m16627(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3507 *, uint16_t, uint16_t, MethodInfo*))GenericEqualityComparer_1_Equals_m16627_gshared)(__this, ___x, ___y, method)
