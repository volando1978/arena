﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreSettings
struct StoreSettings_t74;
// System.String
struct String_t;

// System.Void Soomla.Store.StoreSettings::.ctor()
extern "C" void StoreSettings__ctor_m265 (StoreSettings_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::.cctor()
extern "C" void StoreSettings__cctor_m266 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Soomla.Store.StoreSettings::get_AndroidPublicKey()
extern "C" String_t* StoreSettings_get_AndroidPublicKey_m267 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_AndroidPublicKey(System.String)
extern "C" void StoreSettings_set_AndroidPublicKey_m268 (Object_t * __this /* static, unused */, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreSettings::get_AndroidTestPurchases()
extern "C" bool StoreSettings_get_AndroidTestPurchases_m269 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_AndroidTestPurchases(System.Boolean)
extern "C" void StoreSettings_set_AndroidTestPurchases_m270 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreSettings::get_IosSSV()
extern "C" bool StoreSettings_get_IosSSV_m271 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_IosSSV(System.Boolean)
extern "C" void StoreSettings_set_IosSSV_m272 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreSettings::get_NoneBP()
extern "C" bool StoreSettings_get_NoneBP_m273 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_NoneBP(System.Boolean)
extern "C" void StoreSettings_set_NoneBP_m274 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreSettings::get_GPlayBP()
extern "C" bool StoreSettings_get_GPlayBP_m275 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_GPlayBP(System.Boolean)
extern "C" void StoreSettings_set_GPlayBP_m276 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreSettings::get_AmazonBP()
extern "C" bool StoreSettings_get_AmazonBP_m277 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreSettings::set_AmazonBP(System.Boolean)
extern "C" void StoreSettings_set_AmazonBP_m278 (Object_t * __this /* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
