﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>
struct Func_2_t699;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.IntPtr>
#include "System_Core_System_Func_2_gen_44MethodDeclarations.h"
#define Func_2__ctor_m3983(__this, ___object, ___method, method) (( void (*) (Func_2_t699 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20701_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::Invoke(T)
#define Func_2_Invoke_m20702(__this, ___arg1, method) (( IntPtr_t (*) (Func_2_t699 *, MultiplayerParticipant_t672 *, MethodInfo*))Func_2_Invoke_m20703_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20704(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t699 *, MultiplayerParticipant_t672 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20705_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.IntPtr>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20706(__this, ___result, method) (( IntPtr_t (*) (Func_2_t699 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20707_gshared)(__this, ___result, method)
