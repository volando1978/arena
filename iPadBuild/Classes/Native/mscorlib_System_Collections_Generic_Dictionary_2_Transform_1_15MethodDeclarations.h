﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>,System.Collections.DictionaryEntry>
struct Transform_1_t3617;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_1MethodDeclarations.h"
#define Transform_1__ctor_m18278(__this, ___object, ___method, method) (( void (*) (Transform_1_t3617 *, Object_t *, IntPtr_t, MethodInfo*))Transform_1__ctor_m16126_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m18279(__this, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3617 *, String_t*, List_1_t178 *, MethodInfo*))Transform_1_Invoke_m16128_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m18280(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3617 *, String_t*, List_1_t178 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Transform_1_BeginInvoke_m16130_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m18281(__this, ___result, method) (( DictionaryEntry_t2128  (*) (Transform_1_t3617 *, Object_t *, MethodInfo*))Transform_1_EndInvoke_m16132_gshared)(__this, ___result, method)
