﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// number
struct number_t825;

// System.Void number::.ctor()
extern "C" void number__ctor_m3614 (number_t825 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
