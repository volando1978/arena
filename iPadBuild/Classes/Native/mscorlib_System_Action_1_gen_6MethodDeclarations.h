﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t530;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Int32>
#include "mscorlib_System_Action_1_gen_64MethodDeclarations.h"
#define Action_1__ctor_m18643(__this, ___object, ___method, method) (( void (*) (Action_1_t530 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m18539_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(T)
#define Action_1_Invoke_m18644(__this, ___obj, method) (( void (*) (Action_1_t530 *, int32_t, MethodInfo*))Action_1_Invoke_m18541_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.UIStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m18645(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t530 *, int32_t, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m18543_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m18646(__this, ___result, method) (( void (*) (Action_1_t530 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m18545_gshared)(__this, ___result, method)
