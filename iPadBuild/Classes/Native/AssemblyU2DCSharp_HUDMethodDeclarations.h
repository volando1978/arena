﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// HUD
struct HUD_t733;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void HUD::.ctor()
extern "C" void HUD__ctor_m3190 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD::.cctor()
extern "C" void HUD__cctor_m3191 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD::Start()
extern "C" void HUD_Start_m3192 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD::setSizes()
extern "C" void HUD_setSizes_m3193 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD::Update()
extern "C" void HUD_Update_m3194 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HUD::run()
extern "C" Object_t * HUD_run_m3195 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HUD::runOut()
extern "C" Object_t * HUD_runOut_m3196 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HUD::OnGUI()
extern "C" void HUD_OnGUI_m3197 (HUD_t733 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
