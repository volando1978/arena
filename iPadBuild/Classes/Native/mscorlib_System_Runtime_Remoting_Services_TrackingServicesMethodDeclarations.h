﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Services.TrackingServices
struct TrackingServices_t2628;
// System.Object
struct Object_t;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t2632;

// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
extern "C" void TrackingServices__cctor_m12697 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern "C" void TrackingServices_NotifyUnmarshaledObject_m12698 (Object_t * __this /* static, unused */, Object_t * ___obj, ObjRef_t2632 * ___or, MethodInfo* method) IL2CPP_METHOD_ATTR;
