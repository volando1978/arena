﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct Enumerator_t3470;
// System.Object
struct Object_t;
// UnityEngine.UnityKeyValuePair`2<System.String,System.String>
struct UnityKeyValuePair_2_t164;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>
struct List_1_t163;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m16002(__this, ___l, method) (( void (*) (Enumerator_t3470 *, List_1_t163 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16003(__this, method) (( Object_t * (*) (Enumerator_t3470 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::Dispose()
#define Enumerator_Dispose_m16004(__this, method) (( void (*) (Enumerator_t3470 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::VerifyState()
#define Enumerator_VerifyState_m16005(__this, method) (( void (*) (Enumerator_t3470 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::MoveNext()
#define Enumerator_MoveNext_m16006(__this, method) (( bool (*) (Enumerator_t3470 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UnityKeyValuePair`2<System.String,System.String>>::get_Current()
#define Enumerator_get_Current_m16007(__this, method) (( UnityKeyValuePair_2_t164 * (*) (Enumerator_t3470 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
