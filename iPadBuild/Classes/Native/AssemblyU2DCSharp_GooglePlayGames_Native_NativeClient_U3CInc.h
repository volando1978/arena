﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Boolean>
struct Action_1_t98;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20
struct  U3CIncrementAchievementU3Ec__AnonStorey20_t529  : public Object_t
{
	// System.Action`1<System.Boolean> GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::callback
	Action_1_t98 * ___callback_0;
	// System.String GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::achId
	String_t* ___achId_1;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<IncrementAchievement>c__AnonStorey20::<>f__this
	NativeClient_t524 * ___U3CU3Ef__this_2;
};
