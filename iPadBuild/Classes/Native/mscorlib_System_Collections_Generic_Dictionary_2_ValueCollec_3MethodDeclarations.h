﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Enumerator_t927;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>
struct Dictionary_2_t576;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"
#define Enumerator__ctor_m20298(__this, ___host, method) (( void (*) (Enumerator_t927 *, Dictionary_2_t576 *, MethodInfo*))Enumerator__ctor_m16310_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20299(__this, method) (( Object_t * (*) (Enumerator_t927 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::Dispose()
#define Enumerator_Dispose_m20300(__this, method) (( void (*) (Enumerator_t927 *, MethodInfo*))Enumerator_Dispose_m16312_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::MoveNext()
#define Enumerator_MoveNext_m3827(__this, method) (( bool (*) (Enumerator_t927 *, MethodInfo*))Enumerator_MoveNext_m16313_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Multiplayer.Participant>::get_Current()
#define Enumerator_get_Current_m3826(__this, method) (( Participant_t340 * (*) (Enumerator_t927 *, MethodInfo*))Enumerator_get_Current_m16314_gshared)(__this, method)
