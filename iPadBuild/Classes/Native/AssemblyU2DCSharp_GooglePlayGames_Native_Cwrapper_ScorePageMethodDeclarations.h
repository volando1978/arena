﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.ScorePage
struct ScorePage_t470;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ScorePage_ScorePage_Dispose_m2042 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_TimeSpan(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t ScorePage_ScorePage_TimeSpan_m2043 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_LeaderboardId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  ScorePage_ScorePage_LeaderboardId_m2044 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Collection(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t ScorePage_ScorePage_Collection_m2045 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Start(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t ScorePage_ScorePage_Start_m2046 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScorePage_ScorePage_Valid_m2047 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasPreviousScorePage(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScorePage_ScorePage_HasPreviousScorePage_m2048 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_HasNextScorePage(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScorePage_ScorePage_HasNextScorePage_m2049 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_PreviousScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t ScorePage_ScorePage_PreviousScorePageToken_m2050 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_NextScorePageToken(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t ScorePage_ScorePage_NextScorePageToken_m2051 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  ScorePage_ScorePage_Entries_Length_m2052 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entries_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t ScorePage_ScorePage_Entries_GetElement_m2053 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ScorePage_ScorePage_Entry_Dispose_m2054 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_PlayerId(System.Runtime.InteropServices.HandleRef,System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  ScorePage_ScorePage_Entry_PlayerId_m2055 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, StringBuilder_t36 * ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModified(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t ScorePage_ScorePage_Entry_LastModified_m2056 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Score(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t ScorePage_ScorePage_Entry_Score_m2057 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScorePage_ScorePage_Entry_Valid_m2058 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_Entry_LastModifiedTime(System.Runtime.InteropServices.HandleRef)
extern "C" uint64_t ScorePage_ScorePage_Entry_LastModifiedTime_m2059 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Valid(System.Runtime.InteropServices.HandleRef)
extern "C" bool ScorePage_ScorePage_ScorePageToken_Valid_m2060 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.ScorePage::ScorePage_ScorePageToken_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void ScorePage_ScorePage_ScorePageToken_Dispose_m2061 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
