﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>
struct Dictionary_2_t1392;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t1360;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.UI.ICanvasElement,System.Int32>
struct KeyCollection_t4002;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.ICanvasElement,System.Int32>
struct ValueCollection_t4003;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.ICanvasElement>
struct IEqualityComparer_1_t3946;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4443;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>>
struct IEnumerator_1_t4444;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__24.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_24MethodDeclarations.h"
#define Dictionary_2__ctor_m22637(__this, method) (( void (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2__ctor_m17004_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m22638(__this, ___comparer, method) (( void (*) (Dictionary_2_t1392 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m17006_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Int32)
#define Dictionary_2__ctor_m22639(__this, ___capacity, method) (( void (*) (Dictionary_2_t1392 *, int32_t, MethodInfo*))Dictionary_2__ctor_m17007_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m22640(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1392 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m17009_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m22641(__this, method) (( Object_t * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m17011_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22642(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17013_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22643(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17015_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m22644(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17017_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m22645(__this, ___key, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17019_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22646(__this, method) (( bool (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17021_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22647(__this, method) (( Object_t * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22648(__this, method) (( bool (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17025_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22649(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1392 *, KeyValuePair_2_t4001 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17027_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22650(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1392 *, KeyValuePair_2_t4001 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17029_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22651(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1392 *, KeyValuePair_2U5BU5D_t4443*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17031_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22652(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1392 *, KeyValuePair_2_t4001 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17033_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22653(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1392 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17035_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22654(__this, method) (( Object_t * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17037_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22655(__this, method) (( Object_t* (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17039_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22656(__this, method) (( Object_t * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Count()
#define Dictionary_2_get_Count_m22657(__this, method) (( int32_t (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_get_Count_m17043_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Item(TKey)
#define Dictionary_2_get_Item_m22658(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_get_Item_m17045_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m22659(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, int32_t, MethodInfo*))Dictionary_2_set_Item_m17047_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m22660(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1392 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m17049_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m22661(__this, ___size, method) (( void (*) (Dictionary_2_t1392 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m17051_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m22662(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1392 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m17053_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m22663(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t4001  (*) (Object_t * /* static, unused */, Object_t *, int32_t, MethodInfo*))Dictionary_2_make_pair_m17055_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m22664(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, MethodInfo*))Dictionary_2_pick_key_m17057_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m22665(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, MethodInfo*))Dictionary_2_pick_value_m17059_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m22666(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1392 *, KeyValuePair_2U5BU5D_t4443*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m17061_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::Resize()
#define Dictionary_2_Resize_m22667(__this, method) (( void (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_Resize_m17063_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::Add(TKey,TValue)
#define Dictionary_2_Add_m22668(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, int32_t, MethodInfo*))Dictionary_2_Add_m17065_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::Clear()
#define Dictionary_2_Clear_m22669(__this, method) (( void (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_Clear_m17067_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m22670(__this, ___key, method) (( bool (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_ContainsKey_m17069_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m22671(__this, ___value, method) (( bool (*) (Dictionary_2_t1392 *, int32_t, MethodInfo*))Dictionary_2_ContainsValue_m17071_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m22672(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1392 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m17073_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m22673(__this, ___sender, method) (( void (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m17075_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::Remove(TKey)
#define Dictionary_2_Remove_m22674(__this, ___key, method) (( bool (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_Remove_m17077_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m22675(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1392 *, Object_t *, int32_t*, MethodInfo*))Dictionary_2_TryGetValue_m17079_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Keys()
#define Dictionary_2_get_Keys_m22676(__this, method) (( KeyCollection_t4002 * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_get_Keys_m17081_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Values()
#define Dictionary_2_get_Values_m22677(__this, method) (( ValueCollection_t4003 * (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_get_Values_m17083_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m22678(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m17085_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m22679(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1392 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m17087_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m22680(__this, ___pair, method) (( bool (*) (Dictionary_2_t1392 *, KeyValuePair_2_t4001 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17089_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m22681(__this, method) (( Enumerator_t4004  (*) (Dictionary_2_t1392 *, MethodInfo*))Dictionary_2_GetEnumerator_m17091_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m22682(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, Object_t *, int32_t, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17093_gshared)(__this /* static, unused */, ___key, ___value, method)
