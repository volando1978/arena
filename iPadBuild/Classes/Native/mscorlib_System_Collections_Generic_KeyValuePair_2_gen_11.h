﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>
struct List_1_t178;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>
struct  KeyValuePair_2_t3618 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Soomla.Store.UpgradeVG>>::value
	List_1_t178 * ___value_1;
};
