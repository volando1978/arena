﻿#pragma once
#include <stdint.h>
// UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate
struct BannerWasClickedDelegate_t1571;
// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate
struct BannerWasLoadedDelegate_t974;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.iOS.ADBannerView/Layout
#include "UnityEngine_UnityEngine_iOS_ADBannerView_Layout.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.iOS.ADBannerView
struct  ADBannerView_t717  : public Object_t
{
	// UnityEngine.iOS.ADBannerView/Layout UnityEngine.iOS.ADBannerView::_layout
	int32_t ____layout_0;
	// System.IntPtr UnityEngine.iOS.ADBannerView::_bannerView
	IntPtr_t ____bannerView_1;
};
struct ADBannerView_t717_StaticFields{
	// System.Boolean UnityEngine.iOS.ADBannerView::_AlwaysFalseDummy
	bool ____AlwaysFalseDummy_2;
	// UnityEngine.iOS.ADBannerView/BannerWasClickedDelegate UnityEngine.iOS.ADBannerView::onBannerWasClicked
	BannerWasClickedDelegate_t1571 * ___onBannerWasClicked_3;
	// UnityEngine.iOS.ADBannerView/BannerWasLoadedDelegate UnityEngine.iOS.ADBannerView::onBannerWasLoaded
	BannerWasLoadedDelegate_t974 * ___onBannerWasLoaded_4;
};
