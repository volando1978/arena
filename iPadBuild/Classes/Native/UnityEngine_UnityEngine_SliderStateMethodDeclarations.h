﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SliderState
struct SliderState_t1638;

// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m7387 (SliderState_t1638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
