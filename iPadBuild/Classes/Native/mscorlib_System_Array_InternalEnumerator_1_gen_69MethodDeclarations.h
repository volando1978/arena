﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t4204;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25829_gshared (InternalEnumerator_1_t4204 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25829(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4204 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25829_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25830_gshared (InternalEnumerator_1_t4204 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25830(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4204 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25830_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25831_gshared (InternalEnumerator_1_t4204 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25831(__this, method) (( void (*) (InternalEnumerator_1_t4204 *, MethodInfo*))InternalEnumerator_1_Dispose_m25831_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25832_gshared (InternalEnumerator_1_t4204 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25832(__this, method) (( bool (*) (InternalEnumerator_1_t4204 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25832_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m25833_gshared (InternalEnumerator_1_t4204 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25833(__this, method) (( int8_t (*) (InternalEnumerator_1_t4204 *, MethodInfo*))InternalEnumerator_1_get_Current_m25833_gshared)(__this, method)
