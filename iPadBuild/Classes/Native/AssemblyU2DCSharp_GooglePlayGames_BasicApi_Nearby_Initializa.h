﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Nearby.InitializationStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Initializa.h"
// GooglePlayGames.BasicApi.Nearby.InitializationStatus
struct  InitializationStatus_t360 
{
	// System.Int32 GooglePlayGames.BasicApi.Nearby.InitializationStatus::value__
	int32_t ___value___1;
};
