﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.NetworkReachability
#include "UnityEngine_UnityEngine_NetworkReachability.h"
// UnityEngine.NetworkReachability
struct  NetworkReachability_t1554 
{
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___1;
};
