﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback
struct OnDataReceivedCallback_t456;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnDataReceivedCallback__ctor_m1943 (OnDataReceivedCallback_t456 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::Invoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr)
extern "C" void OnDataReceivedCallback_Invoke_m1944 (OnDataReceivedCallback_t456 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnDataReceivedCallback_t456(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr,System.UIntPtr,System.Boolean,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnDataReceivedCallback_BeginInvoke_m1945 (OnDataReceivedCallback_t456 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, IntPtr_t ___arg2, UIntPtr_t  ___arg3, bool ___arg4, IntPtr_t ___arg5, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.RealTimeEventListenerHelper/OnDataReceivedCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnDataReceivedCallback_EndInvoke_m1946 (OnDataReceivedCallback_t456 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
