﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0.h"
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
struct  QuestMilestoneState_t517 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState::value__
	int32_t ___value___1;
};
