﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>
struct DefaultComparer_t4184;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::.ctor()
extern "C" void DefaultComparer__ctor_m25708_gshared (DefaultComparer_t4184 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m25708(__this, method) (( void (*) (DefaultComparer_t4184 *, MethodInfo*))DefaultComparer__ctor_m25708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25709_gshared (DefaultComparer_t4184 * __this, uint8_t ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m25709(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4184 *, uint8_t, MethodInfo*))DefaultComparer_GetHashCode_m25709_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25710_gshared (DefaultComparer_t4184 * __this, uint8_t ___x, uint8_t ___y, MethodInfo* method);
#define DefaultComparer_Equals_m25710(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4184 *, uint8_t, uint8_t, MethodInfo*))DefaultComparer_Equals_m25710_gshared)(__this, ___x, ___y, method)
