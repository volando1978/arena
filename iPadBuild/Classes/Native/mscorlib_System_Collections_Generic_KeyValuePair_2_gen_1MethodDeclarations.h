﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct KeyValuePair_2_t1373;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1191;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22390(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1373 *, int32_t, PointerEventData_t1191 *, MethodInfo*))KeyValuePair_2__ctor_m22308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
#define KeyValuePair_2_get_Key_m5797(__this, method) (( int32_t (*) (KeyValuePair_2_t1373 *, MethodInfo*))KeyValuePair_2_get_Key_m22309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22391(__this, ___value, method) (( void (*) (KeyValuePair_2_t1373 *, int32_t, MethodInfo*))KeyValuePair_2_set_Key_m22310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
#define KeyValuePair_2_get_Value_m5796(__this, method) (( PointerEventData_t1191 * (*) (KeyValuePair_2_t1373 *, MethodInfo*))KeyValuePair_2_get_Value_m22311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22392(__this, ___value, method) (( void (*) (KeyValuePair_2_t1373 *, PointerEventData_t1191 *, MethodInfo*))KeyValuePair_2_set_Value_m22312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
#define KeyValuePair_2_ToString_m5820(__this, method) (( String_t* (*) (KeyValuePair_2_t1373 *, MethodInfo*))KeyValuePair_2_ToString_m22313_gshared)(__this, method)
