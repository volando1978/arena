﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ArgIterator
struct ArgIterator_t2351;
// System.Object
struct Object_t;

// System.Boolean System.ArgIterator::Equals(System.Object)
extern "C" bool ArgIterator_Equals_m10728 (ArgIterator_t2351 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ArgIterator::GetHashCode()
extern "C" int32_t ArgIterator_GetHashCode_m10729 (ArgIterator_t2351 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
