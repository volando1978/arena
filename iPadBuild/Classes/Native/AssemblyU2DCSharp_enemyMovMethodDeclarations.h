﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// enemyMov
struct enemyMov_t790;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void enemyMov::.ctor()
extern "C" void enemyMov__ctor_m3401 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyMov::Start()
extern "C" void enemyMov_Start_m3402 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyMov::setSpeed()
extern "C" void enemyMov_setSpeed_m3403 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyMov::Update()
extern "C" void enemyMov_Update_m3404 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyMov::resetMovement()
extern "C" void enemyMov_resetMovement_m3405 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemyMov::move()
extern "C" void enemyMov_move_m3406 (enemyMov_t790 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single enemyMov::calculateAngle(UnityEngine.Vector2)
extern "C" float enemyMov_calculateAngle_m3407 (enemyMov_t790 * __this, Vector2_t739  ___target, MethodInfo* method) IL2CPP_METHOD_ATTR;
