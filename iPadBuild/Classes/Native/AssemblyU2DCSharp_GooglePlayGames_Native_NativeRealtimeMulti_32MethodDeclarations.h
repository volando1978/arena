﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36
struct U3CAcceptInvitationU3Ec__AnonStorey36_t606;
// GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse
struct FetchInvitationsResponse_t698;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36::.ctor()
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey36__ctor_m2473 (U3CAcceptInvitationU3Ec__AnonStorey36_t606 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<AcceptInvitation>c__AnonStorey36::<>m__2A(GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse)
extern "C" void U3CAcceptInvitationU3Ec__AnonStorey36_U3CU3Em__2A_m2474 (U3CAcceptInvitationU3Ec__AnonStorey36_t606 * __this, FetchInvitationsResponse_t698 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
