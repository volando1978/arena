﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey16_2_t3708;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Object,System.Object>::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19613_gshared (U3CToOnGameThreadU3Ec__AnonStorey16_2_t3708 * __this, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19613(__this, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey16_2_t3708 *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey16_2__ctor_m19613_gshared)(__this, method)
// System.Void GooglePlayGames.Native.CallbackUtils/<ToOnGameThread>c__AnonStorey16`2<System.Object,System.Object>::<>m__7(T1,T2)
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19614_gshared (U3CToOnGameThreadU3Ec__AnonStorey16_2_t3708 * __this, Object_t * ___val1, Object_t * ___val2, MethodInfo* method);
#define U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19614(__this, ___val1, ___val2, method) (( void (*) (U3CToOnGameThreadU3Ec__AnonStorey16_2_t3708 *, Object_t *, Object_t *, MethodInfo*))U3CToOnGameThreadU3Ec__AnonStorey16_2_U3CU3Em__7_m19614_gshared)(__this, ___val1, ___val2, method)
