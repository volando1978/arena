﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestMilestone
struct NativeQuestMilestone_t676;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Text.StringBuilder
struct StringBuilder_t36;
// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneS.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativeQuestMilestone::.ctor(System.IntPtr)
extern "C" void NativeQuestMilestone__ctor_m2780 (NativeQuestMilestone_t676 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_Id()
extern "C" String_t* NativeQuestMilestone_get_Id_m2781 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_EventId()
extern "C" String_t* NativeQuestMilestone_get_EventId_m2782 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuestMilestone::get_QuestId()
extern "C" String_t* NativeQuestMilestone_get_QuestId_m2783 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_CurrentCount()
extern "C" uint64_t NativeQuestMilestone_get_CurrentCount_m2784 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.NativeQuestMilestone::get_TargetCount()
extern "C" uint64_t NativeQuestMilestone_get_TargetCount_m2785 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] GooglePlayGames.Native.NativeQuestMilestone::get_CompletionRewardData()
extern "C" ByteU5BU5D_t350* NativeQuestMilestone_get_CompletionRewardData_m2786 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.MilestoneState GooglePlayGames.Native.NativeQuestMilestone::get_State()
extern "C" int32_t NativeQuestMilestone_get_State_m2787 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeQuestMilestone::Valid()
extern "C" bool NativeQuestMilestone_Valid_m2788 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestMilestone::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeQuestMilestone_CallDispose_m2789 (NativeQuestMilestone_t676 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeQuestMilestone::ToString()
extern "C" String_t* NativeQuestMilestone_ToString_m2790 (NativeQuestMilestone_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeQuestMilestone GooglePlayGames.Native.NativeQuestMilestone::FromPointer(System.IntPtr)
extern "C" NativeQuestMilestone_t676 * NativeQuestMilestone_FromPointer_m2791 (Object_t * __this /* static, unused */, IntPtr_t ___pointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_Id>m__8A(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_IdU3Em__8A_m2792 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_EventId>m__8B(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_EventIdU3Em__8B_m2793 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_QuestId>m__8C(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_QuestIdU3Em__8C_m2794 (NativeQuestMilestone_t676 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeQuestMilestone::<get_CompletionRewardData>m__8D(System.Byte[],System.UIntPtr)
extern "C" UIntPtr_t  NativeQuestMilestone_U3Cget_CompletionRewardDataU3Em__8D_m2795 (NativeQuestMilestone_t676 * __this, ByteU5BU5D_t350* ___out_bytes, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
