﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// rotateItself
struct  rotateItself_t833  : public MonoBehaviour_t26
{
	// UnityEngine.Vector3 rotateItself::dir
	Vector3_t758  ___dir_2;
};
