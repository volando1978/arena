﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback
struct TurnBasedMatchCallback_t496;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TurnBasedMatchCallback__ctor_m2179 (TurnBasedMatchCallback_t496 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedMatchCallback_Invoke_m2180 (TurnBasedMatchCallback_t496 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_TurnBasedMatchCallback_t496(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * TurnBasedMatchCallback_BeginInvoke_m2181 (TurnBasedMatchCallback_t496 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.TurnBasedMultiplayerManager/TurnBasedMatchCallback::EndInvoke(System.IAsyncResult)
extern "C" void TurnBasedMatchCallback_EndInvoke_m2182 (TurnBasedMatchCallback_t496 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
