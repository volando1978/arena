﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>
struct Action_1_t846;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m19218_gshared (Action_1_t846 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m19218(__this, ___object, ___method, method) (( void (*) (Action_1_t846 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m19218_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::Invoke(T)
extern "C" void Action_1_Invoke_m19219_gshared (Action_1_t846 * __this, ConnectionRequest_t355  ___obj, MethodInfo* method);
#define Action_1_Invoke_m19219(__this, ___obj, method) (( void (*) (Action_1_t846 *, ConnectionRequest_t355 , MethodInfo*))Action_1_Invoke_m19219_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m19220_gshared (Action_1_t846 * __this, ConnectionRequest_t355  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m19220(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t846 *, ConnectionRequest_t355 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m19220_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m19221_gshared (Action_1_t846 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m19221(__this, ___result, method) (( void (*) (Action_1_t846 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m19221_gshared)(__this, ___result, method)
