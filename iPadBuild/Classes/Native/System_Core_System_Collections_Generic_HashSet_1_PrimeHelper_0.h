﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>
struct  PrimeHelper_t3765  : public Object_t
{
};
struct PrimeHelper_t3765_StaticFields{
	// System.Int32[] System.Collections.Generic.HashSet`1/PrimeHelper<System.Int32>::primes_table
	Int32U5BU5D_t107* ___primes_table_0;
};
