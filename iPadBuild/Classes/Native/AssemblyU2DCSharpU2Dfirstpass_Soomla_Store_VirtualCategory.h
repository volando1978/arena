﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.VirtualCategory
struct  VirtualCategory_t126  : public Object_t
{
	// System.String Soomla.Store.VirtualCategory::Name
	String_t* ___Name_1;
	// System.Collections.Generic.List`1<System.String> Soomla.Store.VirtualCategory::GoodItemIds
	List_1_t43 * ___GoodItemIds_2;
};
