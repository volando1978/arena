﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t230;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Component>
struct  Comparison_1_t3887  : public MulticastDelegate_t22
{
};
