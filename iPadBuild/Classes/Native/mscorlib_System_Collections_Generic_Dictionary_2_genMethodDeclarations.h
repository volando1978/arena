﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t165;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.String>
struct KeyCollection_t3479;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.String>
struct ValueCollection_t3480;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t4255;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerator_1_t4256;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m911(__this, method) (( void (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16212(__this, ___comparer, method) (( void (*) (Dictionary_2_t165 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16214(__this, ___capacity, method) (( void (*) (Dictionary_2_t165 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16216(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t165 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m16218(__this, method) (( Object_t * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16220(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t165 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16222(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t165 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16224(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t165 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16226(__this, ___key, method) (( void (*) (Dictionary_2_t165 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16228(__this, method) (( bool (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16230(__this, method) (( Object_t * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16232(__this, method) (( bool (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16234(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t165 *, KeyValuePair_2_t196 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16236(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t165 *, KeyValuePair_2_t196 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16238(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t165 *, KeyValuePair_2U5BU5D_t4255*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16240(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t165 *, KeyValuePair_2_t196 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16242(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t165 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16244(__this, method) (( Object_t * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16246(__this, method) (( Object_t* (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.String>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16248(__this, method) (( Object_t * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count()
#define Dictionary_2_get_Count_m16250(__this, method) (( int32_t (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(TKey)
#define Dictionary_2_get_Item_m16252(__this, ___key, method) (( String_t* (*) (Dictionary_2_t165 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16254(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t165 *, String_t*, String_t*, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16256(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t165 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16258(__this, ___size, method) (( void (*) (Dictionary_2_t165 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16260(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t165 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16262(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t196  (*) (Object_t * /* static, unused */, String_t*, String_t*, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.String>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m16264(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, String_t*, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m16266(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, String_t*, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m16268(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t165 *, KeyValuePair_2U5BU5D_t4255*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Resize()
#define Dictionary_2_Resize_m16270(__this, method) (( void (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(TKey,TValue)
#define Dictionary_2_Add_m16272(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t165 *, String_t*, String_t*, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear()
#define Dictionary_2_Clear_m16274(__this, method) (( void (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m16276(__this, ___key, method) (( bool (*) (Dictionary_2_t165 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m16278(__this, ___value, method) (( bool (*) (Dictionary_2_t165 *, String_t*, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m16280(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t165 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m16282(__this, ___sender, method) (( void (*) (Dictionary_2_t165 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::Remove(TKey)
#define Dictionary_2_Remove_m16284(__this, ___key, method) (( bool (*) (Dictionary_2_t165 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m16286(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t165 *, String_t*, String_t**, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Keys()
#define Dictionary_2_get_Keys_m16288(__this, method) (( KeyCollection_t3479 * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Values()
#define Dictionary_2_get_Values_m16290(__this, method) (( ValueCollection_t3480 * (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.String>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m16292(__this, ___key, method) (( String_t* (*) (Dictionary_2_t165 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.String>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m16294(__this, ___value, method) (( String_t* (*) (Dictionary_2_t165 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m16296(__this, ___pair, method) (( bool (*) (Dictionary_2_t165 *, KeyValuePair_2_t196 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m884(__this, method) (( Enumerator_t197  (*) (Dictionary_2_t165 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.String>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m16299(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, String_t*, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
