﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>
struct InternalEnumerator_1_t3657;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18952_gshared (InternalEnumerator_1_t3657 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m18952(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3657 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m18952_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18953_gshared (InternalEnumerator_1_t3657 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18953(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3657 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18953_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18954_gshared (InternalEnumerator_1_t3657 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18954(__this, method) (( void (*) (InternalEnumerator_1_t3657 *, MethodInfo*))InternalEnumerator_1_Dispose_m18954_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18955_gshared (InternalEnumerator_1_t3657 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18955(__this, method) (( bool (*) (InternalEnumerator_1_t3657 *, MethodInfo*))InternalEnumerator_1_MoveNext_m18955_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>>::get_Current()
extern "C" KeyValuePair_2_t3656  InternalEnumerator_1_get_Current_m18956_gshared (InternalEnumerator_1_t3657 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18956(__this, method) (( KeyValuePair_2_t3656  (*) (InternalEnumerator_1_t3657 *, MethodInfo*))InternalEnumerator_1_get_Current_m18956_gshared)(__this, method)
