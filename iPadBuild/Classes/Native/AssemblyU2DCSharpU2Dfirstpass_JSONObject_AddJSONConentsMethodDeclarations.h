﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// JSONObject/AddJSONConents
struct AddJSONConents_t31;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void JSONObject/AddJSONConents::.ctor(System.Object,System.IntPtr)
extern "C" void AddJSONConents__ctor_m62 (AddJSONConents_t31 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/AddJSONConents::Invoke(JSONObject)
extern "C" void AddJSONConents_Invoke_m63 (AddJSONConents_t31 * __this, JSONObject_t30 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddJSONConents_t31(Il2CppObject* delegate, JSONObject_t30 * ___self);
// System.IAsyncResult JSONObject/AddJSONConents::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C" Object_t * AddJSONConents_BeginInvoke_m64 (AddJSONConents_t31 * __this, JSONObject_t30 * ___self, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/AddJSONConents::EndInvoke(System.IAsyncResult)
extern "C" void AddJSONConents_EndInvoke_m65 (AddJSONConents_t31 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
