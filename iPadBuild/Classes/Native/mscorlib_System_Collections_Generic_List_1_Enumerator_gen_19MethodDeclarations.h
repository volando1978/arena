﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>
struct Enumerator_t3638;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t333;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Achievement>
struct List_1_t841;

// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m18633(__this, ___l, method) (( void (*) (Enumerator_t3638 *, List_1_t841 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18634(__this, method) (( Object_t * (*) (Enumerator_t3638 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::Dispose()
#define Enumerator_Dispose_m18635(__this, method) (( void (*) (Enumerator_t3638 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::VerifyState()
#define Enumerator_VerifyState_m18636(__this, method) (( void (*) (Enumerator_t3638 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::MoveNext()
#define Enumerator_MoveNext_m18637(__this, method) (( bool (*) (Enumerator_t3638 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.BasicApi.Achievement>::get_Current()
#define Enumerator_get_Current_m18638(__this, method) (( Achievement_t333 * (*) (Enumerator_t3638 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
