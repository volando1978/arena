﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t720;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct  Predicate_1_t1303  : public MulticastDelegate_t22
{
};
