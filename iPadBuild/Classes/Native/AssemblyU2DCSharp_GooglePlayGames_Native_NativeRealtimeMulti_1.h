﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B
struct  U3CRoomSetupProgressU3Ec__AnonStorey3B_t567  : public Object_t
{
	// System.Single GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::percent
	float ___percent_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<RoomSetupProgress>c__AnonStorey3B::<>f__this
	OnGameThreadForwardingListener_t563 * ___U3CU3Ef__this_1;
};
