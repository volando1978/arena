﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D
struct U3CCreateQuickGameU3Ec__AnonStorey2D_t591;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B
struct U3CCreateQuickGameU3Ec__AnonStorey2B_t593;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient
struct NativeRealtimeMultiplayerClient_t536;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C
struct  U3CCreateQuickGameU3Ec__AnonStorey2C_t595  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::helper
	RealTimeEventListenerHelper_t594 * ___helper_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2D GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::<>f__ref$45
	U3CCreateQuickGameU3Ec__AnonStorey2D_t591 * ___U3CU3Ef__refU2445_1;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2B GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::<>f__ref$43
	U3CCreateQuickGameU3Ec__AnonStorey2B_t593 * ___U3CU3Ef__refU2443_2;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<CreateQuickGame>c__AnonStorey2C::<>f__this
	NativeRealtimeMultiplayerClient_t536 * ___U3CU3Ef__this_3;
};
