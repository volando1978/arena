﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<JSONObject>
struct IList_1_t3494;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<JSONObject>
struct  ReadOnlyCollection_1_t3495  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<JSONObject>::list
	Object_t* ___list_0;
};
