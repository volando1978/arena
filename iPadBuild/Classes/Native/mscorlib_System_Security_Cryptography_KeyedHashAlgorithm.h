﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t2234  : public HashAlgorithm_t2210
{
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t350* ___KeyValue_4;
};
