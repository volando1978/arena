﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Object>
struct Action_1_t3422;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>::toInvokeOnGameThread
	Action_1_t3422 * ___toInvokeOnGameThread_0;
};
