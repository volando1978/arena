﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E
struct  U3CHelperForSessionU3Ec__AnonStorey2E_t596  : public Object_t
{
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession GooglePlayGames.Native.NativeRealtimeMultiplayerClient/<HelperForSession>c__AnonStorey2E::session
	RoomSession_t566 * ___session_0;
};
