﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderExceptionFallback
struct EncoderExceptionFallback_t2742;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2745;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderExceptionFallback::.ctor()
extern "C" void EncoderExceptionFallback__ctor_m13323 (EncoderExceptionFallback_t2742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderExceptionFallback::CreateFallbackBuffer()
extern "C" EncoderFallbackBuffer_t2745 * EncoderExceptionFallback_CreateFallbackBuffer_m13324 (EncoderExceptionFallback_t2742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderExceptionFallback::Equals(System.Object)
extern "C" bool EncoderExceptionFallback_Equals_m13325 (EncoderExceptionFallback_t2742 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderExceptionFallback::GetHashCode()
extern "C" int32_t EncoderExceptionFallback_GetHashCode_m13326 (EncoderExceptionFallback_t2742 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
