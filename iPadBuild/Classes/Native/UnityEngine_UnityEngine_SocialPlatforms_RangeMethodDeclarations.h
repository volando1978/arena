﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.Range
struct Range_t1628;

// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C" void Range__ctor_m7385 (Range_t1628 * __this, int32_t ___fromValue, int32_t ___valueCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
