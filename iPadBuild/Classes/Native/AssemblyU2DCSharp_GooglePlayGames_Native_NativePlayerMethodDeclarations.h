﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativePlayer
struct NativePlayer_t675;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.Player
struct Player_t347;
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativePlayer::.ctor(System.IntPtr)
extern "C" void NativePlayer__ctor_m2751 (NativePlayer_t675 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativePlayer::Id()
extern "C" String_t* NativePlayer_Id_m2752 (NativePlayer_t675 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativePlayer::Name()
extern "C" String_t* NativePlayer_Name_m2753 (NativePlayer_t675 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativePlayer::AvatarURL()
extern "C" String_t* NativePlayer_AvatarURL_m2754 (NativePlayer_t675 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativePlayer::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativePlayer_CallDispose_m2755 (NativePlayer_t675 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Player GooglePlayGames.Native.NativePlayer::AsPlayer()
extern "C" Player_t347 * NativePlayer_AsPlayer_m2756 (NativePlayer_t675 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<Id>m__82(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CIdU3Em__82_m2757 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<Name>m__83(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CNameU3Em__83_m2758 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativePlayer::<AvatarURL>m__84(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativePlayer_U3CAvatarURLU3Em__84_m2759 (NativePlayer_t675 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
