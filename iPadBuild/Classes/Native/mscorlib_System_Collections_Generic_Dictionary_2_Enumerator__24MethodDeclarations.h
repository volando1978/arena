﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>
struct Enumerator_t4004;
// System.Object
struct Object_t;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t1360;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.ICanvasElement,System.Int32>
struct Dictionary_2_t1392;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m23498(__this, ___dictionary, method) (( void (*) (Enumerator_t4004 *, Dictionary_2_t1392 *, MethodInfo*))Enumerator__ctor_m17124_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23499(__this, method) (( Object_t * (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17125_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m23500(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17126_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m23501(__this, method) (( Object_t * (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17127_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m23502(__this, method) (( Object_t * (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17128_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m23503(__this, method) (( bool (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_MoveNext_m17129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m23504(__this, method) (( KeyValuePair_2_t4001  (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_get_Current_m17130_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m23505(__this, method) (( Object_t * (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_get_CurrentKey_m17131_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m23506(__this, method) (( int32_t (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_get_CurrentValue_m17132_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m23507(__this, method) (( void (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_VerifyState_m17133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m23508(__this, method) (( void (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_VerifyCurrent_m17134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m23509(__this, method) (( void (*) (Enumerator_t4004 *, MethodInfo*))Enumerator_Dispose_m17135_gshared)(__this, method)
