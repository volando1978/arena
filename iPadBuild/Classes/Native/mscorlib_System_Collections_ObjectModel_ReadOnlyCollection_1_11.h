﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Soomla.Store.VirtualCurrency>
struct IList_1_t3590;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualCurrency>
struct  ReadOnlyCollection_1_t3591  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.VirtualCurrency>::list
	Object_t* ___list_0;
};
