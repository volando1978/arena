﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1996;
// System.Net.WebRequest
struct WebRequest_t1990;
// System.Uri
struct Uri_t1986;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m7968 (HttpRequestCreator_t1996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1990 * HttpRequestCreator_Create_m7969 (HttpRequestCreator_t1996 * __this, Uri_t1986 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
