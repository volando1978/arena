﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1975;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t1973;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C" void KeysCollection__ctor_m7912 (KeysCollection_t1975 * __this, NameObjectCollectionBase_t1973 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m7913 (KeysCollection_t1975 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeysCollection_System_Collections_ICollection_get_IsSynchronized_m7914 (KeysCollection_t1975 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeysCollection_System_Collections_ICollection_get_SyncRoot_m7915 (KeysCollection_t1975 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::get_Count()
extern "C" int32_t KeysCollection_get_Count_m7916 (KeysCollection_t1975 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::GetEnumerator()
extern "C" Object_t * KeysCollection_GetEnumerator_m7917 (KeysCollection_t1975 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
