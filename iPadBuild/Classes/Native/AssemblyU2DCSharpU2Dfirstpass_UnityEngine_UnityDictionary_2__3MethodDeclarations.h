﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2/<ContainsKey>c__AnonStorey6<System.Object,System.Object>
struct U3CContainsKeyU3Ec__AnonStorey6_t3453;
// UnityEngine.UnityKeyValuePair`2<System.Object,System.Object>
struct UnityKeyValuePair_2_t3412;

// System.Void UnityEngine.UnityDictionary`2/<ContainsKey>c__AnonStorey6<System.Object,System.Object>::.ctor()
extern "C" void U3CContainsKeyU3Ec__AnonStorey6__ctor_m15773_gshared (U3CContainsKeyU3Ec__AnonStorey6_t3453 * __this, MethodInfo* method);
#define U3CContainsKeyU3Ec__AnonStorey6__ctor_m15773(__this, method) (( void (*) (U3CContainsKeyU3Ec__AnonStorey6_t3453 *, MethodInfo*))U3CContainsKeyU3Ec__AnonStorey6__ctor_m15773_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2/<ContainsKey>c__AnonStorey6<System.Object,System.Object>::<>m__5(UnityEngine.UnityKeyValuePair`2<K,V>)
extern "C" bool U3CContainsKeyU3Ec__AnonStorey6_U3CU3Em__5_m15774_gshared (U3CContainsKeyU3Ec__AnonStorey6_t3453 * __this, UnityKeyValuePair_2_t3412 * ___x, MethodInfo* method);
#define U3CContainsKeyU3Ec__AnonStorey6_U3CU3Em__5_m15774(__this, ___x, method) (( bool (*) (U3CContainsKeyU3Ec__AnonStorey6_t3453 *, UnityKeyValuePair_2_t3412 *, MethodInfo*))U3CContainsKeyU3Ec__AnonStorey6_U3CU3Em__5_m15774_gshared)(__this, ___x, method)
