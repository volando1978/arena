﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.BaseReferenceHolder
struct BaseReferenceHolder_t654;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct  Action_2_t3777  : public MulticastDelegate_t22
{
};
