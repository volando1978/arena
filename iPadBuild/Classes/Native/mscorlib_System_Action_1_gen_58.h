﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Soomla.Store.VirtualItem>
struct  Action_1_t3568  : public MulticastDelegate_t22
{
};
