﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t3447;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m15753_gshared (Predicate_1_t3447 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Predicate_1__ctor_m15753(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3447 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m15753_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m15754_gshared (Predicate_1_t3447 * __this, KeyValuePair_2_t3407  ___obj, MethodInfo* method);
#define Predicate_1_Invoke_m15754(__this, ___obj, method) (( bool (*) (Predicate_1_t3447 *, KeyValuePair_2_t3407 , MethodInfo*))Predicate_1_Invoke_m15754_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m15755_gshared (Predicate_1_t3447 * __this, KeyValuePair_2_t3407  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Predicate_1_BeginInvoke_m15755(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3447 *, KeyValuePair_2_t3407 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m15755_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m15756_gshared (Predicate_1_t3447 * __this, Object_t * ___result, MethodInfo* method);
#define Predicate_1_EndInvoke_m15756(__this, ___result, method) (( bool (*) (Predicate_1_t3447 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m15756_gshared)(__this, ___result, method)
