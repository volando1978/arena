﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t1275;

// System.Void UnityEngine.UI.Scrollbar/ScrollEvent::.ctor()
extern "C" void ScrollEvent__ctor_m5209 (ScrollEvent_t1275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
