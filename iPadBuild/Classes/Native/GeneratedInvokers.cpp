﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t260 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t203 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
void* RuntimeInvoker_Char_t193 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
void* RuntimeInvoker_UInt16_t194 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t235 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
void* RuntimeInvoker_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
void* RuntimeInvoker_UInt64_t239 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// WeaponsController/WEAPONS
#include "AssemblyU2DCSharpU2Dfirstpass_WeaponsController_WEAPONS.h"
void* RuntimeInvoker_WEAPONS_t105 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
void* RuntimeInvoker_DebugLevel_t142 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Min.h"
void* RuntimeInvoker_TOKEN_t146 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Events.EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_EventVisib.h"
void* RuntimeInvoker_EventVisibility_t338 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Invitation/InvType
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Invit.h"
void* RuntimeInvoker_InvType_t339 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Parti.h"
void* RuntimeInvoker_ParticipantStatus_t346 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchTurnStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB_0.h"
void* RuntimeInvoker_MatchTurnStatus_t349 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_TurnB.h"
void* RuntimeInvoker_MatchStatus_t348 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
void* RuntimeInvoker_ResponseStatus_t335 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Nearby.EndpointDetails
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_EndpointDe.h"
void* RuntimeInvoker_EndpointDetails_t356 (MethodInfo* method, void* obj, void** args)
{
	typedef EndpointDetails_t356  (*Func)(void* obj, MethodInfo* method);
	EndpointDetails_t356  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse/Status
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_0.h"
void* RuntimeInvoker_Status_t357 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGamesClientCo_0.h"
void* RuntimeInvoker_PlayGamesClientConfiguration_t366 (MethodInfo* method, void* obj, void** args)
{
	typedef PlayGamesClientConfiguration_t366  (*Func)(void* obj, MethodInfo* method);
	PlayGamesClientConfiguration_t366  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Quests.QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestState.h"
void* RuntimeInvoker_QuestState_t367 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Quests.MilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_MilestoneS.h"
void* RuntimeInvoker_MilestoneState_t368 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"
void* RuntimeInvoker_SavedGameMetadataUpdate_t378 (MethodInfo* method, void* obj, void** args)
{
	typedef SavedGameMetadataUpdate_t378  (*Func)(void* obj, MethodInfo* method);
	SavedGameMetadataUpdate_t378  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t1020 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
void* RuntimeInvoker_ResponseStatus_t409 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
void* RuntimeInvoker_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef HandleRef_t657  (*Func)(void* obj, MethodInfo* method);
	HandleRef_t657  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerInvitationType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult_0.h"
void* RuntimeInvoker_MultiplayerInvitationType_t519 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Part.h"
void* RuntimeInvoker_ParticipantStatus_t513 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/AchievementState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi_0.h"
void* RuntimeInvoker_AchievementState_t507 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/AchievementType
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Achi.h"
void* RuntimeInvoker_AchievementType_t506 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"
void* RuntimeInvoker_RealTimeRoomStatus_t520 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/MatchStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc_0.h"
void* RuntimeInvoker_MatchStatus_t515 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
void* RuntimeInvoker_UIStatus_t412 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"
void* RuntimeInvoker_QuestClaimMilestoneStatus_t415 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"
void* RuntimeInvoker_QuestAcceptStatus_t414 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"
void* RuntimeInvoker_MultiplayerStatus_t413 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/SnapshotOpenStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_6.h"
void* RuntimeInvoker_SnapshotOpenStatus_t416 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Coin/COINTYPE
#include "AssemblyU2DCSharp_Coin_COINTYPE.h"
void* RuntimeInvoker_COINTYPE_t726 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
void* RuntimeInvoker_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
void* RuntimeInvoker_MoveDirection_t1183 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
void* RuntimeInvoker_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t1187  (*Func)(void* obj, MethodInfo* method);
	RaycastResult_t1187  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
void* RuntimeInvoker_InputButton_t1189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
void* RuntimeInvoker_InputMode_t1199 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_LayerMask_t1205 (MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t1205  (*Func)(void* obj, MethodInfo* method);
	LayerMask_t1205  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
void* RuntimeInvoker_ColorTweenMode_t1206 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
void* RuntimeInvoker_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t1221  (*Func)(void* obj, MethodInfo* method);
	ColorBlock_t1221  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
void* RuntimeInvoker_FontStyle_t1462 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
void* RuntimeInvoker_TextAnchor_t1410 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
void* RuntimeInvoker_HorizontalWrapMode_t1463 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
void* RuntimeInvoker_VerticalWrapMode_t1464 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t738  (*Func)(void* obj, MethodInfo* method);
	Rect_t738  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
void* RuntimeInvoker_BlockingObjects_t1234 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
void* RuntimeInvoker_Type_t1240 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
void* RuntimeInvoker_FillMethod_t1241 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
void* RuntimeInvoker_ContentType_t1250 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
void* RuntimeInvoker_LineType_t1253 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
void* RuntimeInvoker_InputType_t1251 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
void* RuntimeInvoker_TouchScreenKeyboardType_t1395 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
void* RuntimeInvoker_CharacterValidation_t1252 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
void* RuntimeInvoker_Mode_t1271 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
void* RuntimeInvoker_Navigation_t1272 (MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t1272  (*Func)(void* obj, MethodInfo* method);
	Navigation_t1272  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
void* RuntimeInvoker_Direction_t1274 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
void* RuntimeInvoker_Axis_t1277 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
void* RuntimeInvoker_MovementType_t1281 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Bounds_t979 (MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t979  (*Func)(void* obj, MethodInfo* method);
	Bounds_t979  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
void* RuntimeInvoker_Transition_t1286 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
void* RuntimeInvoker_SpriteState_t1290 (MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t1290  (*Func)(void* obj, MethodInfo* method);
	SpriteState_t1290  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
void* RuntimeInvoker_SelectionState_t1287 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
void* RuntimeInvoker_Direction_t1292 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
void* RuntimeInvoker_Axis_t1294 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
void* RuntimeInvoker_AspectMode_t1305 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
void* RuntimeInvoker_ScaleMode_t1307 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
void* RuntimeInvoker_ScreenMatchMode_t1308 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
void* RuntimeInvoker_Unit_t1309 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
void* RuntimeInvoker_FitMode_t1311 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
void* RuntimeInvoker_Corner_t1313 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
void* RuntimeInvoker_Axis_t1314 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
void* RuntimeInvoker_Constraint_t1315 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
void* RuntimeInvoker_ScreenOrientation_t1621 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t1537 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t1538 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t1536 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
void* RuntimeInvoker_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ThreadPriority
#include "UnityEngine_UnityEngine_ThreadPriority.h"
void* RuntimeInvoker_ThreadPriority_t1495 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
void* RuntimeInvoker_RuntimePlatform_t1491 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.NetworkReachability
#include "UnityEngine_UnityEngine_NetworkReachability.h"
void* RuntimeInvoker_NetworkReachability_t1554 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t1620 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t1619 (MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t1619  (*Func)(void* obj, MethodInfo* method);
	RenderBuffer_t1619  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
void* RuntimeInvoker_TouchPhase_t996 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.iOS.DeviceGeneration
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration.h"
void* RuntimeInvoker_DeviceGeneration_t995 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t1489 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t1589 (MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t1589  (*Func)(void* obj, MethodInfo* method);
	AnimatorStateInfo_t1589  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t1590 (MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t1590  (*Func)(void* obj, MethodInfo* method);
	AnimatorClipInfo_t1590  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
void* RuntimeInvoker_RenderMode_t1605 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t1635 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t1628 (MethodInfo* method, void* obj, void** args)
{
	typedef Range_t1628  (*Func)(void* obj, MethodInfo* method);
	Range_t1628  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t1636 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
void* RuntimeInvoker_PersistentListenerMode_t1645 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t1979 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t1984 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t2004 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
void* RuntimeInvoker_X509ChainStatusFlags_t2040 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t2047 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t2048 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t2052 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t2045 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t2068 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t2090 (MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t2090  (*Func)(void* obj, MethodInfo* method);
	Interval_t2090  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
void* RuntimeInvoker_Category_t2075 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t2071 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t2196 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t2223 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t2236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t2237 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t2239 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t2258 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t2256 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t2331 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t2271 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t2270 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t2284 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t2257 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t2147 (MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t2147  (*Func)(void* obj, MethodInfo* method);
	RSAParameters_t2147  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t2251 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t2843 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t2557 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t2536 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t2339 (MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t2339  (*Func)(void* obj, MethodInfo* method);
	RuntimeTypeHandle_t2339  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t2383 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
void* RuntimeInvoker_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t2531 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t2835 (MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2835  (*Func)(void* obj, MethodInfo* method);
	RuntimeMethodHandle_t2835  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t2537 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t2534 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_RuntimeFieldHandle_t2340 (MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t2340  (*Func)(void* obj, MethodInfo* method);
	RuntimeFieldHandle_t2340  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t2528 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t2532 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t2553 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t2549 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1674  (*Func)(void* obj, MethodInfo* method);
	StreamingContext_t1674  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t2661 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t2678 (MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t2678  (*Func)(void* obj, MethodInfo* method);
	SerializationEntry_t2678  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t2681 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t2684 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t2696 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t2795 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t2792 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t2832 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Guid_t2814 (MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t2814  (*Func)(void* obj, MethodInfo* method);
	Guid_t2814  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
void* RuntimeInvoker_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t901  (*Func)(void* obj, MethodInfo* method);
	Touch_t901  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
void* RuntimeInvoker_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t1378  (*Func)(void* obj, MethodInfo* method);
	RaycastHit2D_t1378  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
void* RuntimeInvoker_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t990  (*Func)(void* obj, MethodInfo* method);
	RaycastHit_t990  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t1265  (*Func)(void* obj, MethodInfo* method);
	UIVertex_t1265  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t1398  (*Func)(void* obj, MethodInfo* method);
	UILineInfo_t1398  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t1400  (*Func)(void* obj, MethodInfo* method);
	UICharInfo_t1400  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1617  (*Func)(void* obj, MethodInfo* method);
	GcAchievementData_t1617  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1618  (*Func)(void* obj, MethodInfo* method);
	GcScoreData_t1618  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t1591 (MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t1591  (*Func)(void* obj, MethodInfo* method);
	Keyframe_t1591  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2550  (*Func)(void* obj, MethodInfo* method);
	ParameterModifier_t2550  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1629  (*Func)(void* obj, MethodInfo* method);
	HitInfo_t1629  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t2034  (*Func)(void* obj, MethodInfo* method);
	X509ChainStatus_t2034  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t2083 (MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t2083  (*Func)(void* obj, MethodInfo* method);
	Mark_t2083  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t2118 (MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t2118  (*Func)(void* obj, MethodInfo* method);
	UriScheme_t2118  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t2360 (MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t2360  (*Func)(void* obj, MethodInfo* method);
	TableRange_t2360  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t2436 (MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t2436  (*Func)(void* obj, MethodInfo* method);
	Slot_t2436  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t2443 (MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t2443  (*Func)(void* obj, MethodInfo* method);
	Slot_t2443  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Nullable`1<System.DateTime>
#include "mscorlib_System_Nullable_1_gen_0.h"
void* RuntimeInvoker_Nullable_1_t873 (MethodInfo* method, void* obj, void** args)
{
	typedef Nullable_1_t873  (*Func)(void* obj, MethodInfo* method);
	Nullable_1_t873  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
void* RuntimeInvoker_Nullable_1_t377 (MethodInfo* method, void* obj, void** args)
{
	typedef Nullable_1_t377  (*Func)(void* obj, MethodInfo* method);
	Nullable_1_t377  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
void* RuntimeInvoker_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t3880 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3880  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3880  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
void* RuntimeInvoker_Enumerator_t3486 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3486  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_3.h"
void* RuntimeInvoker_Enumerator_t3485 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3485  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3485  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"
void* RuntimeInvoker_Enumerator_t3489 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3489  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3489  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"
void* RuntimeInvoker_Enumerator_t3414 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3414  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3414  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31.h"
void* RuntimeInvoker_Enumerator_t3929 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3929  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3929  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20.h"
void* RuntimeInvoker_Enumerator_t3925 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3925  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3925  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
void* RuntimeInvoker_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3921  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t3921  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"
void* RuntimeInvoker_Enumerator_t3440 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3440  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3440  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
void* RuntimeInvoker_Enumerator_t3535 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3535  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3535  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
void* RuntimeInvoker_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3531  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t3531  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_8.h"
void* RuntimeInvoker_Enumerator_t3534 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3534  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3534  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15.h"
void* RuntimeInvoker_Enumerator_t3538 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3538  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3538  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
void* RuntimeInvoker_Link_t3579 (MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3579  (*Func)(void* obj, MethodInfo* method);
	Link_t3579  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
void* RuntimeInvoker_Enumerator_t3661 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3661  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3661  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
void* RuntimeInvoker_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3656  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t3656  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16.h"
void* RuntimeInvoker_Enumerator_t3660 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3660  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3660  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.UInt32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_23.h"
void* RuntimeInvoker_Enumerator_t3664 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3664  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3664  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.HashSet`1/Link<System.Int32>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_1.h"
void* RuntimeInvoker_Link_t3760 (MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3760  (*Func)(void* obj, MethodInfo* method);
	Link_t3760  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
void* RuntimeInvoker_Enumerator_t3794 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3794  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3794  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
void* RuntimeInvoker_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3790  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t3790  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_22.h"
void* RuntimeInvoker_Enumerator_t3793 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3793  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3793  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
void* RuntimeInvoker_Enumerator_t3798 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3798  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3798  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.IntPtr>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"
void* RuntimeInvoker_Enumerator_t3808 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3808  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3808  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Matrix4x4>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"
void* RuntimeInvoker_Enumerator_t3832 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3832  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3832  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"
void* RuntimeInvoker_Enumerator_t3850 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3850  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3850  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"
void* RuntimeInvoker_Enumerator_t3890 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3890  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3890  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_25.h"
void* RuntimeInvoker_Enumerator_t3924 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3924  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3924  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_39.h"
void* RuntimeInvoker_Enumerator_t3967 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t3967  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t3967  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_49.h"
void* RuntimeInvoker_Enumerator_t4099 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t4099  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t4099  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"
void* RuntimeInvoker_Enumerator_t4109 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t4109  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t4109  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__28.h"
void* RuntimeInvoker_Enumerator_t4175 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t4175  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t4175  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
void* RuntimeInvoker_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t4171  (*Func)(void* obj, MethodInfo* method);
	KeyValuePair_2_t4171  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_35.h"
void* RuntimeInvoker_Enumerator_t4174 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t4174  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t4174  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_39.h"
void* RuntimeInvoker_Enumerator_t4178 (MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t4178  (*Func)(void* obj, MethodInfo* method);
	Enumerator_t4178  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t193_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt16_t194 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t193_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t193_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t193_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t193_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
void* RuntimeInvoker_Void_t260_InternalDrawTextureArgumentsU26_t1723 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, InternalDrawTextureArguments_t1503 * p1, MethodInfo* method);
	((Func)method->method)(obj, (InternalDrawTextureArguments_t1503 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ColorU26_t1465 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Color_t747 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Matrix4x4U26_t1726 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t997 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t997 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t739 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RectU26_t1724 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Rect_t738 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t260_SphericalHarmonicsL2U26_t1732 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t1547 * p1, MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t1547 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t758 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_QuaternionU26_t1728 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t771 * p1, MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t771 *)args[0], method);
	return NULL;
}

struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
void* RuntimeInvoker_Void_t260_StringU26_t319 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_EndpointDetails_t356 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, EndpointDetails_t356  p1, MethodInfo* method);
	((Func)method->method)(obj, *((EndpointDetails_t356 *)args[0]), method);
	return NULL;
}

struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_0.h"
void* RuntimeInvoker_Void_t260_Builder_t376 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Builder_t376  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Builder_t376 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_PlayGamesClientConfiguration_t366 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, PlayGamesClientConfiguration_t366  p1, MethodInfo* method);
	((Func)method->method)(obj, *((PlayGamesClientConfiguration_t366 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t1187  p1, MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_LayerMask_t1205 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t1205  p1, MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t1205 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Navigation_t1272 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t1272  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t1272 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t1221  p1, MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t1221 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SpriteState_t1290 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t1290  p1, MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t1290 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t1618  p1, MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t1618 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t997  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Bounds_t979 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t979  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t979 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t771  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Range_t1628 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t1628  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Range_t1628 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Interval_t2090 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t2090  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t2090 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RSAParameters_t2147 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t2147  p1, MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t2147 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Void_t260_DSAParameters_t2149 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t2149  p1, MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t2149 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1674  p1, MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1674 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t260_MonoEnumInfo_t2806 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2806  p1, MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2806 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
void* RuntimeInvoker_Void_t260_ColorTween_t1209 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t1209  p1, MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t1209 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t2128  p1, MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t2128 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Touch_t901  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Touch_t901 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t1378  p1, MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t1378 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t990  p1, MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t990 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t1398  p1, MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t1400  p1, MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t1617  p1, MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t1617 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Keyframe_t1591 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t1591  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t1591 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t2550  p1, MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t2550 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t1629  p1, MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t1629 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t2034  p1, MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t2034 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Mark_t2083 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t2083  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t2083 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UriScheme_t2118 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t2118  p1, MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t2118 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_TableRange_t2360 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t2360  p1, MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t2360 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Slot_t2436 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t2436  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t2436 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Slot_t2443 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t2443  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t2443 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return NULL;
}

struct Object_t;
// GooglePlayGames.BasicApi.Nearby.AdvertisingResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Advertisin.h"
void* RuntimeInvoker_Void_t260_AdvertisingResult_t354 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, AdvertisingResult_t354  p1, MethodInfo* method);
	((Func)method->method)(obj, *((AdvertisingResult_t354 *)args[0]), method);
	return NULL;
}

struct Object_t;
// GooglePlayGames.BasicApi.Nearby.ConnectionRequest
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection.h"
void* RuntimeInvoker_Void_t260_ConnectionRequest_t355 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ConnectionRequest_t355  p1, MethodInfo* method);
	((Func)method->method)(obj, *((ConnectionRequest_t355 *)args[0]), method);
	return NULL;
}

struct Object_t;
// GooglePlayGames.BasicApi.Nearby.ConnectionResponse
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_Connection_1.h"
void* RuntimeInvoker_Void_t260_ConnectionResponse_t358 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ConnectionResponse_t358  p1, MethodInfo* method);
	((Func)method->method)(obj, *((ConnectionResponse_t358 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t1265  p1, MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
void* RuntimeInvoker_Boolean_t203_LayoutRebuilder_t1325 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t1325  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t1325 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Bounds_t979 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t979  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t979 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Boolean_t203_Ray_t1375 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t1375  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t1375 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1629  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1629 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_Boolean_t203_TextGenerationSettings_t1364 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t1364  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t1364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Interval_t2090 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t2090  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t2090 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
void* RuntimeInvoker_Boolean_t203_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2793  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2793 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Guid_t2814 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t2814  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t2814 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t2128  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t2128 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t997  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Touch_t901  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Touch_t901 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t1187  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t1378  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t1378 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t990  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t990 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t1398  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t1400  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t1617  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t1617 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t1618  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t1618 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Keyframe_t1591 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t1591  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t1591 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t2550  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t2550 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t2034  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t2034 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Mark_t2083 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t2083  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t2083 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UriScheme_t2118 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t2118  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t2118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_TableRange_t2360 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t2360  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t2360 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Slot_t2436 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t2436  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t2436 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Slot_t2443 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t2443  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t2443 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t1265  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t2147_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t2147  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	RSAParameters_t2147  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t236_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t2149_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t2149  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	DSAParameters_t2149  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3407  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3531  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3531 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Link_t3579 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t3579  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Link_t3579 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3656  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3656 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Link_t3760 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t3760  p1, MethodInfo* method);
	((Func)method->method)(obj, *((Link_t3760 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3790  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3790 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t3921  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t3921 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t4171  p1, MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t4171 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t2271_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t2179_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t238_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, int16_t p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, int16_t p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3407  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3531  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3531 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Link_t3579 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t3579  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t3579 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3656  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3656 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Link_t3760 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t3760  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t3760 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3790  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3790 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3921  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3921 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t4171  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t4171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Nullable_1_t377 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t377  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t377 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Nullable_1_t873 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t873  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t873 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ResponseStatus_t335_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"
void* RuntimeInvoker_UIStatus_t336_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
void* RuntimeInvoker_DataSource_t502_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
void* RuntimeInvoker_QuestUiResult_t372_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccep.h"
void* RuntimeInvoker_QuestAcceptStatus_t370_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaim.h"
void* RuntimeInvoker_QuestClaimMilestoneStatus_t371_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/SnapshotConflictPolicy
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Snap.h"
void* RuntimeInvoker_SnapshotConflictPolicy_t521_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa.h"
void* RuntimeInvoker_SavedGameRequestStatus_t374_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectU.h"
void* RuntimeInvoker_SelectUIStatus_t375_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/MatchResult
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Matc.h"
void* RuntimeInvoker_MatchResult_t514_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_InvType_t339_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MatchTurnStatus_t349_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
void* RuntimeInvoker_FramePressState_t1190_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_LayerMask_t1205 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t1205  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t1205 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t1205_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t1205  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	LayerMask_t1205  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t738_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t738  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Rect_t738  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Touch_t901_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t901  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Touch_t901  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t2045_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Interval_t2090_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t2090  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Interval_t2090  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t2374_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2795_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2793  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2793 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Guid_t2814 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t2814  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t2814 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t2128  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t2128 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t997  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Touch_t901  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Touch_t901 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t1187_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t1187  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	RaycastResult_t1187  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t1187  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t1378_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t1378  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	RaycastHit2D_t1378  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t1378  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t1378 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t990_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t990  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	RaycastHit_t990  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t990  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t990 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t1398_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t1398  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	UILineInfo_t1398  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t1398  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t1400_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t1400  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	UICharInfo_t1400  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t1400  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t1617_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1617  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	GcAchievementData_t1617  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t1617  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t1617 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t1618_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1618  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	GcScoreData_t1618  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t1618  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t1618 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t1591_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t1591  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Keyframe_t1591  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Keyframe_t1591 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t1591  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t1591 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t2550_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t2550  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	ParameterModifier_t2550  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t2550  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t2550 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t1629_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1629  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	HitInfo_t1629  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t1629  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t1629 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t2034_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t2034  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	X509ChainStatus_t2034  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t2034  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t2034 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t2083_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t2083  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Mark_t2083  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Mark_t2083 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t2083  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t2083 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t2118_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t2118  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	UriScheme_t2118  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UriScheme_t2118 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t2118  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t2118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t2360_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t2360  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	TableRange_t2360  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_TableRange_t2360 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t2360  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t2360 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t2436_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t2436  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Slot_t2436  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Slot_t2436 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t2436  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t2436 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t2443_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t2443  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Slot_t2443  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Slot_t2443 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t2443  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t2443 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t1265  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t1265_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t1265  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	UIVertex_t1265  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t747_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, float p1, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t1362  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, float p1, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_DecimalU26_t3040 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t240 * p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, float p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, float p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3407  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3531_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3531  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t3531  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3531  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3531 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t3579_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3579  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Link_t3579  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Link_t3579 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t3579  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t3579 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3656_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3656  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t3656  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3656  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3656 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t3760_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Link_t3760  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Link_t3760  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Link_t3760 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t3760  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t3760 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3790  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t3790  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3790  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3790 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3921  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t3921  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3921  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3921 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t4171_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t4171  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	KeyValuePair_2_t4171  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t4171  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t4171 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Interval_t2090 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t2090  p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t2090 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, double p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, double p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, double p1, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t193_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_QuaternionU26_t1728 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Quaternion_t771 * p1, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, (Quaternion_t771 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Vector3_t758 * p1, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, (Vector3_t758 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Matrix4x4U26_t1726 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Matrix4x4_t997 * p1, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, (Matrix4x4_t997 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t2149_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t2149  (*Func)(void* obj, bool* p1, MethodInfo* method);
	DSAParameters_t2149  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Builder_t376_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef Builder_t376  (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	Builder_t376  ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_AchievementState_t507_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_AchievementType_t506_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ResponseStatus_t409_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Even.h"
void* RuntimeInvoker_EventVisibility_t508_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardOrder
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead.h"
void* RuntimeInvoker_LeaderboardOrder_t509_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MultiplayerInvitationType_t519_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParticipantStatus_t513_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MatchResult_t514_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/QuestState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques.h"
void* RuntimeInvoker_QuestState_t516_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_QuestAcceptStatus_t414_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_QuestClaimMilestoneStatus_t415_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIStatus_t412_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/QuestMilestoneState
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Ques_0.h"
void* RuntimeInvoker_QuestMilestoneState_t517_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MultiplayerStatus_t413_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RealTimeRoomStatus_t520_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardTimeSpan
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_1.h"
void* RuntimeInvoker_LeaderboardTimeSpan_t511_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardCollection
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_2.h"
void* RuntimeInvoker_LeaderboardCollection_t512_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.Native.Cwrapper.Types/LeaderboardStart
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Lead_0.h"
void* RuntimeInvoker_LeaderboardStart_t510_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SnapshotOpenStatus_t416_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MatchStatus_t515_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HandleRef_t657_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef HandleRef_t657  (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	HandleRef_t657  ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t1364_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t1364  (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	TextGenerationSettings_t1364  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t738_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t738  (*Func)(void* obj, Rect_t738  p1, MethodInfo* method);
	Rect_t738  ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Vector2_t739  p1, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Color_t747  p1, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
void* RuntimeInvoker_Color32_t992_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t992  (*Func)(void* obj, Color_t747  p1, MethodInfo* method);
	Color32_t992  ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t747_Color32_t992 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, Color32_t992  p1, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, *((Color32_t992 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Quaternion_t771  p1, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Matrix4x4_t997  p1, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t1375_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t1375  (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Ray_t1375  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t1364_TextGenerationSettings_t1364 (MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t1364  (*Func)(void* obj, TextGenerationSettings_t1364  p1, MethodInfo* method);
	TextGenerationSettings_t1364  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t1364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2795_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, TimeSpan_t190  p1, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, DateTime_t48  p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_PlayGamesClientConfiguration_t366_PlayGamesClientConfiguration_t366 (MethodInfo* method, void* obj, void** args)
{
	typedef PlayGamesClientConfiguration_t366  (*Func)(void* obj, PlayGamesClientConfiguration_t366  p1, MethodInfo* method);
	PlayGamesClientConfiguration_t366  ret = ((Func)method->method)(obj, *((PlayGamesClientConfiguration_t366 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t194_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Single_t202_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t235_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t239_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t234_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_MonoMethodInfo_t2545_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t2545  (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	MonoMethodInfo_t2545  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t2537_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t2531_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
void* RuntimeInvoker_GCHandle_t941_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t941  (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	GCHandle_t941  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_GCHandle_t941 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, GCHandle_t941  p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((GCHandle_t941 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_HandleRef_t657 (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, HandleRef_t657  p1, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, KeyValuePair_2_t3407  p1, MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t2180 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t48_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"
struct Object_t;
void* RuntimeInvoker_ParticipantResult_t342_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Builder_t376_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Builder_t376  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Builder_t376  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SavedGameMetadataUpdate_t378 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, SavedGameMetadataUpdate_t378  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((SavedGameMetadataUpdate_t378 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MatchResult_t514_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_PlayGamesClientConfiguration_t366 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, PlayGamesClientConfiguration_t366  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((PlayGamesClientConfiguration_t366 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t758  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RaycastResult_t1187_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t1187  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	RaycastResult_t1187  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
struct Object_t;
void* RuntimeInvoker_EditState_t1257_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t2053_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t2040_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Category_t2075_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t2121_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t240_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t240  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t2843_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t2339 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t2339  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t2339 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t2339_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t2339  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	RuntimeTypeHandle_t2339  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t2340 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t2340  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t2340 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1674  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1674 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2835 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t2835  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t2835 *)args[0]), method);
	return ret;
}

struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t2542_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t2542  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	MonoEventInfo_t2542  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_GCHandle_t941_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t941  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	GCHandle_t941  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t2557_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t758_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIVertex_t1265_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t1265  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	UIVertex_t1265  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ColorTween_t1209 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t1209  p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t1209 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UICharInfo_t1400_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t1400  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	UICharInfo_t1400  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UILineInfo_t1398_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t1398  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	UILineInfo_t1398  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3531_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3531  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t3531  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3656_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3656  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t3656  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3790_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3790  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t3790  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3921_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3921  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t3921  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t4171_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t4171  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	KeyValuePair_2_t4171  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Byte_t237_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int16_t238_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UInt16_t194_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt16_t194_UInt16_t194 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UInt16_t194_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t194_UInt16_t194_UInt16_t194 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RegexOptionsU26_t2180_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t237_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ByteU26_t2328_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t* p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t190  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_ByteU26_t2328_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t* p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UInt16U26_t3017_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t1497;
#include "UnityEngine_ArrayTypes.h"
void* RuntimeInvoker_Void_t260_UserProfileU5BU5DU26_t1721_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t1497** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t1497**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_RectU26_t1724 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t738 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t738 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DecimalU26_t3040_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t240 * p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t240 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t36;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t260_StringBuilderU26_t3364_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t36 ** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t36 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t208;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t208** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t208**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct KeyValuePair_2U5BU5D_t3438;
void* RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2U5BU5D_t3438** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (KeyValuePair_2U5BU5D_t3438**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct IntPtrU5BU5D_t859;
void* RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtrU5BU5D_t859** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (IntPtrU5BU5D_t859**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Matrix4x4U5BU5D_t3830;
void* RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4U5BU5D_t3830** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4U5BU5D_t3830**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector3U5BU5D_t1284;
void* RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t1284** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t1284**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t3888;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t3888** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t3888**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t1264;
void* RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t1264** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t1264**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1669;
void* RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1669** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1669**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1670;
void* RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1670** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1670**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t260_GcAchievementDescriptionData_t1616_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t1616  p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t1616 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t260_GcUserProfileData_t1615_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t1615  p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t1615 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t758  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t758 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t738  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t738 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t1362  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t1362 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t747  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t747 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t1629  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t1629 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_DictionaryEntry_t2128 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t2128  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t2128 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Matrix4x4_t997  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Matrix4x4_t997 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Touch_t901  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Touch_t901 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t1187  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t1187 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastHit2D_t1378 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t1378  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t1378 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t990  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t990 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t739  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t739 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t1398  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t1398 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t1400  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t1400 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_GcAchievementData_t1617 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t1617  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t1617 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_GcScoreData_t1618 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t1618  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t1618 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Keyframe_t1591 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t1591  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t1591 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_ParameterModifier_t2550 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t2550  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t2550 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_X509ChainStatus_t2034 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t2034  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t2034 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Mark_t2083 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t2083  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t2083 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_UriScheme_t2118 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t2118  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t2118 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_TableRange_t2360 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t2360  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t2360 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Slot_t2436 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t2436  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t2436 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Slot_t2443 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t2443  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t2443 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t48  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t48 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t240  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t240 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t190  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t1265  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t1265 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t190  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SingleU26_t316_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_SingleU26_t316_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3407  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3407 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3531 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3531  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3531 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Link_t3579 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t3579  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t3579 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3656 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3656  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3656 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Link_t3760 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t3760  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t3760 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3790 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3790  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3790 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t3921 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t3921  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t3921 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_KeyValuePair_2_t4171 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t4171  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t4171 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ColorU26_t1465_SphericalHarmonicsL2U26_t1732 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747 * p1, SphericalHarmonicsL2_t1547 * p2, MethodInfo* method);
	((Func)method->method)(obj, (Color_t747 *)args[0], (SphericalHarmonicsL2_t1547 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Matrix4x4U26_t1726_Matrix4x4U26_t1726 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t997 * p1, Matrix4x4_t997 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t997 *)args[0], (Matrix4x4_t997 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_BoundsU26_t1729_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t979 * p1, Vector3_t758 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t979 *)args[0], (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_SphericalHarmonicsL2U26_t1732 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, SphericalHarmonicsL2_t1547 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), (SphericalHarmonicsL2_t1547 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2U26_t1725_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739 * p1, Vector2_t739  p2, MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t739 *)args[0], *((Vector2_t739 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t48  p1, DateTime_t48  p2, MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((DateTime_t48 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_ColorU26_t1465_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t747 * p1, Color_t747  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t747 *)args[0], *((Color_t747 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, Color_t747  p2, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), *((Color_t747 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Matrix4x4_t997_Matrix4x4U26_t1726 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), (Matrix4x4_t997 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Ray_t1375_SingleU26_t316 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t1375  p1, float* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t1375 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DateTime_t48_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t48  p1, TimeSpan_t190  p2, MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_NavigationU26_t4645_Navigation_t1272 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t1272 * p1, Navigation_t1272  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t1272 *)args[0], *((Navigation_t1272 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_ColorBlockU26_t4646_ColorBlock_t1221 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t1221 * p1, ColorBlock_t1221  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t1221 *)args[0], *((ColorBlock_t1221 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_SpriteStateU26_t4647_SpriteState_t1290 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t1290 * p1, SpriteState_t1290  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t1290 *)args[0], *((SpriteState_t1290 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Touch_t901_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Touch_t901  p1, Touch_t901  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Touch_t901 *)args[0]), *((Touch_t901 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t203_BoneWeight_t1502_BoneWeight_t1502 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t1502  p1, BoneWeight_t1502  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t1502 *)args[0]), *((BoneWeight_t1502 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Color_t747_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t747  p1, Color_t747  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), *((Color_t747 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Quaternion_t771_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t771  p1, Quaternion_t771  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), *((Quaternion_t771 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Rect_t738_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738  p1, Rect_t738  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), *((Rect_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Matrix4x4_t997 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Bounds_t979_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t979  p1, Vector3_t758  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t979 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Bounds_t979_Bounds_t979 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t979  p1, Bounds_t979  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t979 *)args[0]), *((Bounds_t979 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector4_t1362_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t1362  p1, Vector4_t1362  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), *((Vector4_t1362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_SphericalHarmonicsL2_t1547_SphericalHarmonicsL2_t1547 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t1547  p1, SphericalHarmonicsL2_t1547  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t1547 *)args[0]), *((SphericalHarmonicsL2_t1547 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_HitInfo_t1629_HitInfo_t1629 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1629  p1, HitInfo_t1629  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1629 *)args[0]), *((HitInfo_t1629 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Decimal_t240_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t240  p1, Decimal_t240  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), *((Decimal_t240 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t48  p1, DateTime_t48  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((DateTime_t48 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t190  p1, TimeSpan_t190  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_RaycastResult_t1187_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t1187  p1, RaycastResult_t1187  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), *((RaycastResult_t1187 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UIVertex_t1265_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t1265  p1, UIVertex_t1265  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), *((UIVertex_t1265 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UICharInfo_t1400_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t1400  p1, UICharInfo_t1400  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), *((UICharInfo_t1400 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_UILineInfo_t1398_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t1398  p1, UILineInfo_t1398  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), *((UILineInfo_t1398 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_DateTimeOffset_t2793_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t2793  p1, DateTimeOffset_t2793  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t2793 *)args[0]), *((DateTimeOffset_t2793 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Guid_t2814_Guid_t2814 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t2814  p1, Guid_t2814  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t2814 *)args[0]), *((Guid_t2814 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t193_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3790_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3790  (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	KeyValuePair_2_t3790  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t237_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t234_Double_t234_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int32_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_UInt64U26_t3006 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, uint64_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_Int64U26_t3001 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, int64_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_DecimalU26_t3040 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, Decimal_t240 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], (Decimal_t240 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_Double_t234_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, double p1, int64_t p2, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t1183_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t747_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t237_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_MonoMethodInfoU26_t3351 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t2545 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t2545 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, float p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t1187  p1, RaycastResult_t1187  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), *((RaycastResult_t1187 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastHit_t990_RaycastHit_t990 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t990  p1, RaycastHit_t990  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t990 *)args[0]), *((RaycastHit_t990 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Decimal_t240_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240  p1, Decimal_t240  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), *((Decimal_t240 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t238_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_IntPtr_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DateTime_t48_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t48  p1, DateTime_t48  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((DateTime_t48 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_DateTime_t48_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, DateTime_t48  p1, int32_t p2, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t190  p1, TimeSpan_t190  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int16_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Matrix4x4_t997 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t1265  p1, UIVertex_t1265  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), *((UIVertex_t1265 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t1400  p1, UICharInfo_t1400  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), *((UICharInfo_t1400 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t1398  p1, UILineInfo_t1398  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), *((UILineInfo_t1398 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DateTimeOffset_t2793_DateTimeOffset_t2793 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t2793  p1, DateTimeOffset_t2793  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t2793 *)args[0]), *((DateTimeOffset_t2793 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Guid_t2814_Guid_t2814 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t2814  p1, Guid_t2814  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t2814 *)args[0]), *((Guid_t2814 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_BoundsU26_t1729_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t979 * p1, Vector3_t758 * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t979 *)args[0], (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t3407  p1, KeyValuePair_2_t3407  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), *((KeyValuePair_2_t3407 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_IntPtrU26_t311 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t* p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (IntPtr_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Single_t202_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, float p1, Vector3_t758 * p2, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t739 * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t739 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_ColorU26_t1465 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t747 * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t747 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct UriFormatException_t2120;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t260_Object_t_UriFormatExceptionU26_t2182 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t2120 ** p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t2120 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t235_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t208;
struct Object_t;
void* RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t208** p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t208**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_MonoEventInfoU26_t3350 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t2542 * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t2542 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_MonoEnumInfoU26_t3379 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2806 * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2806 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_ObjectU26_t3349_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, float p2, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Vector3_t758_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Vector3_t758  p1, float p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t747_Color_t747_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, Color_t747  p1, float p2, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Quaternion_t771_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t771  p1, Quaternion_t771  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), *((Quaternion_t771 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Single_t202_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, float p1, Vector3_t758  p2, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Bounds_t979_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t979  p1, Vector3_t758  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t979 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Vector4_t1362_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t1362  p1, Vector4_t1362  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), *((Vector4_t1362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Vector4_t1362_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Vector4_t1362  p1, float p2, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t747  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t747 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t1547_SphericalHarmonicsL2_t1547_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t1547  (*Func)(void* obj, SphericalHarmonicsL2_t1547  p1, float p2, MethodInfo* method);
	SphericalHarmonicsL2_t1547  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t1547 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t1547_Single_t202_SphericalHarmonicsL2_t1547 (MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t1547  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t1547  p2, MethodInfo* method);
	SphericalHarmonicsL2_t1547  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t1547 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1674  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1674 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct IPAddress_t2002;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t203_Object_t_IPAddressU26_t2176 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t2002 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t2002 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t2003;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t203_Object_t_IPv6AddressU26_t2177 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t2003 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t2003 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int64U26_t3001 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_UInt32U26_t318 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_UInt64U26_t3006 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_ByteU26_t2328 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_SByteU26_t3011 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int16U26_t3014 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_UInt16U26_t3017 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_DoubleU26_t3037 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_RuntimeFieldHandle_t2340 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t2340  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t2340 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t48  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t48 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t233_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t240  p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t240 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_ObjectU26_t3349_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_HandleRef_t657_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t739  p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_TextGenerationSettings_t1364 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t1364  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t1364 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t239_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_DateTime_t48_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t48  p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_BoundsU26_t1729_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Bounds_t979 * p1, Vector3_t758 * p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, (Bounds_t979 *)args[0], (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t234_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_IntPtr_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3407  p1, KeyValuePair_2_t3407  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), *((KeyValuePair_2_t3407 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Touch_t901_Touch_t901 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Touch_t901  p1, Touch_t901  p2, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Touch_t901 *)args[0]), *((Touch_t901 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Vector4_t1362_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Vector4_t1362  p1, Rect_t738  p2, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), *((Rect_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, Rect_t738  p2, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Rect_t738 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Vector3_t758_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Quaternion_t771_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Quaternion_t771  p1, Quaternion_t771  p2, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), *((Quaternion_t771 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Quaternion_t771_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Quaternion_t771  p1, Vector3_t758  p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), *((Vector3_t758 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Matrix4x4_t997_Matrix4x4_t997 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997  p2, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Matrix4x4_t997 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Matrix4x4_t997_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Matrix4x4_t997  p1, Vector4_t1362  p2, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Vector4_t1362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t1362_Vector4_t1362_Vector4_t1362 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t1362  (*Func)(void* obj, Vector4_t1362  p1, Vector4_t1362  p2, MethodInfo* method);
	Vector4_t1362  ret = ((Func)method->method)(obj, *((Vector4_t1362 *)args[0]), *((Vector4_t1362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t1547_SphericalHarmonicsL2_t1547_SphericalHarmonicsL2_t1547 (MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t1547  (*Func)(void* obj, SphericalHarmonicsL2_t1547  p1, SphericalHarmonicsL2_t1547  p2, MethodInfo* method);
	SphericalHarmonicsL2_t1547  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t1547 *)args[0]), *((SphericalHarmonicsL2_t1547 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t240_Decimal_t240_Decimal_t240 (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, Decimal_t240  p1, Decimal_t240  p2, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, *((Decimal_t240 *)args[0]), *((Decimal_t240 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t48_DateTime_t48_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, DateTime_t48  p1, TimeSpan_t190  p2, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_TimeSpan_t190_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, TimeSpan_t190  p1, TimeSpan_t190  p2, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((TimeSpan_t190 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t190_DateTime_t48_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t190  (*Func)(void* obj, DateTime_t48  p1, TimeSpan_t190  p2, MethodInfo* method);
	TimeSpan_t190  ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((TimeSpan_t190 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Int32_t189_Object_t_StringU26_t319 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, String_t** p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, bool* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t208;
void* RuntimeInvoker_Int32_t189_Object_t_ObjectU5BU5DU26_t2995 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t208** p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t208**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MatchStatus_t348_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t739  p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_GCHandle_t941_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t941  (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	GCHandle_t941  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t4171_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t4171  (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	KeyValuePair_2_t4171  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t235_HandleRef_t657_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_ConnectionResponse_t358_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef ConnectionResponse_t358  (*Func)(void* obj, int64_t p1, Object_t * p2, MethodInfo* method);
	ConnectionResponse_t358  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t_TextGenerationSettings_t1364 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t1364  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t1364 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t2489_IntPtr_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3531_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3531  (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	KeyValuePair_2_t3531  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3656_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3656  (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	KeyValuePair_2_t3656  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3921_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3921  (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	KeyValuePair_2_t3921  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t193_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t738_Object_t_RectU26_t1724 (MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t738  (*Func)(void* obj, Object_t * p1, Rect_t738 * p2, MethodInfo* method);
	Rect_t738  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t738 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t758_Object_t_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t1375_Object_t_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t1375  (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, MethodInfo* method);
	Ray_t1375  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t237_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t2480_Object_t_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MatchResult_t514_HandleRef_t657_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t758_Object_t_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t1375_Object_t_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t1375  (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, MethodInfo* method);
	Ray_t1375  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t238_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t194_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_HandleRef_t657_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t235_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t239_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t234_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t1966;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2175 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1966 ** p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1966 **)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
// UnityEngine.Hash128
#include "UnityEngine_UnityEngine_Hash128.h"
void* RuntimeInvoker_Object_t_Object_t_Hash128_t1549 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Hash128_t1549  p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Hash128_t1549 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t738_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t738  (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	Rect_t738  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t2190_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t2149 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t2149  p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t2149 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t240_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t2385_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t48_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t2128_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t2128  (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	DictionaryEntry_t2128  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, IntPtr_t p2, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t3407  (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	KeyValuePair_2_t3407  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt16_t194_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int16_t238_Int16_t238_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct PointerEventData_t1191;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
void* RuntimeInvoker_Boolean_t203_Int32_t189_PointerEventDataU26_t1458_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t1191 ** p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t1191 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int16_t238_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t135;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t203_Int32_t189_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t135 ** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t135 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t235_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t208;
void* RuntimeInvoker_Void_t260_ObjectU5BU5DU26_t2995_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t208** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t208**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct KeyValuePair_2U5BU5D_t3438;
void* RuntimeInvoker_Void_t260_KeyValuePair_2U5BU5DU26_t4648_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2U5BU5D_t3438** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (KeyValuePair_2U5BU5D_t3438**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct IntPtrU5BU5D_t859;
void* RuntimeInvoker_Void_t260_IntPtrU5BU5DU26_t4649_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtrU5BU5D_t859** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (IntPtrU5BU5D_t859**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Matrix4x4U5BU5D_t3830;
void* RuntimeInvoker_Void_t260_Matrix4x4U5BU5DU26_t4650_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4U5BU5D_t3830** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4U5BU5D_t3830**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Vector3U5BU5D_t1284;
void* RuntimeInvoker_Void_t260_Vector3U5BU5DU26_t4651_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t1284** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t1284**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t3888;
void* RuntimeInvoker_Void_t260_RaycastResultU5BU5DU26_t4652_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t3888** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t3888**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t1264;
void* RuntimeInvoker_Void_t260_UIVertexU5BU5DU26_t4653_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t1264** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t1264**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1669;
void* RuntimeInvoker_Void_t260_UICharInfoU5BU5DU26_t4654_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1669** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1669**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1670;
void* RuntimeInvoker_Void_t260_UILineInfoU5BU5DU26_t4655_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1670** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1670**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Void_t260_SByte_t236_ByteU5BU5DU26_t2329_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t350** p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t350**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_SByte_t236_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_SByte_t236_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_IntPtr_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Byte_t237_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Byte_t237_Byte_t237 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_RectU26_t1724 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t738 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Rect_t738 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32U26_t317_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Rect_t738 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Rect_t738  p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((Rect_t738 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t239_Int64_t233_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t2876;
struct StringU5BU5D_t169;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int64U5BU5DU26_t3392_StringU5BU5DU26_t3393 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2876** p2, StringU5BU5D_t169** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2876**)args[1], (StringU5BU5D_t169**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t193_Object_t_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Color_t747_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Color_t747  p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Color_t747 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Vector3_t758_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3U26_t1722_ColorU26_t1465_SphericalHarmonicsL2U26_t1732 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758 * p1, Color_t747 * p2, SphericalHarmonicsL2_t1547 * p3, MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t758 *)args[0], (Color_t747 *)args[1], (SphericalHarmonicsL2_t1547 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Double_t234 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, double p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((double*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_RayU26_t1730_BoundsU26_t1729_SingleU26_t316 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t1375 * p1, Bounds_t979 * p2, float* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t1375 *)args[0], (Bounds_t979 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int16_t238 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Double_t234_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Color_t747_SphericalHarmonicsL2U26_t1732 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Color_t747  p2, SphericalHarmonicsL2_t1547 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Color_t747 *)args[1]), (SphericalHarmonicsL2_t1547 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Quaternion_t771_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Quaternion_t771  p2, Vector3_t758  p3, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Quaternion_t771 *)args[1]), *((Vector3_t758 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Vector3_t758_Color_t747 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, Color_t747  p3, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), *((Color_t747 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector3_t758_Vector3_t758_RaycastHitU26_t1754 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, RaycastHit_t990 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), (RaycastHit_t990 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DateTime_t48_DateTime_t48_TimeSpan_t190 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t48  p1, DateTime_t48  p2, TimeSpan_t190  p3, MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((DateTime_t48 *)args[1]), *((TimeSpan_t190 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_IntPtr_t_Int64_t233_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t1183_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t1497;
struct Object_t;
void* RuntimeInvoker_Void_t260_UserProfileU5BU5DU26_t1721_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t1497** p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t1497**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vector3_t758 * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Vector3_t758 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_ColorU26_t1465 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t747 * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t747 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Vector3U26_t1722_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t260_Object_t_MonoPropertyInfoU26_t3352_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t2546 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t2546 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Hash128_t1549_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Hash128_t1549  p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Hash128_t1549 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_AnimatorStateInfo_t1589_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t1589  p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t1589 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_DecimalU26_t3040_DecimalU26_t3040 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, Decimal_t240 * p2, Decimal_t240 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], (Decimal_t240 *)args[1], (Decimal_t240 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_RenderBufferU26_t1734_RenderBufferU26_t1734 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t1619 * p2, RenderBuffer_t1619 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t1619 *)args[1], (RenderBuffer_t1619 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Vector3U26_t1722_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t2040_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t233_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_ColorU26_t1465_ColorU26_t1465 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t747 * p2, Color_t747 * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t747 *)args[1], (Color_t747 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_QuaternionU26_t1728_QuaternionU26_t1728_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Quaternion_t771 * p1, Quaternion_t771 * p2, float p3, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, (Quaternion_t771 *)args[0], (Quaternion_t771 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Void_t260_Object_t_StringU26_t319_StringU26_t319 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t350;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Void_t260_Object_t_ByteU5BU5DU26_t2329_ByteU5BU5DU26_t2329 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t350** p2, ByteU5BU5D_t350** p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t350**)args[1], (ByteU5BU5D_t350**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Byte_t237_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t203_Object_t_MonoIOStatU26_t3347_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t2488 * p2, int32_t* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t2488 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739_Vector2_t739_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t758_Vector3_t758_Vector3_t758_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t758  (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, float p3, MethodInfo* method);
	Vector3_t758  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t747_Color_t747_Color_t747_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Color_t747  (*Func)(void* obj, Color_t747  p1, Color_t747  p2, float p3, MethodInfo* method);
	Color_t747  ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), *((Color_t747 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t771_Quaternion_t771_Quaternion_t771_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t771  (*Func)(void* obj, Quaternion_t771  p1, Quaternion_t771  p2, float p3, MethodInfo* method);
	Quaternion_t771  ret = ((Func)method->method)(obj, *((Quaternion_t771 *)args[0]), *((Quaternion_t771 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t193_Object_t_Int32U26_t317_CharU26_t2181 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Vector3U26_t1722_QuaternionU26_t1728_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Vector3_t758 * p1, Quaternion_t771 * p2, Vector3_t758 * p3, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, (Vector3_t758 *)args[0], (Quaternion_t771 *)args[1], (Vector3_t758 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t1375_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t1375  p1, float p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t1375 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Vector3_t758_Quaternion_t771_Vector3_t758 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, Vector3_t758  p1, Quaternion_t771  p2, Vector3_t758  p3, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Quaternion_t771 *)args[1]), *((Vector3_t758 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t236_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_RectU26_t1724_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738 * p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, (Rect_t738 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Object_t_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t739 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t739 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t237_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_RectU26_t1724_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738 * p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t738 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t739_Rect_t738_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Rect_t738  p1, Object_t * p2, int32_t p3, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t238_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Matrix4x4_t997_Matrix4x4_t997_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Matrix4x4_t997 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Vector3_t758_Vector3_t758_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_RaycastResult_t1187_RaycastResult_t1187_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t1187  p1, RaycastResult_t1187  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), *((RaycastResult_t1187 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_UIVertex_t1265_UIVertex_t1265_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t1265  p1, UIVertex_t1265  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), *((UIVertex_t1265 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_UICharInfo_t1400_UICharInfo_t1400_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t1400  p1, UICharInfo_t1400  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), *((UICharInfo_t1400 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_UILineInfo_t1398_UILineInfo_t1398_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t1398  p1, UILineInfo_t1398  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), *((UILineInfo_t1398 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Rect_t738_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738  p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t194_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t236_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_BooleanU26_t315_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SingleU26_t316_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_UInt32U26_t318_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct String_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_StringU26_t319_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, IntPtr_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t1727_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t1535 * p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t1535 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t235_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t208;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_ObjectU5BU5DU26_t2995_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t208** p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t208**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Vector2U26_t1725_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t739 * p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t739 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t208;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_ObjectU5BU5DU26_t2995 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t208** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t208**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t233_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1674  p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1674 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Rect_t738_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t239_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_StreamingContext_t1674 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1674  p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1674 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_IntPtr_t_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t135 ** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t135 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Touch_t901_BooleanU26_t315_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t901  p1, bool* p2, bool* p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t901 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t2040_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t234_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Nullable_1_t377_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Nullable_1_t377  p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Nullable_1_t377 *)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t3407  p1, KeyValuePair_2_t3407  p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), *((KeyValuePair_2_t3407 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Hash128_t1549_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Hash128_t1549  p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Hash128_t1549 *)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t240_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t240  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	Decimal_t240  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t48_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_ConnectionResponse_t358_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef ConnectionResponse_t358  (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	ConnectionResponse_t358  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Module_t2517;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t317_ModuleU26_t3348 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t2517 ** p3, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t2517 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_IntPtr_t_IntPtr_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t1722_QuaternionU26_t1728 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t758 * p2, Quaternion_t771 * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t758 *)args[1], (Quaternion_t771 *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t2620;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1674_ISurrogateSelectorU26_t3361 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1674  p2, Object_t ** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1674 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3_t758_Quaternion_t771 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t758  p2, Quaternion_t771  p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t758 *)args[1]), *((Quaternion_t771 *)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t739_Vector2_t739_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t739  (*Func)(void* obj, Vector2_t739  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Vector2_t739  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t238_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_HandleRef_t657_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, IntPtr_t p3, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t202_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t234_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t319 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t22;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t3046 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t22 ** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t22 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t135 ** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t135 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t2090_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t2090  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t2090 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1674_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1674  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1674 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t48_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t48  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t240_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t240  p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t240 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_AdvertisingResult_t354_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, AdvertisingResult_t354  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((AdvertisingResult_t354 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ConnectionRequest_t355_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ConnectionRequest_t355  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ConnectionRequest_t355 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ConnectionResponse_t358_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ConnectionResponse_t358  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ConnectionResponse_t358 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Matrix4x4_t997_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Matrix4x4_t997  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t758_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t758  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t1187_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t1187  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t747_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t747  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t747 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t1265_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t1265  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t739_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t739  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t1400_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t1400  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t1398_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t1398  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, KeyValuePair_2_t3407  p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32_t189_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t2070_SByte_t236_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_Single_t202_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, float p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Int32U26_t317_Int32U26_t317_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_SByte_t236_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_DateTime_t48_DateTime_t48_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t48  p1, DateTime_t48  p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((DateTime_t48 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector2_t739_Vector2_t739_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, int8_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_UInt16_t194 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_RectU26_t1724_Vector2U26_t1725_Vector2U26_t1725_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738 * p1, Vector2_t739 * p2, Vector2_t739 * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Rect_t738 *)args[0], (Vector2_t739 *)args[1], (Vector2_t739 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Ray_t1375_RaycastHitU26_t1754_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t1375  p1, RaycastHit_t990 * p2, float p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t1375 *)args[0]), (RaycastHit_t990 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Vector2_t739_Vector2_t739_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Vector2_t739  p2, Vector2_t739  p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), *((Vector2_t739 *)args[1]), *((Vector2_t739 *)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Int32U26_t317_Int32U26_t317_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_SByte_t236_Object_t_Int32_t189_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int16_t238_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int16_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32U26_t317_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_SByte_t236_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int32_t* p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_CharU26_t2181_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Single_t202_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, float p1, float p2, float p3, float p4, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2850;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2850** p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2850**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236_Int32U26_t317_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236_Int64U26_t3001_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236_UInt32U26_t318_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236_SByteU26_t3011_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_SByte_t236_Int16U26_t3014_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t135 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t135 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_DecimalU26_t3040_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t240 * p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t240 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_IntPtr_t_Int64_t233_Int32_t189_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Matrix4x4_t997_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Matrix4x4_t997  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t997 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Vector3_t758_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t758  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t758 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_RaycastResult_t1187_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t1187  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t1187 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_UIVertex_t1265_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t1265  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t1265 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_UICharInfo_t1400_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t1400  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t1400 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_UILineInfo_t1398_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t1398  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t1398 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t1378_Vector2_t739_Vector2_t739_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t1378  (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, int32_t p4, MethodInfo* method);
	RaycastHit2D_t1378  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_KeyValuePair_2_t3407_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t3407  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t3407 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_IntPtr_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t236_SByte_t236_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_IntPtr_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Single_t202_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1673;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t260_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1673 ** p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1673 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, Object_t * p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t_IntPtr_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, IntPtr_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Rect_t738_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738  p1, int8_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t1722_Vector3U26_t1722_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t758 * p1, Vector3_t758 * p2, float p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t758 *)args[0], (Vector3_t758 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Hash128U26_t1733_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Hash128_t1549 * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Hash128_t1549 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t758_Vector3_t758_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, float p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t739_Vector2_t739_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_Int64U26_t3001 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_UInt32U26_t318 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_ByteU26_t2328 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_UInt16U26_t3017 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_DoubleU26_t3037 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, int64_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, IntPtr_t p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_HandleRef_t657_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, IntPtr_t p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2U26_t1725_Object_t_Object_t_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739 * p1, Object_t * p2, Object_t * p3, Vector2_t739 * p4, MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t739 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t739 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Object_t_Object_t_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t2377;
struct Level2MapU5BU5D_t2378;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_ContractionU5BU5DU26_t3205_Level2MapU5BU5DU26_t3206 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t2377** p3, Level2MapU5BU5D_t2378** p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t2377**)args[2], (Level2MapU5BU5D_t2378**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64U26_t3001_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2_t739_Object_t_Object_t_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739  p1, Object_t * p2, Object_t * p3, Vector2_t739 * p4, MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t739 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t739 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Vector2_t739_Object_t_Vector3U26_t1722 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, Object_t * p3, Vector3_t758 * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), (Object_t *)args[2], (Vector3_t758 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Vector2_t739_Object_t_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t739  p2, Object_t * p3, Vector2_t739 * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t739 *)args[1]), (Object_t *)args[2], (Vector2_t739 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t1730_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t1375 * p2, float p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t1375 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, Object_t * p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Object_t_BooleanU26_t315_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ILayoutElement_t1367;
void* RuntimeInvoker_Single_t202_Object_t_Object_t_Single_t202_ILayoutElementU26_t1470 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Single_t202_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Object_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, Object_t * p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_IntPtr_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, IntPtr_t p3, IntPtr_t p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SavedGameMetadataUpdate_t378_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, SavedGameMetadataUpdate_t378  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((SavedGameMetadataUpdate_t378 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_HandleRef_t657_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, Object_t * p3, IntPtr_t p4, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Vector2_t739_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t739  p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t739 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t48_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t48  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, MethodInfo* method);
	DateTime_t48  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, IntPtr_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t236_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_HandleRef_t657_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, Object_t * p3, IntPtr_t p4, MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Matrix4x4_t997_Matrix4x4_t997_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Matrix4x4_t997  p1, Matrix4x4_t997  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Matrix4x4_t997 *)args[0]), *((Matrix4x4_t997 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t758_Vector3_t758_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t1187_RaycastResult_t1187_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t1187  p1, RaycastResult_t1187  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t1187 *)args[0]), *((RaycastResult_t1187 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastHit_t990_RaycastHit_t990_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t990  p1, RaycastHit_t990  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t990 *)args[0]), *((RaycastHit_t990 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t1265_UIVertex_t1265_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t1265  p1, UIVertex_t1265  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t1265 *)args[0]), *((UIVertex_t1265 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t1400_UICharInfo_t1400_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t1400  p1, UICharInfo_t1400  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t1400 *)args[0]), *((UICharInfo_t1400 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t1398_UILineInfo_t1398_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t1398  p1, UILineInfo_t1398  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t1398 *)args[0]), *((UILineInfo_t1398 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t48_Nullable_1_t377_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t48  p1, Nullable_1_t377  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t48 *)args[0]), *((Nullable_1_t377 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_KeyValuePair_2_t3407_KeyValuePair_2_t3407_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, KeyValuePair_2_t3407  p1, KeyValuePair_2_t3407  p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((KeyValuePair_2_t3407 *)args[0]), *((KeyValuePair_2_t3407 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1674_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1674  p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1674 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Color_t747_Single_t202_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t747  p1, float p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((Color_t747 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Object_t_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t233_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Int64_t233_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int64_t p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int64_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Single_t202_Single_t202_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3U26_t1722_Vector3U26_t1722_ColorU26_t1465_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758 * p1, Vector3_t758 * p2, Color_t747 * p3, float p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t758 *)args[0], (Vector3_t758 *)args[1], (Color_t747 *)args[2], *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Double_t234_SByte_t236_SByte_t236_DateTime_t48 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t48  p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t48 *)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector3_t758_Vector3_t758_Color_t747_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, Color_t747  p3, float p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), *((Color_t747 *)args[2]), *((float*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Rect_t738_Object_t_Int32_t189_SByte_t236_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t738  p1, Object_t * p2, int32_t p3, int8_t p4, float p5, MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t738 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector3U26_t1722_Vector3U26_t1722_RaycastHitU26_t1754_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t758 * p1, Vector3_t758 * p2, RaycastHit_t990 * p3, float p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t758 *)args[0], (Vector3_t758 *)args[1], (RaycastHit_t990 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Object_t_SByte_t236_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t135 ** p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t135 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2850;
void* RuntimeInvoker_Void_t260_Byte_t237_Object_t_SByte_t236_ObjectU26_t3349_HeaderU5BU5DU26_t3362 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2850** p5, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2850**)args[4], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t203_Vector3_t758_Vector3_t758_RaycastHitU26_t1754_Single_t202_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t758  p1, Vector3_t758  p2, RaycastHit_t990 * p3, float p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t758 *)args[0]), *((Vector3_t758 *)args[1]), (RaycastHit_t990 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_CharU26_t2181_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_SByte_t236_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_Object_t_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Single_t202_Single_t202_Single_t202_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_RectU26_t1724_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t738 * p2, Object_t * p3, int32_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t738 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_BooleanU26_t315_SByte_t236_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Single_t202_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_RectU26_t1724_Int32_t189_SByte_t236_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738 * p1, int32_t p2, int8_t p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t738 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Rect_t738_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t738  p2, Object_t * p3, int32_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t738 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Int32U26_t317_Object_t_Int32U26_t317_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t135 ** p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t135 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1673;
void* RuntimeInvoker_Void_t260_Byte_t237_Object_t_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1673 ** p5, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1673 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Rect_t738_Int32_t189_SByte_t236_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t738  p1, int32_t p2, int8_t p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t738 *)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, int32_t p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_DecimalU26_t3040_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t240 * p4, int8_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t240 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t_Int64_t233_Int64_t233 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_IntPtr_t_Object_t_Int32_t189_Int32_t189_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_HandleRef_t657_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, HandleRef_t657  p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_RectU26_t1724_Object_t_Int32_t189_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t738 * p2, Object_t * p3, int32_t p4, Vector2_t739 * p5, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t738 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t739 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Void_t260_Object_t_Int32U26_t317_ByteU26_t2328_Int32U26_t317_ByteU5BU5DU26_t2329 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t350** p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t350**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Rect_t738_Object_t_Int32_t189_Vector2U26_t1725 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t738  p2, Object_t * p3, int32_t p4, Vector2_t739 * p5, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t738 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t739 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Object_t_BooleanU26_t315_BooleanU26_t315_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1673;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1673 ** p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1673 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, int32_t p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32U26_t317_Object_t_Object_t_BooleanU26_t315_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Object_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, Object_t * p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_IntPtr_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, IntPtr_t p4, IntPtr_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Int32_t189_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, IntPtr_t p3, Object_t * p4, IntPtr_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Int32_t189_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, IntPtr_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int16_t238_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Nullable_1_t377_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Nullable_1_t377  p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Nullable_1_t377 *)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, IntPtr_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t234_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, IntPtr_t p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_Int32_t189_SByte_t236_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1673;
void* RuntimeInvoker_Void_t260_Object_t_SByte_t236_SByte_t236_Int64U26_t3001_ObjectU26_t3349_SerializationInfoU26_t3363 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1673 ** p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1673 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Color32_t992_Int32_t189_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t992  p2, int32_t p3, int32_t p4, float p5, float p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t992 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t202_Single_t202_Single_t202_SingleU26_t316_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t997_Single_t202_Single_t202_Single_t202_Single_t202_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t997  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, MethodInfo* method);
	Matrix4x4_t997  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Int32_t189_SByte_t236_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, int32_t p3, int8_t p4, Object_t * p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), (Object_t *)args[4], *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Int32_t189_Int32_t189_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t2371 * p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t2371 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t1378_Vector2_t739_Vector2_t739_Single_t202_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t1378  (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, int32_t p4, float p5, float p6, MethodInfo* method);
	RaycastHit2D_t1378  ret = ((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt64U2AU26_t3384_Int32U2AU26_t3385_CharU2AU26_t3386_CharU2AU26_t3386_Int64U2AU26_t3387_Int32U2AU26_t3385 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t236_SByte_t236_SByte_t236_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_MonoIOErrorU26_t3346 (MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Int32_t189_CharU26_t2181_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_Int32U26_t317_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t135 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t135 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_Int64U26_t3001_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t135 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t135 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_UInt32U26_t318_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t135 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t135 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_UInt64U26_t3006_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t135 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t135 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Object_t_SByte_t236_DoubleU26_t3037_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t135 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t135 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t1725_Vector2U26_t1725_Single_t202_Int32_t189_Single_t202_Single_t202 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t739 * p1, Vector2_t739 * p2, float p3, int32_t p4, float p5, float p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t739 *)args[0], (Vector2_t739 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_DecoderFallbackBufferU26_t3367 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t2736 ** p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t2736 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32U26_t317_Int32U26_t317_Int32U26_t317_BooleanU26_t315_StringU26_t319 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t2362;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t2362;
void* RuntimeInvoker_Void_t260_Object_t_CodePointIndexerU26_t3207_ByteU2AU26_t3023_ByteU2AU26_t3023_CodePointIndexerU26_t3207_ByteU2AU26_t3023 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t2362 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t2362 ** p5, uint8_t** p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t2362 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t2362 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_IntPtr_t_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, IntPtr_t p3, int32_t p4, Object_t * p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), *((IntPtr_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Object_t_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_UIVertex_t1265_Vector2_t739_Vector2_t739_Vector2_t739_Vector2_t739 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t1265  p2, Vector2_t739  p3, Vector2_t739  p4, Vector2_t739  p5, Vector2_t739  p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t1265 *)args[1]), *((Vector2_t739 *)args[2]), *((Vector2_t739 *)args[3]), *((Vector2_t739 *)args[4]), *((Vector2_t739 *)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_Object_t_Int64_t233_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2736 ** p2, ByteU5BU5D_t350** p3, Object_t * p4, int64_t p5, int32_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2736 **)args[1], (ByteU5BU5D_t350**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t_DateTime_t48_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t48  p4, Object_t * p5, int32_t p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t48 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Object_t_SByte_t236_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189_Object_t_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t2371 * p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t2371 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_SByte_t236_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, IntPtr_t p3, IntPtr_t p4, int8_t p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), *((int8_t*)args[4]), *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Object_t_Object_t_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int64_t233_Object_t_Int64_t233_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_Object_t_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, Object_t * p3, IntPtr_t p4, Object_t * p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), (Object_t *)args[4], *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_IntPtr_t_Object_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, IntPtr_t p3, Object_t * p4, Object_t * p5, IntPtr_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], *((IntPtr_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Int32_t189_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int32_t189_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t237_Object_t_SByte_t236_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_IntPtr_t_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, IntPtr_t p3, IntPtr_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t236_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t189_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_SByte_t236_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_UInt32U26_t318_Int32_t189_UInt32U26_t318_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_SByte_t236_SByte_t236_SByte_t236_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2U26_t1725_Vector2U26_t1725_Single_t202_Int32_t189_Single_t202_Single_t202_RaycastHit2DU26_t1755 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739 * p1, Vector2_t739 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t1378 * p7, MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t739 *)args[0], (Vector2_t739 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t1378 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Vector2_t739_Vector2_t739_Single_t202_Int32_t189_Single_t202_Single_t202_RaycastHit2DU26_t1755 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t739  p1, Vector2_t739  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t1378 * p7, MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t739 *)args[0]), *((Vector2_t739 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t1378 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_SByte_t236_SByte_t236_SByte_t236_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_CharU26_t2181_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189_Int32_t189_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Object_t_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t2371 * p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t2371 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32_t189_Int32_t189_Object_t_SByte_t236_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_ByteU2AU26_t3023_ByteU2AU26_t3023_DoubleU2AU26_t3024_UInt16U2AU26_t3025_UInt16U2AU26_t3025_UInt16U2AU26_t3025_UInt16U2AU26_t3025 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_SByte_t236_SByte_t236_Int32_t189_Object_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int8_t p2, int8_t p3, int32_t p4, Object_t * p5, Object_t * p6, IntPtr_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Int32_t189_DateTimeU26_t3369_DateTimeOffsetU26_t3370_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t48 * p4, DateTimeOffset_t2793 * p5, int8_t p6, Exception_t135 ** p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t48 *)args[3], (DateTimeOffset_t2793 *)args[4], *((int8_t*)args[5]), (Exception_t135 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Int32_t189_Int32_t189_SByte_t236_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t2745;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t41;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_EncoderFallbackBufferU26_t3365_CharU5BU5DU26_t3366 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t2745 ** p6, CharU5BU5D_t41** p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t2745 **)args[5], (CharU5BU5D_t41**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, Object_t * p6, IntPtr_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Object_t_Int32_t189_Single_t202_Single_t202_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t236_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int64_t233_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_IntPtr_t_Object_t_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, IntPtr_t p3, Object_t * p4, IntPtr_t p5, Object_t * p6, IntPtr_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), (Object_t *)args[3], *((IntPtr_t*)args[4]), (Object_t *)args[5], *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_Object_t_IntPtr_t_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, Object_t * p3, IntPtr_t p4, IntPtr_t p5, Object_t * p6, IntPtr_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), (Object_t *)args[5], *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t208;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t189_Object_t_ObjectU5BU5DU26_t2995_Object_t_Object_t_Object_t_ObjectU26_t3349 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t208** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t208**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t260_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct MethodBase_t1691;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t203_Int32_t189_SByte_t236_MethodBaseU26_t3344_Int32U26_t317_Int32U26_t317_StringU26_t319_Int32U26_t317_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1691 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1691 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int16_t238_Int32_t189_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t2371 * p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t2371 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Object_t_Int32_t189_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t2371 * p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t2371 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Int32_t189_Object_t_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t2371 * p8, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t2371 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t2736 ** p6, ByteU5BU5D_t350** p7, int8_t p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t2736 **)args[5], (ByteU5BU5D_t350**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_Object_t_Int64_t233_Int32_t189_Object_t_Int32U26_t317 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2736 ** p2, ByteU5BU5D_t350** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2736 **)args[1], (ByteU5BU5D_t350**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Int32U26_t317_BooleanU26_t315_BooleanU26_t315_Int32U26_t317_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Int32_t189_Object_t_SByte_t236_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_HandleRef_t657_IntPtr_t_Object_t_IntPtr_t_IntPtr_t_IntPtr_t_Object_t_IntPtr_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HandleRef_t657  p1, IntPtr_t p2, Object_t * p3, IntPtr_t p4, IntPtr_t p5, IntPtr_t p6, Object_t * p7, IntPtr_t p8, MethodInfo* method);
	((Func)method->method)(obj, *((HandleRef_t657 *)args[0]), *((IntPtr_t*)args[1]), (Object_t *)args[2], *((IntPtr_t*)args[3]), *((IntPtr_t*)args[4]), *((IntPtr_t*)args[5]), (Object_t *)args[6], *((IntPtr_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_Int64_t233_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t_IntPtr_t_IntPtr_t_SByte_t236_IntPtr_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, IntPtr_t p3, IntPtr_t p4, int8_t p5, IntPtr_t p6, Object_t * p7, Object_t * p8, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), *((IntPtr_t*)args[2]), *((IntPtr_t*)args[3]), *((int8_t*)args[4]), *((IntPtr_t*)args[5]), (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t189_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_SByte_t236_Int32U26_t317_BooleanU26_t315_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189_BooleanU26_t315_BooleanU26_t315_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t2736 ** p7, ByteU5BU5D_t350** p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t2736 **)args[6], (ByteU5BU5D_t350**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t2364;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Object_t_SByte_t236_Int32_t189_ContractionU26_t3209_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t2364 ** p8, Context_t2371 * p9, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t2364 **)args[7], (Context_t2371 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t135;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_Int32_t189_DateTimeU26_t3369_SByte_t236_BooleanU26_t315_SByte_t236_ExceptionU26_t2996 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t48 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t135 ** p9, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t48 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t135 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t2364;
void* RuntimeInvoker_Boolean_t203_Object_t_Int32U26_t317_Int32_t189_Int32_t189_Int32_t189_Object_t_SByte_t236_Int32_t189_ContractionU26_t3209_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t2364 ** p9, Context_t2371 * p10, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t2364 **)args[8], (Context_t2371 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Object_t_Int32_t189_UInt32U26_t318_UInt32U26_t318_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t2736 ** p8, ByteU5BU5D_t350** p9, int8_t p10, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t2736 **)args[7], (ByteU5BU5D_t350**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Int16_t238_Int16_t238_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_Int32_t189_BooleanU26_t315_BooleanU26_t315_SByte_t236_SByte_t236_ContextU26_t3208 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t2371 * p11, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t2371 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2736;
struct ByteU5BU5D_t350;
void* RuntimeInvoker_Int32_t189_Object_t_Int32_t189_Int32_t189_Object_t_Int32_t189_UInt32U26_t318_UInt32U26_t318_Object_t_DecoderFallbackBufferU26_t3367_ByteU5BU5DU26_t2329_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t2736 ** p9, ByteU5BU5D_t350** p10, int8_t p11, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t2736 **)args[8], (ByteU5BU5D_t350**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Object_t_Object_t_SByte_t236_Object_t_Object_t_Int32_t189_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_SByte_t236_DateTimeU26_t3369_DateTimeOffsetU26_t3370_Object_t_Int32_t189_SByte_t236_BooleanU26_t315_BooleanU26_t315 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t48 * p5, DateTimeOffset_t2793 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t48 *)args[4], (DateTimeOffset_t2793 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int16_t238_Object_t_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236_SByte_t236_SByte_t236_Int16_t238_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t238_Object_t_Int32_t189_Int32_t189_Int32_t189_SByte_t236_SByte_t236_SByte_t236_SByte_t236_Int16_t238_SByte_t236_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t260_Int32_t189_Object_t_Object_t_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189_Int32_t189 (MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Color_t747_Int32_t189_Single_t202_Int32_t189_SByte_t236_SByte_t236_Int32_t189_Int32_t189_Int32_t189_Int32_t189_SByte_t236_Int32_t189_Vector2_t739_Vector2_t739_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t747  p3, int32_t p4, float p5, int32_t p6, int8_t p7, int8_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, int8_t p13, int32_t p14, Vector2_t739  p15, Vector2_t739  p16, int8_t p17, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t747 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int8_t*)args[12]), *((int32_t*)args[13]), *((Vector2_t739 *)args[14]), *((Vector2_t739 *)args[15]), *((int8_t*)args[16]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Color_t747_Int32_t189_Single_t202_Int32_t189_SByte_t236_SByte_t236_Int32_t189_Int32_t189_Int32_t189_Int32_t189_SByte_t236_Int32_t189_Single_t202_Single_t202_Single_t202_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t747  p3, int32_t p4, float p5, int32_t p6, int8_t p7, int8_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, int8_t p13, int32_t p14, float p15, float p16, float p17, float p18, int8_t p19, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t747 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int8_t*)args[12]), *((int32_t*)args[13]), *((float*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((int8_t*)args[18]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t203_Object_t_Object_t_Object_t_ColorU26_t1465_Int32_t189_Single_t202_Int32_t189_SByte_t236_SByte_t236_Int32_t189_Int32_t189_Int32_t189_Int32_t189_SByte_t236_Int32_t189_Single_t202_Single_t202_Single_t202_Single_t202_SByte_t236 (MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t747 * p4, int32_t p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t747 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

