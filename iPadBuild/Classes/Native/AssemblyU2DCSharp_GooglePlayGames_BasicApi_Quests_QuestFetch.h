﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestFetch.h"
// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
struct  QuestFetchFlags_t369 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.QuestFetchFlags::value__
	int32_t ___value___1;
};
