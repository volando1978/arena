﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t213;
struct WaitForSeconds_t213_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m965 (WaitForSeconds_t213 * __this, float ___seconds, MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t213_marshal(const WaitForSeconds_t213& unmarshaled, WaitForSeconds_t213_marshaled& marshaled);
void WaitForSeconds_t213_marshal_back(const WaitForSeconds_t213_marshaled& marshaled, WaitForSeconds_t213& unmarshaled);
void WaitForSeconds_t213_marshal_cleanup(WaitForSeconds_t213_marshaled& marshaled);
