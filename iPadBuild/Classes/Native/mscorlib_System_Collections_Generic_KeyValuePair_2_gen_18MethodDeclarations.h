﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
struct KeyValuePair_2_t3790;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m20539_gshared (KeyValuePair_2_t3790 * __this, int32_t ___key, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m20539(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, int32_t, int32_t, MethodInfo*))KeyValuePair_2__ctor_m20539_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m20540_gshared (KeyValuePair_2_t3790 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m20540(__this, method) (( int32_t (*) (KeyValuePair_2_t3790 *, MethodInfo*))KeyValuePair_2_get_Key_m20540_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m20541_gshared (KeyValuePair_2_t3790 * __this, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m20541(__this, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, int32_t, MethodInfo*))KeyValuePair_2_set_Key_m20541_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m20542_gshared (KeyValuePair_2_t3790 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m20542(__this, method) (( int32_t (*) (KeyValuePair_2_t3790 *, MethodInfo*))KeyValuePair_2_get_Value_m20542_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m20543_gshared (KeyValuePair_2_t3790 * __this, int32_t ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m20543(__this, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m20543_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m20544_gshared (KeyValuePair_2_t3790 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m20544(__this, method) (( String_t* (*) (KeyValuePair_2_t3790 *, MethodInfo*))KeyValuePair_2_ToString_m20544_gshared)(__this, method)
