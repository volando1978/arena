﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SingleCallIdentity
struct SingleCallIdentity_t2644;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t2596;
// System.Type
struct Type_t;

// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern "C" void SingleCallIdentity__ctor_m12765 (SingleCallIdentity_t2644 * __this, String_t* ___objectUri, Context_t2596 * ___context, Type_t * ___objectType, MethodInfo* method) IL2CPP_METHOD_ATTR;
