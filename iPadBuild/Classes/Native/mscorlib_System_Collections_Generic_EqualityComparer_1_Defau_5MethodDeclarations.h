﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>
struct DefaultComparer_t3837;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::.ctor()
extern "C" void DefaultComparer__ctor_m21163_gshared (DefaultComparer_t3837 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m21163(__this, method) (( void (*) (DefaultComparer_t3837 *, MethodInfo*))DefaultComparer__ctor_m21163_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m21164_gshared (DefaultComparer_t3837 * __this, Matrix4x4_t997  ___obj, MethodInfo* method);
#define DefaultComparer_GetHashCode_m21164(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3837 *, Matrix4x4_t997 , MethodInfo*))DefaultComparer_GetHashCode_m21164_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Matrix4x4>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m21165_gshared (DefaultComparer_t3837 * __this, Matrix4x4_t997  ___x, Matrix4x4_t997  ___y, MethodInfo* method);
#define DefaultComparer_Equals_m21165(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3837 *, Matrix4x4_t997 , Matrix4x4_t997 , MethodInfo*))DefaultComparer_Equals_m21165_gshared)(__this, ___x, ___y, method)
