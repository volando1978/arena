﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// Soomla.Store.VirtualCurrencyPack
struct VirtualCurrencyPack_t78;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.Object
#include "mscorlib_System_Object.h"
// Soomla.Store.GameAssets
struct  GameAssets_t80  : public Object_t
{
};
struct GameAssets_t80_StaticFields{
	// Soomla.Store.VirtualCurrency Soomla.Store.GameAssets::COIN_CURRENCY
	VirtualCurrency_t77 * ___COIN_CURRENCY_13;
	// Soomla.Store.VirtualCurrencyPack Soomla.Store.GameAssets::TWOFIFTY_PACK
	VirtualCurrencyPack_t78 * ___TWOFIFTY_PACK_14;
	// Soomla.Store.VirtualCurrencyPack Soomla.Store.GameAssets::TEN_PACK
	VirtualCurrencyPack_t78 * ___TEN_PACK_15;
	// Soomla.Store.VirtualCurrencyPack Soomla.Store.GameAssets::SEVENTEENFIFTY_PACK
	VirtualCurrencyPack_t78 * ___SEVENTEENFIFTY_PACK_16;
	// Soomla.Store.VirtualCurrencyPack Soomla.Store.GameAssets::FORTY_PACK
	VirtualCurrencyPack_t78 * ___FORTY_PACK_17;
	// Soomla.Store.VirtualCurrencyPack Soomla.Store.GameAssets::HUNDRED_PACK
	VirtualCurrencyPack_t78 * ___HUNDRED_PACK_18;
	// Soomla.Store.VirtualGood Soomla.Store.GameAssets::LASER_GOOD
	VirtualGood_t79 * ___LASER_GOOD_19;
	// Soomla.Store.VirtualGood Soomla.Store.GameAssets::TWAY_GOOD
	VirtualGood_t79 * ___TWAY_GOOD_20;
	// Soomla.Store.VirtualGood Soomla.Store.GameAssets::CIRCLE_GOOD
	VirtualGood_t79 * ___CIRCLE_GOOD_21;
	// Soomla.Store.VirtualGood Soomla.Store.GameAssets::MOIRE_GOOD
	VirtualGood_t79 * ___MOIRE_GOOD_22;
	// Soomla.Store.VirtualGood Soomla.Store.GameAssets::RAYOS_GOOD
	VirtualGood_t79 * ___RAYOS_GOOD_23;
};
