﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Sentinels
struct Sentinels_t472;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.IntPtr GooglePlayGames.Native.Cwrapper.Sentinels::Sentinels_AutomatchingParticipant()
extern "C" IntPtr_t Sentinels_Sentinels_AutomatchingParticipant_m2069 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
