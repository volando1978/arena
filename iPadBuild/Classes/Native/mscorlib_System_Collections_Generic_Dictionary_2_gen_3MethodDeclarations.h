﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct Dictionary_2_t102;
// System.Collections.ICollection
struct ICollection_t1789;
// System.Object
struct Object_t;
// Soomla.Store.StoreInventory/LocalUpgrade
struct LocalUpgrade_t101;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct KeyCollection_t3573;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
struct ValueCollection_t3574;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>[]
struct KeyValuePair_2U5BU5D_t4314;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>>
struct IEnumerator_1_t4315;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1968;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.StoreInventory/LocalUpgrade>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_23MethodDeclarations.h"
#define Dictionary_2__ctor_m982(__this, method) (( void (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2__ctor_m16211_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m17506(__this, ___comparer, method) (( void (*) (Dictionary_2_t102 *, Object_t*, MethodInfo*))Dictionary_2__ctor_m16213_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor(System.Int32)
#define Dictionary_2__ctor_m17507(__this, ___capacity, method) (( void (*) (Dictionary_2_t102 *, int32_t, MethodInfo*))Dictionary_2__ctor_m16215_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m17508(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t102 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2__ctor_m16217_gshared)(__this, ___info, ___context, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m17509(__this, method) (( Object_t * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m16219_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17510(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t102 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16221_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17511(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t102 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16223_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m17512(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t102 *, Object_t *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16225_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m17513(__this, ___key, method) (( void (*) (Dictionary_2_t102 *, Object_t *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16227_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17514(__this, method) (( bool (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16229_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17515(__this, method) (( Object_t * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17516(__this, method) (( bool (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17517(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t102 *, KeyValuePair_2_t3572 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16235_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17518(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t102 *, KeyValuePair_2_t3572 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16237_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17519(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t102 *, KeyValuePair_2U5BU5D_t4314*, int32_t, MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16239_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17520(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t102 *, KeyValuePair_2_t3572 , MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16241_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17521(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t102 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16243_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17522(__this, method) (( Object_t * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16245_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17523(__this, method) (( Object_t* (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16247_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17524(__this, method) (( Object_t * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Count()
#define Dictionary_2_get_Count_m17525(__this, method) (( int32_t (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_get_Count_m16251_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Item(TKey)
#define Dictionary_2_get_Item_m17526(__this, ___key, method) (( LocalUpgrade_t101 * (*) (Dictionary_2_t102 *, String_t*, MethodInfo*))Dictionary_2_get_Item_m16253_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m17527(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t102 *, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_set_Item_m16255_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m17528(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t102 *, int32_t, Object_t*, MethodInfo*))Dictionary_2_Init_m16257_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m17529(__this, ___size, method) (( void (*) (Dictionary_2_t102 *, int32_t, MethodInfo*))Dictionary_2_InitArrays_m16259_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m17530(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t102 *, Array_t *, int32_t, MethodInfo*))Dictionary_2_CopyToCheck_m16261_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m17531(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3572  (*) (Object_t * /* static, unused */, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_make_pair_m16263_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m17532(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_pick_key_m16265_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m17533(__this /* static, unused */, ___key, ___value, method) (( LocalUpgrade_t101 * (*) (Object_t * /* static, unused */, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_pick_value_m16267_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m17534(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t102 *, KeyValuePair_2U5BU5D_t4314*, int32_t, MethodInfo*))Dictionary_2_CopyTo_m16269_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Resize()
#define Dictionary_2_Resize_m17535(__this, method) (( void (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_Resize_m16271_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Add(TKey,TValue)
#define Dictionary_2_Add_m17536(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t102 *, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_Add_m16273_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Clear()
#define Dictionary_2_Clear_m17537(__this, method) (( void (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_Clear_m16275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m17538(__this, ___key, method) (( bool (*) (Dictionary_2_t102 *, String_t*, MethodInfo*))Dictionary_2_ContainsKey_m16277_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m17539(__this, ___value, method) (( bool (*) (Dictionary_2_t102 *, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_ContainsValue_m16279_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m17540(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t102 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))Dictionary_2_GetObjectData_m16281_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m17541(__this, ___sender, method) (( void (*) (Dictionary_2_t102 *, Object_t *, MethodInfo*))Dictionary_2_OnDeserialization_m16283_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::Remove(TKey)
#define Dictionary_2_Remove_m17542(__this, ___key, method) (( bool (*) (Dictionary_2_t102 *, String_t*, MethodInfo*))Dictionary_2_Remove_m16285_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m17543(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t102 *, String_t*, LocalUpgrade_t101 **, MethodInfo*))Dictionary_2_TryGetValue_m16287_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Keys()
#define Dictionary_2_get_Keys_m17544(__this, method) (( KeyCollection_t3573 * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_get_Keys_m16289_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::get_Values()
#define Dictionary_2_get_Values_m17545(__this, method) (( ValueCollection_t3574 * (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_get_Values_m16291_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m17546(__this, ___key, method) (( String_t* (*) (Dictionary_2_t102 *, Object_t *, MethodInfo*))Dictionary_2_ToTKey_m16293_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m17547(__this, ___value, method) (( LocalUpgrade_t101 * (*) (Dictionary_2_t102 *, Object_t *, MethodInfo*))Dictionary_2_ToTValue_m16295_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m17548(__this, ___pair, method) (( bool (*) (Dictionary_2_t102 *, KeyValuePair_2_t3572 , MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16297_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m17549(__this, method) (( Enumerator_t3575  (*) (Dictionary_2_t102 *, MethodInfo*))Dictionary_2_GetEnumerator_m16298_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.StoreInventory/LocalUpgrade>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m17550(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2128  (*) (Object_t * /* static, unused */, String_t*, LocalUpgrade_t101 *, MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16300_gshared)(__this /* static, unused */, ___key, ___value, method)
