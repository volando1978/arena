﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>
struct Action_1_t894;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse
struct SnapshotSelectUIResponse_t705;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m3867(__this, ___object, ___method, method) (( void (*) (Action_1_t894 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::Invoke(T)
#define Action_1_Invoke_m20323(__this, ___obj, method) (( void (*) (Action_1_t894 *, SnapshotSelectUIResponse_t705 *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m20324(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t894 *, SnapshotSelectUIResponse_t705 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<GooglePlayGames.Native.PInvoke.SnapshotManager/SnapshotSelectUIResponse>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m20325(__this, ___result, method) (( void (*) (Action_1_t894 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
