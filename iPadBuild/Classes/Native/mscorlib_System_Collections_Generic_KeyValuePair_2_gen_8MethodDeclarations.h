﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>
struct KeyValuePair_2_t3601;
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17997(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3601 *, String_t*, VirtualItem_t125 *, MethodInfo*))KeyValuePair_2__ctor_m15308_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::get_Key()
#define KeyValuePair_2_get_Key_m17998(__this, method) (( String_t* (*) (KeyValuePair_2_t3601 *, MethodInfo*))KeyValuePair_2_get_Key_m15309_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17999(__this, ___value, method) (( void (*) (KeyValuePair_2_t3601 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m15310_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::get_Value()
#define KeyValuePair_2_get_Value_m18000(__this, method) (( VirtualItem_t125 * (*) (KeyValuePair_2_t3601 *, MethodInfo*))KeyValuePair_2_get_Value_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18001(__this, ___value, method) (( void (*) (KeyValuePair_2_t3601 *, VirtualItem_t125 *, MethodInfo*))KeyValuePair_2_set_Value_m15312_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualItem>::ToString()
#define KeyValuePair_2_ToString_m18002(__this, method) (( String_t* (*) (KeyValuePair_2_t3601 *, MethodInfo*))KeyValuePair_2_ToString_m15313_gshared)(__this, method)
