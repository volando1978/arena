﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.SingleUseVG
struct SingleUseVG_t132;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.SingleUseVG::.ctor(System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void SingleUseVG__ctor_m647 (SingleUseVG_t132 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SingleUseVG::.ctor(JSONObject)
extern "C" void SingleUseVG__ctor_m648 (SingleUseVG_t132 * __this, JSONObject_t30 * ___jsonVg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.SingleUseVG::toJSONObject()
extern "C" JSONObject_t30 * SingleUseVG_toJSONObject_m649 (SingleUseVG_t132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUseVG::Give(System.Int32,System.Boolean)
extern "C" int32_t SingleUseVG_Give_m650 (SingleUseVG_t132 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SingleUseVG::Take(System.Int32,System.Boolean)
extern "C" int32_t SingleUseVG_Take_m651 (SingleUseVG_t132 * __this, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SingleUseVG::canBuy()
extern "C" bool SingleUseVG_canBuy_m652 (SingleUseVG_t132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
