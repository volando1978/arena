﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Resources.NeutralResourcesLanguageAttribute
struct NeutralResourcesLanguageAttribute_t1814;
// System.String
struct String_t;

// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern "C" void NeutralResourcesLanguageAttribute__ctor_m7696 (NeutralResourcesLanguageAttribute_t1814 * __this, String_t* ___cultureName, MethodInfo* method) IL2CPP_METHOD_ATTR;
