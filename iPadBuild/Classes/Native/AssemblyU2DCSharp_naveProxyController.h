﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// naveProxyController
struct  naveProxyController_t820  : public MonoBehaviour_t26
{
	// System.Boolean naveProxyController::working
	bool ___working_2;
};
struct naveProxyController_t820_StaticFields{
	// System.Boolean naveProxyController::gizmosReady
	bool ___gizmosReady_3;
};
