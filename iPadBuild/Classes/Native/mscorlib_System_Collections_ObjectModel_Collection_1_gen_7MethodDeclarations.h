﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t4111;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1670;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4517;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1399;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m24977_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1__ctor_m24977(__this, method) (( void (*) (Collection_1_t4111 *, MethodInfo*))Collection_1__ctor_m24977_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24978_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24978(__this, method) (( bool (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24978_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24979_gshared (Collection_1_t4111 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m24979(__this, ___array, ___index, method) (( void (*) (Collection_1_t4111 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m24979_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m24980_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m24980(__this, method) (( Object_t * (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m24980_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m24981_gshared (Collection_1_t4111 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m24981(__this, ___value, method) (( int32_t (*) (Collection_1_t4111 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m24981_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m24982_gshared (Collection_1_t4111 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m24982(__this, ___value, method) (( bool (*) (Collection_1_t4111 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m24982_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m24983_gshared (Collection_1_t4111 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m24983(__this, ___value, method) (( int32_t (*) (Collection_1_t4111 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m24983_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m24984_gshared (Collection_1_t4111 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m24984(__this, ___index, ___value, method) (( void (*) (Collection_1_t4111 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m24984_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m24985_gshared (Collection_1_t4111 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m24985(__this, ___value, method) (( void (*) (Collection_1_t4111 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m24985_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m24986_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m24986(__this, method) (( bool (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m24986_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m24987_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m24987(__this, method) (( Object_t * (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m24987_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m24988_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m24988(__this, method) (( bool (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m24988_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m24989_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m24989(__this, method) (( bool (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m24989_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m24990_gshared (Collection_1_t4111 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m24990(__this, ___index, method) (( Object_t * (*) (Collection_1_t4111 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m24990_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m24991_gshared (Collection_1_t4111 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m24991(__this, ___index, ___value, method) (( void (*) (Collection_1_t4111 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m24991_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m24992_gshared (Collection_1_t4111 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_Add_m24992(__this, ___item, method) (( void (*) (Collection_1_t4111 *, UILineInfo_t1398 , MethodInfo*))Collection_1_Add_m24992_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m24993_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_Clear_m24993(__this, method) (( void (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_Clear_m24993_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m24994_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m24994(__this, method) (( void (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_ClearItems_m24994_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m24995_gshared (Collection_1_t4111 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_Contains_m24995(__this, ___item, method) (( bool (*) (Collection_1_t4111 *, UILineInfo_t1398 , MethodInfo*))Collection_1_Contains_m24995_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m24996_gshared (Collection_1_t4111 * __this, UILineInfoU5BU5D_t1670* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m24996(__this, ___array, ___index, method) (( void (*) (Collection_1_t4111 *, UILineInfoU5BU5D_t1670*, int32_t, MethodInfo*))Collection_1_CopyTo_m24996_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m24997_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m24997(__this, method) (( Object_t* (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_GetEnumerator_m24997_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m24998_gshared (Collection_1_t4111 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m24998(__this, ___item, method) (( int32_t (*) (Collection_1_t4111 *, UILineInfo_t1398 , MethodInfo*))Collection_1_IndexOf_m24998_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m24999_gshared (Collection_1_t4111 * __this, int32_t ___index, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_Insert_m24999(__this, ___index, ___item, method) (( void (*) (Collection_1_t4111 *, int32_t, UILineInfo_t1398 , MethodInfo*))Collection_1_Insert_m24999_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25000_gshared (Collection_1_t4111 * __this, int32_t ___index, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m25000(__this, ___index, ___item, method) (( void (*) (Collection_1_t4111 *, int32_t, UILineInfo_t1398 , MethodInfo*))Collection_1_InsertItem_m25000_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m25001_gshared (Collection_1_t4111 * __this, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_Remove_m25001(__this, ___item, method) (( bool (*) (Collection_1_t4111 *, UILineInfo_t1398 , MethodInfo*))Collection_1_Remove_m25001_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25002_gshared (Collection_1_t4111 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m25002(__this, ___index, method) (( void (*) (Collection_1_t4111 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m25002_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25003_gshared (Collection_1_t4111 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m25003(__this, ___index, method) (( void (*) (Collection_1_t4111 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m25003_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25004_gshared (Collection_1_t4111 * __this, MethodInfo* method);
#define Collection_1_get_Count_m25004(__this, method) (( int32_t (*) (Collection_1_t4111 *, MethodInfo*))Collection_1_get_Count_m25004_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t1398  Collection_1_get_Item_m25005_gshared (Collection_1_t4111 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m25005(__this, ___index, method) (( UILineInfo_t1398  (*) (Collection_1_t4111 *, int32_t, MethodInfo*))Collection_1_get_Item_m25005_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25006_gshared (Collection_1_t4111 * __this, int32_t ___index, UILineInfo_t1398  ___value, MethodInfo* method);
#define Collection_1_set_Item_m25006(__this, ___index, ___value, method) (( void (*) (Collection_1_t4111 *, int32_t, UILineInfo_t1398 , MethodInfo*))Collection_1_set_Item_m25006_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25007_gshared (Collection_1_t4111 * __this, int32_t ___index, UILineInfo_t1398  ___item, MethodInfo* method);
#define Collection_1_SetItem_m25007(__this, ___index, ___item, method) (( void (*) (Collection_1_t4111 *, int32_t, UILineInfo_t1398 , MethodInfo*))Collection_1_SetItem_m25007_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25008_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m25008(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m25008_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t1398  Collection_1_ConvertItem_m25009_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m25009(__this /* static, unused */, ___item, method) (( UILineInfo_t1398  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m25009_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25010_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m25010(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m25010_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25011_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m25011(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m25011_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25012_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m25012(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m25012_gshared)(__this /* static, unused */, ___list, method)
