﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A
struct U3CCancelU3Ec__AnonStorey5A_t649;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::.ctor()
extern "C" void U3CCancelU3Ec__AnonStorey5A__ctor_m2573 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::<>m__64(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch)
extern "C" void U3CCancelU3Ec__AnonStorey5A_U3CU3Em__64_m2574 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, NativeTurnBasedMatch_t680 * ___foundMatch, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<Cancel>c__AnonStorey5A::<>m__6D(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus)
extern "C" void U3CCancelU3Ec__AnonStorey5A_U3CU3Em__6D_m2575 (U3CCancelU3Ec__AnonStorey5A_t649 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
