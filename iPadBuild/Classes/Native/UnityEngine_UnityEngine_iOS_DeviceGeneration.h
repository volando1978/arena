﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.iOS.DeviceGeneration
#include "UnityEngine_UnityEngine_iOS_DeviceGeneration.h"
// UnityEngine.iOS.DeviceGeneration
struct  DeviceGeneration_t995 
{
	// System.Int32 UnityEngine.iOS.DeviceGeneration::value__
	int32_t ___value___1;
};
