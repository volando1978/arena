﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// gameControl/<ChangeLevelWait>c__IteratorC
struct  U3CChangeLevelWaitU3Ec__IteratorC_t800  : public Object_t
{
	// System.Single gameControl/<ChangeLevelWait>c__IteratorC::<timer>__0
	float ___U3CtimerU3E__0_0;
	// System.Int32 gameControl/<ChangeLevelWait>c__IteratorC::$PC
	int32_t ___U24PC_1;
	// System.Object gameControl/<ChangeLevelWait>c__IteratorC::$current
	Object_t * ___U24current_2;
};
