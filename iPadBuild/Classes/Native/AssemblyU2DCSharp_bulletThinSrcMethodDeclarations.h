﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// bulletThinSrc
struct bulletThinSrc_t768;
// UnityEngine.Collider
struct Collider_t900;

// System.Void bulletThinSrc::.ctor()
extern "C" void bulletThinSrc__ctor_m3323 (bulletThinSrc_t768 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletThinSrc::Awake()
extern "C" void bulletThinSrc_Awake_m3324 (bulletThinSrc_t768 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletThinSrc::OnExplosion()
extern "C" void bulletThinSrc_OnExplosion_m3325 (bulletThinSrc_t768 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletThinSrc::OnTriggerEnter(UnityEngine.Collider)
extern "C" void bulletThinSrc_OnTriggerEnter_m3326 (bulletThinSrc_t768 * __this, Collider_t900 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletThinSrc::leavePow()
extern "C" void bulletThinSrc_leavePow_m3327 (bulletThinSrc_t768 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
