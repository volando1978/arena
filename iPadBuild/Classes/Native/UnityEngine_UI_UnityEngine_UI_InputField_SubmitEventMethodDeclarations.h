﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t1254;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
extern "C" void SubmitEvent__ctor_m5038 (SubmitEvent_t1254 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
