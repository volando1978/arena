﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t2578;

// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern "C" void PreserveSigAttribute__ctor_m12488 (PreserveSigAttribute_t2578 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
