﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup
struct CanvasGroup_t1387;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.CanvasGroup>
struct  Action_1_t4024  : public MulticastDelegate_t22
{
};
