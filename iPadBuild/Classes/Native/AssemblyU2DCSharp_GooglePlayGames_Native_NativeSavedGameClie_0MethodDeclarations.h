﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/Prefetcher
struct Prefetcher_t615;
// System.Action`2<System.Byte[],System.Byte[]>
struct Action_2_t614;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse
struct ReadResponse_t704;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa.h"

// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::.ctor(System.Action`2<System.Byte[],System.Byte[]>,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void Prefetcher__ctor_m2499 (Prefetcher_t615 * __this, Action_2_t614 * ___dataFetchedCallback, Action_2_t612 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::OnOriginalDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern "C" void Prefetcher_OnOriginalDataRead_m2500 (Prefetcher_t615 * __this, ReadResponse_t704 * ___readResponse, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::OnUnmergedDataRead(GooglePlayGames.Native.PInvoke.SnapshotManager/ReadResponse)
extern "C" void Prefetcher_OnUnmergedDataRead_m2501 (Prefetcher_t615 * __this, ReadResponse_t704 * ___readResponse, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::MaybeProceed()
extern "C" void Prefetcher_MaybeProceed_m2502 (Prefetcher_t615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::<OnOriginalDataRead>m__55(GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void Prefetcher_U3COnOriginalDataReadU3Em__55_m2503 (Object_t * __this /* static, unused */, int32_t p0, Object_t * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/Prefetcher::<OnUnmergedDataRead>m__56(GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void Prefetcher_U3COnUnmergedDataReadU3Em__56_m2504 (Object_t * __this /* static, unused */, int32_t p0, Object_t * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
