﻿#pragma once
#include <stdint.h>
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>[]
struct UnityKeyValuePair_2U5BU5D_t3464;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct  List_1_t3465  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::_items
	UnityKeyValuePair_2U5BU5D_t3464* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::_version
	int32_t ____version_3;
};
struct List_1_t3465_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>::EmptyArray
	UnityKeyValuePair_2U5BU5D_t3464* ___EmptyArray_4;
};
