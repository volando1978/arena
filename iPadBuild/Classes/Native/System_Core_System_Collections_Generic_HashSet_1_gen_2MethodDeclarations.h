﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t3762;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t4388;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3761;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4305;
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Collections.Generic.HashSet`1<System.Int32>::.ctor()
extern "C" void HashSet_1__ctor_m20229_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1__ctor_m20229(__this, method) (( void (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1__ctor_m20229_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1__ctor_m20231_gshared (HashSet_1_t3762 * __this, Object_t* ___collection, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1__ctor_m20231(__this, ___collection, ___comparer, method) (( void (*) (HashSet_1_t3762 *, Object_t*, Object_t*, MethodInfo*))HashSet_1__ctor_m20231_gshared)(__this, ___collection, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m20233_gshared (HashSet_1_t3762 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define HashSet_1__ctor_m20233(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3762 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1__ctor_m20233_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235(__this, method) (( Object_t* (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20235_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237(__this, method) (( bool (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20237_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239_gshared (HashSet_1_t3762 * __this, Int32U5BU5D_t107* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3762 *, Int32U5BU5D_t107*, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m20239_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared (HashSet_1_t3762 * __this, int32_t ___item, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241(__this, ___item, method) (( void (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243(__this, method) (( Object_t * (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m20243_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m20245_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_get_Count_m20245(__this, method) (( int32_t (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_get_Count_m20245_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m20247_gshared (HashSet_1_t3762 * __this, int32_t ___capacity, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1_Init_m20247(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t3762 *, int32_t, Object_t*, MethodInfo*))HashSet_1_Init_m20247_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m20249_gshared (HashSet_1_t3762 * __this, int32_t ___size, MethodInfo* method);
#define HashSet_1_InitArrays_m20249(__this, ___size, method) (( void (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_InitArrays_m20249_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m20251_gshared (HashSet_1_t3762 * __this, int32_t ___index, int32_t ___hash, int32_t ___item, MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m20251(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t3762 *, int32_t, int32_t, int32_t, MethodInfo*))HashSet_1_SlotsContainsAt_m20251_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m20253_gshared (HashSet_1_t3762 * __this, Int32U5BU5D_t107* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_CopyTo_m20253(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3762 *, Int32U5BU5D_t107*, int32_t, MethodInfo*))HashSet_1_CopyTo_m20253_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m20255_gshared (HashSet_1_t3762 * __this, Int32U5BU5D_t107* ___array, int32_t ___index, int32_t ___count, MethodInfo* method);
#define HashSet_1_CopyTo_m20255(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t3762 *, Int32U5BU5D_t107*, int32_t, int32_t, MethodInfo*))HashSet_1_CopyTo_m20255_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Resize()
extern "C" void HashSet_1_Resize_m20257_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_Resize_m20257(__this, method) (( void (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_Resize_m20257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m20259_gshared (HashSet_1_t3762 * __this, int32_t ___index, MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m20259(__this, ___index, method) (( int32_t (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_GetLinkHashCode_m20259_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int32>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m20261_gshared (HashSet_1_t3762 * __this, int32_t ___item, MethodInfo* method);
#define HashSet_1_GetItemHashCode_m20261(__this, ___item, method) (( int32_t (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_GetItemHashCode_m20261_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Add(T)
extern "C" bool HashSet_1_Add_m20262_gshared (HashSet_1_t3762 * __this, int32_t ___item, MethodInfo* method);
#define HashSet_1_Add_m20262(__this, ___item, method) (( bool (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_Add_m20262_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::Clear()
extern "C" void HashSet_1_Clear_m20264_gshared (HashSet_1_t3762 * __this, MethodInfo* method);
#define HashSet_1_Clear_m20264(__this, method) (( void (*) (HashSet_1_t3762 *, MethodInfo*))HashSet_1_Clear_m20264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Contains(T)
extern "C" bool HashSet_1_Contains_m20266_gshared (HashSet_1_t3762 * __this, int32_t ___item, MethodInfo* method);
#define HashSet_1_Contains_m20266(__this, ___item, method) (( bool (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_Contains_m20266_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int32>::Remove(T)
extern "C" bool HashSet_1_Remove_m20268_gshared (HashSet_1_t3762 * __this, int32_t ___item, MethodInfo* method);
#define HashSet_1_Remove_m20268(__this, ___item, method) (( bool (*) (HashSet_1_t3762 *, int32_t, MethodInfo*))HashSet_1_Remove_m20268_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m20270_gshared (HashSet_1_t3762 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method);
#define HashSet_1_GetObjectData_m20270(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3762 *, SerializationInfo_t1673 *, StreamingContext_t1674 , MethodInfo*))HashSet_1_GetObjectData_m20270_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m20272_gshared (HashSet_1_t3762 * __this, Object_t * ___sender, MethodInfo* method);
#define HashSet_1_OnDeserialization_m20272(__this, ___sender, method) (( void (*) (HashSet_1_t3762 *, Object_t *, MethodInfo*))HashSet_1_OnDeserialization_m20272_gshared)(__this, ___sender, method)
