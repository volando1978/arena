﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t1268;
struct Coroutine_t1268_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m6363 (Coroutine_t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m6364 (Coroutine_t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m6365 (Coroutine_t1268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t1268_marshal(const Coroutine_t1268& unmarshaled, Coroutine_t1268_marshaled& marshaled);
void Coroutine_t1268_marshal_back(const Coroutine_t1268_marshaled& marshaled, Coroutine_t1268& unmarshaled);
void Coroutine_t1268_marshal_cleanup(Coroutine_t1268_marshaled& marshaled);
