﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3439;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t4263;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4259;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t4258;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t3442;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3438;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t3447;
// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t3448;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparison_1_t3451;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void List_1__ctor_m15617_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1__ctor_m15617(__this, method) (( void (*) (List_1_t3439 *, MethodInfo*))List_1__ctor_m15617_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15618_gshared (List_1_t3439 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m15618(__this, ___collection, method) (( void (*) (List_1_t3439 *, Object_t*, MethodInfo*))List_1__ctor_m15618_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15619_gshared (List_1_t3439 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m15619(__this, ___capacity, method) (( void (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1__ctor_m15619_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C" void List_1__cctor_m15620_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m15620(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15620_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15621_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15621(__this, method) (( Object_t* (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15621_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15622_gshared (List_1_t3439 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15622(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3439 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15622_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15623_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15623(__this, method) (( Object_t * (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15623_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15624_gshared (List_1_t3439 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15624(__this, ___item, method) (( int32_t (*) (List_1_t3439 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15624_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15625_gshared (List_1_t3439 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15625(__this, ___item, method) (( bool (*) (List_1_t3439 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15625_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15626_gshared (List_1_t3439 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15626(__this, ___item, method) (( int32_t (*) (List_1_t3439 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15626_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15627_gshared (List_1_t3439 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15627(__this, ___index, ___item, method) (( void (*) (List_1_t3439 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15627_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15628_gshared (List_1_t3439 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15628(__this, ___item, method) (( void (*) (List_1_t3439 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15628_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15629_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15629(__this, method) (( bool (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15630_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15630(__this, method) (( bool (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15630_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15631_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15631(__this, method) (( Object_t * (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15631_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15632_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15632(__this, method) (( bool (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15632_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15633_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15633(__this, method) (( bool (*) (List_1_t3439 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15633_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15634_gshared (List_1_t3439 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15634(__this, ___index, method) (( Object_t * (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15634_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15635_gshared (List_1_t3439 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15635(__this, ___index, ___value, method) (( void (*) (List_1_t3439 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15635_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C" void List_1_Add_m15636_gshared (List_1_t3439 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define List_1_Add_m15636(__this, ___item, method) (( void (*) (List_1_t3439 *, KeyValuePair_2_t3407 , MethodInfo*))List_1_Add_m15636_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15637_gshared (List_1_t3439 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m15637(__this, ___newCount, method) (( void (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15637_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15638_gshared (List_1_t3439 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m15638(__this, ___collection, method) (( void (*) (List_1_t3439 *, Object_t*, MethodInfo*))List_1_AddCollection_m15638_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15639_gshared (List_1_t3439 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m15639(__this, ___enumerable, method) (( void (*) (List_1_t3439 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15639_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15640_gshared (List_1_t3439 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m15640(__this, ___collection, method) (( void (*) (List_1_t3439 *, Object_t*, MethodInfo*))List_1_AddRange_m15640_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3442 * List_1_AsReadOnly_m15641_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m15641(__this, method) (( ReadOnlyCollection_1_t3442 * (*) (List_1_t3439 *, MethodInfo*))List_1_AsReadOnly_m15641_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void List_1_Clear_m15642_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_Clear_m15642(__this, method) (( void (*) (List_1_t3439 *, MethodInfo*))List_1_Clear_m15642_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C" bool List_1_Contains_m15643_gshared (List_1_t3439 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define List_1_Contains_m15643(__this, ___item, method) (( bool (*) (List_1_t3439 *, KeyValuePair_2_t3407 , MethodInfo*))List_1_Contains_m15643_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15644_gshared (List_1_t3439 * __this, KeyValuePair_2U5BU5D_t3438* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m15644(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3439 *, KeyValuePair_2U5BU5D_t3438*, int32_t, MethodInfo*))List_1_CopyTo_m15644_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Find(System.Predicate`1<T>)
extern "C" KeyValuePair_2_t3407  List_1_Find_m15645_gshared (List_1_t3439 * __this, Predicate_1_t3447 * ___match, MethodInfo* method);
#define List_1_Find_m15645(__this, ___match, method) (( KeyValuePair_2_t3407  (*) (List_1_t3439 *, Predicate_1_t3447 *, MethodInfo*))List_1_Find_m15645_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15646_gshared (Object_t * __this /* static, unused */, Predicate_1_t3447 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m15646(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3447 *, MethodInfo*))List_1_CheckMatch_m15646_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m15647_gshared (List_1_t3439 * __this, Predicate_1_t3447 * ___match, MethodInfo* method);
#define List_1_FindIndex_m15647(__this, ___match, method) (( int32_t (*) (List_1_t3439 *, Predicate_1_t3447 *, MethodInfo*))List_1_FindIndex_m15647_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15648_gshared (List_1_t3439 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3447 * ___match, MethodInfo* method);
#define List_1_GetIndex_m15648(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3439 *, int32_t, int32_t, Predicate_1_t3447 *, MethodInfo*))List_1_GetIndex_m15648_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m15649_gshared (List_1_t3439 * __this, Action_1_t3448 * ___action, MethodInfo* method);
#define List_1_ForEach_m15649(__this, ___action, method) (( void (*) (List_1_t3439 *, Action_1_t3448 *, MethodInfo*))List_1_ForEach_m15649_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t3440  List_1_GetEnumerator_m15650_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m15650(__this, method) (( Enumerator_t3440  (*) (List_1_t3439 *, MethodInfo*))List_1_GetEnumerator_m15650_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15651_gshared (List_1_t3439 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define List_1_IndexOf_m15651(__this, ___item, method) (( int32_t (*) (List_1_t3439 *, KeyValuePair_2_t3407 , MethodInfo*))List_1_IndexOf_m15651_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15652_gshared (List_1_t3439 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m15652(__this, ___start, ___delta, method) (( void (*) (List_1_t3439 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15652_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15653_gshared (List_1_t3439 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m15653(__this, ___index, method) (( void (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_CheckIndex_m15653_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15654_gshared (List_1_t3439 * __this, int32_t ___index, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define List_1_Insert_m15654(__this, ___index, ___item, method) (( void (*) (List_1_t3439 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))List_1_Insert_m15654_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15655_gshared (List_1_t3439 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m15655(__this, ___collection, method) (( void (*) (List_1_t3439 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15655_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C" bool List_1_Remove_m15656_gshared (List_1_t3439 * __this, KeyValuePair_2_t3407  ___item, MethodInfo* method);
#define List_1_Remove_m15656(__this, ___item, method) (( bool (*) (List_1_t3439 *, KeyValuePair_2_t3407 , MethodInfo*))List_1_Remove_m15656_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15657_gshared (List_1_t3439 * __this, Predicate_1_t3447 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m15657(__this, ___match, method) (( int32_t (*) (List_1_t3439 *, Predicate_1_t3447 *, MethodInfo*))List_1_RemoveAll_m15657_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15658_gshared (List_1_t3439 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m15658(__this, ___index, method) (( void (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_RemoveAt_m15658_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reverse()
extern "C" void List_1_Reverse_m15659_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_Reverse_m15659(__this, method) (( void (*) (List_1_t3439 *, MethodInfo*))List_1_Reverse_m15659_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort()
extern "C" void List_1_Sort_m15660_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_Sort_m15660(__this, method) (( void (*) (List_1_t3439 *, MethodInfo*))List_1_Sort_m15660_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15661_gshared (List_1_t3439 * __this, Comparison_1_t3451 * ___comparison, MethodInfo* method);
#define List_1_Sort_m15661(__this, ___comparison, method) (( void (*) (List_1_t3439 *, Comparison_1_t3451 *, MethodInfo*))List_1_Sort_m15661_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C" KeyValuePair_2U5BU5D_t3438* List_1_ToArray_m15662_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_ToArray_m15662(__this, method) (( KeyValuePair_2U5BU5D_t3438* (*) (List_1_t3439 *, MethodInfo*))List_1_ToArray_m15662_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TrimExcess()
extern "C" void List_1_TrimExcess_m15663_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_TrimExcess_m15663(__this, method) (( void (*) (List_1_t3439 *, MethodInfo*))List_1_TrimExcess_m15663_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15664_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_get_Capacity_m15664(__this, method) (( int32_t (*) (List_1_t3439 *, MethodInfo*))List_1_get_Capacity_m15664_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15665_gshared (List_1_t3439 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m15665(__this, ___value, method) (( void (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_set_Capacity_m15665_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t List_1_get_Count_m15666_gshared (List_1_t3439 * __this, MethodInfo* method);
#define List_1_get_Count_m15666(__this, method) (( int32_t (*) (List_1_t3439 *, MethodInfo*))List_1_get_Count_m15666_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern "C" KeyValuePair_2_t3407  List_1_get_Item_m15667_gshared (List_1_t3439 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m15667(__this, ___index, method) (( KeyValuePair_2_t3407  (*) (List_1_t3439 *, int32_t, MethodInfo*))List_1_get_Item_m15667_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15668_gshared (List_1_t3439 * __this, int32_t ___index, KeyValuePair_2_t3407  ___value, MethodInfo* method);
#define List_1_set_Item_m15668(__this, ___index, ___value, method) (( void (*) (List_1_t3439 *, int32_t, KeyValuePair_2_t3407 , MethodInfo*))List_1_set_Item_m15668_gshared)(__this, ___index, ___value, method)
