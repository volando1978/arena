﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Action>
struct List_1_t393;
// System.Object
struct Object_t;
// System.Action
struct Action_t588;
// System.Collections.Generic.IEnumerable`1<System.Action>
struct IEnumerable_1_t4371;
// System.Collections.Generic.IEnumerator`1<System.Action>
struct IEnumerator_1_t4372;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<System.Action>
struct ICollection_1_t4373;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Action>
struct ReadOnlyCollection_1_t3702;
// System.Action[]
struct ActionU5BU5D_t3700;
// System.Predicate`1<System.Action>
struct Predicate_1_t3703;
// System.Action`1<System.Action>
struct Action_1_t394;
// System.Comparison`1<System.Action>
struct Comparison_1_t3705;
// System.Collections.Generic.List`1/Enumerator<System.Action>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_23.h"

// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m3732(__this, method) (( void (*) (List_1_t393 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19513(__this, ___collection, method) (( void (*) (List_1_t393 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor(System.Int32)
#define List_1__ctor_m19514(__this, ___capacity, method) (( void (*) (List_1_t393 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Action>::.cctor()
#define List_1__cctor_m19515(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Action>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19516(__this, method) (( Object_t* (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19517(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t393 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Action>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19518(__this, method) (( Object_t * (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19519(__this, ___item, method) (( int32_t (*) (List_1_t393 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19520(__this, ___item, method) (( bool (*) (List_1_t393 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19521(__this, ___item, method) (( int32_t (*) (List_1_t393 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19522(__this, ___index, ___item, method) (( void (*) (List_1_t393 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19523(__this, ___item, method) (( void (*) (List_1_t393 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19524(__this, method) (( bool (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19525(__this, method) (( bool (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Action>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19526(__this, method) (( Object_t * (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19527(__this, method) (( bool (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19528(__this, method) (( bool (*) (List_1_t393 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Action>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19529(__this, ___index, method) (( Object_t * (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Action>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19530(__this, ___index, ___value, method) (( void (*) (List_1_t393 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Add(T)
#define List_1_Add_m19531(__this, ___item, method) (( void (*) (List_1_t393 *, Action_t588 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19532(__this, ___newCount, method) (( void (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Action>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19533(__this, ___collection, method) (( void (*) (List_1_t393 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Action>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19534(__this, ___enumerable, method) (( void (*) (List_1_t393 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Action>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3736(__this, ___collection, method) (( void (*) (List_1_t393 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Action>::AsReadOnly()
#define List_1_AsReadOnly_m19535(__this, method) (( ReadOnlyCollection_1_t3702 * (*) (List_1_t393 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Clear()
#define List_1_Clear_m19536(__this, method) (( void (*) (List_1_t393 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::Contains(T)
#define List_1_Contains_m19537(__this, ___item, method) (( bool (*) (List_1_t393 *, Action_t588 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19538(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t393 *, ActionU5BU5D_t3700*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Action>::Find(System.Predicate`1<T>)
#define List_1_Find_m19539(__this, ___match, method) (( Action_t588 * (*) (List_1_t393 *, Predicate_1_t3703 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Action>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19540(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3703 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m19541(__this, ___match, method) (( int32_t (*) (List_1_t393 *, Predicate_1_t3703 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19542(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t393 *, int32_t, int32_t, Predicate_1_t3703 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Action>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3738(__this, ___action, method) (( void (*) (List_1_t393 *, Action_1_t394 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Action>::GetEnumerator()
#define List_1_GetEnumerator_m19543(__this, method) (( Enumerator_t3704  (*) (List_1_t393 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::IndexOf(T)
#define List_1_IndexOf_m19544(__this, ___item, method) (( int32_t (*) (List_1_t393 *, Action_t588 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19545(__this, ___start, ___delta, method) (( void (*) (List_1_t393 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Action>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19546(__this, ___index, method) (( void (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Insert(System.Int32,T)
#define List_1_Insert_m19547(__this, ___index, ___item, method) (( void (*) (List_1_t393 *, int32_t, Action_t588 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Action>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19548(__this, ___collection, method) (( void (*) (List_1_t393 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Action>::Remove(T)
#define List_1_Remove_m19549(__this, ___item, method) (( bool (*) (List_1_t393 *, Action_t588 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19550(__this, ___match, method) (( int32_t (*) (List_1_t393 *, Predicate_1_t3703 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Action>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19551(__this, ___index, method) (( void (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Reverse()
#define List_1_Reverse_m19552(__this, method) (( void (*) (List_1_t393 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Sort()
#define List_1_Sort_m19553(__this, method) (( void (*) (List_1_t393 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19554(__this, ___comparison, method) (( void (*) (List_1_t393 *, Comparison_1_t3705 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Action>::ToArray()
#define List_1_ToArray_m19555(__this, method) (( ActionU5BU5D_t3700* (*) (List_1_t393 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::TrimExcess()
#define List_1_TrimExcess_m19556(__this, method) (( void (*) (List_1_t393 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Capacity()
#define List_1_get_Capacity_m19557(__this, method) (( int32_t (*) (List_1_t393 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19558(__this, ___value, method) (( void (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
#define List_1_get_Count_m19559(__this, method) (( int32_t (*) (List_1_t393 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Action>::get_Item(System.Int32)
#define List_1_get_Item_m19560(__this, ___index, method) (( Action_t588 * (*) (List_1_t393 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Action>::set_Item(System.Int32,T)
#define List_1_set_Item_m19561(__this, ___index, ___value, method) (( void (*) (List_1_t393 *, int32_t, Action_t588 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
