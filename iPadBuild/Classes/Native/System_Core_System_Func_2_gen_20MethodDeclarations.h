﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>
struct Func_2_t958;
// System.Object
struct Object_t;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.UIntPtr,System.Object>
#include "System_Core_System_Func_2_gen_43MethodDeclarations.h"
#define Func_2__ctor_m3965(__this, ___object, ___method, method) (( void (*) (Func_2_t958 *, Object_t *, IntPtr_t, MethodInfo*))Func_2__ctor_m20391_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::Invoke(T)
#define Func_2_Invoke_m20876(__this, ___arg1, method) (( MultiplayerInvitation_t601 * (*) (Func_2_t958 *, UIntPtr_t , MethodInfo*))Func_2_Invoke_m20393_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m20877(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t958 *, UIntPtr_t , AsyncCallback_t20 *, Object_t *, MethodInfo*))Func_2_BeginInvoke_m20395_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.UIntPtr,GooglePlayGames.Native.PInvoke.MultiplayerInvitation>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m20878(__this, ___result, method) (( MultiplayerInvitation_t601 * (*) (Func_2_t958 *, Object_t *, MethodInfo*))Func_2_EndInvoke_m20397_gshared)(__this, ___result, method)
