﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22
struct U3CShowLeaderboardUIU3Ec__AnonStorey22_t532;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22::.ctor()
extern "C" void U3CShowLeaderboardUIU3Ec__AnonStorey22__ctor_m2251 (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeClient/<ShowLeaderboardUI>c__AnonStorey22::<>m__1A(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" void U3CShowLeaderboardUIU3Ec__AnonStorey22_U3CU3Em__1A_m2252 (U3CShowLeaderboardUIU3Ec__AnonStorey22_t532 * __this, int32_t ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
