﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27
struct U3CFetchMatchingStateU3Ec__AnonStorey27_t553;
// GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse
struct FetchListResponse_t689;

// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27::.ctor()
extern "C" void U3CFetchMatchingStateU3Ec__AnonStorey27__ctor_m2301 (U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient/<FetchMatchingState>c__AnonStorey27::<>m__1F(GooglePlayGames.Native.PInvoke.QuestManager/FetchListResponse)
extern "C" void U3CFetchMatchingStateU3Ec__AnonStorey27_U3CU3Em__1F_m2302 (U3CFetchMatchingStateU3Ec__AnonStorey27_t553 * __this, FetchListResponse_t689 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
