﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DllNotFoundException
struct DllNotFoundException_t2799;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.DllNotFoundException::.ctor()
extern "C" void DllNotFoundException__ctor_m13973 (DllNotFoundException_t2799 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DllNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DllNotFoundException__ctor_m13974 (DllNotFoundException_t2799 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
