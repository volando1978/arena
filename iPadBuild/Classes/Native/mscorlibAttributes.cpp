﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* TypeLibVersionAttribute_t2580_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* StringFreezingAttribute_t2562_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultDependencyAttribute_t2559_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t1809_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var;
void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeLibVersionAttribute_t2580_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5106);
		AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2343);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		StringFreezingAttribute_t2562_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5107);
		DefaultDependencyAttribute_t2559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5108);
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3628);
		DebuggableAttribute_t1809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3626);
		AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2345);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2346);
		AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2348);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3627);
		AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3623);
		AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2347);
		AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3625);
		AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3629);
		AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2342);
		SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3624);
		NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3631);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 21;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibVersionAttribute_t2580 * tmp;
		tmp = (TypeLibVersionAttribute_t2580 *)il2cpp_codegen_object_new (TypeLibVersionAttribute_t2580_il2cpp_TypeInfo_var);
		TypeLibVersionAttribute__ctor_m12499(tmp, 2, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t1419 * tmp;
		tmp = (AssemblyDescriptionAttribute_t1419 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t1419_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m6175(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		StringFreezingAttribute_t2562 * tmp;
		tmp = (StringFreezingAttribute_t2562 *)il2cpp_codegen_object_new (StringFreezingAttribute_t2562_il2cpp_TypeInfo_var);
		StringFreezingAttribute__ctor_m12459(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		DefaultDependencyAttribute_t2559 * tmp;
		tmp = (DefaultDependencyAttribute_t2559 *)il2cpp_codegen_object_new (DefaultDependencyAttribute_t2559_il2cpp_TypeInfo_var);
		DefaultDependencyAttribute__ctor_m12458(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1811 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1811 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1811_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7693(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t1809 * tmp;
		tmp = (DebuggableAttribute_t1809 *)il2cpp_codegen_object_new (DebuggableAttribute_t1809_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m7691(tmp, 2, NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t1421 * tmp;
		tmp = (AssemblyCompanyAttribute_t1421 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t1421_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m6177(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t1422 * tmp;
		tmp = (AssemblyProductAttribute_t1422 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t1422_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m6178(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t1424 * tmp;
		tmp = (AssemblyFileVersionAttribute_t1424 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t1424_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m6180(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t1810 * tmp;
		tmp = (CompilationRelaxationsAttribute_t1810 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t1810_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7692(tmp, 8, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1806 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1806 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1806_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7688(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t1423 * tmp;
		tmp = (AssemblyCopyrightAttribute_t1423 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t1423_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m6179(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1808 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1808 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1808_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7690(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1812 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1812 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1812_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7694(tmp, true, NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t1418 * tmp;
		tmp = (AssemblyTitleAttribute_t1418 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t1418_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m6174(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1807 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1807 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1807_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7689(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[19] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1814 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1814 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1814_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7696(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[20] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object__ctor_m830(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_Finalize_m1177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m3694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ValueType_t329_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
extern const Il2CppType* _Attribute_t1731_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Attribute_t1546_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Attribute_t1731_0_0_0_var = il2cpp_codegen_type_from_index(3161);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Attribute_t1731_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
extern const Il2CppType* Attribute_t1546_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _Attribute_t1731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Attribute_t1546_0_0_0_var = il2cpp_codegen_type_from_index(3162);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Attribute_t1546_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("917B14D0-2D9E-38B8-92A9-381ACF52F7C0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Int32_t189_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IFormattable_t312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void IConvertible_t313_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IComparable_t314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SerializableAttribute_t2334_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4124, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AttributeUsageAttribute_t1705_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ComVisibleAttribute_t1426_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 5597, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Int64_t233_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt32_t235_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt32_t235_CustomAttributesCacheGenerator_UInt32_Parse_m10039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt32_t235_CustomAttributesCacheGenerator_UInt32_Parse_m10040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt32_t235_CustomAttributesCacheGenerator_UInt32_TryParse_m8876(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt32_t235_CustomAttributesCacheGenerator_UInt32_TryParse_m10041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CLSCompliantAttribute_t1813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt64_t239_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt64_t239_CustomAttributesCacheGenerator_UInt64_Parse_m10066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt64_t239_CustomAttributesCacheGenerator_UInt64_Parse_m10068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt64_t239_CustomAttributesCacheGenerator_UInt64_TryParse_m10069(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Byte_t237_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SByte_t236_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SByte_t236_CustomAttributesCacheGenerator_SByte_Parse_m10119(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SByte_t236_CustomAttributesCacheGenerator_SByte_Parse_m10120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SByte_t236_CustomAttributesCacheGenerator_SByte_TryParse_m10121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Int16_t238_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt16_t194_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt16_t194_CustomAttributesCacheGenerator_UInt16_Parse_m10174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt16_t194_CustomAttributesCacheGenerator_UInt16_Parse_m10175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt16_t194_CustomAttributesCacheGenerator_UInt16_TryParse_m10176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UInt16_t194_CustomAttributesCacheGenerator_UInt16_TryParse_m10177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void IEnumerator_t37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("496B0ABF-CDEE-11D3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void IEnumerable_t38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("496B0ABE-CDEE-11d3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern TypeInfo* DispIdAttribute_t2574_il2cpp_TypeInfo_var;
void IEnumerable_t38_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DispIdAttribute_t2574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5114);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DispIdAttribute_t2574 * tmp;
		tmp = (DispIdAttribute_t2574 *)il2cpp_codegen_object_new (DispIdAttribute_t2574_il2cpp_TypeInfo_var);
		DispIdAttribute__ctor_m12465(tmp, -4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IDisposable_t191_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Char_t193_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String__ctor_m10212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m10235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m10236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Split_m1065_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoDocumentationNoteAttribute_t2356_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m10240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoDocumentationNoteAttribute_t2356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5115);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoDocumentationNoteAttribute_t2356 * tmp;
		tmp = (MonoDocumentationNoteAttribute_t2356 *)il2cpp_codegen_object_new (MonoDocumentationNoteAttribute_t2356_il2cpp_TypeInfo_var);
		MonoDocumentationNoteAttribute__ctor_m10736(tmp, il2cpp_codegen_string_new_wrapper("code should be moved to managed"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m10241(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m8795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m903_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m8878_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m8793_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m3689_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m9871_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m10272_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m976_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m4083_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_GetHashCode_m10280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICloneable_t309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Single_t202_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Single_t202_CustomAttributesCacheGenerator_Single_IsNaN_m876(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Double_t234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Double_t234_CustomAttributesCacheGenerator_Double_IsNaN_m10342(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
extern TypeInfo* DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2347 * tmp;
		tmp = (DecimalConstantAttribute_t2347 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10721(tmp, 0, 255, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2347 * tmp;
		tmp = (DecimalConstantAttribute_t2347 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10721(tmp, 0, 0, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_MinusOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2347 * tmp;
		tmp = (DecimalConstantAttribute_t2347 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10721(tmp, 0, 255, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_One(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5116);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2347 * tmp;
		tmp = (DecimalConstantAttribute_t2347 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2347_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10721(tmp, 0, 0, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal__ctor_m10354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal__ctor_m10356(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_Compare_m10387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Boolean_t203_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m7510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m3962(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m7511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10473(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ISerializable_t310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MulticastDelegate_t22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void Delegate_t211_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Delegate_t211_CustomAttributesCacheGenerator_Delegate_Combine_m10507(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Delegate_t211_CustomAttributesCacheGenerator_Delegate_t211_Delegate_Combine_m10507_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_GetValues_m992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_GetName_m10515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_IsDefined_m9878(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10517(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_Parse_m8865(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToString_m1294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToString_m1282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10523(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10525(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Enum_t218_CustomAttributesCacheGenerator_Enum_Format_m10534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Length_m993(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m3935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m8744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10559(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10560(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10561_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10562_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10572(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10575(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10579(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10580(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10586_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10589_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10590(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10590_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10591(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10591_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10592(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10593(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Clear_m9821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m3730(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10603(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m9815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m9845(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10611(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10612(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10613(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14473(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10630(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Resize_m14485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10631(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2930_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14533(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14536(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICollection_t1789_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IList_t183_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void IList_1_t2932_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Void_t260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Type_t2934_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Type_t2934_0_0_0_var = il2cpp_codegen_type_from_index(5117);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Type_t2934_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10695_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MemberInfo_t2935_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MemberInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MemberInfo_t2935_0_0_0_var = il2cpp_codegen_type_from_index(5118);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_MemberInfo_t2935_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICustomAttributeProvider_t2890_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
extern const Il2CppType* MemberInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _MemberInfo_t2935_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemberInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4982);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("f7102fa9-cabb-3a74-a6da-b4567ef1b079"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(MemberInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void IReflect_t2936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Type
#include "mscorlib_System_Type.h"
extern const Il2CppType* Type_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void _Type_t2934_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_0_0_0_var = il2cpp_codegen_type_from_index(62);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Type_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Exception_t324_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void Exception_t135_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Exception_t324_0_0_0_var = il2cpp_codegen_type_from_index(531);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Exception_t324_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
void _Exception_t324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("b36b5c63-42ef-38bc-a07e-0b34c98f164a"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 0, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2340_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2340_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2339_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10711(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ParamArrayAttribute_t1439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2048, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OutAttribute_t2341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ObsoleteAttribute_t259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 6140, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DllImportAttribute_t2342_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2343_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 10496, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2343_CustomAttributesCacheGenerator_MarshalType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2343_CustomAttributesCacheGenerator_MarshalTypeRef(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void InAttribute_t2344_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void GuidAttribute_t1425_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 5149, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ComImportAttribute_t2345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1028, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OptionalAttribute_t2346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void CompilerGeneratedAttribute_t245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void InternalsVisibleToAttribute_t1702_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void RuntimeCompatibilityAttribute_t241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DebuggerHiddenAttribute_t257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 224, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DefaultMemberAttribute_t250_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1036, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2304, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2347_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10721(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FieldOffsetAttribute_t2348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RuntimeArgumentHandle_t2349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsyncCallback_t20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IAsyncResult_t19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void TypedReference_t2350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MarshalByRefObject_t2011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Locale_t2354_CustomAttributesCacheGenerator_Locale_t2354_Locale_GetText_m10733_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t2355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void MonoDocumentationNoteAttribute_t2356_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandleZeroOrMinusOneIsInvalid_t2357_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeWaitHandle_t2359_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SortKey_t2379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void X509CertificateCollection_t2406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t2409_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ASN1_t2403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void SmallXmlParser_t2421_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
extern const Il2CppType* CollectionDebuggerView_2_t2938_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Dictionary_2_t2942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2938_0_0_0_var = il2cpp_codegen_type_from_index(5120);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2938_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Dictionary_2_t2942_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Dictionary_2_t2942_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2938_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
void KeyCollection_t2945_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2938_0_0_0_var = il2cpp_codegen_type_from_index(5120);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2938_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2938_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
void ValueCollection_t2947_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2938_0_0_0_var = il2cpp_codegen_type_from_index(5120);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2938_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void IDictionary_2_t2954_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void KeyNotFoundException_t2427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void KeyValuePair_2_t2956_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("{value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11479(tmp, il2cpp_codegen_string_new_wrapper("[{key}]"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_1_t2937_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void List_1_t2957_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_1_t2937_0_0_0_var = il2cpp_codegen_type_from_index(5123);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_1_t2937_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Collection_1_t2959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ReadOnlyCollection_1_t2960_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
extern const Il2CppType* CollectionDebuggerView_t2434_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void ArrayList_t737_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2434_0_0_0_var = il2cpp_codegen_type_from_index(5124);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2434_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ArrayListWrapper_t2429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void SynchronizedArrayListWrapper_t2430_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ReadOnlyArrayListWrapper_t2432_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void BitArray_t2112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CaseInsensitiveComparer_t2138_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CaseInsensitiveHashCodeProvider_t2139_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use StringComparer instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CollectionBase_t2030_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Comparer_t2435_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void DictionaryEntry_t2128_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("{_value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11479(tmp, il2cpp_codegen_string_new_wrapper("[{_key}]"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2434_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2434_0_0_0_var = il2cpp_codegen_type_from_index(5124);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2434_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m11382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m11385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_Clear_m11401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_Remove_m11404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m11408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialize equalityComparer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_t1667____comparer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_t1667____hcp_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2434_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void HashKeys_t2440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2434_0_0_0_var = il2cpp_codegen_type_from_index(5124);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2434_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2434_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void HashValues_t2441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2434_0_0_0_var = il2cpp_codegen_type_from_index(5124);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2434_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IComparer_t1971_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IDictionary_t182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IDictionaryEnumerator_t1968_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IEqualityComparer_t1977_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void IHashCodeProvider_t1976_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use IEqualityComparer instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SortedList_t2143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ListKeys_t2446_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2434_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var;
void Stack_t1661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2434_0_0_0_var = il2cpp_codegen_type_from_index(5124);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5122);
		DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2454 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2454 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2454_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11481(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2434_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2452 * tmp;
		tmp = (DebuggerDisplayAttribute_t2452 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2452_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11478(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyHashAlgorithm_t2449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyVersionCompatibility_t2450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DebuggableAttribute_t1809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void DebuggingModes_t2451_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DebuggerDisplayAttribute_t2452_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4509, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DebuggerStepThroughAttribute_t2453_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 108, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DebuggerTypeProxyAttribute_t2454_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 13, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StackFrame_t1690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with MS.NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void StackTrace_t1672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Calendar_t2456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CompareInfo_t2325_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CompareOptions_t2460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CultureInfo_t232_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CultureInfo_t232_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CultureInfo_t232_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void DateTimeFormatFlags_t2464_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DateTimeFormatInfo_t2462_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void DateTimeStyles_t2465_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DaylightTime_t2466_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void GregorianCalendar_t2467_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void GregorianCalendarTypes_t2468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NumberFormatInfo_t2461_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NumberStyles_t2469_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void TextInfo_t2376_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("IDeserializationCallback isn't implemented."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void TextInfo_t2376_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TextInfo_t2376_CustomAttributesCacheGenerator_TextInfo_t2376____CultureName_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnicodeCategory_t2179_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IsolatedStorageException_t2471_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void BinaryReader_t2473_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11709(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Directory_t2474_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DirectoryInfo_t2475_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DirectoryNotFoundException_t2477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EndOfStreamException_t2478_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void File_t2479_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void FileAccess_t2141_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void FileAttributes_t2480_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileMode_t2481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileNotFoundException_t2482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileOptions_t2483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void FileShare_t2484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileStream_t2319_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileSystemInfo_t2476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FileSystemInfo_t2476_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IOException_t2326_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void MemoryStream_t1684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Path_t2163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Path_t2163_CustomAttributesCacheGenerator_InvalidPathChars(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("see GetInvalidPathChars and GetInvalidFileNameChars methods."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void PathTooLongException_t2492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SeekOrigin_t2332_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Stream_t1685_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StreamReader_t2497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StreamWriter_t2498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringReader_t147_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TextReader_t231_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TextWriter_t2162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyBuilder_t2961_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void AssemblyBuilder_t2506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyBuilder_t2961_0_0_0_var = il2cpp_codegen_type_from_index(5125);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_AssemblyBuilder_t2961_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorBuilder_t2962_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorBuilder_t2962_0_0_0_var = il2cpp_codegen_type_from_index(5126);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_ConstructorBuilder_t2962_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2509_CustomAttributesCacheGenerator_ConstructorBuilder_t2509____CallingConvention_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EnumBuilder_t2963_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EnumBuilder_t2510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EnumBuilder_t2963_0_0_0_var = il2cpp_codegen_type_from_index(5127);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_EnumBuilder_t2963_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EnumBuilder_t2510_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m12027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldBuilder_t2964_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FieldBuilder_t2512_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldBuilder_t2964_0_0_0_var = il2cpp_codegen_type_from_index(5128);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_FieldBuilder_t2964_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m12061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m12064(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m12103(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m12104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m12105(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2514_GenericTypeParameterBuilder_MakeGenericType_m12105_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBuilder_t2965_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void MethodBuilder_t2513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBuilder_t2965_0_0_0_var = il2cpp_codegen_type_from_index(5129);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_MethodBuilder_t2965_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void MethodBuilder_t2513_CustomAttributesCacheGenerator_MethodBuilder_Equals_m12121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void MethodBuilder_t2513_CustomAttributesCacheGenerator_MethodBuilder_t2513_MethodBuilder_MakeGenericMethod_m12124_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ModuleBuilder_t2966_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void ModuleBuilder_t2516_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ModuleBuilder_t2966_0_0_0_var = il2cpp_codegen_type_from_index(5130);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_ModuleBuilder_t2966_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterBuilder_t2967_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void ParameterBuilder_t2518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterBuilder_t2967_0_0_0_var = il2cpp_codegen_type_from_index(5131);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_ParameterBuilder_t2967_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _TypeBuilder_t2968_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_TypeBuilder_t2968_0_0_0_var = il2cpp_codegen_type_from_index(5132);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_TypeBuilder_t2968_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m12148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m12165(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_t2507_TypeBuilder_MakeGenericType_m12165_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m12172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m12173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m12174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("arrays"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void UnmanagedMarshal_t2511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("An alternate API is available: Emit the MarshalAs custom attribute instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AmbiguousMatchException_t2522_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Assembly_t2969_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Assembly_t2144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Assembly_t2969_0_0_0_var = il2cpp_codegen_type_from_index(5133);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Assembly_t2969_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyCompanyAttribute_t1421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyConfigurationAttribute_t1420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyCopyrightAttribute_t1423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyDefaultAliasAttribute_t1808_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyDelaySignAttribute_t1812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyDescriptionAttribute_t1419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyFileVersionAttribute_t1424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyInformationalVersionAttribute_t1806_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyKeyFileAttribute_t1811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyName_t2970_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void AssemblyName_t2527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyName_t2970_0_0_0_var = il2cpp_codegen_type_from_index(5134);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_AssemblyName_t2970_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyNameFlags_t2528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void AssemblyProductAttribute_t1422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyTitleAttribute_t1418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyTrademarkAttribute_t1427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void Binder_t1688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void Default_t2529_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m12226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void BindingFlags_t2530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CallingConventions_t2531_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorInfo_t2971_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void ConstructorInfo_t1698_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorInfo_t2971_0_0_0_var = il2cpp_codegen_type_from_index(5135);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_ConstructorInfo_t2971_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ConstructorInfo_t1698_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var;
void ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2453 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2453 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11480(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorInfo_t1698____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void EventAttributes_t2532_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EventInfo_t2972_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void EventInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EventInfo_t2972_0_0_0_var = il2cpp_codegen_type_from_index(5137);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_EventInfo_t2972_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FieldAttributes_t2534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldInfo_t2973_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldInfo_t2973_0_0_0_var = il2cpp_codegen_type_from_index(5138);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_FieldInfo_t2973_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m12257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2453 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2453 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11480(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void MemberTypes_t2536_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void MethodAttributes_t2537_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBase_t2974_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodBase_t1691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBase_t2974_0_0_0_var = il2cpp_codegen_type_from_index(5139);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_MethodBase_t2974_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var;
void MethodBase_t1691_CustomAttributesCacheGenerator_MethodBase_Invoke_m12274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2453 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2453 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11480(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodBase_t1691_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m12279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodImplAttributes_t2538_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodInfo_t2975_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodInfo_t2975_0_0_0_var = il2cpp_codegen_type_from_index(5140);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_MethodInfo_t2975_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m12286_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m12287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Missing_t2539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void Missing_t2539_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m12293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Module_t2976_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void Module_t2517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Module_t2976_0_0_0_var = il2cpp_codegen_type_from_index(5141);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Module_t2976_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void PInfo_t2547_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ParameterAttributes_t2549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterInfo_t2977_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void ParameterInfo_t1693_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterInfo_t2977_0_0_0_var = il2cpp_codegen_type_from_index(5142);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_ParameterInfo_t2977_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void ParameterModifier_t2550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Pointer_t2551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ProcessorArchitecture_t2552_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void PropertyAttributes_t2553_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyInfo_t2978_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyInfo_t2978_0_0_0_var = il2cpp_codegen_type_from_index(5143);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_PropertyInfo_t2978_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12443(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5136);
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t2453 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2453 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11480(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2453 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2453 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2453_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11480(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StrongNameKeyPair_t2526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TargetException_t2554_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TargetInvocationException_t2555_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TargetParameterCountException_t2556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void TypeAttributes_t2557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NeutralResourcesLanguageAttribute_t1814_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SatelliteContractVersionAttribute_t1807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void CompilationRelaxations_t2558_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CompilationRelaxationsAttribute_t1810_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 71, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DefaultDependencyAttribute_t2559_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IsVolatile_t2560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void StringFreezingAttribute_t2562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12460(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ReliabilityContractAttribute_t2566_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1133, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ActivationArguments_t2567_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CallingConvention_t2568_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CharSet_t2569_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ClassInterfaceAttribute_t2570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 5, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ClassInterfaceType_t2571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ComDefaultInterfaceAttribute_t2572_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ComInterfaceType_t2573_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void DispIdAttribute_t2574_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 960, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void GCHandle_t941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void GCHandleType_t2575_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HandleRef_t657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void InterfaceTypeAttribute_t2576_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
extern TypeInfo* SuppressUnmanagedCodeSecurityAttribute_t2732_il2cpp_TypeInfo_var;
void Marshal_t188_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressUnmanagedCodeSecurityAttribute_t2732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5144);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressUnmanagedCodeSecurityAttribute_t2732 * tmp;
		tmp = (SuppressUnmanagedCodeSecurityAttribute_t2732 *)il2cpp_codegen_object_new (SuppressUnmanagedCodeSecurityAttribute_t2732_il2cpp_TypeInfo_var);
		SuppressUnmanagedCodeSecurityAttribute__ctor_m13273(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Marshal_t188_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m858(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MarshalDirectiveException_t2577_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void PreserveSigAttribute_t2578_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle__ctor_m12489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Close_m12490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12495(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14952(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12496(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void TypeLibImportClassAttribute_t2579_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeLibVersionAttribute_t2580_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnmanagedType_t2581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Activator
#include "mscorlib_System_Activator.h"
extern const Il2CppType* Activator_t2773_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void _Activator_t2979_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Activator_t2773_0_0_0_var = il2cpp_codegen_type_from_index(5145);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Activator_t2773_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
extern const Il2CppType* Assembly_t2144_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void _Assembly_t2969_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Assembly_t2144_0_0_0_var = il2cpp_codegen_type_from_index(5146);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Assembly_t2144_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
extern const Il2CppType* AssemblyBuilder_t2506_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
void _AssemblyBuilder_t2961_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyBuilder_t2506_0_0_0_var = il2cpp_codegen_type_from_index(4863);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(AssemblyBuilder_t2506_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
extern const Il2CppType* AssemblyName_t2527_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
void _AssemblyName_t2970_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyName_t2527_0_0_0_var = il2cpp_codegen_type_from_index(4872);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(AssemblyName_t2527_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
extern const Il2CppType* ConstructorBuilder_t2509_0_0_0_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
void _ConstructorBuilder_t2962_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorBuilder_t2509_0_0_0_var = il2cpp_codegen_type_from_index(5147);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(ConstructorBuilder_t2509_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern const Il2CppType* ConstructorInfo_t1698_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _ConstructorInfo_t2971_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorInfo_t1698_0_0_0_var = il2cpp_codegen_type_from_index(4861);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(ConstructorInfo_t1698_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
extern const Il2CppType* EnumBuilder_t2510_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
void _EnumBuilder_t2963_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EnumBuilder_t2510_0_0_0_var = il2cpp_codegen_type_from_index(4750);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(EnumBuilder_t2510_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
extern const Il2CppType* EventInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void _EventInfo_t2972_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4745);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(EventInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
extern const Il2CppType* FieldBuilder_t2512_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void _FieldBuilder_t2964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldBuilder_t2512_0_0_0_var = il2cpp_codegen_type_from_index(5148);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(FieldBuilder_t2512_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
extern const Il2CppType* FieldInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
void _FieldInfo_t2973_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4743);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(FieldInfo_t_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
extern const Il2CppType* MethodBase_t1691_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _MethodBase_t2974_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBase_t1691_0_0_0_var = il2cpp_codegen_type_from_index(4866);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(MethodBase_t1691_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
extern const Il2CppType* MethodBuilder_t2513_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _MethodBuilder_t2965_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBuilder_t2513_0_0_0_var = il2cpp_codegen_type_from_index(5149);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(MethodBuilder_t2513_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void _MethodInfo_t2975_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2934);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(MethodInfo_t_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
extern const Il2CppType* Module_t2517_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
void _Module_t2976_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Module_t2517_0_0_0_var = il2cpp_codegen_type_from_index(4858);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Module_t2517_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
extern const Il2CppType* ModuleBuilder_t2516_0_0_0_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _ModuleBuilder_t2966_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ModuleBuilder_t2516_0_0_0_var = il2cpp_codegen_type_from_index(4862);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(ModuleBuilder_t2516_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
extern const Il2CppType* ParameterBuilder_t2518_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
void _ParameterBuilder_t2967_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterBuilder_t2518_0_0_0_var = il2cpp_codegen_type_from_index(5150);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(ParameterBuilder_t2518_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern const Il2CppType* ParameterInfo_t1693_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void _ParameterInfo_t2977_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterInfo_t1693_0_0_0_var = il2cpp_codegen_type_from_index(4860);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(ParameterInfo_t1693_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
extern const Il2CppType* PropertyInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void _PropertyInfo_t2978_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4744);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(PropertyInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
extern const Il2CppType* Thread_t993_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void _Thread_t2980_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t993_0_0_0_var = il2cpp_codegen_type_from_index(1012);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(Thread_t993_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
extern const Il2CppType* TypeBuilder_t2507_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t1425_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void _TypeBuilder_t2968_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeBuilder_t2507_0_0_0_var = il2cpp_codegen_type_from_index(4748);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5113);
		InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5112);
		GuidAttribute_t1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2349);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2579 * tmp;
		tmp = (TypeLibImportClassAttribute_t2579 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2579_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12498(tmp, il2cpp_codegen_type_get_object(TypeBuilder_t2507_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2576 * tmp;
		tmp = (InterfaceTypeAttribute_t2576 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2576_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12482(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t1425 * tmp;
		tmp = (GuidAttribute_t1425 *)il2cpp_codegen_object_new (GuidAttribute_t1425_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m6181(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IActivator_t2582_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IConstructionCallMessage_t2884_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UrlAttribute_t2588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UrlAttribute_t2588_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UrlAttribute_t2588_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ChannelServices_t2592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void ChannelServices_t2592_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void CrossAppDomainSink_t2595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IChannel_t2885_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IChannelReceiver_t2898_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IChannelSender_t2981_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Context_t2596_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ContextAttribute_t2589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IContextAttribute_t2896_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IContextProperty_t2886_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IContributeClientContextSink_t2982_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IContributeServerContextSink_t2983_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2598_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2598_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12545(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2598_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsyncResult_t2605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ConstructionCall_t2606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ConstructionCall_t2606_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2608_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2608_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Header_t2611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMessage_t2604_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMessageCtrl_t2603_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMessageSink_t2187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMethodCallMessage_t2888_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMethodMessage_t2616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IMethodReturnMessage_t2887_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IRemotingFormatter_t2984_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void LogicalCallContext_t2613_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodCall_t2607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MethodCall_t2607_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void MethodDictionary_t2609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MethodDictionary_t2609_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void MethodDictionary_t2609_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RemotingSurrogateSelector_t2621_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ReturnMessage_t2622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void ProxyAttribute_t2623_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 4, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ProxyAttribute_t2623_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ProxyAttribute_t2623_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RealProxy_t2624_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ITrackingHandler_t2901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TrackingServices_t2628_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ActivatedClientTypeEntry_t2629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IChannelInfo_t2635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IEnvoyInfo_t2637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IRemotingTypeInfo_t2636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ObjRef_t2632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjRef_t2632_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void ObjRef_t2632_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RemotingConfiguration_t2638_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RemotingException_t2639_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RemotingServices_t2641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void RemotingServices_t2641_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12740(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void RemotingServices_t2641_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeEntry_t2630_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void WellKnownObjectMode_t2646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void BinaryFormatter_t2640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void BinaryFormatter_t2640_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void BinaryFormatter_t2640_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FormatterAssemblyStyle_t2659_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FormatterTypeStyle_t2660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeFilterLevel_t2661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FormatterConverter_t2662_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FormatterServices_t2663_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IDeserializationCallback_t1842_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IFormatter_t2986_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void IFormatterConverter_t2679_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IObjectReference_t2906_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ISerializationSurrogate_t2671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ISurrogateSelector_t2620_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ObjectManager_t2657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OnDeserializedAttribute_t2672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void OnDeserializingAttribute_t2673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OnSerializedAttribute_t2674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void OnSerializingAttribute_t2675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SerializationBinder_t2652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SerializationEntry_t2678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SerializationException_t2140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SerializationInfo_t1673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SerializationInfo_t1673_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12880(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void SerializationInfo_t1673_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12886(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SerializationInfoEnumerator_t2680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StreamingContext_t1674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StreamingContextStates_t2681_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("X509ContentType.SerializedCert isn't supported (anywhere in the class)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m8959(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use the Issuer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_GetName_m8960(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use the Subject property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Equals_m8951(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Import_m8817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Reset_m8819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void X509KeyStorageFlags_t2178_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsymmetricAlgorithm_t2013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsymmetricKeyExchangeFormatter_t2682_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsymmetricSignatureDeformatter_t2267_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AsymmetricSignatureFormatter_t2269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CipherMode_t2331_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CryptoConfig_t2154_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void CryptoConfig_t2154_CustomAttributesCacheGenerator_CryptoConfig_t2154_CryptoConfig_CreateFromName_m8850_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CryptographicException_t2150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CryptographicUnexpectedOperationException_t2155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CspParameters_t2310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void CspProviderFlags_t2684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DES_t2321_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DESCryptoServiceProvider_t2687_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSA_t2129_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t2148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t2148_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t2148____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSAParameters_t2149_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSASignatureDeformatter_t2317_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DSASignatureFormatter_t2688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMAC_t2314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACMD5_t2689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACRIPEMD160_t2690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACSHA1_t2313_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACSHA256_t2691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACSHA384_t2692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HMACSHA512_t2693_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HashAlgorithm_t2210_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICryptoTransform_t2241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICspAsymmetricAlgorithm_t2987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void KeyedHashAlgorithm_t2234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MACTripleDES_t2694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MD5_t2315_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MD5CryptoServiceProvider_t2695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void PaddingMode_t2696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RC2_t2322_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RC2CryptoServiceProvider_t2697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RIPEMD160_t2699_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RIPEMD160Managed_t2700_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSA_t2130_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t2145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t2145_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t2145____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSAPKCS1KeyExchangeFormatter_t2327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureDeformatter_t2318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureFormatter_t2702_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RSAParameters_t2147_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Rijndael_t2324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RijndaelManaged_t2703_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RijndaelManagedTransform_t2705_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA1_t2159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA1CryptoServiceProvider_t2707_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA1Managed_t2708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA256_t2316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA256Managed_t2709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA384_t2710_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA384Managed_t2712_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA512_t2713_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SHA512Managed_t2714_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SignatureDescription_t2716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SymmetricAlgorithm_t2217_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ToBase64Transform_t2719_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TripleDES_t2323_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TripleDESCryptoServiceProvider_t2720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StrongNamePublicKeyBlob_t2722_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ApplicationTrust_t2724_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Evidence_t2524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Evidence_t2524_CustomAttributesCacheGenerator_Evidence_Equals_m13226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Evidence_t2524_CustomAttributesCacheGenerator_Evidence_GetHashCode_m13228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Hash_t2726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IIdentityPermissionFactory_t2989_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StrongName_t2727_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IPrincipal_t2766_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void PrincipalPolicy_t2728_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IPermission_t2730_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ISecurityEncodable_t2990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SecurityElement_t2419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SecurityException_t2731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SecurityException_t2731_CustomAttributesCacheGenerator_SecurityException_t2731____Demanded_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SecuritySafeCriticalAttribute_t1704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void SuppressUnmanagedCodeSecurityAttribute_t2732_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 5188, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
void UnverifiableCodeAttribute_t2733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 2, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7573(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2734_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m13288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m13289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m13290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("we have simple override to match method signature."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Decoder_t2472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Decoder_t2472_CustomAttributesCacheGenerator_Decoder_t2472____Fallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Decoder_t2472_CustomAttributesCacheGenerator_Decoder_t2472____FallbackBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void DecoderReplacementFallback_t2740_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m13313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void EncoderReplacementFallback_t2747_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m13343(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666_Encoding_InvokeI18N_m13374_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_GetByteCount_m13390(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_GetBytes_m13391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____IsReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____DecoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____EncoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void StringBuilder_t36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m5743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m5742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_t36_StringBuilder_AppendFormat_m9814_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_t36_StringBuilder_AppendFormat_m13423_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13434(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13443(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13454(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void UTF8Encoding_t2757_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("EncoderFallback is not handled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13484(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13500(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2759_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13511(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EventResetMode_t2760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EventWaitHandle_t2761_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ExecutionContext_t2601_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ExecutionContext_t2601_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Interlocked_t2762_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m864(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ManualResetEvent_t2262_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Monitor_t2763_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Monitor_t2763_CustomAttributesCacheGenerator_Monitor_Exit_m3735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Mutex_t2597_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Mutex_t2597_CustomAttributesCacheGenerator_Mutex__ctor_m13529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Mutex_t2597_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13532(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SynchronizationLockException_t2765_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Thread_t2980_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Thread_t2980_0_0_0_var = il2cpp_codegen_type_from_index(5151);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Thread_t2980_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator_local_slots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator__ec(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator_Thread_Finalize_m13554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Thread_t993_CustomAttributesCacheGenerator_Thread_GetHashCode_m13558(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ThreadAbortException_t2767_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ThreadInterruptedException_t2768_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ThreadState_t2769_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ThreadStateException_t2770_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void WaitHandle_t2308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void WaitHandle_t2308_CustomAttributesCacheGenerator_WaitHandle_t2308____Handle_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("In the profiles > 2.x, use SafeHandle instead of Handle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AccessViolationException_t2771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ActivationContext_t2772_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ActivationContext_t2772_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization support"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Activator_t2979_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var;
void Activator_t2773_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Activator_t2979_0_0_0_var = il2cpp_codegen_type_from_index(5153);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5111);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2572 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2572 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2572_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12464(tmp, il2cpp_codegen_type_get_object(_Activator_t2979_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t1439_il2cpp_TypeInfo_var;
void Activator_t2773_CustomAttributesCacheGenerator_Activator_t2773_Activator_CreateInstance_m946_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t1439_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2358);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t1439 * tmp;
		tmp = (ParamArrayAttribute_t1439 *)il2cpp_codegen_object_new (ParamArrayAttribute_t1439_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m6243(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AppDomain_t2774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void AppDomain_t2774_CustomAttributesCacheGenerator_type_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void AppDomain_t2774_CustomAttributesCacheGenerator_assembly_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void AppDomain_t2774_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void AppDomain_t2774_CustomAttributesCacheGenerator__principal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AppDomainManager_t2775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var;
void AppDomainSetup_t2782_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5109);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2570 * tmp;
		tmp = (ClassInterfaceAttribute_t2570 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2570_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12463(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ApplicationException_t2783_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2776_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13603(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ArgumentException_t1409_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ArgumentNullException_t908_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ArgumentOutOfRangeException_t909_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ArithmeticException_t2309_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ArrayTypeMismatchException_t2784_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyLoadEventArgs_t2785_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
void AttributeTargets_t2786_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Buffer_t2787_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void CharEnumerator_t2788_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ContextBoundObject_t2789_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13670(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13721(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13722(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13734(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13772(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13775(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13779(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13791(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13805(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13807(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13808(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13809(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m7495(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13810(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m3757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m7494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13830(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13833(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13834(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var;
void Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13836(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3630);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1813 * tmp;
		tmp = (CLSCompliantAttribute_t1813 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1813_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7695(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DBNull_t2790_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DateTimeKind_t2792_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void DateTimeOffset_t2793_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10734(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DayOfWeek_t2795_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DivideByZeroException_t2798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void DllNotFoundException_t2799_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EntryPointNotFoundException_t2801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void MonoEnumInfo_t2806_CustomAttributesCacheGenerator_cache(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Environment_t2809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SpecialFolder_t2807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EventArgs_t2218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ExecutionEngineException_t2810_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FieldAccessException_t2811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FlagsAttribute_t999_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 16, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void FormatException_t201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void GC_t2813_CustomAttributesCacheGenerator_GC_SuppressFinalize_m3901(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Guid_t2814_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ICustomFormatter_t2891_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IFormatProvider_t2873_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void IndexOutOfRangeException_t1683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void InvalidCastException_t229_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void InvalidOperationException_t905_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void LoaderOptimization_t2815_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void LoaderOptimization_t2815_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m8946(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void LoaderOptimization_t2815_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m8946(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Math_t2816_CustomAttributesCacheGenerator_Math_Max_m9825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Math_t2816_CustomAttributesCacheGenerator_Math_Min_m14046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void Math_t2816_CustomAttributesCacheGenerator_Math_Sqrt_m14055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MemberAccessException_t2812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MethodAccessException_t2817_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MissingFieldException_t2818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MissingMemberException_t2819_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MissingMethodException_t2820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MulticastNotSupportedException_t2826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NonSerializedAttribute_t2827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NotImplementedException_t933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NotSupportedException_t192_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void NullReferenceException_t1675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var;
void NumberFormatter_t2829_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2840 * tmp;
		tmp = (ThreadStaticAttribute_t2840 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2840_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m14289(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ObjectDisposedException_t2312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OperatingSystem_t2808_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OutOfMemoryException_t2830_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void OverflowException_t2831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void PlatformID_t2832_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Random_t207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void RankException_t2833_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ResolveEventArgs_t2834_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2355_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2835_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		MonoTODOAttribute_t2355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2355 * tmp;
		tmp = (MonoTODOAttribute_t2355 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2355_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10735(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2835_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m14271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5110);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2566 * tmp;
		tmp = (ReliabilityContractAttribute_t2566 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2566_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12462(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringComparer_t1680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringComparison_t2838_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t999_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void StringSplitOptions_t2839_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t999_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t999 * tmp;
		tmp = (FlagsAttribute_t999 *)il2cpp_codegen_object_new (FlagsAttribute_t999_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m4393(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void SystemException_t2160_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ThreadStaticAttribute_t2840_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2953);
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1705 * tmp;
		tmp = (AttributeUsageAttribute_t1705 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1705_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7571(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7572(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TimeSpan_t190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TimeZone_t2841_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeCode_t2843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeInitializationException_t2844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeLoadException_t2800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnauthorizedAccessException_t2845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void Version_t1997_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void WeakReference_t2633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void MemberFilter_t2338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void TypeFilter_t2540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void HeaderHandler_t2851_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AppDomainInitializer_t2781_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void AssemblyLoadEventHandler_t2777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void EventHandler_t2779_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void ResolveEventHandler_t2778_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t1426_il2cpp_TypeInfo_var;
void UnhandledExceptionEventHandler_t2780_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t1426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2350);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t1426 * tmp;
		tmp = (ComVisibleAttribute_t1426 *)il2cpp_codegen_object_new (ComVisibleAttribute_t1426_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m6182(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t2872_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_mscorlib_Assembly_AttributeGenerators[935] = 
{
	NULL,
	g_mscorlib_Assembly_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator_Object__ctor_m830,
	Object_t_CustomAttributesCacheGenerator_Object_Finalize_m1177,
	Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m3694,
	ValueType_t329_CustomAttributesCacheGenerator,
	Attribute_t1546_CustomAttributesCacheGenerator,
	_Attribute_t1731_CustomAttributesCacheGenerator,
	Int32_t189_CustomAttributesCacheGenerator,
	IFormattable_t312_CustomAttributesCacheGenerator,
	IConvertible_t313_CustomAttributesCacheGenerator,
	IComparable_t314_CustomAttributesCacheGenerator,
	SerializableAttribute_t2334_CustomAttributesCacheGenerator,
	AttributeUsageAttribute_t1705_CustomAttributesCacheGenerator,
	ComVisibleAttribute_t1426_CustomAttributesCacheGenerator,
	Int64_t233_CustomAttributesCacheGenerator,
	UInt32_t235_CustomAttributesCacheGenerator,
	UInt32_t235_CustomAttributesCacheGenerator_UInt32_Parse_m10039,
	UInt32_t235_CustomAttributesCacheGenerator_UInt32_Parse_m10040,
	UInt32_t235_CustomAttributesCacheGenerator_UInt32_TryParse_m8876,
	UInt32_t235_CustomAttributesCacheGenerator_UInt32_TryParse_m10041,
	CLSCompliantAttribute_t1813_CustomAttributesCacheGenerator,
	UInt64_t239_CustomAttributesCacheGenerator,
	UInt64_t239_CustomAttributesCacheGenerator_UInt64_Parse_m10066,
	UInt64_t239_CustomAttributesCacheGenerator_UInt64_Parse_m10068,
	UInt64_t239_CustomAttributesCacheGenerator_UInt64_TryParse_m10069,
	Byte_t237_CustomAttributesCacheGenerator,
	SByte_t236_CustomAttributesCacheGenerator,
	SByte_t236_CustomAttributesCacheGenerator_SByte_Parse_m10119,
	SByte_t236_CustomAttributesCacheGenerator_SByte_Parse_m10120,
	SByte_t236_CustomAttributesCacheGenerator_SByte_TryParse_m10121,
	Int16_t238_CustomAttributesCacheGenerator,
	UInt16_t194_CustomAttributesCacheGenerator,
	UInt16_t194_CustomAttributesCacheGenerator_UInt16_Parse_m10174,
	UInt16_t194_CustomAttributesCacheGenerator_UInt16_Parse_m10175,
	UInt16_t194_CustomAttributesCacheGenerator_UInt16_TryParse_m10176,
	UInt16_t194_CustomAttributesCacheGenerator_UInt16_TryParse_m10177,
	IEnumerator_t37_CustomAttributesCacheGenerator,
	IEnumerable_t38_CustomAttributesCacheGenerator,
	IEnumerable_t38_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14453,
	IDisposable_t191_CustomAttributesCacheGenerator,
	Char_t193_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator_String__ctor_m10212,
	String_t_CustomAttributesCacheGenerator_String_Equals_m10235,
	String_t_CustomAttributesCacheGenerator_String_Equals_m10236,
	String_t_CustomAttributesCacheGenerator_String_t_String_Split_m1065_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_Split_m10240,
	String_t_CustomAttributesCacheGenerator_String_Split_m10241,
	String_t_CustomAttributesCacheGenerator_String_Split_m8795,
	String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m903_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m8878_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m8793_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m3689_Arg1_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m9871_Arg2_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m10272_Arg3_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m976_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m4083_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_GetHashCode_m10280,
	ICloneable_t309_CustomAttributesCacheGenerator,
	Single_t202_CustomAttributesCacheGenerator,
	Single_t202_CustomAttributesCacheGenerator_Single_IsNaN_m876,
	Double_t234_CustomAttributesCacheGenerator,
	Double_t234_CustomAttributesCacheGenerator_Double_IsNaN_m10342,
	Decimal_t240_CustomAttributesCacheGenerator,
	Decimal_t240_CustomAttributesCacheGenerator_MinValue,
	Decimal_t240_CustomAttributesCacheGenerator_MaxValue,
	Decimal_t240_CustomAttributesCacheGenerator_MinusOne,
	Decimal_t240_CustomAttributesCacheGenerator_One,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal__ctor_m10354,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal__ctor_m10356,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_Compare_m10387,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10415,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10417,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10419,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10421,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10423,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10425,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10427,
	Decimal_t240_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10429,
	Boolean_t203_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m7510,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10462,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10463,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10466,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m3962,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3931,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m7511,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10470,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10471,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10472,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10473,
	ISerializable_t310_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10474,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10478,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10486,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10487,
	MulticastDelegate_t22_CustomAttributesCacheGenerator,
	Delegate_t211_CustomAttributesCacheGenerator,
	Delegate_t211_CustomAttributesCacheGenerator_Delegate_Combine_m10507,
	Delegate_t211_CustomAttributesCacheGenerator_Delegate_t211_Delegate_Combine_m10507_Arg0_ParameterInfo,
	Enum_t218_CustomAttributesCacheGenerator,
	Enum_t218_CustomAttributesCacheGenerator_Enum_GetValues_m992,
	Enum_t218_CustomAttributesCacheGenerator_Enum_GetName_m10515,
	Enum_t218_CustomAttributesCacheGenerator_Enum_IsDefined_m9878,
	Enum_t218_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10517,
	Enum_t218_CustomAttributesCacheGenerator_Enum_Parse_m8865,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToString_m1294,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToString_m1282,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10522,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10523,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10524,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10525,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10526,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10527,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10528,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10529,
	Enum_t218_CustomAttributesCacheGenerator_Enum_ToObject_m10530,
	Enum_t218_CustomAttributesCacheGenerator_Enum_Format_m10534,
	Array_t_CustomAttributesCacheGenerator,
	Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10548,
	Array_t_CustomAttributesCacheGenerator_Array_get_Length_m993,
	Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m3935,
	Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m8744,
	Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10559,
	Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10560,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10561_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10562_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10572,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10575,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10576,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10577,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10578,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10579,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10580,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10586_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10589_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10590,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10590_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10591,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10591_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10592,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10593,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10594,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10595,
	Array_t_CustomAttributesCacheGenerator_Array_Clear_m9821,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10599,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m3730,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10600,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10601,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10602,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10603,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10604,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10606,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10607,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10608,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m9815,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m9845,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10610,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10611,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10612,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10613,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10614,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10615,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10616,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10617,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14470,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14471,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14472,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14473,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14474,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14475,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14476,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14477,
	Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10630,
	Array_t_CustomAttributesCacheGenerator_Array_Resize_m14485,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14496,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14497,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14498,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14499,
	Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10631,
	Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo,
	ArrayReadOnlyList_1_t2930_CustomAttributesCacheGenerator,
	ArrayReadOnlyList_1_t2930_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14526,
	U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14533,
	U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14534,
	U3CGetEnumeratorU3Ec__Iterator0_t2931_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14536,
	ICollection_t1789_CustomAttributesCacheGenerator,
	IList_t183_CustomAttributesCacheGenerator,
	IList_1_t2932_CustomAttributesCacheGenerator,
	Void_t260_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10667,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10683,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10684,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10685,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14585,
	Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10695_Arg0_ParameterInfo,
	MemberInfo_t_CustomAttributesCacheGenerator,
	ICustomAttributeProvider_t2890_CustomAttributesCacheGenerator,
	_MemberInfo_t2935_CustomAttributesCacheGenerator,
	IReflect_t2936_CustomAttributesCacheGenerator,
	_Type_t2934_CustomAttributesCacheGenerator,
	Exception_t135_CustomAttributesCacheGenerator,
	_Exception_t324_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2340_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2340_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10706,
	RuntimeTypeHandle_t2339_CustomAttributesCacheGenerator,
	RuntimeTypeHandle_t2339_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10711,
	ParamArrayAttribute_t1439_CustomAttributesCacheGenerator,
	OutAttribute_t2341_CustomAttributesCacheGenerator,
	ObsoleteAttribute_t259_CustomAttributesCacheGenerator,
	DllImportAttribute_t2342_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2343_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2343_CustomAttributesCacheGenerator_MarshalType,
	MarshalAsAttribute_t2343_CustomAttributesCacheGenerator_MarshalTypeRef,
	InAttribute_t2344_CustomAttributesCacheGenerator,
	GuidAttribute_t1425_CustomAttributesCacheGenerator,
	ComImportAttribute_t2345_CustomAttributesCacheGenerator,
	OptionalAttribute_t2346_CustomAttributesCacheGenerator,
	CompilerGeneratedAttribute_t245_CustomAttributesCacheGenerator,
	InternalsVisibleToAttribute_t1702_CustomAttributesCacheGenerator,
	RuntimeCompatibilityAttribute_t241_CustomAttributesCacheGenerator,
	DebuggerHiddenAttribute_t257_CustomAttributesCacheGenerator,
	DefaultMemberAttribute_t250_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2347_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2347_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10721,
	FieldOffsetAttribute_t2348_CustomAttributesCacheGenerator,
	RuntimeArgumentHandle_t2349_CustomAttributesCacheGenerator,
	AsyncCallback_t20_CustomAttributesCacheGenerator,
	IAsyncResult_t19_CustomAttributesCacheGenerator,
	TypedReference_t2350_CustomAttributesCacheGenerator,
	MarshalByRefObject_t2011_CustomAttributesCacheGenerator,
	Locale_t2354_CustomAttributesCacheGenerator_Locale_t2354_Locale_GetText_m10733_Arg1_ParameterInfo,
	MonoTODOAttribute_t2355_CustomAttributesCacheGenerator,
	MonoDocumentationNoteAttribute_t2356_CustomAttributesCacheGenerator,
	SafeHandleZeroOrMinusOneIsInvalid_t2357_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10737,
	SafeWaitHandle_t2359_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10739,
	MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3,
	MSCompatUnicodeTable_t2369_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4,
	SortKey_t2379_CustomAttributesCacheGenerator,
	PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	PKCS12_t2407_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509CertificateCollection_t2406_CustomAttributesCacheGenerator,
	X509ExtensionCollection_t2409_CustomAttributesCacheGenerator,
	ASN1_t2403_CustomAttributesCacheGenerator,
	SmallXmlParser_t2421_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18,
	Dictionary_2_t2942_CustomAttributesCacheGenerator,
	Dictionary_2_t2942_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	Dictionary_2_t2942_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14674,
	KeyCollection_t2945_CustomAttributesCacheGenerator,
	ValueCollection_t2947_CustomAttributesCacheGenerator,
	IDictionary_2_t2954_CustomAttributesCacheGenerator,
	KeyNotFoundException_t2427_CustomAttributesCacheGenerator,
	KeyValuePair_2_t2956_CustomAttributesCacheGenerator,
	List_1_t2957_CustomAttributesCacheGenerator,
	Collection_1_t2959_CustomAttributesCacheGenerator,
	ReadOnlyCollection_1_t2960_CustomAttributesCacheGenerator,
	ArrayList_t737_CustomAttributesCacheGenerator,
	ArrayListWrapper_t2429_CustomAttributesCacheGenerator,
	SynchronizedArrayListWrapper_t2430_CustomAttributesCacheGenerator,
	ReadOnlyArrayListWrapper_t2432_CustomAttributesCacheGenerator,
	BitArray_t2112_CustomAttributesCacheGenerator,
	CaseInsensitiveComparer_t2138_CustomAttributesCacheGenerator,
	CaseInsensitiveHashCodeProvider_t2139_CustomAttributesCacheGenerator,
	CollectionBase_t2030_CustomAttributesCacheGenerator,
	Comparer_t2435_CustomAttributesCacheGenerator,
	DictionaryEntry_t2128_CustomAttributesCacheGenerator,
	Hashtable_t1667_CustomAttributesCacheGenerator,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m11382,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8737,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m11385,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8738,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable__ctor_m8767,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_Clear_m11401,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_Remove_m11404,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m11408,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_t1667____comparer_PropertyInfo,
	Hashtable_t1667_CustomAttributesCacheGenerator_Hashtable_t1667____hcp_PropertyInfo,
	HashKeys_t2440_CustomAttributesCacheGenerator,
	HashValues_t2441_CustomAttributesCacheGenerator,
	IComparer_t1971_CustomAttributesCacheGenerator,
	IDictionary_t182_CustomAttributesCacheGenerator,
	IDictionaryEnumerator_t1968_CustomAttributesCacheGenerator,
	IEqualityComparer_t1977_CustomAttributesCacheGenerator,
	IHashCodeProvider_t1976_CustomAttributesCacheGenerator,
	SortedList_t2143_CustomAttributesCacheGenerator,
	ListKeys_t2446_CustomAttributesCacheGenerator,
	Stack_t1661_CustomAttributesCacheGenerator,
	AssemblyHashAlgorithm_t2449_CustomAttributesCacheGenerator,
	AssemblyVersionCompatibility_t2450_CustomAttributesCacheGenerator,
	DebuggableAttribute_t1809_CustomAttributesCacheGenerator,
	DebuggingModes_t2451_CustomAttributesCacheGenerator,
	DebuggerDisplayAttribute_t2452_CustomAttributesCacheGenerator,
	DebuggerStepThroughAttribute_t2453_CustomAttributesCacheGenerator,
	DebuggerTypeProxyAttribute_t2454_CustomAttributesCacheGenerator,
	StackFrame_t1690_CustomAttributesCacheGenerator,
	StackTrace_t1672_CustomAttributesCacheGenerator,
	Calendar_t2456_CustomAttributesCacheGenerator,
	CompareInfo_t2325_CustomAttributesCacheGenerator,
	CompareOptions_t2460_CustomAttributesCacheGenerator,
	CultureInfo_t232_CustomAttributesCacheGenerator,
	CultureInfo_t232_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19,
	CultureInfo_t232_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A,
	DateTimeFormatFlags_t2464_CustomAttributesCacheGenerator,
	DateTimeFormatInfo_t2462_CustomAttributesCacheGenerator,
	DateTimeStyles_t2465_CustomAttributesCacheGenerator,
	DaylightTime_t2466_CustomAttributesCacheGenerator,
	GregorianCalendar_t2467_CustomAttributesCacheGenerator,
	GregorianCalendarTypes_t2468_CustomAttributesCacheGenerator,
	NumberFormatInfo_t2461_CustomAttributesCacheGenerator,
	NumberStyles_t2469_CustomAttributesCacheGenerator,
	TextInfo_t2376_CustomAttributesCacheGenerator,
	TextInfo_t2376_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11674,
	TextInfo_t2376_CustomAttributesCacheGenerator_TextInfo_t2376____CultureName_PropertyInfo,
	UnicodeCategory_t2179_CustomAttributesCacheGenerator,
	IsolatedStorageException_t2471_CustomAttributesCacheGenerator,
	BinaryReader_t2473_CustomAttributesCacheGenerator,
	BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11704,
	BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11707,
	BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11708,
	BinaryReader_t2473_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11709,
	Directory_t2474_CustomAttributesCacheGenerator,
	DirectoryInfo_t2475_CustomAttributesCacheGenerator,
	DirectoryNotFoundException_t2477_CustomAttributesCacheGenerator,
	EndOfStreamException_t2478_CustomAttributesCacheGenerator,
	File_t2479_CustomAttributesCacheGenerator,
	FileAccess_t2141_CustomAttributesCacheGenerator,
	FileAttributes_t2480_CustomAttributesCacheGenerator,
	FileMode_t2481_CustomAttributesCacheGenerator,
	FileNotFoundException_t2482_CustomAttributesCacheGenerator,
	FileOptions_t2483_CustomAttributesCacheGenerator,
	FileShare_t2484_CustomAttributesCacheGenerator,
	FileStream_t2319_CustomAttributesCacheGenerator,
	FileSystemInfo_t2476_CustomAttributesCacheGenerator,
	FileSystemInfo_t2476_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11788,
	IOException_t2326_CustomAttributesCacheGenerator,
	MemoryStream_t1684_CustomAttributesCacheGenerator,
	Path_t2163_CustomAttributesCacheGenerator,
	Path_t2163_CustomAttributesCacheGenerator_InvalidPathChars,
	PathTooLongException_t2492_CustomAttributesCacheGenerator,
	SeekOrigin_t2332_CustomAttributesCacheGenerator,
	Stream_t1685_CustomAttributesCacheGenerator,
	StreamReader_t2497_CustomAttributesCacheGenerator,
	StreamWriter_t2498_CustomAttributesCacheGenerator,
	StringReader_t147_CustomAttributesCacheGenerator,
	TextReader_t231_CustomAttributesCacheGenerator,
	TextWriter_t2162_CustomAttributesCacheGenerator,
	AssemblyBuilder_t2506_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2509_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2509_CustomAttributesCacheGenerator_ConstructorBuilder_t2509____CallingConvention_PropertyInfo,
	EnumBuilder_t2510_CustomAttributesCacheGenerator,
	EnumBuilder_t2510_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m12027,
	FieldBuilder_t2512_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m12061,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m12064,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m12103,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m12104,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m12105,
	GenericTypeParameterBuilder_t2514_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2514_GenericTypeParameterBuilder_MakeGenericType_m12105_Arg0_ParameterInfo,
	MethodBuilder_t2513_CustomAttributesCacheGenerator,
	MethodBuilder_t2513_CustomAttributesCacheGenerator_MethodBuilder_Equals_m12121,
	MethodBuilder_t2513_CustomAttributesCacheGenerator_MethodBuilder_t2513_MethodBuilder_MakeGenericMethod_m12124_Arg0_ParameterInfo,
	ModuleBuilder_t2516_CustomAttributesCacheGenerator,
	ParameterBuilder_t2518_CustomAttributesCacheGenerator,
	TypeBuilder_t2507_CustomAttributesCacheGenerator,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m12148,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m12165,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_t2507_TypeBuilder_MakeGenericType_m12165_Arg0_ParameterInfo,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m12172,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m12173,
	TypeBuilder_t2507_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m12174,
	UnmanagedMarshal_t2511_CustomAttributesCacheGenerator,
	AmbiguousMatchException_t2522_CustomAttributesCacheGenerator,
	Assembly_t2144_CustomAttributesCacheGenerator,
	AssemblyCompanyAttribute_t1421_CustomAttributesCacheGenerator,
	AssemblyConfigurationAttribute_t1420_CustomAttributesCacheGenerator,
	AssemblyCopyrightAttribute_t1423_CustomAttributesCacheGenerator,
	AssemblyDefaultAliasAttribute_t1808_CustomAttributesCacheGenerator,
	AssemblyDelaySignAttribute_t1812_CustomAttributesCacheGenerator,
	AssemblyDescriptionAttribute_t1419_CustomAttributesCacheGenerator,
	AssemblyFileVersionAttribute_t1424_CustomAttributesCacheGenerator,
	AssemblyInformationalVersionAttribute_t1806_CustomAttributesCacheGenerator,
	AssemblyKeyFileAttribute_t1811_CustomAttributesCacheGenerator,
	AssemblyName_t2527_CustomAttributesCacheGenerator,
	AssemblyNameFlags_t2528_CustomAttributesCacheGenerator,
	AssemblyProductAttribute_t1422_CustomAttributesCacheGenerator,
	AssemblyTitleAttribute_t1418_CustomAttributesCacheGenerator,
	AssemblyTrademarkAttribute_t1427_CustomAttributesCacheGenerator,
	Binder_t1688_CustomAttributesCacheGenerator,
	Default_t2529_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m12226,
	BindingFlags_t2530_CustomAttributesCacheGenerator,
	CallingConventions_t2531_CustomAttributesCacheGenerator,
	ConstructorInfo_t1698_CustomAttributesCacheGenerator,
	ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorName,
	ConstructorInfo_t1698_CustomAttributesCacheGenerator_TypeConstructorName,
	ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7540,
	ConstructorInfo_t1698_CustomAttributesCacheGenerator_ConstructorInfo_t1698____MemberType_PropertyInfo,
	EventAttributes_t2532_CustomAttributesCacheGenerator,
	EventInfo_t_CustomAttributesCacheGenerator,
	FieldAttributes_t2534_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m12257,
	MemberTypes_t2536_CustomAttributesCacheGenerator,
	MethodAttributes_t2537_CustomAttributesCacheGenerator,
	MethodBase_t1691_CustomAttributesCacheGenerator,
	MethodBase_t1691_CustomAttributesCacheGenerator_MethodBase_Invoke_m12274,
	MethodBase_t1691_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m12279,
	MethodImplAttributes_t2538_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m12286_Arg0_ParameterInfo,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m12287,
	Missing_t2539_CustomAttributesCacheGenerator,
	Missing_t2539_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m12293,
	Module_t2517_CustomAttributesCacheGenerator,
	PInfo_t2547_CustomAttributesCacheGenerator,
	ParameterAttributes_t2549_CustomAttributesCacheGenerator,
	ParameterInfo_t1693_CustomAttributesCacheGenerator,
	ParameterModifier_t2550_CustomAttributesCacheGenerator,
	Pointer_t2551_CustomAttributesCacheGenerator,
	ProcessorArchitecture_t2552_CustomAttributesCacheGenerator,
	PropertyAttributes_t2553_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12443,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12444,
	StrongNameKeyPair_t2526_CustomAttributesCacheGenerator,
	TargetException_t2554_CustomAttributesCacheGenerator,
	TargetInvocationException_t2555_CustomAttributesCacheGenerator,
	TargetParameterCountException_t2556_CustomAttributesCacheGenerator,
	TypeAttributes_t2557_CustomAttributesCacheGenerator,
	NeutralResourcesLanguageAttribute_t1814_CustomAttributesCacheGenerator,
	SatelliteContractVersionAttribute_t1807_CustomAttributesCacheGenerator,
	CompilationRelaxations_t2558_CustomAttributesCacheGenerator,
	CompilationRelaxationsAttribute_t1810_CustomAttributesCacheGenerator,
	DefaultDependencyAttribute_t2559_CustomAttributesCacheGenerator,
	IsVolatile_t2560_CustomAttributesCacheGenerator,
	StringFreezingAttribute_t2562_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12460,
	CriticalFinalizerObject_t2565_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12461,
	ReliabilityContractAttribute_t2566_CustomAttributesCacheGenerator,
	ActivationArguments_t2567_CustomAttributesCacheGenerator,
	CallingConvention_t2568_CustomAttributesCacheGenerator,
	CharSet_t2569_CustomAttributesCacheGenerator,
	ClassInterfaceAttribute_t2570_CustomAttributesCacheGenerator,
	ClassInterfaceType_t2571_CustomAttributesCacheGenerator,
	ComDefaultInterfaceAttribute_t2572_CustomAttributesCacheGenerator,
	ComInterfaceType_t2573_CustomAttributesCacheGenerator,
	DispIdAttribute_t2574_CustomAttributesCacheGenerator,
	GCHandle_t941_CustomAttributesCacheGenerator,
	GCHandleType_t2575_CustomAttributesCacheGenerator,
	HandleRef_t657_CustomAttributesCacheGenerator,
	InterfaceTypeAttribute_t2576_CustomAttributesCacheGenerator,
	Marshal_t188_CustomAttributesCacheGenerator,
	Marshal_t188_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m858,
	MarshalDirectiveException_t2577_CustomAttributesCacheGenerator,
	PreserveSigAttribute_t2578_CustomAttributesCacheGenerator,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle__ctor_m12489,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Close_m12490,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12491,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12492,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12493,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12494,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12495,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14952,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12496,
	SafeHandle_t2358_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14953,
	TypeLibImportClassAttribute_t2579_CustomAttributesCacheGenerator,
	TypeLibVersionAttribute_t2580_CustomAttributesCacheGenerator,
	UnmanagedType_t2581_CustomAttributesCacheGenerator,
	_Activator_t2979_CustomAttributesCacheGenerator,
	_Assembly_t2969_CustomAttributesCacheGenerator,
	_AssemblyBuilder_t2961_CustomAttributesCacheGenerator,
	_AssemblyName_t2970_CustomAttributesCacheGenerator,
	_ConstructorBuilder_t2962_CustomAttributesCacheGenerator,
	_ConstructorInfo_t2971_CustomAttributesCacheGenerator,
	_EnumBuilder_t2963_CustomAttributesCacheGenerator,
	_EventInfo_t2972_CustomAttributesCacheGenerator,
	_FieldBuilder_t2964_CustomAttributesCacheGenerator,
	_FieldInfo_t2973_CustomAttributesCacheGenerator,
	_MethodBase_t2974_CustomAttributesCacheGenerator,
	_MethodBuilder_t2965_CustomAttributesCacheGenerator,
	_MethodInfo_t2975_CustomAttributesCacheGenerator,
	_Module_t2976_CustomAttributesCacheGenerator,
	_ModuleBuilder_t2966_CustomAttributesCacheGenerator,
	_ParameterBuilder_t2967_CustomAttributesCacheGenerator,
	_ParameterInfo_t2977_CustomAttributesCacheGenerator,
	_PropertyInfo_t2978_CustomAttributesCacheGenerator,
	_Thread_t2980_CustomAttributesCacheGenerator,
	_TypeBuilder_t2968_CustomAttributesCacheGenerator,
	IActivator_t2582_CustomAttributesCacheGenerator,
	IConstructionCallMessage_t2884_CustomAttributesCacheGenerator,
	UrlAttribute_t2588_CustomAttributesCacheGenerator,
	UrlAttribute_t2588_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12510,
	UrlAttribute_t2588_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12511,
	ChannelServices_t2592_CustomAttributesCacheGenerator,
	ChannelServices_t2592_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12515,
	CrossAppDomainSink_t2595_CustomAttributesCacheGenerator,
	IChannel_t2885_CustomAttributesCacheGenerator,
	IChannelReceiver_t2898_CustomAttributesCacheGenerator,
	IChannelSender_t2981_CustomAttributesCacheGenerator,
	Context_t2596_CustomAttributesCacheGenerator,
	ContextAttribute_t2589_CustomAttributesCacheGenerator,
	IContextAttribute_t2896_CustomAttributesCacheGenerator,
	IContextProperty_t2886_CustomAttributesCacheGenerator,
	IContributeClientContextSink_t2982_CustomAttributesCacheGenerator,
	IContributeServerContextSink_t2983_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2598_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2598_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12545,
	SynchronizationAttribute_t2598_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12546,
	AsyncResult_t2605_CustomAttributesCacheGenerator,
	ConstructionCall_t2606_CustomAttributesCacheGenerator,
	ConstructionCall_t2606_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20,
	ConstructionCallDictionary_t2608_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23,
	ConstructionCallDictionary_t2608_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24,
	Header_t2611_CustomAttributesCacheGenerator,
	IMessage_t2604_CustomAttributesCacheGenerator,
	IMessageCtrl_t2603_CustomAttributesCacheGenerator,
	IMessageSink_t2187_CustomAttributesCacheGenerator,
	IMethodCallMessage_t2888_CustomAttributesCacheGenerator,
	IMethodMessage_t2616_CustomAttributesCacheGenerator,
	IMethodReturnMessage_t2887_CustomAttributesCacheGenerator,
	IRemotingFormatter_t2984_CustomAttributesCacheGenerator,
	LogicalCallContext_t2613_CustomAttributesCacheGenerator,
	MethodCall_t2607_CustomAttributesCacheGenerator,
	MethodCall_t2607_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F,
	MethodDictionary_t2609_CustomAttributesCacheGenerator,
	MethodDictionary_t2609_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21,
	MethodDictionary_t2609_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22,
	RemotingSurrogateSelector_t2621_CustomAttributesCacheGenerator,
	ReturnMessage_t2622_CustomAttributesCacheGenerator,
	ProxyAttribute_t2623_CustomAttributesCacheGenerator,
	ProxyAttribute_t2623_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12682,
	ProxyAttribute_t2623_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12683,
	RealProxy_t2624_CustomAttributesCacheGenerator,
	ITrackingHandler_t2901_CustomAttributesCacheGenerator,
	TrackingServices_t2628_CustomAttributesCacheGenerator,
	ActivatedClientTypeEntry_t2629_CustomAttributesCacheGenerator,
	IChannelInfo_t2635_CustomAttributesCacheGenerator,
	IEnvoyInfo_t2637_CustomAttributesCacheGenerator,
	IRemotingTypeInfo_t2636_CustomAttributesCacheGenerator,
	ObjRef_t2632_CustomAttributesCacheGenerator,
	ObjRef_t2632_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26,
	ObjRef_t2632_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12720,
	RemotingConfiguration_t2638_CustomAttributesCacheGenerator,
	RemotingException_t2639_CustomAttributesCacheGenerator,
	RemotingServices_t2641_CustomAttributesCacheGenerator,
	RemotingServices_t2641_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12740,
	RemotingServices_t2641_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12744,
	TypeEntry_t2630_CustomAttributesCacheGenerator,
	WellKnownObjectMode_t2646_CustomAttributesCacheGenerator,
	BinaryFormatter_t2640_CustomAttributesCacheGenerator,
	BinaryFormatter_t2640_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField,
	BinaryFormatter_t2640_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12776,
	FormatterAssemblyStyle_t2659_CustomAttributesCacheGenerator,
	FormatterTypeStyle_t2660_CustomAttributesCacheGenerator,
	TypeFilterLevel_t2661_CustomAttributesCacheGenerator,
	FormatterConverter_t2662_CustomAttributesCacheGenerator,
	FormatterServices_t2663_CustomAttributesCacheGenerator,
	IDeserializationCallback_t1842_CustomAttributesCacheGenerator,
	IFormatter_t2986_CustomAttributesCacheGenerator,
	IFormatterConverter_t2679_CustomAttributesCacheGenerator,
	IObjectReference_t2906_CustomAttributesCacheGenerator,
	ISerializationSurrogate_t2671_CustomAttributesCacheGenerator,
	ISurrogateSelector_t2620_CustomAttributesCacheGenerator,
	ObjectManager_t2657_CustomAttributesCacheGenerator,
	OnDeserializedAttribute_t2672_CustomAttributesCacheGenerator,
	OnDeserializingAttribute_t2673_CustomAttributesCacheGenerator,
	OnSerializedAttribute_t2674_CustomAttributesCacheGenerator,
	OnSerializingAttribute_t2675_CustomAttributesCacheGenerator,
	SerializationBinder_t2652_CustomAttributesCacheGenerator,
	SerializationEntry_t2678_CustomAttributesCacheGenerator,
	SerializationException_t2140_CustomAttributesCacheGenerator,
	SerializationInfo_t1673_CustomAttributesCacheGenerator,
	SerializationInfo_t1673_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12880,
	SerializationInfo_t1673_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12886,
	SerializationInfoEnumerator_t2680_CustomAttributesCacheGenerator,
	StreamingContext_t1674_CustomAttributesCacheGenerator,
	StreamingContextStates_t2681_CustomAttributesCacheGenerator,
	X509Certificate_t2026_CustomAttributesCacheGenerator,
	X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m8959,
	X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_GetName_m8960,
	X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Equals_m8951,
	X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Import_m8817,
	X509Certificate_t2026_CustomAttributesCacheGenerator_X509Certificate_Reset_m8819,
	X509KeyStorageFlags_t2178_CustomAttributesCacheGenerator,
	AsymmetricAlgorithm_t2013_CustomAttributesCacheGenerator,
	AsymmetricKeyExchangeFormatter_t2682_CustomAttributesCacheGenerator,
	AsymmetricSignatureDeformatter_t2267_CustomAttributesCacheGenerator,
	AsymmetricSignatureFormatter_t2269_CustomAttributesCacheGenerator,
	CipherMode_t2331_CustomAttributesCacheGenerator,
	CryptoConfig_t2154_CustomAttributesCacheGenerator,
	CryptoConfig_t2154_CustomAttributesCacheGenerator_CryptoConfig_t2154_CryptoConfig_CreateFromName_m8850_Arg1_ParameterInfo,
	CryptographicException_t2150_CustomAttributesCacheGenerator,
	CryptographicUnexpectedOperationException_t2155_CustomAttributesCacheGenerator,
	CspParameters_t2310_CustomAttributesCacheGenerator,
	CspProviderFlags_t2684_CustomAttributesCacheGenerator,
	DES_t2321_CustomAttributesCacheGenerator,
	DESCryptoServiceProvider_t2687_CustomAttributesCacheGenerator,
	DSA_t2129_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t2148_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t2148_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t2148____PublicOnly_PropertyInfo,
	DSAParameters_t2149_CustomAttributesCacheGenerator,
	DSASignatureDeformatter_t2317_CustomAttributesCacheGenerator,
	DSASignatureFormatter_t2688_CustomAttributesCacheGenerator,
	HMAC_t2314_CustomAttributesCacheGenerator,
	HMACMD5_t2689_CustomAttributesCacheGenerator,
	HMACRIPEMD160_t2690_CustomAttributesCacheGenerator,
	HMACSHA1_t2313_CustomAttributesCacheGenerator,
	HMACSHA256_t2691_CustomAttributesCacheGenerator,
	HMACSHA384_t2692_CustomAttributesCacheGenerator,
	HMACSHA512_t2693_CustomAttributesCacheGenerator,
	HashAlgorithm_t2210_CustomAttributesCacheGenerator,
	ICryptoTransform_t2241_CustomAttributesCacheGenerator,
	ICspAsymmetricAlgorithm_t2987_CustomAttributesCacheGenerator,
	KeyedHashAlgorithm_t2234_CustomAttributesCacheGenerator,
	MACTripleDES_t2694_CustomAttributesCacheGenerator,
	MD5_t2315_CustomAttributesCacheGenerator,
	MD5CryptoServiceProvider_t2695_CustomAttributesCacheGenerator,
	PaddingMode_t2696_CustomAttributesCacheGenerator,
	RC2_t2322_CustomAttributesCacheGenerator,
	RC2CryptoServiceProvider_t2697_CustomAttributesCacheGenerator,
	RIPEMD160_t2699_CustomAttributesCacheGenerator,
	RIPEMD160Managed_t2700_CustomAttributesCacheGenerator,
	RSA_t2130_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t2145_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t2145_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t2145____PublicOnly_PropertyInfo,
	RSAPKCS1KeyExchangeFormatter_t2327_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureDeformatter_t2318_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureFormatter_t2702_CustomAttributesCacheGenerator,
	RSAParameters_t2147_CustomAttributesCacheGenerator,
	Rijndael_t2324_CustomAttributesCacheGenerator,
	RijndaelManaged_t2703_CustomAttributesCacheGenerator,
	RijndaelManagedTransform_t2705_CustomAttributesCacheGenerator,
	SHA1_t2159_CustomAttributesCacheGenerator,
	SHA1CryptoServiceProvider_t2707_CustomAttributesCacheGenerator,
	SHA1Managed_t2708_CustomAttributesCacheGenerator,
	SHA256_t2316_CustomAttributesCacheGenerator,
	SHA256Managed_t2709_CustomAttributesCacheGenerator,
	SHA384_t2710_CustomAttributesCacheGenerator,
	SHA384Managed_t2712_CustomAttributesCacheGenerator,
	SHA512_t2713_CustomAttributesCacheGenerator,
	SHA512Managed_t2714_CustomAttributesCacheGenerator,
	SignatureDescription_t2716_CustomAttributesCacheGenerator,
	SymmetricAlgorithm_t2217_CustomAttributesCacheGenerator,
	ToBase64Transform_t2719_CustomAttributesCacheGenerator,
	TripleDES_t2323_CustomAttributesCacheGenerator,
	TripleDESCryptoServiceProvider_t2720_CustomAttributesCacheGenerator,
	StrongNamePublicKeyBlob_t2722_CustomAttributesCacheGenerator,
	ApplicationTrust_t2724_CustomAttributesCacheGenerator,
	Evidence_t2524_CustomAttributesCacheGenerator,
	Evidence_t2524_CustomAttributesCacheGenerator_Evidence_Equals_m13226,
	Evidence_t2524_CustomAttributesCacheGenerator_Evidence_GetHashCode_m13228,
	Hash_t2726_CustomAttributesCacheGenerator,
	IIdentityPermissionFactory_t2989_CustomAttributesCacheGenerator,
	StrongName_t2727_CustomAttributesCacheGenerator,
	IPrincipal_t2766_CustomAttributesCacheGenerator,
	PrincipalPolicy_t2728_CustomAttributesCacheGenerator,
	IPermission_t2730_CustomAttributesCacheGenerator,
	ISecurityEncodable_t2990_CustomAttributesCacheGenerator,
	SecurityElement_t2419_CustomAttributesCacheGenerator,
	SecurityException_t2731_CustomAttributesCacheGenerator,
	SecurityException_t2731_CustomAttributesCacheGenerator_SecurityException_t2731____Demanded_PropertyInfo,
	SecuritySafeCriticalAttribute_t1704_CustomAttributesCacheGenerator,
	SuppressUnmanagedCodeSecurityAttribute_t2732_CustomAttributesCacheGenerator,
	UnverifiableCodeAttribute_t2733_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2734_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m13288,
	ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m13289,
	ASCIIEncoding_t2734_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m13290,
	Decoder_t2472_CustomAttributesCacheGenerator,
	Decoder_t2472_CustomAttributesCacheGenerator_Decoder_t2472____Fallback_PropertyInfo,
	Decoder_t2472_CustomAttributesCacheGenerator_Decoder_t2472____FallbackBuffer_PropertyInfo,
	DecoderReplacementFallback_t2740_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m13313,
	EncoderReplacementFallback_t2747_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m13343,
	Encoding_t1666_CustomAttributesCacheGenerator,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666_Encoding_InvokeI18N_m13374_Arg1_ParameterInfo,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_GetByteCount_m13390,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_GetBytes_m13391,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____IsReadOnly_PropertyInfo,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____DecoderFallback_PropertyInfo,
	Encoding_t1666_CustomAttributesCacheGenerator_Encoding_t1666____EncoderFallback_PropertyInfo,
	StringBuilder_t36_CustomAttributesCacheGenerator,
	StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m5743,
	StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m5742,
	StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_t36_StringBuilder_AppendFormat_m9814_Arg1_ParameterInfo,
	StringBuilder_t36_CustomAttributesCacheGenerator_StringBuilder_t36_StringBuilder_AppendFormat_m13423_Arg2_ParameterInfo,
	UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13433,
	UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13434,
	UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13443,
	UTF32Encoding_t2752_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13445,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13453,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13454,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13466,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13467,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13468,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13469,
	UTF7Encoding_t2755_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13470,
	UTF8Encoding_t2757_CustomAttributesCacheGenerator,
	UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13479,
	UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13484,
	UTF8Encoding_t2757_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13500,
	UnicodeEncoding_t2759_CustomAttributesCacheGenerator,
	UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13508,
	UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13511,
	UnicodeEncoding_t2759_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13515,
	EventResetMode_t2760_CustomAttributesCacheGenerator,
	EventWaitHandle_t2761_CustomAttributesCacheGenerator,
	ExecutionContext_t2601_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13527,
	ExecutionContext_t2601_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13528,
	Interlocked_t2762_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m864,
	ManualResetEvent_t2262_CustomAttributesCacheGenerator,
	Monitor_t2763_CustomAttributesCacheGenerator,
	Monitor_t2763_CustomAttributesCacheGenerator_Monitor_Exit_m3735,
	Mutex_t2597_CustomAttributesCacheGenerator,
	Mutex_t2597_CustomAttributesCacheGenerator_Mutex__ctor_m13529,
	Mutex_t2597_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13532,
	SynchronizationLockException_t2765_CustomAttributesCacheGenerator,
	Thread_t993_CustomAttributesCacheGenerator,
	Thread_t993_CustomAttributesCacheGenerator_local_slots,
	Thread_t993_CustomAttributesCacheGenerator__ec,
	Thread_t993_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13542,
	Thread_t993_CustomAttributesCacheGenerator_Thread_Finalize_m13554,
	Thread_t993_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13557,
	Thread_t993_CustomAttributesCacheGenerator_Thread_GetHashCode_m13558,
	ThreadAbortException_t2767_CustomAttributesCacheGenerator,
	ThreadInterruptedException_t2768_CustomAttributesCacheGenerator,
	ThreadState_t2769_CustomAttributesCacheGenerator,
	ThreadStateException_t2770_CustomAttributesCacheGenerator,
	WaitHandle_t2308_CustomAttributesCacheGenerator,
	WaitHandle_t2308_CustomAttributesCacheGenerator_WaitHandle_t2308____Handle_PropertyInfo,
	AccessViolationException_t2771_CustomAttributesCacheGenerator,
	ActivationContext_t2772_CustomAttributesCacheGenerator,
	ActivationContext_t2772_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13578,
	Activator_t2773_CustomAttributesCacheGenerator,
	Activator_t2773_CustomAttributesCacheGenerator_Activator_t2773_Activator_CreateInstance_m946_Arg1_ParameterInfo,
	AppDomain_t2774_CustomAttributesCacheGenerator,
	AppDomain_t2774_CustomAttributesCacheGenerator_type_resolve_in_progress,
	AppDomain_t2774_CustomAttributesCacheGenerator_assembly_resolve_in_progress,
	AppDomain_t2774_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly,
	AppDomain_t2774_CustomAttributesCacheGenerator__principal,
	AppDomainManager_t2775_CustomAttributesCacheGenerator,
	AppDomainSetup_t2782_CustomAttributesCacheGenerator,
	ApplicationException_t2783_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2776_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2776_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13603,
	ArgumentException_t1409_CustomAttributesCacheGenerator,
	ArgumentNullException_t908_CustomAttributesCacheGenerator,
	ArgumentOutOfRangeException_t909_CustomAttributesCacheGenerator,
	ArithmeticException_t2309_CustomAttributesCacheGenerator,
	ArrayTypeMismatchException_t2784_CustomAttributesCacheGenerator,
	AssemblyLoadEventArgs_t2785_CustomAttributesCacheGenerator,
	AttributeTargets_t2786_CustomAttributesCacheGenerator,
	Buffer_t2787_CustomAttributesCacheGenerator,
	CharEnumerator_t2788_CustomAttributesCacheGenerator,
	ContextBoundObject_t2789_CustomAttributesCacheGenerator,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13655,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13658,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13659,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToBoolean_m13660,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13670,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13674,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13675,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToByte_m13676,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13680,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13683,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13684,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToChar_m13685,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13693,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13694,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13695,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDateTime_m13696,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13703,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13706,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13707,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDecimal_m13708,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13717,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13720,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13721,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToDouble_m13722,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13732,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13734,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13735,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt16_m13736,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13744,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13747,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13748,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt32_m13749,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13757,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13761,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13762,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToInt64_m13763,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13766,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13767,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13768,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13769,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13770,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13771,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13772,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13773,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13774,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13775,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13776,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13777,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13778,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSByte_m13779,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13787,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13790,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13791,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToSingle_m13792,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13796,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13797,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13798,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13799,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13800,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13801,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13802,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13803,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13804,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13805,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13806,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13807,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13808,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt16_m13809,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m7495,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13810,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13811,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13812,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13813,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13814,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m3757,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13815,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13816,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13817,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13818,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13819,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13820,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m7494,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt32_m13821,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13822,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13823,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13824,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13825,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13826,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13827,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13828,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13829,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13830,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13831,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13832,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13833,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13834,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13835,
	Convert_t184_CustomAttributesCacheGenerator_Convert_ToUInt64_m13836,
	DBNull_t2790_CustomAttributesCacheGenerator,
	DateTimeKind_t2792_CustomAttributesCacheGenerator,
	DateTimeOffset_t2793_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13948,
	DayOfWeek_t2795_CustomAttributesCacheGenerator,
	DivideByZeroException_t2798_CustomAttributesCacheGenerator,
	DllNotFoundException_t2799_CustomAttributesCacheGenerator,
	EntryPointNotFoundException_t2801_CustomAttributesCacheGenerator,
	MonoEnumInfo_t2806_CustomAttributesCacheGenerator_cache,
	Environment_t2809_CustomAttributesCacheGenerator,
	SpecialFolder_t2807_CustomAttributesCacheGenerator,
	EventArgs_t2218_CustomAttributesCacheGenerator,
	ExecutionEngineException_t2810_CustomAttributesCacheGenerator,
	FieldAccessException_t2811_CustomAttributesCacheGenerator,
	FlagsAttribute_t999_CustomAttributesCacheGenerator,
	FormatException_t201_CustomAttributesCacheGenerator,
	GC_t2813_CustomAttributesCacheGenerator_GC_SuppressFinalize_m3901,
	Guid_t2814_CustomAttributesCacheGenerator,
	ICustomFormatter_t2891_CustomAttributesCacheGenerator,
	IFormatProvider_t2873_CustomAttributesCacheGenerator,
	IndexOutOfRangeException_t1683_CustomAttributesCacheGenerator,
	InvalidCastException_t229_CustomAttributesCacheGenerator,
	InvalidOperationException_t905_CustomAttributesCacheGenerator,
	LoaderOptimization_t2815_CustomAttributesCacheGenerator,
	LoaderOptimization_t2815_CustomAttributesCacheGenerator_DomainMask,
	LoaderOptimization_t2815_CustomAttributesCacheGenerator_DisallowBindings,
	Math_t2816_CustomAttributesCacheGenerator_Math_Max_m9825,
	Math_t2816_CustomAttributesCacheGenerator_Math_Min_m14046,
	Math_t2816_CustomAttributesCacheGenerator_Math_Sqrt_m14055,
	MemberAccessException_t2812_CustomAttributesCacheGenerator,
	MethodAccessException_t2817_CustomAttributesCacheGenerator,
	MissingFieldException_t2818_CustomAttributesCacheGenerator,
	MissingMemberException_t2819_CustomAttributesCacheGenerator,
	MissingMethodException_t2820_CustomAttributesCacheGenerator,
	MulticastNotSupportedException_t2826_CustomAttributesCacheGenerator,
	NonSerializedAttribute_t2827_CustomAttributesCacheGenerator,
	NotImplementedException_t933_CustomAttributesCacheGenerator,
	NotSupportedException_t192_CustomAttributesCacheGenerator,
	NullReferenceException_t1675_CustomAttributesCacheGenerator,
	NumberFormatter_t2829_CustomAttributesCacheGenerator_threadNumberFormatter,
	ObjectDisposedException_t2312_CustomAttributesCacheGenerator,
	OperatingSystem_t2808_CustomAttributesCacheGenerator,
	OutOfMemoryException_t2830_CustomAttributesCacheGenerator,
	OverflowException_t2831_CustomAttributesCacheGenerator,
	PlatformID_t2832_CustomAttributesCacheGenerator,
	Random_t207_CustomAttributesCacheGenerator,
	RankException_t2833_CustomAttributesCacheGenerator,
	ResolveEventArgs_t2834_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2835_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2835_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m14271,
	StringComparer_t1680_CustomAttributesCacheGenerator,
	StringComparison_t2838_CustomAttributesCacheGenerator,
	StringSplitOptions_t2839_CustomAttributesCacheGenerator,
	SystemException_t2160_CustomAttributesCacheGenerator,
	ThreadStaticAttribute_t2840_CustomAttributesCacheGenerator,
	TimeSpan_t190_CustomAttributesCacheGenerator,
	TimeZone_t2841_CustomAttributesCacheGenerator,
	TypeCode_t2843_CustomAttributesCacheGenerator,
	TypeInitializationException_t2844_CustomAttributesCacheGenerator,
	TypeLoadException_t2800_CustomAttributesCacheGenerator,
	UnauthorizedAccessException_t2845_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t2846_CustomAttributesCacheGenerator,
	Version_t1997_CustomAttributesCacheGenerator,
	WeakReference_t2633_CustomAttributesCacheGenerator,
	MemberFilter_t2338_CustomAttributesCacheGenerator,
	TypeFilter_t2540_CustomAttributesCacheGenerator,
	HeaderHandler_t2851_CustomAttributesCacheGenerator,
	AppDomainInitializer_t2781_CustomAttributesCacheGenerator,
	AssemblyLoadEventHandler_t2777_CustomAttributesCacheGenerator,
	EventHandler_t2779_CustomAttributesCacheGenerator,
	ResolveEventHandler_t2778_CustomAttributesCacheGenerator,
	UnhandledExceptionEventHandler_t2780_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2872_CustomAttributesCacheGenerator,
};
