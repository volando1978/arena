﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<cabezaScr/Frame>
struct IList_1_t3824;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<cabezaScr/Frame>
struct  ReadOnlyCollection_1_t3825  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<cabezaScr/Frame>::list
	Object_t* ___list_0;
};
