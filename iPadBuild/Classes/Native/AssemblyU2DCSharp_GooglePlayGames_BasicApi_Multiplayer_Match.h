﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
struct  ParticipantResult_t342 
{
	// System.Int32 GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult::value__
	int32_t ___value___1;
};
