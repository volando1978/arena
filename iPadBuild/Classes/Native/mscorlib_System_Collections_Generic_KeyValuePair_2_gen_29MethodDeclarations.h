﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct KeyValuePair_2_t4185;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25715(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t4185 *, String_t*, bool, MethodInfo*))KeyValuePair_2__ctor_m25622_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m25716(__this, method) (( String_t* (*) (KeyValuePair_2_t4185 *, MethodInfo*))KeyValuePair_2_get_Key_m25623_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25717(__this, ___value, method) (( void (*) (KeyValuePair_2_t4185 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m25624_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m25718(__this, method) (( bool (*) (KeyValuePair_2_t4185 *, MethodInfo*))KeyValuePair_2_get_Value_m25625_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25719(__this, ___value, method) (( void (*) (KeyValuePair_2_t4185 *, bool, MethodInfo*))KeyValuePair_2_set_Value_m25626_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m25720(__this, method) (( String_t* (*) (KeyValuePair_2_t4185 *, MethodInfo*))KeyValuePair_2_ToString_m25627_gshared)(__this, method)
