﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t4246;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m26107_gshared (DefaultComparer_t4246 * __this, MethodInfo* method);
#define DefaultComparer__ctor_m26107(__this, method) (( void (*) (DefaultComparer_t4246 *, MethodInfo*))DefaultComparer__ctor_m26107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m26108_gshared (DefaultComparer_t4246 * __this, DateTime_t48  ___x, DateTime_t48  ___y, MethodInfo* method);
#define DefaultComparer_Compare_m26108(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4246 *, DateTime_t48 , DateTime_t48 , MethodInfo*))DefaultComparer_Compare_m26108_gshared)(__this, ___x, ___y, method)
