﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.AchievementManager
struct AchievementManager_t401;
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback
struct FetchAllCallback_t398;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback
struct ShowAllUICallback_t400;
// GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback
struct FetchCallback_t399;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.AchievementManager/FetchAllCallback,System.IntPtr)
extern "C" void AchievementManager_AchievementManager_FetchAll_m1639 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t398 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Reveal(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" void AchievementManager_AchievementManager_Reveal_m1640 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Unlock(System.Runtime.InteropServices.HandleRef,System.String)
extern "C" void AchievementManager_AchievementManager_Unlock_m1641 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_ShowAllUI(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.AchievementManager/ShowAllUICallback,System.IntPtr)
extern "C" void AchievementManager_AchievementManager_ShowAllUI_m1642 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, ShowAllUICallback_t400 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_SetStepsAtLeast(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" void AchievementManager_AchievementManager_SetStepsAtLeast_m1643 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, uint32_t ___steps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Increment(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" void AchievementManager_AchievementManager_Increment_m1644 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___achievement_id, uint32_t ___steps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.AchievementManager/FetchCallback,System.IntPtr)
extern "C" void AchievementManager_AchievementManager_Fetch_m1645 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___achievement_id, FetchCallback_t399 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void AchievementManager_AchievementManager_FetchAllResponse_Dispose_m1646 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t AchievementManager_AchievementManager_FetchAllResponse_GetStatus_m1647 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetData_Length(System.Runtime.InteropServices.HandleRef)
extern "C" UIntPtr_t  AchievementManager_AchievementManager_FetchAllResponse_GetData_Length_m1648 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchAllResponse_GetData_GetElement(System.Runtime.InteropServices.HandleRef,System.UIntPtr)
extern "C" IntPtr_t AchievementManager_AchievementManager_FetchAllResponse_GetData_GetElement_m1649 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void AchievementManager_AchievementManager_FetchResponse_Dispose_m1650 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t AchievementManager_AchievementManager_FetchResponse_GetStatus_m1651 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.AchievementManager::AchievementManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t AchievementManager_AchievementManager_FetchResponse_GetData_m1652 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
