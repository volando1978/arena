﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>
struct  Enumerator_t3562 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::l
	List_1_t177 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>::current
	MarketItem_t122 * ___current_3;
};
