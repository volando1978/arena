﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t107;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t3281;
// GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3757;
// GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus[]
struct ParticipantStatusU5BU5D_t3786;
// System.Collections.Generic.IEqualityComparer`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus>
struct IEqualityComparer_1_t3758;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Collections.Generic.Dictionary`2/Transform`1<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus,System.Collections.DictionaryEntry>
struct Transform_1_t3787;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>
struct  Dictionary_2_t671  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::table
	Int32U5BU5D_t107* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::linkSlots
	LinkU5BU5D_t3281* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::keySlots
	ParticipantStatusU5BU5D_t3757* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::valueSlots
	ParticipantStatusU5BU5D_t3786* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::serialization_info
	SerializationInfo_t1673 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t671_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<GooglePlayGames.Native.Cwrapper.Types/ParticipantStatus,GooglePlayGames.BasicApi.Multiplayer.Participant/ParticipantStatus>::<>f__am$cacheB
	Transform_1_t3787 * ___U3CU3Ef__amU24cacheB_15;
};
