﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.EventManager
struct EventManager_t421;
// GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback
struct FetchAllCallback_t419;
// System.String
struct String_t;
// GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback
struct FetchCallback_t420;
// System.IntPtr[]
struct IntPtrU5BU5D_t859;
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAll(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,GooglePlayGames.Native.Cwrapper.EventManager/FetchAllCallback,System.IntPtr)
extern "C" void EventManager_EventManager_FetchAll_m1707 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, FetchAllCallback_t419 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Fetch(System.Runtime.InteropServices.HandleRef,GooglePlayGames.Native.Cwrapper.Types/DataSource,System.String,GooglePlayGames.Native.Cwrapper.EventManager/FetchCallback,System.IntPtr)
extern "C" void EventManager_EventManager_Fetch_m1708 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, int32_t ___data_source, String_t* ___event_id, FetchCallback_t420 * ___callback, IntPtr_t ___callback_arg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_Increment(System.Runtime.InteropServices.HandleRef,System.String,System.UInt32)
extern "C" void EventManager_EventManager_Increment_m1709 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, String_t* ___event_id, uint32_t ___steps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void EventManager_EventManager_FetchAllResponse_Dispose_m1710 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t EventManager_EventManager_FetchAllResponse_GetStatus_m1711 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchAllResponse_GetData(System.Runtime.InteropServices.HandleRef,System.IntPtr[],System.UIntPtr)
extern "C" UIntPtr_t  EventManager_EventManager_FetchAllResponse_GetData_m1712 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, IntPtrU5BU5D_t859* ___out_arg, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_Dispose(System.Runtime.InteropServices.HandleRef)
extern "C" void EventManager_EventManager_FetchResponse_Dispose_m1713 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetStatus(System.Runtime.InteropServices.HandleRef)
extern "C" int32_t EventManager_EventManager_FetchResponse_GetStatus_m1714 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.Cwrapper.EventManager::EventManager_FetchResponse_GetData(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t EventManager_EventManager_FetchResponse_GetData_m1715 (Object_t * __this /* static, unused */, HandleRef_t657  ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
