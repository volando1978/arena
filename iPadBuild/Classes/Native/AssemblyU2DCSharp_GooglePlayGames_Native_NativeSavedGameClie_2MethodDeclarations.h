﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44
struct U3CToOnGameThreadU3Ec__AnonStorey44_t620;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey44__ctor_m2507 (U3CToOnGameThreadU3Ec__AnonStorey44_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::<>m__50()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey44_U3CU3Em__50_m2508 (U3CToOnGameThreadU3Ec__AnonStorey44_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
