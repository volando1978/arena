﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t2511;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t2343;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t2343 * UnmanagedMarshal_ToMarshalAsAttribute_m12181 (UnmanagedMarshal_t2511 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
