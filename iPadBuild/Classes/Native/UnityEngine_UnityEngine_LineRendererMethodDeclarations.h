﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.LineRenderer
struct LineRenderer_t743;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.LineRenderer::SetWidth(System.Single,System.Single)
extern "C" void LineRenderer_SetWidth_m4122 (LineRenderer_t743 * __this, float ___start, float ___end, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetWidth(UnityEngine.LineRenderer,System.Single,System.Single)
extern "C" void LineRenderer_INTERNAL_CALL_SetWidth_m6460 (Object_t * __this /* static, unused */, LineRenderer_t743 * ___self, float ___start, float ___end, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetColors(UnityEngine.Color,UnityEngine.Color)
extern "C" void LineRenderer_SetColors_m4111 (LineRenderer_t743 * __this, Color_t747  ___start, Color_t747  ___end, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetColors(UnityEngine.LineRenderer,UnityEngine.Color&,UnityEngine.Color&)
extern "C" void LineRenderer_INTERNAL_CALL_SetColors_m6461 (Object_t * __this /* static, unused */, LineRenderer_t743 * ___self, Color_t747 * ___start, Color_t747 * ___end, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetVertexCount(System.Int32)
extern "C" void LineRenderer_SetVertexCount_m4123 (LineRenderer_t743 * __this, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetVertexCount(UnityEngine.LineRenderer,System.Int32)
extern "C" void LineRenderer_INTERNAL_CALL_SetVertexCount_m6462 (Object_t * __this /* static, unused */, LineRenderer_t743 * ___self, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern "C" void LineRenderer_SetPosition_m4126 (LineRenderer_t743 * __this, int32_t ___index, Vector3_t758  ___position, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetPosition(UnityEngine.LineRenderer,System.Int32,UnityEngine.Vector3&)
extern "C" void LineRenderer_INTERNAL_CALL_SetPosition_m6463 (Object_t * __this /* static, unused */, LineRenderer_t743 * ___self, int32_t ___index, Vector3_t758 * ___position, MethodInfo* method) IL2CPP_METHOD_ATTR;
