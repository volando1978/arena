﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t41;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t218  : public ValueType_t329
{
};
struct Enum_t218_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t41* ___split_char_0;
};
