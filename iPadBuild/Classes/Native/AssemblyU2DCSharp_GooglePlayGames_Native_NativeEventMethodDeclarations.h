﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t36;
// GooglePlayGames.BasicApi.Events.EventVisibility
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_EventVisib.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.NativeEvent::.ctor(System.IntPtr)
extern "C" void NativeEvent__ctor_m2738 (NativeEvent_t674 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeEvent::get_Id()
extern "C" String_t* NativeEvent_get_Id_m2739 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeEvent::get_Name()
extern "C" String_t* NativeEvent_get_Name_m2740 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeEvent::get_Description()
extern "C" String_t* NativeEvent_get_Description_m2741 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeEvent::get_ImageUrl()
extern "C" String_t* NativeEvent_get_ImageUrl_m2742 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 GooglePlayGames.Native.NativeEvent::get_CurrentCount()
extern "C" uint64_t NativeEvent_get_CurrentCount_m2743 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Events.EventVisibility GooglePlayGames.Native.NativeEvent::get_Visibility()
extern "C" int32_t NativeEvent_get_Visibility_m2744 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeEvent::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeEvent_CallDispose_m2745 (NativeEvent_t674 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeEvent::ToString()
extern "C" String_t* NativeEvent_ToString_m2746 (NativeEvent_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Id>m__7E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_IdU3Em__7E_m2747 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Name>m__7F(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_NameU3Em__7F_m2748 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_Description>m__80(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_DescriptionU3Em__80_m2749 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.NativeEvent::<get_ImageUrl>m__81(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeEvent_U3Cget_ImageUrlU3Em__81_m2750 (NativeEvent_t674 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___out_size, MethodInfo* method) IL2CPP_METHOD_ATTR;
