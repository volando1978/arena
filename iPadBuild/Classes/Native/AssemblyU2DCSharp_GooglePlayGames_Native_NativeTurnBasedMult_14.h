﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.TurnBasedManager
struct TurnBasedManager_t651;
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile)
struct Action_2_t652;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient
struct  NativeTurnBasedMultiplayerClient_t535  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.TurnBasedManager GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::mTurnBasedManager
	TurnBasedManager_t651 * ___mTurnBasedManager_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::mNativeClient
	NativeClient_t524 * ___mNativeClient_1;
	// System.Action`2<GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch,System.Boolean> modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.Native.NativeTurnBasedMultiplayerClient::mMatchDelegate
	Action_2_t652 * ___mMatchDelegate_2;
};
