﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.PlayGamesLocalUser
struct PlayGamesLocalUser_t385;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t850;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t384;
// GooglePlayGames.PlayGamesPlatform
struct PlayGamesPlatform_t382;
// System.Action`1<System.Boolean>
struct Action_1_t98;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"

// System.Void GooglePlayGames.PlayGamesLocalUser::.ctor(GooglePlayGames.PlayGamesPlatform)
extern "C" void PlayGamesLocalUser__ctor_m1508 (PlayGamesLocalUser_t385 * __this, PlayGamesPlatform_t382 * ___plaf, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>)
extern "C" void PlayGamesLocalUser_Authenticate_m1509 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesLocalUser::Authenticate(System.Action`1<System.Boolean>,System.Boolean)
extern "C" void PlayGamesLocalUser_Authenticate_m1510 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, bool ___silent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.PlayGamesLocalUser::LoadFriends(System.Action`1<System.Boolean>)
extern "C" void PlayGamesLocalUser_LoadFriends_m1511 (PlayGamesLocalUser_t385 * __this, Action_1_t98 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.IUserProfile[] GooglePlayGames.PlayGamesLocalUser::get_friends()
extern "C" IUserProfileU5BU5D_t850* PlayGamesLocalUser_get_friends_m1512 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_authenticated()
extern "C" bool PlayGamesLocalUser_get_authenticated_m1513 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_underage()
extern "C" bool PlayGamesLocalUser_get_underage_m1514 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesLocalUser::get_userName()
extern "C" String_t* PlayGamesLocalUser_get_userName_m1515 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesLocalUser::get_id()
extern "C" String_t* PlayGamesLocalUser_get_id_m1516 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesLocalUser::get_isFriend()
extern "C" bool PlayGamesLocalUser_get_isFriend_m1517 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesLocalUser::get_state()
extern "C" int32_t PlayGamesLocalUser_get_state_m1518 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::LoadImage()
extern "C" Texture2D_t384 * PlayGamesLocalUser_LoadImage_m1519 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::get_image()
extern "C" Texture2D_t384 * PlayGamesLocalUser_get_image_m1520 (PlayGamesLocalUser_t385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
