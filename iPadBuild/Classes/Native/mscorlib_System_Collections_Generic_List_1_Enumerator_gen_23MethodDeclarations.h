﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Action>
struct Enumerator_t3704;
// System.Object
struct Object_t;
// System.Action
struct Action_t588;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t393;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m19599(__this, ___l, method) (( void (*) (Enumerator_t3704 *, List_1_t393 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19600(__this, method) (( Object_t * (*) (Enumerator_t3704 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action>::Dispose()
#define Enumerator_Dispose_m19601(__this, method) (( void (*) (Enumerator_t3704 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action>::VerifyState()
#define Enumerator_VerifyState_m19602(__this, method) (( void (*) (Enumerator_t3704 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action>::MoveNext()
#define Enumerator_MoveNext_m19603(__this, method) (( bool (*) (Enumerator_t3704 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action>::get_Current()
#define Enumerator_get_Current_m19604(__this, method) (( Action_t588 * (*) (Enumerator_t3704 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
