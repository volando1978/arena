﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>
struct KeyCollection_t3715;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,GooglePlayGames.BasicApi.Achievement>
struct Dictionary_2_t542;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t34;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.String[]
struct StringU5BU5D_t169;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,GooglePlayGames.BasicApi.Achievement>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_49.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_2MethodDeclarations.h"
#define KeyCollection__ctor_m19688(__this, ___dictionary, method) (( void (*) (KeyCollection_t3715 *, Dictionary_2_t542 *, MethodInfo*))KeyCollection__ctor_m16134_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m19689(__this, ___item, method) (( void (*) (KeyCollection_t3715 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16136_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m19690(__this, method) (( void (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m19691(__this, ___item, method) (( bool (*) (KeyCollection_t3715 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16140_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m19692(__this, ___item, method) (( bool (*) (KeyCollection_t3715 *, String_t*, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16142_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m19693(__this, method) (( Object_t* (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16144_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m19694(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3715 *, Array_t *, int32_t, MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m16146_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m19695(__this, method) (( Object_t * (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16148_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m19696(__this, method) (( bool (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16150_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m19697(__this, method) (( bool (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16152_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m19698(__this, method) (( Object_t * (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m16154_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m19699(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3715 *, StringU5BU5D_t169*, int32_t, MethodInfo*))KeyCollection_CopyTo_m16156_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::GetEnumerator()
#define KeyCollection_GetEnumerator_m19700(__this, method) (( Enumerator_t4376  (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_GetEnumerator_m16158_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,GooglePlayGames.BasicApi.Achievement>::get_Count()
#define KeyCollection_get_Count_m19701(__this, method) (( int32_t (*) (KeyCollection_t3715 *, MethodInfo*))KeyCollection_get_Count_m16160_gshared)(__this, method)
