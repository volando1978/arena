﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// gameControl/<ChangeLevelWait>c__IteratorC
struct U3CChangeLevelWaitU3Ec__IteratorC_t800;
// System.Object
struct Object_t;

// System.Void gameControl/<ChangeLevelWait>c__IteratorC::.ctor()
extern "C" void U3CChangeLevelWaitU3Ec__IteratorC__ctor_m3443 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<ChangeLevelWait>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3444 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object gameControl/<ChangeLevelWait>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CChangeLevelWaitU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3445 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean gameControl/<ChangeLevelWait>c__IteratorC::MoveNext()
extern "C" bool U3CChangeLevelWaitU3Ec__IteratorC_MoveNext_m3446 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<ChangeLevelWait>c__IteratorC::Dispose()
extern "C" void U3CChangeLevelWaitU3Ec__IteratorC_Dispose_m3447 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gameControl/<ChangeLevelWait>c__IteratorC::Reset()
extern "C" void U3CChangeLevelWaitU3Ec__IteratorC_Reset_m3448 (U3CChangeLevelWaitU3Ec__IteratorC_t800 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
