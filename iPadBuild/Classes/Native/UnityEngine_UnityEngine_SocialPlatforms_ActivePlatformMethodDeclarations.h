﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.ActivePlatform
struct ActivePlatform_t1634;
// UnityEngine.SocialPlatforms.ISocialPlatform
struct ISocialPlatform_t1021;

// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::get_Instance()
extern "C" Object_t * ActivePlatform_get_Instance_m7382 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.ActivePlatform::set_Instance(UnityEngine.SocialPlatforms.ISocialPlatform)
extern "C" void ActivePlatform_set_Instance_m7383 (Object_t * __this /* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::SelectSocialPlatform()
extern "C" Object_t * ActivePlatform_SelectSocialPlatform_m7384 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
