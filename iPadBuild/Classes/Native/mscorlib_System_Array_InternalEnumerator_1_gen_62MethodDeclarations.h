﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Uri/UriScheme>
struct InternalEnumerator_1_t4197;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"

// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m25794_gshared (InternalEnumerator_1_t4197 * __this, Array_t * ___array, MethodInfo* method);
#define InternalEnumerator_1__ctor_m25794(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4197 *, Array_t *, MethodInfo*))InternalEnumerator_1__ctor_m25794_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25795_gshared (InternalEnumerator_1_t4197 * __this, MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25795(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4197 *, MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25795_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m25796_gshared (InternalEnumerator_1_t4197 * __this, MethodInfo* method);
#define InternalEnumerator_1_Dispose_m25796(__this, method) (( void (*) (InternalEnumerator_1_t4197 *, MethodInfo*))InternalEnumerator_1_Dispose_m25796_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m25797_gshared (InternalEnumerator_1_t4197 * __this, MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m25797(__this, method) (( bool (*) (InternalEnumerator_1_t4197 *, MethodInfo*))InternalEnumerator_1_MoveNext_m25797_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C" UriScheme_t2118  InternalEnumerator_1_get_Current_m25798_gshared (InternalEnumerator_1_t4197 * __this, MethodInfo* method);
#define InternalEnumerator_1_get_Current_m25798(__this, method) (( UriScheme_t2118  (*) (InternalEnumerator_1_t4197 *, MethodInfo*))InternalEnumerator_1_get_Current_m25798_gshared)(__this, method)
