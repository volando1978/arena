﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback
struct AuthFinishedCallback_t665;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/AuthOperation
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Auth.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_0.h"

// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::.ctor(System.Object,System.IntPtr)
extern "C" void AuthFinishedCallback__ctor_m2670 (AuthFinishedCallback_t665 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus)
extern "C" void AuthFinishedCallback_Invoke_m2671 (AuthFinishedCallback_t665 * __this, int32_t ___operation, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AuthFinishedCallback_t665(Il2CppObject* delegate, int32_t ___operation, int32_t ___status);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/AuthOperation,GooglePlayGames.Native.Cwrapper.CommonErrorStatus/AuthStatus,System.AsyncCallback,System.Object)
extern "C" Object_t * AuthFinishedCallback_BeginInvoke_m2672 (AuthFinishedCallback_t665 * __this, int32_t ___operation, int32_t ___status, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.GameServicesBuilder/AuthFinishedCallback::EndInvoke(System.IAsyncResult)
extern "C" void AuthFinishedCallback_EndInvoke_m2673 (AuthFinishedCallback_t665 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
