﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.ConversionUtils
struct ConversionUtils_t396;
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// GooglePlayGames.BasicApi.UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// GooglePlayGames.Native.Cwrapper.Types/DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Data.h"
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"

// GooglePlayGames.BasicApi.ResponseStatus GooglePlayGames.Native.ConversionUtils::ConvertResponseStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus)
extern "C" int32_t ConversionUtils_ConvertResponseStatus_m1610 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.UIStatus GooglePlayGames.Native.ConversionUtils::ConvertUIStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" int32_t ConversionUtils_ConvertUIStatus_m1611 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/DataSource GooglePlayGames.Native.ConversionUtils::AsDataSource(GooglePlayGames.BasicApi.DataSource)
extern "C" int32_t ConversionUtils_AsDataSource_m1612 (Object_t * __this /* static, unused */, int32_t ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
