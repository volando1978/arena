﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.TurnBasedManager
struct TurnBasedManager_t651;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.String
struct String_t;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>
struct Action_1_t864;
// GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig
struct TurnBasedMatchConfig_t710;
// System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
struct Action_1_t885;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>
struct Action_1_t898;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch
struct NativeTurnBasedMatch_t680;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>
struct Action_1_t899;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
struct Action_1_t890;
// GooglePlayGames.Native.PInvoke.ParticipantResults
struct ParticipantResults_t683;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"

// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void TurnBasedManager__ctor_m3080 (TurnBasedManager_t651 * __this, GameServices_t534 * ___services, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::GetMatch(System.String,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_GetMatch_m3081 (TurnBasedManager_t651 * __this, String_t* ___matchId, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalTurnBasedMatchCallback(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedManager_InternalTurnBasedMatchCallback_m3082 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::CreateMatch(GooglePlayGames.Native.PInvoke.TurnBasedMatchConfig,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_CreateMatch_m3083 (TurnBasedManager_t651 * __this, TurnBasedMatchConfig_t710 * ___config, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ShowPlayerSelectUI(System.UInt32,System.UInt32,System.Boolean,System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>)
extern "C" void TurnBasedManager_ShowPlayerSelectUI_m3084 (TurnBasedManager_t651 * __this, uint32_t ___minimumPlayers, uint32_t ___maxiumPlayers, bool ___allowAutomatching, Action_1_t885 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalPlayerSelectUIcallback(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedManager_InternalPlayerSelectUIcallback_m3085 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::GetAllTurnbasedMatches(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchesResponse>)
extern "C" void TurnBasedManager_GetAllTurnbasedMatches_m3086 (TurnBasedManager_t651 * __this, Action_1_t898 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalTurnBasedMatchesCallback(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedManager_InternalTurnBasedMatchesCallback_m3087 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::AcceptInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_AcceptInvitation_m3088 (TurnBasedManager_t651 * __this, MultiplayerInvitation_t601 * ___invitation, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::DeclineInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void TurnBasedManager_DeclineInvitation_m3089 (TurnBasedManager_t651 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::TakeTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Byte[],GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_TakeTurn_m3090 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, ByteU5BU5D_t350* ___data, MultiplayerParticipant_t672 * ___nextParticipant, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalMatchInboxUICallback(System.IntPtr,System.IntPtr)
extern "C" void TurnBasedManager_InternalMatchInboxUICallback_m3091 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ShowInboxUI(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse>)
extern "C" void TurnBasedManager_ShowInboxUI_m3092 (TurnBasedManager_t651 * __this, Action_1_t899 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::InternalMultiplayerStatusCallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern "C" void TurnBasedManager_InternalMultiplayerStatusCallback_m3093 (Object_t * __this /* static, unused */, int32_t ___status, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::LeaveDuringMyTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern "C" void TurnBasedManager_LeaveDuringMyTurn_m3094 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, MultiplayerParticipant_t672 * ___nextParticipant, Action_1_t890 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::FinishMatchDuringMyTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Byte[],GooglePlayGames.Native.PInvoke.ParticipantResults,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_FinishMatchDuringMyTurn_m3095 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, ByteU5BU5D_t350* ___data, ParticipantResults_t683 * ___results, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::ConfirmPendingCompletion(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_ConfirmPendingCompletion_m3096 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::LeaveMatchDuringTheirTurn(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern "C" void TurnBasedManager_LeaveMatchDuringTheirTurn_m3097 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, Action_1_t890 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::CancelMatch(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern "C" void TurnBasedManager_CancelMatch_m3098 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, Action_1_t890 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.TurnBasedManager::Rematch(GooglePlayGames.Native.PInvoke.NativeTurnBasedMatch,System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" void TurnBasedManager_Rematch_m3099 (TurnBasedManager_t651 * __this, NativeTurnBasedMatch_t680 * ___match, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.TurnBasedManager::ToCallbackPointer(System.Action`1<GooglePlayGames.Native.PInvoke.TurnBasedManager/TurnBasedMatchResponse>)
extern "C" IntPtr_t TurnBasedManager_ToCallbackPointer_m3100 (Object_t * __this /* static, unused */, Action_1_t864 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
