﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Int32>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722;

// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Int32>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19754_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19754(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1__ctor_m19754_gshared)(__this, method)
// System.Void GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Int32>::<>m__E(T)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19755_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722 * __this, int32_t ___result, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19755(__this, ___result, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722 *, int32_t, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_U3CU3Em__E_m19755_gshared)(__this, ___result, method)
