﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>
struct KeyValuePair_2_t3677;
// System.String
struct String_t;
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multiplayer_Match.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m19150(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3677 *, String_t*, int32_t, MethodInfo*))KeyValuePair_2__ctor_m17099_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Key()
#define KeyValuePair_2_get_Key_m19151(__this, method) (( String_t* (*) (KeyValuePair_2_t3677 *, MethodInfo*))KeyValuePair_2_get_Key_m17100_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19152(__this, ___value, method) (( void (*) (KeyValuePair_2_t3677 *, String_t*, MethodInfo*))KeyValuePair_2_set_Key_m17101_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::get_Value()
#define KeyValuePair_2_get_Value_m19153(__this, method) (( int32_t (*) (KeyValuePair_2_t3677 *, MethodInfo*))KeyValuePair_2_get_Value_m17102_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19154(__this, ___value, method) (( void (*) (KeyValuePair_2_t3677 *, int32_t, MethodInfo*))KeyValuePair_2_set_Value_m17103_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult>::ToString()
#define KeyValuePair_2_ToString_m19155(__this, method) (( String_t* (*) (KeyValuePair_2_t3677 *, MethodInfo*))KeyValuePair_2_ToString_m17104_gshared)(__this, method)
