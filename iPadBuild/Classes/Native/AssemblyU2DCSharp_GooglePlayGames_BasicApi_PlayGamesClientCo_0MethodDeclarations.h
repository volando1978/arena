﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
struct PlayGamesClientConfiguration_t366;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t363;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t364;
// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
struct Builder_t365;

// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.ctor(GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder)
extern "C" void PlayGamesClientConfiguration__ctor_m1477 (PlayGamesClientConfiguration_t366 * __this, Builder_t365 * ___builder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.BasicApi.PlayGamesClientConfiguration::.cctor()
extern "C" void PlayGamesClientConfiguration__cctor_m1478 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableSavedGames()
extern "C" bool PlayGamesClientConfiguration_get_EnableSavedGames_m1479 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_EnableDeprecatedCloudSave()
extern "C" bool PlayGamesClientConfiguration_get_EnableDeprecatedCloudSave_m1480 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_InvitationDelegate()
extern "C" InvitationReceivedDelegate_t363 * PlayGamesClientConfiguration_get_InvitationDelegate_m1481 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::get_MatchDelegate()
extern "C" MatchDelegate_t364 * PlayGamesClientConfiguration_get_MatchDelegate_m1482 (PlayGamesClientConfiguration_t366 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
