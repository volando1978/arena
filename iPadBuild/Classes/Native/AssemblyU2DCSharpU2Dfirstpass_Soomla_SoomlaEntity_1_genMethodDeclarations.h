﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.SoomlaEntity`1<Soomla.Reward>
struct SoomlaEntity_1_t59;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;
// System.Object
struct Object_t;
// Soomla.Reward
struct Reward_t55;

// System.Void Soomla.SoomlaEntity`1<Soomla.Reward>::.ctor(System.String)
// Soomla.SoomlaEntity`1<System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_SoomlaEntity_1_gen_1MethodDeclarations.h"
#define SoomlaEntity_1__ctor_m16330(__this, ___id, method) (( void (*) (SoomlaEntity_1_t59 *, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16331_gshared)(__this, ___id, method)
// System.Void Soomla.SoomlaEntity`1<Soomla.Reward>::.ctor(System.String,System.String,System.String)
#define SoomlaEntity_1__ctor_m941(__this, ___id, ___name, ___description, method) (( void (*) (SoomlaEntity_1_t59 *, String_t*, String_t*, String_t*, MethodInfo*))SoomlaEntity_1__ctor_m16332_gshared)(__this, ___id, ___name, ___description, method)
// System.Void Soomla.SoomlaEntity`1<Soomla.Reward>::.ctor(JSONObject)
#define SoomlaEntity_1__ctor_m942(__this, ___jsonEntity, method) (( void (*) (SoomlaEntity_1_t59 *, JSONObject_t30 *, MethodInfo*))SoomlaEntity_1__ctor_m16333_gshared)(__this, ___jsonEntity, method)
// System.String Soomla.SoomlaEntity`1<Soomla.Reward>::get_ID()
#define SoomlaEntity_1_get_ID_m859(__this, method) (( String_t* (*) (SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_get_ID_m16334_gshared)(__this, method)
// JSONObject Soomla.SoomlaEntity`1<Soomla.Reward>::toJSONObject()
#define SoomlaEntity_1_toJSONObject_m944(__this, method) (( JSONObject_t30 * (*) (SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_toJSONObject_m16335_gshared)(__this, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Reward>::Equals(System.Object)
#define SoomlaEntity_1_Equals_m1307(__this, ___obj, method) (( bool (*) (SoomlaEntity_1_t59 *, Object_t *, MethodInfo*))SoomlaEntity_1_Equals_m16336_gshared)(__this, ___obj, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Reward>::Equals(Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_Equals_m16337(__this, ___g, method) (( bool (*) (SoomlaEntity_1_t59 *, SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_Equals_m16338_gshared)(__this, ___g, method)
// System.Int32 Soomla.SoomlaEntity`1<Soomla.Reward>::GetHashCode()
#define SoomlaEntity_1_GetHashCode_m1308(__this, method) (( int32_t (*) (SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_GetHashCode_m16339_gshared)(__this, method)
// T Soomla.SoomlaEntity`1<Soomla.Reward>::Clone(System.String)
#define SoomlaEntity_1_Clone_m1309(__this, ___newId, method) (( Reward_t55 * (*) (SoomlaEntity_1_t59 *, String_t*, MethodInfo*))SoomlaEntity_1_Clone_m16340_gshared)(__this, ___newId, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Reward>::op_Equality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_op_Equality_m938(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t59 *, SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_op_Equality_m16341_gshared)(__this /* static, unused */, ___a, ___b, method)
// System.Boolean Soomla.SoomlaEntity`1<Soomla.Reward>::op_Inequality(Soomla.SoomlaEntity`1<T>,Soomla.SoomlaEntity`1<T>)
#define SoomlaEntity_1_op_Inequality_m16342(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, SoomlaEntity_1_t59 *, SoomlaEntity_1_t59 *, MethodInfo*))SoomlaEntity_1_op_Inequality_m16343_gshared)(__this /* static, unused */, ___a, ___b, method)
