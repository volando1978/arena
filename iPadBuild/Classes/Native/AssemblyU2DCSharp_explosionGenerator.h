﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// explosionGenerator
struct  explosionGenerator_t791  : public MonoBehaviour_t26
{
	// UnityEngine.Vector2 explosionGenerator::randomPos
	Vector2_t739  ___randomPos_2;
	// UnityEngine.GameObject explosionGenerator::explosion
	GameObject_t144 * ___explosion_3;
	// System.Single explosionGenerator::distanciaMax
	float ___distanciaMax_4;
	// System.Int32 explosionGenerator::factor
	int32_t ___factor_5;
};
