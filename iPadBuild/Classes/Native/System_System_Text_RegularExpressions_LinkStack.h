﻿#pragma once
#include <stdint.h>
// System.Collections.Stack
struct Stack_t1661;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t2081  : public LinkRef_t2077
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t1661 * ___stack_0;
};
