﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.GameAssets
struct GameAssets_t80;
// Soomla.Store.VirtualCurrency[]
struct VirtualCurrencyU5BU5D_t172;
// Soomla.Store.VirtualGood[]
struct VirtualGoodU5BU5D_t173;
// Soomla.Store.VirtualCurrencyPack[]
struct VirtualCurrencyPackU5BU5D_t174;
// Soomla.Store.VirtualCategory[]
struct VirtualCategoryU5BU5D_t175;

// System.Void Soomla.Store.GameAssets::.ctor()
extern "C" void GameAssets__ctor_m297 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.GameAssets::.cctor()
extern "C" void GameAssets__cctor_m298 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.GameAssets::GetVersion()
extern "C" int32_t GameAssets_GetVersion_m299 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualCurrency[] Soomla.Store.GameAssets::GetCurrencies()
extern "C" VirtualCurrencyU5BU5D_t172* GameAssets_GetCurrencies_m300 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualGood[] Soomla.Store.GameAssets::GetGoods()
extern "C" VirtualGoodU5BU5D_t173* GameAssets_GetGoods_m301 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualCurrencyPack[] Soomla.Store.GameAssets::GetCurrencyPacks()
extern "C" VirtualCurrencyPackU5BU5D_t174* GameAssets_GetCurrencyPacks_m302 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Soomla.Store.VirtualCategory[] Soomla.Store.GameAssets::GetCategories()
extern "C" VirtualCategoryU5BU5D_t175* GameAssets_GetCategories_m303 (GameAssets_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
