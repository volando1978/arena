﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41
struct  U3CHandleConnectedSetChangedU3Ec__AnonStorey41_t585  : public Object_t
{
	// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ActiveState/<HandleConnectedSetChanged>c__AnonStorey41::selfId
	String_t* ___selfId_0;
};
