﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t4193;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Int32>
struct  Comparer_1_t4193  : public Object_t
{
};
struct Comparer_1_t4193_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::_default
	Comparer_1_t4193 * ____default_0;
};
