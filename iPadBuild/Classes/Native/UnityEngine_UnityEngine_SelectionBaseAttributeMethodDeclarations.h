﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t1441;

// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m6245 (SelectionBaseAttribute_t1441 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
