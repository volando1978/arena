﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.UnsupportedSavedGamesClient
struct UnsupportedSavedGamesClient_t714;
// System.String
struct String_t;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t612;
// GooglePlayGames.BasicApi.SavedGame.ConflictCallback
struct ConflictCallback_t621;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>
struct Action_2_t625;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t627;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>
struct Action_2_t630;
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
// GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_Conflic.h"
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SavedGa_1.h"

// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::.ctor(System.String)
extern "C" void UnsupportedSavedGamesClient__ctor_m3124 (UnsupportedSavedGamesClient_t714 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::OpenWithAutomaticConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void UnsupportedSavedGamesClient_OpenWithAutomaticConflictResolution_m3125 (UnsupportedSavedGamesClient_t714 * __this, String_t* ___filename, int32_t ___source, int32_t ___resolutionStrategy, Action_2_t612 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::OpenWithManualConflictResolution(System.String,GooglePlayGames.BasicApi.DataSource,System.Boolean,GooglePlayGames.BasicApi.SavedGame.ConflictCallback,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void UnsupportedSavedGamesClient_OpenWithManualConflictResolution_m3126 (UnsupportedSavedGamesClient_t714 * __this, String_t* ___filename, int32_t ___source, bool ___prefetchDataOnConflict, ConflictCallback_t621 * ___conflictCallback, Action_2_t612 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::ReadBinaryData(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Byte[]>)
extern "C" void UnsupportedSavedGamesClient_ReadBinaryData_m3127 (UnsupportedSavedGamesClient_t714 * __this, Object_t * ___metadata, Action_2_t625 * ___completedCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::ShowSelectSavedGameUI(System.String,System.UInt32,System.Boolean,System.Boolean,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void UnsupportedSavedGamesClient_ShowSelectSavedGameUI_m3128 (UnsupportedSavedGamesClient_t714 * __this, String_t* ___uiTitle, uint32_t ___maxDisplayedSavedGames, bool ___showCreateSaveUI, bool ___showDeleteSaveUI, Action_2_t627 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::CommitUpdate(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate,System.Byte[],System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>)
extern "C" void UnsupportedSavedGamesClient_CommitUpdate_m3129 (UnsupportedSavedGamesClient_t714 * __this, Object_t * ___metadata, SavedGameMetadataUpdate_t378  ___updateForMetadata, ByteU5BU5D_t350* ___updatedBinaryData, Action_2_t612 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::FetchAllSavedGames(GooglePlayGames.BasicApi.DataSource,System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>>)
extern "C" void UnsupportedSavedGamesClient_FetchAllSavedGames_m3130 (UnsupportedSavedGamesClient_t714 * __this, int32_t ___source, Action_2_t630 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.UnsupportedSavedGamesClient::Delete(GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata)
extern "C" void UnsupportedSavedGamesClient_Delete_m3131 (UnsupportedSavedGamesClient_t714 * __this, Object_t * ___metadata, MethodInfo* method) IL2CPP_METHOD_ATTR;
