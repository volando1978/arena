﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct IList_1_t3648;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct  ReadOnlyCollection_1_t3649  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<GooglePlayGames.BasicApi.Multiplayer.Participant>::list
	Object_t* ___list_0;
};
