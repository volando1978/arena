﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// animKillObj
struct animKillObj_t762;

// System.Void animKillObj::.ctor()
extern "C" void animKillObj__ctor_m3305 (animKillObj_t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void animKillObj::callEvent()
extern "C" void animKillObj_callEvent_m3306 (animKillObj_t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void animKillObj::getTransparent()
extern "C" void animKillObj_getTransparent_m3307 (animKillObj_t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
