﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct IEnumerable_1_t874;
// System.Text.StringBuilder
struct StringBuilder_t36;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Real.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::.ctor(System.IntPtr)
extern "C" void NativeRealTimeRoom__ctor_m2796 (NativeRealTimeRoom_t574 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Id()
extern "C" String_t* NativeRealTimeRoom_Id_m2797 (NativeRealTimeRoom_t574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant> GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Participants()
extern "C" Object_t* NativeRealTimeRoom_Participants_m2798 (NativeRealTimeRoom_t574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::ParticipantCount()
extern "C" uint32_t NativeRealTimeRoom_ParticipantCount_m2799 (NativeRealTimeRoom_t574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.Types/RealTimeRoomStatus GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::Status()
extern "C" int32_t NativeRealTimeRoom_Status_m2800 (NativeRealTimeRoom_t574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void NativeRealTimeRoom_CallDispose_m2801 (NativeRealTimeRoom_t574 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::FromPointer(System.IntPtr)
extern "C" NativeRealTimeRoom_t574 * NativeRealTimeRoom_FromPointer_m2802 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::<Id>m__8E(System.Text.StringBuilder,System.UIntPtr)
extern "C" UIntPtr_t  NativeRealTimeRoom_U3CIdU3Em__8E_m2803 (NativeRealTimeRoom_t574 * __this, StringBuilder_t36 * ___out_string, UIntPtr_t  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant GooglePlayGames.Native.PInvoke.NativeRealTimeRoom::<Participants>m__8F(System.UIntPtr)
extern "C" MultiplayerParticipant_t672 * NativeRealTimeRoom_U3CParticipantsU3Em__8F_m2804 (NativeRealTimeRoom_t574 * __this, UIntPtr_t  ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
