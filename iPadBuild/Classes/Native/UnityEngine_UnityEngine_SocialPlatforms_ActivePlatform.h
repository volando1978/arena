﻿#pragma once
#include <stdint.h>
// UnityEngine.SocialPlatforms.ISocialPlatform
struct ISocialPlatform_t1021;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.SocialPlatforms.ActivePlatform
struct  ActivePlatform_t1634  : public Object_t
{
};
struct ActivePlatform_t1634_StaticFields{
	// UnityEngine.SocialPlatforms.ISocialPlatform UnityEngine.SocialPlatforms.ActivePlatform::_active
	Object_t * ____active_0;
};
