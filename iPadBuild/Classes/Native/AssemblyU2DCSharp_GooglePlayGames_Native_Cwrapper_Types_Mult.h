﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
struct  MultiplayerEvent_t518 
{
	// System.Int32 GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent::value__
	int32_t ___value___1;
};
