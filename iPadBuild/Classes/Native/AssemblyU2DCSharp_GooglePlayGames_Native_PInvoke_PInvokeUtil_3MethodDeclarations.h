﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator0_1_t3776;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CToEnumerableU3Ec__Iterator0_1__ctor_m20398_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1__ctor_m20398(__this, method) (( void (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1__ctor_m20398_gshared)(__this, method)
// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m20399_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m20399(__this, method) (( Object_t * (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m20399_gshared)(__this, method)
// System.Object GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20400_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20400(__this, method) (( Object_t * (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m20400_gshared)(__this, method)
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m20401_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m20401(__this, method) (( Object_t * (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m20401_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20402_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20402(__this, method) (( Object_t* (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20402_gshared)(__this, method)
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CToEnumerableU3Ec__Iterator0_1_MoveNext_m20403_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_MoveNext_m20403(__this, method) (( bool (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_MoveNext_m20403_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_Dispose_m20404_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_Dispose_m20404(__this, method) (( void (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_Dispose_m20404_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator0`1<System.Object>::Reset()
extern "C" void U3CToEnumerableU3Ec__Iterator0_1_Reset_m20405_gshared (U3CToEnumerableU3Ec__Iterator0_1_t3776 * __this, MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator0_1_Reset_m20405(__this, method) (( void (*) (U3CToEnumerableU3Ec__Iterator0_1_t3776 *, MethodInfo*))U3CToEnumerableU3Ec__Iterator0_1_Reset_m20405_gshared)(__this, method)
