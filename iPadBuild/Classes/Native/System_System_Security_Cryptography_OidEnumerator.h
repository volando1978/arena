﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.OidCollection
struct OidCollection_t2039;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.OidEnumerator
struct  OidEnumerator_t2054  : public Object_t
{
	// System.Security.Cryptography.OidCollection System.Security.Cryptography.OidEnumerator::_collection
	OidCollection_t2039 * ____collection_0;
	// System.Int32 System.Security.Cryptography.OidEnumerator::_position
	int32_t ____position_1;
};
