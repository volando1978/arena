﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40
struct U3CParticipantLeftU3Ec__AnonStorey40_t572;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::.ctor()
extern "C" void U3CParticipantLeftU3Ec__AnonStorey40__ctor_m2362 (U3CParticipantLeftU3Ec__AnonStorey40_t572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<ParticipantLeft>c__AnonStorey40::<>m__36()
extern "C" void U3CParticipantLeftU3Ec__AnonStorey40_U3CU3Em__36_m2363 (U3CParticipantLeftU3Ec__AnonStorey40_t572 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
