﻿#pragma once
#include <stdint.h>
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t617;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Byte[]
struct ByteU5BU5D_t350;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43
struct U3CToOnGameThreadU3Ec__AnonStorey43_t619;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44
struct  U3CToOnGameThreadU3Ec__AnonStorey44_t620  : public Object_t
{
	// GooglePlayGames.BasicApi.SavedGame.IConflictResolver GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::resolver
	Object_t * ___resolver_0;
	// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::original
	Object_t * ___original_1;
	// System.Byte[] GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::originalData
	ByteU5BU5D_t350* ___originalData_2;
	// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::unmerged
	Object_t * ___unmerged_3;
	// System.Byte[] GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::unmergedData
	ByteU5BU5D_t350* ___unmergedData_4;
	// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43 GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43/<ToOnGameThread>c__AnonStorey44::<>f__ref$67
	U3CToOnGameThreadU3Ec__AnonStorey43_t619 * ___U3CU3Ef__refU2467_5;
};
