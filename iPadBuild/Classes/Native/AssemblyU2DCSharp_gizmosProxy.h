﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// gizmosProxy
struct  gizmosProxy_t804  : public MonoBehaviour_t26
{
	// UnityEngine.GameObject gizmosProxy::currentNaveProxy
	GameObject_t144 * ___currentNaveProxy_2;
	// UnityEngine.GameObject gizmosProxy::line
	GameObject_t144 * ___line_3;
};
