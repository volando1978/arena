﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t2692;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
extern "C" void HMACSHA384__ctor_m12985 (HMACSHA384_t2692 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
extern "C" void HMACSHA384__ctor_m12986 (HMACSHA384_t2692 * __this, ByteU5BU5D_t350* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
extern "C" void HMACSHA384__cctor_m12987 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m12988 (HMACSHA384_t2692 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
