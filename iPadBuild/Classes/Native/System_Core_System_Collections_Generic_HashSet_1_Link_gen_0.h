﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.HashSet`1/Link<System.String>
struct  Link_t3577 
{
	// System.Int32 System.Collections.Generic.HashSet`1/Link<System.String>::HashCode
	int32_t ___HashCode_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Link<System.String>::Next
	int32_t ___Next_1;
};
