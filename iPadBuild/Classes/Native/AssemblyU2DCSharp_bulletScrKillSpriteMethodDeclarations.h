﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// bulletScrKillSprite
struct bulletScrKillSprite_t765;

// System.Void bulletScrKillSprite::.ctor()
extern "C" void bulletScrKillSprite__ctor_m3314 (bulletScrKillSprite_t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void bulletScrKillSprite::OnBecameInvisible()
extern "C" void bulletScrKillSprite_OnBecameInvisible_m3315 (bulletScrKillSprite_t765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
