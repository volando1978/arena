﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>
struct Action_1_t100;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_49MethodDeclarations.h"
#define Action_1__ctor_m967(__this, ___object, ___method, method) (( void (*) (Action_1_t100 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m15511_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>::Invoke(T)
#define Action_1_Invoke_m17308(__this, ___obj, method) (( void (*) (Action_1_t100 *, List_1_t177 *, MethodInfo*))Action_1_Invoke_m15512_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m17309(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t100 *, List_1_t177 *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m15513_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Collections.Generic.List`1<Soomla.Store.MarketItem>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m17310(__this, ___result, method) (( void (*) (Action_1_t100 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m15514_gshared)(__this, ___result, method)
