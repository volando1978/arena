﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualCurrencyStorageIOS
struct VirtualCurrencyStorageIOS_t85;
// System.String
struct String_t;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;

// System.Void Soomla.Store.VirtualCurrencyStorageIOS::.ctor()
extern "C" void VirtualCurrencyStorageIOS__ctor_m344 (VirtualCurrencyStorageIOS_t85 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::vcStorage_GetBalance(System.String,System.Int32&)
extern "C" int32_t VirtualCurrencyStorageIOS_vcStorage_GetBalance_m345 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::vcStorage_SetBalance(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualCurrencyStorageIOS_vcStorage_SetBalance_m346 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___balance, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::vcStorage_Add(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualCurrencyStorageIOS_vcStorage_Add_m347 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::vcStorage_Remove(System.String,System.Int32,System.Boolean,System.Int32&)
extern "C" int32_t VirtualCurrencyStorageIOS_vcStorage_Remove_m348 (Object_t * __this /* static, unused */, String_t* ___itemId, int32_t ___amount, bool ___notify, int32_t* ___outBalance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::_getBalance(Soomla.Store.VirtualItem)
extern "C" int32_t VirtualCurrencyStorageIOS__getBalance_m349 (VirtualCurrencyStorageIOS_t85 * __this, VirtualItem_t125 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::_setBalance(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorageIOS__setBalance_m350 (VirtualCurrencyStorageIOS_t85 * __this, VirtualItem_t125 * ___item, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::_add(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorageIOS__add_m351 (VirtualCurrencyStorageIOS_t85 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualCurrencyStorageIOS::_remove(Soomla.Store.VirtualItem,System.Int32,System.Boolean)
extern "C" int32_t VirtualCurrencyStorageIOS__remove_m352 (VirtualCurrencyStorageIOS_t85 * __this, VirtualItem_t125 * ___item, int32_t ___amount, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
