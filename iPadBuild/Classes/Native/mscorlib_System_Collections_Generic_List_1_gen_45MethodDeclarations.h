﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1603;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t4513;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t4514;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t4515;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t4100;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1669;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t4104;
// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t4105;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t4108;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_49.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m24741_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1__ctor_m24741(__this, method) (( void (*) (List_1_t1603 *, MethodInfo*))List_1__ctor_m24741_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m24742_gshared (List_1_t1603 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m24742(__this, ___collection, method) (( void (*) (List_1_t1603 *, Object_t*, MethodInfo*))List_1__ctor_m24742_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m7514_gshared (List_1_t1603 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m7514(__this, ___capacity, method) (( void (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1__ctor_m7514_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m24743_gshared (Object_t * __this /* static, unused */, MethodInfo* method);
#define List_1__cctor_m24743(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m24743_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24744_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24744(__this, method) (( Object_t* (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m24745_gshared (List_1_t1603 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m24745(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1603 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m24745_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m24746_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24746(__this, method) (( Object_t * (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m24746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m24747_gshared (List_1_t1603 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m24747(__this, ___item, method) (( int32_t (*) (List_1_t1603 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m24747_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m24748_gshared (List_1_t1603 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m24748(__this, ___item, method) (( bool (*) (List_1_t1603 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m24748_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m24749_gshared (List_1_t1603 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m24749(__this, ___item, method) (( int32_t (*) (List_1_t1603 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m24749_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m24750_gshared (List_1_t1603 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m24750(__this, ___index, ___item, method) (( void (*) (List_1_t1603 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m24750_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m24751_gshared (List_1_t1603 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m24751(__this, ___item, method) (( void (*) (List_1_t1603 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m24751_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24752_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24752(__this, method) (( bool (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m24753_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24753(__this, method) (( bool (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m24753_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m24754_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m24754(__this, method) (( Object_t * (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m24754_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m24755_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m24755(__this, method) (( bool (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m24755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m24756_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m24756(__this, method) (( bool (*) (List_1_t1603 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m24756_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m24757_gshared (List_1_t1603 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m24757(__this, ___index, method) (( Object_t * (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m24757_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m24758_gshared (List_1_t1603 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m24758(__this, ___index, ___value, method) (( void (*) (List_1_t1603 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m24758_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m24759_gshared (List_1_t1603 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define List_1_Add_m24759(__this, ___item, method) (( void (*) (List_1_t1603 *, UICharInfo_t1400 , MethodInfo*))List_1_Add_m24759_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m24760_gshared (List_1_t1603 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m24760(__this, ___newCount, method) (( void (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m24760_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m24761_gshared (List_1_t1603 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m24761(__this, ___collection, method) (( void (*) (List_1_t1603 *, Object_t*, MethodInfo*))List_1_AddCollection_m24761_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m24762_gshared (List_1_t1603 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m24762(__this, ___enumerable, method) (( void (*) (List_1_t1603 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m24762_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m24763_gshared (List_1_t1603 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m24763(__this, ___collection, method) (( void (*) (List_1_t1603 *, Object_t*, MethodInfo*))List_1_AddRange_m24763_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t4100 * List_1_AsReadOnly_m24764_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m24764(__this, method) (( ReadOnlyCollection_1_t4100 * (*) (List_1_t1603 *, MethodInfo*))List_1_AsReadOnly_m24764_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m24765_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_Clear_m24765(__this, method) (( void (*) (List_1_t1603 *, MethodInfo*))List_1_Clear_m24765_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m24766_gshared (List_1_t1603 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define List_1_Contains_m24766(__this, ___item, method) (( bool (*) (List_1_t1603 *, UICharInfo_t1400 , MethodInfo*))List_1_Contains_m24766_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m24767_gshared (List_1_t1603 * __this, UICharInfoU5BU5D_t1669* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m24767(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1603 *, UICharInfoU5BU5D_t1669*, int32_t, MethodInfo*))List_1_CopyTo_m24767_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t1400  List_1_Find_m24768_gshared (List_1_t1603 * __this, Predicate_1_t4104 * ___match, MethodInfo* method);
#define List_1_Find_m24768(__this, ___match, method) (( UICharInfo_t1400  (*) (List_1_t1603 *, Predicate_1_t4104 *, MethodInfo*))List_1_Find_m24768_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m24769_gshared (Object_t * __this /* static, unused */, Predicate_1_t4104 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m24769(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t4104 *, MethodInfo*))List_1_CheckMatch_m24769_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m24770_gshared (List_1_t1603 * __this, Predicate_1_t4104 * ___match, MethodInfo* method);
#define List_1_FindIndex_m24770(__this, ___match, method) (( int32_t (*) (List_1_t1603 *, Predicate_1_t4104 *, MethodInfo*))List_1_FindIndex_m24770_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m24771_gshared (List_1_t1603 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t4104 * ___match, MethodInfo* method);
#define List_1_GetIndex_m24771(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1603 *, int32_t, int32_t, Predicate_1_t4104 *, MethodInfo*))List_1_GetIndex_m24771_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m24772_gshared (List_1_t1603 * __this, Action_1_t4105 * ___action, MethodInfo* method);
#define List_1_ForEach_m24772(__this, ___action, method) (( void (*) (List_1_t1603 *, Action_1_t4105 *, MethodInfo*))List_1_ForEach_m24772_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t4099  List_1_GetEnumerator_m24773_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_GetEnumerator_m24773(__this, method) (( Enumerator_t4099  (*) (List_1_t1603 *, MethodInfo*))List_1_GetEnumerator_m24773_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m24774_gshared (List_1_t1603 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define List_1_IndexOf_m24774(__this, ___item, method) (( int32_t (*) (List_1_t1603 *, UICharInfo_t1400 , MethodInfo*))List_1_IndexOf_m24774_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m24775_gshared (List_1_t1603 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m24775(__this, ___start, ___delta, method) (( void (*) (List_1_t1603 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m24775_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m24776_gshared (List_1_t1603 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m24776(__this, ___index, method) (( void (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_CheckIndex_m24776_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m24777_gshared (List_1_t1603 * __this, int32_t ___index, UICharInfo_t1400  ___item, MethodInfo* method);
#define List_1_Insert_m24777(__this, ___index, ___item, method) (( void (*) (List_1_t1603 *, int32_t, UICharInfo_t1400 , MethodInfo*))List_1_Insert_m24777_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m24778_gshared (List_1_t1603 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m24778(__this, ___collection, method) (( void (*) (List_1_t1603 *, Object_t*, MethodInfo*))List_1_CheckCollection_m24778_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m24779_gshared (List_1_t1603 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define List_1_Remove_m24779(__this, ___item, method) (( bool (*) (List_1_t1603 *, UICharInfo_t1400 , MethodInfo*))List_1_Remove_m24779_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m24780_gshared (List_1_t1603 * __this, Predicate_1_t4104 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m24780(__this, ___match, method) (( int32_t (*) (List_1_t1603 *, Predicate_1_t4104 *, MethodInfo*))List_1_RemoveAll_m24780_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m24781_gshared (List_1_t1603 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m24781(__this, ___index, method) (( void (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_RemoveAt_m24781_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m24782_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_Reverse_m24782(__this, method) (( void (*) (List_1_t1603 *, MethodInfo*))List_1_Reverse_m24782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m24783_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_Sort_m24783(__this, method) (( void (*) (List_1_t1603 *, MethodInfo*))List_1_Sort_m24783_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m24784_gshared (List_1_t1603 * __this, Comparison_1_t4108 * ___comparison, MethodInfo* method);
#define List_1_Sort_m24784(__this, ___comparison, method) (( void (*) (List_1_t1603 *, Comparison_1_t4108 *, MethodInfo*))List_1_Sort_m24784_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t1669* List_1_ToArray_m24785_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_ToArray_m24785(__this, method) (( UICharInfoU5BU5D_t1669* (*) (List_1_t1603 *, MethodInfo*))List_1_ToArray_m24785_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m24786_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_TrimExcess_m24786(__this, method) (( void (*) (List_1_t1603 *, MethodInfo*))List_1_TrimExcess_m24786_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m24787_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_get_Capacity_m24787(__this, method) (( int32_t (*) (List_1_t1603 *, MethodInfo*))List_1_get_Capacity_m24787_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m24788_gshared (List_1_t1603 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m24788(__this, ___value, method) (( void (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_set_Capacity_m24788_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m24789_gshared (List_1_t1603 * __this, MethodInfo* method);
#define List_1_get_Count_m24789(__this, method) (( int32_t (*) (List_1_t1603 *, MethodInfo*))List_1_get_Count_m24789_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t1400  List_1_get_Item_m24790_gshared (List_1_t1603 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m24790(__this, ___index, method) (( UICharInfo_t1400  (*) (List_1_t1603 *, int32_t, MethodInfo*))List_1_get_Item_m24790_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m24791_gshared (List_1_t1603 * __this, int32_t ___index, UICharInfo_t1400  ___value, MethodInfo* method);
#define List_1_set_Item_m24791(__this, ___index, ___value, method) (( void (*) (List_1_t1603 *, int32_t, UICharInfo_t1400 , MethodInfo*))List_1_set_Item_m24791_gshared)(__this, ___index, ___value, method)
