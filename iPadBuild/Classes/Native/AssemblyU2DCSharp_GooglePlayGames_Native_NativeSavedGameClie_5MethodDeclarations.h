﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45
struct U3CInternalManualOpenU3Ec__AnonStorey45_t622;
// GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse
struct OpenResponse_t701;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::.ctor()
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey45__ctor_m2514 (U3CInternalManualOpenU3Ec__AnonStorey45_t622 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<InternalManualOpen>c__AnonStorey45::<>m__4A(GooglePlayGames.Native.PInvoke.SnapshotManager/OpenResponse)
extern "C" void U3CInternalManualOpenU3Ec__AnonStorey45_U3CU3Em__4A_m2515 (U3CInternalManualOpenU3Ec__AnonStorey45_t622 * __this, OpenResponse_t701 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
