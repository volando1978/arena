﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
struct  NeutralResourcesLanguageAttribute_t1814  : public Attribute_t1546
{
	// System.String System.Resources.NeutralResourcesLanguageAttribute::culture
	String_t* ___culture_0;
};
