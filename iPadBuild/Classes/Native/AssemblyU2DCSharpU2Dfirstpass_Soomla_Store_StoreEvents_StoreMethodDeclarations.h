﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEvents/StoreEventPusher
struct StoreEventPusher_t65;
// System.String
struct String_t;
// Soomla.Store.VirtualCurrency
struct VirtualCurrency_t77;
// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// Soomla.Store.EquippableVG
struct EquippableVG_t129;
// Soomla.Store.UpgradeVG
struct UpgradeVG_t133;
// Soomla.Store.PurchasableVirtualItem
struct PurchasableVirtualItem_t124;

// System.Void Soomla.Store.StoreEvents/StoreEventPusher::.ctor()
extern "C" void StoreEventPusher__ctor_m395 (StoreEventPusher_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventSoomlaStoreInitialized()
extern "C" void StoreEventPusher_PushEventSoomlaStoreInitialized_m396 (StoreEventPusher_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventUnexpectedStoreError(System.String)
extern "C" void StoreEventPusher_PushEventUnexpectedStoreError_m397 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnCurrencyBalanceChanged(Soomla.Store.VirtualCurrency,System.Int32,System.Int32)
extern "C" void StoreEventPusher_PushEventOnCurrencyBalanceChanged_m398 (StoreEventPusher_t65 * __this, VirtualCurrency_t77 * ___currency, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnGoodBalanceChanged(Soomla.Store.VirtualGood,System.Int32,System.Int32)
extern "C" void StoreEventPusher_PushEventOnGoodBalanceChanged_m399 (StoreEventPusher_t65 * __this, VirtualGood_t79 * ___good, int32_t ___balance, int32_t ___amountAdded, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnGoodEquipped(Soomla.Store.EquippableVG)
extern "C" void StoreEventPusher_PushEventOnGoodEquipped_m400 (StoreEventPusher_t65 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnGoodUnequipped(Soomla.Store.EquippableVG)
extern "C" void StoreEventPusher_PushEventOnGoodUnequipped_m401 (StoreEventPusher_t65 * __this, EquippableVG_t129 * ___good, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnGoodUpgrade(Soomla.Store.VirtualGood,Soomla.Store.UpgradeVG)
extern "C" void StoreEventPusher_PushEventOnGoodUpgrade_m402 (StoreEventPusher_t65 * __this, VirtualGood_t79 * ___good, UpgradeVG_t133 * ___upgrade, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnItemPurchased(Soomla.Store.PurchasableVirtualItem,System.String)
extern "C" void StoreEventPusher_PushEventOnItemPurchased_m403 (StoreEventPusher_t65 * __this, PurchasableVirtualItem_t124 * ___item, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::PushEventOnItemPurchaseStarted(Soomla.Store.PurchasableVirtualItem)
extern "C" void StoreEventPusher_PushEventOnItemPurchaseStarted_m404 (StoreEventPusher_t65 * __this, PurchasableVirtualItem_t124 * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventSoomlaStoreInitialized(System.String)
extern "C" void StoreEventPusher__pushEventSoomlaStoreInitialized_m405 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventUnexpectedStoreError(System.String)
extern "C" void StoreEventPusher__pushEventUnexpectedStoreError_m406 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventCurrencyBalanceChanged(System.String)
extern "C" void StoreEventPusher__pushEventCurrencyBalanceChanged_m407 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventGoodBalanceChanged(System.String)
extern "C" void StoreEventPusher__pushEventGoodBalanceChanged_m408 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventGoodEquipped(System.String)
extern "C" void StoreEventPusher__pushEventGoodEquipped_m409 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventGoodUnequipped(System.String)
extern "C" void StoreEventPusher__pushEventGoodUnequipped_m410 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventGoodUpgrade(System.String)
extern "C" void StoreEventPusher__pushEventGoodUpgrade_m411 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventItemPurchased(System.String)
extern "C" void StoreEventPusher__pushEventItemPurchased_m412 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/StoreEventPusher::_pushEventItemPurchaseStarted(System.String)
extern "C" void StoreEventPusher__pushEventItemPurchaseStarted_m413 (StoreEventPusher_t65 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
