﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t41;
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t42;
// System.Collections.Generic.List`1<System.String>
struct List_1_t43;
// System.Diagnostics.Stopwatch
struct Stopwatch_t44;
// NullCheckable
#include "AssemblyU2DCSharpU2Dfirstpass_NullCheckable.h"
// JSONObject/Type
#include "AssemblyU2DCSharpU2Dfirstpass_JSONObject_Type.h"
// JSONObject
struct  JSONObject_t30  : public NullCheckable_t45
{
	// JSONObject/Type JSONObject::type
	int32_t ___type_6;
	// System.Collections.Generic.List`1<JSONObject> JSONObject::list
	List_1_t42 * ___list_7;
	// System.Collections.Generic.List`1<System.String> JSONObject::keys
	List_1_t43 * ___keys_8;
	// System.String JSONObject::str
	String_t* ___str_9;
	// System.Single JSONObject::n
	float ___n_10;
	// System.Boolean JSONObject::b
	bool ___b_11;
};
struct JSONObject_t30_StaticFields{
	// System.Char[] JSONObject::WHITESPACE
	CharU5BU5D_t41* ___WHITESPACE_5;
	// System.Diagnostics.Stopwatch JSONObject::printWatch
	Stopwatch_t44 * ___printWatch_12;
};
