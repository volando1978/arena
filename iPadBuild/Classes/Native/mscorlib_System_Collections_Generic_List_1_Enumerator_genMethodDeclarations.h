﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<JSONObject>
struct Enumerator_t204;
// System.Object
struct Object_t;
// JSONObject
struct JSONObject_t30;
// System.Collections.Generic.List`1<JSONObject>
struct List_1_t42;

// System.Void System.Collections.Generic.List`1/Enumerator<JSONObject>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13MethodDeclarations.h"
#define Enumerator__ctor_m16430(__this, ___l, method) (( void (*) (Enumerator_t204 *, List_1_t42 *, MethodInfo*))Enumerator__ctor_m15422_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<JSONObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16431(__this, method) (( Object_t * (*) (Enumerator_t204 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15423_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JSONObject>::Dispose()
#define Enumerator_Dispose_m16432(__this, method) (( void (*) (Enumerator_t204 *, MethodInfo*))Enumerator_Dispose_m15424_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JSONObject>::VerifyState()
#define Enumerator_VerifyState_m16433(__this, method) (( void (*) (Enumerator_t204 *, MethodInfo*))Enumerator_VerifyState_m15425_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<JSONObject>::MoveNext()
#define Enumerator_MoveNext_m919(__this, method) (( bool (*) (Enumerator_t204 *, MethodInfo*))Enumerator_MoveNext_m15426_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<JSONObject>::get_Current()
#define Enumerator_get_Current_m918(__this, method) (( JSONObject_t30 * (*) (Enumerator_t204 *, MethodInfo*))Enumerator_get_Current_m15427_gshared)(__this, method)
