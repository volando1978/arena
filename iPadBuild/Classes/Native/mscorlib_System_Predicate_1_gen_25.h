﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<GooglePlayGames.Native.NativeEvent>
struct  Predicate_1_t3731  : public MulticastDelegate_t22
{
};
