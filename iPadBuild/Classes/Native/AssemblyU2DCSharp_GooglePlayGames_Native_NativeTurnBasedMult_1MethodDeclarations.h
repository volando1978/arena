﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F
struct U3CAcceptFromInboxU3Ec__AnonStorey4F_t635;
// GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse
struct MatchInboxUIResponse_t706;

// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F::.ctor()
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey4F__ctor_m2546 (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeTurnBasedMultiplayerClient/<AcceptFromInbox>c__AnonStorey4F::<>m__59(GooglePlayGames.Native.PInvoke.TurnBasedManager/MatchInboxUIResponse)
extern "C" void U3CAcceptFromInboxU3Ec__AnonStorey4F_U3CU3Em__59_m2547 (U3CAcceptFromInboxU3Ec__AnonStorey4F_t635 * __this, MatchInboxUIResponse_t706 * ___callbackResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
