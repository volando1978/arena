﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3
struct U3CRunLaterPrivU3Ec__Iterator3_t89;
// System.Object
struct Object_t;

// System.Void Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::.ctor()
extern "C" void U3CRunLaterPrivU3Ec__Iterator3__ctor_m422 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CRunLaterPrivU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m423 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CRunLaterPrivU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m424 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::MoveNext()
extern "C" bool U3CRunLaterPrivU3Ec__Iterator3_MoveNext_m425 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::Dispose()
extern "C" void U3CRunLaterPrivU3Ec__Iterator3_Dispose_m426 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreEvents/<RunLaterPriv>c__Iterator3::Reset()
extern "C" void U3CRunLaterPrivU3Ec__Iterator3_Reset_m427 (U3CRunLaterPrivU3Ec__Iterator3_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
