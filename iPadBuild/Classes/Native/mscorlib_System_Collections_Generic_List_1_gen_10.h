﻿#pragma once
#include <stdint.h>
// Soomla.Store.VirtualGood[]
struct VirtualGoodU5BU5D_t173;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Soomla.Store.VirtualGood>
struct  List_1_t116  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::_items
	VirtualGoodU5BU5D_t173* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::_version
	int32_t ____version_3;
};
struct List_1_t116_StaticFields{
	// T[] System.Collections.Generic.List`1<Soomla.Store.VirtualGood>::EmptyArray
	VirtualGoodU5BU5D_t173* ___EmptyArray_4;
};
