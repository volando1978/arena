﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// disparo
struct disparo_t784;
// enemyController
struct enemyController_t785;
// UnityEngine.GameObject
struct GameObject_t144;

// System.Void disparo::.ctor()
extern "C" void disparo__ctor_m3372 (disparo_t784 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void disparo::dispara(enemyController,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject)
extern "C" void disparo_dispara_m3373 (disparo_t784 * __this, enemyController_t785 * ____enemyController, GameObject_t144 * ___bullet, GameObject_t144 * ___bulletThin, GameObject_t144 * ___cirController, GameObject_t144 * ___bulletLaser, GameObject_t144 * ___raycastObj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean disparo::isBullets()
extern "C" bool disparo_isBullets_m3374 (disparo_t784 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
