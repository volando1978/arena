﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>
struct Action_2_t556;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29
struct  U3CAcceptU3Ec__AnonStorey29_t557  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest> GooglePlayGames.Native.NativeQuestClient/<Accept>c__AnonStorey29::callback
	Action_2_t556 * ___callback_0;
};
