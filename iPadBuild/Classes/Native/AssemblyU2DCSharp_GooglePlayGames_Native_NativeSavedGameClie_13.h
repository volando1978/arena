﻿#pragma once
#include <stdint.h>
// System.Action`2<System.Object,System.Object>
struct Action_2_t3481;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Object,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3769  : public Object_t
{
	// System.Action`2<T1,T2> GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Object,System.Object>::toConvert
	Action_2_t3481 * ___toConvert_0;
};
