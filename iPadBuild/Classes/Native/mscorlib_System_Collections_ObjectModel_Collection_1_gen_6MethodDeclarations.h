﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t4101;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1669;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t4514;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1401;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m24828_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1__ctor_m24828(__this, method) (( void (*) (Collection_1_t4101 *, MethodInfo*))Collection_1__ctor_m24828_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24829_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24829(__this, method) (( bool (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24829_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m24830_gshared (Collection_1_t4101 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m24830(__this, ___array, ___index, method) (( void (*) (Collection_1_t4101 *, Array_t *, int32_t, MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m24830_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m24831_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m24831(__this, method) (( Object_t * (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m24831_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m24832_gshared (Collection_1_t4101 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m24832(__this, ___value, method) (( int32_t (*) (Collection_1_t4101 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Add_m24832_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m24833_gshared (Collection_1_t4101 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m24833(__this, ___value, method) (( bool (*) (Collection_1_t4101 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Contains_m24833_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m24834_gshared (Collection_1_t4101 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m24834(__this, ___value, method) (( int32_t (*) (Collection_1_t4101 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m24834_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m24835_gshared (Collection_1_t4101 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m24835(__this, ___index, ___value, method) (( void (*) (Collection_1_t4101 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Insert_m24835_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m24836_gshared (Collection_1_t4101 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m24836(__this, ___value, method) (( void (*) (Collection_1_t4101 *, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_Remove_m24836_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m24837_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m24837(__this, method) (( bool (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m24837_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m24838_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m24838(__this, method) (( Object_t * (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m24838_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m24839_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m24839(__this, method) (( bool (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m24839_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m24840_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m24840(__this, method) (( bool (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m24840_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m24841_gshared (Collection_1_t4101 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m24841(__this, ___index, method) (( Object_t * (*) (Collection_1_t4101 *, int32_t, MethodInfo*))Collection_1_System_Collections_IList_get_Item_m24841_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m24842_gshared (Collection_1_t4101 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m24842(__this, ___index, ___value, method) (( void (*) (Collection_1_t4101 *, int32_t, Object_t *, MethodInfo*))Collection_1_System_Collections_IList_set_Item_m24842_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m24843_gshared (Collection_1_t4101 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_Add_m24843(__this, ___item, method) (( void (*) (Collection_1_t4101 *, UICharInfo_t1400 , MethodInfo*))Collection_1_Add_m24843_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m24844_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_Clear_m24844(__this, method) (( void (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_Clear_m24844_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m24845_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m24845(__this, method) (( void (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_ClearItems_m24845_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m24846_gshared (Collection_1_t4101 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_Contains_m24846(__this, ___item, method) (( bool (*) (Collection_1_t4101 *, UICharInfo_t1400 , MethodInfo*))Collection_1_Contains_m24846_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m24847_gshared (Collection_1_t4101 * __this, UICharInfoU5BU5D_t1669* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m24847(__this, ___array, ___index, method) (( void (*) (Collection_1_t4101 *, UICharInfoU5BU5D_t1669*, int32_t, MethodInfo*))Collection_1_CopyTo_m24847_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m24848_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m24848(__this, method) (( Object_t* (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_GetEnumerator_m24848_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m24849_gshared (Collection_1_t4101 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_IndexOf_m24849(__this, ___item, method) (( int32_t (*) (Collection_1_t4101 *, UICharInfo_t1400 , MethodInfo*))Collection_1_IndexOf_m24849_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m24850_gshared (Collection_1_t4101 * __this, int32_t ___index, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_Insert_m24850(__this, ___index, ___item, method) (( void (*) (Collection_1_t4101 *, int32_t, UICharInfo_t1400 , MethodInfo*))Collection_1_Insert_m24850_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m24851_gshared (Collection_1_t4101 * __this, int32_t ___index, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_InsertItem_m24851(__this, ___index, ___item, method) (( void (*) (Collection_1_t4101 *, int32_t, UICharInfo_t1400 , MethodInfo*))Collection_1_InsertItem_m24851_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m24852_gshared (Collection_1_t4101 * __this, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_Remove_m24852(__this, ___item, method) (( bool (*) (Collection_1_t4101 *, UICharInfo_t1400 , MethodInfo*))Collection_1_Remove_m24852_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m24853_gshared (Collection_1_t4101 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m24853(__this, ___index, method) (( void (*) (Collection_1_t4101 *, int32_t, MethodInfo*))Collection_1_RemoveAt_m24853_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m24854_gshared (Collection_1_t4101 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m24854(__this, ___index, method) (( void (*) (Collection_1_t4101 *, int32_t, MethodInfo*))Collection_1_RemoveItem_m24854_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m24855_gshared (Collection_1_t4101 * __this, MethodInfo* method);
#define Collection_1_get_Count_m24855(__this, method) (( int32_t (*) (Collection_1_t4101 *, MethodInfo*))Collection_1_get_Count_m24855_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t1400  Collection_1_get_Item_m24856_gshared (Collection_1_t4101 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m24856(__this, ___index, method) (( UICharInfo_t1400  (*) (Collection_1_t4101 *, int32_t, MethodInfo*))Collection_1_get_Item_m24856_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m24857_gshared (Collection_1_t4101 * __this, int32_t ___index, UICharInfo_t1400  ___value, MethodInfo* method);
#define Collection_1_set_Item_m24857(__this, ___index, ___value, method) (( void (*) (Collection_1_t4101 *, int32_t, UICharInfo_t1400 , MethodInfo*))Collection_1_set_Item_m24857_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m24858_gshared (Collection_1_t4101 * __this, int32_t ___index, UICharInfo_t1400  ___item, MethodInfo* method);
#define Collection_1_SetItem_m24858(__this, ___index, ___item, method) (( void (*) (Collection_1_t4101 *, int32_t, UICharInfo_t1400 , MethodInfo*))Collection_1_SetItem_m24858_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m24859_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m24859(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_IsValidItem_m24859_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t1400  Collection_1_ConvertItem_m24860_gshared (Object_t * __this /* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m24860(__this /* static, unused */, ___item, method) (( UICharInfo_t1400  (*) (Object_t * /* static, unused */, Object_t *, MethodInfo*))Collection_1_ConvertItem_m24860_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m24861_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m24861(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_CheckWritable_m24861_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m24862_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m24862(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsSynchronized_m24862_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m24863_gshared (Object_t * __this /* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m24863(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, MethodInfo*))Collection_1_IsFixedSize_m24863_gshared)(__this /* static, unused */, ___list, method)
