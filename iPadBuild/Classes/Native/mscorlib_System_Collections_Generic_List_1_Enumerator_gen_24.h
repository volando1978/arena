﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<GooglePlayGames.Native.NativeEvent>
struct List_1_t868;
// GooglePlayGames.Native.NativeEvent
struct NativeEvent_t674;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>
struct  Enumerator_t3733 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::l
	List_1_t868 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<GooglePlayGames.Native.NativeEvent>::current
	NativeEvent_t674 * ___current_3;
};
