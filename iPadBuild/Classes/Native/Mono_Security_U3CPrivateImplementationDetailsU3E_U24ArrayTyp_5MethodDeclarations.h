﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t2303;
struct U24ArrayTypeU2412_t2303_marshaled;

void U24ArrayTypeU2412_t2303_marshal(const U24ArrayTypeU2412_t2303& unmarshaled, U24ArrayTypeU2412_t2303_marshaled& marshaled);
void U24ArrayTypeU2412_t2303_marshal_back(const U24ArrayTypeU2412_t2303_marshaled& marshaled, U24ArrayTypeU2412_t2303& unmarshaled);
void U24ArrayTypeU2412_t2303_marshal_cleanup(U24ArrayTypeU2412_t2303_marshaled& marshaled);
