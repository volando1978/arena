﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t1513;
struct Gradient_t1513_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m6486 (Gradient_t1513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m6487 (Gradient_t1513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m6488 (Gradient_t1513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m6489 (Gradient_t1513 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t1513_marshal(const Gradient_t1513& unmarshaled, Gradient_t1513_marshaled& marshaled);
void Gradient_t1513_marshal_back(const Gradient_t1513_marshaled& marshaled, Gradient_t1513& unmarshaled);
void Gradient_t1513_marshal_cleanup(Gradient_t1513_marshaled& marshaled);
