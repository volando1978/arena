﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`1<System.Object>
struct UnityDictionary_1_t3458;

// System.Void UnityEngine.UnityDictionary`1<System.Object>::.ctor()
extern "C" void UnityDictionary_1__ctor_m15867_gshared (UnityDictionary_1_t3458 * __this, MethodInfo* method);
#define UnityDictionary_1__ctor_m15867(__this, method) (( void (*) (UnityDictionary_1_t3458 *, MethodInfo*))UnityDictionary_1__ctor_m15867_gshared)(__this, method)
