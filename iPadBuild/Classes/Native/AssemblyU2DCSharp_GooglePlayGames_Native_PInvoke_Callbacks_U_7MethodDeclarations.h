﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>
struct U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780;
// System.Object
struct Object_t;

// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>::.ctor()
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20416_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780 * __this, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20416(__this, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780 *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1__ctor_m20416_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.Callbacks/<AsOnGameThreadCallback>c__AnonStorey5E`1<System.Object>::<>m__75(T)
extern "C" void U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20417_gshared (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780 * __this, Object_t * ___result, MethodInfo* method);
#define U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20417(__this, ___result, method) (( void (*) (U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_t3780 *, Object_t *, MethodInfo*))U3CAsOnGameThreadCallbackU3Ec__AnonStorey5E_1_U3CU3Em__75_m20417_gshared)(__this, ___result, method)
