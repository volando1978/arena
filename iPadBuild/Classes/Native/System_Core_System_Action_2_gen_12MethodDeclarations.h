﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>
struct Action_2_t627;
// System.Object
struct Object_t;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.BasicApi.SavedGame.SelectUIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGame_SelectU.h"

// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::.ctor(System.Object,System.IntPtr)
// System.Action`2<System.Int32,System.Object>
#include "System_Core_System_Action_2_gen_18MethodDeclarations.h"
#define Action_2__ctor_m19367(__this, ___object, ___method, method) (( void (*) (Action_2_t627 *, Object_t *, IntPtr_t, MethodInfo*))Action_2__ctor_m18648_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::Invoke(T1,T2)
#define Action_2_Invoke_m19368(__this, ___arg1, ___arg2, method) (( void (*) (Action_2_t627 *, int32_t, Object_t *, MethodInfo*))Action_2_Invoke_m18650_gshared)(__this, ___arg1, ___arg2, method)
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m19369(__this, ___arg1, ___arg2, ___callback, ___object, method) (( Object_t * (*) (Action_2_t627 *, int32_t, Object_t *, AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_2_BeginInvoke_m18652_gshared)(__this, ___arg1, ___arg2, ___callback, ___object, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m19370(__this, ___result, method) (( void (*) (Action_2_t627 *, Object_t *, MethodInfo*))Action_2_EndInvoke_m18654_gshared)(__this, ___result, method)
