﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;
// GooglePlayGames.Native.PInvoke.RealtimeManager
struct RealtimeManager_t564;
// GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener
struct RealTimeMultiplayerListener_t573;
// System.String
struct String_t;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State
struct State_t565;
// System.Action
struct Action_t588;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse
struct RealTimeRoomResponse_t695;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant>
struct List_1_t351;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t340;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::.ctor(GooglePlayGames.Native.PInvoke.RealtimeManager,GooglePlayGames.BasicApi.Multiplayer.RealTimeMultiplayerListener)
extern "C" void RoomSession__ctor_m2328 (RoomSession_t566 * __this, RealtimeManager_t564 * ___manager, Object_t * ___listener, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::get_MinPlayersToStart()
extern "C" uint32_t RoomSession_get_MinPlayersToStart_m2329 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::set_MinPlayersToStart(System.UInt32)
extern "C" void RoomSession_set_MinPlayersToStart_m2330 (RoomSession_t566 * __this, uint32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PInvoke.RealtimeManager GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::Manager()
extern "C" RealtimeManager_t564 * RoomSession_Manager_m2331 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::IsActive()
extern "C" bool RoomSession_IsActive_m2332 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SelfPlayerId()
extern "C" String_t* RoomSession_SelfPlayerId_m2333 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnGameThreadListener()
extern "C" OnGameThreadForwardingListener_t563 * RoomSession_OnGameThreadListener_m2334 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::EnterState(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/State)
extern "C" void RoomSession_EnterState_m2335 (RoomSession_t566 * __this, State_t565 * ___handler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::LeaveRoom()
extern "C" void RoomSession_LeaveRoom_m2336 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::ShowWaitingRoomUI()
extern "C" void RoomSession_ShowWaitingRoomUI_m2337 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::StartRoomCreation(System.String,System.Action)
extern "C" void RoomSession_StartRoomCreation_m2338 (RoomSession_t566 * __this, String_t* ___currentPlayerId, Action_t588 * ___createRoom, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnRoomStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void RoomSession_OnRoomStatusChanged_m2339 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnConnectedSetChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom)
extern "C" void RoomSession_OnConnectedSetChanged_m2340 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnParticipantStatusChanged(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" void RoomSession_OnParticipantStatusChanged_m2341 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::HandleRoomResponse(GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse)
extern "C" void RoomSession_HandleRoomResponse_m2342 (RoomSession_t566 * __this, RealTimeRoomResponse_t695 * ___response, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::OnDataReceived(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Boolean)
extern "C" void RoomSession_OnDataReceived_m2343 (RoomSession_t566 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___sender, ByteU5BU5D_t350* ___data, bool ___isReliable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessageToAll(System.Boolean,System.Byte[])
extern "C" void RoomSession_SendMessageToAll_m2344 (RoomSession_t566 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessageToAll(System.Boolean,System.Byte[],System.Int32,System.Int32)
extern "C" void RoomSession_SendMessageToAll_m2345 (RoomSession_t566 * __this, bool ___reliable, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessage(System.Boolean,System.String,System.Byte[])
extern "C" void RoomSession_SendMessage_m2346 (RoomSession_t566 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::SendMessage(System.Boolean,System.String,System.Byte[],System.Int32,System.Int32)
extern "C" void RoomSession_SendMessage_m2347 (RoomSession_t566 * __this, bool ___reliable, String_t* ___participantId, ByteU5BU5D_t350* ___data, int32_t ___offset, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Multiplayer.Participant> GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetConnectedParticipants()
extern "C" List_1_t351 * RoomSession_GetConnectedParticipants_m2348 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetSelf()
extern "C" Participant_t340 * RoomSession_GetSelf_m2349 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Multiplayer.Participant GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::GetParticipant(System.String)
extern "C" Participant_t340 * RoomSession_GetParticipant_m2350 (RoomSession_t566 * __this, String_t* ___participantId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession::IsRoomConnected()
extern "C" bool RoomSession_IsRoomConnected_m2351 (RoomSession_t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
