﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t4109;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1604;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m24941_gshared (Enumerator_t4109 * __this, List_1_t1604 * ___l, MethodInfo* method);
#define Enumerator__ctor_m24941(__this, ___l, method) (( void (*) (Enumerator_t4109 *, List_1_t1604 *, MethodInfo*))Enumerator__ctor_m24941_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m24942_gshared (Enumerator_t4109 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m24942(__this, method) (( Object_t * (*) (Enumerator_t4109 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m24942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m24943_gshared (Enumerator_t4109 * __this, MethodInfo* method);
#define Enumerator_Dispose_m24943(__this, method) (( void (*) (Enumerator_t4109 *, MethodInfo*))Enumerator_Dispose_m24943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m24944_gshared (Enumerator_t4109 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m24944(__this, method) (( void (*) (Enumerator_t4109 *, MethodInfo*))Enumerator_VerifyState_m24944_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m24945_gshared (Enumerator_t4109 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m24945(__this, method) (( bool (*) (Enumerator_t4109 *, MethodInfo*))Enumerator_MoveNext_m24945_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t1398  Enumerator_get_Current_m24946_gshared (Enumerator_t4109 * __this, MethodInfo* method);
#define Enumerator_get_Current_m24946(__this, method) (( UILineInfo_t1398  (*) (Enumerator_t4109 *, MethodInfo*))Enumerator_get_Current_m24946_gshared)(__this, method)
