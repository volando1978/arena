﻿#pragma once
#include <stdint.h>
// UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate
struct InterstitialWasLoadedDelegate_t1572;
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.iOS.ADInterstitialAd
struct  ADInterstitialAd_t331  : public Object_t
{
	// System.IntPtr UnityEngine.iOS.ADInterstitialAd::interstitialView
	IntPtr_t ___interstitialView_0;
};
struct ADInterstitialAd_t331_StaticFields{
	// System.Boolean UnityEngine.iOS.ADInterstitialAd::_AlwaysFalseDummy
	bool ____AlwaysFalseDummy_1;
	// UnityEngine.iOS.ADInterstitialAd/InterstitialWasLoadedDelegate UnityEngine.iOS.ADInterstitialAd::onInterstitialWasLoaded
	InterstitialWasLoadedDelegate_t1572 * ___onInterstitialWasLoaded_2;
};
