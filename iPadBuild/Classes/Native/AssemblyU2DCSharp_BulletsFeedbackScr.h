﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// BulletsFeedbackScr
struct  BulletsFeedbackScr_t725  : public MonoBehaviour_t26
{
	// System.Int32 BulletsFeedbackScr::time
	int32_t ___time_2;
	// UnityEngine.GUIStyle BulletsFeedbackScr::st
	GUIStyle_t724 * ___st_3;
	// System.String BulletsFeedbackScr::text
	String_t* ___text_4;
};
