﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.VirtualGood
struct VirtualGood_t79;
// System.String
struct String_t;
// Soomla.Store.PurchaseType
struct PurchaseType_t123;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.Store.VirtualGood::.ctor(System.String,System.String,System.String,Soomla.Store.PurchaseType)
extern "C" void VirtualGood__ctor_m660 (VirtualGood_t79 * __this, String_t* ___name, String_t* ___description, String_t* ___itemId, PurchaseType_t123 * ___purchaseType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.VirtualGood::.ctor(JSONObject)
extern "C" void VirtualGood__ctor_m661 (VirtualGood_t79 * __this, JSONObject_t30 * ___jsonVg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.Store.VirtualGood::toJSONObject()
extern "C" JSONObject_t30 * VirtualGood_toJSONObject_m662 (VirtualGood_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGood::ResetBalance(System.Int32,System.Boolean)
extern "C" int32_t VirtualGood_ResetBalance_m663 (VirtualGood_t79 * __this, int32_t ___balance, bool ___notify, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.VirtualGood::GetBalance()
extern "C" int32_t VirtualGood_GetBalance_m664 (VirtualGood_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
