﻿#pragma once
#include <stdint.h>
// System.Func`2<System.IntPtr,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct Func_2_t938;
// System.Action`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct Action_2_t3777;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>
struct  U3CToIntPtrU3Ec__AnonStorey5D_2_t3779  : public Object_t
{
	// System.Func`2<System.IntPtr,P> GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::conversionFunction
	Func_2_t938 * ___conversionFunction_0;
	// System.Action`2<T,P> GooglePlayGames.Native.PInvoke.Callbacks/<ToIntPtr>c__AnonStorey5D`2<System.Object,GooglePlayGames.Native.PInvoke.BaseReferenceHolder>::callback
	Action_2_t3777 * ___callback_1;
};
