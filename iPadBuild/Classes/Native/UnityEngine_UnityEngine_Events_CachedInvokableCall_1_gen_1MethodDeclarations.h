﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t1696;
// UnityEngine.Object
struct Object_t187;
struct Object_t187_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t208;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m7537(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1696 *, Object_t187 *, MethodInfo_t *, String_t*, MethodInfo*))CachedInvokableCall_1__ctor_m25297_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m25309(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1696 *, ObjectU5BU5D_t208*, MethodInfo*))CachedInvokableCall_1_Invoke_m25298_gshared)(__this, ___args, method)
