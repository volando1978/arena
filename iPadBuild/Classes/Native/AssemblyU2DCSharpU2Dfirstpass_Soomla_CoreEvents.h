﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.CoreEvents
struct CoreEvents_t23;
// System.Action`1<Soomla.Reward>
struct Action_1_t24;
// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Action_2_t25;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Soomla.CoreEvents
struct  CoreEvents_t23  : public MonoBehaviour_t26
{
};
struct CoreEvents_t23_StaticFields{
	// Soomla.CoreEvents Soomla.CoreEvents::instance
	CoreEvents_t23 * ___instance_3;
	// System.Action`1<Soomla.Reward> Soomla.CoreEvents::OnRewardGiven
	Action_1_t24 * ___OnRewardGiven_4;
	// System.Action`1<Soomla.Reward> Soomla.CoreEvents::OnRewardTaken
	Action_1_t24 * ___OnRewardTaken_5;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> Soomla.CoreEvents::OnCustomEvent
	Action_2_t25 * ___OnCustomEvent_6;
	// System.Action`1<Soomla.Reward> Soomla.CoreEvents::<>f__am$cache4
	Action_1_t24 * ___U3CU3Ef__amU24cache4_7;
	// System.Action`1<Soomla.Reward> Soomla.CoreEvents::<>f__am$cache5
	Action_1_t24 * ___U3CU3Ef__amU24cache5_8;
	// System.Action`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> Soomla.CoreEvents::<>f__am$cache6
	Action_2_t25 * ___U3CU3Ef__amU24cache6_9;
};
