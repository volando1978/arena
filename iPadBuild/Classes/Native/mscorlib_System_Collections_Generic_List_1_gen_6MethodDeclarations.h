﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Soomla.Store.MarketItem>
struct List_1_t177;
// System.Object
struct Object_t;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// System.Collections.Generic.IEnumerable`1<Soomla.Store.MarketItem>
struct IEnumerable_1_t4309;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.MarketItem>
struct IEnumerator_1_t4308;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.ICollection`1<Soomla.Store.MarketItem>
struct ICollection_1_t4310;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Soomla.Store.MarketItem>
struct ReadOnlyCollection_1_t3559;
// Soomla.Store.MarketItem[]
struct MarketItemU5BU5D_t3557;
// System.Predicate`1<Soomla.Store.MarketItem>
struct Predicate_1_t3560;
// System.Action`1<Soomla.Store.MarketItem>
struct Action_1_t3561;
// System.Comparison`1<Soomla.Store.MarketItem>
struct Comparison_1_t3563;
// System.Collections.Generic.List`1/Enumerator<Soomla.Store.MarketItem>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"

// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_1MethodDeclarations.h"
#define List_1__ctor_m973(__this, method) (( void (*) (List_1_t177 *, MethodInfo*))List_1__ctor_m1042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m17359(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, MethodInfo*))List_1__ctor_m15321_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::.ctor(System.Int32)
#define List_1__ctor_m17360(__this, ___capacity, method) (( void (*) (List_1_t177 *, int32_t, MethodInfo*))List_1__ctor_m15323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::.cctor()
#define List_1__cctor_m17361(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, MethodInfo*))List_1__cctor_m15325_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17362(__this, method) (( Object_t* (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m17363(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t177 *, Array_t *, int32_t, MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15329_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17364(__this, method) (( Object_t * (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15331_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m17365(__this, ___item, method) (( int32_t (*) (List_1_t177 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Add_m15333_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m17366(__this, ___item, method) (( bool (*) (List_1_t177 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Contains_m15335_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m17367(__this, ___item, method) (( int32_t (*) (List_1_t177 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_IndexOf_m15337_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m17368(__this, ___index, ___item, method) (( void (*) (List_1_t177 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_Insert_m15339_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m17369(__this, ___item, method) (( void (*) (List_1_t177 *, Object_t *, MethodInfo*))List_1_System_Collections_IList_Remove_m15341_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17370(__this, method) (( bool (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17371(__this, method) (( bool (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15345_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m17372(__this, method) (( Object_t * (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m17373(__this, method) (( bool (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m17374(__this, method) (( bool (*) (List_1_t177 *, MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15351_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m17375(__this, ___index, method) (( Object_t * (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_System_Collections_IList_get_Item_m15353_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m17376(__this, ___index, ___value, method) (( void (*) (List_1_t177 *, int32_t, Object_t *, MethodInfo*))List_1_System_Collections_IList_set_Item_m15355_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Add(T)
#define List_1_Add_m17377(__this, ___item, method) (( void (*) (List_1_t177 *, MarketItem_t122 *, MethodInfo*))List_1_Add_m15357_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m17378(__this, ___newCount, method) (( void (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_GrowIfNeeded_m15359_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m17379(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, MethodInfo*))List_1_AddCollection_m15361_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m17380(__this, ___enumerable, method) (( void (*) (List_1_t177 *, Object_t*, MethodInfo*))List_1_AddEnumerable_m15363_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m17381(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, MethodInfo*))List_1_AddRange_m15365_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Soomla.Store.MarketItem>::AsReadOnly()
#define List_1_AsReadOnly_m17382(__this, method) (( ReadOnlyCollection_1_t3559 * (*) (List_1_t177 *, MethodInfo*))List_1_AsReadOnly_m15367_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Clear()
#define List_1_Clear_m17383(__this, method) (( void (*) (List_1_t177 *, MethodInfo*))List_1_Clear_m15369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Contains(T)
#define List_1_Contains_m17384(__this, ___item, method) (( bool (*) (List_1_t177 *, MarketItem_t122 *, MethodInfo*))List_1_Contains_m15371_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17385(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t177 *, MarketItemU5BU5D_t3557*, int32_t, MethodInfo*))List_1_CopyTo_m15373_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m17386(__this, ___match, method) (( MarketItem_t122 * (*) (List_1_t177 *, Predicate_1_t3560 *, MethodInfo*))List_1_Find_m15375_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17387(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3560 *, MethodInfo*))List_1_CheckMatch_m15377_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m17388(__this, ___match, method) (( int32_t (*) (List_1_t177 *, Predicate_1_t3560 *, MethodInfo*))List_1_FindIndex_m15379_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17389(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t177 *, int32_t, int32_t, Predicate_1_t3560 *, MethodInfo*))List_1_GetIndex_m15381_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m17390(__this, ___action, method) (( void (*) (List_1_t177 *, Action_1_t3561 *, MethodInfo*))List_1_ForEach_m15383_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Soomla.Store.MarketItem>::GetEnumerator()
#define List_1_GetEnumerator_m17391(__this, method) (( Enumerator_t3562  (*) (List_1_t177 *, MethodInfo*))List_1_GetEnumerator_m15385_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::IndexOf(T)
#define List_1_IndexOf_m17392(__this, ___item, method) (( int32_t (*) (List_1_t177 *, MarketItem_t122 *, MethodInfo*))List_1_IndexOf_m15387_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17393(__this, ___start, ___delta, method) (( void (*) (List_1_t177 *, int32_t, int32_t, MethodInfo*))List_1_Shift_m15389_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17394(__this, ___index, method) (( void (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_CheckIndex_m15391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Insert(System.Int32,T)
#define List_1_Insert_m17395(__this, ___index, ___item, method) (( void (*) (List_1_t177 *, int32_t, MarketItem_t122 *, MethodInfo*))List_1_Insert_m15393_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17396(__this, ___collection, method) (( void (*) (List_1_t177 *, Object_t*, MethodInfo*))List_1_CheckCollection_m15395_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Remove(T)
#define List_1_Remove_m17397(__this, ___item, method) (( bool (*) (List_1_t177 *, MarketItem_t122 *, MethodInfo*))List_1_Remove_m15397_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m17398(__this, ___match, method) (( int32_t (*) (List_1_t177 *, Predicate_1_t3560 *, MethodInfo*))List_1_RemoveAll_m15399_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17399(__this, ___index, method) (( void (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_RemoveAt_m15401_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Reverse()
#define List_1_Reverse_m17400(__this, method) (( void (*) (List_1_t177 *, MethodInfo*))List_1_Reverse_m15403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Sort()
#define List_1_Sort_m17401(__this, method) (( void (*) (List_1_t177 *, MethodInfo*))List_1_Sort_m15405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17402(__this, ___comparison, method) (( void (*) (List_1_t177 *, Comparison_1_t3563 *, MethodInfo*))List_1_Sort_m15407_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Soomla.Store.MarketItem>::ToArray()
#define List_1_ToArray_m17403(__this, method) (( MarketItemU5BU5D_t3557* (*) (List_1_t177 *, MethodInfo*))List_1_ToArray_m15409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::TrimExcess()
#define List_1_TrimExcess_m17404(__this, method) (( void (*) (List_1_t177 *, MethodInfo*))List_1_TrimExcess_m15411_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::get_Capacity()
#define List_1_get_Capacity_m17405(__this, method) (( int32_t (*) (List_1_t177 *, MethodInfo*))List_1_get_Capacity_m15413_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17406(__this, ___value, method) (( void (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_set_Capacity_m15415_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Soomla.Store.MarketItem>::get_Count()
#define List_1_get_Count_m17407(__this, method) (( int32_t (*) (List_1_t177 *, MethodInfo*))List_1_get_Count_m15417_gshared)(__this, method)
// T System.Collections.Generic.List`1<Soomla.Store.MarketItem>::get_Item(System.Int32)
#define List_1_get_Item_m17408(__this, ___index, method) (( MarketItem_t122 * (*) (List_1_t177 *, int32_t, MethodInfo*))List_1_get_Item_m15419_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Soomla.Store.MarketItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m17409(__this, ___index, ___value, method) (( void (*) (List_1_t177 *, int32_t, MarketItem_t122 *, MethodInfo*))List_1_set_Item_m15421_gshared)(__this, ___index, ___value, method)
