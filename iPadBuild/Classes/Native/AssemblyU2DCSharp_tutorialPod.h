﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t144;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// tutorialPod
struct  tutorialPod_t838  : public MonoBehaviour_t26
{
	// System.Int32 tutorialPod::idPod
	int32_t ___idPod_2;
	// System.Boolean tutorialPod::touched
	bool ___touched_3;
	// System.Single tutorialPod::growF
	float ___growF_4;
	// UnityEngine.GameObject tutorialPod::gameControlObj
	GameObject_t144 * ___gameControlObj_5;
};
