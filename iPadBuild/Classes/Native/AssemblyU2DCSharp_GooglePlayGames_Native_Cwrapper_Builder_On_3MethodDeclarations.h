﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback
struct OnTurnBasedMatchEventCallback_t406;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_Types_Mult.h"

// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnTurnBasedMatchEventCallback__ctor_m1669 (OnTurnBasedMatchEventCallback_t406 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::Invoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr)
extern "C" void OnTurnBasedMatchEventCallback_Invoke_m1670 (OnTurnBasedMatchEventCallback_t406 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String
#include "mscorlib_System_String.h"
extern "C" void pinvoke_delegate_wrapper_OnTurnBasedMatchEventCallback_t406(Il2CppObject* delegate, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.Types/MultiplayerEvent,System.String,System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * OnTurnBasedMatchEventCallback_BeginInvoke_m1671 (OnTurnBasedMatchEventCallback_t406 * __this, int32_t ___arg0, String_t* ___arg1, IntPtr_t ___arg2, IntPtr_t ___arg3, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.Builder/OnTurnBasedMatchEventCallback::EndInvoke(System.IAsyncResult)
extern "C" void OnTurnBasedMatchEventCallback_EndInvoke_m1672 (OnTurnBasedMatchEventCallback_t406 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
