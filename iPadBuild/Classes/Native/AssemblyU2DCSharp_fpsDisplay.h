﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t724;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// fpsDisplay
struct  fpsDisplay_t792  : public MonoBehaviour_t26
{
	// UnityEngine.GUIStyle fpsDisplay::fpsSt
	GUIStyle_t724 * ___fpsSt_2;
};
