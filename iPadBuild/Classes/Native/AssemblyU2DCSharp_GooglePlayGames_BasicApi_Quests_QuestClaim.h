﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaim.h"
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
struct  QuestClaimMilestoneStatus_t371 
{
	// System.Int32 GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus::value__
	int32_t ___value___1;
};
