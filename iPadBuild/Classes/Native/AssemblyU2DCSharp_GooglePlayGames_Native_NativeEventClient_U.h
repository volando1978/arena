﻿#pragma once
#include <stdint.h>
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>>
struct Action_2_t544;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24
struct  U3CFetchAllEventsU3Ec__AnonStorey24_t545  : public Object_t
{
	// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Events.IEvent>> GooglePlayGames.Native.NativeEventClient/<FetchAllEvents>c__AnonStorey24::callback
	Action_2_t544 * ___callback_0;
};
