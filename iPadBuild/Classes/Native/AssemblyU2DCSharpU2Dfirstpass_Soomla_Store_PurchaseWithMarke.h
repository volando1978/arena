﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// Soomla.Store.MarketItem
struct MarketItem_t122;
// Soomla.Store.PurchaseType
#include "AssemblyU2DCSharpU2Dfirstpass_Soomla_Store_PurchaseType.h"
// Soomla.Store.PurchaseWithMarket
struct  PurchaseWithMarket_t138  : public PurchaseType_t123
{
	// Soomla.Store.MarketItem Soomla.Store.PurchaseWithMarket::MarketItem
	MarketItem_t122 * ___MarketItem_2;
};
