﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityDictionary`2<System.String,System.Object>
struct UnityDictionary_2_t3459;
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<System.String,System.Object>>
struct List_1_t3465;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t4254;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t4257;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct ICollection_1_t4267;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t4268;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t4269;
// UnityEngine.UnityKeyValuePair`2<System.String,System.Object>
struct UnityKeyValuePair_2_t3457;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"

// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::.ctor()
// UnityEngine.UnityDictionary`2<System.Object,System.Object>
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_UnityDictionary_2__0MethodDeclarations.h"
#define UnityDictionary_2__ctor_m15868(__this, method) (( void (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2__ctor_m15264_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UnityDictionary`2<System.String,System.Object>::System.Collections.IEnumerable.GetEnumerator()
#define UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15869(__this, method) (( Object_t * (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15265_gshared)(__this, method)
// System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.Object>::get_KeyValuePairs()
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::set_KeyValuePairs(System.Collections.Generic.List`1<UnityEngine.UnityKeyValuePair`2<K,V>>)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::SetKeyValuePair(K,V)
// V UnityEngine.UnityDictionary`2<System.String,System.Object>::get_Item(K)
#define UnityDictionary_2_get_Item_m15870(__this, ___key, method) (( Object_t * (*) (UnityDictionary_2_t3459 *, String_t*, MethodInfo*))UnityDictionary_2_get_Item_m15266_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::set_Item(K,V)
#define UnityDictionary_2_set_Item_m15871(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3459 *, String_t*, Object_t *, MethodInfo*))UnityDictionary_2_set_Item_m15267_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::Add(K,V)
#define UnityDictionary_2_Add_m15872(__this, ___key, ___value, method) (( void (*) (UnityDictionary_2_t3459 *, String_t*, Object_t *, MethodInfo*))UnityDictionary_2_Add_m15268_gshared)(__this, ___key, ___value, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Add_m15873(__this, ___kvp, method) (( void (*) (UnityDictionary_2_t3459 *, KeyValuePair_2_t3463 , MethodInfo*))UnityDictionary_2_Add_m15269_gshared)(__this, ___kvp, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::TryGetValue(K,V&)
#define UnityDictionary_2_TryGetValue_m15874(__this, ___key, ___value, method) (( bool (*) (UnityDictionary_2_t3459 *, String_t*, Object_t **, MethodInfo*))UnityDictionary_2_TryGetValue_m15270_gshared)(__this, ___key, ___value, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Remove_m15875(__this, ___item, method) (( bool (*) (UnityDictionary_2_t3459 *, KeyValuePair_2_t3463 , MethodInfo*))UnityDictionary_2_Remove_m15271_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::Remove(K)
#define UnityDictionary_2_Remove_m15876(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3459 *, String_t*, MethodInfo*))UnityDictionary_2_Remove_m15272_gshared)(__this, ___key, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::Clear()
#define UnityDictionary_2_Clear_m15877(__this, method) (( void (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_Clear_m15273_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::ContainsKey(K)
#define UnityDictionary_2_ContainsKey_m15878(__this, ___key, method) (( bool (*) (UnityDictionary_2_t3459 *, String_t*, MethodInfo*))UnityDictionary_2_ContainsKey_m15274_gshared)(__this, ___key, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<K,V>)
#define UnityDictionary_2_Contains_m15879(__this, ___kvp, method) (( bool (*) (UnityDictionary_2_t3459 *, KeyValuePair_2_t3463 , MethodInfo*))UnityDictionary_2_Contains_m15275_gshared)(__this, ___kvp, method)
// System.Int32 UnityEngine.UnityDictionary`2<System.String,System.Object>::get_Count()
#define UnityDictionary_2_get_Count_m15880(__this, method) (( int32_t (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_Count_m15276_gshared)(__this, method)
// System.Void UnityEngine.UnityDictionary`2<System.String,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<K,V>[],System.Int32)
#define UnityDictionary_2_CopyTo_m15881(__this, ___array, ___index, method) (( void (*) (UnityDictionary_2_t3459 *, KeyValuePair_2U5BU5D_t4268*, int32_t, MethodInfo*))UnityDictionary_2_CopyTo_m15277_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.Object>::GetEnumerator()
#define UnityDictionary_2_GetEnumerator_m15882(__this, method) (( Object_t* (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_GetEnumerator_m15278_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<K> UnityEngine.UnityDictionary`2<System.String,System.Object>::get_Keys()
#define UnityDictionary_2_get_Keys_m15883(__this, method) (( Object_t* (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_Keys_m15279_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<V> UnityEngine.UnityDictionary`2<System.String,System.Object>::get_Values()
#define UnityDictionary_2_get_Values_m15884(__this, method) (( Object_t* (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_Values_m15280_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<K,V>> UnityEngine.UnityDictionary`2<System.String,System.Object>::get_Items()
#define UnityDictionary_2_get_Items_m15885(__this, method) (( Object_t* (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_Items_m15282_gshared)(__this, method)
// V UnityEngine.UnityDictionary`2<System.String,System.Object>::get_SyncRoot()
#define UnityDictionary_2_get_SyncRoot_m15886(__this, method) (( Object_t * (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_SyncRoot_m15284_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::get_IsFixedSize()
#define UnityDictionary_2_get_IsFixedSize_m15887(__this, method) (( bool (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_IsFixedSize_m15286_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::get_IsReadOnly()
#define UnityDictionary_2_get_IsReadOnly_m15888(__this, method) (( bool (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_IsReadOnly_m15287_gshared)(__this, method)
// System.Boolean UnityEngine.UnityDictionary`2<System.String,System.Object>::get_IsSynchronized()
#define UnityDictionary_2_get_IsSynchronized_m15889(__this, method) (( bool (*) (UnityDictionary_2_t3459 *, MethodInfo*))UnityDictionary_2_get_IsSynchronized_m15289_gshared)(__this, method)
// K UnityEngine.UnityDictionary`2<System.String,System.Object>::<get_Keys>m__0(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_KeysU3Em__0_m15890(__this /* static, unused */, ___x, method) (( String_t* (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityDictionary_2_U3Cget_KeysU3Em__0_m15291_gshared)(__this /* static, unused */, ___x, method)
// V UnityEngine.UnityDictionary`2<System.String,System.Object>::<get_Values>m__1(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_ValuesU3Em__1_m15891(__this /* static, unused */, ___x, method) (( Object_t * (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityDictionary_2_U3Cget_ValuesU3Em__1_m15293_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.String,System.Object>::<get_Items>m__2(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3Cget_ItemsU3Em__2_m15892(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t3463  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityDictionary_2_U3Cget_ItemsU3Em__2_m15295_gshared)(__this /* static, unused */, ___x, method)
// System.Collections.Generic.KeyValuePair`2<K,V> UnityEngine.UnityDictionary`2<System.String,System.Object>::<CopyTo>m__6(UnityEngine.UnityKeyValuePair`2<K,V>)
#define UnityDictionary_2_U3CCopyToU3Em__6_m15893(__this /* static, unused */, ___x, method) (( KeyValuePair_2_t3463  (*) (Object_t * /* static, unused */, UnityKeyValuePair_2_t3457 *, MethodInfo*))UnityDictionary_2_U3CCopyToU3Em__6_m15297_gshared)(__this /* static, unused */, ___x, method)
