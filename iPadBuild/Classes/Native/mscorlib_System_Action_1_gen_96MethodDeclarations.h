﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t4105;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Action`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m24876_gshared (Action_1_t4105 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m24876(__this, ___object, ___method, method) (( void (*) (Action_1_t4105 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m24876_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C" void Action_1_Invoke_m24877_gshared (Action_1_t4105 * __this, UICharInfo_t1400  ___obj, MethodInfo* method);
#define Action_1_Invoke_m24877(__this, ___obj, method) (( void (*) (Action_1_t4105 *, UICharInfo_t1400 , MethodInfo*))Action_1_Invoke_m24877_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m24878_gshared (Action_1_t4105 * __this, UICharInfo_t1400  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m24878(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t4105 *, UICharInfo_t1400 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m24878_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m24879_gshared (Action_1_t4105 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m24879(__this, ___result, method) (( void (*) (Action_1_t4105 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m24879_gshared)(__this, ___result, method)
