﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Navigation
struct Navigation_t1272;
// UnityEngine.UI.Selectable
struct Selectable_t1215;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
extern "C" int32_t Navigation_get_mode_m5190 (Navigation_t1272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
extern "C" void Navigation_set_mode_m5191 (Navigation_t1272 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
extern "C" Selectable_t1215 * Navigation_get_selectOnUp_m5192 (Navigation_t1272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnUp_m5193 (Navigation_t1272 * __this, Selectable_t1215 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
extern "C" Selectable_t1215 * Navigation_get_selectOnDown_m5194 (Navigation_t1272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnDown_m5195 (Navigation_t1272 * __this, Selectable_t1215 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
extern "C" Selectable_t1215 * Navigation_get_selectOnLeft_m5196 (Navigation_t1272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnLeft_m5197 (Navigation_t1272 * __this, Selectable_t1215 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
extern "C" Selectable_t1215 * Navigation_get_selectOnRight_m5198 (Navigation_t1272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnRight_m5199 (Navigation_t1272 * __this, Selectable_t1215 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
extern "C" Navigation_t1272  Navigation_get_defaultNavigation_m5200 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
