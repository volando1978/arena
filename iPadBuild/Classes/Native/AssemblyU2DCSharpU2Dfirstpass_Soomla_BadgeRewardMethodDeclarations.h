﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.BadgeReward
struct BadgeReward_t54;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t30;

// System.Void Soomla.BadgeReward::.ctor(System.String,System.String)
extern "C" void BadgeReward__ctor_m222 (BadgeReward_t54 * __this, String_t* ___id, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.BadgeReward::.ctor(System.String,System.String,System.String)
extern "C" void BadgeReward__ctor_m223 (BadgeReward_t54 * __this, String_t* ___id, String_t* ___name, String_t* ___iconUrl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.BadgeReward::.ctor(JSONObject)
extern "C" void BadgeReward__ctor_m224 (BadgeReward_t54 * __this, JSONObject_t30 * ___jsonReward, MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject Soomla.BadgeReward::toJSONObject()
extern "C" JSONObject_t30 * BadgeReward_toJSONObject_m225 (BadgeReward_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.BadgeReward::giveInner()
extern "C" bool BadgeReward_giveInner_m226 (BadgeReward_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.BadgeReward::takeInner()
extern "C" bool BadgeReward_takeInner_m227 (BadgeReward_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
