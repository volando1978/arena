﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeQuestClient
struct NativeQuestClient_t561;
// GooglePlayGames.Native.PInvoke.QuestManager
struct QuestManager_t560;
// System.String
struct String_t;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>
struct Action_2_t550;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>
struct Action_2_t552;
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct Action_3_t554;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t861;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse>
struct Action_1_t862;
// System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>
struct Action_2_t556;
// GooglePlayGames.BasicApi.Quests.IQuestMilestone
struct IQuestMilestone_t863;
// System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>
struct Action_3_t558;
// GooglePlayGames.BasicApi.DataSource
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSource.h"
// GooglePlayGames.BasicApi.Quests.QuestFetchFlags
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestFetch.h"
// GooglePlayGames.BasicApi.Quests.QuestUiResult
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestUiRes.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestAccep.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_4.h"
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Quests_QuestClaim.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_5.h"

// System.Void GooglePlayGames.Native.NativeQuestClient::.ctor(GooglePlayGames.Native.PInvoke.QuestManager)
extern "C" void NativeQuestClient__ctor_m2309 (NativeQuestClient_t561 * __this, QuestManager_t560 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::Fetch(GooglePlayGames.BasicApi.DataSource,System.String,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,GooglePlayGames.BasicApi.Quests.IQuest>)
extern "C" void NativeQuestClient_Fetch_m2310 (NativeQuestClient_t561 * __this, int32_t ___source, String_t* ___questId, Action_2_t550 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::FetchMatchingState(GooglePlayGames.BasicApi.DataSource,GooglePlayGames.BasicApi.Quests.QuestFetchFlags,System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Collections.Generic.List`1<GooglePlayGames.BasicApi.Quests.IQuest>>)
extern "C" void NativeQuestClient_FetchMatchingState_m2311 (NativeQuestClient_t561 * __this, int32_t ___source, int32_t ___flags, Action_2_t552 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::ShowAllQuestsUI(System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern "C" void NativeQuestClient_ShowAllQuestsUI_m2312 (NativeQuestClient_t561 * __this, Action_3_t554 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::ShowSpecificQuestUI(GooglePlayGames.BasicApi.Quests.IQuest,System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern "C" void NativeQuestClient_ShowSpecificQuestUI_m2313 (NativeQuestClient_t561 * __this, Object_t * ___quest, Action_3_t554 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.QuestUiResult GooglePlayGames.Native.NativeQuestClient::UiErrorToQuestUiResult(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus)
extern "C" int32_t NativeQuestClient_UiErrorToQuestUiResult_m2314 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<GooglePlayGames.Native.PInvoke.QuestManager/QuestUIResponse> GooglePlayGames.Native.NativeQuestClient::FromQuestUICallback(System.Action`3<GooglePlayGames.BasicApi.Quests.QuestUiResult,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern "C" Action_1_t862 * NativeQuestClient_FromQuestUICallback_m2315 (Object_t * __this /* static, unused */, Action_3_t554 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::Accept(GooglePlayGames.BasicApi.Quests.IQuest,System.Action`2<GooglePlayGames.BasicApi.Quests.QuestAcceptStatus,GooglePlayGames.BasicApi.Quests.IQuest>)
extern "C" void NativeQuestClient_Accept_m2316 (NativeQuestClient_t561 * __this, Object_t * ___quest, Action_2_t556 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.QuestAcceptStatus GooglePlayGames.Native.NativeQuestClient::FromAcceptStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestAcceptStatus)
extern "C" int32_t NativeQuestClient_FromAcceptStatus_m2317 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeQuestClient::ClaimMilestone(GooglePlayGames.BasicApi.Quests.IQuestMilestone,System.Action`3<GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus,GooglePlayGames.BasicApi.Quests.IQuest,GooglePlayGames.BasicApi.Quests.IQuestMilestone>)
extern "C" void NativeQuestClient_ClaimMilestone_m2318 (NativeQuestClient_t561 * __this, Object_t * ___milestone, Action_3_t558 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.Quests.QuestClaimMilestoneStatus GooglePlayGames.Native.NativeQuestClient::FromClaimStatus(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/QuestClaimMilestoneStatus)
extern "C" int32_t NativeQuestClient_FromClaimStatus_m2319 (Object_t * __this /* static, unused */, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
