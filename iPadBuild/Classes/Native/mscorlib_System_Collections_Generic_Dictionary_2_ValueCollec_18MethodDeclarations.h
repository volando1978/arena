﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>
struct ValueCollection_t3603;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualItem>
struct Dictionary_2_t110;
// Soomla.Store.VirtualItem
struct VirtualItem_t125;
// System.Collections.Generic.IEnumerator`1<Soomla.Store.VirtualItem>
struct IEnumerator_1_t4312;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// Soomla.Store.VirtualItem[]
struct VirtualItemU5BU5D_t3564;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Soomla.Store.VirtualItem>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_46.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_10MethodDeclarations.h"
#define ValueCollection__ctor_m18017(__this, ___dictionary, method) (( void (*) (ValueCollection_t3603 *, Dictionary_2_t110 *, MethodInfo*))ValueCollection__ctor_m16162_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18018(__this, ___item, method) (( void (*) (ValueCollection_t3603 *, VirtualItem_t125 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16164_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18019(__this, method) (( void (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16166_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18020(__this, ___item, method) (( bool (*) (ValueCollection_t3603 *, VirtualItem_t125 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16168_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18021(__this, ___item, method) (( bool (*) (ValueCollection_t3603 *, VirtualItem_t125 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16170_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18022(__this, method) (( Object_t* (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16172_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m18023(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3603 *, Array_t *, int32_t, MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18024(__this, method) (( Object_t * (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18025(__this, method) (( bool (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18026(__this, method) (( bool (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16180_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m18027(__this, method) (( Object_t * (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m16182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m18028(__this, ___array, ___index, method) (( void (*) (ValueCollection_t3603 *, VirtualItemU5BU5D_t3564*, int32_t, MethodInfo*))ValueCollection_CopyTo_m16184_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::GetEnumerator()
#define ValueCollection_GetEnumerator_m18029(__this, method) (( Enumerator_t4328  (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_GetEnumerator_m16186_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Soomla.Store.VirtualItem>::get_Count()
#define ValueCollection_get_Count_m18030(__this, method) (( int32_t (*) (ValueCollection_t3603 *, MethodInfo*))ValueCollection_get_Count_m16188_gshared)(__this, method)
