﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1154;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m5737_gshared (Comparison_1_t1154 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Comparison_1__ctor_m5737(__this, ___object, ___method, method) (( void (*) (Comparison_1_t1154 *, Object_t *, IntPtr_t, MethodInfo*))Comparison_1__ctor_m5737_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m21545_gshared (Comparison_1_t1154 * __this, RaycastResult_t1187  ___x, RaycastResult_t1187  ___y, MethodInfo* method);
#define Comparison_1_Invoke_m21545(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t1154 *, RaycastResult_t1187 , RaycastResult_t1187 , MethodInfo*))Comparison_1_Invoke_m21545_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m21546_gshared (Comparison_1_t1154 * __this, RaycastResult_t1187  ___x, RaycastResult_t1187  ___y, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Comparison_1_BeginInvoke_m21546(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t1154 *, RaycastResult_t1187 , RaycastResult_t1187 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Comparison_1_BeginInvoke_m21546_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m21547_gshared (Comparison_1_t1154 * __this, Object_t * ___result, MethodInfo* method);
#define Comparison_1_EndInvoke_m21547(__this, ___result, method) (( int32_t (*) (Comparison_1_t1154 *, Object_t *, MethodInfo*))Comparison_1_EndInvoke_m21547_gshared)(__this, ___result, method)
