﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>
struct Enumerator_t3615;
// System.Object
struct Object_t;
// System.String
struct String_t;
// Soomla.Store.VirtualCategory
struct VirtualCategory_t126;
// System.Collections.Generic.Dictionary`2<System.String,Soomla.Store.VirtualCategory>
struct Dictionary_2_t112;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Soomla.Store.VirtualCategory>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m18221(__this, ___dictionary, method) (( void (*) (Enumerator_t3615 *, Dictionary_2_t112 *, MethodInfo*))Enumerator__ctor_m16190_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18222(__this, method) (( Object_t * (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16192_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18223(__this, method) (( DictionaryEntry_t2128  (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16194_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18224(__this, method) (( Object_t * (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16196_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18225(__this, method) (( Object_t * (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::MoveNext()
#define Enumerator_MoveNext_m18226(__this, method) (( bool (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_MoveNext_m16199_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::get_Current()
#define Enumerator_get_Current_m18227(__this, method) (( KeyValuePair_2_t3612  (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_get_Current_m16200_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18228(__this, method) (( String_t* (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_get_CurrentKey_m16202_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18229(__this, method) (( VirtualCategory_t126 * (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_get_CurrentValue_m16204_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::VerifyState()
#define Enumerator_VerifyState_m18230(__this, method) (( void (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_VerifyState_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18231(__this, method) (( void (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_VerifyCurrent_m16208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Soomla.Store.VirtualCategory>::Dispose()
#define Enumerator_Dispose_m18232(__this, method) (( void (*) (Enumerator_t3615 *, MethodInfo*))Enumerator_Dispose_m16210_gshared)(__this, method)
