﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PInvoke.RealtimeManager
struct RealtimeManager_t564;
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// GooglePlayGames.Native.PInvoke.RealtimeRoomConfig
struct RealtimeRoomConfig_t592;
// GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper
struct RealTimeEventListenerHelper_t594;
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>
struct Action_1_t884;
// System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>
struct Action_1_t885;
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse>
struct Action_1_t886;
// GooglePlayGames.Native.PInvoke.NativeRealTimeRoom
struct NativeRealTimeRoom_t574;
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse>
struct Action_1_t887;
// System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse>
struct Action_1_t888;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>
struct Action_1_t889;
// GooglePlayGames.Native.PInvoke.MultiplayerInvitation
struct MultiplayerInvitation_t601;
// GooglePlayGames.Native.PInvoke.MultiplayerParticipant
struct MultiplayerParticipant_t672;
// System.Byte[]
struct ByteU5BU5D_t350;
// System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>
struct Action_1_t890;
// System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>
struct List_1_t891;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_3.h"

// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::.ctor(GooglePlayGames.Native.PInvoke.GameServices)
extern "C" void RealtimeManager__ctor_m2976 (RealtimeManager_t564 * __this, GameServices_t534 * ___gameServices, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::CreateRoom(GooglePlayGames.Native.PInvoke.RealtimeRoomConfig,GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper,System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>)
extern "C" void RealtimeManager_CreateRoom_m2977 (RealtimeManager_t564 * __this, RealtimeRoomConfig_t592 * ___config, RealTimeEventListenerHelper_t594 * ___helper, Action_1_t884 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::ShowPlayerSelectUI(System.UInt32,System.UInt32,System.Boolean,System.Action`1<GooglePlayGames.Native.PInvoke.PlayerSelectUIResponse>)
extern "C" void RealtimeManager_ShowPlayerSelectUI_m2978 (RealtimeManager_t564 * __this, uint32_t ___minimumPlayers, uint32_t ___maxiumPlayers, bool ___allowAutomatching, Action_1_t885 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalPlayerSelectUIcallback(System.IntPtr,System.IntPtr)
extern "C" void RealtimeManager_InternalPlayerSelectUIcallback_m2979 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalRealTimeRoomCallback(System.IntPtr,System.IntPtr)
extern "C" void RealtimeManager_InternalRealTimeRoomCallback_m2980 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalRoomInboxUICallback(System.IntPtr,System.IntPtr)
extern "C" void RealtimeManager_InternalRoomInboxUICallback_m2981 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::ShowRoomInboxUI(System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RoomInboxUIResponse>)
extern "C" void RealtimeManager_ShowRoomInboxUI_m2982 (RealtimeManager_t564 * __this, Action_1_t886 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::ShowWaitingRoomUI(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.UInt32,System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/WaitingRoomUIResponse>)
extern "C" void RealtimeManager_ShowWaitingRoomUI_m2983 (RealtimeManager_t564 * __this, NativeRealTimeRoom_t574 * ___room, uint32_t ___minimumParticipantsBeforeStarting, Action_1_t887 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalWaitingRoomUICallback(System.IntPtr,System.IntPtr)
extern "C" void RealtimeManager_InternalWaitingRoomUICallback_m2984 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalFetchInvitationsCallback(System.IntPtr,System.IntPtr)
extern "C" void RealtimeManager_InternalFetchInvitationsCallback_m2985 (Object_t * __this /* static, unused */, IntPtr_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::FetchInvitations(System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/FetchInvitationsResponse>)
extern "C" void RealtimeManager_FetchInvitations_m2986 (RealtimeManager_t564 * __this, Action_1_t888 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalLeaveRoomCallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus,System.IntPtr)
extern "C" void RealtimeManager_InternalLeaveRoomCallback_m2987 (Object_t * __this /* static, unused */, int32_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::LeaveRoom(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus>)
extern "C" void RealtimeManager_LeaveRoom_m2988 (RealtimeManager_t564 * __this, NativeRealTimeRoom_t574 * ___room, Action_1_t889 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::AcceptInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation,GooglePlayGames.Native.PInvoke.RealTimeEventListenerHelper,System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>)
extern "C" void RealtimeManager_AcceptInvitation_m2989 (RealtimeManager_t564 * __this, MultiplayerInvitation_t601 * ___invitation, RealTimeEventListenerHelper_t594 * ___listener, Action_1_t884 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::DeclineInvitation(GooglePlayGames.Native.PInvoke.MultiplayerInvitation)
extern "C" void RealtimeManager_DeclineInvitation_m2990 (RealtimeManager_t564 * __this, MultiplayerInvitation_t601 * ___invitation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::SendReliableMessage(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,GooglePlayGames.Native.PInvoke.MultiplayerParticipant,System.Byte[],System.Action`1<GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus>)
extern "C" void RealtimeManager_SendReliableMessage_m2991 (RealtimeManager_t564 * __this, NativeRealTimeRoom_t574 * ___room, MultiplayerParticipant_t672 * ___participant, ByteU5BU5D_t350* ___data, Action_1_t890 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::InternalSendReliableMessageCallback(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/MultiplayerStatus,System.IntPtr)
extern "C" void RealtimeManager_InternalSendReliableMessageCallback_m2992 (Object_t * __this /* static, unused */, int32_t ___response, IntPtr_t ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::SendUnreliableMessageToAll(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.Byte[])
extern "C" void RealtimeManager_SendUnreliableMessageToAll_m2993 (RealtimeManager_t564 * __this, NativeRealTimeRoom_t574 * ___room, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PInvoke.RealtimeManager::SendUnreliableMessageToSpecificParticipants(GooglePlayGames.Native.PInvoke.NativeRealTimeRoom,System.Collections.Generic.List`1<GooglePlayGames.Native.PInvoke.MultiplayerParticipant>,System.Byte[])
extern "C" void RealtimeManager_SendUnreliableMessageToSpecificParticipants_m2994 (RealtimeManager_t564 * __this, NativeRealTimeRoom_t574 * ___room, List_1_t891 * ___recipients, ByteU5BU5D_t350* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.RealtimeManager::ToCallbackPointer(System.Action`1<GooglePlayGames.Native.PInvoke.RealtimeManager/RealTimeRoomResponse>)
extern "C" IntPtr_t RealtimeManager_ToCallbackPointer_m2995 (Object_t * __this /* static, unused */, Action_1_t884 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr GooglePlayGames.Native.PInvoke.RealtimeManager::<SendUnreliableMessageToSpecificParticipants>m__9B(GooglePlayGames.Native.PInvoke.MultiplayerParticipant)
extern "C" IntPtr_t RealtimeManager_U3CSendUnreliableMessageToSpecificParticipantsU3Em__9B_m2996 (Object_t * __this /* static, unused */, MultiplayerParticipant_t672 * ___r, MethodInfo* method) IL2CPP_METHOD_ATTR;
