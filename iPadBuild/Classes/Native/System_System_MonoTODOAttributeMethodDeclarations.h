﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1963;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m7854 (MonoTODOAttribute_t1963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C" void MonoTODOAttribute__ctor_m7855 (MonoTODOAttribute_t1963 * __this, String_t* ___comment, MethodInfo* method) IL2CPP_METHOD_ATTR;
