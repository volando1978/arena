﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.UnitySerializationHolder
struct UnitySerializationHolder_t2848;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1673;
// System.Type
struct Type_t;
// System.DBNull
struct DBNull_t2790;
// System.Reflection.Module
struct Module_t2517;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder__ctor_m14350 (UnitySerializationHolder_t2848 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetTypeData_m14351 (Object_t * __this /* static, unused */, Type_t * ___instance, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetDBNullData_m14352 (Object_t * __this /* static, unused */, DBNull_t2790 * ___instance, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetModuleData_m14353 (Object_t * __this /* static, unused */, Module_t2517 * ___instance, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetObjectData_m14354 (UnitySerializationHolder_t2848 * __this, SerializationInfo_t1673 * ___info, StreamingContext_t1674  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * UnitySerializationHolder_GetRealObject_m14355 (UnitySerializationHolder_t2848 * __this, StreamingContext_t1674  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
