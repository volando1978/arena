﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3438;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  List_1_t3439  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::_items
	KeyValuePair_2U5BU5D_t3438* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::_version
	int32_t ____version_3;
};
struct List_1_t3439_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EmptyArray
	KeyValuePair_2U5BU5D_t3438* ___EmptyArray_4;
};
