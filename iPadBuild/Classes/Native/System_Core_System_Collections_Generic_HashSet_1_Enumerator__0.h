﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t3762;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Int32>
struct  Enumerator_t3764 
{
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::hashset
	HashSet_1_t3762 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator<System.Int32>::current
	int32_t ___current_3;
};
