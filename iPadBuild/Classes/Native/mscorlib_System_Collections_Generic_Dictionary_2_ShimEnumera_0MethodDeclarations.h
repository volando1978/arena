﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t3541;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3530;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17171_gshared (ShimEnumerator_t3541 * __this, Dictionary_2_t3530 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m17171(__this, ___host, method) (( void (*) (ShimEnumerator_t3541 *, Dictionary_2_t3530 *, MethodInfo*))ShimEnumerator__ctor_m17171_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17172_gshared (ShimEnumerator_t3541 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m17172(__this, method) (( bool (*) (ShimEnumerator_t3541 *, MethodInfo*))ShimEnumerator_MoveNext_m17172_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m17173_gshared (ShimEnumerator_t3541 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m17173(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t3541 *, MethodInfo*))ShimEnumerator_get_Entry_m17173_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17174_gshared (ShimEnumerator_t3541 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m17174(__this, method) (( Object_t * (*) (ShimEnumerator_t3541 *, MethodInfo*))ShimEnumerator_get_Key_m17174_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17175_gshared (ShimEnumerator_t3541 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m17175(__this, method) (( Object_t * (*) (ShimEnumerator_t3541 *, MethodInfo*))ShimEnumerator_get_Value_m17175_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17176_gshared (ShimEnumerator_t3541 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m17176(__this, method) (( Object_t * (*) (ShimEnumerator_t3541 *, MethodInfo*))ShimEnumerator_get_Current_m17176_gshared)(__this, method)
