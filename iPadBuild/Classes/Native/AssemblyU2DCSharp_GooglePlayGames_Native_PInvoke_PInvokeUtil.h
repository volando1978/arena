﻿#pragma once
#include <stdint.h>
// System.Text.StringBuilder
struct StringBuilder_t36;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
struct  OutStringMethod_t681  : public MulticastDelegate_t22
{
};
