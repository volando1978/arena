﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Advertisements.Advertisement/DebugLevel
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisements_Adv.h"
// UnityEngine.Advertisements.Advertisement
struct  Advertisement_t143  : public Object_t
{
};
struct Advertisement_t143_StaticFields{
	// System.String UnityEngine.Advertisements.Advertisement::version
	String_t* ___version_0;
	// UnityEngine.Advertisements.Advertisement/DebugLevel UnityEngine.Advertisements.Advertisement::_debugLevel
	int32_t ____debugLevel_1;
	// System.Boolean UnityEngine.Advertisements.Advertisement::<UnityDeveloperInternalTestMode>k__BackingField
	bool ___U3CUnityDeveloperInternalTestModeU3Ek__BackingField_2;
};
