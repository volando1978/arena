﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.SoomlaStoreIOS
struct SoomlaStoreIOS_t82;
// System.String
struct String_t;

// System.Void Soomla.Store.SoomlaStoreIOS::.ctor()
extern "C" void SoomlaStoreIOS__ctor_m306 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_LoadBillingService()
extern "C" void SoomlaStoreIOS_soomlaStore_LoadBillingService_m307 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.SoomlaStoreIOS::soomlaStore_BuyMarketItem(System.String,System.String)
extern "C" int32_t SoomlaStoreIOS_soomlaStore_BuyMarketItem_m308 (Object_t * __this /* static, unused */, String_t* ___productId, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_RestoreTransactions()
extern "C" void SoomlaStoreIOS_soomlaStore_RestoreTransactions_m309 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_RefreshInventory()
extern "C" void SoomlaStoreIOS_soomlaStore_RefreshInventory_m310 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_RefreshMarketItemsDetails()
extern "C" void SoomlaStoreIOS_soomlaStore_RefreshMarketItemsDetails_m311 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_TransactionsAlreadyRestored(System.Boolean&)
extern "C" void SoomlaStoreIOS_soomlaStore_TransactionsAlreadyRestored_m312 (Object_t * __this /* static, unused */, bool* ___outResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::soomlaStore_SetSSV(System.Boolean,System.String)
extern "C" void SoomlaStoreIOS_soomlaStore_SetSSV_m313 (Object_t * __this /* static, unused */, bool ___ssv, String_t* ___verifyUrl, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::_loadBillingService()
extern "C" void SoomlaStoreIOS__loadBillingService_m314 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::_buyMarketItem(System.String,System.String)
extern "C" void SoomlaStoreIOS__buyMarketItem_m315 (SoomlaStoreIOS_t82 * __this, String_t* ___productId, String_t* ___payload, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::_refreshInventory()
extern "C" void SoomlaStoreIOS__refreshInventory_m316 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::_restoreTransactions()
extern "C" void SoomlaStoreIOS__restoreTransactions_m317 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.SoomlaStoreIOS::_refreshMarketItemsDetails()
extern "C" void SoomlaStoreIOS__refreshMarketItemsDetails_m318 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Soomla.Store.SoomlaStoreIOS::_transactionsAlreadyRestored()
extern "C" bool SoomlaStoreIOS__transactionsAlreadyRestored_m319 (SoomlaStoreIOS_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
