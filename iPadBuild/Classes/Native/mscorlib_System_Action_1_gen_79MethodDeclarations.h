﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t3897;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m21942_gshared (Action_1_t3897 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Action_1__ctor_m21942(__this, ___object, ___method, method) (( void (*) (Action_1_t3897 *, Object_t *, IntPtr_t, MethodInfo*))Action_1__ctor_m21942_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C" void Action_1_Invoke_m21943_gshared (Action_1_t3897 * __this, RaycastResult_t1187  ___obj, MethodInfo* method);
#define Action_1_Invoke_m21943(__this, ___obj, method) (( void (*) (Action_1_t3897 *, RaycastResult_t1187 , MethodInfo*))Action_1_Invoke_m21943_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m21944_gshared (Action_1_t3897 * __this, RaycastResult_t1187  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Action_1_BeginInvoke_m21944(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t3897 *, RaycastResult_t1187 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Action_1_BeginInvoke_m21944_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m21945_gshared (Action_1_t3897 * __this, Object_t * ___result, MethodInfo* method);
#define Action_1_EndInvoke_m21945(__this, ___result, method) (( void (*) (Action_1_t3897 *, Object_t *, MethodInfo*))Action_1_EndInvoke_m21945_gshared)(__this, ___result, method)
