﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Soomla.Store.StoreInfoIOS
struct StoreInfoIOS_t84;
// System.String
struct String_t;
// Soomla.Store.IStoreAssets
struct IStoreAssets_t176;

// System.Void Soomla.Store.StoreInfoIOS::.ctor()
extern "C" void StoreInfoIOS__ctor_m339 (StoreInfoIOS_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.StoreInfoIOS::storeInfo_SetStoreAssets(System.String,System.Int32)
extern "C" int32_t StoreInfoIOS_storeInfo_SetStoreAssets_m340 (Object_t * __this /* static, unused */, String_t* ___storeMetaJSON, int32_t ___version, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Soomla.Store.StoreInfoIOS::storeInfo_LoadFromDB()
extern "C" int32_t StoreInfoIOS_storeInfo_LoadFromDB_m341 (Object_t * __this /* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfoIOS::_setStoreAssets(Soomla.Store.IStoreAssets)
extern "C" void StoreInfoIOS__setStoreAssets_m342 (StoreInfoIOS_t84 * __this, Object_t * ___storeAssets, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Soomla.Store.StoreInfoIOS::loadNativeFromDB()
extern "C" void StoreInfoIOS_loadNativeFromDB_m343 (StoreInfoIOS_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
