﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t169;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener
struct OnGameThreadForwardingListener_t563;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E
struct  U3CPeersDisconnectedU3Ec__AnonStorey3E_t570  : public Object_t
{
	// System.String[] GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::participantIds
	StringU5BU5D_t169* ___participantIds_0;
	// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener GooglePlayGames.Native.NativeRealtimeMultiplayerClient/OnGameThreadForwardingListener/<PeersDisconnected>c__AnonStorey3E::<>f__this
	OnGameThreadForwardingListener_t563 * ___U3CU3Ef__this_1;
};
