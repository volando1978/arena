﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t3924;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3920;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22328_gshared (Enumerator_t3924 * __this, Dictionary_2_t3920 * ___host, MethodInfo* method);
#define Enumerator__ctor_m22328(__this, ___host, method) (( void (*) (Enumerator_t3924 *, Dictionary_2_t3920 *, MethodInfo*))Enumerator__ctor_m22328_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22329_gshared (Enumerator_t3924 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22329(__this, method) (( Object_t * (*) (Enumerator_t3924 *, MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m22330_gshared (Enumerator_t3924 * __this, MethodInfo* method);
#define Enumerator_Dispose_m22330(__this, method) (( void (*) (Enumerator_t3924 *, MethodInfo*))Enumerator_Dispose_m22330_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22331_gshared (Enumerator_t3924 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m22331(__this, method) (( bool (*) (Enumerator_t3924 *, MethodInfo*))Enumerator_MoveNext_m22331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22332_gshared (Enumerator_t3924 * __this, MethodInfo* method);
#define Enumerator_get_Current_m22332(__this, method) (( int32_t (*) (Enumerator_t3924 *, MethodInfo*))Enumerator_get_Current_m22332_gshared)(__this, method)
