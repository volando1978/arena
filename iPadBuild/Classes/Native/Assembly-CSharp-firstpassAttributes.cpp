﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharpU2Dfirstpass_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t241 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t241 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t241_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m1069(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m1070(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectDictionary_t12_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectDictionary_t12_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectDictionary_t12_CustomAttributesCacheGenerator_ObjectDictionary_U3Cget_KeyValuePairsU3Em__7_m19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ObjectDictionary_t12_CustomAttributesCacheGenerator_ObjectDictionary_U3Cset_KeyValuePairsU3Em__8_m20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CSetKeyValuePairU3Ec__AnonStorey7_t8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t246_il2cpp_TypeInfo_var;
void SoomlaEditorScript_t14_CustomAttributesCacheGenerator_SoomlaSettings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t246 * tmp;
		tmp = (SerializeField_t246 *)il2cpp_codegen_object_new (SerializeField_t246_il2cpp_TypeInfo_var);
		SerializeField__ctor_m1073(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_get_Key_m1080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_set_Key_m1081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_get_Value_m1082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_set_Value_m1083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_KeysU3Em__0_m1110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_ValuesU3Em__1_m1111(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_ItemsU3Em__2_m1112(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3CCopyToU3Em__6_m1113(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CU3Ec__AnonStorey4_t252_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRemoveU3Ec__AnonStorey5_t253_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CContainsKeyU3Ec__AnonStorey6_t254_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnRewardGivenU3Em__A_m41(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnRewardTakenU3Em__B_m42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnCustomEventU3Em__C_m43(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t250_il2cpp_TypeInfo_var;
void JSONObject_t30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t250 * tmp;
		tmp = (DefaultMemberAttribute_t250 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t250_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m1084(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_BakeAsync_m166(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_PrintAsync_m169(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_StringifyAsync_m170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m75(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m76(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m77(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m78(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_Dispose_m80(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_Reset_m81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m83(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m84(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m85(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_Dispose_m88(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_Reset_m89(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m92(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m93(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m94(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_Dispose_m96(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_Reset_m97(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void SoomlaExtensions_t51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t242_il2cpp_TypeInfo_var;
void SoomlaExtensions_t51_CustomAttributesCacheGenerator_SoomlaExtensions_AddOrUpdate_m1144(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t242 * tmp;
		tmp = (ExtensionAttribute_t242 *)il2cpp_codegen_object_new (ExtensionAttribute_t242_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m1071(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void EventHandler_t76_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void SoomlaStore_t63_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void SoomlaStore_t63_CustomAttributesCacheGenerator_SoomlaStore_get_Initialized_m393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void SoomlaStore_t63_CustomAttributesCacheGenerator_SoomlaStore_set_Initialized_m394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache17(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache23(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache24(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache27(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache28(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache29(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_RunLaterPriv_m433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnBillingNotSupportedU3Em__D_m464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnBillingSupportedU3Em__E_m465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnCurrencyBalanceChangedU3Em__F_m466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodBalanceChangedU3Em__10_m467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodEquippedU3Em__11_m468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodUnEquippedU3Em__12_m469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodUpgradeU3Em__13_m470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnItemPurchasedU3Em__14_m471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnItemPurchaseStartedU3Em__15_m472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseCancelledU3Em__16_m473(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseU3Em__17_m474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseStartedU3Em__18_m475(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketRefundU3Em__19_m476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnRestoreTransactionsFinishedU3Em__1A_m477(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnRestoreTransactionsStartedU3Em__1B_m478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshStartedU3Em__1C_m479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshFailedU3Em__1D_m480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshFinishedU3Em__1E_m481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnUnexpectedErrorInStoreU3Em__1F_m482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnSoomlaStoreInitializedU3Em__20_m483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_Dispose_m426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var;
void U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_Reset_m427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t257 * tmp;
		tmp = (DebuggerHiddenAttribute_t257 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t257_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m1132(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreInfo_t67_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreInfo_t67_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreInfo_t67_CustomAttributesCacheGenerator_StoreInfo_U3CGetFirstUpgradeForVirtualGoodU3Em__21_m538(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void StoreInfo_t67_CustomAttributesCacheGenerator_StoreInfo_U3CGetLastUpgradeForVirtualGoodU3Em__22_m539(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void EquippableVG_t129_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CBuyU3Ec__AnonStorey8_t140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Advertisement_t143_CustomAttributesCacheGenerator_U3CUnityDeveloperInternalTestModeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_isReady_m699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.IsReady method instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_get_UnityDeveloperInternalTestMode_m701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_set_UnityDeveloperInternalTestMode_m702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_t143____allowPrecache_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Advertisement.allowPrecache is no longer supported and does nothing"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DebugLevel_t142_CustomAttributesCacheGenerator_NONE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.DebugLevel.None instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DebugLevel_t142_CustomAttributesCacheGenerator_ERROR(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.DebugLevel.Error instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DebugLevel_t142_CustomAttributesCacheGenerator_WARNING(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.DebugLevel.Warning instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DebugLevel_t142_CustomAttributesCacheGenerator_INFO(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.DebugLevel.Info instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void DebugLevel_t142_CustomAttributesCacheGenerator_DEBUG(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Use Advertisement.DebugLevel.Debug instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void Parser_t148_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptionsExtended_t152_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_get_gamerSid_m737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_set_gamerSid_m738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_t152____gamerSid_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("Please use gamerSid on ShowOptions instead of ShowOptionsExtended"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_U3CpauseU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_U3CresultCallbackU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_pause_m823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_pause_m824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_resultCallback_m825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_resultCallback_m826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_gamerSid_m827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_gamerSid_m828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t259_il2cpp_TypeInfo_var;
void ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_t153____pause_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t259 * tmp;
		tmp = (ObsoleteAttribute_t259 *)il2cpp_codegen_object_new (ObsoleteAttribute_t259_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m1159(tmp, il2cpp_codegen_string_new_wrapper("ShowOptions.pause is no longer supported and does nothing, video ads will always pause the game"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t162_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t245 * tmp;
		tmp = (CompilerGeneratedAttribute_t245 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t245_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m1072(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_Assembly_AttributeGenerators[141] = 
{
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_Assembly_CustomAttributesCacheGenerator,
	ObjectDictionary_t12_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ObjectDictionary_t12_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ObjectDictionary_t12_CustomAttributesCacheGenerator_ObjectDictionary_U3Cget_KeyValuePairsU3Em__7_m19,
	ObjectDictionary_t12_CustomAttributesCacheGenerator_ObjectDictionary_U3Cset_KeyValuePairsU3Em__8_m20,
	U3CSetKeyValuePairU3Ec__AnonStorey7_t8_CustomAttributesCacheGenerator,
	SoomlaEditorScript_t14_CustomAttributesCacheGenerator_SoomlaSettings,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_get_Key_m1080,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_set_Key_m1081,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_get_Value_m1082,
	UnityKeyValuePair_2_t248_CustomAttributesCacheGenerator_UnityKeyValuePair_2_set_Value_m1083,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_KeysU3Em__0_m1110,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_ValuesU3Em__1_m1111,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3Cget_ItemsU3Em__2_m1112,
	UnityDictionary_2_t249_CustomAttributesCacheGenerator_UnityDictionary_2_U3CCopyToU3Em__6_m1113,
	U3CU3Ec__AnonStorey4_t252_CustomAttributesCacheGenerator,
	U3CRemoveU3Ec__AnonStorey5_t253_CustomAttributesCacheGenerator,
	U3CContainsKeyU3Ec__AnonStorey6_t254_CustomAttributesCacheGenerator,
	CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	CoreEvents_t23_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnRewardGivenU3Em__A_m41,
	CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnRewardTakenU3Em__B_m42,
	CoreEvents_t23_CustomAttributesCacheGenerator_CoreEvents_U3COnCustomEventU3Em__C_m43,
	JSONObject_t30_CustomAttributesCacheGenerator,
	JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_BakeAsync_m166,
	JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_PrintAsync_m169,
	JSONObject_t30_CustomAttributesCacheGenerator_JSONObject_StringifyAsync_m170,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m75,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m76,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m77,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m78,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_Dispose_m80,
	U3CBakeAsyncU3Ec__Iterator0_t35_CustomAttributesCacheGenerator_U3CBakeAsyncU3Ec__Iterator0_Reset_m81,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m83,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m84,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m85,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m86,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_Dispose_m88,
	U3CPrintAsyncU3Ec__Iterator1_t39_CustomAttributesCacheGenerator_U3CPrintAsyncU3Ec__Iterator1_Reset_m89,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m92,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m93,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m94,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_Dispose_m96,
	U3CStringifyAsyncU3Ec__Iterator2_t40_CustomAttributesCacheGenerator_U3CStringifyAsyncU3Ec__Iterator2_Reset_m97,
	SoomlaExtensions_t51_CustomAttributesCacheGenerator,
	SoomlaExtensions_t51_CustomAttributesCacheGenerator_SoomlaExtensions_AddOrUpdate_m1144,
	EventHandler_t76_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	SoomlaStore_t63_CustomAttributesCacheGenerator_U3CInitializedU3Ek__BackingField,
	SoomlaStore_t63_CustomAttributesCacheGenerator_SoomlaStore_get_Initialized_m393,
	SoomlaStore_t63_CustomAttributesCacheGenerator_SoomlaStore_set_Initialized_m394,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache16,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache17,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache18,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache19,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1A,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1B,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1C,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1D,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1E,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1F,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache20,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache21,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache22,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache23,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache24,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache25,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache26,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache27,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache28,
	StoreEvents_t90_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache29,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_RunLaterPriv_m433,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnBillingNotSupportedU3Em__D_m464,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnBillingSupportedU3Em__E_m465,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnCurrencyBalanceChangedU3Em__F_m466,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodBalanceChangedU3Em__10_m467,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodEquippedU3Em__11_m468,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodUnEquippedU3Em__12_m469,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnGoodUpgradeU3Em__13_m470,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnItemPurchasedU3Em__14_m471,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnItemPurchaseStartedU3Em__15_m472,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseCancelledU3Em__16_m473,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseU3Em__17_m474,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketPurchaseStartedU3Em__18_m475,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketRefundU3Em__19_m476,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnRestoreTransactionsFinishedU3Em__1A_m477,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnRestoreTransactionsStartedU3Em__1B_m478,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshStartedU3Em__1C_m479,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshFailedU3Em__1D_m480,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnMarketItemsRefreshFinishedU3Em__1E_m481,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnUnexpectedErrorInStoreU3Em__1F_m482,
	StoreEvents_t90_CustomAttributesCacheGenerator_StoreEvents_U3COnSoomlaStoreInitializedU3Em__20_m483,
	U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator,
	U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m423,
	U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m424,
	U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_Dispose_m426,
	U3CRunLaterPrivU3Ec__Iterator3_t89_CustomAttributesCacheGenerator_U3CRunLaterPrivU3Ec__Iterator3_Reset_m427,
	StoreInfo_t67_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache9,
	StoreInfo_t67_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheA,
	StoreInfo_t67_CustomAttributesCacheGenerator_StoreInfo_U3CGetFirstUpgradeForVirtualGoodU3Em__21_m538,
	StoreInfo_t67_CustomAttributesCacheGenerator_StoreInfo_U3CGetLastUpgradeForVirtualGoodU3Em__22_m539,
	EquippableVG_t129_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1,
	U3CBuyU3Ec__AnonStorey8_t140_CustomAttributesCacheGenerator,
	Advertisement_t143_CustomAttributesCacheGenerator_U3CUnityDeveloperInternalTestModeU3Ek__BackingField,
	Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_isReady_m699,
	Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_get_UnityDeveloperInternalTestMode_m701,
	Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_set_UnityDeveloperInternalTestMode_m702,
	Advertisement_t143_CustomAttributesCacheGenerator_Advertisement_t143____allowPrecache_PropertyInfo,
	DebugLevel_t142_CustomAttributesCacheGenerator_NONE,
	DebugLevel_t142_CustomAttributesCacheGenerator_ERROR,
	DebugLevel_t142_CustomAttributesCacheGenerator_WARNING,
	DebugLevel_t142_CustomAttributesCacheGenerator_INFO,
	DebugLevel_t142_CustomAttributesCacheGenerator_DEBUG,
	Parser_t148_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	ShowOptionsExtended_t152_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField,
	ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_get_gamerSid_m737,
	ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_set_gamerSid_m738,
	ShowOptionsExtended_t152_CustomAttributesCacheGenerator_ShowOptionsExtended_t152____gamerSid_PropertyInfo,
	ShowOptions_t153_CustomAttributesCacheGenerator_U3CpauseU3Ek__BackingField,
	ShowOptions_t153_CustomAttributesCacheGenerator_U3CresultCallbackU3Ek__BackingField,
	ShowOptions_t153_CustomAttributesCacheGenerator_U3CgamerSidU3Ek__BackingField,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_pause_m823,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_pause_m824,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_resultCallback_m825,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_resultCallback_m826,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_get_gamerSid_m827,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_set_gamerSid_m828,
	ShowOptions_t153_CustomAttributesCacheGenerator_ShowOptions_t153____pause_PropertyInfo,
	U3CPrivateImplementationDetailsU3E_t162_CustomAttributesCacheGenerator,
};
