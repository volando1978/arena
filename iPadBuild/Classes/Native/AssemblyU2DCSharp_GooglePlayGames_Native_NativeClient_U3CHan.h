﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.NativeClient
struct NativeClient_t524;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C
struct  U3CHandleAuthTransitionU3Ec__AnonStorey1C_t525  : public Object_t
{
	// System.UInt32 GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::currentAuthGeneration
	uint32_t ___currentAuthGeneration_0;
	// GooglePlayGames.Native.NativeClient GooglePlayGames.Native.NativeClient/<HandleAuthTransition>c__AnonStorey1C::<>f__this
	NativeClient_t524 * ___U3CU3Ef__this_1;
};
