﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t2633;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct  ClientIdentity_t2634  : public Identity_t2626
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t2633 * ____proxyReference_5;
};
