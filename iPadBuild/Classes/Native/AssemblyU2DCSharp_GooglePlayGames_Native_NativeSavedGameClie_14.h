﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<System.Object,System.Object>
struct U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3769;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>
struct  U3CToOnGameThreadU3Ec__AnonStorey4C_2_t3770  : public Object_t
{
	// T1 GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>::val1
	Object_t * ___val1_0;
	// T2 GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>::val2
	Object_t * ___val2_1;
	// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2<T1,T2> GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey4B`2/<ToOnGameThread>c__AnonStorey4C`2<System.Object,System.Object>::<>f__ref$75
	U3CToOnGameThreadU3Ec__AnonStorey4B_2_t3769 * ___U3CU3Ef__refU2475_2;
};
