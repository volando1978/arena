﻿#pragma once
#include <stdint.h>
// System.Action`1<System.Int32>
struct Action_1_t911;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Int32>
struct  U3CAsOnGameThreadCallbackU3Ec__AnonStorey1A_1_t3722  : public Object_t
{
	// System.Action`1<T> GooglePlayGames.Native.NativeClient/<AsOnGameThreadCallback>c__AnonStorey1A`1<System.Int32>::callback
	Action_1_t911 * ___callback_0;
};
