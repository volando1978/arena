﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback
struct ShowUICallback_t433;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro.h"

// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void ShowUICallback__ctor_m1760 (ShowUICallback_t433 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::Invoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr)
extern "C" void ShowUICallback_Invoke_m1761 (ShowUICallback_t433 * __this, int32_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ShowUICallback_t433(Il2CppObject* delegate, int32_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::BeginInvoke(GooglePlayGames.Native.Cwrapper.CommonErrorStatus/UIStatus,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * ShowUICallback_BeginInvoke_m1762 (ShowUICallback_t433 * __this, int32_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.LeaderboardManager/ShowUICallback::EndInvoke(System.IAsyncResult)
extern "C" void ShowUICallback_EndInvoke_m1763 (ShowUICallback_t433 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
