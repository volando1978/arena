﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip
struct AudioClip_t753;
// System.Object
#include "mscorlib_System_Object.h"
// SoundManager/soundLibrary
struct  soundLibrary_t754  : public Object_t
{
	// UnityEngine.AudioClip SoundManager/soundLibrary::nBulletSnd
	AudioClip_t753 * ___nBulletSnd_0;
	// UnityEngine.AudioClip SoundManager/soundLibrary::wBulletSnd
	AudioClip_t753 * ___wBulletSnd_1;
	// UnityEngine.AudioClip SoundManager/soundLibrary::laserBulletSnd
	AudioClip_t753 * ___laserBulletSnd_2;
	// UnityEngine.AudioClip SoundManager/soundLibrary::triBulletSnd
	AudioClip_t753 * ___triBulletSnd_3;
	// UnityEngine.AudioClip SoundManager/soundLibrary::circularBulletSnd
	AudioClip_t753 * ___circularBulletSnd_4;
	// UnityEngine.AudioClip SoundManager/soundLibrary::moireBulletSnd
	AudioClip_t753 * ___moireBulletSnd_5;
	// UnityEngine.AudioClip SoundManager/soundLibrary::dustXPLSnd
	AudioClip_t753 * ___dustXPLSnd_6;
	// UnityEngine.AudioClip SoundManager/soundLibrary::bombaXPLSnd
	AudioClip_t753 * ___bombaXPLSnd_7;
	// UnityEngine.AudioClip SoundManager/soundLibrary::smallBombaXPLSnd
	AudioClip_t753 * ___smallBombaXPLSnd_8;
	// UnityEngine.AudioClip SoundManager/soundLibrary::bombaAmarillaXPLSnd
	AudioClip_t753 * ___bombaAmarillaXPLSnd_9;
	// UnityEngine.AudioClip SoundManager/soundLibrary::risaSpeechSnd
	AudioClip_t753 * ___risaSpeechSnd_10;
	// UnityEngine.AudioClip SoundManager/soundLibrary::bolasSpeechSnd
	AudioClip_t753 * ___bolasSpeechSnd_11;
	// UnityEngine.AudioClip SoundManager/soundLibrary::tresViasSpeechSnd
	AudioClip_t753 * ___tresViasSpeechSnd_12;
	// UnityEngine.AudioClip SoundManager/soundLibrary::laserSpeechSnd
	AudioClip_t753 * ___laserSpeechSnd_13;
	// UnityEngine.AudioClip SoundManager/soundLibrary::circularSpeechSnd
	AudioClip_t753 * ___circularSpeechSnd_14;
	// UnityEngine.AudioClip SoundManager/soundLibrary::repeteaSpeechSnd
	AudioClip_t753 * ___repeteaSpeechSnd_15;
	// UnityEngine.AudioClip SoundManager/soundLibrary::rayosSpeechSnd
	AudioClip_t753 * ___rayosSpeechSnd_16;
	// UnityEngine.AudioClip SoundManager/soundLibrary::fantasmaSpeechSnd
	AudioClip_t753 * ___fantasmaSpeechSnd_17;
	// UnityEngine.AudioClip SoundManager/soundLibrary::bombaSpeechSnd
	AudioClip_t753 * ___bombaSpeechSnd_18;
	// UnityEngine.AudioClip SoundManager/soundLibrary::boomSpeechSnd
	AudioClip_t753 * ___boomSpeechSnd_19;
	// UnityEngine.AudioClip SoundManager/soundLibrary::caralludoSpeechSnd
	AudioClip_t753 * ___caralludoSpeechSnd_20;
	// UnityEngine.AudioClip SoundManager/soundLibrary::crispadoSpeechSnd
	AudioClip_t753 * ___crispadoSpeechSnd_21;
	// UnityEngine.AudioClip SoundManager/soundLibrary::arenaWomanSnd
	AudioClip_t753 * ___arenaWomanSnd_22;
	// UnityEngine.AudioClip SoundManager/soundLibrary::raysWomanSnd
	AudioClip_t753 * ___raysWomanSnd_23;
	// UnityEngine.AudioClip SoundManager/soundLibrary::explosionWomanSnd
	AudioClip_t753 * ___explosionWomanSnd_24;
	// UnityEngine.AudioClip SoundManager/soundLibrary::awesomeWomanSnd
	AudioClip_t753 * ___awesomeWomanSnd_25;
	// UnityEngine.AudioClip SoundManager/soundLibrary::interferenciaGameSnd
	AudioClip_t753 * ___interferenciaGameSnd_26;
	// UnityEngine.AudioClip SoundManager/soundLibrary::pillaPaqueteGameSnd
	AudioClip_t753 * ___pillaPaqueteGameSnd_27;
	// UnityEngine.AudioClip SoundManager/soundLibrary::buttonShortSnd
	AudioClip_t753 * ___buttonShortSnd_28;
	// UnityEngine.AudioClip SoundManager/soundLibrary::buttonLongSnd
	AudioClip_t753 * ___buttonLongSnd_29;
	// UnityEngine.AudioClip SoundManager/soundLibrary::dingSnd
	AudioClip_t753 * ___dingSnd_30;
};
