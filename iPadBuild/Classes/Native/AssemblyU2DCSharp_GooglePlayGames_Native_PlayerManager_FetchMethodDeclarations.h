﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.PlayerManager/FetchSelfResponse
struct FetchSelfResponse_t684;
// GooglePlayGames.Native.NativePlayer
struct NativePlayer_t675;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_Native_Cwrapper_CommonErro_1.h"
// System.Runtime.InteropServices.HandleRef
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"

// System.Void GooglePlayGames.Native.PlayerManager/FetchSelfResponse::.ctor(System.IntPtr)
extern "C" void FetchSelfResponse__ctor_m2870 (FetchSelfResponse_t684 * __this, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.Cwrapper.CommonErrorStatus/ResponseStatus GooglePlayGames.Native.PlayerManager/FetchSelfResponse::Status()
extern "C" int32_t FetchSelfResponse_Status_m2871 (FetchSelfResponse_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.NativePlayer GooglePlayGames.Native.PlayerManager/FetchSelfResponse::Self()
extern "C" NativePlayer_t675 * FetchSelfResponse_Self_m2872 (FetchSelfResponse_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.PlayerManager/FetchSelfResponse::CallDispose(System.Runtime.InteropServices.HandleRef)
extern "C" void FetchSelfResponse_CallDispose_m2873 (FetchSelfResponse_t684 * __this, HandleRef_t657  ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.Native.PlayerManager/FetchSelfResponse GooglePlayGames.Native.PlayerManager/FetchSelfResponse::FromPointer(System.IntPtr)
extern "C" FetchSelfResponse_t684 * FetchSelfResponse_FromPointer_m2874 (Object_t * __this /* static, unused */, IntPtr_t ___selfPointer, MethodInfo* method) IL2CPP_METHOD_ATTR;
