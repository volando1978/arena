﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t1263;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.UI.Text>
struct  Action_1_t3956  : public MulticastDelegate_t22
{
};
