﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43
struct U3CToOnGameThreadU3Ec__AnonStorey43_t619;
// GooglePlayGames.BasicApi.SavedGame.IConflictResolver
struct IConflictResolver_t617;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t618;
// System.Byte[]
struct ByteU5BU5D_t350;

// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43::.ctor()
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey43__ctor_m2509 (U3CToOnGameThreadU3Ec__AnonStorey43_t619 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeSavedGameClient/<ToOnGameThread>c__AnonStorey43::<>m__49(GooglePlayGames.BasicApi.SavedGame.IConflictResolver,GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[],GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata,System.Byte[])
extern "C" void U3CToOnGameThreadU3Ec__AnonStorey43_U3CU3Em__49_m2510 (U3CToOnGameThreadU3Ec__AnonStorey43_t619 * __this, Object_t * ___resolver, Object_t * ___original, ByteU5BU5D_t350* ___originalData, Object_t * ___unmerged, ByteU5BU5D_t350* ___unmergedData, MethodInfo* method) IL2CPP_METHOD_ATTR;
