﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// HUD
struct HUD_t733;
// System.Object
#include "mscorlib_System_Object.h"
// HUD/<run>c__Iterator1
struct  U3CrunU3Ec__Iterator1_t734  : public Object_t
{
	// System.Int32 HUD/<run>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object HUD/<run>c__Iterator1::$current
	Object_t * ___U24current_1;
	// HUD HUD/<run>c__Iterator1::<>f__this
	HUD_t733 * ___U3CU3Ef__this_2;
};
