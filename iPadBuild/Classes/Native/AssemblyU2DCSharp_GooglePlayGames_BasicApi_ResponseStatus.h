﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// GooglePlayGames.BasicApi.ResponseStatus
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_ResponseStatus.h"
// GooglePlayGames.BasicApi.ResponseStatus
struct  ResponseStatus_t335 
{
	// System.Int32 GooglePlayGames.BasicApi.ResponseStatus::value__
	int32_t ___value___1;
};
