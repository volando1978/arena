﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityNameValuePair`1<System.Object>
struct UnityNameValuePair_1_t3456;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void UnityEngine.UnityNameValuePair`1<System.Object>::.ctor()
extern "C" void UnityNameValuePair_1__ctor_m15857_gshared (UnityNameValuePair_1_t3456 * __this, MethodInfo* method);
#define UnityNameValuePair_1__ctor_m15857(__this, method) (( void (*) (UnityNameValuePair_1_t3456 *, MethodInfo*))UnityNameValuePair_1__ctor_m15857_gshared)(__this, method)
// System.Void UnityEngine.UnityNameValuePair`1<System.Object>::.ctor(System.String,V)
extern "C" void UnityNameValuePair_1__ctor_m15858_gshared (UnityNameValuePair_1_t3456 * __this, String_t* ___key, Object_t * ___value, MethodInfo* method);
#define UnityNameValuePair_1__ctor_m15858(__this, ___key, ___value, method) (( void (*) (UnityNameValuePair_1_t3456 *, String_t*, Object_t *, MethodInfo*))UnityNameValuePair_1__ctor_m15858_gshared)(__this, ___key, ___value, method)
// System.String UnityEngine.UnityNameValuePair`1<System.Object>::get_Key()
extern "C" String_t* UnityNameValuePair_1_get_Key_m15859_gshared (UnityNameValuePair_1_t3456 * __this, MethodInfo* method);
#define UnityNameValuePair_1_get_Key_m15859(__this, method) (( String_t* (*) (UnityNameValuePair_1_t3456 *, MethodInfo*))UnityNameValuePair_1_get_Key_m15859_gshared)(__this, method)
// System.Void UnityEngine.UnityNameValuePair`1<System.Object>::set_Key(System.String)
extern "C" void UnityNameValuePair_1_set_Key_m15860_gshared (UnityNameValuePair_1_t3456 * __this, String_t* ___value, MethodInfo* method);
#define UnityNameValuePair_1_set_Key_m15860(__this, ___value, method) (( void (*) (UnityNameValuePair_1_t3456 *, String_t*, MethodInfo*))UnityNameValuePair_1_set_Key_m15860_gshared)(__this, ___value, method)
