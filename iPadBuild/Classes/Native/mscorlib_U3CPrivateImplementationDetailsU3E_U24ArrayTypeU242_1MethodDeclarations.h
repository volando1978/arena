﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct U24ArrayTypeU242048_t2866;
struct U24ArrayTypeU242048_t2866_marshaled;

void U24ArrayTypeU242048_t2866_marshal(const U24ArrayTypeU242048_t2866& unmarshaled, U24ArrayTypeU242048_t2866_marshaled& marshaled);
void U24ArrayTypeU242048_t2866_marshal_back(const U24ArrayTypeU242048_t2866_marshaled& marshaled, U24ArrayTypeU242048_t2866& unmarshaled);
void U24ArrayTypeU242048_t2866_marshal_cleanup(U24ArrayTypeU242048_t2866_marshaled& marshaled);
