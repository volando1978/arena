﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OptionalAttribute
struct OptionalAttribute_t2346;

// System.Void System.Runtime.InteropServices.OptionalAttribute::.ctor()
extern "C" void OptionalAttribute__ctor_m10719 (OptionalAttribute_t2346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
