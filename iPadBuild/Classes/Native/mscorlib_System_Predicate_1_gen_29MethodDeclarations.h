﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Matrix4x4>
struct Predicate_1_t3838;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"

// System.Void System.Predicate`1<UnityEngine.Matrix4x4>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m21166_gshared (Predicate_1_t3838 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method);
#define Predicate_1__ctor_m21166(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3838 *, Object_t *, IntPtr_t, MethodInfo*))Predicate_1__ctor_m21166_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Matrix4x4>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m21167_gshared (Predicate_1_t3838 * __this, Matrix4x4_t997  ___obj, MethodInfo* method);
#define Predicate_1_Invoke_m21167(__this, ___obj, method) (( bool (*) (Predicate_1_t3838 *, Matrix4x4_t997 , MethodInfo*))Predicate_1_Invoke_m21167_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Matrix4x4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m21168_gshared (Predicate_1_t3838 * __this, Matrix4x4_t997  ___obj, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method);
#define Predicate_1_BeginInvoke_m21168(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3838 *, Matrix4x4_t997 , AsyncCallback_t20 *, Object_t *, MethodInfo*))Predicate_1_BeginInvoke_m21168_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Matrix4x4>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m21169_gshared (Predicate_1_t3838 * __this, Object_t * ___result, MethodInfo* method);
#define Predicate_1_EndInvoke_m21169(__this, ___result, method) (( bool (*) (Predicate_1_t3838 *, Object_t *, MethodInfo*))Predicate_1_EndInvoke_m21169_gshared)(__this, ___result, method)
