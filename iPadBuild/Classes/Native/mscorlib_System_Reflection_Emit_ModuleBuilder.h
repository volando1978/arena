﻿#pragma once
#include <stdint.h>
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t2506;
// System.Char[]
struct CharU5BU5D_t41;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// System.Reflection.Emit.ModuleBuilder
struct  ModuleBuilder_t2516  : public Module_t2517
{
	// System.Reflection.Emit.AssemblyBuilder System.Reflection.Emit.ModuleBuilder::assemblyb
	AssemblyBuilder_t2506 * ___assemblyb_10;
};
struct ModuleBuilder_t2516_StaticFields{
	// System.Char[] System.Reflection.Emit.ModuleBuilder::type_modifiers
	CharU5BU5D_t41* ___type_modifiers_11;
};
