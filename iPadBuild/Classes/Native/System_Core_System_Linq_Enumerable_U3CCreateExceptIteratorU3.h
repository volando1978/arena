﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t221;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3482;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3580;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>
struct  U3CCreateExceptIteratorU3Ec__Iterator4_1_t3766  : public Object_t
{
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::second
	Object_t* ___second_0;
	// System.Collections.Generic.IEqualityComparer`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::comparer
	Object_t* ___comparer_1;
	// System.Collections.Generic.HashSet`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<items>__0
	HashSet_1_t3580 * ___U3CitemsU3E__0_2;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::first
	Object_t* ___first_3;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<$s_49>__1
	Object_t* ___U3CU24s_49U3E__1_4;
	// TSource System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<element>__2
	Object_t * ___U3CelementU3E__2_5;
	// System.Int32 System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::$PC
	int32_t ___U24PC_6;
	// TSource System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::$current
	Object_t * ___U24current_7;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<$>second
	Object_t* ___U3CU24U3Esecond_8;
	// System.Collections.Generic.IEqualityComparer`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<$>comparer
	Object_t* ___U3CU24U3Ecomparer_9;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateExceptIterator>c__Iterator4`1<System.Object>::<$>first
	Object_t* ___U3CU24U3Efirst_10;
};
