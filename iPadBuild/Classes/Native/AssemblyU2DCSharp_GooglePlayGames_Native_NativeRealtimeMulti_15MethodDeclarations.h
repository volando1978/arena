﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState
struct ShutdownState_t587;
// GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession
struct RoomSession_t566;

// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::.ctor(GooglePlayGames.Native.NativeRealtimeMultiplayerClient/RoomSession)
extern "C" void ShutdownState__ctor_m2437 (ShutdownState_t587 * __this, RoomSession_t566 * ___session, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::IsActive()
extern "C" bool ShutdownState_IsActive_m2438 (ShutdownState_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.NativeRealtimeMultiplayerClient/ShutdownState::LeaveRoom()
extern "C" void ShutdownState_LeaveRoom_m2439 (ShutdownState_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
