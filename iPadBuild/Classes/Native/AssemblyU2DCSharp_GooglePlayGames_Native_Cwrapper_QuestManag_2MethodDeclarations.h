﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback
struct ClaimMilestoneCallback_t447;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t19;
// System.AsyncCallback
struct AsyncCallback_t20;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ClaimMilestoneCallback__ctor_m1881 (ClaimMilestoneCallback_t447 * __this, Object_t * ___object, IntPtr_t ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::Invoke(System.IntPtr,System.IntPtr)
extern "C" void ClaimMilestoneCallback_Invoke_m1882 (ClaimMilestoneCallback_t447 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ClaimMilestoneCallback_t447(Il2CppObject* delegate, IntPtr_t ___arg0, IntPtr_t ___arg1);
// System.IAsyncResult GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::BeginInvoke(System.IntPtr,System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * ClaimMilestoneCallback_BeginInvoke_m1883 (ClaimMilestoneCallback_t447 * __this, IntPtr_t ___arg0, IntPtr_t ___arg1, AsyncCallback_t20 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.Native.Cwrapper.QuestManager/ClaimMilestoneCallback::EndInvoke(System.IAsyncResult)
extern "C" void ClaimMilestoneCallback_EndInvoke_m1884 (ClaimMilestoneCallback_t447 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
