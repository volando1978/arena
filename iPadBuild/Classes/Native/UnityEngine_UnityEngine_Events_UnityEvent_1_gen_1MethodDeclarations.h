﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t1208;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t1359;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1647;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C" void UnityEvent_1__ctor_m5841_gshared (UnityEvent_1_t1208 * __this, MethodInfo* method);
#define UnityEvent_1__ctor_m5841(__this, method) (( void (*) (UnityEvent_1_t1208 *, MethodInfo*))UnityEvent_1__ctor_m5841_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m5844_gshared (UnityEvent_1_t1208 * __this, UnityAction_1_t1359 * ___call, MethodInfo* method);
#define UnityEvent_1_AddListener_m5844(__this, ___call, method) (( void (*) (UnityEvent_1_t1208 *, UnityAction_1_t1359 *, MethodInfo*))UnityEvent_1_AddListener_m5844_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m22545_gshared (UnityEvent_1_t1208 * __this, UnityAction_1_t1359 * ___call, MethodInfo* method);
#define UnityEvent_1_RemoveListener_m22545(__this, ___call, method) (( void (*) (UnityEvent_1_t1208 *, UnityAction_1_t1359 *, MethodInfo*))UnityEvent_1_RemoveListener_m22545_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m6304_gshared (UnityEvent_1_t1208 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m6304(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t1208 *, String_t*, Object_t *, MethodInfo*))UnityEvent_1_FindMethod_Impl_m6304_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1647 * UnityEvent_1_GetDelegate_m6305_gshared (UnityEvent_1_t1208 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, MethodInfo* method);
#define UnityEvent_1_GetDelegate_m6305(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1647 * (*) (UnityEvent_1_t1208 *, Object_t *, MethodInfo_t *, MethodInfo*))UnityEvent_1_GetDelegate_m6305_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1647 * UnityEvent_1_GetDelegate_m22546_gshared (Object_t * __this /* static, unused */, UnityAction_1_t1359 * ___action, MethodInfo* method);
#define UnityEvent_1_GetDelegate_m22546(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1647 * (*) (Object_t * /* static, unused */, UnityAction_1_t1359 *, MethodInfo*))UnityEvent_1_GetDelegate_m22546_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m5843_gshared (UnityEvent_1_t1208 * __this, Color_t747  ___arg0, MethodInfo* method);
#define UnityEvent_1_Invoke_m5843(__this, ___arg0, method) (( void (*) (UnityEvent_1_t1208 *, Color_t747 , MethodInfo*))UnityEvent_1_Invoke_m5843_gshared)(__this, ___arg0, method)
