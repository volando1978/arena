﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>
struct ShimEnumerator_t3667;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt32>
struct Dictionary_2_t3655;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m19034_gshared (ShimEnumerator_t3667 * __this, Dictionary_2_t3655 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m19034(__this, ___host, method) (( void (*) (ShimEnumerator_t3667 *, Dictionary_2_t3655 *, MethodInfo*))ShimEnumerator__ctor_m19034_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m19035_gshared (ShimEnumerator_t3667 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m19035(__this, method) (( bool (*) (ShimEnumerator_t3667 *, MethodInfo*))ShimEnumerator_MoveNext_m19035_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Entry()
extern "C" DictionaryEntry_t2128  ShimEnumerator_get_Entry_m19036_gshared (ShimEnumerator_t3667 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m19036(__this, method) (( DictionaryEntry_t2128  (*) (ShimEnumerator_t3667 *, MethodInfo*))ShimEnumerator_get_Entry_m19036_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m19037_gshared (ShimEnumerator_t3667 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m19037(__this, method) (( Object_t * (*) (ShimEnumerator_t3667 *, MethodInfo*))ShimEnumerator_get_Key_m19037_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m19038_gshared (ShimEnumerator_t3667 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m19038(__this, method) (( Object_t * (*) (ShimEnumerator_t3667 *, MethodInfo*))ShimEnumerator_get_Value_m19038_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.UInt32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m19039_gshared (ShimEnumerator_t3667 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m19039(__this, method) (( Object_t * (*) (ShimEnumerator_t3667 *, MethodInfo*))ShimEnumerator_get_Current_m19039_gshared)(__this, method)
