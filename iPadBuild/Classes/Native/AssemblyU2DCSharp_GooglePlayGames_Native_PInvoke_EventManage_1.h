﻿#pragma once
#include <stdint.h>
// GooglePlayGames.Native.PInvoke.GameServices
struct GameServices_t534;
// System.Object
#include "mscorlib_System_Object.h"
// GooglePlayGames.Native.PInvoke.EventManager
struct  EventManager_t548  : public Object_t
{
	// GooglePlayGames.Native.PInvoke.GameServices GooglePlayGames.Native.PInvoke.EventManager::mServices
	GameServices_t534 * ___mServices_0;
};
