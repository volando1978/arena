﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t3944;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t37;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t166;
// System.Object[]
struct ObjectU5BU5D_t208;
// System.Predicate`1<System.Object>
struct Predicate_1_t3421;
// System.Comparison`1<System.Object>
struct Comparison_1_t3429;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m22554_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1__ctor_m22554(__this, method) (( void (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1__ctor_m22554_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556(__this, method) (( Object_t * (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m22556_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m22558_gshared (IndexedSet_1_t3944 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Add_m22558(__this, ___item, method) (( void (*) (IndexedSet_1_t3944 *, Object_t *, MethodInfo*))IndexedSet_1_Add_m22558_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m22560_gshared (IndexedSet_1_t3944 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Remove_m22560(__this, ___item, method) (( bool (*) (IndexedSet_1_t3944 *, Object_t *, MethodInfo*))IndexedSet_1_Remove_m22560_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m22562_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m22562(__this, method) (( Object_t* (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1_GetEnumerator_m22562_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m22564_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1_Clear_m22564(__this, method) (( void (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1_Clear_m22564_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m22566_gshared (IndexedSet_1_t3944 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Contains_m22566(__this, ___item, method) (( bool (*) (IndexedSet_1_t3944 *, Object_t *, MethodInfo*))IndexedSet_1_Contains_m22566_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m22568_gshared (IndexedSet_1_t3944 * __this, ObjectU5BU5D_t208* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define IndexedSet_1_CopyTo_m22568(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t3944 *, ObjectU5BU5D_t208*, int32_t, MethodInfo*))IndexedSet_1_CopyTo_m22568_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m22570_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1_get_Count_m22570(__this, method) (( int32_t (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1_get_Count_m22570_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m22572_gshared (IndexedSet_1_t3944 * __this, MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m22572(__this, method) (( bool (*) (IndexedSet_1_t3944 *, MethodInfo*))IndexedSet_1_get_IsReadOnly_m22572_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m22574_gshared (IndexedSet_1_t3944 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_IndexOf_m22574(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t3944 *, Object_t *, MethodInfo*))IndexedSet_1_IndexOf_m22574_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m22576_gshared (IndexedSet_1_t3944 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Insert_m22576(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t3944 *, int32_t, Object_t *, MethodInfo*))IndexedSet_1_Insert_m22576_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m22578_gshared (IndexedSet_1_t3944 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_RemoveAt_m22578(__this, ___index, method) (( void (*) (IndexedSet_1_t3944 *, int32_t, MethodInfo*))IndexedSet_1_RemoveAt_m22578_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m22580_gshared (IndexedSet_1_t3944 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_get_Item_m22580(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t3944 *, int32_t, MethodInfo*))IndexedSet_1_get_Item_m22580_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m22582_gshared (IndexedSet_1_t3944 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define IndexedSet_1_set_Item_m22582(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t3944 *, int32_t, Object_t *, MethodInfo*))IndexedSet_1_set_Item_m22582_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m22583_gshared (IndexedSet_1_t3944 * __this, Predicate_1_t3421 * ___match, MethodInfo* method);
#define IndexedSet_1_RemoveAll_m22583(__this, ___match, method) (( void (*) (IndexedSet_1_t3944 *, Predicate_1_t3421 *, MethodInfo*))IndexedSet_1_RemoveAll_m22583_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m22584_gshared (IndexedSet_1_t3944 * __this, Comparison_1_t3429 * ___sortLayoutFunction, MethodInfo* method);
#define IndexedSet_1_Sort_m22584(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t3944 *, Comparison_1_t3429 *, MethodInfo*))IndexedSet_1_Sort_m22584_gshared)(__this, ___sortLayoutFunction, method)
